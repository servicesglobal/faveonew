package com.org.faveo.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.org.faveo.Base.BaseClass;



public class DbManager
{
	private static Connection con = null; //sql
	private static Connection conn = null; //mysql

	//SQL Server
	public static void setDbConnection() throws SQLException, ClassNotFoundException
	{
		
		
		try
		{
			String[][] TestCaseData1 = BaseClass.excel_Files("Credentials");
			String Environment = TestCaseData1[3][1].toString().trim();
			
		if(Environment.contains("UAT")){
		Class.forName(TestConfig.driverUAT);
		con =	DriverManager.getConnection(TestConfig.dbConnectionUrlUAT, TestConfig.dbUserNameUAT, TestConfig.dbPasswordUAT);
		
		if(!con.isClosed())
			System.out.println("Successfully connected to SQL server");
		}else if(Environment.contains("QC")){
			Class.forName(TestConfig.driverQC);
			con =	DriverManager.getConnection(TestConfig.dbConnectionUrlQC, TestConfig.dbUserNameQC, TestConfig.dbPasswordQC);
			
			if(!con.isClosed())
				System.out.println("Successfully connected to SQL server");
			}	
		else if(Environment.contains("Staging")){
			Class.forName(TestConfig.driverStage);
			con =	DriverManager.getConnection(TestConfig.dbConnectionUrlStage, TestConfig.dbUserNameStage, TestConfig.dbPasswordStage);
			
			if(!con.isClosed())
				System.out.println("Successfully connected to SQL server");
			}
		
		
	}catch(Exception e)
		{
		System.err.println("Exception: " + e.getMessage());

		}
		
		
	}
	
		
	public static List<String> getSqlQuery(String query) throws SQLException{
		
		
		Statement St = con.createStatement();
		ResultSet rs = St.executeQuery(query);
		List<String> values = new ArrayList<String>();
		
		while(rs.next())
		{
		
			values.add(rs.getString("policynum")+  "\t" + rs.getString("proposalnum")+  "\t" +rs.getString("PROCESSSTATUSCD") );
			
			
		}
		return values;
	}
	
	
	public static Connection getConnection()
	{
		return con;
			}
}
