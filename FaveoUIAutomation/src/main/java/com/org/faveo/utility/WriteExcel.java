package com.org.faveo.utility;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.relevantcodes.extentreports.ExtentTest;

public class WriteExcel {

	private static XSSFSheet ExcelWSheet;

	private static XSSFWorkbook ExcelWBook;

	private static XSSFCell Cell;

	private static XSSFRow Row;

	static String FilePath = System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx";
	static String FilePath1 = System.getProperty("user.dir") + "\\TestData\\Renewal_Data.xlsx";

	// This method is to set the File path and to open the Excel file, Pass
	// Excel Path and Sheetname as Arguments to this method

	public static void setExcelFile(String Path, String SheetName) throws Exception {

		try {

			// Open the Excel file

			FileInputStream ExcelFile = new FileInputStream(FilePath);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

		} catch (Exception e) {

			throw (e);

		}

	}

	// This method is to read the test data from the Excel cell, in this we are
	// passing parameters as Row num and Col num

	public static String getCellData(int RowNum, int ColNum) throws Exception {

		try {

			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

			String CellData = Cell.getStringCellValue();

			return CellData;

		} catch (Exception e) {

			return "";

		}

	}

	// This method is to write in the Excel cell, Row num and Col num are the
	// parameters

	public static void setCellData(String SheetName, String Result, int RowNum, int ColNum) throws Exception {

		try {
			FileInputStream ExcelFile = new FileInputStream(FilePath);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			Row = ExcelWSheet.getRow(RowNum);

			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);

			/* if (Cell == null) { */

			Cell = Row.createCell(ColNum);

			Cell.setCellValue(Result);

			/* } else { */

			Cell.setCellValue(Result);

			// }

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(FilePath);

			ExcelWBook.write(fileOut);

			fileOut.flush();

			// fileOut.close();

		} catch (Exception e) {

			throw (e);

		}

	}

	public static void setCellData1(String SheetName, String TestData, String Result, int RowNum, int ColNum,
			int Colnum) throws Exception {
		try {
			FileInputStream ExcelFile = new FileInputStream(FilePath);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			Row = ExcelWSheet.getRow(RowNum);

			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);

			Cell = Row.createCell(Colnum);
			Cell.setCellValue(TestData);

			Cell = Row.createCell(ColNum);

			Cell.setCellValue(Result);

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(FilePath);

			ExcelWBook.write(fileOut);

			fileOut.flush();

			// fileOut.close();

		} catch (Exception e) {

			throw (e);

		}

	}

	public static void setCellData2(String SheetName, String Result, int RowNum, int ColNum) throws Exception {

		try {
			FileInputStream ExcelFile = new FileInputStream(FilePath1);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			Row = ExcelWSheet.getRow(RowNum);

			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);

			/* if (Cell == null) { */

			Cell = Row.createCell(ColNum);

			Cell.setCellValue(Result);

			/* } else { */

			Cell.setCellValue(Result);

			// }

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(FilePath1);

			ExcelWBook.write(fileOut);

			fileOut.flush();

			// fileOut.close();

		} catch (Exception e) {

			throw (e);

		}

	}

	public static void setCellData2(String SheetName, String TestData, String Result, int RowNum, int ColNum,
			int Colnum) throws Exception {

		try {
			FileInputStream ExcelFile = new FileInputStream(FilePath1);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			Row = ExcelWSheet.getRow(RowNum);

			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);

			Cell = Row.createCell(Colnum);
			Cell.setCellValue(TestData);

			Cell = Row.createCell(ColNum);

			Cell.setCellValue(Result);

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(FilePath1);

			ExcelWBook.write(fileOut);

			fileOut.flush();

			// fileOut.close();

		} catch (Exception e) {

			throw (e);

		}

	}

}