package com.org.faveo.utility;
public class TestConfig

{

public static String server = "mail.religare.com";
public static String from = "rhiclqctech@religare.com";
public static String password = "mail@123";

public static String subject = "Faveo Automation Test Report";
//public static String[] to = {"bijaya.sahoo@ext.religare.in","harshit.jain@ext.religare.in","sunder.shyam@ext.religare.in",""};
//public static String[] to ={"manojkumar1@ext.religare.in"};,"raja.vaishnav@religare.com","amit.ty@religare.com","deepak.mahajan@ext.religare.in","k.santhosh@religare.com","prateek.jaiswal@religare.com","gaurav.kumr@religare.com","manojkumar1@ext.religare.in"
public static String[] to ={"bijaya.sahoo@ext.religare.in"};
//public static String[] to ={"bijaya.sahoo@ext.religare.in","harshit.jain@ext.religare.in","chandan.rai@ext.religare.in","raja.vaishnav@religare.com","amit.ty@religare.com","sunder.shyam@ext.religare.in","gaurav.kumr@religare.com","k.santhosh@religare.com","amitshr@religare.com","jain.shashank@religare.com"};
public static String messageBody ="Hi Team, <br> <br> Please Find the attached Faveo Automation Report. <br> <br> <br> Kind Regards, <br> Bijaya Kumar Sahoo <br> Software Test Engineer <br> Monocept";
//public static String attachmentPath=System.getProperty("user.dir") + "/Reports/TestReport.html";
//public static String attachmentPath=System.getProperty("user.dir") + "/Reports/";
public static String attachmentPath=System.getProperty("user.dir") + ".\\reports\\TestReport.html";
//public static String attachmentName="Automation TestReport.html";

public static String attachmentName="TestReport.html";

public static String attachmentPath1="D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx";
public static String attachmentName1="Test Data Sheet.xlsx"; 

//For Gmail
/*	public static String server="mail.religare.com";
	public static String from = "rhiclqctech@religare.com";
	public static String password = "mail@123";
	
	public static String[] to ={"arynadevi@gmail.com"};//"deepak@monocept.com","harshit@monocept.com","ashish@monocept.com","amit.ty@religare.com","raja.vaishnav@religare.com"
	public static String subject = "Faveo Automation Test Report";
	
	public static String messageBody ="Hi Team, <br> <br> Please Find the attached Faveo Automation Report. <br> <br> <br> Kind Regards, <br> Ashish Singh <br> Software Test Engineer <br> Monocept";
	public static String attachmentPath=System.getProperty("user.dir") + "/Reports/TestReport.html";
	public static String attachmentName="Automation TestReport.html";
	public static String attachmentPath1=System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx";
	public static String attachmentName1="Test Data Sheet.xlsx"; */
	
	
	
	//QC SQL DATABASE DETAILS	
	public static String driverQC="oracle.jdbc.driver.OracleDriver"; 
	public static String dbConnectionUrlQC="jdbc:oracle:thin:@10.216.30.112:1521:PROPDEV"; 
	public static String dbUserNameQC="RHQCDEV"; 
	public static String dbPasswordQC="RHQCDEV"; 
	
	
	
/*	//QC SQL DATABASE DETAILS	
		public static String driverQC="oracle.jdbc.driver.OracleDriver"; 
		public static String dbConnectionUrlQC="jdbc:oracle:thin:@10.216.30.112:1521:POC12CDB"; 
		public static String dbUserNameQC="RHQCDEV"; 
		public static String dbPasswordQC="RHQCDEV#059"; 
		*/
	
	
	
	
	
	//UAT SQL DATABASE DETAILS	
	public static String driverUAT="oracle.jdbc.driver.OracleDriver"; 
	public static String dbConnectionUrlUAT="jdbc:oracle:thin:@10.216.30.112:1521:POC12CDB"; 
	public static String dbUserNameUAT="rhuat"; 
	public static String dbPasswordUAT="rhuat#123"; 
	
	
	//Staging DATABASE DETAILS
	public static String driverStage="oracle.jdbc.driver.OracleDriver"; 
	public static String dbConnectionUrlStage="jdbc:oracle:thin:@10.216.30.112:1521:POC12CDB"; 
	public static String dbUserNameStage="rhstage"; 
	public static String dbPasswordStage="rhstage"; 
	
	
	
	
}
