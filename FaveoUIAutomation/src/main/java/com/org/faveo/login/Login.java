package com.org.faveo.login;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.google.common.base.Functions;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.utility.ReadExcel;

 public class Login extends Reusable {
	
	
	/*@BeforeTest
	public static void start() throws Exception{
		Reusable.LaunchBrowser();
		Thread.sleep(15000);
		Reusable.login();
		System.out.println("=========Execution starts============");
	}*/
	
	@Test(priority=1)
	public static void POSCare() throws Exception{
		Reusable.Quatotion("POS");
		//Functions.selectdropdowns("POS");
		//Functions.linklist();
		Reusable.dropdownall();
		Reusable.possuminsured();
		Reusable.Addon();
		Reusable.Checkpremium();
		Reusable.Premiumpage();
		Reusable.QuestionSet();
		Reusable.payment2();
		
		
	}
	/*@Test(priority=2)
public static void POSFreedom() throws Exception{
	System.out.println("=================POS freedom execution starts====================");
	Functions.Quatotion("POSFreedom");
	Functions.frredomddown();
}*/
	
	/*@Test(priority=3)
	public static void GroupExplore() throws Exception{
		Reusable.quatotionpage("GroupExpplore");
		Reusable.startenddate();
		Reusable.PED();
		Reusable.selectAge();
		Reusable.suminsuredGroup("ProcessType");
		Reusable.verifyPremium();
		Reusable.personaldetails();
		Reusable.Group_Explore_Insured_details();

		Reusable.Group_question();
		Reusable.payment2();
	}*/
	@Test(priority=4)
	public static void careSenior() throws Exception{
		Reusable.Quatotion("CareSenior");
		//Reusable.Seniordropdownall();
		//Reusable.selectdrop();
		Reusable.dpdown();
		Reusable.seniorsuminsured();
		Reusable.Addon();
		Reusable.Checkpremium();
		Reusable.Insured_details();
		//Reusable.QuestionSet();
		Reusable.senior();
		Reusable.payment2();
	}
	
	@Test(priority=5)
	public static void StudentExplore() throws Exception {
		System.out.println("===================Execution started==================");
		BaseClass.excel_Files("StudentExploreTestcase");
		System.out.println("rowCount is "+rowCount);
		
		for( n=1;n<=BaseClass.rowCount;n++) {
			
		Reusable.StudentTravel("StudentExplore");
		Reusable.studentdp();
		Reusable.verifyPremium();
		Reusable.personaldetails();
		Reusable.TravelInsuredDetails();
		Reusable.EducationalDetails();
		Reusable.sponserDetails();
		Reusable.studentExplore_Questionaries();
		Reusable.StudentExplore_Payment();
	
	        }  // for loop completed
	     }
	@Test(priority=6)
		public static void POSCareSuperSaver() throws Exception {
			
		System.out.println("=================================POS care super saver execution started===============================");
		                        
		BaseClass.excel_Files("TestCasesPOSSuperSaver");
		System.out.println("rowCount is "+rowCount);
		System.out.println("Total number of rows : "+BaseClass.rowCount);
		int rowc=rowCount;
		for( n=1;n<=rowc;n++) {
			Reusable.LaunchBrowser();
			Thread.sleep(15000);
			Reusable.login();
			System.out.println("Total number of rows : "+BaseClass.rowCount);
			Reusable.Quatotion("POSCareSuperSaver");
		Reusable.POSCaresuperdropdown();
		Reusable.sliderdrag();
		Reusable.Tenure();      
		Reusable.verifyPremium();
		Reusable.proposerDetails();
		Reusable.Insured_details();
		Reusable.POS_Supersaver_QuestionSet();
		Reusable.POSSuperSaverPayment();
		}
	}
		@Test(priority=7)
		public static void supermediclaimcritical() throws Exception  {
			
			System.out.println("Super Medical : Critical Script Execution Started");
			BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
			System.out.println("rowCount is "+rowCount);
			System.out.println("Total number of rows : "+BaseClass.rowCount);
			int rowc=rowCount;
			for( n=1;n<=rowc;n++) {
			for( n=1;n<=61;n++) {
				Reusable.LaunchBrowser();
				Thread.sleep(15000);
				Reusable.login();
				
				try {
					System.out.println("Total number of rows : "+BaseClass.rowCount);
Reusable.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
Reusable.criticaldropdown();
Reusable.critical_dragdrop();
Reusable.Tenurecritical();
Reusable.critical_dragdrop();
Reusable.Monthly_frequency();
Reusable.critical_verify_premium();
Reusable.Critical_proposerDetails();
Reusable.critical_insuredDetails();
//Reusable.critical_questionset();
Reusable.super_criticalQuestionSet();
Reusable.Critical_Payment();
driver.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			continue;
		}
	
			}
		}
 }
 
