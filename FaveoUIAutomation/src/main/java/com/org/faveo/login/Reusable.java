
package com.org.faveo.login;


import java.io.File;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.bridj.cpp.std.list;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class Reusable extends BaseClass implements AccountnSettingsInterface {
	public static String PolicyType = null;
	public static String PolicyType2 = null;
	public static String testName = null;
	public static String covertype=null;
	public static String persontitle=null;
	public static Integer n;

	public static void login() throws Exception {

		String[][] loginData = BaseClass.excel_Files("Sheet1");

		/*
		 * String[][] TestCaseData=BaseClass.excel_Files("TestCases");
		 * String[][] FamilyData=BaseClass.excel_Files("FamilyMembers");
		 * String[][] QuestionSetData=BaseClass.excel_Files("QuestionSet");
		 * String[][] carddetails=BaseClass.excel_Files("carddetails");
		 */
		// BaseClass.readingdata; l
		enterText(By.xpath(Textbox_Username_Xpath), loginData[13][0].toString().trim());

		// Enter Password in Password TextBox
		enterText(By.xpath(Textbox_Pasword_Xpath), loginData[13][1].toString().trim());
		waitForElements(By.id(Button_Signin_Id));

		clickElement(By.id(Button_Signin_Id));
		Thread.sleep(5000);
		System.out.println("Successfully loged in");
		Thread.sleep(5000);
		String title = driver.getTitle();
		System.out.println("Titlle is: " + title);

		/* String expected=driver.getTitle();
		 * System.out.println("title is:"+expected); String actual=null;
		 */
		if (!driver.getCurrentUrl().equals("Religare Health Insurance")) {
			System.out.println("successfully logged in");
		} else {
			System.out.println("UserId or password that you have entered is incorrect.");
		}
	}
	// driver.findElement(By.xpath("//*[@id='navbar']/ul/li[2]/a")).click();
	public static void quatotionpage(String TypeTest) throws Exception{
		PolicyType = TypeTest;
		//PolicyType2 = TestType;
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		testName = TypeTest.toString();

		if (TypeTest.contains("GroupExpplore")) {
			Thread.sleep(2000);
			clickElement(By.xpath(travelinsurance_xpath));
			Thread.sleep(4000);

			clickElement(By.xpath(GroupExplore_xpath));
		}
		List<WebElement>traveldropdown=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
		int dropsize=traveldropdown.size();
		System.out.println("Total dropdowns are :"  +dropsize);
		int travelingto=	Integer.parseInt(TestCaseData[1][49].toString().trim());
		int scheme=	Integer.parseInt(TestCaseData[1][50].toString().trim());
		System.out.println("Data is :"+travelingto +scheme);
		Thread.sleep(5000);
		for(WebElement dropdownvalue:traveldropdown){	
			dropdownvalue.click();
			//System.out.println("Text is : "+dropdownvalue.getText());
			Thread.sleep(3000);
			if (dropdownvalue.getText().contains("Asia")) {
				driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/ul/li["
						+ travelingto+ "]")).click();//select 4 based on the excel data
				Thread.sleep(5000);
				//break;
			}
			if (dropdownvalue.getText().contains("Budget")) {
				driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]"
						+ "/div/div[2]/div/ui-dropdown/div/div/ul/li["+scheme+"]")).click();//select 4 based on the excel data
				Thread.sleep(5000);
				break;
			}

		}
	}
	public static void startenddate() throws Exception{
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String StartDate = TestCaseData[1][51].toString().trim();	
		System.out.println("Excel from date is  :"   +StartDate );
		String monthdate[]=StartDate.split(",");			
		String excelday = monthdate[0];
		String excelmonth = monthdate[1];
		String excelYear = monthdate[2];
		driver.findElement(By.xpath("//input[@id='start_date']")).click();
		String CMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
		String CYears=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
		WebElement next=driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
		while(!((excelYear.contentEquals(CYears)) && (excelmonth.contentEquals(CMonth))))
		{
			Thread.sleep(5000);
			next.click();
			CMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			CYears=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
		}
		driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+excelday+"'"+")]")).click();
		System.out.println("Entered Start_Date on Faveo UI is for Group Explore is:"+StartDate);	


		String EndDate = TestCaseData[1][52].toString().trim();
		String enddate[]=EndDate.split(",");			
		String endday = enddate[0];
		String endmonth = enddate[1];
		String endYear = enddate[2];
		driver.findElement(By.xpath("//input[@id='end_date']")).click();
		String endCMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
		String endCYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
		WebElement endCNext = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next Button
		while(!((endYear.contentEquals(endCYear)) && (endmonth.contentEquals(endCMonth))))
		{
			Thread.sleep(2000);
			endCNext.click();
			endCMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			endYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			endCNext = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
		}
		driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+endday+"'"+")]")).click();
		System.out.println("Entered End_Date on Faveo UI for Group Explore is :"+EndDate);	
	}
	public static void PED() throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String pedcase= TestCaseData[1][53].toString().trim();   //Yes or No
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		System.out.println("Ped case is  :"   +pedcase);
		List<WebElement> peddropdown=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[2]/div/div/div/a"));
		int total_pedvalue=peddropdown.size();
		//System.out.println("Total size is  :"  +total_pedvalue +peddropdown.get(1).getText());
		for(WebElement pedtypes:peddropdown){
			System.out.println("Total types is : " +pedtypes.getText());
			pedtypes.click();
			if (pedtypes.getText().contains("No")) {
				driver.findElement(By.xpath("//*[@class='toolbar_plan_name_input']/div/div/ul/li["+pedcase+"]")).click();

			}
		}
		List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		try{
			for (WebElement DropDownName : dropdown) {
				if (DropDownName.getText().equals("1")) {
					DropDownName.click();
					int Travellers = Integer.parseInt(TestCaseData[1][54].toString().trim());
					Thread.sleep(2000);
					clickElement(By.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
					//clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
					System.out.println("Total Number of Travellers Selected : "+Travellers);
					break;
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Unable to Select Member.");
		}
	}
	public static void selectAge() throws Exception{

		String[][] TestCaseData = BaseClass.excel_Files("TestCases");

		String[][] FamilyData = BaseClass.excel_Files("GroupExplore_FamilyData");
		String[][] Group_Travel=BaseClass.excel_Files("Group_Travel");
		int membersSize = Integer.parseInt(TestCaseData[1][54].toString().trim());
		System.out.println("Total member size is : "  +membersSize);
		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int dpsize=dropdown.size();
		System.out.println("Total number of dps are  :"+dpsize);
		System.out.println("Total Number of Member in Excel : "+membersSize);
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		outer:

			for (WebElement DropDownName : dropdown) 
			{
				String TravelAge = DropDownName.getText();
				System.out.println("TravelAge name is :"+TravelAge);
				if(TravelAge.contains("Up to 40 years")){
					//System.out.println("TravelAge name is------------:"+TravelAge);
					DropDownName.click();



					if (membersSize == 1) 
					{
						//List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));



						// reading members from test cases sheet memberlist 1,2,3,4,5
						String Membersdetails = TestCaseData[1][55];
						System.out.println("Members details is : "+Membersdetails);

						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split(",");

							member:
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									System.out.println("value of Mcount Index : "+mcount);
									mcountindex = mcountindex + 1;


									driver.findElement(By.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'up to 40 Years')]")).click();


									// List Age of members dropdown
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));

									String text = Group_Travel[mcount][0].toString().trim();

									for (WebElement ListData : List) 
									{

										if (ListData.getText().contains(Group_Travel[mcount][0].toString().trim())) 
										{

											Thread.sleep(1000);
											ListData.click();

											if (count == membersSize) 
											{
												break outer;
											} 
											else 
											{
												count = count + 1;
												break member;
												//break outer;
											}

										}

									}
								}
						}

					}  

					else

					{
						List <WebElement> dropdowns = DropDownName.findElements(By.xpath("//Ul[@class='dropdown-menu dropdown_menu_focus month_year q_dMenue']/li"));
						//WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) 
						{
							//System.out.println("DropDowns is :"+DropDowns.getText());
							if (DropDowns.getText().equals("Up to 40 years")) 
							{


								String Membersdetails = TestCaseData[1][55];
								//System.out.println("Memeber details :"  +Membersdetails);
								if (Membersdetails.contains(",")) 
								{

									BaseClass.membres = Membersdetails.split(",");
								} else {
									//BaseClass.membres = Membersdetails.split(" ");
									System.out.println("Hello");
								}
								System.out.println("total members are :" +BaseClass.membres.length);
								member:
									// total number of members

									for (int i = 0; i <= BaseClass.membres.length; i++) {

										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;



										List<WebElement> totallist=driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));			

										Thread.sleep(5000);
										for (WebElement ListData : totallist) {
											//System.out.println("List of datas are:  "  +  Li.get(i).getText());
											if (ListData.getText().contains(Group_Travel[mcount][0].toString().trim())) {
												System.out.println("Selcted Age Of Member :" + ListData.getText());

												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													break member;
												}

											}

										}
									}
							}
						}
					}
				}
			}

	}
	public static void suminsuredGroup(String ProcessType) throws Exception{

		PolicyType = ProcessType;
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		//Reading Mobile number from Excel
		String mnumber = TestCaseData[1][5].toString().trim();
		int size = mnumber.length();
		System.out.println("mobile number is: " + mnumber);
		String format = "^[789]\\d{9}$";


		if (mnumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}
		//Reading email from excel
		String email = TestCaseData[1][6].toString().trim();
		System.out.println("Email is :" + email);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		Thread.sleep(5000);
		String suminsured_value=TestCaseData[1][56].toString().trim();
		System.out.println("Insured Value is  :"  +suminsured_value);

		WebElement progress= driver.findElement(By.xpath("//*[@class='progress_slider_container col-md-12']/div/div"));


		WebElement travelingdp=driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/a"));
		String trvaelingtext=travelingdp.getText();
		System.out.println("Travelling to drop down text is  :"   +trvaelingtext);
		WebElement schmedp=driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[2]/div/ui-dropdown/div/div/a"));
		String schmetext=schmedp.getText();
		System.out.println("Scheme dropdown text is :"  + schmetext);
		String Traveldpvalue=TestCaseData[1][65].toString().trim(); //value from excel/
		String schemedp_value=TestCaseData[1][66].toString().trim();  //values from excel
		System.out.println("Excel test data  is : "  + Traveldpvalue  +schemedp_value);

		List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
		String SliderValue=null;
		for(WebElement Slider:Slidnumber) {

			//System.out.println("Slider get Text  is : "+Slider.getText());
			System.out.println("Policy type is : "+PolicyType);

			SliderValue=suminsured_value;

			/*if(PolicyType.contains("ProcessType")) {
				SliderValue=suminsured_value;
			}
			else if(PolicyType.contains("Secure")) {
				SliderValue="25";
			}
			else if(PolicyType.contains("NCB")) {
				SliderValue="75";
			}else if(PolicyType.contains("SuperSaver")) {
				SliderValue="4";
			}else if(PolicyType.contains("HNI")) {
				SliderValue="50";
			}*/
			if(Slider.getText().equals(SliderValue.toString())) {
				Slider.click();
				break;
			}
		}

	}
	public static void verifyPremium() throws InterruptedException{
		//	WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]/b[1]"));
		WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div/label[2]/span"));
		Thread.sleep(10000);
		String firstpage_PremiumValue=firstpage_Premium.getText();
		Thread.sleep(10000);
		System.out.println("First page premium value is   :"  +firstpage_PremiumValue);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,150)", "");
		//clickElement(By.xpath(Group_BuyNow_xpath));
		clickElement(By.xpath(POSSuperSaver_xpath));
		Thread.sleep(5000);
		jse.executeScript("window.scrollBy(0,-350)", "");
		//WebElement secondpage_premium=driver.findElement(By.xpath("//div[@class='col-md-12 tr_quotation_heading']/h4/span/span[@class='ng-binding']"));

		WebElement secondpage_premium=driver.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		String secondpage_PremiumValue=secondpage_premium.getText();
		Thread.sleep(10000);
		System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
		Assert.assertEquals(firstpage_PremiumValue, secondpage_PremiumValue);

	}



	public static void Quatotion(String TestType) throws Exception {

		PolicyType = TestType;
		PolicyType2 = TestType;
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembersPOSSuperSaver");
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] TestCaseDataCritical = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		testName = TestType.toString();
		Thread.sleep(5000);
		if(TestType.contains("POSCareSuperSaver")) {
			System.out.println("++++Started pos care++++++");
			clickElement(By.xpath(Health_Insurance_xppath));
			Thread.sleep(8000);
			mousehover(By.xpath(care_xpath));
			clickElement(By.xpath(POsCareSUperSaver_xpath));

		}else if (TestType.contains("super_mediclaim_critical")) {
			clickElement(By.xpath(Health_Insurance_xppath));
			Thread.sleep(8000);
			mousehover(By.xpath(mediclaimcare_xpath));
			clickElement(By.xpath(supermediclaim_xpath));
		}else if (TestType.contains("CareSenior")) {
			clickElement(By.xpath(Health_Insurance_xppath));
			Thread.sleep(8000);
			mousehover(By.xpath(care_xpath));
			clickElement(By.xpath(CareSenior_xpath));
		}

		else if (TestType.contains("POSFreedom")) {
			clickElement(By.xpath(Health_Insurance_xppath));
			Thread.sleep(4000);
			mousehover(By.xpath(care_xpath));
			clickElement(By.xpath(posfreedom_xpath));
		}
		else if (TestType.contains("POS")) {
			clickElement(By.xpath(Health_Insurance_xppath));
			Thread.sleep(4000);
			mousehover(By.xpath(care_xpath));
			clickElement(By.xpath(poscare_xpath));
		} else if (TestType.contains("Enhance")) {
			clickElement(By.xpath(Health_Insurance_xppath));
			Thread.sleep(4000);
			clickElement(By.xpath(enhancebutton_xpath));
		} else if (TestType.contains("Secure")) {
			clickElement(By.xpath(FixedBenefitInsurance_xpath));
			Thread.sleep(4000);
			clickElement(By.xpath(securebutton_xpath));
		} else if (TestType.contains("Health_Insurance_xppath")) {
			clickElement(By.xpath(Health_Insurance_xppath));
			waitForElements(By.xpath(care_xpath));
		} else if (TestType.contains("NCB")) {
			mousehover(By.xpath(care_xpath));
			waitForElements(By.xpath(care_with_NCB_xpath));
			clickElement(By.xpath(care_with_NCB_xpath));
		} else if (TestType.contains("SuperSaver")) {
			mousehover(By.xpath(care_xpath));
			waitForElements(By.xpath(care_super_saver_xpath));
			clickElement(By.xpath(care_super_saver_xpath));
		} else if (TestType.contains("HNI")) {
			mousehover(By.xpath(care_xpath));
			waitForElements(By.xpath(care_for_HNI_xpath));
			clickElement(By.xpath(care_for_HNI_xpath));
		} else if (TestType.contains("HNI")) {
			System.out.println("Click on HNI");
		}
		driver.findElement(By.name("name")).clear();
		driver.findElement(By.name("name")).sendKeys(TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
		driver.findElement(By.name("ValidEmail")).clear();

		String email = TestCaseData[n][6].toString().trim();
		System.out.println("Email is :" + email);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		Thread.sleep(5000);
		String mnumber = TestCaseData[n][5].toString().trim();
		int size = mnumber.length();
		System.out.println("mobile number is: " + mnumber);
		String format = "^[789]\\d{9}$";

		if (mnumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}
	}

	public static void dropdownall() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");

		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));  
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ TestCaseData[1][15].toString().trim() + "]")).click();//select 4 based on the excel Test case data
					System.out.println("Total Number of Member Selected : " + TestCaseData[1][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// again call the dropdown
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		try {
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[1][16];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By
											.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

									// List Age of members dropdown
									List<WebElement> List = driver
											.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[1][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By
								.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
						.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[1][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println(
											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[1][31].toString().trim());
									clickElement(
											By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By
											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[1][16];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")) {
								//} else if (DropDowns.getText().equals("46 - 50 Yrs")) {
								String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}
										}
									}
							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Done ");
		}
	}
	public static void frredomddown() throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");

		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ TestCaseData[1][15].toString().trim() + "]")).click();//select 4 based on the excel data
					System.out.println("Total Number of Member Selected : " + TestCaseData[1][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// again call the dropdown
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		try {
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[1][16];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By
											.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

									// List Age of members dropdown
									List<WebElement> List = driver
											.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[1][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By
								.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
						.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[1][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println(
											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[1][31].toString().trim());
									clickElement(
											By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By
											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[1][16];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							String Membersdetails = TestCaseData[1][16];
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							System.out.println("Drop down text is :"+DropDowns.getText());
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								System.out.println("Drop down text is :"+DropDowns.getText());
								//} else if (DropDowns.getText().equals("5 - 24 years")) {
							} else if (DropDowns.getText().equals("46 - 50 Yrs")) {
								System.out.println("text is "+Membersdetails);
								//String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										//List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
										List<WebElement> List = driver.findElements(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div[1]/div/div/div/ul/li[1]/a"));
										System.out.println("Total number of data is :"  +List.size());
										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}
										}
									}
							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Done ");
		}

	}
	public static void Posdropdownall() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");

		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ TestCaseData[1][15].toString().trim() + "]")).click();
					System.out.println("Total Number of Member Selected : " + TestCaseData[1][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// again call the dropdown
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		try {
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[1][16];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By
											.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

									// List Age of members dropdown
									List<WebElement> List = driver
											.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[1][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By
								.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
						.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[1][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println(
											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[1][31].toString().trim());
									clickElement(
											By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By
											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[1][16];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")) {

								String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}
										}
									}
							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Done ");
		}
	}

	public static void selectdropdowns(String Test) throws Exception {
		String[][] TestCaseData=BaseClass.excel_Files("TestCases");
		String[][] FamilyData=BaseClass.excel_Files("FamilyMembers");

		/*		if(Test.contains("POS")) {
				List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
				try{
				for (WebElement DropDownName : dropdown) {
								DropDownName.click();
								if (DropDownName.getText().equals("1")) {
								driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[1][15].toString().trim() + "]")).click();
									System.out.println("Total Number of Member Selected : " + TestCaseData[1][15].toString().trim());
									Thread.sleep(5000);
									break;
								}
							}}
							catch(Exception e){
								logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
								BaseClass.AbacusURL();
				}


				// again call the dropdown
				Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				try{
							outer:

							for (WebElement DropDownName : dropdown) {

							if (membersSize == 1) {

								String Membersdetails = TestCaseData[1][16];
									if (Membersdetails.contains("")) 
									{

										BaseClass.membres = Membersdetails.split("");

										member:
										// total number of members
										for (int i = 0; i <= BaseClass.membres.length; i++) {

											// one by one will take from 83 line
											mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
											mcountindex = mcountindex + 1;

						driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();

						// List Age of members dropdown
											List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

											for (WebElement ListData : List) 
											{

												if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
													System.out.println("Selcted Age Of Member :" + ListData.getText());

													ListData.click();

													if (count == membersSize) {
														break outer;
													} else {
														count = count + 1;
														break member;
													}

												}

											}
										}
									}

								} else if (DropDownName.getText().contains("Individual")) {
									System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

									if (TestCaseData[1][17].toString().trim().equals("Individual")) {
										covertype = 1;
									} else {
										covertype = 2;
									}
									DropDownName.click();
									driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ covertype + "]")).click();
									// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
									Thread.sleep(10000);
									if (covertype == 2) 
									{
										List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
										for (WebElement DropDowns : dropdowns) 
										{
											if (DropDowns.getText().contains("Floater")) 
											{
												System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
											} else if (DropDowns.getText().equals(TestCaseData[1][15].toString().trim())) {
												System.out.println("DropDownName is  " + DropDowns.getText());
											} else if (DropDowns.getText().equals("2")) {
												System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
											} else if (DropDowns.getText().equals("18 - 24 years")) {
												// reading members from test cases sheet
												// memberlist
									int Children = Integer.parseInt(TestCaseData[1][31].toString().trim());
									clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
									System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
												String Membersdetails = TestCaseData[1][16];
												if (Membersdetails.contains(",")) {


													BaseClass.membres = Membersdetails.split(",");
												} else {
													System.out.println("Hello");
												}

												member:
												for (int i = 0; i <= BaseClass.membres.length; i++) {

													// one by one will take from 83 line
													mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
													mcountindex = mcountindex + 1;

													DropDowns.click();
													// List Age of members dropdown

													List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

													for (WebElement ListData : List) {


														if (ListData.getText()
																.contains(FamilyData[mcount][0].toString().trim())) {
															System.out.println("Age of Eldest Member is :" + ListData.getText());
															Thread.sleep(2000);
															ListData.click();

															if (count == membersSize) {
																break outer;
															} else {
																count = count + 1;
																// break member;
																break outer;
															}

														}

													}
												}

											}
										}
									}
								} 


							}
				}
		 */


		if(Test.contains("Enhance")) {
			List <WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

			for(WebElement DropDownName:dropdown) {
				DropDownName.click();
				if(DropDownName.getText().equals("1")) {
					String age=TestCaseData[1][15].toString().trim();
					System.out.println("Age is :"+age);
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+TestCaseData[1][15].toString().trim()+"]")).click();
					Thread.sleep(10000);
					break;
				}
			}
			System.out.println("dropdown size is "+dropdown.size());

			for(WebElement DropDownName:dropdown) {
				//DropDownName.click();
				System.out.println("DropDownName.getText()  :"+DropDownName.getText());
				if(Test.contains("Enhance") && DropDownName.getText().contains("18 to 24 Years"))  {
					DropDownName.click();
					//Thread.sleep(10000);
					String Eldest_Age=TestCaseData[1][47].toString().trim();
					System.out.println("Eldest from testcase sheet is: "  +Eldest_Age);
					//for(int i=0; i<=BaseClass.membres.length;i++) {

					int mcountindex=0;

					int mcount= Integer.parseInt(Eldest_Age);
					mcountindex=mcountindex+1;

					List<WebElement> list = driver.findElements(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li"));
					//System.out.println("List is :"+list.get(index));
					int mcountE = 0;
					for(WebElement ListData:list) {
						System.out.println("List is :"+list.get(mcountE).getText());
						if(ListData.getText().contains(FamilyData[mcount][0].toString().trim())){
							System.out.println("List Data is :"+ListData.getText());	
							ListData.click();
							break;
						}	else {
							mcountE =mcountE+1;
						}
					}
				}}
		}


		else if(Test.contains("Secure"))
		{
			List <WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

			for(WebElement DropDownName:dropdown) {
				DropDownName.click();
				if(DropDownName.getText().equals("1")) {
					String age=TestCaseData[6][16].toString().trim();
					System.out.println("Age is :"+age);
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+TestCaseData[6][16].toString().trim()+"]")).click();
					Thread.sleep(10000);
					break;
				}
			}
			//again call the dropdown
			dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

			//Thread.sleep(3000);
			int membersSize =Integer.parseInt(TestCaseData[6][16].toString().trim());
			int count=1;
			int mcount;
			int mcountindex=0;
			int covertype;
			System.out.println("dropdown size is "+dropdown.size());  //3 dropdowns
			outer:

				for(WebElement DropDownName:dropdown) {
					System.out.println("DropDownName is  "+DropDownName.getText());
					if(DropDownName.getText().equals(TestCaseData[6][15].toString().trim())) {
						System.out.println("DropDownName is  "+DropDownName.getText());
					}else if(DropDownName.getText().contains("Individual")) {
						System.out.
						println("DropDownName is  "+DropDownName.getText());
						if(TestCaseData[1][17].toString().trim().equals("Individual")) {
							covertype=1;
						}else {
							covertype=2;
						}
						DropDownName.click();
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+covertype+"]")).click();
						Thread.sleep(10000);

					}else {

						//reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[6][16];
						if(Membersdetails.contains(",")) {

							//data taking form test case sheet which is 7,4,1,8,2,5
							BaseClass.membres = Membersdetails.split(",");

							member:
								//total number of members
								for(int i=0; i<=BaseClass.membres.length;i++) {
									//System.out.println("Mdeatils is : "+membres);

									//one by one will take from 83 line
									mcount= Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex=mcountindex+1;

									DropDownName.click();
									//List Age of members dropdown
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
									//*[@class='ng-binding' and contains(text(), 'year')]


									//List <WebElement> ListName = driver.findElements(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+count+"]"));

									for(WebElement ListData:List) {

										if(ListData.getText().contains(FamilyData[mcount][0].toString().trim())){
											System.out.println("List Data is :"+ListData.getText());	
											ListData.click();


											if(count==membersSize) {
												break outer;
											}else {
												count=count+1;
												break member;
											}

										}

									}
								}
						}else {


							//data taking form test case sheet which is 7,4,1,8,2,5
							BaseClass.membres = Membersdetails.split(" ");
							System.out.println("BaseClass.membres.length "+BaseClass.membres.length);

							member:
								//total number of members
								for(int i=0; i<=BaseClass.membres.length-1;i++) {
									//System.out.println("Mdeatils is : "+membres);

									//one by one will take from 83 line
									mcount= Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex=mcountindex+1;

									//DropDownName.click();
									//List Age of members dropdown
									List<WebElement> List =null;
									if(testName.equalsIgnoreCase("Secure")) {
										List = driver.findElements(By.xpath("//ul[@class=\"dropdown-menu dropdown_menu_focus month_year\"]/li"));
										//*[@class='ng-binding' and contains(text(), 'year')]
									}else {
										List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
									}

									//List <WebElement> ListName = driver.findElements(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+count+"]"));

									for(WebElement ListData:List) {

										if(ListData.getText().contains(FamilyData[mcount][0].toString().trim())){
											System.out.println("List Data is :"+ListData.getText());	
											ListData.click();


											if(count==membersSize) {
												break outer;
											}else {
												count=count+1;
												break member;
											}

										}

									}
								}

						}
					}

				}

			String Anualincome=TestCaseData[1][30].toString().trim();
			System.out.println("Annual income is :"+Anualincome);
			driver.findElement(By.xpath("//input[@name='annualIncome']")).sendKeys(Anualincome);

			WebElement e1=driver.findElement(By.xpath("//input[@id='RadioJ1q']"));
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", e1);
			e1.click();		
			Thread.sleep(5000);

			WebElement progress= driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/div"));

			List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
			String SliderValue=null;
			for(WebElement Slider:Slidnumber) {

				//System.out.println("Slider get Text  is : "+Slider.getText());
				System.out.println("Policy type is : "+PolicyType);
				if(PolicyType.contains("Secure")) {
					SliderValue="25";
				}
				else if(PolicyType.contains("NCB")) {
					SliderValue="75";
				}else if(PolicyType.contains("SuperSaver")) {
					SliderValue="4";
				}else if(PolicyType.contains("HNI")) {
					SliderValue="50";
				}
				if(Slider.getText().equals(SliderValue.toString())) {
					Slider.click();
					break;
				}

			}
			Thread.sleep(5000);                    
			driver.findElement(By.xpath("//*[@for='Radio2q']")).click();
			Thread.sleep(5000);

			//Selection of addon


			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			waitForElements(By.xpath(secureaddon_xpath));
			clickElement(By.xpath(secureaddon_xpath));
		}

	}
	public static void Addon() throws Exception{
		String[][] TestCaseData=BaseClass.excel_Files("TestCases");
		/*	if(PolicyType.contains("CareSenior")) {
			String addonsdata=TestCaseData[1][19].toString().trim();
			String[] addons = addonsdata.split(",");
			for(String Addonname :addons ) {
				if(Addonname.equals("CareSenior")) {
					System.out.println("Type is "+PolicyType);
					System.out.println("Care details are :" +driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).getTagName());
					System.out.println("Care details are :" +driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).getAttribute("value"));
				}*/
		if(PolicyType.contains("CareSenior")) {
			String addonsdata=TestCaseData[1][19].toString().trim();
			String[] addons = addonsdata.split(",");
			for(String Addonname :addons ) {

				if(Addonname.equals("CareSenior")) {
					waitForElements(By.xpath("//*[@for='premiumRadio0q']"));

					driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).click();
					break;
				}else if(Addonname.equals("care with NCB")) {
					JavascriptExecutor jse1 = (JavascriptExecutor)driver;
					jse1.executeScript("window.scrollBy(0,-250)", "");
					System.out.println("Care details are :" +driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).getTagName());
					waitForElements(By.xpath("//*[@for='premiumRadio0q']"));
					System.out.println("Care details are :" +driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).getAttribute("value"));

					try {
						if(driver.findElement(By.xpath("//*[@for='premiumRadio1q']")).isDisplayed()){
							Thread.sleep(5000);
							driver.findElement(By.xpath("//*[@for='premiumRadio1q']")).click();

						}else{Thread.sleep(5000);
						driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).click();
						}
						break;
					}catch(Exception e) {
						driver.findElement(By.xpath("//*[@for='premiumRadio0q']")).click();
						break;
					}
				}else{
					System.out.println("addons are"+Addonname);
					Thread.sleep(5000);
					List <WebElement> addonst=driver.findElements(By.xpath("//span[@class='add_on_btn_icon_cont']"));
					int addoncount=0;
					for(WebElement addonsclick :addonst ) {
						addoncount=addoncount+1;
						System.out.println("AdOns Text is :"+addonsclick.getText() +"----"+addonsclick.getTagName()+"---"+addonsclick.getAttribute("value"));
						if(Addonname.equals("NC")&&(addoncount==1)) {
							addonsclick.click();
							break;
						}else if(Addonname.equals("SS")&&(addoncount==2)) {
							addonsclick.click();
							break;
						}else if(Addonname.equals("OPD")&&(addoncount==3)) {
							addonsclick.click();
							break;
						}

					}
				}
			}
		}

	}

	public static void dragdrop() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		// Selecting Plan Type

		String PlanType = TestCaseData[1][42].toString().trim();
		String PlanType2 = TestCaseData[2][42].toString().trim();

		System.out.println(PlanType);

		if (PlanType.contains("Enhance 2")) {
			clickElement(By.xpath("//label[@for='RadioJ1q']"));

			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,350)", "");
			WebElement deductableprogress = null;
			List<WebElement> deductSlidnumber = null;
			int Deductible;
			int suminsured;
			for (int i = 7; i <= 8; i++) {
				if (i == 7) {
					deductableprogress = driver.findElement(
							By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[" + i + "]/div[2]/div/div/div"));
				} else if (i == 8) {
					deductableprogress = driver
							.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[" + i + "]/div/div/div"));
				}
				deductSlidnumber = deductableprogress.findElements(By.xpath("//span[@class='ui-slider-number']"));
				System.out.println("deductSlidnumber : " + deductSlidnumber.size());
				String DeductSliderValue = null;
				for (WebElement Slider : deductSlidnumber) {
					if (PolicyType.contains("Enhance")) {
						if (i == 7) {
							Deductible = Integer.parseInt(TestCaseData[1][44].toString().trim());//
						} else {
							Deductible = Integer.parseInt(TestCaseData[1][46].toString().trim());
						}
						// System.out.println(Deductible);
						// driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[7]/div[2]/div/div/div"));
						Thread.sleep(5000);
						try {
							clickElement(By.xpath("//span[@class='ui-slider-number' and text()=" + Deductible + "]"));
							break;
						} catch (Exception e) {
							System.out.println("Slider value not matched");
							i = i - 1;
						}
					}
				}
			}

		} else if (PlanType.contains("Enhance 1")) {

			clickElement(By.xpath("//label[@for='RadioJ0q']"));
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,350)", "");
			WebElement deductableprogress = null;
			List<WebElement> deductSlidnumber = null;
			int Deductible;
			// int suminsured ;
			for (int i = 7; i <= 8; i++) {
				if (i == 7) {
					deductableprogress = driver.findElement(
							By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[" + i + "]/div[2]/div/div/div"));
				} else if (i == 8) {
					deductableprogress = driver
							.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[" + i + "]/div/div/div"));
				}
				deductSlidnumber = deductableprogress.findElements(By.xpath("//span[@class='ui-slider-number']"));
				System.out.println("deductSlidnumber : " + deductSlidnumber.size());
				String DeductSliderValue = null;
				System.out.println("Value is :" + DeductSliderValue);
				for (WebElement Slider : deductSlidnumber) {
					if (PolicyType.contains("Enhance")) {
						if (i == 7) {
							Deductible = Integer.parseInt(TestCaseData[1][43].toString().trim());
						} else {
							Deductible = Integer.parseInt(TestCaseData[1][45].toString().trim());
						}
						// System.out.println(Deductible);
						// driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[7]/div[2]/div/div/div"));
						Thread.sleep(5000);
						try {
							clickElement(By.xpath("//span[@class='ui-slider-number' and text()=" + Deductible + "]"));
							break;
						} catch (Exception e) {
							System.out.println("Slider value not matched");
							i = i - 1;
						}
					}
				}
			}

		}

		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@for='Radio2q']")).click();
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("window.scrollBy(0,-350)", "");

		// driver.findElement(By.xpath("buynow_xpath"));

	}

	public static void Checkpremium() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		Thread.sleep(5000);
		if(PolicyType.contains("CareSenior")) {
			WebElement premium_value = driver
					.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[1]/b/p[1]/span"));
			String value1 = premium_value.getText();
			System.out.println("Total premium value is:" + value1);
			clickElement(By.xpath(Button_Buynow_xpath));
			Thread.sleep(5000);
			Thread.sleep(2000);
			WebElement premium_value_secpage = driver
					.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[5]/p[1]"));
			String value2 = premium_value_secpage.getText();
			System.out.println("second page prmium value is :" + value2);
			Assert.assertEquals(value1, value2);
		}
		else if(PolicyType.contains("POS")) {
			WebElement premium_value = driver
					.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[1]/b/p[1]/span"));
			String value1 = premium_value.getText();
			System.out.println("Total premium value is:" + value1);
			clickElement(By.xpath(Button_Buynow_xpath));
			Thread.sleep(5000);
			Thread.sleep(2000);
			WebElement premium_value_secpage = driver
					.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[5]/p[1]"));
			String value2 = premium_value_secpage.getText();
			System.out.println("second page prmium value is :" + value2);
			Assert.assertEquals(value1, value2);
		}
		else if (PolicyType.contains("Enhance")) {
			WebElement premium_value = driver
					.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[2]/div/div[1]/b/p[1]/span"));
			String value1 = premium_value.getText();
			System.out.println("Total premium value is:" + value1);
			waitForElements(By.xpath(Enhance_buynow_xpath));
			clickElement(By.xpath(Enhance_buynow_xpath));
			Thread.sleep(2000);
			WebElement premium_value_secpage = driver
					.findElement(By.xpath("//*[@id=\"msform\"]/div[1]/div/div/div/div[4]/p[1]"));
			String value2 = premium_value_secpage.getText();
			System.out.println("second page prmium value is :" + value2);
			Assert.assertEquals(value1, value2);
		} else if (PolicyType.contains("Secure")) {
			WebElement premium_value = driver
					.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[1]/b/p[1]/span"));
			String value1 = premium_value.getText();
			System.out.println("Total premium value is:" + value1);
			waitForElements(By.xpath(buynow_xpath));
			clickElement(By.xpath(buynow_xpath));
			Thread.sleep(2000); // *[@id='msform']/div[1]/div/div/div/div[5]/p[1]
			WebElement premium_value_secpage = driver
					.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[5]/p[1]"));
			String value2 = premium_value_secpage.getText();
			System.out.println("second page prmium value is :" + value2);
			Assert.assertEquals(value1, value2);
		} else if (PolicyType.contains("NCB")) {
			WebElement premium_value = driver
					.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[3]/div/div[1]/b/p[1]/span"));
			String value1 = premium_value.getText();
			System.out.println("Total premium value is:" + value1);
			waitForElements(By.xpath(buynow_xpath));
			clickElement(By.xpath(buynow_xpath));
			Thread.sleep(2000); // *[@id='msform']/div[1]/div/div/div/div[5]/p[1]
			WebElement premium_value_secpage = driver
					.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[5]/p[1]"));
			String value2 = premium_value_secpage.getText();
			System.out.println("second page prmium value is :" + value2);
			Assert.assertEquals(value1, value2);
		} else if (PolicyType.contains("SuperSaver")) {
			WebElement fstpage_premium = driver
					.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[2]/div/div[1]/b/p[1]/span"));

			String first_premium = fstpage_premium.getText().toString().trim();
			System.out.println("Premium value of 1st page is : " + first_premium);
			clickElement(By.xpath(fst_buy_now_xpath));

			WebElement premiumpagetwo = driver
					.findElement(By.xpath("//*[@id=\"msform\"]/div[1]/div/div/div/div[4]/p[1]"));
			String second_premium = premiumpagetwo.getText().toString().trim();
			System.out.println("2nd page premium value is :" + second_premium);
			Assert.assertEquals(first_premium, second_premium);
			System.out.println("Successfully premium verified");
		}

		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-250)", "");

		String Title = TestCaseData[1][1].toString().trim();
		System.out.println("titel Name is:" + Title);
		clickElement(By.xpath(click_title_xpath));

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[1][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));

		Thread.sleep(3000);
		// proposer_dob

		/*
		 * DOB_Proposer=readExcel("Health Insurance").getRow(1).getCell(4).
		 * getStringCellValue(); System.out.println(DOB_Proposer);
		 * enterText(By.id(Dob_Proposer_id),String.valueOf(DOB_Proposer));
		 * logger.log(LogStatus.PASS, "Loan Amount for PA is " + " - " +
		 * DOB_Proposer);
		 */

		final String address1 = TestCaseData[1][7].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		enterText(By.xpath(addressline1_xpath), address1);
		enterText(By.xpath(addressline2_xpath), TestCaseData[1][8].toString().trim());
		enterText(By.xpath(pincode_xpath), TestCaseData[1][9]);
		// Height selection
		String Height = TestCaseData[1][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[1][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[1][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);

		String NomineeName = TestCaseData[1][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		// Nominee Relation
		String Nrelation = TestCaseData[1][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
		String pancard=TestCaseData[1][19].toString().trim();

		String pospancard="KJHYS8977E";
		System.out.println("pancard number is :"+pospancard); try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).
			sendKeys(pospancard); }catch(Exception e) {
				System.out.println("Pan card field not visibled"); }


		clickElement(By.xpath(submit_xpath));

	}

	public static void quote_summary() throws Exception {
		// List Age of members dropdown
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		WebElement Occumpationtype = (WebElement) driver.findElement(By.xpath("//*[@id='occupation0']"));
		Thread.sleep(10000);
		Occumpationtype.click();
		List<WebElement> List = driver.findElements(By.xpath("//*[@id='occupation0']/option"));
		int totalsize = List.size();
		System.out.println("Size is:" + totalsize);
		// WebElement
		// List=driver.findElement(By.xpath("//*[@id='occupation0']/option"));
		final String type = TestCaseData[1][31].toString().trim();
		// membernumber=membernumber-1;
		int occupationnumber = 1;
		for (WebElement occupationname : List) {
			System.out.println("occupationname.getText()" + occupationname.getText());
			if (occupationname.getText().equals(type)) {
				// Thread.sleep(10000);
				occupationname.click();
				// driver.findElement(By.xpath("//*[@id='occupation0']/option["+occupationnumber+"]")).click();
				Thread.sleep(3000);
				break;
			} else {
				occupationnumber = occupationnumber + 1;
			}
		}

		WebElement Occupationclassname = (WebElement) driver.findElement(By.xpath("//*[@name='occupationClass0']"));
		Thread.sleep(3000);
		Occupationclassname.click();
		Thread.sleep(5000);
		List<WebElement> occupationlist = driver.findElements(By.xpath("//*[@name='occupationClass0']/option"));
		int totalsizeoccupation = occupationlist.size();
		System.out.println("Size is:" + totalsizeoccupation);
		// WebElement
		// List=driver.findElement(By.xpath("//*[@id='occupation0']/option"));
		String occupationclasstype = TestCaseData[1][32].toString().trim();
		System.out.println("occupationclasstype  :  " + occupationclasstype);
		// membernumber=membernumber-1;
		int occupationclassnumber = 1;
		// Thread.sleep(10000);
		for (WebElement occupationclassname : occupationlist) {
			System.out.println("occupationname.getText()" + occupationclassname.getText());
			if (occupationclassname.getText().equalsIgnoreCase(occupationclasstype)) {
				Thread.sleep(2000);
				occupationclassname.click();
				// driver.findElement(By.xpath("//*[@id='occupationClass0']/option["+occupationnumber+"]")).click();
				Thread.sleep(10000);
				break;
			} else {
				occupationclassnumber = occupationclassnumber + 1;
			}
		}
		driver.findElement(By.xpath("//*[@id=\"msform\"]/div[2]/fieldset[2]/input[2]")).click();
	}

	public static void Questionaries() throws Exception {
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSet");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String preExistingdeases = TestCaseData[1][21].toString().trim();
		Thread.sleep(1000);
		System.out.println(
				"Do you have an existing personal Accident/Health Insurance policy with Religare or any other Insurer? :"
						+ preExistingdeases);

		if (preExistingdeases.contains("YES")) {
			waitForElements(By.xpath(secureYes_question_xpath));
			clickElement(By.xpath(secureYes_question_xpath));
			enterText(By.xpath(insurer_xpath), TestCaseData[1][33].toString().trim());
			enterText(By.xpath(policynumber_xpath), TestCaseData[1][34].toString().trim());
			enterText(By.xpath(plan_name_xpath), TestCaseData[1][35].toString().trim());
			enterText(By.xpath(sum_insured_xpath), TestCaseData[1][36].toString().trim());
		} else if (preExistingdeases.contains("NO")) {
			clickElement(By.xpath(secureNo_question_xpath));
		}
		String PublicFunction = TestCaseData[1][37].toString().trim();
		String hazardousactivity = TestCaseData[1][38].toString().trim();
		String hamperingvision = TestCaseData[1][39].toString().trim();
		String PersonalAccident = TestCaseData[1][40].toString().trim();
		String extremesports = TestCaseData[1][41].toString().trim();
		// System.out.println("extremesports :" + extremesports);
		System.out.println("Do you participate in Adventure/ extreme sports? :" + PublicFunction);
		// System.out.println("Do you have an existing personal Accident/Health
		// Insurance policy with Religare or any other Insurer? :"
		// +PublicFunction);
		System.out.println("Have you ever been entrusted with prominent public functions? :" + hazardousactivity);
		System.out.println(
				"working in aircrafts or sea-going vessels or adventure sports or armed forces? :" + hamperingvision);
		System.out.println(
				"any terminal illness or any illness or disease causing restriction to activities(Eg Epilepsy or Seizures)?:"
						+ PersonalAccident);
		// System.out.println("Has any company ever declined to issue/renew a
		// Personal Accident policy for any proposed? :" +extremesports);

		if (PublicFunction.contains("Yes")) {
			clickElement(By.xpath(question2_Yes_xpath));
		} else {
			clickElement(By.xpath(question2_No_xpath));
		}
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");
		if (hazardousactivity.contains("Yes")) {
			clickElement(By.xpath(question3_Yes_xpath));
		} else {
			clickElement(By.xpath(question3_No_xpath));
		}
		if (hamperingvision.contains("Yes")) {
			clickElement(By.xpath(question4_Yes_xpath));
		} else {
			clickElement(By.xpath(question4_No_xpath));
		}
		if (PersonalAccident.contains("Yes")) {
			clickElement(By.xpath(question5_Yes_xpath));
		} else {
			clickElement(By.xpath(question5_No_xpath));
		}
		if (extremesports.contains("Yes")) {
			clickElement(By.xpath(question6_Yes_xpath));
		} else {
			clickElement(By.xpath(question6_No_xpath));
		}
		clickElement(By.xpath(Agree_xpath));
		clickElement(By.xpath(proposer_xpath));

		clickElement(By.xpath(proceed_to_pay_secure_xpath));
	}

	public static void payment2() throws Exception {



		/*waitForElement(By.xpath(pay_online_xpath));
		clickElement(By.xpath(pay_online_xpath));
		 */
		driver.findElement(By.xpath("//button[@class='btn']")).click();
		waitForElement(By.xpath(pay_online_xpath));
		clickElement(By.xpath(pay_online_xpath));

		//clickElement(By.xpath(Enhance_pay_online_xpath));

		String[][] carddetails = BaseClass.excel_Files("carddetails");
		System.out.println("card number is: " + carddetails[1][0].toString().trim());

		WebElement text=driver.findElement(By.xpath("//*[@id='manageCardLink']"));
		System.out.println("Text is :"  +text.toString().trim());
		// if(text.equals("Manage this card")){
		if(text.equals(driver.findElement(By.xpath("//*[@id='manageCardLink']")))){  
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			driver.findElement(By.xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();

			String message = driver
					.findElement(By.xpath("//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p"))
					.getText();
			System.out.println("Message is :" + message);
			WebElement Policy=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]"));
			String policy_Number=Policy.getText();
			System.out.println("Policy number is  :"   +policy_Number);

		}else{

			enterText(By.xpath("//input[@name='ccard_number']"), carddetails[1][0].toString().trim());
			enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,250)", "");
			String expmonth = carddetails[1][3].toString().trim();
			String expyear = carddetails[1][4].toString().trim();
			/*
			 * WebElement
			 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
			 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
			 */

			BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
			BaseClass.selecttext("cexpiry_date_year", expyear.toString());
			driver.findElement(By.xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();

			String message = driver
					.findElement(By.xpath("//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p"))
					.getText();
			System.out.println("Message is :" + message);
			WebElement Policy=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]"));
			String policy_Number=Policy.getText();
			System.out.println("Policy number is  :"   +policy_Number);
		}
	}

	public static void Premiumpage() throws Exception {
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		int mcount;
		System.out.println("BaseClass.membres.length  :" +BaseClass.membres.length);
		for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
			mcount = Integer.parseInt(BaseClass.membres[i].toString());
			if (i == 0) {
				clickElement(By.xpath(title1_xpath));
				// Select Self Primary
				BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
			} else {

				// String firstName= "fname"+i+"_xpath";
				String Date = FamilyData[mcount][5].toString().trim();
				// Date=Date.replaceAll("-", "/");
				// Relation
				BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
				// title
				BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
				enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
				enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
				clickElement(By.name("rel_dob" + i));
				enterText(By.name("rel_dob" + i), String.valueOf(Date));

				BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
				BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
				enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

				/*
				 * WebElement List=driver.findElement(By.xpath(
				 * "//*[@id=\"occupation0\"]/option"));
				 * System.out.println("Total elements no is :"+List.getText().
				 * length());
				 */

			}

		}

		driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
		// input[@type='button' and @name='next']
	}

	public static void Insured_details() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembersPOSSuperSaver");
		int mcountindex = 0;
		int mcount;
		String Membersdetails = TestCaseData[n][16].toString().trim();
		
		mcountindex = mcountindex + 1;
	
		if (Membersdetails.contains(",")) {

			// data taking form test case sheet which is
			// 7,4,1,8,2,5
			BaseClass.membres = Membersdetails.split(",");
		} else {
			BaseClass.membres = Membersdetails.split(" ");
		}

		System.out.println("BaseClass.membres.length " + BaseClass.membres.length);
		member: 
			for (int i = 0; i <= BaseClass.membres.length-1; i++) {
				//for (int i = 0; i <= BaseClass.membres.length-2; i++) { for number of members=1

				mcount = Integer.parseInt(BaseClass.membres[i].toString());
				if (i == 0) {
					clickElement(By.xpath(title1_xpath));
					// Select Self Primary
					BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
				} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
					// Date=Date.replaceAll("-", "/");
					// Relation
					BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
					// title
					BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
					enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
					enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
					clickElement(By.name("rel_dob" + i));
					enterText(By.name("rel_dob" + i), String.valueOf(Date));

					BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
					BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
					enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
					//Thread.sleep(5000);
				}
			}
		clickElement(By.xpath("//*[@id='msform']/div[2]/fieldset[2]/input[2]"));
	}

	public static void QuestionSet() throws Exception {
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSet");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		JavascriptExecutor jset = (JavascriptExecutor) driver;
		String preExistingdeases = TestCaseData[1][21].toString().trim();
		Thread.sleep(1000);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);

		if (preExistingdeases.contains("YES")) {
			waitForElements(By.xpath(yes_button_xpath));
			clickElement(By.xpath(yes_button_xpath));
			// Thread.sleep(2000);
			String years = null;
			String Details = null;
			for (int qlist = 1; qlist <= 11; qlist++) { // take 13 for others
				Details = QuestionSetData[1][qlist + (qlist - 1)].toString().trim();
				years = QuestionSetData[1][qlist + qlist].toString().trim();

				if (Details.equals("")) {
					// break;
				} else {
					int detailsnumber = Integer.parseInt(Details);

					// Will click on check box and select the month & year u
					detailsnumber = detailsnumber + 1;
					System.out.println("Details and years are :" + Details + "----" + years);

					clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td[" + detailsnumber
							+ "]//input[@type='checkbox']"));
					Thread.sleep(1000);
					try {
						clickElement(By.xpath(
								"//*[@class='multyple_body']/tr[" + qlist + "]/td[" + detailsnumber + "]//label"));
						enterText(By.xpath(
								"//*[@class='multyple_body']/tr[" + qlist + "]/td[" + detailsnumber + "]//label"),
								years);
					} catch (Exception e) {
						clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td[" + detailsnumber
								+ "]//label[@class='monthYear']"));
						enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td[" + detailsnumber
								+ "]//label[@class='monthYear']"), years);
					}
				}
			}
		}
		jset.executeScript("window.scrollBy(0,150)", "");
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;

		jse1.executeScript("window.scrollBy(0,750)", "");
		System.out.println("Scrolling Done");
		String ChecksData = null;
		String[] ChckData = null;
		int datacheck = 0;
		String Question1 = TestCaseData[1][48].toString().trim();
		System.out.println("Question1 status :" + Question1);
		if (Question1.contains("Yes")) {
			//driver.findElement(By.xpath("//*[@id='headingSix']/h4/div[2]/div/label[1]")).click();
			driver.findElement(By.xpath("//*[@id='headingTwo']/h4/div[2]/div/label[1]")).click();
			driver.findElement(By.xpath("//*[@id='collapse2']/div/table/tbody/tr/td[1]/div/input")).click();
			//driver.findElement(By.xpath("//*[@id='collapse6']/div/table/tbody/tr/td[2]/div/input")).click();
			Thread.sleep(8000);
		} else {
			driver.findElement(By.xpath("//*[@id='headingSix']/h4/div[2]/div/label[2]")).click();
		}

		for (int morechecks = 1; morechecks <= 3; morechecks++) {
			int mch = morechecks + 1;

			ChecksData = TestCaseData[1][21 + morechecks].toString().trim();
			if (ChecksData.equalsIgnoreCase("NO")) {
				System.out.println("Quatation set to NO");
				// label[@for='question_"+mch+"_no']
				driver.findElement(By.xpath("//label[@for='question_" + mch + "_no'] ")).click(); // click
				// on yes/no dependig  upon Excel
			} else if (ChecksData.equals("YES") || ChecksData.equalsIgnoreCase("Yes") || ChecksData.equalsIgnoreCase("YES") ) {
				System.out.println("Quatation set to only YES");
				// label[@for='question_"+mch+"_yes']
				driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();

				if (ChecksData.contains(",")) {
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;

						driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']")).click();
						Thread.sleep(5000);

					}
				}
			} else {
				driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
				if (ChecksData.contains(",")) {
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;

						driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']")).click();
						Thread.sleep(5000);

					}
				} else {
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']")).click();
					// driver.findElement(By.xpath("//*[@id='collapse2']/div/table/tbody/tr/td["+mch+"]//input[@type='checkbox']")).click();
					Thread.sleep(5000);
				}
			}
			Thread.sleep(4000);
		}

		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//*[@id='termsCheckbox3']")).click();
		// driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
		driver.findElement(By.xpath("//*[@id=\"alertCheck\"]")).click();

		// driver.findElement(By.xpath("//*[@id='siQues']/label[2]")).click();
		jse1.executeScript("window.scrollBy(0,250)", "");

		clickElement(By.xpath(proceed_to_pay_xpath));

		/*
		 * try{ (new WebDriverWait(driver,
		 * 100)).until(ExpectedConditions.presenceOfElementLocated(By.
		 * xpath("//p[@class='premium_amount ng-binding']"))); } catch(Exception
		 * e){ String Error_Message = driver.findElement(By.xpath(
		 * "//*[@id='myModalMail_bel']/div/div/div[2]/h5[2]")).getText();
		 * System.out.println(Error_Message);
		 * 
		 * }
		 */
	}

	public static void Payment() {

		waitForElement(By.xpath(Enhance_pay_online_xpath));
		clickElement(By.xpath(Enhance_pay_online_xpath));

		try {
			clickElement(By.xpath("//*[@id='drop_image_1']"));
			Thread.sleep(5000);

			driver.findElement(By.xpath("//*[@id=\"drop_list_1\"]/div/ul/li[3]/a")).click();
			String[][] carddetails = BaseClass.excel_Files("carddetails");

			System.out.println("card number is: " + carddetails[1][0].toString().trim());
			enterText(By.xpath("//input[@name=\"ccard_number\"]"), carddetails[1][0].toString().trim());
			enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,250)", "");
			String expmonth = carddetails[1][3].toString().trim();
			String expyear = carddetails[1][4].toString().trim();
			/*
			 * WebElement elm=driver.findElements(By.xpath(
			 * "//select[@id='cexpiry_date_month']")).click(); Select sel=new
			 * Select(elm); sel.selectByValue(month);
			 */

			BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
			BaseClass.selecttext("cexpiry_date_year", expyear.toString());
			driver.findElement(By.xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();

			String message = driver
					.findElement(By.xpath("//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p"))
					.getText();
			System.out.println("Message is :" + message);
			if (message.equals("Your payment transaction is successful !")) {

				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File("D:\\My_Workspace\\NewFaveoUIAutomation\\screenshot.jpeg"));
			} else {
				System.out.println("Successful with error message");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Invalid data ");
		}

	}

	public static void selectdropdownforHNI() throws Exception {

		String[][] loginData = BaseClass.excel_Files("Sheet1");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSet");
		String[][] carddetails = BaseClass.excel_Files("carddetails");
		String[][] carewithHNI = BaseClass.excel_Files("care_for_HNI");
		WebElement policy_name = driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[1]/div/p/span"));
		String name = policy_name.getText().toString().trim();
		System.out.println("Name is:" + name);
		int membernumber = 0;
		if (name.equals("Care For HNI")) {

			System.out.println("if loop started");
			List<WebElement> dropdown = driver.findElements(By.xpath(
					"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("2")) {
					membernumber = Integer.parseInt(TestCaseData[1][15].toString().trim());
					membernumber = membernumber - 1;
					driver.findElement(By.xpath(
							"//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + membernumber + "]"))
					.click();
					Thread.sleep(10000);
					break;
				}
			}
			// again call the dropdown
			dropdown = driver.findElements(By.xpath(
					"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

			// Thread.sleep(3000);
			int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
			int count = 1;
			int mcount;
			int mcountindex = 0;
			int covertype;
			System.out.println("dropdown size is " + dropdown.size());
			outer: for (WebElement DropDownName : dropdown) {
				System.out.println("DropDownName is  " + DropDownName.getText());
				if (DropDownName.getText().equals(TestCaseData[1][15].toString().trim())) {
					System.out.println("DropDownName is  " + DropDownName.getText());
				}

				else if (DropDownName.getText().contains("Individual")) {
					System.out.println("DropDownName is  " + DropDownName.getText());
					if (TestCaseData[1][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
					.click();
					Thread.sleep(10000);
				} else if (name.equals("Care For HNI") && !DropDownName.getText().contains("18 to 24 Years")) {
					int Test = Integer.parseInt(DropDownName.getText());

					// System.out.println("Children Dropdown text is :"+Test);
					DropDownName.click();
					System.out.println("Children Dropdown text is :" + Test);
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li[1]")).click();
					driver.findElement(
							By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a")).click();
					Thread.sleep(5000);

				} else {

					// reading members from carewith TestData sheet memberlist
					String Membersdetails = TestCaseData[1][29];
					if (Membersdetails.contains(",")) {

						// data taking form test case sheet which is 7,4,1,8,2,5
						BaseClass.membres = Membersdetails.split(",");
					} else {
						BaseClass.membres = Membersdetails.split("");
					}

					member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {
							// System.out.println("Mdeatils is : "+membres);

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							DropDownName.click();
							// List Age of members dropdown

							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='dropdown year_drop_slect master open']/ul/li/a"));
							int totalsize = List.size();
							System.out.println("Size is:" + totalsize);
							System.out.println("Family Member details :" + FamilyData[mcount][0].toString().trim());
							for (WebElement age : List) {
								// System.out.println("Age details is :
								// "+age.getText());
								if (age.getText().equals(FamilyData[mcount][0].toString().trim())) {
									age.click();
								}
							}

							if (count == membersSize) {
								break outer;
							} else {
								count = count + 1;
								break member;
							}

						}

				}

			}
		}

		Thread.sleep(5000);
		// DragandDrop(By.xpath(slider_xpth));
		WebElement progress = driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[7]"));
		List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
		for (WebElement Slider : Slidnumber) {

			// System.out.println("Slider get Text is : "+Slider.getText());
			if (Slider.getText().equals("50")) {
				Slider.click();
				break;
			}

		}
		Thread.sleep(20000);
		driver.findElement(By.xpath("//*[@for='Radio2q']")).click();
		Thread.sleep(15000);

		WebElement fstpage_premium = driver
				.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[2]/div/div[1]/b/p[1]/span"));

		String first_premium = fstpage_premium.getText().toString().trim();
		System.out.println("Premium value of 1st page is : " + first_premium);
		clickElement(By.xpath(fst_buy_now_xpath));

		WebElement premiumpagetwo = driver.findElement(By.xpath("//*[@id=\"msform\"]/div[1]/div/div/div/div[4]/p[1]"));
		String second_premium = premiumpagetwo.getText().toString().trim();
		System.out.println("2nd page premium value is :" + second_premium);
		Assert.assertEquals(first_premium, second_premium);
		System.out.println("Successfully premium verified");

		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-250)", "");
		// praposal page
		String Title = TestCaseData[1][1].toString().trim();
		System.out.println("titel Name is:" + Title);
		clickElement(By.xpath(click_title_xpath));

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[1][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));

		Thread.sleep(3000);
		// proposer_dob

		String address1 = TestCaseData[1][7].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		enterText(By.xpath(addressline1_xpath), address1);
		enterText(By.xpath(addressline2_xpath), TestCaseData[1][8].toString().trim());
		enterText(By.xpath(pincode_xpath), TestCaseData[1][9]);
		// Height selection
		String Height = TestCaseData[1][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[1][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[1][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);

		String NomineeName = TestCaseData[1][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		// Nominee Relation
		String Nrelation = TestCaseData[1][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());

		// String pancard=TestCaseData[1][19].toString().trim();
		String pancard = "KJHYS8977E";
		System.out.println("pancard number is :" + pancard);
		try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).sendKeys(pancard);
		} catch (Exception e) {
			System.out.println("Pan card field not visibled");
		}

		waitForElements(By.xpath(submit_xpath));

		clickElement(By.xpath(submit_xpath));
		int mcount;

		for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
			mcount = Integer.parseInt(BaseClass.membres[i].toString());
			if (i == 0) {

				// Select Self Primary
				BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
				clickElement(By.xpath(title1_xpath));
			} else {

				// String firstName= "fname"+i+"_xpath";
				String Date = FamilyData[mcount][5].toString().trim();
				// Date=Date.replaceAll("-", "/");
				// Relation
				BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
				// title
				BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
				enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
				enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
				clickElement(By.name("rel_dob" + i));
				enterText(By.name("rel_dob" + i), String.valueOf(Date));

				BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
				BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
				enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

			}

		}
		clickElement(By.xpath(Next_xpath));
		/*
		 * String preExistingdeases=TestCaseData[1][21].toString().trim();
		 * Thread.sleep(1000); System.out.
		 * println("Does any person(s) to be insured has any pre-exsiting diseases? :"
		 * +preExistingdeases); JavascriptExecutor jset =
		 * (JavascriptExecutor)driver;
		 * jset.executeScript("window.scrollBy(0,-250)", "");
		 * if(preExistingdeases.contains("YES")) {
		 * waitForElements(By.xpath(HNI_NExt_xpath));
		 * clickElement(By.xpath(HNI_NExt_xpath)); //Thread.sleep(2000); String
		 * years=null; String Details=null; for(int qlist=1;qlist<=13;qlist++) {
		 * Details =QuestionSetData[1][qlist+(qlist-1)].toString().trim();
		 * years=QuestionSetData[1][qlist+qlist].toString().trim();
		 * if(Details.equals("")) { //break; }else { int detailsnumber =
		 * Integer.parseInt(Details);
		 * 
		 * //Will click on check box and select the month & year u
		 * detailsnumber=detailsnumber+1;
		 * System.out.println("Details and years are :"+Details+"----"+years);
		 * clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["
		 * +detailsnumber+"]//input[@type='checkbox']")); Thread.sleep(1000);
		 * try {
		 * clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["
		 * +detailsnumber+"]//label"));
		 * enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+
		 * detailsnumber+"]//label"),years); }catch(Exception e) {
		 * clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["
		 * +detailsnumber+"]//label[@class='monthYear']"));
		 * enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+
		 * detailsnumber+"]//label[@class='monthYear']"),years); } } } }
		 * jse.executeScript("window.scrollBy(0,250)", "");
		 * System.out.println("Scrolling Done"); String ChecksData =null;
		 * String[] ChckData =null; int datacheck=0; for(int
		 * morechecks=1;morechecks<=3;morechecks++) { int mch =morechecks+1;
		 * ChecksData = TestCaseData[1][21+morechecks].toString().trim();
		 * if(ChecksData.equalsIgnoreCase("NO")) {
		 * System.out.println("Quatation set to NO");
		 * //label[@for='question_"+mch+"_no']
		 * driver.findElement(By.xpath("//label[@for='question_"+mch+"_no'] ")).
		 * click(); //click on yes/no dependig upon Excel }else
		 * if(ChecksData.equals("YES")||ChecksData.equalsIgnoreCase("Yes")) {
		 * System.out.println("Quatation set to only YES");
		 * //label[@for='question_"+mch+"_yes']
		 * driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).
		 * click(); }else{
		 * driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).
		 * click(); if(ChecksData.contains(",")) {
		 * ChckData=ChecksData.split(","); for(String Chdata:ChckData) {
		 * datacheck = Integer.parseInt(Chdata); datacheck=datacheck-1; //
		 * driver.findElement(By.xpath(
		 * "//*[@id='collapse2']/div/table/tbody/tr/td["+mch+
		 * "]//input[@type='checkbox']")).click();
		 * driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck
		 * +"']")).click(); Thread.sleep(5000); //input[@name='question2_4']
		 * //*[@id="collapse2"]/div/table/tbody/tr/td["+mch+"]
		 * //input[@name='question"+mch+"] } }else { datacheck =
		 * Integer.parseInt(ChecksData); datacheck=datacheck-1;
		 * driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck
		 * +"']")).click(); // driver.findElement(By.xpath(
		 * "//*[@id='collapse2']/div/table/tbody/tr/td["+mch+
		 * "]//input[@type='checkbox']")).click(); Thread.sleep(5000); } }
		 * 
		 * }
		 * driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click()
		 * ;
		 * driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click()
		 * ; jse.executeScript("window.scrollBy(0,250)", "");
		 * clickElement(By.xpath(proceed_to_pay_xpath));
		 */// button to open calendar

		WebElement selectDate = driver.findElement(By.xpath("//span[@aria-controls='datetimepicker_dateview']"));

		/*
		 * waitForElement(By.xpath(pay_pre_online_xpath));
		 * clickElement(By.xpath(pay_pre_online_xpath));
		 * 
		 * try { clickElement(By.xpath("//*[@id='drop_image_1']"));
		 * Thread.sleep(5000);
		 * 
		 * driver.findElement(By.xpath("//*[@id=\"drop_list_1\"]/div/ul/li[3]/a"
		 * )).click();
		 * 
		 * 
		 * System.out.println("card number is: "+carddetails[1][0].toString().
		 * trim()); enterText(By.xpath("//input[@name=\"ccard_number\"]"),
		 * carddetails[1][0].toString().trim());
		 * enterText(By.xpath(card_name_xpath),
		 * carddetails[1][1].toString().trim()); enterText(By.xpath(cvv_xpath),
		 * carddetails[1][2].toString().trim());
		 * jse.executeScript("window.scrollBy(0,250)", ""); String
		 * expmonth=carddetails[1][3].toString().trim(); String
		 * expyear=carddetails[1][4].toString().trim(); WebElement
		 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
		 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
		 * 
		 * BaseClass.selecttext("cexpiry_date_month",expmonth.toString());
		 * BaseClass.selecttext("cexpiry_date_year",expyear.toString());
		 * driver.findElement(By.
		 * xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();
		 * 
		 * String message=driver.findElement(By.xpath(
		 * "//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p"
		 * )).getText(); System.out.println("Message is :" +message);
		 * if(message.equals("Your payment transaction is successful !")) {
		 * 
		 * File scrFile =
		 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 * FileUtils.copyFile(scrFile, new
		 * File("D:\\My_Workspace\\NewFaveoUIAutomation\\screenshot.png")); }
		 * else { System.out.println("Successful with error message"); } } catch
		 * (Exception e) {
		 * 
		 * System.out.println("Invalid data "); }
		 */

	}

	public static void Lunchpraposal() throws Exception {

		clickElement(By.xpath(dashboard_xpath));
		clickElement(By.xpath(praposal_xapth));
		System.out.println("Title is  :" + driver.getTitle());
		// clickElement(By.xpath("//*[@id='datetimepicker1']"));
	}

	public static void Last10() throws Exception {
		System.out.println("===================Selection started==============");
		//// *[@class="col-md-6 Check_buttons
		//// p_not_left"]//button[starts-with(@class,'btn2')]
		Thread.sleep(5000);
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("window.scrollBy(0,-3200)", "");
		List<WebElement> listshowtype = driver.findElements(
				By.xpath("//*[@class=\"col-md-6 Check_buttons p_not_left\"]//button[starts-with(@class,'btn2')]"));
		for (WebElement Showtype : listshowtype) {
			System.out.println("Show Element name is : " + Showtype.getText());
			if (!Showtype.getText().equalsIgnoreCase("Send PDF Link") && !Showtype.getText().equals("")
					&& !Showtype.getText().equals(null)) {
				// if(Showtype.getText().equals("Show All")){
				Showtype.click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@class='pdf_btn pdf_btn_marg']")).click();

				Thread.sleep(6000);
				// JavascriptExecutor jse1 = (JavascriptExecutor)driver;

				String PagenationAvail = "no";
				// pagination
				int pagesize;
				int lstpage = 0;
				int lastNavbutton;

				List<WebElement> pagination = driver
						.findElements(By.xpath("//ul[@class='pagination ng-table-pagination']/li"));

				pagesize = pagination.size();

				if (pagesize >= 11) {
					lstpage = 11 - 1;
					lastNavbutton = 11;
				} else {
					lstpage = pagesize - 1;
					lastNavbutton = pagesize;
				}
				try {
					Thread.sleep(6000);
					String lastpage = driver
							.findElement(
									By.xpath("//ul[@class='pagination ng-table-pagination']/li[" + lstpage + "]/a"))
							.getText();
					lstpage = Integer.parseInt(lastpage);
					System.out.println("last page no is :" + lstpage);
					System.out.println("Total number of pages are  :" + pagesize);
					jse1.executeScript("window.scrollBy(0,3200)", "");
					PagenationAvail = "yes";
				} catch (Exception e) {
					pagesize = 3;
					lstpage = 1;
					System.out.println("Pagination option not available at this time");
				}

				for (int i = 1; i <= lstpage; i++) {
					// System.out.println("values are
					// "+pagination.get(i).getText());
					List<WebElement> Tabledate_row = driver
							.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tr"));
					List<WebElement> Tabledate_col = driver
							.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tr[1]//td"));
					int rows_Size = Tabledate_row.size();
					int cols_size = Tabledate_col.size();
					System.out.println("----------------------------------------------------- page no :" + i);
					System.out.println("Total number of rows is :" + rows_Size);
					System.out.println("Total number of cols is  :" + cols_size);
					System.out.println("-----------------------------------------------------------------");
					int page = i + 1;
					// jse1.executeScript("window.scrollBy(0,2000)", "");
					if (PagenationAvail.equalsIgnoreCase("yes")) {
						WebElement pagenation = driver.findElement(
								By.xpath("//ul[@class='pagination ng-table-pagination']/li[" + lastNavbutton + "]/a"));
						// pagenation.click();
						Actions actions = new Actions(driver);
						actions.moveToElement(pagenation);
						actions.click();
						actions.build().perform();
					} else {
						System.out.println("Pagination option not available at this time for clicking ");
					}
					Thread.sleep(6000);

				}

			}
		}
		// }
	}

	public static void fromtodate() throws Exception {

		String[][] praposal = BaseClass.excel_Files("Praposal");
		String fromdate = praposal[1][0].toString().trim();
		String Todate = praposal[1][1].toString().trim();
		System.out.println("date is:" + fromdate);
		Thread.sleep(10000);
		enterText(By.id("from_date"), String.valueOf(fromdate));
		System.out.println("To date is:" + Todate);
		Thread.sleep(10000);
		enterText(By.id("to_date"), String.valueOf(Todate));
		NextTab(By.id("to_date"));
		// driver.findElement(By.xpath("//*[@class='pdf_btn
		// pdf_btn_marg']")).click();
		Thread.sleep(5000);

		List<WebElement> pagination = driver.findElements(By.xpath("//ul[@class='pagination ng-table-pagination']/li"));
		int total_size = pagination.size();
		System.out.println("Total number of pages are :" + total_size);
		for (int i = 1; i <= total_size - 2; i++) {
			// System.out.println("values are "+pagination.get(i).getText());
			List<WebElement> Tabledate_row = driver
					.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tr"));
			List<WebElement> Tabledate_col = driver
					.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tr[1]//td"));
			int rows_Size = Tabledate_row.size();
			int cols_size = Tabledate_col.size();
			System.out.println("Total number of rows is :" + rows_Size);
			System.out.println("Total number of cols is  :" + cols_size);
			int page = i + 1;
			// jse1.executeScript("window.scrollBy(0,2000)", "");
			try {
				WebElement pagenation = driver
						.findElement(By.xpath("//ul[@class='pagination ng-table-pagination']/li[" + page + "]/a"));
				// pagenation.click();
				Actions actions = new Actions(driver);
				actions.moveToElement(pagenation);
				actions.click();
				actions.build().perform();
			} catch (Exception e) {
				System.out.println("Pagination option not available at this time for clicking ");
			}
			Thread.sleep(6000);

		}

	}

	public static void Search() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 40);
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("window.scrollBy(0,-1000)", "");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//ul[contains(@class,'search_input_drop')]//descendant::ul[contains(@class,'dropdown-menu')]//preceding-sibling::a[@ng-bind-html='dropDownValue']")));
		Thread.sleep(5000);

		driver.findElement(By
				.xpath("//ul[contains(@class,'search_input_drop')]//descendant::ul[contains(@class,'dropdown-menu')]//preceding-sibling::a[@ng-bind-html='dropDownValue']"))
		.click();
		List<WebElement> dropdownvalue = driver.findElements(By.xpath(
				"//ul[contains(@class,'search_input_drop')]//descendant::ul[contains(@class,'dropdown-menu')]//li//a"));
		wait.until(ExpectedConditions.visibilityOfAllElements(dropdownvalue));
		int cellno;
		for (int i = 0; i < dropdownvalue.size(); i++) {
			cellno = i + 2;
			String[][] Bynames = BaseClass.excel_Files("Praposal");
			String ByName = Bynames[0][cellno].toString();
			System.out.println("Headers are : " + ByName);
			System.out.println(dropdownvalue.get(i).getText());
			if (dropdownvalue.get(i).getText().equalsIgnoreCase(ByName)) {
				String Values = dropdownvalue.get(i).getText();
				System.out.println("Values are :" + Values);
				dropdownvalue.get(i).click();
				String Name = Bynames[1][cellno].toString().trim();
				System.out.println("Name is  :" + Name);
				// driver.findElement(By.xpath("//*[@class='dropdown
				// month_drop_slect drop_w_arr master
				// open']/ul/li["+Values+"]")).click();
				driver.findElement(
						By.xpath("//ul[contains(@class,'search_input_drop')]//following-sibling::div[1]/input[1]"))
				.sendKeys(Name);
				driver.findElement(By.xpath("//ul[contains(@class,'search_input_drop')]//following-sibling::div[1]//i"))
				.click();
				Thread.sleep(5000);
				List<WebElement> Tabledate_row = driver
						.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tbody//tr"));
				int rows_Size = Tabledate_row.size();
				System.out.println("Total number of rows is :" + rows_Size);
				driver.findElement(By
						.xpath("//ul[contains(@class,'search_input_drop')]//descendant::ul[contains(@class,'dropdown-menu')]//preceding-sibling::a[@ng-bind-html='dropDownValue']"))
				.click();
			}

		}

	}

	public static void linklist() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,1000)", "");
		List<WebElement> links = driver.findElements(By.xpath("//div[@class='footer_network_container']/div/li"));
		int total_links = links.size();
		System.out.println("Total number of footer links in home page is :" + total_links);

		for (WebElement text : links) {
			System.out.println("text is :" + text.getText());
		}
	}
	public static void possuminsured() throws Exception{
		WebElement progress= driver.findElement(By.xpath("//div[@class='wrapper slider2']/div"));

		List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
		String SliderValue=null;
		for(WebElement Slider:Slidnumber) {

			//System.out.println("Slider get Text  is : "+Slider.getText());
			System.out.println("Policy type is : "+PolicyType);
			if(PolicyType.contains("POS")) {
				SliderValue="5";
			}
			else if(PolicyType.contains("NCB")) {
				SliderValue="75";
			}else if(PolicyType.contains("SuperSaver")) {
				SliderValue="4";
			}else if(PolicyType.contains("HNI")) {
				SliderValue="50";
			}
			if(Slider.getText().equals(SliderValue.toString())) {
				Slider.click();
				break;
			}
		}
		String[][] TestCaseData=BaseClass.excel_Files("TestCases");
		String testcovertype=TestCaseData[1][17].toString().trim();
		String ctype=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a")).getText();
		System.out.println("Cover type is :"  +ctype);
		if(ctype.equals(testcovertype)){
			clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[8]/div/div/div/div[3]/label[1]"));
		}else {
			clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[7]/div/div/div/div[3]/label[1]"));
		}
	}

	public static void personaldetails() throws Exception  {
		//selecting Nationality and passort number based on  excel
		String[][] StudentExploreTestcase=BaseClass.excel_Files("StudentExploreTestcase");
		String nationality=StudentExploreTestcase[1][12].toString().trim();
		System.out.println("Nationality is  : "  +nationality);

		clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
		clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text(),"+"'"+nationality+"'"+")]"));
		System.out.println("Entered Nationality is : " +nationality);

		String Passport = StudentExploreTestcase[1][13];
		//clearTextfield(By.xpath(PassportNumber_xpath));
		ExplicitWait1(By.xpath(PassportNumber_xpath));
		enterText(By.xpath(PassportNumber_xpath), Passport);
		Thread.sleep(2000);
		System.out.println("Entered Passport Number : "+Passport);
		String Title = StudentExploreTestcase[1][14].toString().trim();
		clickElement(By.xpath("//select[@name='ValidTitle']"));
		clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text(),"+"'"+Title+"'"+")]"));
		Thread.sleep(3000);
		System.out.println("Entered Title is : " +Title);

		BaseClass.selecttext("ValidTitle", Title.toString());

		String FirstName = StudentExploreTestcase[1][15];
		clearTextfield(By.xpath(FirstName_xpath));
		enterText(By.xpath(FirstName_xpath), FirstName);
		Thread.sleep(3000);
		System.out.println("Entered First Name is : " +FirstName);
		String LastName = StudentExploreTestcase[1][16];
		clearTextfield(By.xpath(LastName_xpath));
		enterText(By.xpath(LastName_xpath), LastName);
		System.out.println("Entered Last Name is : "+LastName);

		// Entering DOB from Excel into dob field

		String DOB = StudentExploreTestcase[1][17].toString().trim();
		System.out.println("date is:" + DOB);

		try{
			clickElement(By.xpath(DOBCalender_xpath));
			clearTextfield(By.xpath(DOBCalender_xpath));
			enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
			driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

			System.out.println("Entered Proposer DOB is : "+ DOB);
			Thread.sleep(3000);
		}
		catch(Exception e)
		{
			System.out.println("Unable to Enter Date of Birth Date.");
		}



		final String address1 = StudentExploreTestcase[1][18].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		clearTextfield(By.xpath(addressline1_xpath));
		enterText(By.xpath(addressline1_xpath), address1);
		clearTextfield(By.xpath(addressline2_xpath));
		enterText(By.xpath(addressline2_xpath), StudentExploreTestcase[1][19].toString().trim());
		clearTextfield(By.xpath(pincode_xpath));
		enterText(By.xpath(pincode_xpath), StudentExploreTestcase[1][20]);

		String NomineeName = StudentExploreTestcase[1][21].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);

		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE);
		Thread.sleep(2000);
		String nominiRelation=StudentExploreTestcase[1][22];
		clickElement(By.xpath(nomineRelation_xpath));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//select[@name='nomineeRelation']/option[contains(text(),"+"'"+nominiRelation+"'"+")]")).click();
		// driver.findElement(By.xpath("//*[@id=\"tr_details_full_cont\"]/div[1]/div[2]/div[3]/div[4]/div/select/option"));
		System.out.println("Selected nominee relation is : "+nominiRelation);

		String PurposeofVisit = StudentExploreTestcase[1][23];
		clickElement(By.xpath(PurposeofVisit_xpath));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"+"'"+PurposeofVisit+"'"+")]")).click();
		System.out.println("Selected Purpose of Visit is : "+PurposeofVisit);
		String TripstartDate=StudentExploreTestcase[1][24];

		// Nominee Relation
		String Pancardnumber = StudentExploreTestcase[1][25].toString().trim();
		try{
			clickElement(By.xpath(PanCard_xpath));
			enterText(By.xpath(PanCard_xpath), Pancardnumber);
		}catch(Exception e)
		{
			System.out.println("Pan card Field is not Present on UI.");
		}

		String spilitter[]=TripstartDate.split(","); 
		String eday = spilitter[0];
		String emonth = spilitter[1];
		String eYear = spilitter[2];
		String emonthYear = (emonth +" "+eYear);
		System.out.println("Current month and year is :  "+emonthYear);
		clickElement(By.xpath(TripStartDate_xpath));
		WebElement Header=null;
		for(int year=0;year<=12;year++) {
			Header=driver.findElement(By.xpath("//*[@class='ui-datepicker-title']"));
			if(Header.getText().contains(emonth)) {
				break;
			}else {
				System.out.println("Month not matched :"+Header.getText()+"--"+emonth);
				driver.findElement(By.xpath("//*[@class='ui-icon ui-icon-circle-triangle-e']")).click();
			}
		}
		System.out.println("Header Text is :"  +Header.getText());

		List<WebElement> mnt=Header.findElements(By.xpath("//*[@id='ui-datepicker-div']/table/tbody/tr/td/a"));

		for(WebElement calend:mnt) {
			if(calend.getText().equals(eday)) {
				calend.click();
				break;
			}
		}
	}

	public static void Group_Explore_Insured_details() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] Group_Travel = BaseClass.excel_Files("Group_Travel");

		int mcount;
		for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
			{
				mcount = Integer.parseInt(BaseClass.membres[i].toString());

				String Relation = Group_Travel[mcount][1].toString().trim();
				String InsuredNationality = Group_Travel[mcount][2].toString().trim();
				String InsuredPassport = Group_Travel[mcount][3].toString().trim();
				String InsuredTitle = Group_Travel[mcount][4].toString().trim();
				System.out.println("Value of Mcount : "+mcount);
				System.out.println("Insured Title is : "+InsuredTitle);
				String  InsuredFirstName = Group_Travel[mcount][5].toString().trim();
				String  InsuredLastName = Group_Travel[mcount][6].toString().trim();
				String InsuredDOB = Group_Travel[mcount][7].toString().trim();


				BaseClass.selecttext("ValidRelation" + i, Relation);
				System.out.println("Selected Relation is : "+Relation);


				BaseClass.selecttext("rel_nationality" + i, InsuredNationality);
				System.out.println("Selected Nationality is : "+InsuredNationality);


				enterText(By.name("rel_passport" + i), InsuredPassport);
				System.out.println("Entered Passport is : "+InsuredPassport);
				//String GInsuredTitle = FamilyData[mcount][4].toString().trim();
				/*try{

					System.out.println("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+InsuredTitle+"'"+"]");
					clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']"));
					Thread.sleep(2000);
					clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+InsuredTitle+"'"+"]"));
					System.out.println("Selcted Title is : "+InsuredTitle);

					}catch(Exception e)
					{
						System.out.println("Unable to Select Title.");
					}*/
				clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']"));
				Thread.sleep(2000);
				//clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+Group_Travel[mcount][4].toString().trim()+"'"+"]"));
				String GInsuredTitle = FamilyData[mcount][4].toString().trim();
				int persontitle;
				if(GInsuredTitle.contains("Mr")){
					persontitle=2;
				}else{
					persontitle=3;
				}

				clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option["+persontitle+"]"));
				Thread.sleep(5000);
				if(Relation.contains("Self-primary"))
				{
					enterText(By.name("RelFName" + i), InsuredFirstName);
					enterText(By.name("RelLName" + i), InsuredLastName);
					System.out.println("Entered Name is : "+InsuredFirstName +" " + InsuredLastName);
				}
				else{
					clearTextfield(By.name("RelFName" + i));
					enterText(By.name("RelFName" + i), InsuredFirstName);
					clearTextfield(By.name("RelLName" + i));
					enterText(By.name("RelLName" + i), InsuredLastName);
					System.out.println("Entered Name is : "+InsuredFirstName +" " + InsuredLastName);
				}

				if(Relation.contains("Self-primary"))
				{
					try{
						clickElement(By.xpath("//input[@name='rel_dob"+i+"']"));
						enterText(By.xpath("//input[@name='rel_dob"+i+"']"), String.valueOf(InsuredDOB));
						System.out.println("Entered DateofBirth is : "+InsuredDOB);
					}
					catch(Exception e)
					{
						System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

					}
				}
				else
				{
					try{
						clickElement(By.xpath("//input[@name='rel_dob"+i+"']"));
						clearTextfield(By.xpath("//input[@name='rel_dob"+i+"']"));
						Thread.sleep(2000);
						enterText(By.xpath("//input[@name='rel_dob"+i+"']"), String.valueOf(InsuredDOB));
						System.out.println("Entered DateofBirth is : "+InsuredDOB);
					}
					catch(Exception e)
					{
						System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

					}
				}

			}
		}
	}

	public static void Group_insured_details() throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] Group_Travel = BaseClass.excel_Files("Group_Travel");
		int mcount;
		int mcountindex = 0;
		String Membersdetails = TestCaseData[1][55].toString().trim();
		if (Membersdetails.contains(",")) {

			// data taking form test case sheet which is
			// 7,4,1,8,2,5
			BaseClass.membres = Membersdetails.split(",");
		} else {
			BaseClass.membres = Membersdetails.split(" ");
		}
		for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
			mcount = Integer.parseInt(BaseClass.membres[i].toString());
			if (i == 0) {

				driver.findElement(By.xpath("//select[@name='ValidRelation0']")).click();
				// Select Self Primary
				BaseClass.selecttext("ValidRelation0", Group_Travel[mcount][1].toString().trim());
			} else {

				// Relation
				BaseClass.selecttext("ValidRelation" + i, Group_Travel[mcount][1].toString().trim());
				// title
				BaseClass.selecttext("rel_nationality" + i, Group_Travel[mcount][2].toString().trim());
				enterText(By.name("rel_passport" + i), Group_Travel[mcount][3].toString().trim());
				//BaseClass.selecttext("rel_passport" + i, Group_Travel[mcount][3].toString().trim());
				Thread.sleep(5000);
				/*BaseClass.selecttext("ValidRelTitle" + i, Group_Travel[mcount][4].toString().trim());

				Thread.sleep(5000);*/

				clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']"));
				Thread.sleep(2000);
				//clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+Group_Travel[mcount][4].toString().trim()+"'"+"]"));
				String InsuredTitle = FamilyData[mcount][4].toString().trim();
				int persontitle;
				if(InsuredTitle.contains("Mr")){
					persontitle=2;
				}else{
					persontitle=3;
				}

				clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option["+persontitle+"]"));
				Thread.sleep(5000);
				clearTextfield(By.name("RelFName" + i));
				driver.findElement(By.name("RelFName" + i)).sendKeys(Group_Travel[mcount][5].toString().trim());
				//enterText(By.name("RelFName" + i), Group_Travel[mcount][5].toString().trim());
				clearTextfield(By.xpath("RelLName" + i));
				enterText(By.xpath("RelLName" + i), Group_Travel[mcount][6].toString().trim());
				//clickElement(By.name("rel_dob" + i));
				String Date = Group_Travel[mcount][7].toString().trim();
				/*	clearTextfield(By.xpath("rel_dob" + i));
				enterText(By.name("rel_dob" + i), String.valueOf(Date));*/
				clickElement(By.xpath("//input[@name='rel_dob"+i+"']"));
				clearTextfield(By.xpath("//input[@name='rel_dob"+i+"']"));
				Thread.sleep(2000);
				enterText(By.xpath("//input[@name='rel_dob"+i+"']"), String.valueOf(Date));
				System.out.println("Entered DateofBirth is : "+Date);



			}
		}
	}
	public static void Group_question() throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] Group_Travel = BaseClass.excel_Files("Group_Travel");
		String[][] Group_Travel_Question = BaseClass.excel_Files("Group_Questions");
		String[][] Group_wuestion=BaseClass.excel_Files("Group_Questions");
		//String preExistingdeases = TestCaseData[1][60].toString().trim();
		int Travellers = Integer.parseInt(TestCaseData[1][54].toString().trim());
		Thread.sleep(2000);


		WebElement pedtext=driver.findElement(By.xpath("//*[@class='col-md-2']/p/span[2]"));
		String PEDText=pedtext.getText();
		System.out.println("Ped Text is :  "    +PEDText);
		if(PEDText.equalsIgnoreCase("YES")){
			try{
				String preExistingdeases = TestCaseData[1][60].toString().trim();
				System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);

				if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes"))
				{

					/* waitForElements(By.xpath(PEDYesButton_xpath));
					 clickElement(By.xpath(PEDYesButton_xpath));
					 */ 
					Thread.sleep(5000);
					driver.findElement(By.xpath("//input[@ng-checked='ped.question_1 == true']")).click();
					Thread.sleep(3000);
					String[] ChckData = null;
					int datacheck1 = 0;
					String HealthQuestion = Group_wuestion[1][1].toString().trim();
					System.out.println("Member Having PED : "+HealthQuestion);
					for(int i=0;i<Travellers;i++)
					{
						clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
						clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='YES']"));
					}	

					if (HealthQuestion.contains(",")) 
					{




						ChckData = HealthQuestion.split(",");
						for (String Chdata : ChckData) 
						{
							datacheck1 = Integer.parseInt(Chdata);
							datacheck1 = datacheck1 - 1;

							clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
							clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));

						}
					}
					else if (HealthQuestion.contains(""))
					{
						datacheck1 = Integer.parseInt(HealthQuestion);
						datacheck1 = datacheck1-1;

						clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
						clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
					}

					String[] ChckData1 = null;
					int detailsnumber = 0;

					for(int qlist=2;qlist<=6;qlist++) 
					{

						String Details =Group_wuestion[1][qlist].toString().trim();
						int q1list = qlist+1;

						if(Details.equals("")) 
						{
							System.out.println("Please Enter Some value in Excel to Select PED.");
						}
						else if(Details.contains(",")) 
						{

							ChckData1 = Details.split(",");

							for (String Chdata1 : ChckData1) 
							{

								detailsnumber = Integer.parseInt(Chdata1);
								clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));

							}
						}

						else if (Details.contains(""))
						{
							detailsnumber = Integer.parseInt(Details);
							try{
								Thread.sleep(3000);
								driver.findElement(By.xpath("//html//div["+qlist+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']")).click();
							}catch(Exception e)
							{
								System.out.println("Not able to click on Sub Questions Values of Question 1.");	 
							}

						}

					}


					String[] AlimnetsQuestionData = null;
					for(int qlist=7;qlist<=7;qlist++) 
					{
						String Details =Group_wuestion[1][qlist].toString().trim();
						String AlimnetsQuestion =Group_wuestion[1][7].toString().trim();
						String OtherQuestionDetails =Group_wuestion[1][8].toString().trim();

						if(AlimnetsQuestion.equals("")) 
						{
							System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 

						}else if(AlimnetsQuestion.contains(","))
						{
							AlimnetsQuestionData = AlimnetsQuestion.split(",");
							for(String AlimntQuesnData : AlimnetsQuestionData)
							{
								detailsnumber = Integer.parseInt(AlimntQuesnData);
								int Num = detailsnumber-1;
								int q1list=qlist+1;
								clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
								enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
							}
						}
						else if(AlimnetsQuestion.contains(""))
						{
							detailsnumber = Integer.parseInt(AlimnetsQuestion);
							int Num = detailsnumber-1;
							int q1list=qlist+1;
							clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
							enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
						}
					}									
				}
				else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) 
				{
					clickElement(By.xpath(PEDNoButton_xpath));
				}
			}
			catch(Exception e)
			{
				System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
				//logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
			}


			//2nd Question injury during the last 48 months?
			String QuestionNumber2 = TestCaseData[1][61].toString().trim();
			String QuestionNumber2Details = TestCaseData[1][62].toString().trim();
			String[] CheckData = null;
			int DataCheck=0;
			try{
				if(QuestionNumber2.contains("NO") || QuestionNumber2.contains("No") || QuestionNumber2.contains("no"))
				{
					System.out.println("Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
					clickElement(By.xpath(PED_2_NoButton_xpath));
				}else
				{
					System.out.println("Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");				
					System.out.println("Member Who have injured During last 48 months : "+QuestionNumber2);
					clickElement(By.xpath(PED_2_YesButton_xpath));

					for(int i=0;i<Travellers;i++)
					{
						Select SelectBox = new Select(driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")));
						SelectBox.selectByValue("YES");
						/*driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
						driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'YES')]")).click();*/
						if(QuestionNumber2Details.contains(",")){

							String Reason[]=QuestionNumber2Details.split(",");
							WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T001Desc_"+i+"']"));
							reasontextbox.clear();
							reasontextbox.sendKeys(Reason[i]);
						}
						else{
							String Reason[]=QuestionNumber2Details.split("");
							WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T001Desc_"+i+"']"));
							reasontextbox.clear();
							reasontextbox.sendKeys(Reason[i]);
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("Unable to Seclect Question 2.");
			}

			//3rd Question injury during the last 48 months?
			String QuestionNumber3 = TestCaseData[1][63].toString().trim();
			String QuestionNumber3Details = TestCaseData[1][64].toString().trim();
			try{
				if(QuestionNumber3.contains("NO") || QuestionNumber3.contains("No") || QuestionNumber3.contains("no"))
				{
					System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
					clickElement(By.xpath(PED_3_NoButton_xpath));
				}else
				{
					System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
					System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
					clickElement(By.xpath(PED_3_YesButton_xpath));

					for(int i=0;i<Travellers;i++)
					{

						Select SelectBox = new Select(driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']")));
						SelectBox.selectByValue("YES");
						/*driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
						driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'YES')]")).click();*/
						if(QuestionNumber3Details.contains(",")){

							String thirdquestionReason[]=QuestionNumber3Details.split(",");
							WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T002Desc_"+i+"']"));
							reasontextbox.clear();
							reasontextbox.sendKeys(thirdquestionReason[i]);
						}
						else{
							String thirdquestionReason[]=QuestionNumber2Details.split("");
							WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T002Desc_"+i+"']"));
							reasontextbox.clear();
							reasontextbox.sendKeys(thirdquestionReason[i]);
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("Unable to Seclect Question 3.");
			}

			clickElement(By.xpath("//input[@id='termsCheckbox1']"));
			clickElement(By.xpath("//input[@id='tripStart']"));
			//	clickElement(By.xpath("//input[@id='termsCheckbox2']"));

			clickElement(By.xpath("//button[@class='btn btn-success']"));
		}

		else if(PEDText.contains("No")){
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,1550)", "");
			Thread.sleep(5000);
			WebElement textmessage=driver.findElement(By.xpath("//*[@id='accordion']/div[2]"));
			String Errortext=textmessage.getText();
			System.out.println("Error text after selecting Ped  No : "  +Errortext);
			clickElement(By.xpath("//input[@id='termsCheckbox1']"));
			clickElement(By.xpath("//input[@id='tripStart']"));
			//	clickElement(By.xpath("//input[@id='termsCheckbox2']"));

			clickElement(By.xpath("//button[@class='btn btn-success']"));	
		}
	}
	public static void Seniordropdownall() throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		int covertype;

		//Thread.sleep(10000);

		List<WebElement>seniorDrop=driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int dpsize=seniorDrop.size();
		System.out.println("Total dropdowns are   :   "+dpsize);
		for(WebElement dropdown:seniorDrop) 
		{

			if(dropdown.getText().contains("Individual")) {
				System.out.println("The drop down name is  :"  +dropdown.getText());
				if(TestCaseData[1][17].toString().trim().equals("Floater")) //coverType from excel==================
				{
					covertype=2;
				}else {
					covertype=1;
				}
				dropdown.click();
				driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/ul/li["+covertype+"]")).click();
			}
			dropdown.click();
			if(dropdown.getText().equals("2")) {
				//Number of members=========================
				int members=Integer.parseInt(TestCaseData[1][15].toString().trim());
				driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li["+members+"]")).click();
				break;
			}
		}
		//List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		//again call the drop down

		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());//Number of members
		int count = 1;
		int mcount;
		int mcountindex = 0;
		seniorDrop=driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));



		//Need to select the Age of the Eldest Member
		List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
		for (WebElement DropDowns : dropdowns) {
			if(DropDowns.getText().equals("1")) {
				System.out.println("Not required to select chilidren and covertype");
				String Membersdetails = TestCaseData[1][16];  //60,61
				if (Membersdetails.contains(",")) {				
					BaseClass.membres = Membersdetails.split(",");
				} else {
					System.out.println("Hello");
				}
				System.out.println("BaseClass.membres.length  :" +BaseClass.membres.length);

				outer:

					member:
						for (int i = 0; i <= BaseClass.membres.length; i++) {
							// one by one will take from 83 line

							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;
							DropDowns.click();
							// List Age of members dropdown				
							List<WebElement> List = driver.findElements(By.xpath("//*[@class='dropdown year_drop_slect master open']/ul/li/a"));
							List<WebElement> list=driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
							/*String text=driver.findElement(By.xpath("//*[@class='dropdown year_drop_slect master open']/ul/li/a]")).getText();
				System.out.println("Text is :" +text);*/
							System.out.println("List datas are :" +list);
							System.out.println("List of data are : "  +List);
							for (WebElement ListData : List) {	
								System.out.println("ListData.getText():  "+ListData.getText());
								if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Eldest Member is :" + ListData.getText());
									Thread.sleep(2000);
									ListData.click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break member;
									}

								}//end of list data if

							}
						}
			}
			else {

				System.out.println("Required to seect the above all covertype and member");
				//	dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				seniorDrop=driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

				outer:

					for (WebElement DropDownName : seniorDrop) {

						if (membersSize == 1) {

							String Membersdetails = TestCaseData[1][16];
							if (Membersdetails.contains("")) 
							{

								BaseClass.membres = Membersdetails.split("");

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();

										// List Age of members dropdown
										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) 
										{

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Selcted Age Of Member :" + ListData.getText());

												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													break member;
												}

											}

										}
									}
							}

						} 
						else if (DropDownName.getText().contains("Individual")) {
							System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

							if (TestCaseData[1][17].toString().trim().equals("Individual")) //covertype from excel=========
							{
								covertype = 1;
							} else {
								covertype = 2;
							}
							DropDownName.click();
							driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ covertype + "]")).click();
							// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
							Thread.sleep(10000);
							if (covertype == 2) 
							{
								List<WebElement> dropdownst = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
								for (WebElement DropDowns1 : dropdownst) 
								{
									if (DropDowns1.getText().contains("Floater")) 
									{
										System.out.println("Cover Type in Excel is :  " + DropDowns1.getText());
									} //Member details from excel==========================
									else if (DropDowns1.getText().equals(TestCaseData[1][15].toString().trim()))
									{
										System.out.println("DropDownName is  " + DropDowns1.getText());
									} else if (DropDowns1.getText().equals("2")) {
										System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns1.getText());
									} else if (DropDowns1.getText().equals("61 - 65 years")) {
										// reading members from test cases sheet
										// memberlist
										int Children = Integer.parseInt(TestCaseData[1][26].toString().trim());
										clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
										clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
										System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
										String Membersdetails = TestCaseData[1][16];
										if (Membersdetails.contains(",")) {


											BaseClass.membres = Membersdetails.split(",");
										} else {
											System.out.println("Hello");
										}

										member:
											for (int i = 0; i <= BaseClass.membres.length; i++) {

												// one by one will take from 83 line
												mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
												mcountindex = mcountindex + 1;

												DropDowns1.click();
												// List Age of members dropdown

												List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

												for (WebElement ListData : List) {


													if (ListData.getText()
															.contains(FamilyData[mcount][0].toString().trim())) {
														System.out.println("Age of Eldest Member is :" + ListData.getText());
														Thread.sleep(2000);
														ListData.click();

														if (count == membersSize) {
															break outer;
														} else {
															count = count + 1;
															// break member;
															break outer;
														}

													}

												}
											}


									}
								}
							} 
						}

					}

			}

		}

	}
	public static void selectdrop() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");

		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

		List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

		dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());

		int count = 1;

		int mcount;

		int mcountindex = 0;

		int covertype;



		try {

			outer:



				for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {

						String Membersdetails = TestCaseData[1][16];
						if (Membersdetails.contains("")) 
						{

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
									// one by one will take from 83 line

									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());

									mcountindex = mcountindex + 1;



									driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));
									for (WebElement ListData : List)

									{
										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();



											if (count == membersSize) {

												break outer;

											} else {

												count = count + 1;

												break member;

											}



										}



									}

								}

						}



					} else if (DropDownName.getText().contains("Individual")) {

						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());



						if (TestCaseData[1][17].toString().trim().equals("Individual")) {

							covertype = 1;

						} else {

							covertype = 2;

						}

						DropDownName.click();

						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]")).click();



						if (covertype == 2) {

							List<WebElement> dropdowns = driver

									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

							for (WebElement DropDowns : dropdowns) {

								if (DropDowns.getText().contains("Floater")) {

									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());

								} else if (DropDowns.getText().equals(TestCaseData[1][15].toString().trim())) {

									System.out.println("DropDownName is  " + DropDowns.getText());

								} else if (DropDowns.getText().equals("2")) {

									System.out.println(

											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());

								} else if (DropDowns.getText().equals("61 - 65 years")) {

									int Children = Integer.parseInt(TestCaseData[1][26].toString().trim());

									clickElement(

											By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));

									clickElement(By

											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"

                                                                        + "'" + Children + "'" + ")]"));

									String Membersdetails = TestCaseData[1][16];

									if (Membersdetails.contains(",")) {



										BaseClass.membres = Membersdetails.split(",");

									} else {

										System.out.println("Hello");

									}



									member:

										for (int i = 0; i <= BaseClass.membres.length; i++) {



											// one by one will take from 83 line

											mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());

											mcountindex = mcountindex + 1;



											DropDowns.click();

											// List Age of members dropdown



											List<WebElement> List = driver.findElements(

													By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));



											for (WebElement ListData : List) {



												if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

													System.out.println("Age of Eldest Member is :" + ListData.getText());
													ListData.click();
													if (count == membersSize) {

														break outer;

													} else {

														count = count + 1;

														// break member;

														break outer;

													}



												}



											}

										}



								}

							}

						}

					} else {



						List<WebElement> dropdowns = driver

								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

						for (WebElement DropDowns : dropdowns) {

							if (DropDowns.getText().contains("Floater")) {

								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());

							} else if (DropDowns.getText().equals("61 - 65 years")) {



								String Membersdetails = TestCaseData[1][16];

								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");

								} else {

									System.out.println("Hello");

								}



								member:

									// total number of members

									for (int i = 0; i <= BaseClass.membres.length; i++) {



										// one by one will take from 83 line

										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());

										mcountindex = mcountindex + 1;



										DropDowns.click();

										// List Age of members dropdown

										List<WebElement> List = driver.findElements(

												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));



										for (WebElement ListData : List) {



											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

												System.out.println("Age of Member is :" + ListData.getText());



												ListData.click();



												if (count == membersSize) {

													break outer;

												} else {

													count = count + 1;

													break member;

												}



											}



										}

									}



							}

						}

					}

				}

		} catch (Exception e) {

			System.out.println("Unable to Select Total Members.");

			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");

		}
	}

	public static void dpdown() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		int covertype;
		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
		if(membersSize==1) {
			int count = 1;
			int mcount;
			int mcountindex = 0;
			List<WebElement>seniorDrop=driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			int dpsize=seniorDrop.size();
			System.out.println("Total dropdowns are   :   "+dpsize);
			//String membersize=TestCaseData[1][15].toString().trim();
			//	System.out.println("Member size is :" +membersize);

			driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[5]/div/ui-dropdown/div/div/a")).click();;
			if(membersSize==1) {

				driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li["+membersSize+"]")).click();	
			}
			String Membersdetails = TestCaseData[1][16];
			if (Membersdetails.contains(",")) {


				BaseClass.membres = Membersdetails.split(",");
			} else {
				System.out.println("Hello");
			}
			outer:
				member:
					for (int i = 0; i <= BaseClass.membres.length; i++) {

						// one by one will take from 83 line
						mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
						mcountindex = mcountindex + 1;
						Thread.sleep(5000);
						WebElement elm=	driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[1]/div[5]/div/div/div/div/a"));
						elm.click();		
						// List Age of members dropdown

						List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

						for (WebElement ListData : List) {


							if (ListData.getText()
									.contains(FamilyData[mcount][0].toString().trim())) {
								System.out.println("Age of Eldest Member is :" + ListData.getText());
								Thread.sleep(2000);
								ListData.click();

								if (count == membersSize) {
									break outer;
								} else {
									count = count + 1;
									// break member;
									break outer;
								}

							}

						}
					}	

		}else if(membersSize==2) {
			int count = 1;
			int mcount;
			int mcountindex = 0;
			List<WebElement>dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			int dpsize=dropdown.size();
			System.out.println("Total dropdowns are   :   "+dpsize);	
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("2")) {
					driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li["+ TestCaseData[1][15].toString().trim() + "]")).click();
					System.out.println("Total Number of Member Selected : " + TestCaseData[1][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
			dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[1][16];
						if (Membersdetails.contains("")) 
						{

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();

									// List Age of members dropdown
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) 
									{

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} /*else if (DropDownName.getText().contains("Individual")) {
	System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());*/

					if (TestCaseData[1][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ covertype + "]")).click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) 
					{
						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[1][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("61 - 65 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[1][31].toString().trim());
								clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
								clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
								System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
								String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {


									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {


											if (ListData.getText()
													.contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

							}
						}
					}

					//} 
					else{

						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("61 - 65 years")) {

								String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else 
								{
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}

										}
									}

							}
						}
					}

				}
		}	

	}


	public static void seniorsuminsured() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String suminsured_value=TestCaseData[1][56].toString().trim();

		if (TestCaseData[1][17].toString().trim().equals("Individual")) {
			WebElement progress= driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[7]/div/div/div"));
			List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
			String SliderValue=null;
			for(WebElement Slider:Slidnumber) {

				//System.out.println("Slider get Text  is : "+Slider.getText());
				System.out.println("Policy type is : "+PolicyType);

				SliderValue=suminsured_value;

				if(Slider.getText().equals(SliderValue.toString())) {
					Slider.click();
					break;
				}
			}
			Thread.sleep(5000);                    
			driver.findElement(By.xpath("//*[@for='Radio2q']")).click();
			Thread.sleep(5000);
		}
		else {                                                //*[@id="getquote"]/form/div[1]/div[8]/div/div/div
			WebElement progress= driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[8]/div/div/div"));
			List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
			String SliderValue=null;
			for(WebElement Slider:Slidnumber) {

				//System.out.println("Slider get Text  is : "+Slider.getText());
				System.out.println("Policy type is : "+PolicyType);

				SliderValue=suminsured_value;

				if(Slider.getText().equals(SliderValue.toString())) {
					Slider.click();
					break;
				}
			}
			Thread.sleep(5000);                    
			driver.findElement(By.xpath("//*[@for='Radio2q']")).click();
			Thread.sleep(5000);
		}

	}
	public static void senior() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSet");
		Thread.sleep(3000);
		BaseClass.scrollup();
		JavascriptExecutor jset = (JavascriptExecutor) driver;
		String preExistingdeases = TestCaseData[1][21].toString().trim();
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		try{
			if(preExistingdeases.contains("YES")) {

				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				//Thread.sleep(2000);
				String years=null;
				String Details=null;		
				for(int qlist=1;qlist<=13;qlist++) {
					Details =QuestionSetData[1][qlist+(qlist-1)].toString().trim();
					years=QuestionSetData[1][qlist+qlist].toString().trim();
					if(Details.equals("")) 
					{
						//break;
					}else 
					{
						int detailsnumber = Integer.parseInt(Details);

						//Will click on check box and select the month & year u
						detailsnumber=detailsnumber+1;
						System.out.println("Details and years are :"+Details+"----"+years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						}catch(Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						}
					}
				}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
			}
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}


		JavascriptExecutor jse1 = (JavascriptExecutor) driver;

		jse1.executeScript("window.scrollBy(0,750)", "");
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[1][21 + morechecks].toString().trim();


			if (ChecksData.contains("NO")) 
			{
				System.out.println("Quatation set to NO");

				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else 
			{

				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
								By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
					/*{
				datacheck = Integer.parseInt(ChecksData);
				datacheck = datacheck - 1;
				driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				//driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
			}*/
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}

			}
			driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
			driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
			driver.findElement(By.xpath("//input[@id='alertCheck']")).click();
			jse1.executeScript("window.scrollBy(0,750)", "");
			Thread.sleep(5000);


		}

	}
	public static void StudentTravel(String TypeTest) throws Exception {
		System.out.println("++++++++++++++Student Expolre starts here+++++++++++++++");
		PolicyType = TypeTest;
		//PolicyType2 = TestType;
		//System.out.println("rowCount is "+rowCount);
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] TestCaseData = BaseClass.excel_Files("TestCases");
		String[][] StudentExploreTestcase = BaseClass.excel_Files("StudentExploreTestcase");
		testName = TypeTest.toString();
		if (TypeTest.contains("StudentExplore")) {
			Thread.sleep(2000);
			clickElement(By.xpath(travelinsurance_xpath));
			Thread.sleep(4000);
			clickElement(By.xpath(StudentExplorer_xpath));
		}
		Thread.sleep(2000);
		String email=StudentExploreTestcase[n][1].toString().trim();
		String mobilenumber=StudentExploreTestcase[n][2].toString().trim();
		System.out.println("Email is :" + email);
		Thread.sleep(4000);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		int size = mobilenumber.length();
		System.out.println("mobile number is: " + mobilenumber);
		String format = "^[789]\\d{9}$";

		Thread.sleep(4000);

		if (mobilenumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mobilenumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}	

	}
	public static void studentdp() throws Exception {
		String[][] StudentExploreTestcase = BaseClass.excel_Files("StudentExplore_Quotation");
		String GeographicalScope=StudentExploreTestcase[n][4].toString().trim();	
		System.out.println("Scope is : "+GeographicalScope);
		String PolicyTenure=StudentExploreTestcase[n][5].toString().trim();
		System.out.println("Policy Tenure is  : "+ PolicyTenure);
		String planType=StudentExploreTestcase[n][6].toString().trim();
		System.out.println(" Plan type is :  "+ planType);	
		String ped=StudentExploreTestcase[n][7].toString().trim();
		System.out.println(" Plan type is :  "+ ped);	

		String suminsured=StudentExploreTestcase[n][8].toString().trim();
		System.out.println("Age is  :" +suminsured);
		List<WebElement> ddowns=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
		int dropsize=ddowns.size();
		System.out.println("Total dropdowns are :"  +dropsize);
		int x=0;
		for(WebElement dropdownvalue:ddowns){	
			dropdownvalue.click();
			Thread.sleep(3000);
			String Ageexvalue=StudentExploreTestcase[1][3+x].toString().trim();
			//System.out.println("Age is  :" +Ageexvalue);
			List<WebElement> ddlist=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div//ul/li"));
			for(int j=0;j<=ddlist.size()-1;j++) {
				if(ddlist.get(j).getText().contains(Ageexvalue)) {
					System.out.println("Click value is : "+ddlist.get(j).getText());
					//Thread.sleep(5000);
					ddlist.get(j).click();
					Thread.sleep(5000);
					break;
				}
			}
			x=x+1;


		}
		//WebElement progress= driver.findElement(By.xpath("//*[@class='wrapper slider2']/div"));
		WebElement progress= driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[2]/div/div/div"));

		List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
		int total_size=Slidnumber.size();
		System.out.println("Slider numbers are :  " +total_size );

		String SliderValue=null;
		for(WebElement Slider:Slidnumber) {

			SliderValue=suminsured;

			Thread.sleep(7000);
			if(Slider.getText().equals(SliderValue)) {
				Thread.sleep(5000);
			
				Slider.click();
				break;
			}
		}

	}
	public static void TravelInsuredDetails() throws Exception {
		String[][] StudentExploreInsuredDetails = BaseClass.excel_Files("StudentExploreInsuredDetails");
		String Relation=StudentExploreInsuredDetails[n][0].toString().trim();

		//String Relation2=StudentExplore_InsuredDetails[1][0].toString().trim();

		String passport=StudentExploreInsuredDetails[n][1].toString().trim();

		String title=StudentExploreInsuredDetails[n][2].toString().trim();

		String Fname=StudentExploreInsuredDetails[n][3].toString().trim();

		String Lname=StudentExploreInsuredDetails[n][4].toString().trim();

		String Dob=StudentExploreInsuredDetails[n][5].toString().trim();
		System.out.println("Relation is : "+Relation  +passport  +title  +Fname   +Lname   +Dob);
		List<WebElement>Rel=driver.findElements(By.xpath("//select[@name='ValidRelation0']/option"));
		int size=Rel.size();
		System.out.println("List size is : "+size);
		for(WebElement Relations:Rel) {
			if(Relations.getText().contains("Self-primary")&&Relation.contains("Self-primary")) {
				Relations.click();
				System.out.println("Selected relation is :"  +Relation);
				Actions action = new Actions(driver);
				action.sendKeys(Keys.ESCAPE);
				break;
			}else if ((!Relations.getText().contains("Self-primary")) && Relations.getText().contains(Relation)) {
				Relations.click();
				driver.findElement(By.xpath("//*[@name='rel_passport0']")).sendKeys(passport);
				clickElement(By.xpath("//*[@id='ValidRelTitle0']"));
				Thread.sleep(3000);

				BaseClass.selecttext("ValidRelTitle0", title.toString());
				driver.findElement(By.xpath("//*[@id='RelFName0']")).clear();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='RelFName0']")).sendKeys(Fname);
				driver.findElement(By.xpath("//*[@id='RelLName0']")).clear();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='RelLName0']")).sendKeys(Lname);
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@name='rel_dob0']")).clear();
				driver.findElement(By.xpath("//*[@name='rel_dob0']")).click();
				Thread.sleep(3000);
				enterText(By.name("rel_dob0"), String.valueOf(Dob));

				Thread.sleep(3000);
				break;
			}

		}

	}
	public static void EducationalDetails() throws Exception {

		String[][] StudentEducationDetails = BaseClass.excel_Files("StudentEducationDetails");
		//String InstitutionName=EducationDetails[1][0].toString().trim();
		String CourseName=StudentEducationDetails[n][1].toString().trim();
		String InstituteAddress=StudentEducationDetails[n][2].toString().trim();
		String Country=StudentEducationDetails[n][3].toString().trim();
		System.out.println("Excel values are :"  +CourseName +"," +InstituteAddress +"," +Country);


		List<WebElement> Textbox=driver.findElements(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[6]/div[2]/div//input"));
		int totaltextbox=Textbox.size();
		System.out.println("Text box size is :"+totaltextbox );
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;

		jse1.executeScript("window.scrollBy(0,150)", "");

		int a=0;
		for(WebElement textdata : Textbox ) {
			String InstitutionName=StudentEducationDetails[n][a].toString().trim();
			System.out.println("Selected text data is : "+InstitutionName);
			textdata.sendKeys(InstitutionName);
			a++;
		}
	}
	public static void sponserDetails() throws Exception {

		String[][] StudentEducationDetails = BaseClass.excel_Files("StudentEducationDetails");
		String SponserName=StudentEducationDetails[n][4].toString().trim();
		String SponserDOB=StudentEducationDetails[n][5].toString().trim();
		String SponserRelation=StudentEducationDetails[n][6].toString().trim();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,200)");
		driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[6]/div[4]/div/div[1]/p[2]/input")).sendKeys(SponserName);
		driver.findElement(By.xpath("//*[@name='sponsor_dob']")).click();
		Thread.sleep(3000);
		enterText(By.name("sponsor_dob"), String.valueOf(SponserDOB));
		Thread.sleep(3000);
		/*List<WebElement> sponserrel=driver.findElements(By.xpath("//*[@name='sponsorRelation']"));
		int relationlist=sponserrel.size();*/
		clickElement(By.xpath("//*[@name='sponsorRelation']"));
		//  Thread.sleep(5000);
		driver.findElement(By.xpath("//select[@name='sponsorRelation']/option[contains(text(),"+"'"+SponserRelation+"'"+")]")).click();
		// driver.findElement(By.xpath("//*[@id=\"tr_details_full_cont\"]/div[1]/div[2]/div[3]/div[4]/div/select/option"));
		System.out.println("Selected nominee relation is : "+SponserRelation);
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE);

	}
	public static void studentExplore_Questionaries() throws Exception {

		String[][] StudentExploreTestcase= BaseClass.excel_Files("StudentExploreTestcase");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembers");
		String[][] Group_Travel = BaseClass.excel_Files("Group_Travel");
		String[][] Group_Travel_Question = BaseClass.excel_Files("Group_Questions");
		String[][] StudentExploreQuestions=BaseClass.excel_Files("StudentExploreQuestions");


		WebElement pedtext=driver.findElement(By.xpath("/html/body/div[2]/div[31]/div[1]/div[1]/div/div/div/div[2]/div[4]/p/span[2]"));
		System.out.println("PED text is :  "   +pedtext.getText());
		if(pedtext.equals("No")) {

			System.out.println("Not required to select any questions ");


		}else if(pedtext.getText().equals("Yes")){
			String preExistingdeases = StudentExploreTestcase[n][26].toString().trim();
			System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[@ng-checked='ped.question_1 == true']")).click();
			int i=0;
			clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
			clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='YES']"));

			List<WebElement> checkbox_list=driver.findElements(By.xpath("//input[@type='checkbox' and contains(@id,'qs_')]"));
			int total=checkbox_list.size();

			for(WebElement checkboxclick:checkbox_list) {
				checkboxclick.click();

			}
			String reason =StudentExploreQuestions[n][8].toString();
			driver.findElement(By.xpath("//*[@id='otherDiseasesDescription_0']")).sendKeys(reason);

			//Question two selection
			String QuestionNumber3 = StudentExploreQuestions[n][8].toString().trim();
			String QuestionNumber3Details = StudentExploreQuestions[n][9].toString().trim();
			System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
			System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
			driver.findElement(By.xpath("//input[@ng-checked='ped.question_2 == true']")).click();

			//*[@id="qs_T001_0"]/option[2]
			driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
			driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'Yes')]")).click();

			WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T001Desc_"+i+"']"));
			reasontextbox.clear();
			//String reason =StudentExploreQuestion[1][8].toString();
			reasontextbox.sendKeys(reason);

		}
	}

	public static void StudentExplore_Payment() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//input[@id='tripStart']")).click();

		//  js.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.name("optCovers")).click();
		driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
		waitForElement(By.xpath(pay_online_xpath));
		clickElement(By.xpath(pay_online_xpath));

		//clickElement(By.xpath(Enhance_pay_online_xpath));

		String[][] carddetails = BaseClass.excel_Files("carddetails");
		System.out.println("card number is: " + carddetails[n][0].toString().trim());

		WebElement text=driver.findElement(By.xpath("//*[@id='manageCardLink']"));
		System.out.println("Text is :"  +text.toString().trim());
		// if(text.equals("Manage this card")){
		/*if(text.equals(driver.findElement(By.xpath("//*[@id='manageCardLink']")))){  
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			driver.findElement(By.xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();

			String message = driver
					.findElement(By.xpath("//*[@class='ng-scope']/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p"))
					.getText();
			System.out.println("Message is :" + message);
			String policy_Number;
			try {
				WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/p"));
							policy_Number = Policy.getText();
			} catch (Exception e) {

				WebElement Policy=driver.findElement(By.xpath("//p[@class='your_payment_sucess_p ng-binding']"));
				policy_Number = Policy.getText();

			}

			System.out.println("Policy number is  :"   +policy_Number);

		}else{*/

		Thread.sleep(3000);
		enterText(By.xpath("//input[@name='ccard_number']"), carddetails[n][0].toString().trim());
		enterText(By.xpath(card_name_xpath), carddetails[n][1].toString().trim());
		enterText(By.xpath(cvv_xpath), carddetails[n][2].toString().trim());
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("window.scrollBy(0,250)", "");
		String expmonth = carddetails[n][3].toString().trim();
		String expyear = carddetails[n][4].toString().trim();
		/*
		 * WebElement
		 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
		 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
		 */

		BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
		BaseClass.selecttext("cexpiry_date_year", expyear.toString());
		driver.findElement(By.xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();

		String message = driver.findElement(By.xpath("//*[@class='ng-scope']/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
		System.out.println("Message is :" + message);
		WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
		String policy_Number=Policy.getText();
		System.out.println("Policy number is  :"   +policy_Number);
		Thread.sleep(5000);
		//driver.close();
	}

	public static void POSCaresuperdropdown() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembersPOSSuperSaver");

		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[n][15].toString().trim() + "]")).click();//select 4 based on the excel data
					System.out.println("Total Number of Member Selected : " + TestCaseData[n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// again call the dropdown
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[n][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		try {
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[n][16];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By
											.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

									// List Age of members dropdown
									List<WebElement> List = driver
											.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[n][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By
								.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
						.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[n][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println(
											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[n][26].toString().trim());
									clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
											+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[n][16];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							String Membersdetails = TestCaseData[n][16];
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							System.out.println("Drop down text is :"+DropDowns.getText());
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								System.out.println("Drop down text is :"+DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")) {
								//} else if (DropDowns.getText().equals("46 - 50 Yrs")) {
								System.out.println("text is "+Membersdetails);
								//String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										//List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
										List<WebElement> List = driver.findElements(By.xpath("//div[@class='form-group']//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li"));
										System.out.println("Total number of data is :"  +List.size());
										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}
										}
									}
							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Done ");
		}


	}
	public static void sliderdrag() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String suminsure = TestCaseData[n][25];
		WebElement covertext=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a"));
		System.out.println("Cover Type is : " +covertext.getText());
		if(covertext.getText().equals("Floater")) {

			WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[8]/div/div/div"));
			List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
			//String SliderValue=null;

			for(WebElement Slider:dragable) {
				System.out.println("Policy type is : "+PolicyType);
				Thread.sleep(5000);
				if(	Slider.getText().equals(suminsure)) {
					Slider.click();
					break;
				}
			}

		}else {
			WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[7]/div/div/div"));
			List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
			//String SliderValue=null;

			for(WebElement Slider:dragable) {
				System.out.println("Policy type is : "+PolicyType);
				Thread.sleep(5000);
				if(	Slider.getText().equals(suminsure)) {
					Slider.click();
					break;
				}
			}
		}


	}
	public static void Tenure() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");

		int Tenure = Integer.parseInt(TestCaseData[1][18].toString().trim());

		System.out.println("Selected Tenure is : " +Tenure);
		if(Tenure==1)
		{
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			//clickElement(By.xpath(Radio_Tenure1_xpath));
		}
		else if(Tenure==2)
		{
			clickbyHover(By.xpath(Radio_Tenure2_xpath));	
			//clickElement(By.xpath(Radio_Tenure2_xpath));
		}
		else if(Tenure==3)
		{
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			Thread.sleep(5000);
			//BaseClass.scrollup();
			//clickElement(By.xpath(Radio_Tenure3_xpath));
		}		
		BaseClass.scrolldown();
		clickElement(By.xpath("//button[@class='btn']"));
	}
	public static void proposerDetails() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String Title = TestCaseData[n][1].toString().trim();
		System.out.println("titel Name is:" + Title);
		clickElement(By.xpath(click_title_xpath));

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[n][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));

		Thread.sleep(3000);
		// proposer_dob

		/*
		 * DOB_Proposer=readExcel("Health Insurance").getRow(1).getCell(4).
		 * getStringCellValue(); System.out.println(DOB_Proposer);
		 * enterText(By.id(Dob_Proposer_id),String.valueOf(DOB_Proposer));
		 * logger.log(LogStatus.PASS, "Loan Amount for PA is " + " - " +
		 * DOB_Proposer);
		 */

		final String address1 = TestCaseData[n][7].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		enterText(By.xpath(addressline1_xpath), address1);
		enterText(By.xpath(addressline2_xpath), TestCaseData[1][8].toString().trim());
		enterText(By.xpath(pincode_xpath), TestCaseData[1][9]);
		// Height selection
		String Height = TestCaseData[n][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[n][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[n][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);

		String NomineeName = TestCaseData[n][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		// Nominee Relation
		String Nrelation = TestCaseData[n][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
		String pancard=TestCaseData[n][19].toString().trim();

		String pospancard="KJHYS8977E";
		System.out.println("pancard number is :"+pospancard); try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).
			sendKeys(pospancard); }catch(Exception e) {
				System.out.println("Pan card field not visibled"); }


		clickElement(By.xpath(submit_xpath));

	}
	public static void POS_Supersaver_QuestionSet() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSetPOSSuperSaver");
		String preExistingdeases = TestCaseData[n][20].toString().trim();
		Thread.sleep(5000);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			if(preExistingdeases.contains("YES")) {

				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				//Thread.sleep(2000);
				String years=null;
				String Details=null;		
				for(int qlist=1;qlist<=13;qlist++) {
					Details =QuestionSetData[1][qlist+(qlist-1)].toString().trim();
					years=QuestionSetData[1][qlist+qlist].toString().trim();
					if(Details.equals("")) {
						//break;
					}else 
					{
						int detailsnumber = Integer.parseInt(Details);

						//Will click on check box and select the month & year u
						detailsnumber=detailsnumber+1;
						System.out.println("Details and years are :"+Details+"----"+years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						}catch(Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						}
					}
				}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
			}
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}

		String ChecksData = null;
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) {
			int mch = morechecks + 1;
			ChecksData = TestCaseData[n][20 + morechecks].toString().trim();
			if (ChecksData.equalsIgnoreCase("NO")) {
				System.out.println("Quatation set to NO");
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_no']")).click();
			}  else 
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) {
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();

					}
				} else {
					System.out.println("Check Data is :"  +ChecksData);
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}

		}

		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//*[@id='termsCheckbox3']")).click();
		// driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
		driver.findElement(By.xpath("//*[@id=\"alertCheck\"]")).click();
		JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		// driver.findElement(By.xpath("//*[@id='siQues']/label[2]")).click();
		jse1.executeScript("window.scrollBy(0,250)", "");

		clickElement(By.xpath(proceed_to_pay_xpath));
	}

	public static void POSSuperSaverPayment() throws Exception {
		clickElement(By.xpath(POS_PAYonline_xpath));	
		Thread.sleep(10000);

		String[][] carddetails = BaseClass.excel_Files("carddetails");
		System.out.println("card number is: " + carddetails[1][0].toString().trim());


		//clickElement(By.xpath("//*[@id='drop_image_1']"));
		WebElement text=driver.findElement(By.xpath("//*[@id='manageCardLink']"));
		System.out.println("Text is :"  +text.toString().trim());
		// if(text.equals("Manage this card")){
		if(text.isDisplayed()){  
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

			/*String message = driver
				.findElement(By.xpath("//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
		System.out.println("Message is :" + message);*/
			String policy_Number;
			try {
				WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/p"));
				policy_Number = Policy.getText();
			} catch (Exception e) {

				WebElement Policy=driver.findElement(By.xpath("//p[@class='your_payment_sucess_p ng-binding']"));
				policy_Number = Policy.getText();

			}

			System.out.println("Policy number is  :"   +policy_Number);

		}else{

			Thread.sleep(5000);
			enterText(By.xpath("//input[@name='ccard_number']"), carddetails[1][0].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,250)", "");
			String expmonth = carddetails[1][3].toString().trim();
			String expyear = carddetails[1][4].toString().trim();
			/*
			 * WebElement
			 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
			 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
			 */

			BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
			BaseClass.selecttext("cexpiry_date_year", expyear.toString());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

			/*String message = driver.findElement(By.xpath("//*[@class='ng-scope']/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
		System.out.println("Message is :" + message);*/
			WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
			String policy_Number=Policy.getText();
			System.out.println("Policy number is  :"   +policy_Number);
			Thread.sleep(1000);
			//driver.close();

			driver.close();
		}
	}

	public static void super_mediclaim_critical_Quatotion(String TestType) throws Exception  {

		PolicyType = TestType;
		PolicyType2 = TestType;
		String[][] FamilyData = BaseClass.excel_Files("SuperMediclaimCriticalFamilyMem");
		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String[][] SuperMediClaimTestCaseList = BaseClass.excel_Files("SuperMediClaimTestCaseList");
		String TestCaseName = (SuperMediClaimTestCaseList[n][0].toString().trim() + " - " + SuperMediClaimTestCaseList[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim-Critical - " + TestCaseName);
		System.out.println("SuperMediclaim-Critical - " + TestCaseName);
		
		try {
			testName = TestType.toString();
			if (TestType.contains("super_mediclaim_critical")) {
				clickElement(By.xpath(Health_Insurance_xppath));
				Thread.sleep(8000);
				mousehover(By.xpath(mediclaimcare_xpath));
				clickElement(By.xpath(supermediclaim_xpath));
			}else {
				System.out.println("super_mediclaim critical is not present");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("super_mediclaim critical is not present");
		}
		logger.log(LogStatus.PASS, "Successfully Entered into SuperMedical Critical ");
		driver.findElement(By.name("name")).clear();
		driver.findElement(By.name("name")).sendKeys(TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
		driver.findElement(By.name("ValidEmail")).clear();

		String email = TestCaseData[n][6].toString().trim();
		System.out.println("Email is :" + email);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		
		Thread.sleep(5000);
		String mnumber = TestCaseData[n][5].toString().trim();
		int size = mnumber.length();
		System.out.println("mobile number is: " + mnumber);
		String format = "^[789]\\d{9}$";

		if (mnumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}
	}
	public static void criticaldropdown() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String[][] FamilyData = BaseClass.excel_Files("SuperMediclaimCriticalFamilyMem");

		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[n][15].toString().trim() + "]")).click();//select 4 based on the excel data
					System.out.println("Total Number of Member Selected : " + TestCaseData[n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		//	dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String Membersdetails = TestCaseData[n][16];
					if (Membersdetails.contains("")) {

						BaseClass.membres = Membersdetails.split("");

						member:
							// total number of members
							for (int i = 1; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
								.click();

								// List Age of members dropdown
								List<WebElement> List = driver	
										.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Selcted Age Of Member :" + ListData.getText());

										ListData.click();
									}

								}

							}
					}
					break;
				} 
				else {
					//List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
					List<WebElement> dropdowns = driver.findElements(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
					for (WebElement DropDowns : dropdowns) {	
						String Membersdetails = TestCaseData[n][16];
						if (Membersdetails.contains(",")) {

							BaseClass.membres = Membersdetails.split(",");
						} else {
							System.out.println("Hello");
						}

						member: 
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								//List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
								List<WebElement> List = driver.findElements(By.xpath("//div[@class='form-group']//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li"));
								System.out.println("Total number of data is :"  +List.size());
								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is :" + ListData.getText());
										Thread.sleep(2000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											break member;
										}

									}
								}
							}

					}
				}
			}
	}
	public static void critical_dragdrop() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String suminsure = TestCaseData[n][17];


		WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/div/div"));
		List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
		//String SliderValue=null;

		for(WebElement Slider:dragable) {
			System.out.println("Policy type is : "+PolicyType);
			Thread.sleep(5000);
			if(	Slider.getText().equals(suminsure)) {
				Slider.click();
				break;
			}
		}
Thread.sleep(5000);

	}
	public static void Tenurecritical() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");

		int Tenure = Integer.parseInt(TestCaseData[n][18].toString().trim());

		System.out.println("Selected Tenure is : " +Tenure);
		if(Tenure==1)
		{
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			//clickElement(By.xpath(Radio_Tenure1_xpath));
		}
		else if(Tenure==2)
		{
			clickbyHover(By.xpath(Radio_Tenure2_xpath));	
			//clickElement(By.xpath(Radio_Tenure2_xpath));
		}
		else if(Tenure==3)
		{
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			//clickElement(By.xpath(Radio_Tenure3_xpath));
		}		
		BaseClass.scrollup();
		Thread.sleep(5000);
	}
	public static void Monthly_frequency() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		int Tenure = Integer.parseInt(TestCaseData[n][18].toString().trim());

		String Monthlyfrequency = TestCaseData[n][19];
		System.out.println("Monthly Frequency is :  "  +Monthlyfrequency);
		if(Tenure==1)
		{
			System.out.println("Selected monthly frequency is : "+Monthlyfrequency);
		}

		else if(Tenure==2) {
			if(Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='paymentRadio1q']"));

			}else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='paymentRadio2q']"));
			}
			else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='paymentRadio0q']"));
			}
		}else if(Tenure==3) {
			if(Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='paymentRadio1q']"));

			}else if(Monthlyfrequency.contains("Quarterly")) {
				try {
					clickElement(By.xpath("//label[@for='paymentRadio2q']"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//input[@id='paymentRadio0q']"));
			}
		}
		/*clickElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[2]/div[1]/button"));
		Thread.sleep(3000);
		clickElement(By.xpath("//*[@class='col-md-12 col-xs-12 text-center last-child']/button[contains(text(),'Send')]"));*/
Thread.sleep(5000);
	}	

	public static void critical_verify_premium() throws InterruptedException {

		//	WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]/b[1]"));
		WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[1]/b/p[1]/span"));
		Thread.sleep(10000);
		String firstpage_PremiumValue=firstpage_Premium.getText();
		Thread.sleep(10000);
		System.out.println("First page premium value is   :"  +firstpage_PremiumValue);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,150)", "");
		//clickElement(By.xpath(Group_BuyNow_xpath));
		clickElement(By.xpath(critical_xpath));
		Thread.sleep(5000);
		jse.executeScript("window.scrollBy(0,-350)", "");
		//WebElement secondpage_premium=driver.findElement(By.xpath("//div[@class='col-md-12 tr_quotation_heading']/h4/span/span[@class='ng-binding']"));

		WebElement secondpage_premium=driver.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		String secondpage_PremiumValue=secondpage_premium.getText();
		Thread.sleep(10000);
		System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
		Assert.assertEquals(firstpage_PremiumValue, secondpage_PremiumValue);


	}
	public static void Critical_proposerDetails() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		
		
		clickElement(By.xpath(critical_edit_xpath));
		Thread.sleep(5000);
		
		//Edit quotation page startes
		String[][] TestCaseDataUpdate = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String suminsure = TestCaseData[n][25];


		WebElement drag=driver.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/div/div"));
		List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
		//String SliderValue=null;

		for(WebElement Slider:dragable) {
			System.out.println("Policy type is : "+PolicyType);
			Thread.sleep(5000);
			if(	Slider.getText().equals(suminsure)) {
				Slider.click();
				break;
			}
		}
		
		//Selecting Tenure
		
		int Tenure = Integer.parseInt(TestCaseData[n][26].toString().trim());

		String Monthlyfrequency = TestCaseData[n][27];
		System.out.println("Monthly Frequency is :  "  +Monthlyfrequency);
		if(Tenure==1)
		{
			System.out.println("Selected monthly frequency is : "+Monthlyfrequency);
		}

		else if(Tenure==2) {
			if(Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='Radio2q']"));

			}else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='Radio1q']"));
			}
			else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='Radio0q']"));
			}
		}else if(Tenure==3) {
			if(Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='Radio1q']"));

			}else if(Monthlyfrequency.contains("Quarterly")) {
				try {
					clickElement(By.xpath("//label[@for='Radio2q']"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//input[@id='Radio0q']"));
			}
		}
		
		//Here premium verify
		WebElement update_firstpage_Premium=driver.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[3]/b/p/span"));
		Thread.sleep(10000);
		String update_firstpage_PremiumValue=update_firstpage_Premium.getText();
		Thread.sleep(1000);
		System.out.println("First page premium value is   :"  +update_firstpage_PremiumValue);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		clickElement(By.xpath(critical_updatepremium_xpath));
	

		WebElement secondpage_premium=driver.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		String secondpage_PremiumValue=secondpage_premium.getText();
		Thread.sleep(10000);
		System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
		Assert.assertEquals(update_firstpage_PremiumValue, secondpage_PremiumValue);
Thread.sleep(5000);
		
		
		Thread.sleep(10000);
		System.out.println("All the given data is correct");
		String Title = TestCaseData[n][1].toString().trim();
		System.out.println("titel Name is:" + Title);
		clickElement(By.xpath(click_title_xpath));

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[n][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));

		Thread.sleep(3000);
		// proposer_dob

		/*
		 * DOB_Proposer=readExcel("Health Insurance").getRow(1).getCell(4).
		 * getStringCellValue(); System.out.println(DOB_Proposer);
		 * enterText(By.id(Dob_Proposer_id),String.valueOf(DOB_Proposer));
		 * logger.log(LogStatus.PASS, "Loan Amount for PA is " + " - " +
		 * DOB_Proposer);
		 */

		final String address1 = TestCaseData[n][7].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		enterText(By.xpath(addressline1_xpath), address1);
		enterText(By.xpath(addressline2_xpath), TestCaseData[n][8].toString().trim());
		enterText(By.xpath(pincode_xpath), TestCaseData[n][9]);
		// Height selection
		String Height = TestCaseData[n][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[n][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[n][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);

		String NomineeName = TestCaseData[n][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		// Nominee Relation
		String Nrelation = TestCaseData[n][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
		String pancard=TestCaseData[n][20].toString().trim();

		String pospancard="KJHYS8977E";
		System.out.println("pancard number is :"+pospancard); try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).
			sendKeys(pospancard); }catch(Exception e) {
				System.out.println("Pan card field not visibled"); }


		clickElement(By.xpath(submit_xpath));
Thread.sleep(5000);

	}
	public static void critical_insuredDetails() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String[][] FamilyData = BaseClass.excel_Files("SuperMediclaimCriticalFamilyMem");
		int mcountindex = 0;
		int mcount;
		String Membersdetails = TestCaseData[n][16].toString().trim();
		// BaseClass.membres=Membersdetails.split(",");
		// System.out.println("BaseClass.membres :
		// "+BaseClass.membres[mcountindex].length());
		// mcount= Integer.parseInt(BaseClass.membres[mcountindex].toString());
		mcountindex = mcountindex + 1;
		// String Membersdetails=TestCaseData[1][16].toString().trim();
		if (Membersdetails.contains(",")) {

			// data taking form test case sheet which is
			// 7,4,1,8,2,5
			BaseClass.membres = Membersdetails.split(",");
		} else {
			BaseClass.membres = Membersdetails.split(" ");
		}

		System.out.println("BaseClass.membres.length " + BaseClass.membres.length);
		member: 
			for (int i = 0; i <= BaseClass.membres.length-1; i++) {
				//for (int i = 0; i <= BaseClass.membres.length-2; i++) { for number of members=1

				mcount = Integer.parseInt(BaseClass.membres[i].toString());
				if (i == 0) {
					clickElement(By.xpath(title1_xpath));
					// Select Self Primary
					BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
				} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
					// Date=Date.replaceAll("-", "/");
					// Relation
					BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
					// title
					BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
					enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
					enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
					clickElement(By.name("rel_dob" + i));
					enterText(By.name("rel_dob" + i), String.valueOf(Date));

					BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
					BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
					enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
					//Thread.sleep(5000);
				}
			}
		clickElement(By.xpath("//*[@id='msform']/div[2]/fieldset[2]/input[2]"));

	}

	public static void critical_questionset() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String[][] QuestionSetData = BaseClass.excel_Files("SuperMediclaimQuestion");
		String preExistingdeases = TestCaseData[n][21].toString().trim();
		Thread.sleep(5000);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			if(preExistingdeases.contains("YES")) {

				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				//Thread.sleep(2000);
				String years=null;
				String Details=null;		
				for(int qlist=1;qlist<=13;qlist++) {
					Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
					years=QuestionSetData[n][qlist+qlist].toString().trim();
					if(Details.equals("")) {
						//break;
					}else 
					{
						int detailsnumber = Integer.parseInt(Details);

						//Will click on check box and select the month & year u
						detailsnumber=detailsnumber+1;
						System.out.println("Details and years are :"+Details+"----"+years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						}catch(Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						}
					}
				}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
			}
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}
		String Questionsdata = null;
		String ChecksData = null;
		String[] ChckData = null;
		int datacheck = 0;
		int datacheckq3=0;
		//	String syndromes=TestCaseData[1][28].toString().trim();
		for (int morechecks = 1; morechecks <= 3; morechecks++) {
			int mch = morechecks + 1;
			ChecksData = TestCaseData[n][21 + morechecks].toString().trim();
			Questionsdata= QuestionSetData[n][27 + morechecks].toString().trim();

			if (ChecksData.equalsIgnoreCase("NO")) {
				System.out.println("Quatation set to NO");
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_no']")).click();
			}  else 
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();

				if (Questionsdata.contains(",")) {
					ChckData = Questionsdata.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						WebElement question2=driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']"));
						if(question2.isDisplayed()) {
							question2.click();
						}else {

							WebElement questions2=driver.findElement(By.xpath("//input[@name='qs_503_"+datacheckq3+"']"));
							questions2.click();                   
						}
					}
				}
				else {
					System.out.println("Check Data is :"  +ChecksData);
					//datacheck = Integer.parseInt(ChecksData);
					//datacheck = datacheck - 1;

					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					/*System.out.println("Check data is :"+ChecksData );
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_"+ChecksData+"'] ")).click();*/
				}
				Thread.sleep(5000);
			}

			//}

			driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
			driver.findElement(By.xpath("//*[@id='termsCheckbox3']")).click();
			// driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
			driver.findElement(By.xpath("//*[@id=\"alertCheck\"]")).click();
			JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			// driver.findElement(By.xpath("//*[@id='siQues']/label[2]")).click();
			jse1.executeScript("window.scrollBy(0,250)", "");

			clickElement(By.xpath(proceed_to_pay_xpath));
			Thread.sleep(5000);
		}
	}
	public static void super_criticalQuestionSet() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		String[][] QuestionSetData = BaseClass.excel_Files("SuperMediclaimQuestion");
		String preExistingdeases = TestCaseData[n][21].toString().trim();
		Thread.sleep(5000);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		//BaseClass.scrollup();
		try{
			if(preExistingdeases.contains("YES")) {
				// String testra = TestCaseData[1][21].toString().trim();
				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				//Thread.sleep(2000);
				String years=null;
				String Details=null;		
				for(int qlist=1;qlist<=13;qlist++) {
					Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
					years=QuestionSetData[n][qlist+qlist].toString().trim();
					if(Details.equals("")) {
						//break;
					}else 
					{
						int detailsnumber = Integer.parseInt(Details);

						//Will click on check box and select the month & year u
						detailsnumber=detailsnumber+1;
						System.out.println("Details and years are :"+Details+"----"+years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						}catch(Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						}
					}
				}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
				Thread.sleep(5000);
			}
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}
		Thread.sleep(5000);

		//question2 selection
		String Questionsdata = null;
		String ChecksData = null;
		String[] ChckData = null;
		int datacheck = 0;
		int datacheckq3=0;
		ChecksData = TestCaseData[n][22].toString().trim();
		Questionsdata= QuestionSetData[n][29].toString().trim();

		if (ChecksData.equalsIgnoreCase("no")) {
			System.out.println("Quatation set to NO");
			driver.findElement(By.xpath("//label[@for='question_3_"+ChecksData+"']")).click();
		}else {
			driver.findElement(By.xpath("//label[@for='question_3_"+ChecksData+"']")).click();
			if (Questionsdata.contains(",")) {
				ChckData = Questionsdata.split(",");
				for (String Chdata : ChckData) {
					datacheck = Integer.parseInt(Chdata);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_503_"+datacheck+"']")).click();		

				}
			}else {
				driver.findElement(By.xpath("//input[@name='qs_503_"+datacheck+"']")).click();
			}
		}
		//Question3 selection
		String question3ChecksData=null;
		String question3Questionsdata=null;
		int datacheck3=0;
		question3ChecksData = TestCaseData[n][23].toString().trim();


		String quantityperDay=null;

		if (question3ChecksData.equalsIgnoreCase("no")) {
			System.out.println("Quatation set to NO");
			driver.findElement(By.xpath("//label[@for='question_4_"+question3ChecksData+"']")).click();
		}else {
			driver.findElement(By.xpath("//label[@for='question_4_"+question3ChecksData+"']")).click();

		
			int queslist;
			int qcount=0;
			for( queslist=1;queslist<=3;queslist++) {
				List<WebElement>Noofmember=driver.findElements(By.xpath("//*[@id='collapse4']/div/table/tbody/tr["+queslist+"]/td/div/input[@type='checkbox']"));
				int total_membe=Noofmember.size();
				System.out.println("Total members are : "+total_membe);
				int loop=30+qcount;
				//int loop1=1;
				question3Questionsdata= QuestionSetData[n][loop].toString().trim();
				quantityperDay=QuestionSetData[n][loop+1].toString().trim();
				int noofMemb = Integer.parseInt(TestCaseData[n][15].toString().trim());
				for(int i=1;i<=noofMemb;i++) {	
					if (question3Questionsdata.contains(",")) {
						ChckData = question3Questionsdata.split(",");
						for(String chekdta:ChckData) {
							if(chekdta.equals(String.valueOf(i))) {
								Noofmember.get(i-1).click();
								int td=i+1;
								driver.findElement(By.xpath("//*[@id='collapse4']/div/table/tbody/tr["+queslist+"]/td["+td+"]//textarea[@required='required']")).sendKeys(quantityperDay);
								break;
							}
						}
					}
					else if(question3Questionsdata.contains("")){
						ChckData = question3Questionsdata.split("");
						for(String chekdta:ChckData) {
							if(chekdta.equals(String.valueOf(i))) {
								Noofmember.get(i-1).click();
								int td=i+1;
								driver.findElement(By.xpath("//*[@id='collapse4']/div/table/tbody/tr["+queslist+"]/td["+td+"]//textarea[@required='required']")).sendKeys(quantityperDay);
								break;
							}
						}
					}
				}
				qcount=qcount+2;
			}
		}
			//question4 selection

			String Questions4data = null;
			String Checks4Data = null;
			String[] Chck4Data = null;
			int data4check = 0;
			int datacheckq4=0;
			ChecksData = TestCaseData[n][24].toString().trim();
			// Questionsdata= QuestionSetData[1][28].toString().trim();

			if (ChecksData.equalsIgnoreCase("no")) {
				System.out.println("Quatation set to NO");
				driver.findElement(By.xpath("//label[@for='question_2_"+ChecksData+"']")).click();
			}else {
				driver.findElement(By.xpath("//label[@for='question_2_"+ChecksData+"']")).click();
				if (Questionsdata.contains(",")) {
					ChckData = Questionsdata.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(By.xpath("//input[@name='qs_H002_"+datacheck+"']")).click();;			

					}
				}else {
					driver.findElement(By.xpath("//input[@name='qs_H002_"+datacheck+"']")).click();;
				}
			}

			driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
			driver.findElement(By.xpath("//*[@id='termsCheckbox3']")).click();
			driver.findElement(By.xpath("//input[@id='alertCheck']")).click();
			// driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
			//driver.findElement(By.xpath("//*[@id=\"alertCheck\"]")).click();
			JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			// driver.findElement(By.xpath("//*[@id='siQues']/label[2]")).click();
			jse1.executeScript("window.scrollBy(0,250)", "");
			Thread.sleep(5000);
			clickElement(By.xpath(proceed_to_pay_xpath));
			Thread.sleep(15000);	

		}
	

	public static void Critical_Payment() throws Exception {


		clickElement(By.xpath(critical_PAYonline_xpath));	
		Thread.sleep(10000);

		String[][] carddetails = BaseClass.excel_Files("carddetails");
		System.out.println("card number is: " + carddetails[1][0].toString().trim());


		//clickElement(By.xpath("//*[@id='drop_image_1']"));
		WebElement text=driver.findElement(By.xpath("//*[@id='manageCardLink']"));
		System.out.println("Text is :"  +text.toString().trim());
		// if(text.equals("Manage this card")){
		if(text.isDisplayed()){  
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

			/*String message = driver
					.findElement(By.xpath("//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
			System.out.println("Message is :" + message);*/
			String policy_Number;
			try {
				WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/p"));
				policy_Number = Policy.getText();
			} catch (Exception e) {

				WebElement Policy=driver.findElement(By.xpath("//p[@class='your_payment_sucess_p ng-binding']"));
				policy_Number = Policy.getText();

			}

			System.out.println("Policy number is  :"   +policy_Number);

		}else{

			Thread.sleep(5000);
			enterText(By.xpath("//input[@name='ccard_number']"), carddetails[1][0].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,250)", "");
			String expmonth = carddetails[1][3].toString().trim();
			String expyear = carddetails[1][4].toString().trim();
			/*
			 * WebElement
			 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
			 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
			 */

			BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
			BaseClass.selecttext("cexpiry_date_year", expyear.toString());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

			/*String message = driver.findElement(By.xpath("//*[@class='ng-scope']/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
			System.out.println("Message is :" + message);*/
			WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
			String policy_Number=Policy.getText();
			System.out.println("Policy number is  :"   +policy_Number);
			Thread.sleep(5000);
			//driver.close();

			//driver.close();


		}
	}

}	
