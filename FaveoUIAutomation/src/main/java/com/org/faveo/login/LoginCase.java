package com.org.faveo.login;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.healthinsurance.CareWithNCB;
import com.relevantcodes.extentreports.LogStatus;


public class LoginCase extends BaseClass implements AccountnSettingsInterface{

	public static Logger log = Logger.getLogger("devpinoyLogger");
	
	
	@Test
	public static void LoginwithValidCredendial() throws Exception
	{
			String[][] TestCase=BaseClass.excel_Files("Test_Cases");
			String[][] loginData=BaseClass.excel_Files("Credentials");
			String Environment = loginData[3][1].toString().trim();
		
		//Reading UserName and Password from Excel	
		
		try
		{
		if(Environment.contains("QC"))
		{	
		Fluentwait1(By.xpath(Textbox_UserName_xpath));
		enterText(By.xpath(Textbox_UserName_xpath),loginData[5][1].toString().trim());
		logger.log(LogStatus.PASS,"Entered User Name is  " + loginData[5][1].toString().trim());
		
		enterText(By.xpath(Textbox_Pasword_xpath), loginData[5][2].toString().trim());
		logger.log(LogStatus.PASS,"Entered User Name is  " + loginData[5][2].toString().trim());
		CareWithNCB.TestResult="Pass";
		}
		else if(Environment.contains("UAT"))
		{	
		enterText(By.xpath(Textbox_UserName_xpath),loginData[6][1].toString().trim());
		logger.log(LogStatus.PASS,"Entered User Name is  " + loginData[6][1].toString().trim());
		
		enterText(By.xpath(Textbox_Pasword_xpath), loginData[6][2].toString().trim());
		logger.log(LogStatus.PASS,"Entered Password Name is  " + loginData[6][2].toString().trim());
		CareWithNCB.TestResult="Pass";
		}
		else if(Environment.contains("Staging"))
		{	
		enterText(By.xpath(Textbox_UserName_xpath),loginData[7][1].toString().trim());
		enterText(By.xpath(Textbox_Pasword_xpath), loginData[7][2].toString().trim());
		CareWithNCB.TestResult="Pass";
		}
		}
		catch(AssertionError e){
			logger.log(LogStatus.FAIL, e.getMessage());
			CareWithNCB.TestResult="Fail";	
		}
		
		
		
		//Click on Signin Button
		try{
		waitForElements(By.id(Button_Signin_id));
		clickElement(By.id(Button_Signin_id));
		System.out.println("Successfully Clicked on Sign In Button.");
		logger.log(LogStatus.PASS,"Clicked on Login button");
		}
		catch(AssertionError e)
		{
			System.out.println("Unable to Click on SignIn Button");
		}
		
		
		try{
			Thread.sleep(5000);
		if(driver.findElement(By.xpath("//div[@ng-if='errorMessage']")).isDisplayed()==true){
			Assert.fail(driver.findElement(By.xpath("//div[@ng-if='errorMessage']")).getText());
		}
		}
		catch(Exception e){
			System.out.println("No error Found");
		}
		
		/*try
		{
		String Propo= driver.findElement(By.xpath("//h5[@class='text-center'][contains(text(),'Your Proposals')]")).getText();
		
		}
		catch(Exception e) 
		{
				try{
				waitForElements(By.xpath("//div[@class='alert alert-danger text-center ng-binding ng-scope']"));
				String ErrorMessage = driver.findElement(By.xpath("//div[@class='alert alert-danger text-center ng-binding ng-scope']")).getText();
				
				System.out.println(ErrorMessage);
				logger.log(LogStatus.FAIL, "Test Case is Failed Because : " +ErrorMessage);
				}catch(Exception E1){
					waitForElement(By.xpath(WebServiceDown_xpath));
					String ErrorMessage1 = driver.findElement(By.xpath(WebServiceDown_xpath)).getText();
					logger.log(LogStatus.FAIL, "Test Case is Failed Because : " +ErrorMessage1);
				}
				}*/
			}
		}
		
			
