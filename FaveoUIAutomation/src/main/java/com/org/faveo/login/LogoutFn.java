package com.org.faveo.login;

import org.openqa.selenium.By;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class LogoutFn extends BaseClass implements AccountnSettingsInterface
{

	
	public static void LogoutfromApp()
	{
		
		try{
		//Clicking on Logout Menu Bar
		clickElement(By.xpath(Logout_Grid_xpath));
		
		//Clicking on Logout Button from Menu Bar
		Fluentwait(By.xpath(Logout_Button_xpath));
		clickElement(By.xpath(Logout_Button_xpath));
		
		//verifying logout Message on Login Screen
		String Logout = driver.findElement(By.xpath(Logout_message_xpath)).getText();
		if(Logout.contains("Logged Out successfully!!"))
		{
			System.out.println(Logout);
			logger.log(LogStatus.PASS, Logout);
		}
		else {
			System.out.println(Logout);
			logger.log(LogStatus.FAIL, Logout);
		}
		
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, e);
			System.out.println("Unable to Logout from Application.");
		}
		
	}
	
	
}
