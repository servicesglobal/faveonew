package com.org.faveo.login;

import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class LoginFn extends BaseClass
{
	@Test
	public static void LoginPage() throws Exception
	{
		
			LoginEncaps obj = new LoginEncaps();
			String[][] loginData=BaseClass.excel_Files("Credentials");
			String Environment = loginData[3][1].toString().trim();
			String ENV = Environment;
			int n = 0;
			
			switch(ENV)
			{
			case "QC" : n=5;
						break;
			case "UAT" : n=6;
						break;
			case "Staging" : n=7;
							break;
			default : System.out.println("Invalid Environment.");
			
			}
	    				
			String newUsername = loginData[n][1].toString().trim();
			String newPassword = loginData[n][2].toString().trim();
			
			System.out.println(newUsername);
			
			
			obj.setUsername(newUsername);
			obj.setPassword(newPassword);

			//Launching Browser Using Base Class Method
			LaunchBrowser();
			
			//Entering Credentials and Verifying Login
			obj.LoginwithCredendial(obj.getUsername(), obj.getpassword());
			//obj.VerifyLogin();
		
		}
	@Test
	public static void LoginPage1() throws Exception
	{
		
			LoginEncaps obj = new LoginEncaps();
			String[][] loginData=BaseClass.excel_Files("Credentials");
			String Environment = loginData[3][1].toString().trim();
			String ENV = Environment;
			int n = 0;
			
			switch(ENV)
			{
			case "QC" : n=5;
						break;
			case "UAT" : n=6;
						break;
			case "Staging" : n=7;
							break;
			default : System.out.println("Invalid Environment.");
			
			}
	    				
			String newUsername = loginData[n][1].toString().trim();
			String newPassword = loginData[n][2].toString().trim();
			
			System.out.println(newUsername);
			
			
			obj.setUsername(newUsername);
			obj.setPassword(newPassword);

			//Launching Browser Using Base Class Method
			LaunchBrowser();
			
			//Entering Credentials and Verifying Login
			obj.LoginwithCredendial(obj.getUsername(), obj.getpassword());
			//obj.VerifyLogin();
		
		}
	
	@Test
	public static void LoginPage1(String productType) throws Exception
	{
		
			LoginEncaps obj = new LoginEncaps();
			String[][] loginData=BaseClass.excel_Files("Credentials");
			
			String care = loginData[1][5].toString().trim();
			String superMediclaim = loginData[2][5].toString().trim();
			int n = 0;
			
			if(care.equalsIgnoreCase(productType)){
				String Environment = loginData[3][1].toString().trim();
				String ENV = Environment;
				
				
				switch(ENV)
				{
				case "QC" : n=5;
							break;
				case "UAT" : n=6;
							break;
				case "Staging" : n=7;
								break;
				default : System.out.println("Invalid Environment.");
				
				}
			}
			
			else if(superMediclaim.equalsIgnoreCase(productType)){
				String Environment = loginData[3][1].toString().trim();
				String ENV = Environment;
	
				switch(ENV)
				{
				case "QC" : n=13;
							break;
				case "UAT" : n=14;
							break;
				case "Staging" : n=15;
								break;
				default : System.out.println("Invalid Environment.");
				
				}
			}
	    				
			String newUsername = loginData[n][1].toString().trim();
			String newPassword = loginData[n][2].toString().trim();
			
			System.out.println("Entered UserName: " + newUsername);
			System.out.println("Entered Password: "+ newPassword);
			logger.log(LogStatus.PASS, "Entered UserName: " + newUsername);
			logger.log(LogStatus.PASS, "Entered Password: "+ newPassword);
			
			
			obj.setUsername(newUsername);
			obj.setPassword(newPassword);

			//Launching Browser Using Base Class Method
			LaunchBrowser();
			
			//Entering Credentials and Verifying Login
			obj.LoginwithCredendial(obj.getUsername(), obj.getpassword());
			//obj.VerifyLogin();
		
		}
	
}
