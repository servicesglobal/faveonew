package com.org.faveo.Base;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

import com.faveo.edit.Edit;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.QuotationPage;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationShareQuotationPage extends BaseClass implements AccountnSettingsInterface {
	
	public static String email;
	public static String mobile;
	static String expectedCreationDateValue;

	//Click on Share Quotation Button
	public static void clickOnShareQuotationButton(){
		clickElement(By.xpath(shareQuotation_Xpath));
		System.out.println("Clicked on Share Quotation Button");
		logger.log(LogStatus.PASS, "Clicked on Share Quotation Button");
	}
	
	//Set Email
	public static void setEmail(String SheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String shareQuoteEmail = (TestCaseData[rowNum][3].toString().trim());
		clearTextfield(By.xpath(emailShareQuotation_Xpath));
		enterText(By.xpath(emailShareQuotation_Xpath), shareQuoteEmail);
		
		System.out.println("Data Entered for Quotation Share Email: " + shareQuoteEmail);
		logger.log(LogStatus.PASS, "Data Entered for Quotation Share Email: " + shareQuoteEmail);
		
	}
	
	//Set Email
		public static void setEmail_TravelInsurance(String SheetName, int rowNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteEmail = (TestCaseData[rowNum][3].toString().trim());
			clearTextfield(By.xpath(emailShareQuotationTravel_Xpath));
			enterText(By.xpath(emailShareQuotationTravel_Xpath), shareQuoteEmail);
			
			System.out.println("Data Entered for Quotation Share Email: " + shareQuoteEmail);
			logger.log(LogStatus.PASS, "Data Entered for Quotation Share Email: " + shareQuoteEmail);
			
		}
	
	//Set SMS
	public static void setSms(String SheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String shareQuoteSms = (TestCaseData[rowNum][5].toString().trim());
		clearTextfield(By.xpath(SmsQuotation_Xpath));
		enterText(By.xpath(SmsQuotation_Xpath), shareQuoteSms);
		
		System.out.println("Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
		logger.log(LogStatus.PASS, "Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
	}
	
	//Set SMS
		public static void setSms_TravelInsurance(String SheetName, int rowNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteSms = (TestCaseData[rowNum][5].toString().trim());
			clearTextfield(By.xpath(SmsQuotation_Travel_Xpath));
			enterText(By.xpath(SmsQuotation_Travel_Xpath), shareQuoteSms);
			
			System.out.println("Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
			logger.log(LogStatus.PASS, "Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
		}
	
	//Click on Email Slider
	public static void SetEmailSlider(){
		clickElement(By.xpath(EmailSliderShareQuotation_Xpath));
	}
	
	//Click on Sms Slider
	public static void SetSmsSlider(){
		clickElement(By.xpath(SmsSliderShareQuotation_Xpath));
	}
	
	//Click on Send Button
	public static void clickOnSendButton() throws Exception{
		Thread.sleep(5000);
		//clickElement(By.xpath("//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']//button[text()='Send']"));
		
		clickElement(By.xpath("//h4[text()='Share Quotation']//following::div[1]//button"));
		
		System.out.println("Clicked on Send Button");
		logger.log(LogStatus.PASS, "Clicked on Send Button");
	}
	
	//Click on Send Button
		public static void clickOnSendButton_TravelInsurance() throws Exception{
			Thread.sleep(5000);
			clickElement(By.xpath("//h4[text()='Share Quotation']//following::div[1]/form//div//button"));
			System.out.println("Clicked on Send Button");
			logger.log(LogStatus.PASS, "Clicked on Send Button");
		}
	
	//Close Share Quotation Poup Box
	public static void closePopUp() throws Exception{
		Thread.sleep(2000);
		Fluentwait(By.xpath(closeShareQuotationPoupBox));
		clickElement(By.xpath(closeShareQuotationPoupBox));
		
		System.out.println("Closed Share Quoation Poup");
		logger.log(LogStatus.PASS, "Closed Share Quotation popup");
		
	}
	
	//Close Share Quotation Poup Box
		public static void closePopUp_TravelInsurance() throws Exception{
			Thread.sleep(2000);
			Fluentwait(By.xpath(closeShareQuotationPoupBox_TravelInsurance));
			clickElement(By.xpath(closeShareQuotationPoupBox_TravelInsurance));
			
			System.out.println("Closed Share Quoation Poup");
			logger.log(LogStatus.PASS, "Closed Share Quotation popup");
			
		}
	
	//Read Email
	public static String readEmail() throws Exception{
		Thread.sleep(2000);
		 String Popupemail=driver.findElement(By.xpath(emailTextValueInPopupBox)).getAttribute("innerHTML");
		 email=Popupemail;
		 
		System.out.println("Email Dispalyed in Shared Quotation Popup: " + email);
		logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + email);
		return email;
		
	}
	
	//Read Mobile
	public static String readMobileNum() throws Exception{
		Thread.sleep(2000);
		String popupmobile=driver.findElement(By.xpath(SmsTextValueInPopupBox)).getAttribute("innerHTML");
	      mobile=popupmobile;
		 
		System.out.println("Mobile Number Dispalyed in Shared Quotation Popup: " + mobile);
		logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + mobile);
		return mobile;
	}
	
	//Read Email
		public static String readEmail_TravelInsurance() throws Exception{
			Thread.sleep(2000);
			 String Popupemail=driver.findElement(By.xpath(emailTextValueInPopupBox_TravelInsurance)).getAttribute("innerHTML");
			 email=Popupemail;
			 
			System.out.println("Email Dispalyed in Shared Quotation Popup: " + email);
			logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + email);
			return email;
			
		}
		
		//Read Mobile
		public static String readMobileNum_TravelInsurance() throws Exception{
			Thread.sleep(2000);
			String popupmobile=driver.findElement(By.xpath(SmsTextValueInPopupBox_TravelInsurance)).getAttribute("innerHTML");
		      mobile=popupmobile;
			 
			System.out.println("Mobile Number Dispalyed in Shared Quotation Popup: " + mobile);
			logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + mobile);
			return mobile;
		}
	
	//Get Current Date
	public static String getCurrentDateValue(){
		SimpleDateFormat f=new SimpleDateFormat("dd/MM/YYYY");
		Date d=new Date();
		 expectedCreationDateValue=f.format(d);
		System.out.println(expectedCreationDateValue);
		return expectedCreationDateValue;
	}
	
	//Function for Set Details of Share Quotation inside Popup in Quotation Page
	public static void setDetailsOfShareQuotation(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
		String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
		
		//String shareQuoteOnlyByEmail="Yes";
		//String shareQuoteOnlyBySms="Yes";
		if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
			setEmail(SheetName, rowNum);
			SetSmsSlider();
			clickOnSendButton();
			readEmail();
			
		}
		else if(shareQuoteOnlyByEmail.equalsIgnoreCase("No") && shareQuoteOnlyBySms.equalsIgnoreCase("Yes")){
			SetEmailSlider();
			setSms(SheetName, rowNum);
			clickOnSendButton();
			readMobileNum();
		}
		else{
			setEmail(SheetName, rowNum);
			setSms(SheetName, rowNum);
			clickOnSendButton();
			readEmail();
			readMobileNum();
			
		}
		
		getCurrentDateValue();
	}
	
	//Function for Set Details of Share Quotation inside Popup in Quotation Page
		public static void setDetailsOfShareQuotation_TravelInsurance(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
			String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
			
			//String shareQuoteOnlyByEmail="Yes";
			//String shareQuoteOnlyBySms="Yes";
			if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
				setEmail_TravelInsurance(SheetName, rowNum);
				SetSmsSlider();
				clickOnSendButton();
				readEmail();
				
			}
			else if(shareQuoteOnlyByEmail.equalsIgnoreCase("No") && shareQuoteOnlyBySms.equalsIgnoreCase("Yes")){
				SetEmailSlider();
				setSms_TravelInsurance(SheetName, rowNum);
				clickOnSendButton();
				readMobileNum();
			}
			else{
				setEmail_TravelInsurance(SheetName, rowNum);
				setSms_TravelInsurance(SheetName, rowNum);
				clickOnSendButton_TravelInsurance();
				readEmail_TravelInsurance();
				readMobileNum_TravelInsurance();
				
			}
			
			getCurrentDateValue();
		}
	
	//Function for Click on Dashboard
	public static void clickOnDashboard(){
		Fluentwait(By.xpath(Dashboard_logoImage_Xpath));
		clickElement(By.xpath(Dashboard_logoImage_Xpath));
		System.out.println("Moved to Dashboard Page");
		logger.log(LogStatus.PASS, "Moved to Dashboard Page");
	}
	
	//Click on Hamburger Menu
	public static void clickOnHaburgerMenu() throws Exception{
		Thread.sleep(5000);
		Fluentwait(By.xpath(hamburgerMenu_Xpath));
		clickElement(By.xpath(hamburgerMenu_Xpath));
		System.out.println("Clicked on Hamburger Menu");
		logger.log(LogStatus.PASS, "Clicked on Hamburger Menu");
	}
	
	//Click on Quotation Tracker
	public static void clickOnQuotationTracker() throws Exception{
		Thread.sleep(2000);
		Fluentwait(By.xpath(quotationTracker_Xpath));
		clickElement(By.xpath(quotationTracker_Xpath));
		System.out.println("Select Quotation Tracker");
		logger.log(LogStatus.PASS, "Select Quotation Tracker");
	}
	
	//Select Filters
	public static void setFilterByDays(String SheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Days = (TestCaseData[rowNum][8].toString().trim());
		
		Thread.sleep(2000);
		scrollup();
		scrollup();
		List<WebElement> list=driver.findElements(By.xpath("//div[contains(@class,'Check_buttons')]//button"));
	
		for(WebElement options:list){
			if(options.getText().equalsIgnoreCase(Days)){
				options.click();
				break;
			}
			else if(options.getText().equalsIgnoreCase(Days)){
				options.click();
			}
			else if(options.getText().equalsIgnoreCase(Days)){
				options.click();
			}
		}
		
		
		System.out.println("Days Selected: "+ Days);
		logger.log(LogStatus.PASS, "Days Selected: "+ Days);
	}
	
	//Select Filter by Search Text Box
	public static void setFilterBySearchTextBoxParameters(String sheetName, int rowNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(sheetName);
		String email = (TestCaseData[rowNum][3].toString().trim());
		clickElement(By.xpath("//div[contains(@class,'padding0')]//ul[contains(@class,'month_year_by')]//preceding-sibling::a"));
		List<WebElement> list=driver.findElements(By.xpath("//div[contains(@class,'padding0')]//ul[contains(@class,'month_year_by')]//preceding-sibling::a//following-sibling::ul//li//a"));
		Iterator<WebElement> itr=list.iterator();
		while(itr.hasNext()){
			WebElement option=itr.next();
			String str=option.getText();
			
			if(str.equalsIgnoreCase("By Email")){
				option.click();
				System.out.println("Filter By Email Selected");
				break;
			}
		}
		enterText(By.xpath(searchByEmailTextBox_Xpath), email);
		System.out.println("Enter Email Id in Search Text box: " + email);
		logger.log(LogStatus.PASS, "Enter Email Id in Search Text box: " + email);
		
		clickElement(By.xpath(Quotation_searchButton_Xpath));
		System.out.println("Clicked on Search Button");
		logger.log(LogStatus.PASS, "Clicked on Search Button");
	}
	
	//Set and Read Details in Quotation Tracker Table
	public static void VerifyDetailsShouldBeAvailableInQuotationTable(String ProductName) throws Exception{
		Thread.sleep(3000);
		List<WebElement> emailList=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[5]"));
		List<WebElement> premiumAmount=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[2]"));
		List<WebElement> status=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[8]"));
		List<WebElement> CreationDate=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[3]"));
		List<WebElement> productNameList = driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[1]"));
		String expectedEmail=email;
		String expectedPremium= AddonsforProducts.ExpectedpremiumAmount;
		String expectedCreationDateValue1=expectedCreationDateValue;
		String expcetedProductName=ProductName;
		
		/*String expectedEmail="rhiclqctech@religare.com";
		String expectedPremium="607";*/
		//String expectedCreationDateValue1="31/12/2018";
		
		System.out.println("Expected Data in Quotation Details Page - ProductName: "+ expcetedProductName +" Email: "+expectedEmail+","+"Premium:"+","+expectedPremium+",Creation Date:"+expectedCreationDateValue1);
		logger.log(LogStatus.PASS, "Expected Data in Quotation Details Page - ProductName: "+ expcetedProductName +" Email: "+expectedEmail+","+"Premium:"+","+expectedPremium+",Creation Date:"+expectedCreationDateValue1);
		int j=0;
		int k=0;
		int l=0;
		int m=0;
			
		for(WebElement emailOptions: emailList){
		String email=emailOptions.getText();
		
		String premiumAmountVal=premiumAmount.get(j).getText();
		String premiumAmountVal1=premiumAmountVal.replaceAll("\\W", "");
		
		String StatusValue=status.get(k).getText();
		String creationDateValue=CreationDate.get(l).getText();
		String productname=productNameList.get(m).getText();
		//String productname="care";
		
		System.out.println("Acutal Data in quotation Tracker - ProducName: "+productname+" Email : "+email+",Premium: "+premiumAmountVal1+",Creation Date:"+creationDateValue);
		logger.log(LogStatus.PASS, "Acutal Data in quotation Tracker - Email: "+email+","+" Premium: "+ premiumAmountVal1+",Creation Date:"+creationDateValue);
		
		if(productname.equalsIgnoreCase(expcetedProductName) && email.equalsIgnoreCase(expectedEmail) && premiumAmountVal1.equalsIgnoreCase(expectedPremium) && creationDateValue.equalsIgnoreCase(expectedCreationDateValue1) && StatusValue.equalsIgnoreCase("Shared")){
		System.out.println("Verified the Share Quotation Data Inside Quotation Tracker Value of email:" +expectedEmail +"," +" Premium Amount: "+ expectedPremium+ ","+ "Creation Date: "+expectedCreationDateValue1 + "," + "Status Value: "+StatusValue);
		break;
			}
			else{
				Assert.fail("Shared Quotation Values Productname: "+productname+"Email" + email +","+ "Premium Amount: "+premiumAmountVal1+","+"Creation Date Value: "+ creationDateValue + " "+"are not available inside Quotation Tracker");
			}
		}
		
	}
	
	
	// Set and Read Details in Quotation Tracker Table
	public static void VerifyDetailsShouldBeNotAvailableInQuotationTable(String ProductName) throws Exception {
		Thread.sleep(3000);
		List<WebElement> emailList = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[5]"));
		List<WebElement> premiumAmount = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[2]"));
		List<WebElement> productNameList = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[1]"));
		List<WebElement> CreationDate = driver
				.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[3]"));

		
		 String expectedEmail=email; 
		 String expectedPremium=AddonsforProducts.ExpectedpremiumAmount;
		 String expectedCreationDateValue1=expectedCreationDateValue;
		 String expctedProductName = ProductName;
		 

		/*String expectedEmail = "rhiclqctech@religre.com";
		String expectedPremium = "7848";
	    String expectedCreationDateValue1 = "10/04/2019";*/
		

		int j = 0;
		int k = 0;
		int l = 0;

		for (WebElement emailOptions : emailList) {
			String Acutalemail = emailOptions.getText();

			String premiumAmountVal = premiumAmount.get(j).getText();
			String ActualpremiumAmountVal1 = premiumAmountVal.replaceAll("\\W", "");

			String ActualStatusValue = productNameList.get(k).getText();
			
			//String ActualStatusValue="care";
			
			String ActualcreationDateValue = CreationDate.get(l).getText();

			if ((!expectedEmail.equals(Acutalemail)) || (!expectedPremium.equals(ActualpremiumAmountVal1))
					|| (!expectedCreationDateValue1.equals(ActualcreationDateValue))
					|| (!expctedProductName.equals(ActualStatusValue))) {
				System.out.println("Verified the Share Quotation Data is not avilable in Quotation Tracker");
				break;
			} else {
				System.out.println("Share Quotation Data is still avilable in Quotation Tracker");
				break;
			}
		}

	}
	
	
	//Set and Read Details in Quotation Tracker Table
		public static void VerifyDetailsShouldBeAvailableInQuotationTable2(String ProductName) throws Exception{
			Thread.sleep(3000);
			List<WebElement> emailList=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[5]"));
			List<WebElement> premiumAmount=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[2]"));
			List<WebElement> status=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[8]"));
			List<WebElement> CreationDate=driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr//td[3]"));
			List<WebElement> productNameList = driver.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[1]"));
			String expectedEmail=email;
			String expectedPremium= AddonsforProducts.ExpectedpremiumAmount;
			String expectedCreationDateValue1=expectedCreationDateValue;
			String expcetedProductName=ProductName;
			
			/*String expectedEmail="rhiclqctech@religare.com";
			String expectedPremium="607";*/
			//String expectedCreationDateValue1="31/12/2018";
			
			System.out.println("Expected Data in Quotation Details Page - ProductName: "+ expcetedProductName +" Email: "+expectedEmail+","+"Premium:"+","+expectedPremium+",Creation Date:"+expectedCreationDateValue1);
			logger.log(LogStatus.PASS, "Expected Data in Quotation Details Page - ProductName: "+ expcetedProductName +" Email: "+expectedEmail+","+"Premium:"+","+expectedPremium+",Creation Date:"+expectedCreationDateValue1);
			int j=0;
			int k=0;
			int l=0;
			int m=0;
				
			for(WebElement emailOptions: emailList){
			String email=emailOptions.getText();
			
			String premiumAmountVal=premiumAmount.get(j).getText();
			String premiumAmountVal1=premiumAmountVal.replaceAll("\\W", "");
			
			String StatusValue=status.get(k).getText();
			String creationDateValue=CreationDate.get(l).getText();
			String productname=productNameList.get(m).getText();
			//String productname="care";
			
			System.out.println("Acutal Data in quotation Tracker - ProducName: "+productname+" Email : "+email+",Premium: "+premiumAmountVal1+",Creation Date:"+creationDateValue);
			logger.log(LogStatus.PASS, "Acutal Data in quotation Tracker - Email: "+email+","+" Premium: "+ premiumAmountVal1+",Creation Date:"+creationDateValue);
			
			if(productname.contains(expcetedProductName) && email.equalsIgnoreCase(expectedEmail) && premiumAmountVal1.equalsIgnoreCase(expectedPremium) && creationDateValue.equalsIgnoreCase(expectedCreationDateValue1) && StatusValue.equalsIgnoreCase("Shared")){
			System.out.println("Verified the Share Quotation Data Inside Quotation Tracker Value of email:" +expectedEmail +"," +" Premium Amount: "+ expectedPremium+ ","+ "Creation Date: "+expectedCreationDateValue1 + "," + "Status Value: "+StatusValue);
			break;
				}
				else{
					Assert.fail("Shared Quotation Values Productname: "+productname+"Email" + email +","+ "Premium Amount: "+premiumAmountVal1+","+"Creation Date Value: "+ creationDateValue + " "+"are not available inside Quotation Tracker");
				}
			}
			
		}
		
		
		// Set and Read Details in Quotation Tracker Table
		public static void VerifyDetailsShouldBeNotAvailableInQuotationTable2(String ProductName) throws Exception {
			Thread.sleep(3000);
			List<WebElement> emailList = driver
					.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[5]"));
			List<WebElement> premiumAmount = driver
					.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[2]"));
			List<WebElement> productNameList = driver
					.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[1]"));
			List<WebElement> CreationDate = driver
					.findElements(By.xpath("//table[@ng-table='quoteTrackingTable']//tbody//tr[1]//td[3]"));

			
			 String expectedEmail=email; 
			 String expectedPremium=AddonsforProducts.ExpectedpremiumAmount;
			 String expectedCreationDateValue1=expectedCreationDateValue;
			 String expctedProductName = ProductName;
			 

			/*String expectedEmail = "rhiclqctech@religre.com";
			String expectedPremium = "7848";
		    String expectedCreationDateValue1 = "10/04/2019";*/
			

			int j = 0;
			int k = 0;
			int l = 0;

			for (WebElement emailOptions : emailList) {
				String Acutalemail = emailOptions.getText();

				String premiumAmountVal = premiumAmount.get(j).getText();
				String ActualpremiumAmountVal1 = premiumAmountVal.replaceAll("\\W", "");

				String ActualStatusValue = productNameList.get(k).getText();
				
				//String ActualStatusValue="care";
				
				String ActualcreationDateValue = CreationDate.get(l).getText();

				if ((!expectedEmail.equals(Acutalemail)) || (!expectedPremium.equals(ActualpremiumAmountVal1))
						|| (!expectedCreationDateValue1.equals(ActualcreationDateValue))
						|| (!expctedProductName.contains(ActualStatusValue))) {
					System.out.println("Verified the Share Quotation Data is not avilable in Quotation Tracker");
					break;
				} else {
					System.out.println("Share Quotation Data is still avilable in Quotation Tracker");
					break;
				}
			}

		}
	
	
	public static void verifyPageTitle(){
		String expectedTitle="Religare Health Insurance";
		String acutalTitle=driver.getTitle();
		Assert.assertEquals(expectedTitle, acutalTitle);
		System.out.println("Application Title is verified and GUID is Reusable for multiple Times");
		logger.log(LogStatus.PASS, "Application Title is verified and GUID is Reusable for multiple Times");
	}
	
	// Function for VerifyData in Quotation Tracker
	public static void verifyDataInQuotationTracker(String SheetName, int rowNum, String ProductName) throws Exception {
		clickOnHaburgerMenu();
		clickOnQuotationTracker();
		setFilterByDays(SheetName, rowNum);
		setFilterBySearchTextBoxParameters(SheetName, rowNum);
		VerifyDetailsShouldBeAvailableInQuotationTable(ProductName);

	}
	
	// Function for VerifyData in Quotation Tracker
	public static void verifyDataShouldBeNotAvailableInQuotationTracker(String SheetName, int rowNum, int colNum,
			String ProductName) throws Exception {
		Thread.sleep(5000);
		clickOnHaburgerMenu();
		clickOnQuotationTracker();
		setFilterByDays(SheetName, rowNum);
		setFilterBySearchTextBoxParameters(SheetName, rowNum);
		VerifyDetailsShouldBeNotAvailableInQuotationTable(ProductName);

	}
	
	// Function for VerifyData in Quotation Tracker
		public static void verifyDataInQuotationTracker2(String SheetName, int rowNum, String ProductName) throws Exception {
			clickOnHaburgerMenu();
			clickOnQuotationTracker();
			setFilterByDays(SheetName, rowNum);
			setFilterBySearchTextBoxParameters(SheetName, rowNum);
			VerifyDetailsShouldBeAvailableInQuotationTable2(ProductName);

		}
		
		// Function for VerifyData in Quotation Tracker
		public static void verifyDataShouldBeNotAvailableInQuotationTracker2(String SheetName, int rowNum, int colNum,
				String ProductName) throws Exception {
			Thread.sleep(5000);
			clickOnHaburgerMenu();
			clickOnQuotationTracker();
			setFilterByDays(SheetName, rowNum);
			setFilterBySearchTextBoxParameters(SheetName, rowNum);
			VerifyDetailsShouldBeNotAvailableInQuotationTable2(ProductName);

		}
	
	public static void setDataOfSharequotationPopupBox(int rowNum) throws Exception{
		setDetailsOfShareQuotation("ShareQuotation", rowNum, 6, 7);
		closePopUp();
	}
	
	public static void setDataOfSharequotationPopupBox_TravelInsurance(int rowNum) throws Exception{
		setDetailsOfShareQuotation_TravelInsurance("ShareQuotation", rowNum, 6, 7);
		closePopUp_TravelInsurance();
	}
	
	//***************************************************************************************
	
	//Enter Quotation Data for Super Mediclaim Cancer
	public static void QuotationPageJourney(int Rownum) throws Exception
	{
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("SuperMediclaimCancer_TestData", Rownum, 2);
					
		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("SuperMediclaimCancer_TestData", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("SuperMediclaimCancer_TestData", Rownum, 4);
		
		// Enter The Value of Total members present in policy
		QuotationPage.SelectNoOfMembersInSuperMediClaim("SuperMediclaimCancer_TestData", Rownum, 5);
		
		// Select Age of Member 1 from Excel
		QuotationPage.setAgeDeatilsforMemebrsInSuperMediclaim("SuperMediclaimCancer_TestData", Rownum,6,7,8,9,10,11);
		
		// Read the Value of Suminsured
		QuotationPage.SumInsured("SuperMediclaimCancer_TestData", Rownum, 13);
		
		// Reading the value of Tenure from Excel
		QuotationPage.setTenureForSuperMedicliamCancer("SuperMediclaimCancer_TestData", Rownum, 14);
		
		// Scroll Window Up
		BaseClass.scrollup();

		// NCB Super Addon Selection
		AddonsforProducts.setPaymentFrequency("SuperMediclaimCancer_TestData", Rownum, 15);
		
		AddonsforProducts.readPremiumFromFirstPage();
		
		clickOnShareQuotationButton();
			
	}
	
	//************* SuperMediclaim Cancer **************************************
	//Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
	public static void verifyDataInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(int rowNum) throws Exception{
		/*setDetailsOfShareQuotation("ShareQuotation", rowNum, 6, 7);
		closePopUp();*/
		//setDataOfSharequotationPopupBox(rowNum);
		verifyDataInQuotationTracker("ShareQuotation", rowNum,"Cancer Mediclaim");
	}
	
	//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForSuperMediclaimCancer(int rowNum) throws Exception{
		/*setDetailsOfShareQuotation("ShareQuotation", rowNum, 6, 7);
		closePopUp();*/
		//setDataOfSharequotationPopupBox(rowNum);
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody("SuperMediclaimCancer_TestData", rowNum);
	}
	
	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(int rowNum) throws Exception{
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"Cancer Mediclaim");
	}
	
	//***************** Super Mediclaim Critical **************************************
	//Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimCritical(int rowNum) throws Exception{
			verifyDataInQuotationTracker("ShareQuotation", rowNum,"Critical Mediclaim");
		}
	
		//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForSuperMediclaimCritical(int rowNum) throws Exception{
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_SuperMediclaimCritical("SuperMediclaimCriticalTestCase", rowNum);
		}
		
		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimritical(int rowNum) throws Exception{
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"Critical Mediclaim");
		}
	
	//************** Care HNI *******************************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
	public static void verifyDataInOfShareQuotationInQuotationTracker_ForCareHNI(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum,"Care");
	}
	
	//****************** care(with NCB) **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForCareWithNCB(int rowNum)throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum,"Care");
	}
	//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForCareWithNCB(int rowNum) throws Exception{
		try{
				ReadDataFromEmail.openEmail();
				ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareWithNCB("Care_Quotation_Data", rowNum);
		}
		catch(Exception e){
			throw e;
		}
	}
	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareNCB(int rowNum) throws Exception{
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"Care");
	}
	
	
	//****************** care Heart **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForCareHeart(int rowNum)throws Exception {
			verifyDataInQuotationTracker("CareHeart_Quotation", rowNum,"Care");
		}
	
		public static void verifyDataOfShareQuotationInMail_ForCareHeart(int rowNum) throws Exception{
			try{
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareHeart("CareHeart_Quotation", rowNum);
			}
			catch(Exception e){
				throw e;
			}
		}
		
		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareHeart(int rowNum) throws Exception{
			verifyDataShouldBeNotAvailableInQuotationTracker("CareHeart_Quotation", rowNum, 8,"Care");
		}
	//****************** care Freedom **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForCareFreedom(int rowNum)throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum,"Care");
		}
		//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForCareFreedom(int rowNum) throws Exception{
			try{
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareFreedom("Care_Freedom_Quotation", rowNum);
			}
			catch(Exception e){
				throw e;
			}
		}
		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareFreedom(int rowNum) throws Exception{
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"Care");
		}
		
	// ****************** care Global **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care
	// with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForCareGlobal(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "Care");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForCareGlobal(int rowNum) throws Exception {
		try {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareGlobal("CareGlobal_Quotation", rowNum);
		} catch (Exception e) {
			throw e;
		}
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareGlobal(int rowNum)
			throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "Care");
	}

	// ****************** CARE HNI **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care
	// with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForCareHNI(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "Care");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForCareHNI(int rowNum) throws Exception {
		try {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareGlobal("CareHNI_Quotation", rowNum);
		} catch (Exception e) {
			throw e;
		}
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareHNI(int rowNum)
			throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "Care");
	}
	
	//*********** POS CARE FREEDOM ******************************************
	
	public static void QuotationPageJourney_POSCAREFreedom(int Rownum) throws Exception {
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("PosCarefreedom_Quotation_Data", Rownum, 2);

		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("PosCarefreedom_Quotation_Data", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("PosCarefreedom_Quotation_Data", Rownum, 4);

		// Enter The Value of Total members present in policy
		QuotationPage.SelectMembers("PosCarefreedom_Quotation_Data", Rownum, 5);

		// again call the dropdown
		QuotationPage.SelectAgeofMemberCareFreedom("PosCarefreedom_Quotation_Data", "Pos_CareFreedom_Insured_Details",
				Rownum, 5, 6, 7, 8);

		// Read the Value of Suminsured
		QuotationPage.SumInsured("PosCarefreedom_Quotation_Data", Rownum, 9);

		// Reading the value of Tenure from Excel
		QuotationPage.Tenure("PosCarefreedom_Quotation_Data", Rownum, 10);
		Thread.sleep(5000);
		// Scroll Window Up
		// BaseClass.scrollup();
		scrollup();
		// NCB Super Addon Selection
		AddonsforProducts.CareFreedomAddons("PosCarefreedom_Quotation_Data", Rownum, 11);

		/*// Assertion
		QuotationandProposalVerification.PremiumAssertionPOSFreedom();

		String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");
		String executionstatus = TestCase[Rownum][4].toString().trim();
		if (executionstatus.equals("Edit")) {
			Edit.editposcarefreedom();
		} else {
			System.out.println("Edit not required.............");
		}*/
		scrollup();

	}
	
	// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
			public static void verifyDataOfShareQuotationInQuotationTracker_ForPOSCareFreedom(int rowNum)throws Exception {
				verifyDataInQuotationTracker("ShareQuotation", rowNum,"POS Care Freedom");
			}
			//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
			public static void verifyDataOfShareQuotationInMail_ForPOSCareFreedom(int rowNum) throws Exception{
				try{
						ReadDataFromEmail.openEmail();
						ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_POSCareFreedom("PosCarefreedom_Quotation_Data", rowNum);
				}
				catch(Exception e){
					throw e;
				}
			}
			public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSCareFreedom(int rowNum) throws Exception{
				verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"POS Care Freedom");
			}
			
	// ****************** Care with Smart Select **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care
	// with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForCareWithSmartSelect(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "Care");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForCareWithSmartSelect(int rowNum) throws Exception {
		try {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareWithSmartSelect("SmartSelect_Quotation", rowNum);
		} catch (Exception e) {
			throw e;
		}
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareWithSmartSelect(int rowNum)
			throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "Care");
	}
	
	// ****************** POS Care with Smart Select **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function for Care
		// with NCB SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForPOSCareWithSmartSelect(int rowNum) throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum, "Care");
		}

		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForPOSCareWithSmartSelect(int rowNum) throws Exception {
			try {
				ReadDataFromEmail.openEmail();
				ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_POSCareWithSmartSelect("POSSmartSelect_Quotation", rowNum);
			} catch (Exception e) {
				throw e;
			}
		}

		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSCareWithSmartSelect(int rowNum)
				throws Exception {
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "Care");
		}
	
	//****************** POS care with NCB **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForPOSCareWithNCB(int rowNum)throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum,"POS CARE");
		}
		//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForPOSCareWithNCB(int rowNum) throws Exception{
			try{
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_POSCareWithNCB("POSCare_With_NCB_TestCaseData", rowNum);
			}
			catch(Exception e){
				throw e;
			}
		}
		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSCareNCB(int rowNum) throws Exception{
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"POS CARE");
		}
		
		//****************** Care Senior **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForCareSenior(int rowNum)throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum,"CARE SENIOR");
		}
		//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForCareSenior(int rowNum) throws Exception{
			try{
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareSenior("Senior_Quotation_Data", rowNum);
			}
			catch(Exception e){
				throw e;
			}
		}
		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareSenior(int rowNum) throws Exception{
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"CARE SENIOR");
		}
		
		
		//****************** POS Care Senior **************************************
				// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
				public static void verifyDataOfShareQuotationInQuotationTracker_ForPOSCareSenior(int rowNum)throws Exception {
					verifyDataInQuotationTracker("ShareQuotation", rowNum,"POS CARE SENIOR");
				}
				//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
				public static void verifyDataOfShareQuotationInMail_ForPOSCareSenior(int rowNum) throws Exception{
					try{
							ReadDataFromEmail.openEmail();
							ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_POSCareSenior("POScaresenior_Quotation", rowNum);
					}
					catch(Exception e){
						throw e;
					}
				}
				public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSCareSenior(int rowNum) throws Exception{
					verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"POS CARE SENIOR");
				}
		
		
	// ****************** Care OPD **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care
	// with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForCareOPD(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "CARE");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForCareOPD(int rowNum) throws Exception {
		try {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_CareOPD("CareWithOPD_Quotation", rowNum);
		} catch (Exception e) {
			throw e;
		}
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareOPD(int rowNum)
			throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "CARE");
	}
				
		
	// ****************** Enhance **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForEnhance(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "Enhance - 1");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForEnhance(int rowNum) throws Exception {
		try {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_Enhnace("Enhance_Quotation", rowNum);
		} catch (Exception e) {
			e.getMessage();
			
		}
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForEnhance(int rowNum)
			throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "Enhance - 1");
	}
	
	//********************* Secure ***************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForSecure(int rowNum) throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "SECURE - 1");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForSecure(int rowNum) throws Exception {
		try {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_Secure("Secure_Quotation", rowNum);
		} catch (Exception e) {
			throw e;
		}
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSecure(int rowNum)
			throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "SECURE - 1");
	}
	
	//********************* POS Secure ***************************
	// Verify Data for Share Quotation In Quotation Tracker Function for Care with NCB SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForPOSSecure(int rowNum) throws Exception {
		verifyDataInQuotationTracker2("ShareQuotation", rowNum, "SECURE");
	}
	
	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForPOSSecure(int rowNum) throws Exception {
			try {
				ReadDataFromEmail.openEmail();
				ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_POSSecure("POSSecQuotation", rowNum);
			} catch (Exception e) {
				throw e;
			}
		}
		
		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSSecure(int rowNum)
				throws Exception {
			verifyDataShouldBeNotAvailableInQuotationTracker2("ShareQuotation", rowNum, 8, "SECURE");
		}
	
	//***************** Super Mediclaim Operation **************************************
		//Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
			public static void verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimOperation(int rowNum) throws Exception{
				verifyDataInQuotationTracker("ShareQuotation", rowNum,"Operation Mediclaim");
			}
		
			//Verify Data of Share Quotation In Email for Super Mediclaim Cancer
			public static void verifyDataOfShareQuotationInMail_ForSuperMediclaimOperation(int rowNum) throws Exception{
				ReadDataFromEmail.openEmail();
				ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_SuperMediclaimOperation("Operation_Quotation", rowNum);
			}
			
			public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimOperation(int rowNum) throws Exception{
				verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8,"Operation Mediclaim");
			}
			
	// ***************** Super Mediclaim Operation **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for
	// SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimHeart(int rowNum)
			throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "Heart Mediclaim");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForSuperMediclaimHeart(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareQuotation_SuperMediclaimHeart("Heart_Quotation",
				rowNum);
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimHeart(
			int rowNum) throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "Heart Mediclaim");
	}
	
	// ***************** Travel Insurance - Explore **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function for
		// SuperMedclaim
		public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_Explore(int rowNum)
				throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum, "EXPLORE");
		}

		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_Explore(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_Explore("Explore_Quotation",rowNum);
		}

		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_Explore(
				int rowNum) throws Exception {
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "EXPLORE");
		}
		
		// ***************** Travel Insurance -POS Explore **************************************
				// Verify Data for Share Quotation In Quotation Tracker Function for
				// SuperMedclaim
				public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_POSExplore(int rowNum)
						throws Exception {
					verifyDataInQuotationTracker("ShareQuotation", rowNum, "POS EXPLORE");
				}

				// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
				public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_POSExplore(int rowNum) throws Exception {
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_POSExplore("POSExplore_Quotation",rowNum);
				}

				public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_POSExplore(
						int rowNum) throws Exception {
					verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "POS EXPLORE");
				}
		
		// ***************** Travel Insurance - Student Explore ************************************** Verify Data for Share Quotation In Quotation Tracker Function for
	public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_StudentExplore(int rowNum)
			throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "STUDENT EXPLORE");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_StudentExplore(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_StudentExplore("StudentExplore_Quotation",
				rowNum);
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_StudentExplore(
			int rowNum) throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "STUDENT EXPLORE");
	}
	
	// ***************** Travel Insurance - POS Student Explore ************************************** Verify Data for Share Quotation In Quotation Tracker Function for
		public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_POSStudentExplore(int rowNum)
				throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum, "POS STUDENT EXPLORE");
		}

		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_POSStudentExplore(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_POSStudentExplore("POS_StudentExplore_Quotation",
					rowNum);
		}

		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_POSStudentExplore(
				int rowNum) throws Exception {
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "POS STUDENT EXPLORE");
		}

	// ***************** Travel Insurance - Group Explore **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for
	// SuperMedclaim
	public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_GroupExplore(int rowNum)
			throws Exception {
		verifyDataInQuotationTracker("ShareQuotation", rowNum, "GROUP EXPLORE");
	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_GroupExplore(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_GroupExplore("GroupExplore_Quotation",
				rowNum);
	}

	public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_GroupExplore(
			int rowNum) throws Exception {
		verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "GROUP EXPLORE");
	}
	
	// ***************** Travel Insurance - Group Student Explore ************************************** Verify Data for Share Quotation In Quotation Tracker Function for
		public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_GroupStudentExplore(int rowNum)
				throws Exception {
			verifyDataInQuotationTracker("ShareQuotation", rowNum, "GROUP STUDENT EXPLORE");
		}

		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_GroupStudentExplore(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_GroupStudentExplore("GroupStudentExplore_Quotation",
					rowNum);
		}

		public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_GroupStudentExplore(
				int rowNum) throws Exception {
			verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "GROUP STUDENT EXPLORE");
		}
		
		// ***************** Travel Insurance - Explore **************************************
				// Verify Data for Share Quotation In Quotation Tracker Function for
				// SuperMedclaim
				public static void verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_DomesticTravel(int rowNum)
						throws Exception {
					verifyDataInQuotationTracker("ShareQuotation", rowNum, "DOMESTIC TRAVEL");
				}

				// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
				public static void verifyDataOfShareQuotationInMail_ForTravelInsurance_DomesticTravel(int rowNum) throws Exception {
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_TravelInsurance_DomesticTravel("DomesticTravel_quotation",rowNum);
				}

				public static void verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_DomesticTravel(
						int rowNum) throws Exception {
					verifyDataShouldBeNotAvailableInQuotationTracker("ShareQuotation", rowNum, 8, "DOMESTIC TRAVEL");
				}
}
