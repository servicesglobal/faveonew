package com.org.faveo.Base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseClass implements AccountnSettingsInterface{
//test
	public static String Quotationpremium_value;
	public static WebDriver driver;
	public static Properties Credential = null;
	public static String[] test;
	public static ExtentReports report;
	public static ExtentTest logger;
	public static ExtentReports extent;
	public static XSSFRow row = null;
	public static XSSFCell cell = null;
	public static File file = null;
	public static FileInputStream fileInput = null;
	public static Properties prop;
	// public static String[][] excelData=null;
	public static int colCount = 0;
	public static int rowCount;
	public static ArrayList<String> al = new ArrayList<String>();
	// public static List<String>arrl=new ArrayList<String>();
	public static String[][] excelData2 = null;
	public static String[] membres = null;
	public static String[] Diseases = null;
	public static String member=null;
	public static String TestResult;
	public static String[] addons = null;
	public static String[] Editmembres = null;


	public static String testDataPath = "Favio_Framework.xlsx";

	// Launching Browser as per the Credentials File
	public static void LaunchBrowser() throws Exception {
		String[][] TestCaseData = excel_Files("Credentials");
		Properties credential = new Properties();
		String path = System.getProperty("user.dir") + ".\\Config Files\\Credential.properties";
		System.out.println("Path is : "+path);
		FileInputStream ip = new FileInputStream(path);
		credential.load(ip);
		String Browser = TestCaseData[3][0].toString().trim();
		Credential = new Properties();
		FileInputStream ip1 = new FileInputStream(
				System.getProperty("user.dir") + "\\Config Files\\Credential.properties");
		Credential.load(ip1);
		//Verifying Browser from Credentials.properties file and Executing the same
		if(credential.get("Browser").toString().equals("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver.exe");
			driver=new FirefoxDriver();

			//System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\OR.properties");

		}

		else if (credential.get("Browser").toString().equals("chrome"))
		{
			ChromeOptions options = new ChromeOptions();
			HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
			chromeOptionsMap.put("plugins.plugins_disabled", new String[] {
					"Chrome PDF Viewer"
			});
			chromeOptionsMap.put("plugins.always_open_pdf_externally", true);
			options.setExperimentalOption("prefs", chromeOptionsMap);
			String downloadFilepath = "D:\\DownloadforPDF";
			chromeOptionsMap.put("download.default_directory", downloadFilepath);

			DesiredCapabilities ds = DesiredCapabilities.chrome();
			ds.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			ds.setCapability(ChromeOptions.CAPABILITY, options);
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver(ds);
			//Thread.sleep(20000);
		}
		else if (credential.get("Browser").toString().equals("ie"))
		{
			DesiredCapabilities ds = DesiredCapabilities.internetExplorer();
			ds.setCapability( InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		}

		driver.manage().window().maximize();
		String Environment = TestCaseData[3][1].toString().trim();
		if (Environment.contains("QC")) {
			// String BaseURLQC =
			// "https://faveoqc.religarehealthinsurance.com/Faveo/agencyportal/index.html#/auth/login";
			////String BaseURLQC = "http://10.216.9.231:8090/Faveo/agencyportal/index.html#/auth/login";
			openURL((String) Credential.get("BaseURLQC"));
		} else if (Environment.contains("UAT")) {
			//String BaseURLUAT = "https://faveouat.religarehealthinsurance.com/Faveo/agencyportal/index.html#/auth/login";
			openURL((String) Credential.get("BaseURLUAT"));
		} else if (Environment.contains("Staging")) {
			//String BaseURLStage = "https://faveostage.religarehealthinsurance.com/Faveo/agencyportal/index.html#/auth/login";
			openURL((String) Credential.get("BaseURLStage"));
		}else if(Environment.contains("CustomerPortal")) {
			String CustomorPortalUrl = "https://rhicluat.religarehealthinsurance.com/";
			openURL((String) Credential.get("CustomorPortalUrl"));
			
			System.out.println();
			
		}
	}
	/*	openURL((String) credential.get("BaseURLQC"));
		//openURL((String) credential.get("BaseURLUAT"));
		//openURL((String) credential.get("customerPortal"));

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().refresh();
		//extent.log(LogStatus.PASS,"Application launched successfully");	
	}*/


	public static void AbacusURL() throws Exception {

		Credential = new Properties();
		FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + "\\Config Files\\Credential.properties");
		Credential.load(ip);


		// Verifying Browser from Credentials.properties file and Executing the
		if (Credential.get("Browser").toString().equals("firefox")) {
			driver = new FirefoxDriver();

		} else if (Credential.get("Browser").toString().equals("chrome")) {
			DesiredCapabilities ds = DesiredCapabilities.chrome();
			ds.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver(ds);
		} else if (Credential.get("Browser").toString().equals("ie")) {
			DesiredCapabilities ds = DesiredCapabilities.internetExplorer();
			ds.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}

	
		driver.manage().window().maximize();
		openURL((String) Credential.get("BaseURLQC"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);



	}

	@BeforeTest
	public static void Report(){
		extent = new ExtentReports(System.getProperty("user.dir") + "/Reports/TestReport.html", true);
		// extent = new ExtentReports(System.getProperty("user.dir") +
		// "Reports/TestReports.html",true);
		// extent.addSystemInfo("Environment","Environment Name")
		extent.addSystemInfo("Host Name", "Faveo").addSystemInfo("Environment", "QC Enviroment")
		.addSystemInfo("User Name", "Bijaya Kumar Sahoo");
		// loading the external xml file (i.e., extent-config.xml) which was
		// placed under the base directory
		// You could find the xml file below. Create xml file in your project
		// and copy past the code mentioned below
		extent.loadConfig(new File(System.getProperty("user.dir") + "/Config Files/extent-config.xml"));

	}

	public static void Excelloop(String SheetName) throws IOException{
		//String FilePath = System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx";
		String FilePath=".\\TestData\\Favio_Framework.xlsx";
		//String FilePath="C:\\Users\\bijaya\\Desktop\\StudentExplore_TestData.xlsx";
		//String FilePath="C:\\Users\\bijaya\\Desktop\\TestDataFor_POSSuperSaver.xlsx";

		FileInputStream finputStream = new FileInputStream(new File(FilePath));
		XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
		XSSFSheet sheet = workbook.getSheet(SheetName);
		int rowCount = sheet.getLastRowNum();
		System.out.println("Total number of Row : "+rowCount);

	}

	//This method is to capture the screenshot and return the path of the screenshot.

	public static String getScreenhot(WebDriver driver, String screenshotName) throws Exception 
	{
		try{
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
		}
		catch(Exception e){
			return e.getMessage();
		}
	}


	// Implementing Report Generation
	@AfterMethod
	public static void getResult(ITestResult result) throws Exception {
		if (result.getStatus() == ITestResult.FAILURE) {

			if (result.getStatus() == ITestResult.FAILURE) {
				logger.log(LogStatus.FAIL, "Test Case is Failed " + result.getName());

				//To capture screenshot path and store the path of the screenshot in the string "screenshotPath"
				//We do pass the path captured by this mehtod in to the extent reports using "logger.addScreenCapture" method. 			
				String screenshotPath = BaseClass.getScreenhot(driver, result.getName());
				//To add it in the extent report 
				logger.log(LogStatus.FAIL, logger.addScreenCapture(screenshotPath));
			}

		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(LogStatus.SKIP, "Test Case  is Skipped" + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			//logger.log(LogStatus.PASS, "Test Case Pass is " + result.getName());
		}

		/*extent.endTest(logger);*/
		//ExtentTest logger

	}

	/*public static void openURL(String url) {
		driver.get(url);
		if (driver.getPageSource().contains("certificate") && Credential.get("Browser").toString().equals("IE")) {
			driver.navigate().to("javascript:document.getElementById('overridelink').click()");
		}
	}*/

	// Select Option Values using name
	/*public static void selecttext(String selectName, String selectText) {
		WebElement mySelectElement = driver.findElement(By.name(selectName));
		Select titleDropdown = new Select(mySelectElement);

		// titleDropdown.selectByValue(Title.toString());
		titleDropdown.selectByVisibleText(selectText.toString());
		System.out.println("Selected Dropdown Values is :" + selectText.toString());

	}*/
	public static void switchToNewTab() {
		Object[] s = driver.getWindowHandles().toArray();
		/*for(int i=0; i<s.length; i++){
			System.out.println(s[i].toString());
			driver.switchTo().window(s[1].toString());
		}*/
		driver.switchTo().window(s[1].toString());
		
	}
   
	public static void Fluentwait2(By by) {

		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec")
				.ignoring(NoSuchElementException.class);

		wait2.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	public static void scrollup()
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-250)", "");
		/*System.out.println("Scroll Up is Done");*/

	}

	public static void scrolldown(){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		/*System.out.println("Scroll Down is Done");*/
	}

	/*public static String clickElement(By by) 
	{
		waitForElement(by);
		driver.findElement(by).click();
		driver.findElement(by).getText();
		return null;
	}*/

	public static void captureScreenshot() throws IOException
	{
		Date d = new Date();
		String fileName = d.toString().replace(":", "_").replace(" ", "_")+".jpg";
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir")+"\\screenshot\\"+fileName));
	}



	public static void PayuPage_Credentials() throws Exception
	{
		String[][] carddetails=excel_Files("Credentials");
		try
		{
			clickElement(By.id("drop_image_1"));
			Thread.sleep(1000);
			clickElement(By.xpath("//small[contains(text(),'Visa | Master')]"));
		}
		catch(Exception e)
		{
			clickElement(By.xpath("//input[@id='credit-card']"));
			System.out.println("Card Selection Option is not available");
		}


		String CardNumber = carddetails[1][0].toString().trim();
		Thread.sleep(2000);
		try{
			waitForElement(By.xpath("//input[@name='ccard_number']"));
			System.out.println("Entered Credit Card Number : "+CardNumber);
			clickElement(By.id("ccard_number"));
			
			
			Thread.sleep(2000);
			clearTextfield(By.xpath("//input[@name='ccard_number']"));
			Thread.sleep(2000);
			clickbyid(By.xpath("//input[@name='ccard_number']"));
			Thread.sleep(1000);
			enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
			
			logger.log(LogStatus.PASS, "credit Card Number entered " + CardNumber);
		}
		catch(Exception e)
		{
			System.out.println("Credit Card Number is not Entered.");
		}


		//Reading Name on Card
		waitForElement(By.id("cname_on_card"));
		String NameOnCard = carddetails[1][1].toString().trim();
		Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("cname_on_card"))).sendKeys(NameOnCard);
		logger.log(LogStatus.PASS, "Name on Card Number entered " + NameOnCard);

		//Read CVV Number from Excel
		waitForElement(By.id("ccvv_number"));
		String CVVNum = carddetails[1][2].toString().trim();
		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);
		wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("ccvv_number"))).sendKeys(CVVNum);
		logger.log(LogStatus.PASS, "CVV Number on Card entered " + CVVNum);
		
		//Reading Exp Month from Excel
		String Month = carddetails[1][3].toString().trim();
		waitForElement(By.id("cexpiry_date_month"));
		clickElement(By.id("cexpiry_date_month"));
		clickElement(By.xpath("//select[@id='cexpiry_date_month']//option[contains(text()," + "'"+ Month + "'"+")]"));
		logger.log(LogStatus.PASS, "Expiray Month on Card entered " + Month);

		//Reading Exp Year from Excel
		waitForElement(By.id("cexpiry_date_year"));
		String ExpYear = carddetails[1][4].toString().trim();
		clickElement(By.xpath("//select[@id='cexpiry_date_year']"));
		clickElement(By.xpath("//select[@id='cexpiry_date_year']//option[contains(text(),"+ExpYear+")]"));
		Thread.sleep(1000);
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
		logger.log(LogStatus.PASS, "Expiray Year on Card entered " + ExpYear);
 
		//Verifying Invalid credit card number Error
		try{
			for(int i=1;i<=10;i++)
			{
				String InvalidCardError = driver.findElement(By.xpath("//label[@for='ccard_number'][contains(text(),'Invalid credit card number.')]")).getText();
				if(InvalidCardError.contains("Invalid credit card number."))
				{

					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@name='ccard_number']")).clear();
					Thread.sleep(2000);		
					clickElement(By.xpath("//input[@name='ccard_number']"));		
					Thread.sleep(2000);
					enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
					(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
					continue;	
				}
				else
				{
					System.out.println("Valid Credit Card Number.");
					break;
				}


			}
		}
		catch(Exception e){
			System.out.println("Valid Credit Card Number");
		}

		try{
			driver.findElement(By.xpath("//label[@for='ccvv_number'][contains(text(),'Invalid CVV number.')]"));
			driver.findElement(By.id("ccvv_number")).clear();
			enterText(By.id("ccvv_number"), String.valueOf(CVVNum));
			Thread.sleep(3000);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
		}catch(Exception e)
		{
			System.out.println("Valid CVV Number");
		}
		
		try{
			driver.findElement(By.xpath("//input[@value='Submit']")).click();
			logger.log(LogStatus.PASS, "Clicked on submit Button");
		}
		catch(Exception e){
			System.out.println("Payment Error Page is not being displayed");
		}
		
		Thread.sleep(2000);
		setDetailsofAxisSimulator();

		try{
			Fluentwait1(By.xpath(TransNumMessage_xpath));
			System.out.println("Transaction Number is Found.");
		}
		catch(Exception e)
		{
			try{
				Fluentwait1(By.xpath(Congratulations_xpath));
			}catch(Exception e1)
			{
				logger.log(LogStatus.PASS, "Test Case is Failed Because : PayU is down.");
			}
		}

	}


	public static void TravelPayuPage_Credentials() throws Exception
	{
		String[][] carddetails=excel_Files("Credentials");
		try
		{
			clickElement(By.xpath("//input[@id='credit-card']"));
			System.out.println("Card Selection Option is not available");


		}
		catch(Exception e)
		{
			clickElement(By.id("drop_image_1"));
			Thread.sleep(1000);
			clickElement(By.xpath("//small[contains(text(),'Visa | Master')]"));	
		}

		waitForElement(By.id("ccard_number"));
		Thread.sleep(2000);
		String CardNumber = carddetails[1][0].toString().trim();
		System.out.println("Entered Credit Card Number : "+CardNumber);
		Thread.sleep(5000);
		enterText(By.id("ccard_number"), String.valueOf(CardNumber));



		//Reading Name on Card
		waitForElement(By.id("cname_on_card"));
		String NameOnCard = carddetails[1][1].toString().trim();
		Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("cname_on_card"))).sendKeys(NameOnCard);


		//Read CVV Number from Excel
		waitForElement(By.id("ccvv_number"));
		String CVVNum = carddetails[1][2].toString().trim();
		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);
		wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("ccvv_number"))).sendKeys(CVVNum);

		//Reading Exp Month from Excel
		String Month = carddetails[1][3].toString().trim();
		waitForElement(By.id("cexpiry_date_month"));
		clickElement(By.id("cexpiry_date_month"));
		clickElement(By.xpath("//select[@id='cexpiry_date_month']//option[contains(text()," + "'"+ Month + "'"+")]"));


		//Reading Exp Year from Excel
		waitForElement(By.id("cexpiry_date_year"));
		String ExpYear = carddetails[1][4].toString().trim();
		clickElement(By.xpath("//select[@id='cexpiry_date_year']"));
		clickElement(By.xpath("//select[@id='cexpiry_date_year']//option[contains(text(),"+ExpYear+")]"));
		Thread.sleep(1000);
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();


		//Verifying Invalid credit card number Error
		try{
			for(int i=1;i<=20;i++)
			{
				String InvalidCardError = driver.findElement(By.xpath("//label[@for='ccard_number'][contains(text(),'Invalid credit card number.')]")).getText();
				if(InvalidCardError.contains("Invalid credit card number."))
				{

					Thread.sleep(4000);
					driver.findElement(By.xpath("//input[@name='ccard_number']")).clear();
					Thread.sleep(3000);		
					clickElement(By.xpath("//input[@name='ccard_number']"));		
					Thread.sleep(5000);
					enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
					(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
					continue;	
				}
				else
				{
					System.out.println("Valid Credit Card Number.");
					break;
				}


			}
		}
		catch(Exception e){
			System.out.println("Valid Credit Card Number");
		}

		try{
			driver.findElement(By.xpath("//label[@for='ccvv_number'][contains(text(),'Invalid CVV number.')]"));
			driver.findElement(By.id("ccvv_number")).clear();
			enterText(By.id("ccvv_number"), String.valueOf(CVVNum));
			Thread.sleep(3000);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
		}catch(Exception e)
		{
			System.out.println("Valid CVV Number");
		}
       
		Thread.sleep(2000);
		setDetailsofAxisSimulator();


		try{
			Fluentwait1(By.xpath(TransNumMessage_xpath));
			System.out.println("Transaction Number is Found.");
		}
		catch(Exception e)
		{
			try{
				Fluentwait1(By.xpath(TransSuccess_xpath));
			}catch(Exception e1)
			{
				logger.log(LogStatus.FAIL, "Test Case is Failed Because : PayU is down.");
			}
		}


	}



	/*public static void dropdown(By by) {
		waitForElement(by);
		WebElement element = driver.findElement(by);
		Select select = new Select(element);
		select.selectByVisibleText("");
	}

	public static void mousehover(By by) {
		waitForElement(by);
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(by)).perform();
	}

	public static void NextTab(By by)
	{
		driver.findElement(by).sendKeys(Keys.TAB);
	}

	public static void EnterData(By by) 
	{
		driver.findElement(by).click();
	}*/

	public static void findelement(By by)
	{
		driver.findElement(by);
	}

	/*public static List<String> getAllOptions(By by) {
		List<String> options = new ArrayList<String>();
		for (WebElement option : new Select(driver.findElement(by)).getOptions()) {
			String txt = option.getText();
			if (option.getAttribute("value") != "")
				options.add(option.getText());
		}
		return options;
	}
	 */
	/*public static void waitForElement(By by) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

	}

	public static void clearTextfield(By by) {
		waitForElement(by);
		driver.findElement(by).clear();
		;
	}

	public static void enterText(By by, String string) {
		waitForElement(by);
		driver.findElement(by).sendKeys(string);
	}

	public static void DragandDrop(By by) {
		waitForElement(by);
		WebElement slidebar = driver.findElement(by);

		Actions move = new Actions(driver);
		move.dragAndDropBy(slidebar, 50, 0).build().perform();
		;
		// ((Actions) action).perform();
	}*/



	/*@DataProvider(name = "Login")
	public static String[][] excel_Files(String sheetname) throws Exception {
		String[][] excelData = null;
		try {
			//String FilePath = "D:\\Test Data Faveo\\Favio_Framework.xlsx";
			String FilePath = System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(FilePath));

			XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);
			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			int rowCount = sheet.getPhysicalNumberOfRows();

			ArrayList<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			sheetNames.add(workbook.getSheetName(i));

			}

			excelData = new String[rowCount][colCount];

			for (int Nrow = 0; Nrow < rowCount; Nrow++) {

				row = sheet.getRow(Nrow);

				for (int Ncolumn = 0; Ncolumn < colCount; Ncolumn++) {

					cell = sheet.getRow(Nrow).getCell(Ncolumn);

					DataFormatter df = new DataFormatter();
					excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

				}

			}

		} catch (Exception e) {
		}
		// return null;

		return excelData;

	}
	 */

	@DataProvider(name = "Renewal")
	public static String[][] excel_File(String sheetname) throws Exception {
		String[][] excelData = null;
		try {
			//String FilePath = "D:\\Test Data Faveo\\Favio_Framework.xlsx";
			String FilePath = System.getProperty("user.dir") + "\\TestData\\Renewal_Data.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(FilePath));

			XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);
			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			 rowCount = sheet.getPhysicalNumberOfRows();

			ArrayList<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheetName(i));

			}

			excelData = new String[rowCount][colCount];

			for (int Nrow = 0; Nrow < rowCount; Nrow++) {

				row = sheet.getRow(Nrow);

				for (int Ncolumn = 0; Ncolumn < colCount; Ncolumn++) {

					cell = sheet.getRow(Nrow).getCell(Ncolumn);

					DataFormatter df = new DataFormatter();
					excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

				}

			}

		} catch (Exception e) {
		}


		return excelData;

	}


	@DataProvider(name = "BVT")
	public static String[][] excel_File1(String sheetname) throws Exception {
		String[][] excelData = null;
		try {
			//String FilePath = "D:\\Test Data Faveo\\Favio_Framework.xlsx";
			String FilePath = System.getProperty("user.dir") + "\\TestData\\BVTSheet.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(FilePath));

			XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);
			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			int rowCount = sheet.getPhysicalNumberOfRows();

			ArrayList<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheetName(i));

			}

			excelData = new String[rowCount][colCount];

			for (int Nrow = 0; Nrow < rowCount; Nrow++) {

				row = sheet.getRow(Nrow);

				for (int Ncolumn = 0; Ncolumn < colCount; Ncolumn++) {

					cell = sheet.getRow(Nrow).getCell(Ncolumn);

					DataFormatter df = new DataFormatter();
					excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

				}

			}

		} catch (Exception e) {
		}
		// return null;

		return excelData;

	}


	/*public static String[][] readExcel(String sheetName) throws Exception {

		// int i;
		excelData2 = BaseClass.excel_Files(sheetName);
		int n = excelData2.length;
		System.out.println("Excel data is:");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < excelData2[i].length; j++) {
				System.out.print(excelData2[i][j] + ", ");
			}
			System.out.println();
		}
		return excelData2;
	}*/

	public static void Thankyoupageverification()
	{
		String expectedTitle = "Your payment transaction is successful !";
		Fluentwait(By.xpath(ExpectedMessage_xpath));
		String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
		try{
			Assert.assertEquals(expectedTitle,actualTitle);
			logger.log(LogStatus.INFO, actualTitle);
		}catch(AssertionError e){
			System.out.println("Payment Failed");
			String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
			logger.log(LogStatus.FAIL, "Test Case is Fail Because getting " + FoundError);
			driver.close();
			throw e;
		}

	}



	public static void Fluentwait(By by){

		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);

		wait2.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public static void Fluentwait1(By by){

		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("Faveo Url is not loaded so unable to find Userid and Password Field.").ignoring(NoSuchElementException.class);

		wait2.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public static void ExplicitWait(By by, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
	}
	// Click functionality by Java Script
		public static void clickByJS(WebElement e1) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", e1);

			} catch (Exception e) {
				e.getMessage();
			}
		}
		public static void ImplicitWait(int Time) {
			driver.manage().timeouts().implicitlyWait(Time, TimeUnit.SECONDS);
		}
	public static void Fluentwait(By by, int Time, String Message) {

		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(Time, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).withMessage(Message).ignoring(NoSuchElementException.class);

		wait2.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	public static void ExplicitWait(By by){

		//WebElement myDynamicElement = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(by));
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));

	}

	public static void ExplicitWait1(By by){

		WebElement myDynamicElement = (new WebDriverWait(driver, 1)).until(ExpectedConditions.presenceOfElementLocated(by));
	}

	/*public static void waitForElements(By by) {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

	}

	public static boolean isValid(String s) {

		Pattern p = Pattern.compile("(0/91)?[6-9][0-9]{9}");
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}
	 */
	/*	public static boolean isInValid(String s) {

		Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}
	 */

	public static void ErroronHelathquestionnaire() throws Exception
	{
		Thread.sleep(30000);
		    waitForElement(By.xpath("//p[@class='premium_amount ng-binding']"));
			WebElement premium_text=driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']"));
			if(premium_text.isDisplayed()) {
			String ProposalPre=premium_text.getText();
			System.out.println("Premium is :"+ProposalPre);
			//String ProposalPre = (new WebDriverWait(driver, 40)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']"))).getText();
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
			}
		else {
	
			String Error_Message = driver.findElement(By.xpath("//*[@id='myModalMail_bel']/div/div/div[2]/h5[2]")).getText();
			System.out.println("Test Case is failed Because Getting Error : "+Error_Message);
			logger.log(LogStatus.PASS, "Test Case is failed Because Getting Error : " + Error_Message);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			
			TestResult = "Fail";
		}
	
	}



	public static void VerifyPremiumIncrease_on_Proposalsummarypage(String ProposalPremimPage_Value, String proposalSummarypremium_value) throws UnsupportedEncodingException
	{

		String PropPremium = ProposalPremimPage_Value;
		PropPremium = PropPremium.replace(",", "");

		String PropSum = proposalSummarypremium_value;
		PropSum = PropSum.replace(",", "");

		double a = Double.valueOf(PropPremium);
		double b = Long.valueOf(PropSum);
		double x = a+10;
		double y = a-10;





		/*Assert.assertEquals(ProposalPremimPage_Value,proposalSummarypremium_value);
			logger.log(LogStatus.PASS, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);*/

		if(a==b || (b>y && b<x))
		{
			try{
				String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
				logger.log(LogStatus.INFO, "Premium is getting Modified :"+PremiumV);		
				System.out.println("Premium is getting Modified :"+PremiumV);
			}catch(Exception e){
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);	
				System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			}
		}else {
			String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
			logger.log(LogStatus.PASS, "Test case is fail because The DOB of member to be insured is different from the age range you selected while taking quote. Hence, the total premium is revised from:"+PremiumV);		
			System.out.println("Test case is fail because :"+PremiumV);
		}


	}
	public static void VerifyPremiumIncrease_on_ProposalsummarypagePOSfreedom(String ProposalPremimPage_Value, String proposalSummarypremium_value) throws UnsupportedEncodingException
	{

		String PropPremium = ProposalPremimPage_Value;
		PropPremium = PropPremium.replace(",", "");

		String PropSum = proposalSummarypremium_value;
		PropSum = PropSum.replace(",", "");

		double a = Double.valueOf(PropPremium);
		double b = Long.valueOf(PropSum);
		double x = a+10;
		double y = a-10;





		/*Assert.assertEquals(ProposalPremimPage_Value,proposalSummarypremium_value);
			logger.log(LogStatus.PASS, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);*/

		if(a==b || (b>y && b<x))
		{
			try{
				String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
				logger.log(LogStatus.INFO, "Premium is getting Modified :"+PremiumV);		
				System.out.println("Premium is getting Modified :"+PremiumV);
			}catch(Exception e){
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);	
				System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			}
		}else {
			String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
			logger.log(LogStatus.PASS, "Test case is fail because The DOB of member to be insured is different from the age range you selected while taking quote. Hence, the total premium is revised from:"+PremiumV);		
			System.out.println("Test case is fail because :"+PremiumV);
		}


	}
	public static void VerifyPremiumIncrease_on_Proposalsummarypage_afterEdit(String afterEdit_Proposalpremium_value, String proposalSummarypremium_value) throws UnsupportedEncodingException
	{

		String PropPremium = afterEdit_Proposalpremium_value;
		PropPremium = PropPremium.replaceAll("\\W","");
System.out.println(PropPremium);
		String PropSum = proposalSummarypremium_value;
		PropSum = PropSum.replace("," , "");
		
		double a = Double.valueOf(PropPremium);
		double b = Long.valueOf(PropSum);
		
		//double b = Double.parseDouble(PropSum);
		double x = a+10;
		double y = a-10;





		/*Assert.assertEquals(ProposalPremimPage_Value,proposalSummarypremium_value);
			logger.log(LogStatus.PASS, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);*/

		if(a==b || (b>y && b<x))
		{
			try{
				String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
				logger.log(LogStatus.INFO, "Premium is getting Modified :"+PremiumV);		
				System.out.println("Premium is getting Modified :"+PremiumV);
			}catch(Exception e){
				logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);	
				System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
			}
		}else {
			String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
			//logger.log(LogStatus.FAIL, "Test case is fail because premium mismathed:"+PremiumV);		
			System.out.println("Test case is fail because "+PremiumV);
		}


	}


	public static void HelathQuestionnairecheckbox(){
		Fluentwait(By.id(Term1checkbox_id));
		clickElement(By.id(Term1checkbox_id));
		clickElement(By.id(Term3checkbox_id));
		clickElement(By.id(Term2checkbox_id));
		clickElement(By.id(alertcheckbox_id));
	}

	public static void clickbyid(By by)
	{

		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(by)).click().build().perform();

	}

	public static String GetText(By by)
	{
		driver.findElement(by).getText();
		return null;
	}

	public static void clickbyHover(By by)
	{

		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(by)).click().build().perform();

	}

	public static void DBVerification(String PayuProposalNum) throws SQLException, ClassNotFoundException
	{
		DbManager.setDbConnection();
		List<String> DBValues = DbManager.getSqlQuery("Select policynum,proposalnum,PROCESSSTATUSCD from policy Where proposalnum ='"+ PayuProposalNum + "'");
		System.out.println(DBValues);

		logger.log(LogStatus.INFO,"Proposal Number/Policy Num and Premium is Verified in DB : " + DBValues);

	}

	public static void pdfmatchcode(String FinalAmount,String PayuProposalNum,String proposalSummarypremium_value) throws InterruptedException
	{
		waitForElement(By.xpath(PolProp_xpath));
		String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
		String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
		System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
		if (ProposalSummary.contains("Policy No.")) {
			Thread.sleep(15000);
			clickElement(By.xpath(Downloadpdf_Thankyou_xpath));
			Thread.sleep(25000);
			try{

				String UserName = System.getProperty("user.home");
				System.out.println(UserName);

				File file = new File(UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");

				FileInputStream input = new FileInputStream(file);

				PDFParser parser = new PDFParser(input);

				parser.parse();

				COSDocument cosDoc = parser.getDocument();

				PDDocument pdDoc = new PDDocument(cosDoc);

				PDFTextStripper strip = new PDFTextStripper();

				strip.setStartPage(1);
				strip.setEndPage(2);

				String data = strip.getText(pdDoc);

				//	System.out.println(data);

				Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
				Assert.assertTrue(data.contains(FinalAmount));
				//Assert.assertTrue(data.contains(ProposerName));

				cosDoc.close();
				pdDoc.close();
				System.out.println("Text Found on the pdf File...");
				logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
				TestResult = "Pass";
			} catch (Exception e) {

				String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
				System.out.println(ErrorMessage);
				logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
				TestResult = "Fail";
			}


		} else if (ProposalSummary.contains("Application No.")) {
			System.out.println(ThankyoupageProposal_Pol_num);
			try {
				Assert.assertEquals(PayuProposalNum, ThankyoupageProposal_Pol_num);
				TestResult = "Pass";
				logger.log(LogStatus.INFO,"Proposal number on Payu page and Thankyour Page is Verified and Both are Same");
			} catch (AssertionError e) {
				System.out.println(proposalSummarypremium_value + " - failed");
				TestResult = "Fail";
				logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same");
				throw e;
			}
		}

	}

	public static void Renewalpdfmatchcode(String FinalAmount,String PayuProposalNum,String ProposalSum_Premium,int n) throws Exception
	{
		BaseClass.scrolldown();
		String ThankyoupageProposal_Pol_num=null;
		try{
			driver.findElement(By.xpath("//th[@align='left'][contains(text(),'Policy No.')]"));
			ThankyoupageProposal_Pol_num = driver.findElement(By.xpath("//html//section[@id='succ_case1']//div[@class='applicationNoTable applicationTablenone']//td[1]")).getText();
			Thread.sleep(15000);
			clickElement(By.xpath(Download_Renewal_pdf));
			Thread.sleep(25000);
			try{

				String UserName = System.getProperty("user.home");
				System.out.println(UserName);

				File file = new File(UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");

				FileInputStream input = new FileInputStream(file);

				PDFParser parser = new PDFParser(input);

				parser.parse();

				COSDocument cosDoc = parser.getDocument();

				PDDocument pdDoc = new PDDocument(cosDoc);

				PDFTextStripper strip = new PDFTextStripper();

				strip.setStartPage(1);
				strip.setEndPage(3);

				String data = strip.getText(pdDoc);



				Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
				Assert.assertTrue(data.contains(FinalAmount));
				//Assert.assertTrue(data.contains(ProposerName));

				cosDoc.close();
				pdDoc.close();
				System.out.println("Text Found on the pdf File...");
				logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
				TestResult = "Pass";
			} catch (Exception e) 

			{

				String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
				System.out.println(ErrorMessage);
				logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
				logger.log(LogStatus.FAIL, e);
				TestResult = "Fail";
			} 
		}catch(Exception e)
		{
			driver.findElement(By.xpath("//th[@align='left'][contains(text(),'Proposal No.')]"));
			ThankyoupageProposal_Pol_num = driver.findElement(By.xpath("//html//section[@id='succ_case2']//div[@class='applicationNoTable applicationTablenone']//td[1]")).getText();
			System.out.println("Thankyou Page Proposal Number : "+ThankyoupageProposal_Pol_num);
			try {
				Assert.assertEquals(PayuProposalNum, ThankyoupageProposal_Pol_num);
				TestResult = "Pass";
				logger.log(LogStatus.INFO,"Proposal number on Payu page and Thankyour Page is Verified and Both are Same");
			} catch (AssertionError e1) {
				System.out.println(ProposalSum_Premium + " - failed");
				TestResult = "Fail";
				logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same");
				throw e;
			}
		}
		WriteExcel.setCellData1("Test_Cases_Renewal", TestResult, ThankyoupageProposal_Pol_num, n, 2, 3);

	}

	public static void ThankyouPagePremiumMatch(String ProposalSum_Premium,String ThankyoupagePremium)
	{
		try {
			Assert.assertEquals(ProposalSum_Premium, ThankyoupagePremium);
			System.out.println("Renewal page premium and Thankyou premium is Verified and Both are Same : "+ ThankyoupagePremium);
			logger.log(LogStatus.INFO,"Renewal page premium and Thankyou premium is Verified and Both are Same : "+ ThankyoupagePremium);
		} catch (AssertionError e) {
			System.out.println("Renewal page premium and Thankyou premium are not Same :"+ProposalSum_Premium );
			logger.log(LogStatus.FAIL, "Renewal page premium and Thankyou premium are not Same.");

		}


	}

	@DataProvider(name = "Login")
	public static String[][] excel_Files1(String sheetname) throws Exception {
		String[][] excelData = null;
		try {

			String FilePath=".\\TestData\\Favio_Framework.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(FilePath));

			XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);

			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			int rowCount = sheet.getPhysicalNumberOfRows();

			ArrayList<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheetName(i));

			}

			excelData = new String[rowCount][colCount];

			for (int Nrow = 0; Nrow < rowCount; Nrow++) {

				row = sheet.getRow(Nrow);

				for (int Ncolumn = 0; Ncolumn < colCount; Ncolumn++) {

					cell = sheet.getRow(Nrow).getCell(Ncolumn);

					DataFormatter df = new DataFormatter();
					excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

				}

			}

		} catch (Exception e) {
		}
		// return null;

		return excelData;

	}

	public static void RenewalPayuPage_Credentials() throws Exception
	{
		String[][] carddetails=excel_Files("Credentials");
		try
		{
			clickElement(By.id("drop_image_1"));
			Thread.sleep(1000);
			clickElement(By.xpath("//small[contains(text(),'Visa | Master')]"));
		}
		catch(Exception e)
		{
			clickElement(By.xpath("//input[@id='credit-card']"));
			System.out.println("Card Selection Option is not available");
		}


		String CardNumber = carddetails[1][0].toString().trim();
		Thread.sleep(2000);
		try{
			waitForElement(By.xpath("//input[@name='ccard_number']"));
			System.out.println("Entered Credit Card Number : "+CardNumber);
			clickElement(By.id("ccard_number"));
			Thread.sleep(2000);
			clearTextfield(By.xpath("//input[@name='ccard_number']"));
			Thread.sleep(2000);
			clickbyid(By.xpath("//input[@name='ccard_number']"));
			Thread.sleep(1000);
			enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
		}
		catch(Exception e)
		{
			System.out.println("Credit Card Number is not Entered.");
		}


		//Reading Name on Card
		waitForElement(By.id("cname_on_card"));
		String NameOnCard = carddetails[1][1].toString().trim();
		Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);
		wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("cname_on_card"))).sendKeys(NameOnCard);


		//Read CVV Number from Excel
		waitForElement(By.id("ccvv_number"));
		String CVVNum = carddetails[1][2].toString().trim();
		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage("User defined timeout after 50 sec").ignoring(NoSuchElementException.class);
		wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("ccvv_number"))).sendKeys(CVVNum);

		//Reading Exp Month from Excel
		String Month = carddetails[1][3].toString().trim();
		waitForElement(By.id("cexpiry_date_month"));
		clickElement(By.id("cexpiry_date_month"));
		clickElement(By.xpath("//select[@id='cexpiry_date_month']//option[contains(text()," + "'"+ Month + "'"+")]"));


		//Reading Exp Year from Excel
		waitForElement(By.id("cexpiry_date_year"));
		String ExpYear = carddetails[1][4].toString().trim();
		clickElement(By.xpath("//select[@id='cexpiry_date_year']"));
		clickElement(By.xpath("//select[@id='cexpiry_date_year']//option[contains(text(),"+ExpYear+")]"));
		Thread.sleep(1000);
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();


		//Verifying Invalid credit card number Error
		try{
			for(int i=1;i<=20;i++)
			{
				String InvalidCardError = driver.findElement(By.xpath("//label[@for='ccard_number'][contains(text(),'Invalid credit card number.')]")).getText();
				if(InvalidCardError.contains("Invalid credit card number."))
				{

					Thread.sleep(4000);
					driver.findElement(By.xpath("//input[@name='ccard_number']")).clear();
					Thread.sleep(3000);		
					clickElement(By.xpath("//input[@name='ccard_number']"));		
					Thread.sleep(5000);
					enterText(By.xpath("//input[@name='ccard_number']"), String.valueOf(CardNumber));
					(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
					continue;	
				}
				else
				{
					System.out.println("Valid Credit Card Number.");
					break;
				}


			}
		}
		catch(Exception e){
			System.out.println("Valid Credit Card Number");
		}

		try{
			driver.findElement(By.xpath("//label[@for='ccvv_number'][contains(text(),'Invalid CVV number.')]"));
			driver.findElement(By.id("ccvv_number")).clear();
			enterText(By.id("ccvv_number"), String.valueOf(CVVNum));
			Thread.sleep(3000);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='credit']//div[@class='payment-buttons append-bottom']//input[@id='pay_button']"))).click();
		}catch(Exception e)
		{
			System.out.println("Valid CVV Number");
		}

		try{
			clickElement(By.xpath(Transaction_Refrence_num_Xpath));
			System.out.println("Transaction Number is Found.");
		}
		catch(Exception e)
		{
			try{
				Fluentwait1(By.xpath(Cong_xpath));
			}catch(Exception e1)
			{
				logger.log(LogStatus.FAIL, "Test Case is Failed Because : PayU is down.");
			}
		}

	}
	@AfterSuite
	public void tearDown() {
		extent.flush();
	}
	public static void openURL(String url) {
		driver.get(url);
		if (driver.getPageSource().contains("certificate")
				&& Credential.get("Browser").toString().equals("IE")) {
			driver.navigate().to("javascript:document.getElementById('overridelink').click()");
		}
	}

	//Select Option Values using name
	public static void selecttext(String selectName, String selectText) throws InterruptedException {
		WebElement mySelectElement = driver.findElement(By.name(selectName));
		Select titleDropdown= new Select(mySelectElement);
		Thread.sleep(2000);
		//titleDropdown.selectByValue(Title.toString());
		
		titleDropdown.selectByVisibleText(selectText.toString());
		System.out.println("Selected Tilte is :"+ selectText.toString());

	}
	public static void rowandcolumn(By by) throws InterruptedException {
		List <WebElement> Tabledate_row=driver.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tr"));
		List <WebElement> Tabledate_col=driver.findElements(By.xpath("//table[contains(@class,'proposal_table_container')]//tr[1]//td"));
		int rows_Size=Tabledate_row.size();
		int cols_size=Tabledate_col.size();
		System.out.println("Total number of rows is :"  +rows_Size);
		System.out.println("Total number of cols is  :"  +cols_size);
		Thread.sleep(8000);
	}

	public static void NextTab(By by)
	{
		driver.findElement(by).sendKeys(Keys.ESCAPE);//TAB
	}

	public static String clickElement(By by) {
		waitForElement(by);
		driver.findElement(by).click();
		return null;

	}
	public static void  dropdown(By by) {
		waitForElement(by);

		WebElement element = driver.findElement(by);
		Select select = new Select(element);
		select.selectByVisibleText("");

	}
	public static void mousehover(By by) {
		waitForElement(by);
		Actions action=new Actions(driver);
		action.moveToElement(driver.findElement(by)).perform();

	}
	public static void EnterData(By by) {
		driver.findElement(by).click();
	}
	public static List<String> getAllOptions(By by) {
		List<String> options = new ArrayList<String>();
		for (WebElement option : new Select(driver.findElement(by)).getOptions()) {
			String txt = option.getText();
			if (option.getAttribute("value") != "") options.add(option.getText());
		}
		return options;
	}
	public static void waitForElement(By by) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

	}
	
	public static void waitTillElementToBeClickable(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(by));

	}
	
	public static void waitTillElementVisible(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

	}

	public static void clearTextfield(By by) {
		waitForElement(by);
		driver.findElement(by).clear();
		;
	}

	public static void enterText(By by, String string) {
		waitForElement(by);
		driver.findElement(by).sendKeys(string);
		//driver.findElement(by).sendKeys(Keys.ESCAPE);

	}
	public static void DragandDrop(By by) {
		waitForElement(by);
		WebElement slidebar=driver.findElement(by);

		Actions move = new Actions(driver);
		move.dragAndDropBy(slidebar, 50, 0).build().perform();;
		// ((Actions) action).perform();

	}

	/*	public static void enterText1(By by,String password) {
		waitForElement(by);
		driver.findElement(by).sendKeys(password);
	}*/

	/*	public static void ReadExcel() throws IOException {


		String FilePath = "C:\\Users\\bijaya\\Documents\\Book2.xlsx";
		FileInputStream finputStream = new FileInputStream(new File(FilePath));

		XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

		System.out.println("Columns"+ colCount);

		int rowCount = sheet.getPhysicalNumberOfRows();

		System.out.println("Rows"+ rowCount);

		String[][] excelData = new String[rowCount][colCount];

		for(int Nrow = 0; Nrow<rowCount; Nrow++) {

			row = sheet.getRow(Nrow);

			for(int Ncolumn =0; Ncolumn<colCount ; Ncolumn++) {

				cell = sheet.getRow(Nrow).getCell(Ncolumn);

				DataFormatter df = new DataFormatter();
				excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

System.out.println("values are: " +excelData[Nrow][Ncolumn].toString());

				  driver.findElement(By.xpath("//input[@name='userId']")).sendKeys(cell.getStringCellValue());
				  driver.findElement(By.xpath("//input[@type='password']")).sendKeys(cell.getStringCellValue());
				  driver.findElement(By.xpath("//button[@type='submit']")).click();
			             }
			             else
			            	 driver.close();

			 // driver.findElement(By.xpath("//button[@type='submit']")).click();
			         }



		 }
	}*/

	/*public static Properties readProperties() throws IOException
	{
		file = new File(System.getProperty("C:\\Users\\bijaya\\Documents\\Book2.xlsx"));
		FileInputStream fileInput = null;
			fileInput = new FileInputStream(file);
			prop = new Properties();
			prop.load(fileInput);


	return prop;
	}*/

	/*public static String username(String sheetname, int i, int j) throws IOException{
		String username=null;
		Cell cell1=readExcel(sheetname).getRow(i).getCell(j);

		if(cell1.getCellType()==Cell.CELL_TYPE_STRING){
			username=cell1.getStringCellValue();	
		}
		else if (cell1.getCellType()==Cell.CELL_TYPE_NUMERIC){
			 username=String.valueOf((long) cell1.getNumericCellValue());
		}
		return username;

	}*/


	/*public static Properties readingdata(int username, int password) throws Exception 
	{
		String FilePath = "C:\\Users\\bijaya\\Documents\\Book2.xlsx";
		FileInputStream finputStream = new FileInputStream(new File(FilePath));

		XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		 password = sheet.getRow(0).getPhysicalNumberOfCells();

		System.out.println("Columns"+ password);

		 username = sheet.getPhysicalNumberOfRows();

		System.out.println("Rows"+ username);

		String[][] excelData = new String[username][password];

		for(int Nrow = 0; Nrow<username; Nrow++) {

			row = sheet.getRow(Nrow);

			for(int Ncolumn =0; Ncolumn<password ; Ncolumn++) {

				cell = sheet.getRow(Nrow).getCell(Ncolumn);

				DataFormatter df = new DataFormatter();
				excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

System.out.println("values are: " +excelData[Nrow][Ncolumn].toString());
//return excelData[Nrow][Ncolumn];
	}
		}
		return prop;
		}
	public static void readExcel(String sheetName) {

	}


	public static void type(WebElement textbox, String inputdata) throws Exception {
		Thread.sleep(1000);
		for (int i = 0; i <= 2; i++) {
			try {
				textbox.clear();
				textbox.sendKeys(inputdata);
				break;

			} catch (Exception e) {
				if (i == 2) {
					throw e;

				} else {
					Thread.sleep(1000);
				}
			}
		}
	}
	 */
	@DataProvider(name="Login")
	public static String[][] excel_Files(String sheetname) throws Exception {
		

		String[][] excelData = null;
		try {

			//String FilePath = "D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx";
			//String FilePath = ".\\TestData\\Favio_Framework_Update.xlsx";
			String FilePath=".\\TestData\\Favio_Framework.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(FilePath));

			/*@SuppressWarnings("resource")*/
			XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);

			 colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			 rowCount = sheet.getPhysicalNumberOfRows();

			ArrayList<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheetName(i));

			}

			excelData = new String[rowCount][colCount];

			for (int Nrow = 0; Nrow < rowCount; Nrow++) {

				row = sheet.getRow(Nrow);

				for (int Ncolumn = 0; Ncolumn < colCount; Ncolumn++) {

					cell = sheet.getRow(Nrow).getCell(Ncolumn);

					DataFormatter df = new DataFormatter();
					excelData[Nrow][Ncolumn] = df.formatCellValue(cell);

				}

			}

		} catch (Exception e) {
			
		}
		
		return excelData;

	
		
		/*
		String[][]  excelData = null;
		try {
			String FilePath = ".\\TestData\\Favio_Framework.xlsx";
			//String FilePath = "F:\\New_WorkSpace\\FaveoUIAutomation\\Favio_Framework_Update.xlsx";
			//String FilePath = ".\\TestData\\Favio_Framework_Update.xlsx";
			//String FilePath = "C:\\Users\\bijaya\\Desktop\\StudentExplore_TestData.xlsx";
			//String FilePath = "C:\\Users\\bijaya\\Desktop\\TestDataFor_POSSuperSaver.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(FilePath));
            XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);
			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();
			int excelrows = 0;
			System.out.println("Columns"+ colCount);
			if(!sheet.toString().equals("Sheet1")) {
			rowCount = sheet.getPhysicalNumberOfRows();
			excelrows=rowCount;
			}else {
			excelrows=sheet.getPhysicalNumberOfRows();
			}
			System.out.println("Rows"+ excelrows);

			List<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add( workbook.getSheetName(i) ); 

			}

			excelData = new String[rowCount][colCount];

			for(int Nrow = 0; Nrow<excelrows; Nrow++) {

				row = sheet.getRow(Nrow);

				for(int Ncolumn =0; Ncolumn<colCount ; Ncolumn++) {

					cell = sheet.getRow(Nrow).getCell(Ncolumn);


					DataFormatter df = new DataFormatter();
					excelData[Nrow][Ncolumn] = df.formatCellValue(cell);



				}

			}


		}catch(Exception e) {}
		//return null;

		return excelData;

	*/}



	//read excel[
	public static String[][] readExcel(String sheetName) throws Exception {

		//int i;
		excelData2 = BaseClass.excel_Files(sheetName);
		int n=excelData2.length;
		System.out.println("Excel data is:");
		for(int i=0; i<n;i++)
		{
			for(int j=0; j<excelData2[i].length;j++)
			{
				System.out.print(excelData2[i][j] + ", ");
			}
			System.out.println();
		}
		return excelData2;
	}

	public static void waitForElements(By by) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

	}
	public static boolean isValid(String s)
	{

		Pattern p = Pattern.compile("(0/91)?[6-9][0-9]{9}");
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}	          

	public static boolean isInValid(String s)
	{

		Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}	          

	// Point To Element
	public static void pointToElement(String e1, WebDriver driver){
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", e1);



	}
	//Function For Read Data from Properties File
	
		public static File file1 = null;
		
		public static Properties prop1;
			
			public static Properties readProperties()
			{
				file1 = new File(System.getProperty("user.dir") + ".\\Config Files\\Credential.properties");
				FileInputStream fileInput = null;

				try {
					fileInput = new FileInputStream(file1);
					prop1 = new Properties();
					prop1.load(fileInput);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				catch (IOException e1) {
					System.out.println(e1.getMessage());
				}
			
			return prop1;
			}
			public static void pointToElement(WebElement e1) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", e1);
			}
			
			
			public static void VerifyPremiumIncrease_on_ProposalsummarypageCareHeart(String ProposalPremimPage_Value, String proposalSummarypremium_value) {


				

				String PropPremium = ProposalPremimPage_Value;
				PropPremium = PropPremium.replaceAll("\\W","");
		System.out.println(PropPremium);
				String PropSum = proposalSummarypremium_value;
				PropSum = PropSum.replace("," , "");
				
				double a = Double.valueOf(PropPremium);
				double b = Long.valueOf(PropSum);
				
				//double b = Double.parseDouble(PropSum);
				double x = a+10;
				double y = a-10;
				/*Assert.assertEquals(ProposalPremimPage_Value,proposalSummarypremium_value);
					logger.log(LogStatus.PASS, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
					System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);*/

				if(a==b || (b>y && b<x))
				{
					try{
						String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
						logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
						logger.log(LogStatus.INFO, "Premium is getting Modified :"+PremiumV);		
						System.out.println("Premium is getting Modified :"+PremiumV);
					}catch(Exception e){
						logger.log(LogStatus.INFO, "Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);	
						System.out.println("Proposal Page Premium and Proposal Summary Page Premium is Verified and Both are Same i.e : "+proposalSummarypremium_value);
					}
				}else {
					String PremiumV = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
					logger.log(LogStatus.PASS, "Test case is fail because The DOB of member to be insured is different from the age range you selected while taking quote. Hence, the total premium is revised from:"+PremiumV);		
					System.out.println("Test case is fail because :"+PremiumV);
				}


			
			}
			
			public static void setDetailsofAxisSimulator() throws Exception{
				Thread.sleep(2000);
				/*List<String> list=new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(list.get(2));*/
				Thread.sleep(3000);
				driver.findElement(By.xpath("//input[@id='password']")).sendKeys("123456");
				Thread.sleep(2000);
				driver.findElement(By.xpath("//input[@id='submitBtn']")).click();
			}
}
