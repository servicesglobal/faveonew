package com.org.faveo.Base;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.relevantcodes.extentreports.LogStatus;

public class ReadDataFromEmail extends BaseClass implements AccountnSettingsInterface {

	// ********* Mail Verification Functions
	// *************************************************
	public static void openEmail() throws Exception {
		try{
		String[][] TestCaseData = BaseClass.excel_Files("Credentials");
		
		Properties prop = readProperties();
		String url = prop.getProperty("MailUrl");
		// System.out.println("Property Value:"+ str);
		driver.get(url);
		System.out.println("Open Mail Id");
		logger.log(LogStatus.PASS, "Open Mail Id");
		
		for(int i=0; i<=3; i++){
		
        try{
		//String userName = prop.getProperty("mailUserName");
		String userName = TestCaseData[1][5].toString().trim();
		//waitForElement(By.xpath(userName_Xpath));
		waitForElement(By.xpath(userName_Xpath));
		enterText(By.xpath(userName_Xpath), userName);
		System.out.println("Data Entered for Mail User Name:  " + userName);
		logger.log(LogStatus.PASS, "Data Entered for Mail User Name:  " + userName);
		break;
        }
        catch(Exception e){
        	driver.navigate().refresh();
        }
		}

		//String Password = prop.getProperty("mailPassword");
		String Password = TestCaseData[1][6].toString().trim();
		waitForElement(By.xpath(password_Xpath));
		enterText(By.xpath(password_Xpath), Password);
		System.out.println("Data Entered for Mail Password:  " + Password);
		logger.log(LogStatus.PASS, "Data Entered for Mail Password:  " + Password);

		waitForElement(By.xpath(signIN_Xpath));
		clickElement(By.xpath(signIN_Xpath));
		System.out.println("Clicked on Sign-In button");
		logger.log(LogStatus.PASS, "Clicked on Sign-In button");

		waitForElement(By.xpath(shareQuotationFolder_Xpath));
		clickElement(By.xpath(shareQuotationFolder_Xpath));

		System.out.println("Moved to Shared Proposal Email Folder");
		logger.log(LogStatus.PASS, "Moved to Shared Proposal Email Folder");
		}
		catch(Exception e){
			throw e;
		}

	}

	// Delete Button in Email
	public static void clickOndeleteButtonInEmail() {
		/*waitForElement(By.xpath(deleteButton_In_Mail_Xpath));
		clickElement(By.xpath(deleteButton_In_Mail_Xpath));*/
		try{
	    Thread.sleep(10000);
		if(driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).isDisplayed());
		{
		driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).click();
		System.out.println("Clicked on Delete Button");
		logger.log(LogStatus.PASS, "Clicked on Delete Button");
		}
		}
		catch(Exception e){
			System.out.println("No Need to Delete any Email");
		}	

	}
	
	// Delete Button in Email
		public static void clickOndeleteButtonInEmail2() {
			/*waitForElement(By.xpath(deleteButton_In_Mail_Xpath));
			clickElement(By.xpath(deleteButton_In_Mail_Xpath));*/
			try{
		    Thread.sleep(10000);
			if(driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).isDisplayed());
			{
			List<WebElement> list=driver.findElements(By.xpath("//div[@class='_lv_m1']//button[@autoid='_lv_4']"));
			System.out.println(list.size());
			for(int i=0; i<list.size(); i++){
			Thread.sleep(4000);
			driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).click();
			System.out.println("Clicked on Delete Button");
			logger.log(LogStatus.PASS, "Clicked on Delete Button");
			}
			}
			}
			catch(Exception e){
				System.out.println("No Need to Delete any Email");
			}	

		}


	public static void openAndDeleteOldEmail() throws Exception {
		openEmail();
		clickOndeleteButtonInEmail2();
		

	}
		
	// Proposer Name
	public static void readAndVerifyPropserName(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name = TestCaseData[Rownum][2].toString().trim();
		WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
		String propserName = e1.getText();
		if (propserName.contains(Name)) {
			System.out.println("Propser Name Verified: " + Name);
		} else {
			Assert.fail("Propser Name is not available inside Email Body: " + propserName);
		}
	}
			
	// Proposer Name
	public static void readAndVerifyPropserName_ForSharePorposal(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name = TestCaseData[Rownum][2].toString().trim();
		String[] Name1=Name.split(" ");
		System.out.println(Name1[0]+ Name1[1]);
		WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
		String propserName = e1.getText();
		
		for(int i=0; i<=Name1.length; i++){
			if (propserName.contains(Name1[i])) {
				break;
			} else {
				Assert.fail("Propser Name is not available inside Email Body: " + propserName);
			}
		}
		System.out.println("Propser Name Verified: " + Name);
	}

	// Total Number of Members
	public static void readAndVerifyTotalNumberOfMemebers(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
		Thread.sleep(2000);
		WebElement e1 = driver.findElement(By.xpath(totalNumberOfMembers_In_MailBody_Xpath));
		String numOfMember = e1.getText();
		if (numOfMember.contains(TotalMember)) {
			System.out.println("Total Number of Members are Verified: " + TotalMember);
		} else {
			Assert.fail("Total Number of Members are not Verified in Email Body: " + numOfMember);
		}
	}
			
	// Sum Insured
	public static void readAndVerifySumInsured(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String sumInsuredValue = (TestCaseData[Rownum][Colnum].toString().trim());

		WebElement e1 = driver.findElement(By.xpath(sumInsured_In_MailBody_Xpath));
		String sumInsured = e1.getText();
		if (sumInsured.contains(sumInsuredValue)) {
			System.out.println("Sum Insured is Verified in Email Body: " + sumInsuredValue);
		} else {
			Assert.fail("Sum Insured is not Verified in Email Body: " + sumInsured);
		}
	}
	
	
			
	// Premium
	public static void readAndVerifyPremium() throws Exception {
		// String premiumValue=AddonsforProducts.ExpectedpremiumAmount;
		// String premiumValue="831";
		String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured1 = e1.getText();
		String sumInsured = sumInsured1.replaceAll("\\W", "");

		String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
		// System.out.println(str);

		if (premiumValue.contains(sumInsured2)) {
			System.out.println("Total Premium is Verified: " + sumInsured2);
		} else {
			Assert.fail("Total Premium is not Verified: " + premiumValue);
		}
	}
	
	
			
	// Tenure
	public static void readAndVerifyTenure(String SheetName, int Rownum, int ColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TenureValue = (TestCaseData[Rownum][ColNum].toString().trim());

		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured = e1.getText();

		String[] str = sumInsured.split(" ");
		String Num = str[2];
		String year = str[3];
		String tenure = Num.concat(" ");
		String tenure1 = tenure.concat(year);
		System.out.println(tenure1);

		if (TenureValue.equalsIgnoreCase(tenure1)) {
			System.out.println("Tenure is Verified: " + TenureValue);
		} else {
			Assert.fail("Tenure is not Verified: " + tenure1);
		}
	}

	//This code for click on Infinite Times
	/*// Function for Click on Refresh Button Until New Email Not Displayed
	public static void clickOnRefreshButtonUntilNewemailNotDisplayed() throws Exception {
		boolean flag = true;
      
		while (flag) {
			
			try {
				if (driver.findElement(By.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']")).isDisplayed()) {
					System.out.println("Pass");
					break;
				}
			} catch (Exception e) {
				Thread.sleep(1000);
				driver.navigate().refresh();
				waitForElement(By.xpath(shareQuotationFolder_Xpath));
				clickElement(By.xpath(shareQuotationFolder_Xpath));
			}
		}
	}*/
	
	//This code wait till 5 times means then it will break the loop
	// Function for Click on Refresh Button Until New Email Not Displayed
	public static void clickOnRefreshButtonUntilNewemailNotDisplayed() throws Exception {
		boolean flag = true;
      l1:
		while (flag) {
			
			
			for(int i=0; i<=10; i++){
			Thread.sleep(8000);
			try {
				/*if (driver.findElement(By.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']")).isDisplayed()) {*/
				if(driver.findElement(By.xpath("//div[@class='_lv_m1']//div[@class='_lv_C _lv_D']/span[text()='Religare Health Insurance'][1]")).isDisplayed()){	
				System.out.println("Email is being Displayed");
					logger.log(LogStatus.PASS, "Email is being Displayed");
					break l1;
				}
			} catch (Exception e) {
				if(i==10){
					System.out.println("Email is not Displayed");
					logger.log(LogStatus.FAIL, "Email is not Displayed");
					break l1;
				}
				else{
					driver.navigate().refresh();
					//waitForElement(By.xpath(shareQuotationFolder_Xpath));
					//clickElement(By.xpath(shareQuotationFolder_Xpath));
					waitForElement(By.xpath(shareQuotationFolder_Xpath));
					clickElement(By.xpath(shareQuotationFolder_Xpath));
					System.out.println("Email is not Desplayed for time: "+ i);
					
				}
			}
		}
		}
	}

	public static void readAndVerifyAgeGroupMembers2(String SheetName, int RowNum, int ColNumOfNumOfMembers,
			int ColNumOfAgeGroup1, int ColNumOfAgeGroup2, int ColNumOfAgeGroup3, int ColNumOfAgeGroup4,
			int ColNumOfAgeGroup5, int ColNumOfAgeGroup6) throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String NoOfMembers = (TestCaseData[RowNum][ColNumOfNumOfMembers].toString().trim());
		String AgeGroupOfMemeber1 = (TestCaseData[RowNum][ColNumOfAgeGroup1].toString().trim());
		String AgeGroupOfMemeber2 = (TestCaseData[RowNum][ColNumOfAgeGroup2].toString().trim());
		String AgeGroupOfMemeber3 = (TestCaseData[RowNum][ColNumOfAgeGroup3].toString().trim());
		String AgeGroupOfMemeber4 = (TestCaseData[RowNum][ColNumOfAgeGroup4].toString().trim());
		String AgeGroupOfMemeber5 = (TestCaseData[RowNum][ColNumOfAgeGroup5].toString().trim());
		String AgeGroupOfMemeber6 = (TestCaseData[RowNum][ColNumOfAgeGroup6].toString().trim());

		String str = driver.findElement(By.xpath(groupMembers_In_MailBody_Xpath)).getText();
		String[] agegroup = str.split(",");

		l1: for (String str1 : agegroup) {
			System.out.println(str1);

			String AgeGroupOfMemeber1_Mail = str1;
			String AgeGroupOfMemeber2_Mail = str1;
			String AgeGroupOfMemeber3_Mail = str1;
			String AgeGroupOfMemeber4_Mail = str1;
			String AgeGroupOfMemeber5_Mail = str1;
			String AgeGroupOfMemeber6_Mail = str1;
            
			switch (NoOfMembers) {
			case "1":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)) {
					System.out.println("AgeGroup is verified for Member 1: " + AgeGroupOfMemeber1_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "2":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)) {
					System.out.println("AgeGroup is verified for Member 1 and 2: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "3":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2 and 3: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "4":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3 and 4: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " " + AgeGroupOfMemeber4_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not verified");
				}
				break;
			case "5":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
						&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3, 4 and 5: " + AgeGroupOfMemeber1_Mail
							+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
							+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "6":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
						&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)
						&& AgeGroupOfMemeber6.equalsIgnoreCase(AgeGroupOfMemeber6_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3, 4, 5 and 6: " + AgeGroupOfMemeber1_Mail
							+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
							+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail + " " + AgeGroupOfMemeber6_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;

			}
		}

	}
	
	
	public static void readAndVerifyAgeGroupMembers(String SheetName, int RowNum, int ColNumOfNumOfMembers,
			int ColNumOfAgeGroup1, int ColNumOfAgeGroup2, int ColNumOfAgeGroup3, int ColNumOfAgeGroup4,
			int ColNumOfAgeGroup5, int ColNumOfAgeGroup6) throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String NoOfMembers = (TestCaseData[RowNum][ColNumOfNumOfMembers].toString().trim());
		String AgeGroupOfMemeber1 = (TestCaseData[RowNum][ColNumOfAgeGroup1].toString().trim());
		String AgeGroupOfMemeber2 = (TestCaseData[RowNum][ColNumOfAgeGroup2].toString().trim());
		String AgeGroupOfMemeber3 = (TestCaseData[RowNum][ColNumOfAgeGroup3].toString().trim());
		String AgeGroupOfMemeber4 = (TestCaseData[RowNum][ColNumOfAgeGroup4].toString().trim());
		String AgeGroupOfMemeber5 = (TestCaseData[RowNum][ColNumOfAgeGroup5].toString().trim());
		String AgeGroupOfMemeber6 = (TestCaseData[RowNum][ColNumOfAgeGroup6].toString().trim());

		String str = driver.findElement(By.xpath(groupMembers_In_MailBody_Xpath)).getText();
		String[] agegroup = str.split(",");

		l1: for (int h=0; h<=agegroup.length; h++) {
			System.out.println(agegroup);

			String AgeGroupOfMemeber1_Mail = agegroup[0];
			String AgeGroupOfMemeber2_Mail = agegroup[1];
			String AgeGroupOfMemeber3_Mail = agegroup[2];
			String AgeGroupOfMemeber4_Mail = agegroup[3];
			String AgeGroupOfMemeber5_Mail = agegroup[4];
			String AgeGroupOfMemeber6_Mail = agegroup[5];
            
			switch (NoOfMembers) {
			case "1":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)) {
					System.out.println("AgeGroup is verified for Member 1: " + AgeGroupOfMemeber1_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "2":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)) {
					System.out.println("AgeGroup is verified for Member 1 and 2: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "3":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2 and 3: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "4":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3 and 4: " + AgeGroupOfMemeber1_Mail + " "
							+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " " + AgeGroupOfMemeber4_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not verified");
				}
				break;
			case "5":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
						&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3, 4 and 5: " + AgeGroupOfMemeber1_Mail
							+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
							+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;
			case "6":
				if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
						&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
						&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
						&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
						&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)
						&& AgeGroupOfMemeber6.equalsIgnoreCase(AgeGroupOfMemeber6_Mail)) {
					System.out.println("AgeGroup is verified for Member 1, 2, 3, 4, 5 and 6: " + AgeGroupOfMemeber1_Mail
							+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
							+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail + " " + AgeGroupOfMemeber6_Mail);
					break l1;
				} else {
					Assert.fail("Age Group is not Verified");
				}
				break;

			}
		}

	}
	

	// Clicked on Buy Now Button from Email Link
	public static void clickOnBuyNowEmailLink() throws Exception {
		Thread.sleep(10000);
		waitForElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		WebElement e1 = driver.findElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		pointToElement(e1);
		//waitForElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		clickElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
		System.out.println("Clicked on Buy Now Link Button from Email");
		logger.log(LogStatus.PASS, "Clicked on Buy Now Link Button from Email");

	}
	
	// Clicked on Buy Now Button from Email Link
		public static void clickOnBuyNowEmailLink2() throws Exception {
			Thread.sleep(10000);
			waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[4]/td//a"));
			WebElement e1 = driver.findElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[4]/td//a"));
			pointToElement(e1);
			//waitForElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
			clickElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[4]/td//a"));
			System.out.println("Clicked on Buy Now Link Button from Email");
			logger.log(LogStatus.PASS, "Clicked on Buy Now Link Button from Email");

		}
	//Read and Verify Data in Email Body
		public static void readAndVerifyDataInEmailBody(String SheetName, int Rownum) throws Exception{
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserName(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebers(SheetName, Rownum, 5);
			readAndVerifyAgeGroupMembers(SheetName,Rownum,5,6,7,8,9,10,11);
			readAndVerifySumInsured(SheetName, Rownum, 13);
			readAndVerifyPremium();
			readAndVerifyTenure(SheetName, Rownum, 14);
		}
		
	// Read and Verify Data in Email Body
	public static void readAndVerifyDataInEmailBody_ShareProposal(String SheetName, int Rownum) throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserName_ForSharePorposal(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebers(SheetName, Rownum, 5);
		//readAndVerifyAgeGroupMembers(SheetName, Rownum, 5, 6, 7, 8, 9, 10, 11);
		//readAndVerifyAgeGroupMembers2(SheetName, Rownum, 5, 6, 7, 8, 9, 10, 11);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsured(SheetName, Rownum, 13);
		readAndVerifyPremium();
		readAndVerifyTenure(SheetName, Rownum, 14);
	}
	
	
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
	public static void OpenEmailAndClickOnBuyNowButton(int rowNum) throws Exception {
		openEmail();
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		clickOnBuyNowEmailLink();
		switchToNewTab();
		Thread.sleep(2000);

	}
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
		public static void OpenEmailAndClickOnBuyNowButton2(int rowNum) throws Exception {
			openEmail();
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			clickOnBuyNowEmailLink2();
			switchToNewTab();
			Thread.sleep(2000);

		}
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
		public static void ClickOnBuyNowButtonFromEmail() throws Exception {
			//openEmail();
			//clickOnRefreshButtonUntilNewemailNotDisplayed();
			clickOnBuyNowEmailLink();
			switchToNewTab();
			Thread.sleep(2000);

		}
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
	public static void ClickOnBuyNowButtonFromEmail2() throws Exception {
		// openEmail();
		// clickOnRefreshButtonUntilNewemailNotDisplayed();
		clickOnBuyNowEmailLink2();
		switchToNewTab();
		Thread.sleep(2000);

	}
	//************  Super Mediclaim Critical and Other Functions Functions **********************************
	// Proposer Name
	public static void readAndVerifyPropserNameForOtherProducts(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1(SheetName);
		String Name = TestCaseData[Rownum][Colnum].toString().trim();
		/*
		 * String[] Name1=Name.split(" "); System.out.println(Name1[0]+
		 * Name1[1]);
		 */
		WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
		String propserName = e1.getText();

		// String propserName = "Arpan";

		/*
		 * for(int i=0; i<=Name1.length; i++){ if
		 * (propserName.contains(Name1[i])) { break; } else {
		 * Assert.fail("Propser Name is not available inside Email Body: " +
		 * propserName); } }
		 */

		if (propserName.contains(Name)) {
			System.out.println("Propser Name Verified: " + Name);
		} else {
			Assert.fail("Propser Name is not available inside Email Body: " + propserName);
		}

	}
		
	// Proposer Name
	public static void readAndVerifyPropserNameForOtherProducts2(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name = TestCaseData[Rownum][Colnum].toString().trim();
		Thread.sleep(5000);
		WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
		String propserName = e1.getText();
        
		if (propserName.contains(Name)) {
			System.out.println("Propser Name Verified: " + Name);
		} else {
			Assert.fail("Propser Name is not available inside Email Body: " + propserName);
		}

	}
	
	// Proposer Name
		public static void readAndVerifyPropserNameForOtherProducts4(String SheetName, int Rownum, int Colnum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String Name = TestCaseData[Rownum][Colnum].toString().trim();
			Thread.sleep(5000);
			WebElement e1 = driver.findElement(By.xpath(popserName_In_MailBody_Xpath));
			String propserName = e1.getText();
			
			String[] name1=Name.split(" ");
			 Name=name1[0].toString();
	        
			if (propserName.contains(Name)) {
				System.out.println("Propser Name Verified: " + Name);
			} else {
				Assert.fail("Propser Name is not available inside Email Body: " + propserName);
			}

		}
	
	
	// Proposer Name
	public static void readAndVerifyPropserNameForOtherProducts3(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name = TestCaseData[Rownum][Colnum].toString().trim();
		Thread.sleep(5000);
		List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
		int iSize=e1.size();
		for (int i=0; i<iSize; i++) {
			System.out.println(e1.get(i).getText());
			String propserName = e1.get(i).getText();
			
				if (propserName.contains(Name)) {
					System.out.println("Propser Name Verified: " + Name);
					break;
				}
				else{
					if(i==e1.size()-1){
					
					Assert.fail("Propser Name is not available inside Email Body: " + Name);
					}
					
				}
				
			}
		
	}
	
	// Proposer Name
		public static void readAndVerifyPropserNameForOtherProducts3_ShareProposal(String SheetName, int Rownum, int Colnum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String Name = TestCaseData[Rownum][Colnum].toString().trim();
			String[] names=Name.split("\\s");
			Name=names[0].toString();
			Thread.sleep(5000);
			List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
			int iSize=e1.size();
			for (int i=0; i<iSize; i++) {
				System.out.println(e1.get(i).getText());
				String propserName = e1.get(i).getText();
				
					if (propserName.contains(Name)) {
						System.out.println("Propser Name Verified: " + Name);
						break;
					}
					else{
						if(i==e1.size()-1){
						
						Assert.fail("Propser Name is not available inside Email Body: " + Name);
						}
						
					}
					
				}
			
		}
	
	// Proposer Name
		public static void readAndVerifyPropserNameForOtherProducts_ShareProposal3(String SheetName, int Rownum, int Colnum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String Name = TestCaseData[Rownum][Colnum].toString().trim();
			String[] str=Name.split("\\s");
			String Name1=str[0].toString();
			Thread.sleep(5000);
			List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
			int iSize=e1.size();
			for (int i=0; i<iSize; i++) {
				System.out.println(e1.get(i).getText());
				String propserName = e1.get(i).getText();
				
					if (propserName.contains(Name1)) {
						System.out.println("Propser Name Verified: " + Name);
						break;
					}
					else{
						if(i==e1.size()-1){
						
						Assert.fail("Propser Name is not available inside Email Body: " + Name);
						}
						
					}
					
				}
			
		}

	// Total Number of Members
	public static void readAndVerifyTotalNumberOfMemebersForOtherProducts(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1(SheetName);
		String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
		//String TotalMember = "1";
		Thread.sleep(2000);
		WebElement e1 = driver.findElement(By.xpath(totalNumberOfMembers_In_MailBody_Xpath));
		String numOfMember = e1.getText();

		// String numOfMember = "1";

		if (numOfMember.contains(TotalMember)) {
			System.out.println("Total Number of Members are Verified: " + TotalMember);
		} else {
			Assert.fail("Total Number of Members are not Verified in Email Body: " + numOfMember);
		}
	}

	// Total Number of Members
	public static void readAndVerifyTotalNumberOfMemebersForOtherProducts2(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
		//String TotalMember = "1";
		Thread.sleep(4000);
		waitForElement(By.xpath(totalNumberOfMembers_In_MailBody_Xpath));
		WebElement e1 = driver.findElement(By.xpath(totalNumberOfMembers_In_MailBody_Xpath));
		String numOfMember = e1.getText();

		// String numOfMember = "1";

		if (numOfMember.contains(TotalMember)) {
			System.out.println("Total Number of Members are Verified: " + TotalMember);
			
		} else {
			Assert.fail("Total Number of Members are not Verified in Email Body: " + numOfMember);
		}
	}
	
	// Total Number of Members
		public static void readAndVerifyTotalNumberOfMemebersForOtherProducts3(String SheetName, int Rownum, int Colnum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
			//String TotalMember = "1";
			Thread.sleep(4000);
			waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
			List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
			int iSize=e1.size();
			for(int i=0; i<iSize; i++){
			String numOfMember = e1.get(i).getText();
			// String numOfMember = "1";
			if (numOfMember.contains(TotalMember)) {
				System.out.println("Total Number of Members are Verified: " + TotalMember);
				break;
			}
			else{
				if(i==iSize-1){
					Assert.fail("Total Number of Members are not Verified in Email Body: " + numOfMember);
				}
			}
			}
		}
				
	// Sum Insured
	public static void readAndVerifySumInsuredForOtherProducts(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1(SheetName);
		String sumInsuredValue = (TestCaseData[Rownum][Colnum].toString().trim());

		WebElement e1 = driver.findElement(By.xpath(sumInsured_In_MailBody_Xpath));
		String sumInsured = e1.getText();
		// String sumInsured = "10";
		if (sumInsured.contains(sumInsuredValue)) {
			System.out.println("Sum Insured is Verified in Email Body: " + sumInsuredValue);
		} else {
			Assert.fail("Sum Insured is not Verified in Email Body: " + sumInsured);
		}
	}

	// Sum Insured
	public static void readAndVerifySumInsuredForOtherProducts2(String SheetName, int Rownum, int Colnum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String sumInsuredValue = (TestCaseData[Rownum][Colnum].toString().trim());
		Thread.sleep(4000);
        waitForElement(By.xpath(sumInsured_In_MailBody_Xpath));
		WebElement e1 = driver.findElement(By.xpath(sumInsured_In_MailBody_Xpath));
		String sumInsured = e1.getText();
		// String sumInsured = "10";
		if (sumInsured.contains(sumInsuredValue)) {
			System.out.println("Sum Insured is Verified in Email Body: " + sumInsuredValue);
		} else {
			Assert.fail("Sum Insured is not Verified in Email Body: " + sumInsured);
		}
	}
	
	// Sum Insured
		public static void readAndVerifySumInsuredForOtherProducts3(String SheetName, int Rownum, int Colnum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String sumInsuredValue = (TestCaseData[Rownum][Colnum].toString().trim());
			//String sumInsuredValue = "10";
			Thread.sleep(4000);
	        waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
			List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//table//table//tr//td"));
			
			int iSize=e1.size();
			for(int i=0; i<iSize; i++){
			String sumInsured = e1.get(i).getText();
			 
			
			if (sumInsured.contains(sumInsuredValue)) {
				System.out.println("Sum Insured is Verified in Email Body: " + sumInsuredValue);
				break;
			}
			else {
				if(i==iSize-1){
				Assert.fail("Sum Insured is not Verified in Email Body: " + sumInsured);
				}
			}
			}
		}
		
	// Premium
	public static void readAndVerifyPremiumForOtherProducts() throws Exception {
		String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
		// String premiumValue="607";
		// String premiumValue =
		// SuperMediclaimCriticalFunctions.firstpage_PremiumValue;
		// String premiumValue = "969";

		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured1 = e1.getText();
		String sumInsured = sumInsured1.replaceAll("\\W", "");

		String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
		// System.out.println(str);

		if (premiumValue.contains(sumInsured2)) {
			System.out.println("Total Premium is Verified: " + sumInsured2);
		} else {
			Assert.fail("Total Premium is not Verified: " + premiumValue);
		}
	}
			
	// Premium
	public static void readAndVerifyPremiumForOtherProducts2() throws Exception {
		String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
		// String premiumValue="607";
		// String premiumValue =
		// SuperMediclaimCriticalFunctions.firstpage_PremiumValue;
		// String premiumValue = "969";
		Thread.sleep(4000);
        waitForElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured1 = e1.getText();
		String sumInsured = sumInsured1.replaceAll("\\W", "");

		String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
		// System.out.println(str);

		if (premiumValue.contains(sumInsured2)) {
			System.out.println("Total Premium is Verified: " + sumInsured2);
		} else {
			Assert.fail("Total Premium is not Verified: " + premiumValue);
		}
	}
	
	// Premium
		public static void readAndVerifyPremiumForOtherProducts3() throws Exception {
			String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
			 //String premiumValue="4024";
			// String premiumValue =
			// SuperMediclaimCriticalFunctions.firstpage_PremiumValue;
			// String premiumValue = "969";
			Thread.sleep(4000);
	        waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[3]//table//tr[6]//td[3]/strong"));
			WebElement e1 = driver.findElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[3]//table//tr[6]//td[3]/strong"));
			pointToElement(e1);
			String sumInsured1 = e1.getText();
			String sumInsured = sumInsured1.replaceAll("\\W", "");

			String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
			// System.out.println(str);

			if (premiumValue.contains(sumInsured2)) {
				System.out.println("Total Premium is Verified: " + sumInsured2);
			} else {
				Assert.fail("Total Premium is not Verified: " + premiumValue);
			}
		}
			
	// Tenure
	public static void readAndVerifyTenureForOtherProducts(String SheetName, int Rownum, int ColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1(SheetName);
		String TenureValue = (TestCaseData[Rownum][ColNum].toString().trim());
		 //String TenureValue = "1";
		Thread.sleep(4000);
		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured = e1.getText();

		String[] str = sumInsured.split(" ");
		String Num = str[2];
		String year = str[3];
		String tenure = Num.concat(" ");
		String tenure1 = tenure.concat(year);
		System.out.println(tenure1);

		if (tenure1.contains(TenureValue)) {
			System.out.println("Tenure is Verified: " + TenureValue);
		} else {
			Assert.fail("Tenure is not Verified: " + tenure1);
		}
	}
	
	// Tenure
	public static void readAndVerifyTenureForOtherProducts2(String SheetName, int Rownum, int ColNum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TenureValue = (TestCaseData[Rownum][ColNum].toString().trim());
		// String TenureValue = "1";

		WebElement e1 = driver.findElement(By.xpath(premiumAndTenure_In_MailBody_Xpath));
		pointToElement(e1);
		String sumInsured = e1.getText();

		String[] str = sumInsured.split(" ");
		String Num = str[2];
		String year = str[3];
		String tenure = Num.concat(" ");
		String tenure1 = tenure.concat(year);
		System.out.println(tenure1);

		if (tenure1.contains(TenureValue)) {
			System.out.println("Tenure is Verified: " + TenureValue);
		} else {
			Assert.fail("Tenure is not Verified: " + tenure1);
		}
	}
	
	// Tenure
		public static void readAndVerifyTenureForOtherProducts3(String SheetName, int Rownum, int ColNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String TenureValue = (TestCaseData[Rownum][ColNum].toString().trim());
			// String TenureValue = "1";

			WebElement e1 = driver.findElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[3]//table//tr[6]//td[3]/strong"));
			pointToElement(e1);
			String sumInsured = e1.getText();

			String[] str = sumInsured.split(" ");
			String Num = str[1];
			String year = str[2];
			String tenure = Num.concat(" ");
			String tenure1 = tenure.concat(year);
			System.out.println(tenure1);

			if (tenure1.contains(TenureValue)) {
				System.out.println("Tenure is Verified: " + TenureValue);
			} else {
				Assert.fail("Tenure is not Verified: " + tenure1);
			}
		}
		
		
			//Age Group for Super Mediclaim Critical
			public static String ageGroupOfMember1;
			public static String ageGroupOfMember2;
			public static String ageGroupOfMember3;
			public static String ageGroupOfMember4;
			public static String ageGroupOfMember5;
			public static String ageGroupOfMember6;
			
			public static String readAgeGroupOfMember1SuperMediclaimCritical(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Age of Member 1']//following::div[contains(@class,'year_drop_slect master')]/a"));
				ageGroupOfMember1=e1.getText();
				return ageGroupOfMember1;
			}
			
			public static String readAgeGroupOfMember2SuperMediclaimCritical(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Age of Member 2']//following::div[contains(@class,'year_drop_slect master')]/a"));
				 ageGroupOfMember2=e1.getText();
				return ageGroupOfMember2;
			}
			
			public static String readAgeGroupOfMember3SuperMediclaimCritical(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Age of Member 3']//following::div[contains(@class,'year_drop_slect master')]/a"));
				 ageGroupOfMember3=e1.getText();
				return ageGroupOfMember3;
			}
			
			public static String readAgeGroupOfMember4SuperMediclaimCritical(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Age of Member 4']//following::div[contains(@class,'year_drop_slect master')]/a"));
				 ageGroupOfMember4=e1.getText();
				return ageGroupOfMember4;
			}
			public static String readAgeGroupOfMember5SuperMediclaimCritical(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Age of Member 5']//following::div[contains(@class,'year_drop_slect master')]/a"));
				 ageGroupOfMember5=e1.getText();
				return ageGroupOfMember5;
			}
			public static String readAgeGroupOfMember6SuperMediclaimCritical(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Age of Member 6']//following::div[contains(@class,'year_drop_slect master')]/a"));
				 ageGroupOfMember6=e1.getText();
				return ageGroupOfMember6;
			}
			
			public static String NumberOfMemebers;
			public static String numberOfMemebers(){
				WebElement e1=driver.findElement(By.xpath("//p[text()='Total Members']//following-sibling::div[1]//div[contains(@class,'year_drop_slect master')]/a"));
				 NumberOfMemebers=e1.getText();
				return NumberOfMemebers;
			}
			
			public static void closeEditPopupBox(){
				waitForElement(By.xpath("//div[contains(@class,'get_qout_modal_heading_container')]//following::p[1]/img"));
				WebElement e2=driver.findElement(By.xpath("//div[contains(@class,'get_qout_modal_heading_container')]//following::p[1]/img"));
				clickByJS(e2);
				//e2.click();
			}
			
			//Read Age Group based on number of members
			public static void readAgeGroup() throws Exception{
				/*waitForElement(By.xpath("//div[contains(@class,'edit_save_btn ')]/a"));
				WebElement editButton=driver.findElement(By.xpath("//div[contains(@class,'edit_save_btn ')]/a"));
				//editButton.click();
				clickByJS(editButton);*/
				Thread.sleep(2000);
				String numberOfMemebers = numberOfMemebers();
				if(numberOfMemebers.equals("1")){
					readAgeGroupOfMember1SuperMediclaimCritical();
				}
				else if(numberOfMemebers.equals("2")){
					readAgeGroupOfMember1SuperMediclaimCritical();
					readAgeGroupOfMember2SuperMediclaimCritical();
				}
				else if(numberOfMemebers.equals("3")){
					readAgeGroupOfMember1SuperMediclaimCritical();
					readAgeGroupOfMember2SuperMediclaimCritical();
					readAgeGroupOfMember3SuperMediclaimCritical();
				}
				else if(numberOfMemebers.equals("4")){
					readAgeGroupOfMember1SuperMediclaimCritical();
					readAgeGroupOfMember2SuperMediclaimCritical();
					readAgeGroupOfMember3SuperMediclaimCritical();
					readAgeGroupOfMember4SuperMediclaimCritical();
				}
				else if(numberOfMemebers.equals("5")){
					readAgeGroupOfMember1SuperMediclaimCritical();
					readAgeGroupOfMember2SuperMediclaimCritical();
					readAgeGroupOfMember3SuperMediclaimCritical();
					readAgeGroupOfMember4SuperMediclaimCritical();
					readAgeGroupOfMember5SuperMediclaimCritical();
				}
				else if(numberOfMemebers.equals("6")){
					readAgeGroupOfMember1SuperMediclaimCritical();
					readAgeGroupOfMember2SuperMediclaimCritical();
					readAgeGroupOfMember3SuperMediclaimCritical();
					readAgeGroupOfMember4SuperMediclaimCritical();
					readAgeGroupOfMember5SuperMediclaimCritical();
					readAgeGroupOfMember6SuperMediclaimCritical();
					
				}
				//closeEditPopupBox();
				
			}
	
			//Read and Verify age groups in email body
			public static void readAndVerifyAgeGroupMembersForOtherProducts(String SheetName, int RowNum) throws Exception {

				String NoOfMembers = NumberOfMemebers;
				String AgeGroupOfMemeber1 = ageGroupOfMember1;
				String AgeGroupOfMemeber2 = ageGroupOfMember2;
				String AgeGroupOfMemeber3 = ageGroupOfMember3;
				String AgeGroupOfMemeber4 = ageGroupOfMember4;
				String AgeGroupOfMemeber5 = ageGroupOfMember5;
				String AgeGroupOfMemeber6 = ageGroupOfMember6;
				
				/*String NoOfMembers = "1";
				String AgeGroupOfMemeber1 = "5 - 24 years";
				String AgeGroupOfMemeber2 = ageGroupOfMember2;
				String AgeGroupOfMemeber3 = ageGroupOfMember3;
				String AgeGroupOfMemeber4 = ageGroupOfMember4;
				String AgeGroupOfMemeber5 = ageGroupOfMember5;
				String AgeGroupOfMemeber6 = ageGroupOfMember6;*/
				//waitForElement(By.xpath(groupMembers_In_MailBody_Xpath));
				Thread.sleep(4000);
                WebElement e1=driver.findElement(By.xpath(groupMembers_In_MailBody_Xpath));
                pointToElement(e1);
				String str = driver.findElement(By.xpath(groupMembers_In_MailBody_Xpath)).getText();
				String[] agegroup = str.split(",");

				l1: 
					for (int h=0; h<=agegroup.length; h++){
					
					//Based on Number of members we are segregating Age group of members
					String AgeGroupOfMemeber1_Mail=null;;
					String AgeGroupOfMemeber2_Mail=null;;
					String AgeGroupOfMemeber3_Mail=null;;
					String AgeGroupOfMemeber4_Mail=null;;
					String AgeGroupOfMemeber5_Mail=null;;
					String AgeGroupOfMemeber6_Mail=null;;
					
					switch(NoOfMembers){
					case "1":
						 AgeGroupOfMemeber1_Mail = agegroup[0];
						 break;
					case "2":
						 AgeGroupOfMemeber1_Mail = agegroup[0];
						 AgeGroupOfMemeber2_Mail = agegroup[1];
						 break;
					case "3":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						break;
					case "4":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						AgeGroupOfMemeber4_Mail = agegroup[3];
						break;
					case "5":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						AgeGroupOfMemeber4_Mail = agegroup[3];
						AgeGroupOfMemeber5_Mail = agegroup[4];
						break;
					case "6":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						AgeGroupOfMemeber4_Mail = agegroup[3];
						AgeGroupOfMemeber5_Mail = agegroup[4];
						AgeGroupOfMemeber6_Mail = agegroup[5];
						break;
					
					}
					
					/*String AgeGroupOfMemeber1_Mail = agegroup[0];
					String AgeGroupOfMemeber2_Mail = agegroup[1];
					String AgeGroupOfMemeber3_Mail = agegroup[2];
					String AgeGroupOfMemeber4_Mail = agegroup[3];
					String AgeGroupOfMemeber5_Mail = agegroup[4];
					String AgeGroupOfMemeber6_Mail = agegroup[5];*/
					
                   //Age group of members are verifying
					switch (NoOfMembers) {
					case "1":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)) {
							System.out.println("AgeGroup is verified for Member 1: " + AgeGroupOfMemeber1_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "2":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)) {
							System.out.println("AgeGroup is verified for Member 1 and 2: " + AgeGroupOfMemeber1_Mail + " "
									+ AgeGroupOfMemeber2_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "3":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2 and 3: " + AgeGroupOfMemeber1_Mail + " "
									+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "4":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
								&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2, 3 and 4: " + AgeGroupOfMemeber1_Mail + " "
									+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " " + AgeGroupOfMemeber4_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not verified");
						}
						break;
					case "5":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
								&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
								&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2, 3, 4 and 5: " + AgeGroupOfMemeber1_Mail
									+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
									+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "6":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
								&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
								&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)
								&& AgeGroupOfMemeber6.equalsIgnoreCase(AgeGroupOfMemeber6_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2, 3, 4, 5 and 6: " + AgeGroupOfMemeber1_Mail
									+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
									+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail + " " + AgeGroupOfMemeber6_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;

					}
				}

			}
			
	// Read and Verify Data in Email Body
/*	public static void readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimCritical(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
		readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
	}*/
	
	// Read and Verify Data in Email Body For Super Mediclaim Critical
		public static void readAndVerifyDataInEmailBody_ShareQuotation_SuperMediclaimCritical(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
		}

		
		
//*************** Share Quote Functions *************************************************************************		
   
//*********** Care With NCB **************************
	// Read and Verify Data in Email Body For Care With NCB
	public static void readAndVerifyDataInEmailBody_ShareQuotation_CareWithNCB(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 15);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 18);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 19);
	}
	
	
	//*********** Care Heart **************************
		// Read and Verify Data in Email Body For Care With NCB
		public static void readAndVerifyDataInEmailBody_ShareQuotation_CareHeart(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 15);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 18);
			readAndVerifyPremiumForOtherProducts2();
			readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 19);
		}
	
	
	// Read and Verify Data in Email Body For Care Freedom
		public static void readAndVerifyDataInEmailBody_ShareQuotation_CareFreedom(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 18);
			readAndVerifyPremiumForOtherProducts2();
			readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 19);
		}

	// Read and Verify Data in Email Body For Care Global
		public static void readAndVerifyDataInEmailBody_ShareQuotation_CareGlobal(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 5);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 8);
			readAndVerifyPremiumForOtherProducts2();
			readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 9);
		}

	// Read and Verify Data in Email Body For Care With POS Care Freedom
	public static void readAndVerifyDataInEmailBody_ShareQuotation_POSCareFreedom(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 5);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 9);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 10);
	}
	
	// Read and Verify Data in Email Body For Care With Smart Select
			public static void readAndVerifyDataInEmailBody_ShareQuotation_CareWithSmartSelect(String SheetName, int Rownum)
					throws Exception {
				
				String[][] TestCaseData = BaseClass.excel_Files("SmartSelect_Quotation");
				clickOnRefreshButtonUntilNewemailNotDisplayed();
				readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
				readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 5);
				
				String CoverType = TestCaseData[Rownum][8].toString().trim();
				if(CoverType.equalsIgnoreCase("Individual")){
					readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
				}
				else{
					System.out.println("Age group not verified for this  product");
				}
				
			   //readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
				readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 9);
				readAndVerifyPremiumForOtherProducts2();
				readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 10);
			}
			
	// Read and Verify Data in Email Body For POS Care With Smart Select
	public static void readAndVerifyDataInEmailBody_ShareQuotation_POSCareWithSmartSelect(String SheetName, int Rownum)
			throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files("POSSmartSelect_Quotation");
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);

		String CoverType = TestCaseData[Rownum][8].toString().trim();
		if (CoverType.equalsIgnoreCase("Individual")) {
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		} else {
			System.out.println("Age group not verified for this  product");
		}

		// readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 10);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 11);
	}
			
	// Read and Verify Data in Email Body For Care With POS Care With NCB
	public static void readAndVerifyDataInEmailBody_ShareQuotation_POSCareWithNCB(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 15);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 18);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 19);
	}
	
	//*********** Care Senior **************************
		// Read and Verify Data in Email Body For Care With NCB
		public static void readAndVerifyDataInEmailBody_ShareQuotation_CareSenior(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 10);
			readAndVerifyPremiumForOtherProducts2();
			readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 11);
		}
		
	// *********** Care Senior **************************
	// Read and Verify Data in Email Body For Care With NCB
	public static void readAndVerifyDataInEmailBody_ShareQuotation_POSCareSenior(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 10);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 11);
	}
		
	// *********** Care OPD **************************
	// Read and Verify Data in Email Body For Care With NCB
	public static void readAndVerifyDataInEmailBody_ShareQuotation_CareOPD(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);
		// readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 10);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 11);
	}

	// *********** Enhance **************************	
	// Read and Verify Data in Email Body For Care With NCB
	public static void readAndVerifyDataInEmailBody_ShareQuotation_Enhnace(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts3(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts3(SheetName, Rownum, 5);
		//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts3(SheetName, Rownum, 10);
		readAndVerifyPremiumForOtherProducts3();
		readAndVerifyTenureForOtherProducts3(SheetName, Rownum, 11);
	}
	
	// *********** Enhance **************************	
	
	// Premium
			public static void readAndVerifyPremiumForSecure() throws Exception {
				String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
				 //String premiumValue="4024";
				// String premiumValue =
				// SuperMediclaimCriticalFunctions.firstpage_PremiumValue;
				// String premiumValue = "969";
				Thread.sleep(4000);
		        waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[3]//table//tr[5]//td[3]/strong"));
				WebElement e1 = driver.findElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[3]//table//tr[5]//td[3]/strong"));
				pointToElement(e1);
				String sumInsured1 = e1.getText();
				String sumInsured = sumInsured1.replaceAll("\\W", "");

				String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
				// System.out.println(str);

				if (premiumValue.contains(sumInsured2)) {
					System.out.println("Total Premium is Verified: " + sumInsured2);
				} else {
					Assert.fail("Total Premium is not Verified: " + premiumValue);
				}
			}
			
			// Tenure
			public static void readAndVerifyTenureForSecure(String SheetName, int Rownum, int ColNum) throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);
				String TenureValue = (TestCaseData[Rownum][ColNum].toString().trim());
				// String TenureValue = "1";

				WebElement e1 = driver.findElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table/tbody/tr[3]/td/table/tbody/tr[3]//table//tr[5]//td[3]/strong"));
				pointToElement(e1);
				String sumInsured = e1.getText();

				String[] str = sumInsured.split(" ");
				String Num = str[1];
				String year = str[2];
				String tenure = Num.concat(" ");
				String tenure1 = tenure.concat(year);
				System.out.println(tenure1);

				if (tenure1.contains(TenureValue)) {
					System.out.println("Tenure is Verified: " + TenureValue);
				} else {
					Assert.fail("Tenure is not Verified: " + tenure1);
				}
			}
			
		// Read and Verify Data in Email Body For Care With NCB
		public static void readAndVerifyDataInEmailBody_ShareQuotation_Secure(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts3(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts3(SheetName, Rownum, 5);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts3(SheetName, Rownum, 9);
			readAndVerifyPremiumForSecure();
			readAndVerifyTenureForSecure(SheetName, Rownum, 10);
		}
		
		// Read and Verify Data in Email Body For Care With NCB
				public static void readAndVerifyDataInEmailBody_ShareQuotation_POSSecure(String SheetName, int Rownum)
						throws Exception {
					clickOnRefreshButtonUntilNewemailNotDisplayed();
					readAndVerifyPropserNameForOtherProducts3(SheetName, Rownum, 2);
					readAndVerifyTotalNumberOfMemebersForOtherProducts3(SheetName, Rownum, 5);
					//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
					readAndVerifySumInsuredForOtherProducts3(SheetName, Rownum, 9);
					readAndVerifyPremiumForSecure();
					readAndVerifyTenureForSecure(SheetName, Rownum, 10);
				}
		
	// Read and Verify Data in Email Body For Super Mediclaim Operation
	public static void readAndVerifyDataInEmailBody_ShareQuotation_SuperMediclaimOperation(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
		readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
	}
	
	// Read and Verify Data in Email Body For Super Mediclaim Operation
		public static void readAndVerifyDataInEmailBody_ShareQuotation_SuperMediclaimHeart(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
		}
		
//************ Share Proposal Functions *****************************************************************
	
    // Read and Verify Data in Email Body
	public static void readAndVerifyDataInEmailBody_ShareProposal_CareWithNCB(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 18);
		readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 19);
	}
	
	// Read and Verify Data in Email Body
		public static void readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimHeart(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
		}
		
		// Read and Verify Data in Email Body
	public static void readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimOperation(String SheetName, int Rownum)throws Exception {
					clickOnRefreshButtonUntilNewemailNotDisplayed();
					readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
					readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
					readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
					readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
					readAndVerifyPremiumForOtherProducts();
					readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
				}
				

	// Read and Verify Data in Email Body
	public static void readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimCritical(String SheetName, int Rownum)throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 17);
		readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 18);
	}
	
	 // Read and Verify Data in Email Body
		public static void readAndVerifyDataInEmailBody_ShareProposal_CareHeart(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 18);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 19);
		}
	
	 // Read and Verify Data in Email Body
		public static void readAndVerifyDataInEmailBody_ShareProposal_POSCareWithNCB(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 18);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 19);
		}
	
	// Read and Verify Data in Email Body for Care Freedom
		public static void readAndVerifyDataInEmailBody_ShareProposal_CareFreedom(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 15);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 18);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 19);
		}
		
	// Read and Verify Data in Email Body for Care Smart Select
		public static void readAndVerifyDataInEmailBody_ShareProposal_POSCareFreedom(String SheetName, int Rownum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files("PosCarefreedom_Quotation_Data");
			
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts4(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 5);
			
			String CoverType = TestCaseData[Rownum][8].toString().trim();
			if(CoverType.equalsIgnoreCase("Individual")){
				readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			}
			else{
				System.out.println("Age group not verified for this  product");
			}
			
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 9);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 10);
		}
		
		
	// Read and Verify Data in Email Body for Care Smart Select
	public static void readAndVerifyDataInEmailBody_ShareProposal_Enhance(String SheetName, int Rownum)
			throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts_ShareProposal3(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts3(SheetName, Rownum, 5);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts3(SheetName, Rownum, 10);
			readAndVerifyPremiumForOtherProducts3();
			readAndVerifyTenureForOtherProducts3(SheetName, Rownum, 11);
		}
	
	// Read and Verify Data of Share Proposal in Email Body For Care HNI
			public static void readAndVerifyDataInEmailBody_ShareProposal_Secure(String SheetName, int Rownum)
					throws Exception {
				/*clickOnRefreshButtonUntilNewemailNotDisplayed();
				//readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
				readAndVerifyPropserNameForOtherProducts3(SheetName, Rownum, 2);
				readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 5);
				//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
				readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 9);
				readAndVerifyPremiumForOtherProducts();
				readAndVerifyTenureForOtherProducts(SheetName, Rownum, 10);*/
				
				clickOnRefreshButtonUntilNewemailNotDisplayed();
				readAndVerifyPropserNameForOtherProducts3_ShareProposal(SheetName, Rownum, 2);
				readAndVerifyTotalNumberOfMemebersForOtherProducts3(SheetName, Rownum, 5);
				//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
				readAndVerifySumInsuredForOtherProducts3(SheetName, Rownum, 9);
				readAndVerifyPremiumForSecure();
				readAndVerifyTenureForSecure(SheetName, Rownum, 10);
			}
		
	// Read and Verify Data of Share Proposal in Email Body For Care HNI
	public static void readAndVerifyDataInEmailBody_ShareProposal_POSSecure(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts3_ShareProposal(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts3(SheetName, Rownum, 5);
		readAndVerifySumInsuredForOtherProducts3(SheetName, Rownum, 9);
		readAndVerifyPremiumForSecure();
		readAndVerifyTenureForSecure(SheetName, Rownum, 10);
	}
		
	// Read and Verify Data in Email Body for Care Smart Select
	public static void readAndVerifyDataInEmailBody_ShareProposal_CareWithOPD(String SheetName, int Rownum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("CareWithOPD_Quotation");

		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 6);

		String CoverType = TestCaseData[Rownum][8].toString().trim();
		if (CoverType.equalsIgnoreCase("Individual")) {
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		} else {
			System.out.println("Age group not verified for this  product");
		}

		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 10);
		readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 11);
	}
		
	// Read and Verify Data in Email Body for Care Smart Select
	public static void readAndVerifyDataInEmailBody_ShareProposal_CareSmartSelect(String SheetName, int Rownum)
			throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("SmartSelect_Quotation");
		
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts4(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 5);
		
		String CoverType = TestCaseData[Rownum][8].toString().trim();
		if(CoverType.equalsIgnoreCase("Individual")){
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		}
		else{
			System.out.println("Age group not verified for this  product");
		}
		
		//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		
		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 9);
		readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 10);
	}
	
	// Read and Verify Data in Email Body for Care Smart Select
		public static void readAndVerifyDataInEmailBody_ShareProposal_POSCareSmartSelect(String SheetName, int Rownum)
				throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files("POSSmartSelect_Quotation");
			
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts4(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);
			
			String CoverType = TestCaseData[Rownum][8].toString().trim();
			if(CoverType.equalsIgnoreCase("Individual")){
				readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			}
			else{
				System.out.println("Age group not verified for this  product");
			}
			
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 10);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 11);
		}
		
	// Read and Verify Data in Email Body for Care Freedom
	public static void readAndVerifyDataInEmailBody_ShareProposal_CareGlobal(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts4(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 5);
		//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 8);
		 readAndVerifyPremiumForOtherProducts();
		readAndVerifyTenureForOtherProducts(SheetName, Rownum, 9);
	}
	
	// Read and Verify Data in Email Body For Care Senior
	public static void readAndVerifyDataInEmailBody_ShareProposal_CareSenior(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
		readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 10);
		readAndVerifyPremiumForOtherProducts2();
		readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 11);
	}
	
	// Read and Verify Data in Email Body For Care Senior
		public static void readAndVerifyDataInEmailBody_ShareProposal_POSCareSenior(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyPropserNameForOtherProducts2(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts2(SheetName, Rownum, 6);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts2(SheetName, Rownum, 10);
			readAndVerifyPremiumForOtherProducts2();
			readAndVerifyTenureForOtherProducts2(SheetName, Rownum, 11);
		}
	
	
	 // Read and Verify Data of Share Proposal in Email Body For Care HNI
		public static void readAndVerifyDataInEmailBody_ShareProposal_CareHNI(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			//readAndVerifyPropserNameForOtherProducts(SheetName, Rownum, 2);
			readAndVerifyPropserNameForOtherProducts4(SheetName, Rownum, 2);
			readAndVerifyTotalNumberOfMemebersForOtherProducts(SheetName, Rownum, 5);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifySumInsuredForOtherProducts(SheetName, Rownum, 8);
			readAndVerifyPremiumForOtherProducts();
			readAndVerifyTenureForOtherProducts(SheetName, Rownum, 9);
		}
			
		
}
