package com.org.faveo.Base;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.travelInsurance.DomesticTravel;
import com.relevantcodes.extentreports.LogStatus;

public class ReadDataFromEmail_TravelInsurance extends BaseClass implements AccountnSettingsInterface {

	// ********* Mail Verification Functions
	// *************************************************
	public static void openEmail() throws Exception {
		try{
		String[][] TestCaseData = BaseClass.excel_Files("Credentials");
		
		Properties prop = readProperties();
		String url = prop.getProperty("MailUrl");
		// System.out.println("Property Value:"+ str);
		driver.get(url);
		System.out.println("Open Mail Id");
		logger.log(LogStatus.PASS, "Open Mail Id");

		//String userName = prop.getProperty("mailUserName");
		String userName = TestCaseData[1][5].toString().trim();
		//waitForElement(By.xpath(userName_Xpath));
		waitForElement(By.xpath(userName_Xpath));
		enterText(By.xpath(userName_Xpath), userName);
		System.out.println("Data Entered for Mail User Name:  " + userName);
		logger.log(LogStatus.PASS, "Data Entered for Mail User Name:  " + userName);

		//String Password = prop.getProperty("mailPassword");
		String Password = TestCaseData[1][6].toString().trim();
		waitForElement(By.xpath(password_Xpath));
		enterText(By.xpath(password_Xpath), Password);
		System.out.println("Data Entered for Mail Password:  " + Password);
		logger.log(LogStatus.PASS, "Data Entered for Mail Password:  " + Password);

		waitForElement(By.xpath(signIN_Xpath));
		clickElement(By.xpath(signIN_Xpath));
		System.out.println("Clicked on Sign-In button");
		logger.log(LogStatus.PASS, "Clicked on Sign-In button");

		waitForElement(By.xpath(shareQuotationFolder_Xpath));
		clickElement(By.xpath(shareQuotationFolder_Xpath));

		System.out.println("Moved to Shared Proposal Email Folder");
		logger.log(LogStatus.PASS, "Moved to Shared Proposal Email Folder");
		}
		catch(Exception e){
			throw e;
		}

	}

	// Delete Button in Email
	public static void clickOndeleteButtonInEmail() {
		/*waitForElement(By.xpath(deleteButton_In_Mail_Xpath));
		clickElement(By.xpath(deleteButton_In_Mail_Xpath));*/
		try{
	    Thread.sleep(10000);
		if(driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).isDisplayed());
		{
		driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).click();
		System.out.println("Clicked on Delete Button");
		logger.log(LogStatus.PASS, "Clicked on Delete Button");
		}
		}
		catch(Exception e){
			System.out.println("No Need to Delete any Email");
		}	

	}
	
	// Delete Button in Email
		public static void clickOndeleteButtonInEmail2() {
			/*waitForElement(By.xpath(deleteButton_In_Mail_Xpath));
			clickElement(By.xpath(deleteButton_In_Mail_Xpath));*/
			try{
		    Thread.sleep(10000);
			if(driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).isDisplayed());
			{
			List<WebElement> list=driver.findElements(By.xpath("//div[@class='_lv_m1']//button[@autoid='_lv_4']"));
			System.out.println(list.size());
			for(int i=0; i<list.size(); i++){
			Thread.sleep(4000);
			driver.findElement(By.xpath("//button[@autoid='_lv_4']//following::button[@title='Delete']/span[1]")).click();
			System.out.println("Clicked on Delete Button");
			logger.log(LogStatus.PASS, "Clicked on Delete Button");
			}
			}
			}
			catch(Exception e){
				System.out.println("No Need to Delete any Email");
			}	

		}


	public static void openAndDeleteOldEmail() throws Exception {
		openEmail();
		clickOndeleteButtonInEmail2();
	}
	
	//This code wait till 5 times means then it will break the loop
		// Function for Click on Refresh Button Until New Email Not Displayed
		public static void clickOnRefreshButtonUntilNewemailNotDisplayed() throws Exception {
			boolean flag = true;
	      l1:
			while (flag) {
				
				
				for(int i=0; i<=10; i++){
				Thread.sleep(8000);
				try {
					/*if (driver.findElement(By.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']")).isDisplayed()) {*/
					if(driver.findElement(By.xpath("//div[@class='_lv_m1']//div[@class='_lv_C _lv_D']/span[text()='Religare Health Insurance'][1]")).isDisplayed()){	
					System.out.println("Email is being Displayed");
						logger.log(LogStatus.PASS, "Email is being Displayed");
						break l1;
					}
				} catch (Exception e) {
					if(i==10){
						System.out.println("Email is not Displayed");
						logger.log(LogStatus.FAIL, "Email is not Displayed");
						break l1;
					}
					else{
						driver.navigate().refresh();
						waitForElement(By.xpath(shareQuotationFolder_Xpath));
						clickElement(By.xpath(shareQuotationFolder_Xpath));
						System.out.println("Email is not Desplayed for time: "+ i);
						
					}
				}
			}
			}
		}
	
	// Total Number of Members
			public static void readAndVerifyTravellingTo(String SheetName, int Rownum, int Colnum)
					throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);
				String TravellingTo = (TestCaseData[Rownum][Colnum].toString().trim());
				//String TotalMember = "1";
				Thread.sleep(4000);
				waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
				List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
				int iSize=e1.size();
				for(int i=0; i<iSize; i++){
				String travellingtoValue = e1.get(i).getText();
				// String numOfMember = "1";
				if (travellingtoValue.contains(TravellingTo)) {
					System.out.println("Travelling to is Verified in Email Body: " + TravellingTo);
					break;
				}
				else{
					if(i==iSize-1){
						Assert.fail("Travelling to is not Verified in Email Body: " + TravellingTo);
					}
				}
				}
			}
			
	// Total Number of Members
	public static void readAndVerifySumInsuredExplore(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String SumInsured = (TestCaseData[Rownum][Colnum].toString().trim());
		SumInsured=SumInsured+","+"000";
		// String TotalMember = "1";
		Thread.sleep(4000);
		waitForElement(By.xpath(
				"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
		List<WebElement> e1 = driver.findElements(By.xpath(
				"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
		int iSize = e1.size();
		for (int i = 0; i < iSize; i++) {
			String sumInsuredTestData = e1.get(i).getText();
			//System.out.println(sumInsuredTestData);
			// String numOfMember = "1";
			if (sumInsuredTestData.contains(SumInsured)) {
				System.out.println("Sum Insured is Verified in Email Body: " + SumInsured);
				break;
			} else {
				if (i == iSize - 1) {
					Assert.fail("Sum Insured is not Verified in Email Body: " + SumInsured);
				}
			}
		}
	}
	
	// Total Number of Members
		public static void readAndVerifyPeriodOfInsuranceFrom() throws Exception {
			
			String fromDate =travelStartDate;
			Thread.sleep(4000);
			waitForElement(By.xpath(
					"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
			List<WebElement> e1 = driver.findElements(By.xpath(
					"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
			int iSize = e1.size();
			for (int i = 0; i < iSize; i++) {
				String sumInsuredTestData = e1.get(i).getText();
				//System.out.println(sumInsuredTestData);
				// String numOfMember = "1";
				if (sumInsuredTestData.contains(fromDate)) {
					System.out.println("Travelling Start Date Period is Verified in Email Body: " + fromDate);
					break;
				} else {
					if (i == iSize - 1) {
						Assert.fail("Travelling Start Date Period is not Verified in Email Body: " + fromDate);
					}
				}
			}
		}
		
		// Total Number of Members
				public static void readAndVerifyPeriodOfInsuranceTo() throws Exception {
					
					String toDate=travelEndDate;
					Thread.sleep(4000);
					waitForElement(By.xpath(
							"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
					List<WebElement> e1 = driver.findElements(By.xpath(
							"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
					int iSize = e1.size();
					for (int i = 0; i < iSize; i++) {
						String sumInsuredTestData = e1.get(i).getText();
						System.out.println(sumInsuredTestData);
						// String numOfMember = "1";
						if (sumInsuredTestData.contains(toDate)) {
							System.out.println("Travelling End Date Period is Verified in Email Body: " + toDate);
							break;
						} else {
							if (i == iSize - 1) {
								Assert.fail("Travelling End Date Period is not Verified in Email Body: " + toDate);
							}
						}
					}
				}

	/*// Total Number of Members
	public static void readAndVerifyPremium() throws Exception {
		scrolldown();
		String premium = AddonsforProducts.ExpectedpremiumAmount;
		Thread.sleep(4000);
		waitForElement(By.xpath(
				"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
		List<WebElement> e1 = driver.findElements(By.xpath(
				"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
		int iSize = e1.size();
		for (int i = 0; i < iSize; i++) {
			String sumInsuredTestData = e1.get(i).getText();
			System.out.println(sumInsuredTestData);
			if (sumInsuredTestData.contains(premium)) {
				System.out.println("Total Premium is Verified in Email Body: " + premium);
				break;
			} else {
				if (i == iSize - 1) {
					Assert.fail("Total Premium is not Verified in Email Body: " + premium);
				}
			}
		}
	}*/
				
	// Premium
	public static void readAndVerifyPremium() throws Exception {
		String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
		Thread.sleep(4000);
		waitForElement(By.xpath(
				"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[4]/td/table/tbody/tr[8]/td[3]/strong"));
		WebElement e1 = driver.findElement(By.xpath(
				"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[4]/td/table/tbody/tr[8]/td[3]/strong"));
		pointToElement(e1);
		String sumInsured1 = e1.getText();
		String sumInsured = sumInsured1.replaceAll("\\W", "");

		//String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
		// System.out.println(str);

		if (premiumValue.contains(sumInsured)) {
			System.out.println("Total Premium is Verified: " + sumInsured);
		} else {
			Assert.fail("Total Premium is not Verified: " + premiumValue);
		}
	}
	
	// Premium
		public static void readAndVerifyPremiumStudentExplore() throws Exception {
			String premiumValue = AddonsforProducts.ExpectedpremiumAmount;
			//String premiumValue ="2672";
			Thread.sleep(4000);
			waitForElement(By.xpath(
					"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td[3]/strong"));
			WebElement e1 = driver.findElement(By.xpath(
					"//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td[3]/strong"));
			pointToElement(e1);
			String sumInsured1 = e1.getText();
			String sumInsured = sumInsured1.replaceAll("\\W", "");

			//String sumInsured2 = sumInsured.substring(0, sumInsured.indexOf('f'));
			// System.out.println(str);

			if (premiumValue.contains(sumInsured)) {
				System.out.println("Total Premium is Verified: " + sumInsured);
			} else {
				Assert.fail("Total Premium is not Verified: " + premiumValue);
			}
		}
	
	// Total Number of Members
			public static void readAndVerifyTotalNumberOfMemebersForExplore(String SheetName, int Rownum, int Colnum)
					throws Exception {
				String[][] TestCaseData = BaseClass.excel_Files(SheetName);
				String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
				//String TotalMember = "1";
				Thread.sleep(4000);
				waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
				List<WebElement> e1 = driver.findElements(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]//following::table/tbody//td//tr//td"));
				int iSize=e1.size();
				for(int i=0; i<iSize; i++){
				String numOfMember = e1.get(i).getText();
				// String numOfMember = "1";
				if (numOfMember.contains(TotalMember)) {
					System.out.println("Total Number of Members are Verified: " + TotalMember);
					break;
				}
				else{
					if(i==iSize-1){
						Assert.fail("Total Number of Members are not Verified in Email Body: " + numOfMember);
					}
				}
				}
			}
			
	
	// Clicked on Buy Now Button from Email Link
		public static void clickOnBuyNowEmailLink() throws Exception {
			Thread.sleep(10000);
			waitForElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[5]/td/p[2]/a"));
			WebElement e1 = driver.findElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[5]/td/p[2]/a"));
			pointToElement(e1);
			//waitForElement(By.xpath(buyNow_LinkButton_In_MailBody_Xpath));
			clickElement(By.xpath("//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[5]/td/p[2]/a"));
			System.out.println("Clicked on Buy Now Link Button from Email");
			logger.log(LogStatus.PASS, "Clicked on Buy Now Link Button from Email");

		}
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
	public static void ClickOnBuyNowButtonFromEmail() throws Exception {
		//openEmail();
	    //clickOnRefreshButtonUntilNewemailNotDisplayed();
		clickOnBuyNowEmailLink();
		switchToNewTab();
		Thread.sleep(2000);

	}
	
	// Verify Proposal Generated GUID Should not be Reusable for Super Mediclaim Cancer
		public static void openEmailAndClickOnBuyNowButtonFromEmail() throws Exception {
			openEmail();
		    clickOnRefreshButtonUntilNewemailNotDisplayed();
			clickOnBuyNowEmailLink();
			switchToNewTab();
			Thread.sleep(2000);

		}
			//Age Group for Super Mediclaim Critical
			public static String ageGroupOfMember1;
			public static String ageGroupOfMember2;
			public static String ageGroupOfMember3;
			public static String ageGroupOfMember4;
			public static String ageGroupOfMember5;
			public static String ageGroupOfMember6;
			
			public static String readAgeGroupOfMember1(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a//following::div[1]/div/div[1]/div/li/a"));
				ageGroupOfMember1=e1.getText();
				if(ageGroupOfMember1.equalsIgnoreCase("> 80 Years")){
					ageGroupOfMember1="Above 80 Years";
				}
				return ageGroupOfMember1;
			}
			
			public static String readAgeGroupOfMember2(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a//following::div[1]/div/div[2]/div/li/a"));
				 ageGroupOfMember2=e1.getText();
				 if(ageGroupOfMember2.equalsIgnoreCase("> 80 Years")){
					 ageGroupOfMember2="Above 80 Years";
					}
				return ageGroupOfMember2;
			}
			
			public static String readAgeGroupOfMember3(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a//following::div[1]/div/div[3]/div/li/a"));
				 ageGroupOfMember3=e1.getText();
				 if(ageGroupOfMember3.equalsIgnoreCase("> 80 Years")){
					 ageGroupOfMember3="Above 80 Years";
					}
				return ageGroupOfMember3;
			}
			
			public static String readAgeGroupOfMember4(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a//following::div[1]/div/div[4]/div/li/a"));
				 ageGroupOfMember4=e1.getText();
				 if(ageGroupOfMember4.equalsIgnoreCase("> 80 Years")){
					 ageGroupOfMember4="Above 80 Years";
					}
				return ageGroupOfMember4;
			}
			public static String readAgeGroupOfMember5(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a//following::div[1]/div/div[5]/div/li/a"));
				 ageGroupOfMember5=e1.getText();
				 if(ageGroupOfMember5.equalsIgnoreCase("> 80 Years")){
					 ageGroupOfMember5="Above 80 Years";
					}
				return ageGroupOfMember5;
			}
			public static String readAgeGroupOfMember6(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a//following::div[1]/div/div[6]/div/li/a"));
				 ageGroupOfMember6=e1.getText();
				 if(ageGroupOfMember6.equalsIgnoreCase("> 80 Years")){
					 ageGroupOfMember6="Above 80 Years";
					}
				return ageGroupOfMember6;
			}
			
			public static String NumberOfMemebers;
			public static String numberOfMemebers(){
				WebElement e1=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='form-group ng-scope'][4]/div/a"));
				 NumberOfMemebers=e1.getText();
				return NumberOfMemebers;
			}
			
			public static String travelStartDate;
			public static String travelEndDate;
			public static void readDates() throws Exception{
				Thread.sleep(2000);
				WebElement stratDateElement=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='start_date']//input[@id='start_date']"));
				travelStartDate=stratDateElement.getAttribute("value");
				Thread.sleep(1000);
				WebElement endDateElement=driver.findElement(By.xpath("//div[@class='col-md-4 travelQuote']//descendant::div[@class='start_date']//input[@id='end_date']"));
				travelEndDate=endDateElement.getAttribute("value");
				
			}
			
			public static void closeEditPopupBox(){
				waitForElement(By.xpath("//div[contains(@class,'get_qout_modal_heading_container')]//following::p[1]/img"));
				WebElement e2=driver.findElement(By.xpath("//div[contains(@class,'get_qout_modal_heading_container')]//following::p[1]/img"));
				clickByJS(e2);
				//e2.click();
			}
			
			//Read Age Group based on number of members
			public static void readAgeGroupTravel() throws Exception{
				Thread.sleep(2000);
				String numberOfMemebers = numberOfMemebers();
				if(numberOfMemebers.equals("1")){
					readAgeGroupOfMember1();
				}
				else if(numberOfMemebers.equals("2")){
					readAgeGroupOfMember1();
					readAgeGroupOfMember2();
				}
				else if(numberOfMemebers.equals("3")){
					readAgeGroupOfMember1();
					readAgeGroupOfMember2();
					readAgeGroupOfMember3();
				}
				else if(numberOfMemebers.equals("4")){
					readAgeGroupOfMember1();
					readAgeGroupOfMember2();
					readAgeGroupOfMember3();
					readAgeGroupOfMember4();
				}
				else if(numberOfMemebers.equals("5")){
					readAgeGroupOfMember1();
					readAgeGroupOfMember2();
					readAgeGroupOfMember3();
					readAgeGroupOfMember4();
					readAgeGroupOfMember5();
				}
				else if(numberOfMemebers.equals("6")){
					readAgeGroupOfMember1();
					readAgeGroupOfMember2();
					readAgeGroupOfMember3();
					readAgeGroupOfMember4();
					readAgeGroupOfMember5();
					readAgeGroupOfMember6();
					
				}
				//closeEditPopupBox();
				
			}
	
			//Read and Verify age groups in email body
			public static void readAndVerifyAgeGroupMembersForOtherProducts(String SheetName, int RowNum) throws Exception {
				String NoOfMembers = NumberOfMemebers;
				String AgeGroupOfMemeber1 = ageGroupOfMember1;
				String AgeGroupOfMemeber2 = ageGroupOfMember2;
				String AgeGroupOfMemeber3 = ageGroupOfMember3;
				String AgeGroupOfMemeber4 = ageGroupOfMember4;
				String AgeGroupOfMemeber5 = ageGroupOfMember5;
				String AgeGroupOfMemeber6 = ageGroupOfMember6;
				
				
				
				
				Thread.sleep(4000);
                WebElement e1=driver.findElement(By.xpath(groupMembers_Travel_In_MailBody_Xpath));
                pointToElement(e1);
				String str = driver.findElement(By.xpath(groupMembers_Travel_In_MailBody_Xpath)).getText();
				String[] agegroup = str.split(",");

				l1: 
					for (int h=0; h<=agegroup.length; h++){
					
					//Based on Number of members we are segregating Age group of members
					String AgeGroupOfMemeber1_Mail=null;;
					String AgeGroupOfMemeber2_Mail=null;;
					String AgeGroupOfMemeber3_Mail=null;;
					String AgeGroupOfMemeber4_Mail=null;;
					String AgeGroupOfMemeber5_Mail=null;;
					String AgeGroupOfMemeber6_Mail=null;;
					
					switch(NoOfMembers){
					case "1":
						 AgeGroupOfMemeber1_Mail = agegroup[0];
						 break;
					case "2":
						 AgeGroupOfMemeber1_Mail = agegroup[0];
						 AgeGroupOfMemeber2_Mail = agegroup[1];
						 break;
					case "3":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						break;
					case "4":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						AgeGroupOfMemeber4_Mail = agegroup[3];
						break;
					case "5":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						AgeGroupOfMemeber4_Mail = agegroup[3];
						AgeGroupOfMemeber5_Mail = agegroup[4];
						break;
					case "6":
						AgeGroupOfMemeber1_Mail = agegroup[0];
						AgeGroupOfMemeber2_Mail = agegroup[1];
						AgeGroupOfMemeber3_Mail = agegroup[2];
						AgeGroupOfMemeber4_Mail = agegroup[3];
						AgeGroupOfMemeber5_Mail = agegroup[4];
						AgeGroupOfMemeber6_Mail = agegroup[5];
						break;
					
					}
					
                   //Age group of members are verifying
					switch (NoOfMembers) {
					case "1":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)) {
							System.out.println("AgeGroup is verified for Member 1: " + AgeGroupOfMemeber1_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "2":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)) {
							System.out.println("AgeGroup is verified for Member 1 and 2: " + AgeGroupOfMemeber1_Mail + " "
									+ AgeGroupOfMemeber2_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "3":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2 and 3: " + AgeGroupOfMemeber1_Mail + " "
									+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "4":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
								&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2, 3 and 4: " + AgeGroupOfMemeber1_Mail + " "
									+ AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " " + AgeGroupOfMemeber4_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not verified");
						}
						break;
					case "5":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
								&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
								&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2, 3, 4 and 5: " + AgeGroupOfMemeber1_Mail
									+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
									+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;
					case "6":
						if (AgeGroupOfMemeber1.equalsIgnoreCase(AgeGroupOfMemeber1_Mail)
								&& AgeGroupOfMemeber2.equalsIgnoreCase(AgeGroupOfMemeber2_Mail)
								&& AgeGroupOfMemeber3.equalsIgnoreCase(AgeGroupOfMemeber3_Mail)
								&& AgeGroupOfMemeber4.equalsIgnoreCase(AgeGroupOfMemeber4_Mail)
								&& AgeGroupOfMemeber5.equalsIgnoreCase(AgeGroupOfMemeber5_Mail)
								&& AgeGroupOfMemeber6.equalsIgnoreCase(AgeGroupOfMemeber6_Mail)) {
							System.out.println("AgeGroup is verified for Member 1, 2, 3, 4, 5 and 6: " + AgeGroupOfMemeber1_Mail
									+ " " + AgeGroupOfMemeber2_Mail + " " + AgeGroupOfMemeber3_Mail + " "
									+ AgeGroupOfMemeber4_Mail + " " + AgeGroupOfMemeber5_Mail + " " + AgeGroupOfMemeber6_Mail);
							break l1;
						} else {
							Assert.fail("Age Group is not Verified");
						}
						break;

					}
				}

			}
			
	// Read and Verify Data in Email Body For Travel Explore
	public static void readAndVerifyDataInEmailBody_TravelInsurance_Explore(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 1);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 12);
		readAndVerifyPeriodOfInsuranceFrom();
		readAndVerifyPeriodOfInsuranceTo();
		readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 8);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
	
	// Read and Verify Data in Email Body For Travel Explore
		public static void readAndVerifyDataInEmailBody_TravelInsurance_POSExplore(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyTravellingTo(SheetName, Rownum, 1);
			readAndVerifySumInsuredExplore(SheetName, Rownum, 9);
			readAndVerifyPeriodOfInsuranceFrom();
			readAndVerifyPeriodOfInsuranceTo();
			readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 5);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifyPremium();

		}

	// Read and Verify Data in Email Body For Travel Student Explore
	public static void readAndVerifyDataInEmailBody_TravelInsurance_StudentExplore(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 4);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 8);
		//readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 8);
		//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremiumStudentExplore();

	}
	
	// Read and Verify Data in Email Body For Travel POS Student Explore
		public static void readAndVerifyDataInEmailBody_TravelInsurance_POSStudentExplore(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyTravellingTo(SheetName, Rownum, 4);
			readAndVerifySumInsuredExplore(SheetName, Rownum, 8);
			//readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 8);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifyPremiumStudentExplore();

		}
			
	// Read and Verify Data in Email Body For Travel Group Explore
	public static void readAndVerifyDataInEmailBody_TravelInsurance_GroupExplore(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 1);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 10);
		readAndVerifyPeriodOfInsuranceFrom();
		readAndVerifyPeriodOfInsuranceTo();
		readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 6);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
	
	// Read and Verify Data in Email Body For Travel Group Student Explore
		public static void readAndVerifyDataInEmailBody_TravelInsurance_GroupStudentExplore(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyTravellingTo(SheetName, Rownum, 4);
			readAndVerifySumInsuredExplore(SheetName, Rownum, 8);
			//readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 8);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifyPremiumStudentExplore();

		}
		
	// Read and Verify Data in Email Body For Travel Explore
	public static void readAndVerifyDataInEmailBody_TravelInsurance_DomesticTravel(String SheetName, int Rownum)
			throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 1);
		//readAndVerifySumInsuredExplore(SheetName, Rownum, 10);
		//This logic implemented because once select one way trip another calendar is getting hide
		String triptype=DomesticTravel.TestCaseData[DomesticTravel.n][1].toString().trim();
		if(triptype.equalsIgnoreCase("One Way Trip")){
		}
		else{
			readAndVerifyPeriodOfInsuranceFrom();
			readAndVerifyPeriodOfInsuranceTo();
		}
		readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 6);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
	
	//************* Share Proposal Functions **********************************************
	
	// Read and Verify Data in Email Body For Travel Explore
		public static void readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_Explore(String SheetName, int Rownum)
				throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyTravellingTo(SheetName, Rownum, 1);
			readAndVerifySumInsuredExplore(SheetName, Rownum, 12);
			readAndVerifyPeriodOfInsuranceFrom();
			readAndVerifyPeriodOfInsuranceTo();
			readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 8);
			readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifyPremium();

		}
		
	// Read and Verify Data in Email Body For Travel - Group Explore
	public static void readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_GroupExplore(String SheetName,
			int Rownum) throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 1);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 10);
		readAndVerifyPeriodOfInsuranceFrom();
		readAndVerifyPeriodOfInsuranceTo();
		readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 6);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
	
	// Read and Verify Data in Email Body For Travel Explore
	public static void readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_POSExplore(String SheetName,
			int Rownum) throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 1);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 9);
		readAndVerifyPeriodOfInsuranceFrom();
		readAndVerifyPeriodOfInsuranceTo();
		readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 5);
		readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
			
	// Read and Verify Data in Email Body For Travel Student Explore
	public static void readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_POSStudentExplore(String SheetName,
			int Rownum) throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 4);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 8);
		//readAndVerifyPeriodOfInsuranceFrom();
		//readAndVerifyPeriodOfInsuranceTo();
		//readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 5);
		//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
	
	
	// Read and Verify Data in Email Body For Travel Student Explore
		public static void readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_GroupStudentExplore(String SheetName,
				int Rownum) throws Exception {
			clickOnRefreshButtonUntilNewemailNotDisplayed();
			readAndVerifyTravellingTo(SheetName, Rownum, 4);
			readAndVerifySumInsuredExplore(SheetName, Rownum, 8);
			//readAndVerifyPeriodOfInsuranceFrom();
			//readAndVerifyPeriodOfInsuranceTo();
			//readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 5);
			//readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
			readAndVerifyPremium();

		}
		
		
	// Read and Verify Data in Email Body For Travel Student Explore
	public static void readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_StudentExplore(
			String SheetName, int Rownum) throws Exception {
		clickOnRefreshButtonUntilNewemailNotDisplayed();
		readAndVerifyTravellingTo(SheetName, Rownum, 4);
		readAndVerifySumInsuredExplore(SheetName, Rownum, 8);
		// readAndVerifyPeriodOfInsuranceFrom();
		// readAndVerifyPeriodOfInsuranceTo();
		// readAndVerifyTotalNumberOfMemebersForExplore(SheetName, Rownum, 5);
		// readAndVerifyAgeGroupMembersForOtherProducts(SheetName, Rownum);
		readAndVerifyPremium();

	}
}
