package com.org.faveo.Base;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.InsuredDetails;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationShareProposalPage extends BaseClass implements AccountnSettingsInterface {
	
	public static String expectedCreationDateValue;
	public static String email;
	public static String mobile;

	public static void clickOnShareProposal(){
		getCurrentDateValue();
		scrolldown();
		//clickElement(By.xpath(shareProposal_Xpath));
		waitForElement(By.xpath(shareProposal_Xpath));
		WebElement e1=driver.findElement(By.xpath(shareProposal_Xpath));
		clickByJS(e1);
		System.out.println("Clicked on Share Proposal Button");
		logger.log(LogStatus.PASS, "Clicked on Share Proposal Button");
	}
	
	public static void clickOnShareProposal_Travel(){
		getCurrentDateValue();
		scrolldown();
		//clickElement(By.xpath(shareProposal_Xpath));
		waitForElement(By.xpath(shareProposalTravel_Xpath));
		WebElement e1=driver.findElement(By.xpath(shareProposalTravel_Xpath));
		clickByJS(e1);
		System.out.println("Clicked on Share Proposal Button");
		logger.log(LogStatus.PASS, "Clicked on Share Proposal Button");
	}
	
	//Get Current Date
		public static void getCurrentDateValue(){
			/*SimpleDateFormat f=new SimpleDateFormat("dd/MM/YYYY");
			Date d=new Date();
			 expectedCreationDateValue=f.format(d);
			System.out.println(expectedCreationDateValue);*/
			
			LocalDateTime ldt = LocalDateTime.now();
			expectedCreationDateValue=DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH).format(ldt);
			System.out.println(DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH).format(ldt));
		}
	
	//Set Email
		public static void setEmail(String SheetName, int rowNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteEmail = (TestCaseData[rowNum][3].toString().trim());
			clearTextfield(By.xpath(emailShareProposal_Xpath));
			enterText(By.xpath(emailShareProposal_Xpath), shareQuoteEmail);
			
			System.out.println("Data Entered for Quotation Share Email: " + shareQuoteEmail);
			logger.log(LogStatus.PASS, "Data Entered for Quotation Share Email: " + shareQuoteEmail);
			
		}
		
	
	// Set Email
		public static void setEmail_ShareProposal_TravelInsurance(String SheetName, int rowNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteEmail = (TestCaseData[rowNum][3].toString().trim());
			clearTextfield(By.xpath(emailShareProposalTravel_Xpath));
			enterText(By.xpath(emailShareProposalTravel_Xpath), shareQuoteEmail);

			System.out.println("Data Entered for Quotation Share Email: " + shareQuoteEmail);
			logger.log(LogStatus.PASS, "Data Entered for Quotation Share Email: " + shareQuoteEmail);

		}
		
		//Set SMS
		public static void setSms(String SheetName, int rowNum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteSms = (TestCaseData[rowNum][5].toString().trim());
			clearTextfield(By.xpath(phoneShareProposalXpath));
			enterText(By.xpath(phoneShareProposalXpath), shareQuoteSms);
			
			System.out.println("Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
			logger.log(LogStatus.PASS, "Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
		}
	
	// Set SMS
		public static void setSms_ShareProposal_TravelInsurance(String SheetName, int rowNum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteSms = (TestCaseData[rowNum][5].toString().trim());
			clearTextfield(By.xpath(sms_ShareProposal_Travel_Xpath));
			enterText(By.xpath(sms_ShareProposal_Travel_Xpath), shareQuoteSms);

			System.out.println("Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
			logger.log(LogStatus.PASS, "Data Entered for Get SMS Mobile Number: " + shareQuoteSms);
		}
		
		//Click on Email Slider
		public static void SetSmsSlider(){
			clickElement(By.xpath(smsSliderShareProposalXpath));
		}
		
		//Click on Send Button
		public static void clickOnSendAndSaveButton() throws Exception{
			Thread.sleep(5000);
			clickElement(By.xpath(saveAndShareProposalButton_Xpath));
			System.out.println("Clicked on Save and Share Proposal Button");
			logger.log(LogStatus.PASS, "Clicked on Save and Share Proposal Button");
		}
		
		
	// Click on Send Button
	public static void clickOnSendButton_TravelInsurance() throws Exception {
		Thread.sleep(3000);
		clickElement(By.xpath("//button[text()='Save & Share Proposal']"));
		System.out.println("Clicked on Send Button");
		logger.log(LogStatus.PASS, "Clicked on Send Button");
	}
	
	public static void clickOnYesButton(){
		waitForElement(By.xpath("//button[@ng-click='sendSaveContinueEmail()']"));
		clickElement(By.xpath("//button[@ng-click='sendSaveContinueEmail()']"));
	}
		
	// Close Share Quotation Poup Box
	public static void closePopUp() throws Exception {
		Thread.sleep(2000);
		//Fluentwait(By.xpath(closeShareProposalPopupBox_Xpath));
		ExplicitWait(By.xpath(closeShareProposalPopupBox_Xpath), 60);
		
		clickElement(By.xpath(closeShareProposalPopupBox_Xpath));

		System.out.println("Closed Share Quoation Poup");
		logger.log(LogStatus.PASS, "Closed Share Quotation popup");

	}
	
	//Read Email
		public static void readText() throws Exception{
			Thread.sleep(2000);
			 String txt1=driver.findElement(By.xpath(txtvalue1_ShareProposal_Xpath)).getAttribute("innerHTML");
			 String txt2=driver.findElement(By.xpath(txtvalue2_ShareProposal_Xpath)).getAttribute("innerHTML");
			 String txt3=txt1.concat(" ").concat(txt2);
			System.out.println("Email Dispalyed in Shared Quotation Popup: " + txt3);
			logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + txt3);
		}
		
	// Read Email
	public static String readEmail_TravelInsurance() throws Exception {
		Thread.sleep(2000);
		String Popupemail = driver.findElement(By.xpath(emailTextValueInPopupBox_TravelInsurance))
				.getAttribute("innerHTML");
		email = Popupemail;

		System.out.println("Email Dispalyed in Shared Quotation Popup: " + email);
		logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + email);
		return email;

	}

	// Read Mobile
	public static String readMobileNum_TravelInsurance() throws Exception {
		Thread.sleep(2000);
		String popupmobile = driver.findElement(By.xpath(SmsTextValueInPopupBox_TravelInsurance))
				.getAttribute("innerHTML");
		mobile = popupmobile;

		System.out.println("Mobile Number Dispalyed in Shared Quotation Popup: " + mobile);
		logger.log(LogStatus.PASS, "Email Dispalyed in Shared Quotation Popup: " + mobile);
		return mobile;
	}

	//Function for Set Details of Share Quotation inside Popup in Quotation Page
		public static void setDetailsOfShareQuotation(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
			String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
			
			//String shareQuoteOnlyByEmail="Yes";
			//String shareQuoteOnlyBySms="Yes";
			if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
				setEmail(SheetName, rowNum);
				SetSmsSlider();
				clickOnSendAndSaveButton();
				readText();
				
			}
			else{
				Thread.sleep(2000);
				setEmail(SheetName, rowNum);
				Thread.sleep(2000);
				setSms(SheetName, rowNum);
				Thread.sleep(2000);
				clickOnSendAndSaveButton();
				Thread.sleep(2000);
				readText();
				
			}
			
			//DataVerificationShareQuotationPage.getCurrentDateValue();
			getCurrentDateValue();
		}
		
		/*//Function for Set Details of Share Quotation inside Popup in Quotation Page
				public static void setDetailsOfShareQuotation_TravelInsurance(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);
					String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
					String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
					
					//String shareQuoteOnlyByEmail="Yes";
					//String shareQuoteOnlyBySms="Yes";
					if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
						setEmail(SheetName, rowNum);
						SetSmsSlider();
						clickOnSendAndSaveButton();
						readText();
						
					}
					else{
						setEmail_TravelInsurance(SheetName, rowNum);
						setSms_TravelInsurance(SheetName, rowNum);
						clickOnSendButton_TravelInsurance();
						clickOnYesButton();
						readEmail_TravelInsurance();
						readMobileNum_TravelInsurance();
						
					}
					
					//DataVerificationShareQuotationPage.getCurrentDateValue();
					getCurrentDateValue();
				}*/
				
				//Function for Set Details of Share Quotation inside Popup in Quotation Page
				public static void setDetailsOfShareProposal_TravelInsurance(String SheetName, int rowNum, int colNum1, int colNum2) throws Exception{
					String[][] TestCaseData = BaseClass.excel_Files(SheetName);
					String shareQuoteOnlyByEmail = (TestCaseData[rowNum][colNum1].toString().trim());
					String shareQuoteOnlyBySms = (TestCaseData[rowNum][colNum2].toString().trim());
					
					//String shareQuoteOnlyByEmail="Yes";
					//String shareQuoteOnlyBySms="Yes";
					if(shareQuoteOnlyByEmail.equalsIgnoreCase("Yes") && shareQuoteOnlyBySms.equalsIgnoreCase("No")){
						setEmail(SheetName, rowNum);
						SetSmsSlider();
						clickOnSendAndSaveButton();
						readText();
						
					}
					else{
						setEmail_ShareProposal_TravelInsurance(SheetName, rowNum);
						setSms_ShareProposal_TravelInsurance(SheetName, rowNum);
						clickOnSendButton_TravelInsurance();
						clickOnYesButton();
						/*readEmail_TravelInsurance();
						readMobileNum_TravelInsurance();*/
						email=shareQuoteOnlyByEmail;
						mobile=shareQuoteOnlyBySms;
						
						
					}
					
					getCurrentDateValue();
				}
				
				
		//Click on Quotation Tracker
		public static void clickOnDraftHistory() throws Exception{
			Thread.sleep(2000);
			Fluentwait(By.xpath(draftHistory_Xpath));
			clickElement(By.xpath(draftHistory_Xpath));
			System.out.println("Select Draft History");
			logger.log(LogStatus.PASS, "Select Draft History");
		}
		
		//Function for Verify the Data in Draft History
		public static void readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldBeAvailable(String shteetName1, int FirstNameOFCustomerColNum, int SecondNameOFCustomerColNum,int rowNum, String ProductName) throws Exception{
			Thread.sleep(5000);
			
			List<WebElement> CreadtionDatelist=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[3]"));
			List<WebElement> productList=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[2]"));
			List<WebElement> CustomerNameList=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[1]"));
			
			int i=0;
			int j=0;
			
			String ExpectedValueOfdates=expectedCreationDateValue;
			//String ExpectedValueOfdates="24/12/2018";
			
			String ExcpectedValueOfCustomerNames=null;
			//Verify Product then read the excel File
			if(ProductName.equalsIgnoreCase("Cancer Mediclaim")){
				String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
			    ExcpectedValueOfCustomerNames = TestCaseData[rowNum][FirstNameOFCustomerColNum].toString().trim();
			}
			else
			{
				String[][] TestCaseData = BaseClass.excel_Files1(shteetName1);
				String FirstName = TestCaseData[rowNum][FirstNameOFCustomerColNum].toString().trim();
				 String lastName = TestCaseData[rowNum][SecondNameOFCustomerColNum].toString().trim();
				 ExcpectedValueOfCustomerNames=FirstName +" "+lastName;
			}
			
			//String ExpecetedValueOfproducts="Cancer Mediclaim";
			String ExpecetedValueOfproducts=ProductName;
			
			//String ExcpectedValueOfCustomerNames="Ashish Singh";
			
			System.out.println("Expected Data From Shared Proposal Page - Creation Date: "+ExpectedValueOfdates+","+"Product Names: "+ExpecetedValueOfproducts+","+"Customer Names: "+ExcpectedValueOfCustomerNames);
			logger.log(LogStatus.PASS, "Expected Data From Shared Proposal Page - Creation Date: "+ExpectedValueOfdates+","+"Product Names: "+ExpecetedValueOfproducts+","+"Customer Names: "+ExcpectedValueOfCustomerNames);
			
			
			for(WebElement dates:CreadtionDatelist){
				
				String ActualValueOfdates=dates.getText();
				String AcutalValueOfproducts=productList.get(i).getText();
				//String AcutalValueOfproducts="CARE";
				String AcutalValueOfCustomerNames=CustomerNameList.get(j).getText();
				
				System.out.println("Actual Data From Shared Proposal Page - Creation Date: "+ActualValueOfdates+","+"Product Names: "+AcutalValueOfproducts+","+"Customer Names: "+AcutalValueOfCustomerNames);
				logger.log(LogStatus.PASS, "Actual Data From Shared Proposal Page - Creation Date: "+ActualValueOfdates+","+"Product Names: "+AcutalValueOfproducts+","+"Customer Names: "+AcutalValueOfCustomerNames);
				
				if(ActualValueOfdates.equalsIgnoreCase(ExpectedValueOfdates) && AcutalValueOfproducts.equalsIgnoreCase(ExpecetedValueOfproducts) && AcutalValueOfCustomerNames.equalsIgnoreCase(ExcpectedValueOfCustomerNames)){
                 	System.out.println("Verified the Actual and Expected Shared Proposal Data Inside Draft History and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);
	                logger.log(LogStatus.PASS, "Verified the Actual and Expected Shared Proposal Data Inside Draft History and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);			
				break;
				}
				else{
					Assert.fail("Shared Proposal Data Inside Draft History not Verified and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);
				}
					
			}
			
		}
		
		//Function for Verify the Data in Draft History
				public static void readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldBeAvailable2(String shteetName1, int NameOFCustomerColNum, int rowNum, String ProductName) throws Exception{
					Thread.sleep(5000);
					
					List<WebElement> CreadtionDatelist=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[3]"));
					List<WebElement> productList=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[2]"));
					List<WebElement> CustomerNameList=driver.findElements(By.xpath("//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[1]"));
					
					int i=0;
					int j=0;
					
					String ExpectedValueOfdates=expectedCreationDateValue;
					//String ExpectedValueOfdates="24/12/2018";
					
					String ExcpectedValueOfCustomerNames=null;
					//Verify Product then read the excel File
					if(ProductName.equalsIgnoreCase("Cancer Mediclaim")){
						String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
					    ExcpectedValueOfCustomerNames = TestCaseData[rowNum][NameOFCustomerColNum].toString().trim();
					}
					else
					{
						String[][] TestCaseData = BaseClass.excel_Files1(shteetName1);
						String FirstName = TestCaseData[rowNum][NameOFCustomerColNum].toString().trim();
						 ExcpectedValueOfCustomerNames=FirstName;
					}
					
					//String ExpecetedValueOfproducts="Cancer Mediclaim";
					String ExpecetedValueOfproducts=ProductName;
					
					//String ExcpectedValueOfCustomerNames="Ashish Singh";
					
					System.out.println("Expected Data From Shared Proposal Page - Creation Date: "+ExpectedValueOfdates+","+"Product Names: "+ExpecetedValueOfproducts+","+"Customer Names: "+ExcpectedValueOfCustomerNames);
					logger.log(LogStatus.PASS, "Expected Data From Shared Proposal Page - Creation Date: "+ExpectedValueOfdates+","+"Product Names: "+ExpecetedValueOfproducts+","+"Customer Names: "+ExcpectedValueOfCustomerNames);
					
					
					for(WebElement dates:CreadtionDatelist){
						
						String ActualValueOfdates=dates.getText();
						String AcutalValueOfproducts=productList.get(i).getText();
						String AcutalValueOfCustomerNames=CustomerNameList.get(j).getText();
						
						System.out.println("Actual Data From Shared Proposal Page - Creation Date: "+ActualValueOfdates+","+"Product Names: "+AcutalValueOfproducts+","+"Customer Names: "+AcutalValueOfCustomerNames);
						logger.log(LogStatus.PASS, "Actual Data From Shared Proposal Page - Creation Date: "+ActualValueOfdates+","+"Product Names: "+AcutalValueOfproducts+","+"Customer Names: "+AcutalValueOfCustomerNames);
						
						if(ActualValueOfdates.equalsIgnoreCase(ExpectedValueOfdates) && AcutalValueOfproducts.contains(ExpecetedValueOfproducts) && AcutalValueOfCustomerNames.equalsIgnoreCase(ExcpectedValueOfCustomerNames)){
		                 	System.out.println("Verified the Actual and Expected Shared Proposal Data Inside Draft History and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);
			                logger.log(LogStatus.PASS, "Verified the Actual and Expected Shared Proposal Data Inside Draft History and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);			
						break;
						}
						else{
							Assert.fail("Shared Proposal Data Inside Draft History not Verified and value of Creation Date:" + ActualValueOfdates +"," +" Product Name: "+ AcutalValueOfproducts+ ","+ "Customer Name: "+AcutalValueOfCustomerNames);
						}
							
					}
					
				}
		
		
	
	
	// Function for Verify the Data in Draft History
		public static void readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldNotBeAvailable(String shteetName1,
				int FirstNameOfCustomerColNum,int SecondNameOfCustomerColNum, int rowNum, String ProductName) throws Exception {
			Thread.sleep(5000);

			List<WebElement> CreadtionDatelist = driver.findElements(By.xpath(
					"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[3]"));
			List<WebElement> productList = driver.findElements(By.xpath(
					"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[2]"));
			List<WebElement> CustomerNameList = driver.findElements(By.xpath(
					"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[1]"));

			int i = 0;
			int j = 0;

			String ExpectedValueOfdates=expectedCreationDateValue;
			//String ExpectedValueOfdates = "18/12/2018";
			
			/*String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
			String ExcpectedValueOfCustomerNames = TestCaseData[rowNum][CustomerNameColNum].toString().trim();*/
			
			String ExcpectedValueOfCustomerNames=null;
			//Verify Product then read the excel File
			if(ProductName.equalsIgnoreCase("Cancer Mediclaim")){
				String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
			    ExcpectedValueOfCustomerNames = TestCaseData[rowNum][FirstNameOfCustomerColNum].toString().trim();
			}
			else
			{
				String[][] TestCaseData = BaseClass.excel_Files1(shteetName1);
				String FirstName = TestCaseData[rowNum][FirstNameOfCustomerColNum].toString().trim();
				 String lastName = TestCaseData[rowNum][SecondNameOfCustomerColNum].toString().trim();
				 ExcpectedValueOfCustomerNames=FirstName +" "+lastName;
			}

			// String ExpecetedValueOfproducts="Cancer Mediclaim";
			String ExpecetedValueOfproducts = ProductName;

			// String ExcpectedValueOfCustomerNames="Ashish Singh";

			System.out.println("Expected Data From Shared Proposal Page - Creation Date: " + ExpectedValueOfdates + ","+ "Product Names: "+ ExpecetedValueOfproducts +","+"Customer Names: "+ ExcpectedValueOfCustomerNames);
			logger.log(LogStatus.PASS,"Expected Data From Shared Proposal Page - Creation Date: " + ExpectedValueOfdates + ","+ "Product Names: "+ ExpecetedValueOfproducts+","+"Customer Names: "+ ExcpectedValueOfCustomerNames);

			for (WebElement dates : CreadtionDatelist) {

				String ActualValueOfdates = dates.getText();
				
				String AcutalValueOfproducts = productList.get(i).getText();
				//String AcutalValueOfproducts = "CARE";
			    /*String AcutalValueOfproducts = "null";
				if(productList.get(i).getText().toString().isEmpty()){
					AcutalValueOfproducts = "CARE";
				}
				else{
					 AcutalValueOfproducts = productList.get(i).getText();
				}*/
				
				String AcutalValueOfCustomerNames = CustomerNameList.get(j).getText();

				System.out.println("Actual Data From Shared Proposal Page - Creation Date: " + ActualValueOfdates + ","+ "Product Names: " + AcutalValueOfproducts +","+"Customer Names: " + AcutalValueOfCustomerNames);
				logger.log(LogStatus.PASS,"Actual Data From Shared Proposal Page - Creation Date: " + ActualValueOfdates + ","+ "Product Names: " + AcutalValueOfproducts +","+"Customer Names: "+ AcutalValueOfCustomerNames);

				if((!ExpectedValueOfdates.equals(ActualValueOfdates)) 
						|| (!ExpecetedValueOfproducts.contains(AcutalValueOfproducts))
						|| (!ExcpectedValueOfCustomerNames.equals(AcutalValueOfCustomerNames)))
				{
					System.out.println("Verfied Share Porposal data has been removed from Draft History");
					break;
				}
				else{
					System.out.println("Share Porposal data is still available in Draft History");
				}
	            
			}

		}
		
		
		// Function for Verify the Data in Draft History
		public static void readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldNotBeAvailable2(String shteetName1,
				int NameOfCustomerColNum, int rowNum, String ProductName) throws Exception {
			Thread.sleep(5000);

			List<WebElement> CreadtionDatelist = driver.findElements(By.xpath(
					"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[3]"));
			List<WebElement> productList = driver.findElements(By.xpath(
					"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[2]"));
			List<WebElement> CustomerNameList = driver.findElements(By.xpath(
					"//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[1]"));

			int i = 0;
			int j = 0;

			String ExpectedValueOfdates=expectedCreationDateValue;
			
			String ExcpectedValueOfCustomerNames=null;
			
			//Verify Product then read the excel File
			if(ProductName.equalsIgnoreCase("Cancer Mediclaim")){
				String[][] TestCaseData = BaseClass.excel_Files(shteetName1);
			    ExcpectedValueOfCustomerNames = TestCaseData[rowNum][NameOfCustomerColNum].toString().trim();
			}
			else
			{
				String[][] TestCaseData = BaseClass.excel_Files1(shteetName1);
				String FirstName = TestCaseData[rowNum][NameOfCustomerColNum].toString().trim();
				 ExcpectedValueOfCustomerNames=FirstName;
			}

			// String ExpecetedValueOfproducts="Cancer Mediclaim";
			String ExpecetedValueOfproducts = ProductName;

			// String ExcpectedValueOfCustomerNames="Ashish Singh";

			System.out.println("Expected Data From Shared Proposal Page - Creation Date: " + ExpectedValueOfdates + ","+ "Product Names: "+ ExpecetedValueOfproducts +","+"Customer Names: "+ ExcpectedValueOfCustomerNames);
			logger.log(LogStatus.PASS,"Expected Data From Shared Proposal Page - Creation Date: " + ExpectedValueOfdates + ","+ "Product Names: "+ ExpecetedValueOfproducts+","+"Customer Names: "+ ExcpectedValueOfCustomerNames);

			for (WebElement dates : CreadtionDatelist) {

				String ActualValueOfdates = dates.getText();
				String AcutalValueOfproducts = productList.get(i).getText();
				String AcutalValueOfCustomerNames = CustomerNameList.get(j).getText();

				System.out.println("Actual Data From Shared Proposal Page - Creation Date: " + ActualValueOfdates + ","+ "Product Names: " + AcutalValueOfproducts +","+"Customer Names: " + AcutalValueOfCustomerNames);
				logger.log(LogStatus.PASS,"Actual Data From Shared Proposal Page - Creation Date: " + ActualValueOfdates + ","+ "Product Names: " + AcutalValueOfproducts +","+"Customer Names: "+ AcutalValueOfCustomerNames);

				if((!ExpectedValueOfdates.equals(ActualValueOfdates)) 
						|| (!AcutalValueOfproducts.contains(ExpecetedValueOfproducts))
						|| (!ExcpectedValueOfCustomerNames.equals(AcutalValueOfCustomerNames)))
				{
					System.out.println("Verfied Share Porposal data has been removed from Draft History");
					break;
				}
				else{
					System.out.println("Share Porposal data is still available in Draft History");
				}
	            
			}

		}
		
		
		//Function For verify Data in Draft History
		public static void verifyDataInDraftHistory(String SheetName1,int FirstNameOfCustomerColNum,int SecondNameOfCustomerColNum, int rowNum,String ProductName) throws Exception{
			DataVerificationShareQuotationPage.clickOnHaburgerMenu();
			clickOnDraftHistory();
			Thread.sleep(20000);
			scrollup();
			readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldBeAvailable(SheetName1,FirstNameOfCustomerColNum,SecondNameOfCustomerColNum,rowNum,ProductName);

		}
		
	// Function For verify Data in Draft History
	public static void verifyDataInDraftHistory2(String SheetName1, int NameOfCustomerColNum, int rowNum,
			String ProductName) throws Exception {
		DataVerificationShareQuotationPage.clickOnHaburgerMenu();
		clickOnDraftHistory();
		Thread.sleep(10000);
		scrollup();
		readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldBeAvailable2(SheetName1, NameOfCustomerColNum,
				rowNum, ProductName);

	}
		
	// Function For verify Data in Draft History
	public static void verifyDataShouldNotAvailableInDraftHistory(String SheetName1, int FirstNameOfCustomerColNum,int LastNameOfCusctomerColnum, int rowNum, String ProductName)
			throws Exception {
		DataVerificationShareQuotationPage.clickOnHaburgerMenu();
		clickOnDraftHistory();
		scrollup();
		readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldNotBeAvailable(SheetName1, FirstNameOfCustomerColNum,LastNameOfCusctomerColnum, rowNum,ProductName);

	}
	
	// Function For verify Data in Draft History
	public static void verifyDataShouldNotAvailableInDraftHistory2(String SheetName1, int NameOfCustomerColNum,
			 int rowNum, String ProductName) throws Exception {
		DataVerificationShareQuotationPage.clickOnHaburgerMenu();
		clickOnDraftHistory();
		scrollup();
		readAndVerifyDataInDraftHistoryToCheckSharePropalDataShouldNotBeAvailable2(SheetName1, NameOfCustomerColNum,rowNum, ProductName);

	}
		
		//Function for click on Resume Policy from Draft History
		public static void clickOnResumePolicy() throws Exception{
			WebElement e1=driver.findElement(By.xpath(resumeProposalLink_Xpath));
			Point p=e1.getLocation();
			int y=p.getY();
			
			((JavascriptExecutor)driver).executeScript("window.scroll("+y+",0);");
			
			Thread.sleep(2000);
			clickElement(By.xpath(resumeProposalLink_Xpath));
			System.out.println("Clicked on Resume Proposal");
			logger.log(LogStatus.PASS, "Clicked on Resume Proposal");
		}
		
		public static void clickOnNextButtonFromProposarDetailsPage() throws Exception{
			//Thread.sleep(5000);
			//waitForElement(By.id(Button_NextProposer_id));
			
			waitTillElementToBeClickable(By.id(Button_NextProposer_id));
			Thread.sleep(10000);
			driver.findElement(By.id(Button_NextProposer_id)).click();
			//clickElement(By.id(Button_NextProposer_id));
			System.out.println("Clicked On Next Button in Proposer Detail Page and Moved to Next Page");
			logger.log(LogStatus.PASS, "Clicked On Next Button in Proposer Detail Page and Moved to Next Page");
		}
		
		public static void clickOnNextButtonFromInsuredDetailsPage() throws InterruptedException{
			Thread.sleep(5000);
			waitForElement(By.xpath(nextButtonSuperMediclaim_Xpath));
			WebElement e1=driver.findElement(By.xpath(nextButtonSuperMediclaim_Xpath));
			clickByJS(e1);
			//clickElement(By.xpath(nextButtonSuperMediclaim_Xpath));
			System.out.println("Clicked On Next Button in Insured Detail Page and Moved to Next Page");
			logger.log(LogStatus.PASS, "Clicked On Next Button in Insured Detail Page and Moved to Next Page");
		}
		
		public static void verifyPageTitle(){
			String expectedTitle="Religare Health Insurance";
			String acutalTitle=driver.getTitle();
			Assert.assertEquals(expectedTitle, acutalTitle);
			System.out.println("Application Title is verified and GUID is Reusable for multiple Times");
			logger.log(LogStatus.PASS, "Application Title is verified and GUID is Reusable for multiple Times");
		}
	
	//**********************************************************************************************	
		public static void setDetailsOfShareProposalPoupBox(int rowNum) throws Exception{
		clickOnShareProposal();
		setDetailsOfShareQuotation("ShareProposal", rowNum, 6, 7);
		closePopUp();
	}
	
	// **********************************************************************************************
	public static void setDetailsOfShareProposalPoupBox_TravelInsurance(int rowNum) throws Exception {
		clickOnShareProposal_Travel();
		setDetailsOfShareProposal_TravelInsurance("ShareProposal", rowNum, 6, 7);
		closePopUp();
	}
		
		
// ***************** SuperMediclaim Cancer ****************************
	// Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
	public static void verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCancer(int rowNum) throws Exception {
		verifyDataInDraftHistory("SuperMediclaimCancer_TestData", 2,3, rowNum, "Cancer Mediclaim");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCancer(int rowNum) throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("SuperMediclaimCancer_TestData", 2,3, rowNum, "Cancer Mediclaim");

	}

	/*// Verify Data for Share Proposal In Draft History and Punch the Policy on Click Resume Policy
	public static void verifyDataInOfShareProposalInDrafthistoryAndPunchThePolicyOnClickResumePolicy_ForSuperMediclaimCancer(int rowNum)
			throws Exception {
		verifyDataInDraftHistory("SuperMediclaimCancer_TestData", 2,3, rowNum, "Cancer Mediclaim");
		clickOnResumePolicy();
		switchToNewTab();
		clickOnNextButtonFromProposarDetailsPage();
		clickOnNextButtonFromInsuredDetailsPage();

	}*/
	
	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareProposalInMail_ForSuperMediclaimCancer(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal("SuperMediclaimCancer_TestData", rowNum);

	}
	
	// Verify Data for Share Proposal In Draft History and Punch the Policy on Click Resume Policy
	public static void setDetailsAfterResumePolicy()throws Exception {
		clickOnResumePolicy();
		switchToNewTab();
		clickOnNextButtonFromProposarDetailsPage();
		clickOnNextButtonFromInsuredDetailsPage();

	}
	
	// Verify Data for Share Proposal In Draft History and Punch the Policy on Click Resume Policy for Travel
	public static void setDetailsAfterResumePolicyTravel() throws Exception {
		clickOnResumePolicy();
		switchToNewTab();
	}
	
	//********** Super Mediclaim Critical **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function for SuperMedclaim
		public static void verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCritical(int rowNum) throws Exception {
			verifyDataInDraftHistory("SuperMediclaimCriticalTestCase", 2,3, rowNum, "Critical Mediclaim");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCritical(int rowNum) throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("SuperMediclaimCriticalTestCase", 2,3, rowNum, "Critical Mediclaim");

		}
		
		// Verify Data for Share Proposal In Draft History and Punch the Policy on Click Resume Policy
		public static void verifyDataInOfShareProposalInDrafthistoryAndPunchThePolicyOnClickResumePolicy_ForSuperMediclaimCritical(int rowNum)
				throws Exception {
			verifyDataInDraftHistory("SuperMediclaimCriticalTestCase", 2,3, rowNum, "Critical Mediclaim");
			clickOnResumePolicy();
			switchToNewTab();
			clickOnNextButtonFromProposarDetailsPage();
			clickOnNextButtonFromInsuredDetailsPage();

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForSuperMediclaimCrtical(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimCritical("SuperMediclaimCriticalTestCase", rowNum);

		}
		
	// ********** Care With NCB **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
	public static void verifyDataOfShareProposalInDraftHistory_ForCareWithNCB(int rowNum) throws Exception {
		verifyDataInDraftHistory("Care_Quotation_Data", 2, 3, rowNum, "CARE");

	}

	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareWithNCB(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("Care_Quotation_Data", 2, 3, rowNum, "CARE");

	}

	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareProposalInMail_ForCareWithNCB(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareWithNCB("Care_Quotation_Data", rowNum);

	}
	
	// ********** Super Mediclaim Heart **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamHeart(int rowNum) throws Exception {
			verifyDataInDraftHistory("Heart_Quotation", 2, 3, rowNum, "Heart Mediclaim");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamHeart(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("Heart_Quotation", 2, 3, rowNum, "Heart Mediclaim");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForSuperMedicliamHeart(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimHeart("Heart_Quotation", rowNum);

		}
		
		// ********** Super Mediclaim Operation **************************************
				// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamOperation(int rowNum) throws Exception {
					verifyDataInDraftHistory("Operation_Quotation", 2, 3, rowNum, "Operation Mediclaim");

				}
				
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamOperation(int rowNum)
						throws Exception {
					verifyDataShouldNotAvailableInDraftHistory("Operation_Quotation", 2, 3, rowNum, "Operation Mediclaim");

				}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
				public static void verifyDataOfShareProposalInMail_ForSuperMedicliamOperation(int rowNum) throws Exception {
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimOperation("Operation_Quotation", rowNum);

				}
				
	// ********** Super Mediclaim Operation **************************************Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamCritical(int rowNum) throws Exception {
					verifyDataInDraftHistory("SuperMediclaimCriticalTestCase", 2, 3, rowNum, "Critical Mediclaim");

				}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamCritical(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("SuperMediclaimCriticalTestCase", 2, 3, rowNum, "Critical Mediclaim");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForSuperMedicliamCritical(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_SuperMediclaimCritical("SuperMediclaimCriticalTestCase", rowNum);

		}
		
	// ********** Care Heart **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForCareHeart(int rowNum) throws Exception {
			verifyDataInDraftHistory("CareHeart_Quotation", 2, 3, rowNum, "CARE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareHeart(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("CareHeart_Quotation", 2, 3, rowNum, "CARE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForCareHeart(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareHeart("CareHeart_Quotation", rowNum);

		}
	
	// ********** POS Care With NCB **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForPOSCareWithNCB(int rowNum) throws Exception {
			verifyDataInDraftHistory("POSCare_With_NCB_TestCaseData", 2, 3, rowNum, "POS CARE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareWithNCB(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("POSCare_With_NCB_TestCaseData", 2, 3, rowNum, "POS CARE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForPOSCareWithNCB(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_POSCareWithNCB("POSCare_With_NCB_TestCaseData", rowNum);

		}
	
	// ********** Enhnace **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForEnhance(int rowNum) throws Exception {
			verifyDataInDraftHistory2("Enhance_Quotation", 2, rowNum, "Enhance - 1");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForEnhance(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory2("Enhance_Quotation", 2, rowNum, "Enhance - 1");

		}
		
		public static void verifyDataOfShareProposalInMail_ForEnhnace(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_Enhance("Enhance_Quotation", rowNum);

		}
		
	// ********** Secure **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function

	public static void verifyDataOfShareProposalInDraftHistory_ForSecure(int rowNum) throws Exception {
		verifyDataInDraftHistory2("Secure_Quotation", 2, rowNum, "SECURE");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSecure(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory2("Secure_Quotation", 2, rowNum, "SECURE");

	}
	
	public static void verifyDataOfShareProposalInMail_ForSecure(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_Secure("Secure_Quotation", rowNum);

	}
	
	// ********** POS Secure **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
	public static void verifyDataOfShareProposalInDraftHistory_ForPOSSecure(int rowNum) throws Exception {
		verifyDataInDraftHistory2("POSSecQuotation", 2, rowNum, "SECURE");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSSecure(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory2("POSSecQuotation", 2, rowNum, "SECURE");

	}
	
	public static void verifyDataOfShareProposalInMail_ForPOSSecure(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_POSSecure("POSSecQuotation", rowNum);

	}
	
	// ********** Care Freedom **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForCareFreedom(int rowNum) throws Exception {
			verifyDataInDraftHistory("Care_Freedom_Quotation", 2, 3, rowNum, "CARE");

		}
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareFreedom(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("Care_Freedom_Quotation", 2, 3, rowNum, "CARE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCare(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("Care_Freedom_Quotation", 2, 3, rowNum, "CARE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForCareFreedom(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareFreedom("Care_Freedom_Quotation", rowNum);

		}
		
	// ********** POS Care Freedom **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
	public static void verifyDataOfShareProposalInDraftHistory_ForPOSCareFreedom(int rowNum) throws Exception {
		verifyDataInDraftHistory2("PosCarefreedom_Quotation_Data", 2, rowNum, "POS Care Freedom");

	}

	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareFreedom(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory2("PosCarefreedom_Quotation_Data", 2, rowNum, "POS Care Freedom");

	}
			public static void verifyDataOfShareProposalInMail_ForPOSCareFreedom(int rowNum) throws Exception {
				ReadDataFromEmail.openEmail();
				ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_POSCareFreedom("PosCarefreedom_Quotation_Data", rowNum);

			}
			
			// ********** Care with OPD **************************************
			// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
			public static void verifyDataOfShareProposalInDraftHistory_ForCareWithOPD(int rowNum) throws Exception {
				verifyDataInDraftHistory("CareWithOPD_Quotation", 2,3, rowNum, "Care");

			}
			
			public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareWithOPD(int rowNum)
					throws Exception {
				verifyDataShouldNotAvailableInDraftHistory("CareWithOPD_Quotation", 2,3, rowNum, "Care");

			}
			
			// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
			public static void verifyDataOfShareProposalInMail_ForCareWithOPD(int rowNum) throws Exception {
				ReadDataFromEmail.openEmail();
				ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareWithOPD("CareWithOPD_Quotation",
						rowNum);

			}
			
		
		// ********** Care Smart Select **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForCareSmartSelect(int rowNum) throws Exception {
					verifyDataInDraftHistory2("SmartSelect_Quotation", 2, rowNum, "CARE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareSmartSelect(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory2("SmartSelect_Quotation", 2, rowNum, "CARE");

		}
		
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareProposalInMail_ForCareSmartSelect(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareSmartSelect("SmartSelect_Quotation", rowNum);

		}
		
	
	// ********** POS Care Smart Select **************************************
			// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
			public static void verifyDataOfShareProposalInDraftHistory_ForPOSCareSmartSelect(int rowNum) throws Exception {
				verifyDataInDraftHistory("POSSmartSelect_Quotation", 2,3, rowNum, "CARE");

			}
			
			public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareSmartSelect(int rowNum)
					throws Exception {
				verifyDataShouldNotAvailableInDraftHistory("POSSmartSelect_Quotation", 2,3, rowNum, "CARE");

			}
			
			// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
			public static void verifyDataOfShareProposalInMail_ForPOSCareSmartSelect(int rowNum) throws Exception {
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_POSCareSmartSelect("POSSmartSelect_Quotation", rowNum);

				}
	
		// ********** Care Global **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForCareGlobal(int rowNum) throws Exception {
			verifyDataInDraftHistory2("CareGlobal_Quotation", 2, rowNum, "CARE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareGlobal(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory2("CareGlobal_Quotation", 2, rowNum, "CARE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForCareGlobal(int rowNum) throws Exception {
					ReadDataFromEmail.openEmail();
					ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareGlobal("CareGlobal_Quotation", rowNum);

				}
		
//************* Care Senior **************************************
	// Verify Data for Share Proposal In Draft History Function forCareSenior
	public static void verifyDataOfShareProposalInDraftHistory_ForCareSenior(int rowNum) throws Exception {
		verifyDataInDraftHistory("Senior_Quotation_Data", 2, 3, rowNum, "CARE SENIOR");

	}
	
	// Verify Data of Share Proposal In Email for Care Senior
	public static void verifyDataOfShareProposalInMail_ForCareSenior(int rowNum) throws Exception {
		ReadDataFromEmail.openEmail();
		ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareSenior("Senior_Quotation_Data", rowNum);

	}
	
	// Verify Data should not be available for Share Proposal In Draft History Function forCareSenior
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareSenior(int rowNum) throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("Senior_Quotation_Data", 2,3, rowNum, "CARE SENIOR");

	}
	
	//*************POS Care Senior **************************************
		// Verify Data for Share Proposal In Draft History Function forCareSenior
		public static void verifyDataOfShareProposalInDraftHistory_ForPOSCareSenior(int rowNum) throws Exception {
			verifyDataInDraftHistory("POScaresenior_Quotation", 2, 3, rowNum, "POS CARE SENIOR");

		}
		
		// Verify Data of Share Proposal In Email for Care Senior
		public static void verifyDataOfShareProposalInMail_ForPOSCareSenior(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_POSCareSenior("POScaresenior_Quotation", rowNum);

		}
		
		// Verify Data should not be available for Share Proposal In Draft History Function forCareSenior
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareSenior(int rowNum) throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("POScaresenior_Quotation", 2,3, rowNum, "POS CARE SENIOR");

		}
	
	// ********** Care HNI **************************************
		// Verify Data for Share Quotation In Quotation Tracker Function forSuperMedclaim
		public static void verifyDataOfShareProposalInDraftHistory_ForCareHNI(int rowNum) throws Exception {
			
		verifyDataInDraftHistory2("CareHNI_Quotation", 2, rowNum, "CARE");

		}

		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareHNI(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory2("CareHNI_Quotation", 2, rowNum, "CARE");

		}

		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForCareHNI(int rowNum) throws Exception {
			ReadDataFromEmail.openEmail();
			ReadDataFromEmail.readAndVerifyDataInEmailBody_ShareProposal_CareHNI("CareHNI_Quotation", rowNum);

		}
	
	// ********** Travel - Explore  **************************************
	// Verify Data for Share Quotation In Quotation Tracker Function
	// forSuperMedclaim
	public static void verifyDataOfShareProposalInDraftHistory_ForTravelExplore(int rowNum) throws Exception {

		verifyDataInDraftHistory("Explore_Quotation", 17,18, rowNum, "EXPLORE");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelExplore(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("Explore_Quotation", 17,18, rowNum, "EXPLORE");

	}
	
	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareProposalInMail_ForTravelExplore(int rowNum) throws Exception {
		ReadDataFromEmail_TravelInsurance.openEmail();
		ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_Explore("Explore_Quotation", rowNum);

	}
	
	// ********** Travel - Group Explore  **************************************
		public static void verifyDataOfShareProposalInDraftHistory_ForTravelGroupExplore(int rowNum) throws Exception {

			verifyDataInDraftHistory("GroupExplore_Quotation", 14,15, rowNum, "GROUP EXPLORE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelGroupExplore(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("GroupExplore_Quotation", 14,15, rowNum, "GROUP EXPLORE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForTravelGroupExplore(int rowNum) throws Exception {
			ReadDataFromEmail_TravelInsurance.openEmail();
			ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_GroupExplore("GroupExplore_Quotation", rowNum);

		}
		
		// ********** Travel - POS Explore  **************************************
	
		public static void verifyDataOfShareProposalInDraftHistory_ForTravelPOSExplore(int rowNum) throws Exception {

			verifyDataInDraftHistory("POSExplore_Quotation", 14,15, rowNum, "POS EXPLORE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelPOSExplore(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("POSExplore_Quotation", 14,15, rowNum, "POS EXPLORE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForTravelPOSExplore(int rowNum) throws Exception {
			ReadDataFromEmail_TravelInsurance.openEmail();
			ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_POSExplore("POSExplore_Quotation", rowNum);

		}
		
	// ********** Travel - POS Student Explore **************************************

	public static void verifyDataOfShareProposalInDraftHistory_ForTravelPOSStudentExplore(int rowNum) throws Exception {

		verifyDataInDraftHistory("POS_StudentExplore_Quotation", 15, 16, rowNum, "POS STUDENT EXPLORE");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelPOSStudentExplore(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("POS_StudentExplore_Quotation", 15,16, rowNum, "POS STUDENT EXPLORE");

	}
	
	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
			public static void verifyDataOfShareProposalInMail_ForTravelPOSStudentExplore(int rowNum) throws Exception {
				ReadDataFromEmail_TravelInsurance.openEmail();
				ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_POSStudentExplore("POS_StudentExplore_Quotation", rowNum);

			}
			
	// ********** Travel - GROUP STUDENT Explore **************************************

	public static void verifyDataOfShareProposalInDraftHistory_ForTravelGroupStudentExplore(int rowNum) throws Exception {

		verifyDataInDraftHistory("GroupStudentExplore_Quotation", 15, 16, rowNum, "GROUP STUDENT EXPLORE");

	}
	
	public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelGroupStudentExplore(int rowNum)
			throws Exception {
		verifyDataShouldNotAvailableInDraftHistory("GroupStudentExplore_Quotation", 15,16, rowNum, "GROUP STUDENT EXPLORE");

	}
	
	// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
	public static void verifyDataOfShareProposalInMail_ForTravelStudentGroupExplore(int rowNum) throws Exception {
		ReadDataFromEmail_TravelInsurance.openEmail();
		ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_GroupStudentExplore("GroupStudentExplore_Quotation", rowNum);

	}
	
	// ********** Travel - STUDENT Explore **************************************
        public static void verifyDataOfShareProposalInDraftHistory_ForTravelStudentExplore(int rowNum) throws Exception {

			verifyDataInDraftHistory("StudentExplore_Quotation", 15, 16, rowNum, "STUDENT EXPLORE");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelStudentExplore(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("StudentExplore_Quotation", 15,16, rowNum, "STUDENT EXPLORE");

		}
		
		// Verify Data of Share Quotation In Email for Super Mediclaim Cancer
		public static void verifyDataOfShareProposalInMail_ForTravelStudentExplore(int rowNum) throws Exception {
			ReadDataFromEmail_TravelInsurance.openEmail();
			ReadDataFromEmail_TravelInsurance.readAndVerifyDataInEmailBody_ForShareProposal_TravelInsurance_StudentExplore("StudentExplore_Quotation", rowNum);

		}
		
		// ********** Travel - GROUP STUDENT Explore **************************************

		public static void verifyDataOfShareProposalInDraftHistory_ForDomesticTravel(int rowNum) throws Exception {

			verifyDataInDraftHistory("DomesticTravel_proposerInsuredD", 2, 3, rowNum, "DOMESTIC TRAVEL");

		}
		
		public static void verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForDomesticTravel(int rowNum)
				throws Exception {
			verifyDataShouldNotAvailableInDraftHistory("DomesticTravel_proposerInsuredD", 2,3, rowNum, "DOMESTIC TRAVEL");

		}
}
