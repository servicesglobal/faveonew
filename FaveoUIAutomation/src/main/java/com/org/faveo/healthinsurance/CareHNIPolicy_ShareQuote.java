package com.org.faveo.healthinsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareHNIPolicy_ShareQuote extends BaseClass implements AccountnSettingsInterface {
	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
public static Integer n;
	@Test
	public static void CareHNI() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		String[][] TestCaseData = BaseClass.excel_Files("CareHNI_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareHNI_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("CareHNI_QuestionSet");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for ( n = 1; n <=6; n++) {

			try {
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care For HNI - " + TestCaseName);
				System.out.println("Care For HNI - " + TestCaseName);
				
				//Step 1 - Open Email and Delete any Old Email 
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
	            LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
				System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
				logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				// Clicking on CAREHNI Product dropdown
				Thread.sleep(5000);
				HealthInsuranceDropDown.CareHNIPolicy();
				
				// Reading Proposer Name from Excel
				String Name = TestCaseData[n][2].toString().trim();
				enterText(By.xpath(Textbox_Name_xpath), TestCaseData[n][2].toString().trim());
				System.out.println("Proposer Name is : " + Name);

				// Reading Emailfrom Excel Sheet
				String Email = TestCaseData[n][3].toString().trim();
				System.out.println("Email of Proposer is :" + Email);
				Fluentwait(By.xpath(Textbox_Email_xpath));
				enterText(By.xpath(Textbox_Email_xpath), Email);

				// Reading Mobile Number from Excel
				Fluentwait(By.xpath(Textbox_Mobile_xpath));
				String MobileNumber = TestCaseData[n][4].toString().trim();
				int size = MobileNumber.length();
				if (isValid(MobileNumber)) {
					System.out.println("Mobile number is: " + MobileNumber + "Is a valid number");
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
				} else {
					System.out.println("Not a valid Number");
				}
				
				Thread.sleep(7000);
				WebElement policy_name = driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[1]/div/p/span"));
				String name = policy_name.getText().toString().trim();
				System.out.println("Name is:" + name);
                try{
				if (name.equals("Care For HNI")) {

					System.out.println("if loop started");
					List<WebElement> dropdown = driver.findElements(By.xpath(AllDropdown_Xpath));
					for (WebElement DropDownName : dropdown) {
						Fluentwait(By.xpath(AllDropdown_Xpath));
						DropDownName.click();
						int TotalMemberQuotation = Integer.parseInt(TestCaseData[n][5].toString().trim());
						System.out.println("Total Number of member in Excel Sheet : " + TotalMemberQuotation);
						int ActualCount = TotalMemberQuotation - 1;
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + ActualCount + "]"))
								.click();
						Thread.sleep(10000);
						break;
					}
	
					// again call the dropdown
					dropdown = driver.findElements(By.xpath(AllDropdown_Xpath));

					// Thread.sleep(3000);
					int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
					int count = 1;
					int mcount;
					int mcountindex = 0;
					int covertype;
					System.out.println("dropdown size is " + dropdown.size());
					outer: for (WebElement DropDownName : dropdown) {
						System.out.println("DropDownName is  " + DropDownName.getText());
						if (DropDownName.getText().equals(TestCaseData[n][5].toString().trim())) {
							System.out.println("DropDownName is  " + DropDownName.getText());
						}

						else if (DropDownName.getText().contains("Individual")) {
							System.out.println("DropDownName is  " + DropDownName.getText());
							if (TestCaseData[n][17].toString().trim().equals("Individual")) {
								covertype = 1;
							} else {
								covertype = 2;
							}
							DropDownName.click();
							driver.findElement(
									By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
											+ covertype + "]"))
									.click();
						} else if (name.equals("Care For HNI") && !DropDownName.getText().contains("18 to 24 Years")) {
							int Test = Integer.parseInt(DropDownName.getText());

							int Children = Integer.parseInt(TestCaseData[n][7].toString().trim());
							DropDownName.click();
							System.out.println("Children Dropdown text is :" + Test);
							
							driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ Children + ")]")).click();

						} else {

							// reading members from carewith TestData sheet
							// memberlist
							String Membersdetails = TestCaseData[n][6];
							if (Membersdetails.contains(",")) {

								// data taking form test case sheet which is
								// 7,4,1,8,2,5
								BaseClass.membres = Membersdetails.split(",");
							} else {
								BaseClass.membres = Membersdetails.split("");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
								// System.out.println("Mdeatils is : "+membres);

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDownName.click();
								// List Age of members dropdown

								List<WebElement> List = driver.findElements(By.xpath("//*[@class='dropdown year_drop_slect master open']/ul/li/a"));
								int totalsize = List.size();
								System.out.println("Size is:" + totalsize);
								System.out.println("Family Member details :" + FamilyData[mcount][0].toString().trim());
								for (WebElement age : List) {
									// System.out.println("Age details is :
									// "+age.getText());
									if (age.getText().equals(FamilyData[mcount][0].toString().trim())) {
										age.click();
									}
								}

								if (count == membersSize) {
									break outer;
								} else {
									count = count + 1;
									break member;
								}

							}

						}

					}
				}}catch(Exception e){
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}

				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][8].toString().trim());
				System.out.println("Selected suminsured is :"+SumInsured);
				Fluentwait(By.xpath(SumInsured_xpath));
				clickElement(By.xpath(SumInsured_xpath));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
                Thread.sleep(3000);
				scrolldown();
				Thread.sleep(2000);
				// Reading the value of Tenure from Excel
				int Tenure = Integer.parseInt(TestCaseData[n][9].toString().trim());
				System.out.println("Selected tenure is :"+Tenure);
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					//clickElement(By.xpath(Radio_Tenure1_xpath));
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					//clickElement(By.xpath(Radio_Tenure2_xpath));
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					//clickElement(By.xpath(Radio_Tenure3_xpath));
				}

				// Scroll Window Up
				BaseClass.scrollup();
				
				AddonsforProducts.readPremiumFromFirstPage();
                //ReadDataFromEmail.readAgeGroup(); 
      
                DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForCareHNI(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		        driver.quit();
		        
		     // Step 3 - Open Email and Verify Data in Email Body
				System.out.println("Step 3 -  Open Email and Verify Data in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForCareHNI(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
				System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
		        
				//*********** Proposar Details Page Functions **************************
				/*String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Title is:" + Title);
				waitForElement(By.xpath(click_title_xpath));
				clickElement(By.xpath(click_title_xpath));
				clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"+ Title + "'" + ")]"));*/
				waitForElement(By.name("ValidTitle"));
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				//clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);
		        BaseClass.selecttext("ValidTitle", Title.toString()); 
				
				Thread.sleep(5000);
				// Entering DOB from Excel into dob field
				String DOB = TestCaseData[n][10].toString().trim();
				System.out.println("date is:" + DOB);
				clickElement(By.id(Dob_Proposer_id));
				enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));

				Thread.sleep(3000);
				// Reading AddressLine 1 from Excel
				Fluentwait(By.xpath(Textbox_AddressLine1_xpath));
				String address1 = TestCaseData[n][11].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				enterText(By.xpath(Textbox_AddressLine1_xpath), address1);

				// Reading AddressLine 2 from Excel
				String address2 = TestCaseData[n][12].toString().trim();
				System.out.println(address2);
				Fluentwait(By.xpath(Textbox_AddressLine2_xpath));
				enterText(By.xpath(Textbox_AddressLine2_xpath), address2);

				// Reading Pincode from Excel
				int Pincode = Integer.parseInt(TestCaseData[n][13].toString().trim());
				System.out.println(Pincode);
				Fluentwait(By.xpath(Textbox_Pincode_xpath));
				enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));

				// Reading Proposer Height in Feet from Excel
				String HeightProposer_Feet = TestCaseData[n][14].toString().trim();
				System.out.println("Height value from excel  is:" + HeightProposer_Feet);
				Fluentwait(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text()," + HeightProposer_Feet + ")]"));

				// Reading Proposer Height in Inch from Excel
				String HeightProposer_Inch = TestCaseData[n][15].toString().trim();
				System.out.println("Inch value from excel  is:" + HeightProposer_Inch);
				Fluentwait(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath(
						"//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text()," + HeightProposer_Inch + ")]"));

				// Reading Weight of Proposer from Excel
				String Weight = TestCaseData[n][16].toString().trim();
				System.out.println("Weight is :" + Weight);
				Fluentwait(By.xpath(Textbox_Weight_xpath));
				enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));
				Thread.sleep(2000);
				// Reading Nominee Name from Excel
				String NomineeName = TestCaseData[n][17].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				Fluentwait(By.xpath(Textbox_NomineeName_xpath));
				enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);

				Thread.sleep(2000);
				// Nominee Relation
				String NomineeRelation = TestCaseData[n][18].toString().trim();
				System.out.println("Nominee  relation from excel  is:" + NomineeRelation);
				Fluentwait(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"
						+ NomineeRelation + "'" + ")]"));

				// String pancard=TestCaseData[n][19].toString().trim();
				String PanCard = TestCaseData[n][19].toString().trim();
				System.out.println("pancard number is :" + PanCard);
				Boolean PanCardNumberPresence = driver
						.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"))
						.size() > 0;
				if (PanCardNumberPresence == true) {
					enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

				} else {
					System.out.println("PAN Card Field is not Present");
				}

				// Click on Next button
				try{
					clickElement(By.id(Button_NextProposer_id));
					System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
						System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
					}
             //************** Insured Details Page Functions *********************************
				Thread.sleep(5000);
				int mcount;
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
					

						// String firstName= "fname"+i+"_xpath";
						/*String Date = FamilyData[mcount][5].toString().trim();
*/						// Date=Date.replaceAll("-", "/");
						// Relation
						BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
						// title
						BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
						enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
						enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
						clickElement(By.name("rel_dob" + i));
						enterText(By.name("rel_dob" + i), String.valueOf(Date));

						BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
						BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
						enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

					}
				}
				
				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
				
				//*********  HEALTH QUESTIONNAIRE *********************************

				String preExistingdeases = TestCaseData[n][20].toString().trim();
				Thread.sleep(5000);
				System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				BaseClass.scrollup();
				try{
					 if(preExistingdeases.contains("YES")) {
						
						 waitForElements(By.xpath(YesButton_xpath));
						 clickElement(By.xpath(YesButton_xpath));
						 //Thread.sleep(2000);
						 String years=null;
						 String Details=null;		
						 for(int qlist=1;qlist<=13;qlist++) {
							 Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
							 years=QuestionSetData[n][qlist+qlist].toString().trim();
							 if(Details.equals("")) {
					 //break;
							 }else 
							 {
								 int detailsnumber = Integer.parseInt(Details);

					 //Will click on check box and select the month & year u
					 detailsnumber=detailsnumber+1;
					 System.out.println("Details and years are :"+Details+"----"+years);
					 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
					 Thread.sleep(1000);
					 try {
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
						 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
					 }catch(Exception e) {
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
						 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
					 }
				 }
			}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
				}
				
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}
				
				//BaseClass.scrolldown();
				
				String ChecksData = null;
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 3; morechecks++) {
					int mch = morechecks + 1;
					ChecksData = TestCaseData[n][20 + morechecks].toString().trim();
					if (ChecksData.equalsIgnoreCase("NO")) {
						System.out.println("Quatation set to NO");
						driver.findElement(By.xpath("//label[@for='question_"+mch+"_no']")).click();
					} /*else if (ChecksData.equals("YES") || ChecksData.equalsIgnoreCase("Yes")) {
						System.out.println("Quatation set to only YES");
						driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
					}*/ else 
					{
						driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
						if (ChecksData.contains(",")) {
							ChckData = ChecksData.split(",");
							for (String Chdata : ChckData) {
								datacheck = Integer.parseInt(Chdata);
								datacheck = datacheck - 1;
								driver.findElement(
										By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
								// input[@name='question2_4']
							}
						} else {
							datacheck = Integer.parseInt(ChecksData);
							datacheck = datacheck - 1;
							driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
						}
					}

				}

				BaseClass.HelathQuestionnairecheckbox();
				
				try{
					clickElement(By.xpath(proceed_to_pay_xpath));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
						System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
					}

				BaseClass.ErroronHelathquestionnaire();
				waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
				clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
				
				BaseClass.PayuPage_Credentials();
				
				driver.quit();
				
				// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
				System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareGlobal(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				driver.quit();  
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				driver.quit();
				

				
			} catch (Exception e) {
				//WriteExcel.setCellData("TestCase_HNI", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				driver.quit();
			}
			continue;
	}
}
}