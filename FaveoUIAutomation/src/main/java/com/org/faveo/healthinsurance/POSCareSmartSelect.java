package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.CarewithOPDPageJourney;
import com.org.faveo.PolicyJourney.POSCareSmartSelectJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class POSCareSmartSelect extends BaseClass implements AccountnSettingsInterface{
 public static Integer n;
	
	@Test(priority = 1, enabled = false)
	public static void SmartSelect() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("POSSmartSelect_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files("POSSmartSelect_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("POSSmartSelect_TestCase");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 1; n < 21; n++) {
			try {
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("POS Smart Select test case name is  -" + TestCaseName);
				System.out.println("POS Smart Select -" + TestCaseName);
				BaseClass.LaunchBrowser();
				driver.navigate().refresh();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				HealthInsuranceDropDown.POSSmartSlectSelection();
				POSCareSmartSelectJourney.possmartselectQuotationpage();
				POSCareSmartSelectJourney.POScareSmartSelectDropdownselect();
				POSCareSmartSelectJourney.possmartselectsuminsuredtenure();
				POSCareSmartSelectJourney.possmartselectAddons();
				POSCareSmartSelectJourney.poscaresmartselectpremiumwithoutEdit();
				POSCareSmartSelectJourney.possmartselectproposerdetails();
				POSCareSmartSelectJourney.poscaresmartselectinsureddetails();
				POSCareSmartSelectJourney.poscaresmartselectQuestionset();
				POSCareSmartSelectJourney.poscaresmartselectproposalsummaryandPayment();

				WriteExcel.setCellData("POSSmartSelect_TestCase", "PASS", n, 3);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
				driver.quit();

			} catch (Exception e) {
				WriteExcel.setCellData("POSSmartSelect_TestCase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				driver.quit();
			}
			continue;

		}
	}
	
	//******************* Share Quote *******************************************
	@Test(priority = 2, enabled = true)
	public static void SmartSelect_ShareQuote() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		String[][] TestCaseData = BaseClass.excel_Files("POSSmartSelect_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 6; n <=6; n++) {
			try {
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("POS Smart Select test case name is  -" + TestCaseName);
				System.out.println("POS Smart Select -" + TestCaseName);
				
				//Step 1 - Open Email and Delete any Old Email 
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
	            LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
				System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
				logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				HealthInsuranceDropDown.POSSmartSlectSelection();
				POSCareSmartSelectJourney.possmartselectQuotationpage();
				POSCareSmartSelectJourney.POScareSmartSelectDropdownselect();
				POSCareSmartSelectJourney.possmartselectsuminsuredtenure();
				POSCareSmartSelectJourney.possmartselectAddons();
				
				AddonsforProducts.readPremiumFromFirstPage();
				//Read Age Group Based on Cover Type
				String CoverType = TestCaseData[n][8].toString().trim();
				if(CoverType.equalsIgnoreCase("Individual")){
                ReadDataFromEmail.readAgeGroup(); 
				}
				else{
					System.out.println("Age Group Value not Read");
				}
      
                DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForCareWithSmartSelect(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		        driver.quit();
				
		        // Step 3 - Open Email and Verify Data in Email Body
				System.out.println("Step 3 -  Open Email and Verify Data in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForPOSCareWithSmartSelect(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
				System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				POSCareSmartSelectJourney.possmartselectproposerdetails();
				POSCareSmartSelectJourney.poscaresmartselectinsureddetails();
				POSCareSmartSelectJourney.poscaresmartselectQuestionset();
				
				Thread.sleep(20000);
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				driver.quit();
				
				 // Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
				System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSCareWithSmartSelect(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				driver.quit();  
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
		        

				//WriteExcel.setCellData("POSSmartSelect_TestCase", "PASS", n, 3);

				LaunchBrowser();

			} 
			
			catch (Exception e) {
				//WriteExcel.setCellData("POSSmartSelect_TestCase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				driver.quit();
			}
			continue;

		}
	}
	
	
	//******************* Share Proposal *******************************************
		@Test(priority = 3, enabled = false)
		public static void verifyDataOfShareProposalInDraftHistory() throws Exception {

			String[][] TestCase = BaseClass.excel_Files("ShareProposal");
			String[][] TestCaseData = BaseClass.excel_Files("POSSmartSelect_Quotation");
			String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("ShareProposal");
			System.out.println("Total Number of Row in Sheet : " + rowCount);

			for (n = 2; n <=2; n++) {
				try {
					String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
					logger = extent.startTest("POS Smart Select test case name is  -" + TestCaseName);
					System.out.println("POS Smart Select -" + TestCaseName);
					
					// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
					System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
					logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
		            
					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					//clickElement(By.xpath(Close_VideoPopup_Xpath));
					
					HealthInsuranceDropDown.POSSmartSlectSelection();
					POSCareSmartSelectJourney.possmartselectQuotationpage();
					POSCareSmartSelectJourney.POScareSmartSelectDropdownselect();
					POSCareSmartSelectJourney.possmartselectsuminsuredtenure();
					POSCareSmartSelectJourney.possmartselectAddons();
					
					/*AddonsforProducts.readPremiumFromFirstPage();
					//Read Age Group Based on Cover Type
					String CoverType = TestCaseData[n][8].toString().trim();
					if(CoverType.equalsIgnoreCase("Individual")){
	                ReadDataFromEmail.readAgeGroup(); 
					}
					else{
						System.out.println("Age Group Value not Read");
					}*/
	      
					Thread.sleep(3000);
					clickElement(By.xpath(Button_Buynow_xpath));
					
					POSCareSmartSelectJourney.possmartselectproposerdetails();
					POSCareSmartSelectJourney.poscaresmartselectinsureddetails();
					
					
					//Set Details of Share Proposal Popup Box and Verify Data in Draft History
					DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
					DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForPOSCareSmartSelect(n);
					
					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					
					//Click on Resume Policy
					DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
					
					
					POSCareSmartSelectJourney.poscaresmartselectQuestionset();
					
					Thread.sleep(20000);
					System.out.println("Before Pay U");
					(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
					System.out.println("After Pay U");
					
					PayuPage_Credentials();
					driver.quit();
					
					//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
					System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
					logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		
					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					//clickElement(By.xpath(Close_VideoPopup_Xpath));
					DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareSmartSelect(n);

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");	
					
			       driver.quit();
					
				
				} 
				
				catch (AssertionError e) {
					//WriteExcel.setCellData("POSSmartSelect_TestCase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);   
					driver.quit();
				}
				
				catch (Exception e) {
					//WriteExcel.setCellData("POSSmartSelect_TestCase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);   
					//driver.quit();
				}
				continue;

			}
		}
		
		//*********** Share Proposal Test Case 2 ***************************************************
		
		@Test(priority = 4, enabled = true)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {

			String[][] TestCase = BaseClass.excel_Files("ShareProposal");
			String[][] TestCaseData = BaseClass.excel_Files("POSSmartSelect_Quotation");
			String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("ShareProposal");
			System.out.println("Total Number of Row in Sheet : " + rowCount);

			for (n = 6; n <=6; n++) {
				try {
					String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
					logger = extent.startTest("POS Smart Select test case name is  -" + TestCaseName);
					System.out.println("POS Smart Select -" + TestCaseName);
					
					//Step 1 - Open Email and Delete Old Email
					System.out.println("Step 1 - Open Email and Delete Old Email");
					logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
					
					LaunchBrowser();
					ReadDataFromEmail.openAndDeleteOldEmail();
					driver.quit(); 
					
					//Step 2 - Go to Application and share a proposal
					System.out.println("Step 2 - Go to Application and share a proposal");
					logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
		            
					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					//clickElement(By.xpath(Close_VideoPopup_Xpath));
					
					HealthInsuranceDropDown.POSSmartSlectSelection();
					POSCareSmartSelectJourney.possmartselectQuotationpage();
					POSCareSmartSelectJourney.POScareSmartSelectDropdownselect();
					POSCareSmartSelectJourney.possmartselectsuminsuredtenure();
					POSCareSmartSelectJourney.possmartselectAddons();
					
					/*AddonsforProducts.readPremiumFromFirstPage();
					//Read Age Group Based on Cover Type
					String CoverType = TestCaseData[n][8].toString().trim();
					if(CoverType.equalsIgnoreCase("Individual")){
	                ReadDataFromEmail.readAgeGroup(); 
					}
					else{
						System.out.println("Age Group Value not Read");
					}*/
					
					AddonsforProducts.readPremiumFromFirstPage();
	                
					//Read Age Group Based on Cover Type
					String CoverType = TestCaseData[n][8].toString().trim();
					if(CoverType.equalsIgnoreCase("Individual")){
	                ReadDataFromEmail.readAgeGroup(); 
					}
					else{
						System.out.println("Age Group Value not Read");
					}
					
					//ReadDataFromEmail.readAgeGroup(); 
					
					Thread.sleep(3000);
					clickElement(By.xpath(Button_Buynow_xpath));
	     
					POSCareSmartSelectJourney.possmartselectproposerdetails();
					POSCareSmartSelectJourney.poscaresmartselectinsureddetails();
					
					
					//Set Details of Share Proposal Popup Box and Verify Data in Draft History
					DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
					//DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForPOSCareSmartSelect(n);
					
					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					driver.quit();
					
					//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
					System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
					logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
					
				    LaunchBrowser();
				    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForPOSCareSmartSelect(n);
				    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					
					//Step 4 - Click on Buy Now button from Email Body and punch the policy
					System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
					logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
					
					ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
					
					DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
					DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
					
					
					//Click on Resume Policy
					//DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
					
					
					POSCareSmartSelectJourney.poscaresmartselectQuestionset();
					
					Thread.sleep(20000);
					System.out.println("Before Pay U");
					(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
					System.out.println("After Pay U");
					
					PayuPage_Credentials();
					driver.quit();
					
					//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
					System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
					logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
		
					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					//clickElement(By.xpath(Close_VideoPopup_Xpath));
					DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareSmartSelect(n);

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");	
					
			       driver.quit();
					
					 // Step 6- Again Open the Email and Verify GUID Should be Reusable
					System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
					logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

					LaunchBrowser();
					ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

					// Verify the Page Title
					DataVerificationShareProposalPage.verifyPageTitle();   

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					 driver.quit(); 
					
				
				} 
				
				catch (AssertionError e) {
					//WriteExcel.setCellData("POSSmartSelect_TestCase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);   
					driver.quit();
				}
				
				catch (Exception e) {
					//WriteExcel.setCellData("POSSmartSelect_TestCase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);   
					driver.quit();
				}
				continue;

			}
		}
		
		
}