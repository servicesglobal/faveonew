package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.PolicyJourney.PosCareFreedomPageJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestions;
import com.org.faveo.model.InsuredDetails;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class PosCareFreedomPolicy_ShareProposal extends BaseClass implements AccountnSettingsInterface {

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static Integer n;

	@Test(priority=1, enabled=false)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n =4; n <= 6; n++) {
			try {

				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Pos Care Freedom -" + TestCaseName);
				System.out.println("Pos Care Freedom -" + TestCaseName);
				
				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
	           
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				HealthInsuranceDropDown.PosCareFreedom();
				DataVerificationShareQuotationPage.QuotationPageJourney_POSCAREFreedom(n);
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				PosCareFreedomPageJourney.ProposalPageJourney2(n);
				
				InsuredDetails.InsuredDetail("Pos_CareFreedom_Insured_Details");
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForPOSCareFreedom(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
				
				//Health Questionnarire Elements 
				HealthQuestions.CareFreedomHealthQuestions("PosCarefreedom_Quotation_Data", "Pos_CareFreedom_QuestionSet", n, 22);
			
				BaseClass.ErroronHelathquestionnaire();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
				System.out.println("After Pay U");
				
				Thread.sleep(7000);

				BaseClass.PayuPage_Credentials();
				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareFreedom(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				driver.quit();
				
				
				
			} catch (Exception e) {
				// WriteExcel.setCellData("Test_Case_PosFreedom", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				// Thread.sleep(4000);
				 driver.quit();

			}
			continue;

		}
	}
	
	// ****************** Share Proposal Test Case 2 ***********************************************8
	@Test(priority=2, enabled=true)
	public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n =5; n <= 5; n++) {
			try {

				String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
				logger = extent.startTest("Pos Care Freedom -" + TestCaseName);
				System.out.println("Pos Care Freedom -" + TestCaseName);
				
				//Step 1 - Open Email and Delete Old Email
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a proposal
				System.out.println("Step 2 - Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
	           
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				// Clicking on CAREHNI Product dropdown
				HealthInsuranceDropDown.PosCareFreedom();
				DataVerificationShareQuotationPage.QuotationPageJourney_POSCAREFreedom(n);
				
				//Read Premium and Age Group
				AddonsforProducts.readPremiumFromFirstPage();
				
				String[][] TestCaseData = BaseClass.excel_Files("PosCarefreedom_Quotation_Data");
				//Read Age Group Based on Cover Type
				String CoverType = TestCaseData[n][8].toString().trim();
				if(CoverType.equalsIgnoreCase("Individual")){
                ReadDataFromEmail.readAgeGroup(); 
				}
				else{
					System.out.println("Age Group Value not Read");
				}
				
               // ReadDataFromEmail.readAgeGroup(); 
				
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				PosCareFreedomPageJourney.ProposalPageJourney2(n);
				
				InsuredDetails.InsuredDetail("Pos_CareFreedom_Insured_Details");
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			    driver.quit();
				
				//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
				System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				
			    LaunchBrowser();
			    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForPOSCareFreedom(n);
			    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Step 4 - Click on Buy Now button from Email Body and punch the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
				
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
				DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
				
				//******** Health Question Page *********************************
				//Health Questionnarire Elements 
				HealthQuestions.CareFreedomHealthQuestions("PosCarefreedom_Quotation_Data", "Pos_CareFreedom_QuestionSet", n, 22);
			
				BaseClass.ErroronHelathquestionnaire();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
				System.out.println("After Pay U");
				
				Thread.sleep(7000);

				BaseClass.PayuPage_Credentials();
				driver.quit();
				
				//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareFreedom(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				driver.quit();
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				 driver.quit();  
				
				
				
			} catch (Exception e) {
				// WriteExcel.setCellData("Test_Case_PosFreedom", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				// Thread.sleep(4000);
				driver.quit();

			}
			continue;

		}
	}
}
