package com.org.faveo.healthinsurance;

import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.fixedbenefitinsurance.Assure;
import com.org.faveo.fixedbenefitinsurance.Secure;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class MainClass extends BaseClass implements AccountnSettingsInterface 

{
	/*@org.testng.annotations.BeforeTest
	public static void BeforeTest() {
		System.out.println("Execution Start for Regression");
	}*/
	
	/*@Test(priority=1, enabled=true)
	public void f1() throws Exception{
		
	}*/
	
	@Test
	public void Execute() throws Exception
	{
		System.out.println("Execution starts for all the care varients");
		String[][] TestCase=BaseClass.excel_Files("Execution_Sheet");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		//ReadExcel fis = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Execution_Sheet");
		System.out.println("Total rows for Execution status sheet is :"+rowCount);
		//BaseClass.LaunchBrowser();
		for (int n = 1; n < rowCount; n++) 
		{
		
			String Product_Name = TestCase[n][0].toString().trim();
			
			if(Product_Name.contains("Care With NCB"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				
				if(Execution_Status.contains("Yes"))
				{
					CareWithNCB.CareNCB();
					
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care With NCB as per Excel sheet.");
				}
				
				
			}
			
			else if(Product_Name.contains("Care Freedom"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					CareFreedomPolicy.CareFreedom();
				
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Freedom as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care Global"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					CareGlobalPolicy.CareGlobalCase();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Global as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care For HNI"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					CareHNIPolicy.CareHNI();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care HNI as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Care With Smart Select"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
				
					CareSmartSelectPolicy.CaresmartSelect();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Select as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("POSCareSuperSaver"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					POSCareSuperSaver.POSSupersaver();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Enhnace as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Pos Care With NCB"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					POSCareWithNCB.poscarewithNCB();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Enhnace as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Pos Care With Freedom"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					PosCareFreedomPolicy.PosCareFreedom();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Enhnace as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Enhance"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					Enhance.EnhanceCases();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Enhnace as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Secure"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					Secure.SecureTestCases();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Secure as per Excel sheet.");
				}
				
				
			}
			else if(Product_Name.contains("Assure"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					Assure.AssureTestCases();
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Assure as per Excel sheet.");
				}
				
				
			}
			
			else if(Product_Name.contains("Care Super Saver"))
			{
				String Execution_Status = TestCase[n][1].toString().trim();
				
				if(Execution_Status.contains("Yes"))
				{
					SuperSaverPolicy.SuperSaver();
					
				}else if(Execution_Status.contains("No"))
				{
					System.out.println("No Need to Execute to Care Super Saver as per Excel sheet.");
				}
				
				
			}
			continue;
		}
	}
	
	
}
