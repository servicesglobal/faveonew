package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.PolicyJourney.SuperMediclaimCancerPolicyJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.model.HealthQuestionSuperMediClaim;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class SuperMediclaimCancer extends BaseClass implements AccountnSettingsInterface {

	@Test(enabled = false, priority = 1)
	public void superMediclaimWithCancer() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("SuperMediClainCancerTestCase");

		ReadExcel read = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("SuperMediClainCancerTestCase");

		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (int n = 1; n <= 1; n++) {

			try {
				String Execution_Status = TestCase[n][4].toString().trim();
				if (Execution_Status.equalsIgnoreCase("Execute")) {
					String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
					logger = extent.startTest("Super Medicliam Cancer -" + TestCaseName);
					System.out.println("Super Medicliam Cancer -" + TestCaseName);
					// LoginFn.LoginPage1("Super Mediclaim");
					LoginFn.LoginPage();
					Thread.sleep(5000);
					HealthInsuranceDropDown.superMediClaimWithCancer();
					SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
					SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
					SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
					HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
					SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
					// DataVerificationByPDF.verifyPolicyDetailsInPdf();

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");

					// Test case
					// LogoutFn.LogoutfromApp();
					// driver.quit();

				} else {
					System.out.println("No Need to Execute Scenario Number : " + n);
					// WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				// logger.log(LogStatus.FAIL,
				// logger.addScreenCapture(BaseClass.getScreenhot(driver,
				// "Failure")));
				/*
				 * logger.log(LogStatus.FAIL,
				 * "Test Case is Failed beacuse getting :" + e.getMessage());
				 * logger.log(LogStatus.FAIL, "Test Case is Failed.");
				 */
				// driver.quit();

			}
			continue;
		}
	}

	// *********************** Test case for Share Quotation *****************************************
	// Test case - Mix - Verify after punching the policy through Shared
	// Quotation, the Proposal should exit from the Quotes Tracker Page
	@Test(priority = 2, enabled = false)
	public void verifySharedQuotationScenario_InSuperMediclaimCancer() throws Exception {

		// int n= 1;
		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		/*
		 * String TestCaseName = (TestCase[n][0].toString().trim() + " - " +
		 * TestCase[n][1].toString().trim()); logger =
		 * extent.startTest("Super Medicliam Cancer Share quotation -" +
		 * TestCaseName); System.out.println(TestCaseName);
		 */

		for (int n = 6; n <= 6; n++) {
			String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
			logger = extent.startTest("Super Medicliam Cancer Share quotation -" + TestCaseName);
			System.out.println(TestCaseName);

			try {
				// Step 1 - Open Email and delete old Email
				System.out.println("**** Open Email and delete old Email *****");
				logger.log(LogStatus.PASS, "Open Email and delete old Email");
				LaunchBrowser();

				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); // Closed the browser

				// Step 2 - Go to Application and share a Quote and Verify Data
				// in Quotation Tracker
				System.out
						.println("**** Go to Application and share a Quote and Verify Data in Quotation Tracker ****");
				logger.log(LogStatus.PASS,
						"Go to Application and Quote a proposal and Verify Data in Quotation Tracker");

				LoginFn.LoginPage1("Super Mediclaim");
				Thread.sleep(5000);
				HealthInsuranceDropDown.superMediClaimWithCancer();
				DataVerificationShareQuotationPage.QuotationPageJourney(n);
				DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
				DataVerificationShareQuotationPage
						.verifyDataInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 3 - Open Email and Verify Data in Email Body
				System.out.println("**** Open Email and Verify Data in Email Body ****");
				logger.log(LogStatus.PASS, " Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimCancer(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

				// Step 4 - Click on Buy Now Button from Email Body and punch
				// the Policy
				System.out.println("**** Click on Buy Now Button from Email Body and punch the Policy ****");
				logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();

				SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
				SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
				HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);
				SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 5 - Again Login the application and verify after punch
				// the policy data should be not available in quotation Tracker

				System.out.println(
						"**** Again Login the application and verify after punch the policy data should be not available in quotation Tracker ****");
				logger.log(LogStatus.PASS,
						"Again Login the application and verify after punch the policy data should be not available in quotation Tracker");

				LoginFn.LoginPage1("Super Mediclaim");
				DataVerificationShareQuotationPage
						.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimCancer(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

				// Step 6- Again Open the Email and Verify GUID Should be
				// Reusable
				System.out.println("**** Again Open the Email and Verify GUID Should be Reusable ****");
				logger.log(LogStatus.PASS, "Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");

				// LogoutFn.LogoutfromApp();
				// driver.quit();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				driver.quit();

			}
			continue;
		}

	}

	// ******************************** Shared Proposals Test Case 1 ******************************************
	// Test Case 1 - Verify that details the share Proposal details in Quotes
	// Tracker
	@Test(enabled = false, priority = 3)
	public void verifyDataOfShareProposalInDraftHistory() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");

		ReadExcel read = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("SuperMediClainCancerTestCase");

		for (int n = 4; n <= 6; n++) {
			String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
			logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
			System.out.println(TestCaseName);
			try {
				// Step 1 - Open the application And Share a Proposal and
				// VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS,
						"Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				Thread.sleep(5000);
				HealthInsuranceDropDown.superMediClaimWithCancer();
				SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);
				SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
				SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);

				// Set Details of Share Proposal Popup Box and Verify Data in
				// Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCancer(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				// Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();

				HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);

				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
				System.out.println("After Pay U");

				PayuPage_Credentials();
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// SuperMediclaimCancerPolicyJourney.setPaymetDetailsSuperMediClaim(1);

				// Step 2 - Again Login the application and verify shared
				// proposal data should not be available in Draft History
				System.out.println(
						"Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS,
						"Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage
						.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCancer(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");

				 driver.quit();

			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();

			}
			continue;
		}

	}

	// ******************************** Shared Proposals Test Case 2 ******************************************
	@Test(enabled = true, priority = 4)
	public void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
		// int n = 3;
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		ReadExcel read = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("SuperMediClainCancerTestCase");

		for (int n = 1; n <= 6; n++) {
			String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
			logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
			System.out.println(TestCaseName);
			try {

				// Step 1 - Open Email and Delete Old Email
				System.out.println("Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Open Email and Delete Old Email");

				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit();

				// Step 2 - Go to Application and share a proposal
				System.out.println("Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Go to Application and share a proposal");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				Thread.sleep(5000);
				HealthInsuranceDropDown.superMediClaimWithCancer();
				SuperMediclaimCancerPolicyJourney.QuotationPageJourney(n);

				// ReadDataFromEmail.readAgeGroup();

				SuperMediclaimCancerPolicyJourney.ProposalPageJourney(n);
				SuperMediclaimCancerPolicyJourney.SetInsuredDetails(n);
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				driver.quit();

				// Step 3- Again Open the email and verify the data of Share
				// Proposal in Email Body
				System.out.println("Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Again Open the email and verify the data of Share Proposal in Email Body");

				LaunchBrowser();
				DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForSuperMediclaimCancer(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

				// Step 4 - Click on Buy Now button from Email Body and punch
				// the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");

				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();

				DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
				DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();

				HealthQuestionSuperMediClaim.setSuperMediclaimCancerHealthQuestions(n);

				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
				System.out.println("After Pay U");

				PayuPage_Credentials();
				driver.quit();

				// Step 5 - Again Login the application and verify shared
				// proposal data should not be available in Draft History
				System.out.println(
						"Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage
						.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCancer(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				driver.quit();

				// Step 6- Again Open the Email and Verify GUID Should be
				// Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
			     driver.quit();

			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				driver.quit();

			}
			continue;
		}
	}

}
