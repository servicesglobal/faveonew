package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.PolicyJourney.PosCareFreedomPageJourney;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class PosCareFreedomPolicy extends BaseClass implements AccountnSettingsInterface
{

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
public static Integer n;
	@Test
	public static void PosCareFreedom() throws Exception 
	{
		String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Test_Case_PosFreedom");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
	
		for( n=1;n<=1;n++) {
		try 
		{
		
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Pos Care Freedom -" + TestCaseName);
		System.out.println("Pos Care Freedom -" + TestCaseName);
		
		// Login with the help of Login Case class
		LoginFn.LoginPage();

		// Clicking on CAREHNI Product dropdown
		HealthInsuranceDropDown.PosCareFreedom();
		
		PosCareFreedomPageJourney.QuotationPageJourney(n);
		
		PosCareFreedomPageJourney.ProposalPageJourney(n);
		
		PosCareFreedomPageJourney.ProposalSummaryPage("Test_Case_PosFreedom",n);
		
		
		Thread.sleep(4000);
		 driver.quit();

			
		

		} catch (Exception e) 
		{
					//WriteExcel.setCellData("Test_Case_PosFreedom", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);
					 driver.quit();

					
		}
				 continue; 
			
		}
	}
}
