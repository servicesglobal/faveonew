package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.SuperMediclaimCriticalFunctions;
import com.org.faveo.PolicyJourney.OperationQuotationPageJourney;
import com.org.faveo.PolicyJourney.SuperMediclaimCancerPolicyJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestionSuperMediClaim;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class SuperMediclaimCritical extends SuperMediclaimCriticalFunctions{
	
	//public static int rowc;
	
	@Test(priority = 1, enabled = false)
	public static void supermediclaimcritical() throws Exception {

		System.out.println("Super Medical : Critical create policy  Script Execution Started");
		BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		System.out.println("rowCount is " + rowCount);
		System.out.println("Total number of rows : " + BaseClass.rowCount);
		 int rowc = rowCount;
		 
		 String[][] TestCase = BaseClass.excel_Files("SuperMediClaimTestCaseList");
        
		for(n=2;n<=2;n++) {
	   
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
			Thread.sleep(10000);
		  
			try {
				LoginFn.LoginPage();
				Thread.sleep(5000);
				
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				//SuperMediclaimCriticalFunctions.critical_verify_premium();
				SuperMediclaimCriticalFunctions.Critical_Edits();
				SuperMediclaimCriticalFunctions.CriticalProposerDetails();
				SuperMediclaimCriticalFunctions.critical_insuredDetails();
				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				SuperMediclaimCriticalFunctions.Critical_Payment();
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//driver.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
			}
			continue;
		}

		}
	
	//************************ Test Cases for Shared Quote *******************************************
	// Test case - 1 - Verify after punching the policy through Shared Quotation, the Proposal should exit from the Quotes Tracker Page
	@Test(priority = 5, enabled = false)
	public static void verifySharedQuotationScenario_InSuperMediclaimCritical() throws Exception {
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");

		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		/*String TestCaseName = (TestCase[1][0].toString().trim() + " - " + TestCase[1][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share quotation -" + TestCaseName);
		System.out.println(TestCaseName);*/

		for(n=1; n<=6; n++){
			String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
			logger = extent.startTest("Super Medicliam Critical Share quotation -" + TestCaseName);
			System.out.println(TestCaseName);
			
		try {
			//Step 1 - Open Email and Delete any Old Email
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");

			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit();
           
			// Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
			System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
			logger.log(LogStatus.PASS, "Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");

			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
			SuperMediclaimCriticalFunctions.criticaldropdown();
			SuperMediclaimCriticalFunctions.critical_dragdrop();
			SuperMediclaimCriticalFunctions.Tenurecritical();
			SuperMediclaimCriticalFunctions.Monthly_frequency();
			AddonsforProducts.readPremiumFromFirstPage();

			ReadDataFromEmail.readAgeGroup();

			DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
			DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimCritical(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

			driver.quit();

			// Step 3 - Open Email and Verify Data in Email Body
			System.out.println("Step 3 - Open Email and Verify Data in Email Body");
			logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
			LaunchBrowser();
			DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimCritical(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

			// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
			System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
			logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");
			
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();

			SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
			SuperMediclaimCriticalFunctions.Critical_Payment();
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			driver.quit();

			// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
			System.out.println(
					" Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
			logger.log(LogStatus.PASS,
					" Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
			LoginFn.LoginPage1("Super Mediclaim");
			DataVerificationShareQuotationPage
					.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimritical(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

			// Step 6- Again Open the Email and Verify GUID Should be Reusable
			System.out.println("Again Open the Email and Verify GUID Should be Reusable");
			logger.log(LogStatus.PASS, "Again Open the Email and Verify GUID Should be Reusable");

			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

			// Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");

			// driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			// driver.quit();
		}
		continue;
		}
	}
	
	//******************************** Test Case for Shared Proposals ******************************************
	@Test(priority = 1, enabled = true)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {

		System.out.println("Super Medical : Critical create policy  Script Execution Started");
		BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		System.out.println("rowCount is " + rowCount);
		System.out.println("Total number of rows : " + BaseClass.rowCount);
		 int rowc = rowCount;
		 
		 String[][] TestCase = BaseClass.excel_Files("ShareProposal");
        
		for(n=2;n<=6;n++) {
	   
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		  
			try {
				
				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				//LoginFn.LoginPage();
				Thread.sleep(5000);
				
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				//SuperMediclaimCriticalFunctions.critical_verify_premium();
				
				try {
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
			} catch (Exception e) {
				System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
			}
			
			SuperMediclaimCriticalFunctions.CriticalProposerDetails();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			
			//Set Details of Share Proposal Popup Box and Verify Data in Draft History
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamCritical(n);
					
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			
			//Click on Resume Policy
			DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
				
				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				//SuperMediclaimCriticalFunctions.Critical_Payment();
				
				waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
			    clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
					
				BaseClass.PayuPage_Credentials();
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		        driver.quit();
		        
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamCritical(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				 driver.quit();
				//logger.log(LogStatus.PASS, "Test Case Passed");
				
				//driver.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
			}
			continue;
		}

		}
	//************* Share Proposal Test Case 2 *******************************************
	@Test(priority = 1, enabled = false)
	public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {

		System.out.println("Super Medical : Critical create policy  Script Execution Started");
		BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		System.out.println("rowCount is " + rowCount);
		System.out.println("Total number of rows : " + BaseClass.rowCount);
		 int rowc = rowCount;
		 
		 String[][] TestCase = BaseClass.excel_Files("ShareProposal");
        
		for(n=5;n<=5;n++) {
	   
		String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		  
			try {
				
				//Step 1 - Open Email and Delete Old Email
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a proposal
				System.out.println("Step 2 - Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				//LoginFn.LoginPage();
				Thread.sleep(5000);
				
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				//SuperMediclaimCriticalFunctions.critical_verify_premium();
				
				Thread.sleep(5000);
				AddonsforProducts.readPremiumFromFirstPage();
	            ReadDataFromEmail.readAgeGroup(); 
				
				try {
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
			} catch (Exception e) {
				System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
			}
			
			SuperMediclaimCriticalFunctions.CriticalProposerDetails();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			
			//Set Details of Share Proposal Popup Box and Verify Data in Draft History
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			//DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamCritical(n);
		    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		    driver.quit();
			
		  //Step 3- Again Open the email and verify the data of Share Proposal in Email Body
			System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			
		    LaunchBrowser();
		    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForSuperMedicliamCritical(n);
		    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			//logger.log(LogStatus.PASS, "Test Case Passed");
			
			//Step 4 - Click on Buy Now button from Email Body and punch the policy
			System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
			
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
			
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
				
				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				//SuperMediclaimCriticalFunctions.Critical_Payment();
				
				waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
			    clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
					
				BaseClass.PayuPage_Credentials();
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();
				
				//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
		        driver.quit();
		        
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamCritical(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				 driver.quit();
				 
				 
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
					System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
					logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

					LaunchBrowser();
					ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

					// Verify the Page Title
					DataVerificationShareProposalPage.verifyPageTitle();

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					driver.quit();
				 
				//logger.log(LogStatus.PASS, "Test Case Passed");
				
				//driver.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
			}
			continue;
		}

		}
	
	
}
