	package com.org.faveo.healthinsurance;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareWithNCB extends BaseClass implements AccountnSettingsInterface {

	/*
	 * BaseURLAPI=https://apiqc.religarehealthinsurance.com/Faveo/agencyportal/index
	 * .html#/auth/login
	 * BaseURLQC=https://faveoqc.religarehealthinsurance.com/Faveo/agencyportal/
	 * index.html#/auth/login
	 * BaseURLStaging=https://faveostage.religarehealthinsurance.com/Faveo/
	 * agencyportal/index.html#/auth/login
	 * AbacusUrl=https://abacusuat.religarehealthinsurance.com/religare/partner/
	 * generic-religare-know-popup
	 * customerPortal="https://rhicluat.religarehealthinsurance.com/hp/atulsingh";
	 */
	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static Integer n;

	@Test
	public static void CareNCB() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("Test_Cases_Care");
		String[][] TestCaseData = BaseClass.excel_Files("Care_Quotation_Data");
		String[][] FamilyData = BaseClass.excel_Files("Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("Care_QuestionSet");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		// ReadExcel fis = new ReadExcel("D:\\Test Data Faveo\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Test_Cases_Care");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 1; n <21; n++) {
			try {

				// Launching Browser using Method of Base Class
				BaseClass.LaunchBrowser();
				//driver.navigate().refresh();

				// Picking Test Case Name from Excel
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("CareWithNCB - " + TestCaseName);
				System.out.println("CareWithNCB - " + TestCaseName);

				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));

				// Clicking on CAREHNI Product dropdown
				HealthInsuranceDropDown.CareWithNCBPolicy();

				// Reading Proposer Name from Excel
				String Name = (TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
				System.out.println("Entered Proposer Name is : " + Name);
				Fluentwait(By.xpath(Textbox_Name_xpath));
				try {
					enterText(By.xpath(Textbox_Name_xpath), Name);
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Unable to click on Name Textbox on Quotation Page.");
				}

				// Reading Emailfrom Excel Sheet
				String Email = TestCaseData[n][6].toString().trim();
				System.out.println("Email of Proposer is :" + Email);
				Fluentwait(By.xpath(Textbox_Email_xpath));
				enterText(By.xpath(Textbox_Email_xpath), Email);

				// Reading Mobile Number from Excel
				String MobileNumber = TestCaseData[n][5].toString().trim();
				int size = MobileNumber.length();
				if (isValid(MobileNumber)) {
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
					System.out.println("Entered Mobile number is: " + MobileNumber);
				} else {
					System.out.println("Not a valid Number");
				}

				// Enter The Value of Total members present in policy
				Thread.sleep(10000);
				List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
				try {
					for (WebElement DropDownName : dropdown) {
						DropDownName.click();
						if (DropDownName.getText().equals("1")) {
							driver.findElement(
									By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["
											+ TestCaseData[n][15].toString().trim() + "]"))
									.click();
							System.out.println(
									"Total Number of Member Selected : " + TestCaseData[n][15].toString().trim());
							Thread.sleep(5000);
							break;
						}
					}
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}

				// again call the dropdown
				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][15].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				try {
					outer:

					for (WebElement DropDownName : dropdown) {

						if (membersSize == 1) {

							String Membersdetails = TestCaseData[n][16];
							if (Membersdetails.contains("")) {

								BaseClass.membres = Membersdetails.split("");

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By.xpath(
											"//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
											.click();

									// List Age of members dropdown
									// DropDownName.click();
									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
							}

						} else if (DropDownName.getText().contains("Individual")) {
							System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

							if (TestCaseData[n][17].toString().trim().equals("Individual")) {
								covertype = 1;
							} else {
								covertype = 2;
							}
							DropDownName.click();
							driver.findElement(
									By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["
											+ covertype + "]"))
									.click();
							// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
							Thread.sleep(10000);
							if (covertype == 2) {
								List<WebElement> dropdowns = driver
										.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
								for (WebElement DropDowns : dropdowns) {
									if (DropDowns.getText().contains("Floater")) {
										System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
									} else if (DropDowns.getText().equals(TestCaseData[n][15].toString().trim())) {
										System.out.println("DropDownName is  " + DropDowns.getText());
									} else if (DropDowns.getText().equals("2")) {
										System.out.println("Total DropDownName Present on Quotation page are : "
												+ DropDowns.getText());
									} else if (DropDowns.getText().equals("18 - 24 years")) {
										// reading members from test cases sheet
										// memberlist
										int Children = Integer.parseInt(TestCaseData[n][31].toString().trim());
										clickElement(By.xpath(
												"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
										clickElement(By.xpath(
												"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
														+ "'" + Children + "'" + ")]"));
										System.out.println(
												"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
														+ "'" + Children + "'" + ")]");
										String Membersdetails = TestCaseData[n][16];
										if (Membersdetails.contains(",")) {

											BaseClass.membres = Membersdetails.split(",");
										} else {
											System.out.println("Hello");
										}

										member: for (int i = 0; i <= BaseClass.membres.length; i++) {

											// one by one will take from 83 line
											mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
											mcountindex = mcountindex + 1;

											DropDowns.click();
											// List Age of members dropdown

											List<WebElement> List = driver.findElements(
													By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

											for (WebElement ListData : List) {

												if (ListData.getText()
														.contains(FamilyData[mcount][0].toString().trim())) {
													System.out
															.println("Age of Eldest Member is :" + ListData.getText());
													Thread.sleep(2000);
													ListData.click();

													if (count == membersSize) {
														break outer;
													} else {
														count = count + 1;
														// break member;
														break outer;
													}

												}

											}
										}

									}
								}
							}
						} else {

							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("5 - 24 years")) {

									String Membersdetails = TestCaseData[n][16];
									if (Membersdetails.contains(",")) {
										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}

										}
									}

								}
							}
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Select Total Members.");
					logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
				}

				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][18].toString().trim());
				clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " +
				// SumInsured + ")]"));
				System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");

				// Reading the value of Tenure from Excel
				int Tenure = Integer.parseInt(TestCaseData[n][19].toString().trim());
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					// clickElement(By.xpath(Radio_Tenure1_xpath));
					System.out.println("Selected Tenure is : " + Tenure + " Year");
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					// clickElement(By.xpath(Radio_Tenure2_xpath));
					System.out.println("Selected Tenure is : " + Tenure + " Year");
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					// clickElement(By.xpath(Radio_Tenure3_xpath));
					System.out.println("Selected Tenure is : " + Tenure + " Year");
				}

				// Scroll Window Up
				BaseClass.scrollup();

				/*// NCB Super Addon Selection
				String NCBSuper = TestCaseData[n][26].toString().trim();
				waitForElement(By.xpath(Checkbox_CareWithNCB_xpath));
				if (NCBSuper.contains("for care with NCB Super")) {
					clickbyHover(By.xpath(Checkbox_CareWithNCB_xpath));
					// clickElement(By.xpath(Checkbox_CareWithNCB_xpath));
					System.out.println("Selected Addon is : " + NCBSuper);
				} else if (NCBSuper.contains("for care")) {
					clickbyHover(By.xpath(Checkbox_Care_xpath));
					// clickElement(By.xpath(Checkbox_Care_xpath));
				}*/

				// UAR Addon Selection
				String UAR = TestCaseData[n][27].toString().trim();
				waitForElement(By.xpath(Checkbox_UAR_xpath));
				if (UAR.contains("Unlimited Recharge")) {
					clickbyHover(By.xpath(Checkbox_UAR_xpath));
					// clickElement(By.xpath(Checkbox_UAR_xpath));
					System.out.println("Selected Addon is : " + UAR);
				} else if (UAR.contains("No") || UAR.contains("NO") || UAR.contains("no")) {
					System.out.println("UAR Addon is not Selected.");
				}

				// Everyday Care Addon Selection
				String EverydayCare = TestCaseData[n][28].toString().trim();
				waitForElement(By.xpath(Checkbox_EverydayCare_xpath));
				if (EverydayCare.contains("Everyday Care")) {
					clickbyHover(By.xpath(Checkbox_EverydayCare_xpath));
					// clickElement(By.xpath(Checkbox_EverydayCare_xpath));
					System.out.println("Selected Addon is : " + EverydayCare);
				} else if (EverydayCare.contains("No") || EverydayCare.contains("NO") || EverydayCare.contains("no")) {
					System.out.println("EverydayCare Addon is not Selected.");
				}

				// PA Care Addon Selection
				/*Thread.sleep(5000);
				String PA = TestCaseData[n][29].toString().trim();
				Thread.sleep(7000);
				if (PA.contains("Personal Accident")) {
					clickbyHover(By.xpath(Checkbox_PA__xpath));
					// clickElement(By.xpath(Checkbox_PA__xpath));
					System.out.println("Selected Addon is : " + PA);
				} else if (PA.contains("No") || PA.contains("NO") || PA.contains("no")) {
					System.out.println("PA Addon is not Selected.");
				}*/

				// Verify TotalMembers from Excel and Quotation Page
				String TotalMemberPresentonQuotation = driver
						.findElement(By.xpath(TotalMemberPresentQuotaion_CareHNI_xpath)).getText();
				System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);

				// Capture The Premium value
				Thread.sleep(5000);
				String Quotationpremium_value = driver.findElement(By.xpath(Quotationpage_premium_xpath)).getText();
				System.out.println("Total Premium Value on Quotation Page is :" + " - "
						+ Quotationpremium_value.substring(1, Quotationpremium_value.length()));

				// Click on BuyNow Button
				try {
					Thread.sleep(3000);
					clickElement(By.xpath(Button_Buynow_xpath));
					System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
				} catch (Exception e) {
					System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
				}

				// Premium verification on Proposal Page
				Thread.sleep(10000);
				String Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']"))
						.getText();
				String ProposalPremimPage_Value = Proposalpremium_value.substring(1, Proposalpremium_value.length());
				System.out.println("Total Premium Value on Proposal Page is : " + " - "
						+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
				try {
					Assert.assertEquals(Quotationpremium_value, Proposalpremium_value);
					logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
							+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
				} catch (AssertionError e) {
					System.out.println(Quotationpremium_value + " - failed");
					logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

				}

				/*// Edit starts Here
				String executionstatus = TestCase[n][4].toString().trim();
				if (executionstatus.equals("Edit")) {
					Edit.careEdit();
				} else {
					System.out.println("Edit not required.............");
				}
                scrolldown();*/
				
				// Reading Proposer Title from Excel
				scrollup();
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				Thread.sleep(5000);
				clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);

				BaseClass.selecttext("ValidTitle", Title.toString());

				// Entering DOB from Excel into dob field
				driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
				String DOB = TestCaseData[n][4].toString().trim();
				System.out.println("date is:" + DOB);
				enterText(By.id("proposer_dob"), String.valueOf(DOB));
				logger.log(LogStatus.PASS, "Entered DOB is :" + DOB);

				Thread.sleep(3000);
				final String address1 = TestCaseData[n][7].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				enterText(By.xpath(addressline1_xpath), address1);
				enterText(By.xpath(addressline2_xpath), TestCaseData[n][8].toString().trim());
				enterText(By.xpath(pincode_xpath), TestCaseData[1][9]);
				logger.log(LogStatus.PASS, "Entered Address name is :" + address1);
				logger.log(LogStatus.PASS, "Entered Address name is :" + TestCaseData[n][8].toString().trim());
				logger.log(LogStatus.PASS, "Entered nominee name is :" + TestCaseData[n][9]);

				// Height selection
				String Height = TestCaseData[n][10].toString().trim();
				System.out.println("Height value from excel  is:" + Height);
				clickElement(By.xpath(height_xpath));
				BaseClass.selecttext("heightFeet", Height.toString().trim());
				// Inch Selection
				String Inch = TestCaseData[n][11].toString().trim();
				System.out.println("Inch value from excel  is:" + Inch);
				clickElement(By.xpath(inch_xpath));
				BaseClass.selecttext("heightInches", Inch.toString().trim());
				String Weight = TestCaseData[n][12].toString().trim();
				System.out.println("Weight is :" + Weight);
				enterText(By.xpath(weight_xpath), Weight);
				logger.log(LogStatus.PASS, "Entered Height,Inch and Weight is :" + Height + Inch + Weight);

				String NomineeName = TestCaseData[n][13].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
				logger.log(LogStatus.PASS, "Entered nominee name is :" + NomineeName);

				// Nominee Relation
				String Nrelation = TestCaseData[n][14].toString().trim();
				System.out.println("Nominee  relation from excel  is:" + Nrelation);
				clickElement(By.xpath(Nominee_relation_xpath));
				BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
				logger.log(LogStatus.PASS, "Entered nominee relation is :" + Nrelation);

				String pancard = TestCaseData[n][19].toString().trim();

				String pospancard = "KJHYS8977E";
				System.out.println("pancard number is :" + pospancard);
				try {
					driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).sendKeys(pospancard);
				} catch (Exception e) {
					System.out.println("Pan card field not visibled");
				}
				logger.log(LogStatus.PASS, "Entered Pancard number  is :" + pospancard);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
				Thread.sleep(7000);
				clickElement(By.xpath(submit_xpath));

				/*
				 * ===============================================Insured Details
				 * Page=====================================================
				 */

				Thread.sleep(3000);
				/*
				 * for (int i = 0; i <= BaseClass.membres.length - 1; i++) { mcount =
				 * Integer.parseInt(BaseClass.membres[i].toString());
				 * 
				 * // String firstName= "fname"+i+"_xpath"; String Date =
				 * FamilyData[mcount][5].toString().trim();
				 */

				scrollup();

				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {

						// String firstName= "fname"+i+"_xpath";
						String Date = FamilyData[mcount][5].toString().trim();

						BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
						// title
						BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
						enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
						enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
						clickElement(By.name("rel_dob" + i));
						enterText(By.name("rel_dob" + i), String.valueOf(Date));

						BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
						BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
						enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

					}
				}

				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");

				// Health Questionnarire Elements
				String preExistingdeases = TestCaseData[n][21].toString().trim();
				Thread.sleep(3000);
				System.out.println(
						"Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				// BaseClass.scrollup();
				try {
					if (preExistingdeases.contains("YES")) {

						waitForElements(By.xpath(YesButton_xpath));
						clickElement(By.xpath(YesButton_xpath));
						// Thread.sleep(2000);
						String years = null;
						String Details = null;
						for (int qlist = 1; qlist <= 13; qlist++) {
							Details = QuestionSetData[n][qlist + (qlist - 1)].toString().trim();
							years = QuestionSetData[n][qlist + qlist].toString().trim();
							if (Details.equals("")) {
								// break;
							} else {
								int detailsnumber = Integer.parseInt(Details);

								// Will click on check box and select the month & year u
								detailsnumber = detailsnumber + 1;
								System.out.println("Details and years are :" + Details + "----" + years);
								clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
										+ detailsnumber + "]//input[@type='checkbox']"));
								Thread.sleep(1000);
								try {
									clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label"));
									enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label"), years);
								} catch (Exception e) {
									clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label[@class='monthYear']"));
									enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label[@class='monthYear']"), years);
								}
							}
						}
					} else if (preExistingdeases.contains("NO")) {
						clickElement(By.xpath(NoButton_xpath));
					}
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

				// BaseClass.scrolldown();
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 3; morechecks++) {
					int mch = morechecks + 1;
					String ChecksData = TestCaseData[n][21 + morechecks].toString().trim();

					try {
						if (ChecksData.contains("NO")) {
							System.out.println("Quatation set to NO");

							clickElement(By.xpath("//label[@for='question_" + mch + "_no']"));
						} else {
							driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
							if (ChecksData.contains(",")) {
								ChckData = ChecksData.split(",");
								for (String Chdata : ChckData) {
									datacheck = Integer.parseInt(Chdata);
									datacheck = datacheck - 1;
									driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
											.click();
								}
							} else if (ChecksData.contains("")) {
								datacheck = Integer.parseInt(ChecksData);
								datacheck = datacheck - 1;
								driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
										.click();
							}
						}
					} catch (Exception e) {
						logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
					}

				}

				// Pa Question
				String JobRisk = TestCaseData[n][25].toString().trim();
			/*	System.out.println("PA Question Value : " + JobRisk);
				if (PA.contains("Personal Accident") && JobRisk.contains("YES")) {
					clickElement(By.xpath("//label[@for='question_5_yes']"));
				} else if (PA.contains("NO") && JobRisk.contains("NO")) {
					System.out.println("PA Question is not Selected");
				} else if (PA.contains("Personal Accident") && JobRisk.contains("NO")) {
					clickElement(By.xpath("//label[@for='question_5_no']"));
				}*/

				// Check Box on Health Questionnaire
				BaseClass.scrolldown();
				BaseClass.HelathQuestionnairecheckbox();

				try {
					clickElement(By.xpath(proceed_to_pay_xpath));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
					System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
				}

			Thread.sleep(20000);
				// sBaseClass.ErroronHelathquestionnaire();
				Thread.sleep(10000);
				String proposalSummarypremium_value = (new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']")))
						.getText();
				System.out.println("Total premium value is: " + proposalSummarypremium_value);

				String executionstatusNCB = TestCase[n][4].toString().trim();
				if (executionstatusNCB.equals("Edit")) {

					BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage_afterEdit(
							Edit.afterEdit_Proposalpremium_value, proposalSummarypremium_value);

				} else {
					BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage(ProposalPremimPage_Value,
							proposalSummarypremium_value);
				}
				// BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage(ProposalPremimPage_Value,proposalSummarypremium_value);
				// BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage_afterEdit(Edit.afterEdit_Proposalpremium_value,proposalSummarypremium_value);

				try {
					(new WebDriverWait(driver, 20))
							.until(ExpectedConditions
									.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
							.click();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println("unable to load psoposal summary payment method page :");
				}

				waitForElement(By.xpath(payu_proposalnum_xpath));
				String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
				System.out.println("Pay U Proposal Numeber : " + PayuProposalNum);

				waitForElement(By.xpath(payuAmount_xpath));
				String PayuPremium = driver.findElement(By.xpath(payuAmount_xpath)).getText();
				String FinalAmount = PayuPremium.substring(0, PayuPremium.length() - 2);
				System.out.println("Pay U Premium Amount : " + FinalAmount);

				BaseClass.PayuPage_Credentials();

				Thread.sleep(2000);
				BaseClass.scrolldown();

				Thread.sleep(10000);
				try {
					String PayuTimeout = driver.findElement(By.xpath("/html/body/h1")).getText();
					logger.log(LogStatus.FAIL,
							"Test Case is Failed because  Payu is downn and getting : " + PayuTimeout);

				} catch (Exception e) {
					System.out.println("Test Case Conti...");
				}

				String expectedTitle = "Your payment transaction is successful !";
				Fluentwait(By.xpath(ExpectedMessage_xpath));
				String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
				try {
					Assert.assertEquals(expectedTitle, actualTitle);
					logger.log(LogStatus.INFO, actualTitle);
				} catch (AssertionError e) {
					System.out.println("Payment Failed");
					String FoundError = driver
							.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p"))
							.getText();
					logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
					TestResult = "Fail";
				}

				String ProposerName = driver
						.findElement(By.xpath(
								"/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p"))
						.getText();
				System.out.println(ProposerName);

				String Thankyoupagepremium_value = driver
						.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
				System.out.println("Total premium value is:" + Thankyoupagepremium_value);

				try {
					Assert.assertEquals(proposalSummarypremium_value, Thankyoupagepremium_value);
					logger.log(LogStatus.INFO,
							"Proposal Summuary Premium and Thankyou page Premium is Verified and Both are Same i.e : "
									+ Thankyoupagepremium_value.substring(0, Thankyoupagepremium_value.length()));
					System.out.println("Proposal Summuary Premium and Thankyou page Premium are Same and the premium value is : "+proposalSummarypremium_value +Thankyoupagepremium_value);
				} catch (AssertionError e) {
					System.out.println(proposalSummarypremium_value + " - failed");
					logger.log(LogStatus.FAIL, "Proposal Summuary Premium and Thankyou page Premium are not Same");
					throw e;
				}

				// BaseClass.DBVerification(PayuProposalNum);
				waitForElement(By.xpath(PolProp_xpath));
				String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
				String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();

				BaseClass.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);

				WriteExcel.setCellData1("Test_Cases_Care", TestResult, ThankyoupageProposal_Pol_num, n, 2, 3);

				Thread.sleep(4000);
				 driver.quit();


			} catch (Exception e) {
				WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				 driver.quit();


			}
			continue;

		}
	}
}
