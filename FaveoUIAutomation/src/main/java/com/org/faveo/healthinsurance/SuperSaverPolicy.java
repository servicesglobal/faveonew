package com.org.faveo.healthinsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class SuperSaverPolicy extends BaseClass implements AccountnSettingsInterface {

	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static String testDataPath = "Favio_Framework.xlsx";
	public static String TestResult;
	public static Integer n;
	@Test
	public static void SuperSaver() throws Exception {
		
				// To get the number of rows present in sheet
				ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
				int rowCount = fis.getRowCount("Test_Cases_SuperSaver");
				System.out.println("Total Number of Row in Sheet : "+rowCount);

				for(n=1;n<=rowCount;n++)
			{
		
					try{
			// To get the number of rows present in sheet
			String[][] TestCase = BaseClass.excel_Files("Test_Cases_SuperSaver");
			String[][] TestCaseData = BaseClass.excel_Files("SuperSaver_Quotation");
			String[][] FamilyData = BaseClass.excel_Files("SuperSaver_Insured_Details");
			String[][] QuestionSetData = BaseClass.excel_Files("SuperSaver_QuestionSet");
			
			//Launching Browser using Method of Base Class
			BaseClass.LaunchBrowser();

			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("Care Super Saver - " + TestCaseName);
			System.out.println("Care Super Saver - "+TestCaseName);
			
			//Login with the help of Login Case class
			LoginCase.LoginwithValidCredendial();
			
/*===============================================Dashboard========================================================*/		

			//Clicking on CAREHNI Product dropdown
			HealthInsuranceDropDown.SuperSaverPolicy();
			
/*===============================================Quotation Page========================================================*/

			
			
			// Reading Proposer Name from Excel
			waitForElement(By.xpath(Textbox_Name_xpath));
			String Name = (TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
			enterText(By.xpath(Textbox_Name_xpath), Name);
			System.out.println("Proposer Name is : " + Name);
			
			
			
			// Reading Emailfrom Excel Sheet
			waitForElement(By.xpath(Textbox_Email_xpath));
			String Email = TestCaseData[n][6];
			System.out.println("Email of Proposer is :" + Email);
			enterText(By.xpath(Textbox_Email_xpath), Email);
			
			
			
			// Reading Mobile Number from Excel
			waitForElement(By.xpath(Textbox_Mobile_xpath));
			String MobileNumber = TestCaseData[n][5].toString().trim();
			int size = MobileNumber.length();
			System.out.println("Mobile number is: " + MobileNumber);
			if (isValid(MobileNumber)) {
				System.out.println("Is a valid number");
				enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
			} else {
				System.out.println("Not a valid Number");
			}
			
			
			// Enter The Value of Total members present in policy
			Thread.sleep(10000);
			List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			try{
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
				driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[n][15].toString().trim() + "]")).click();
					System.out.println("Total Number of Member Selected : " + TestCaseData[n][15].toString().trim());
					Thread.sleep(10000);
					break;
				}
			}}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
				BaseClass.AbacusURL();
			}
			
			
			
			// again call the dropdown
			Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			// Thread.sleep(3000);
			int membersSize = Integer.parseInt(TestCaseData[n][15].toString().trim());
			int count = 1;
			int mcount;
			int mcountindex = 0;
			int covertype;
			System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3

			outer:

			for (WebElement DropDownName : dropdown) {
				// System.out.println("DropDownName is " +
				// DropDownName.getText());
				if (membersSize == 1) {

					// reading members from test cases sheet memberlist
					String Membersdetails = TestCaseData[n][16];
					System.out.println("Total Number of Member in Excel :" + Membersdetails);
					if (Membersdetails.contains("")) {

						BaseClass.membres = Membersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {
							// System.out.println("Mdeatils is :
							// "+membres);

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;
							System.out.println("Mcount Index : " + mcountindex);

							driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();
							// List Age of members dropdown
							List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));
							System.out.println("List Data is : " + List);

							for (WebElement ListData : List) {

								if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("List Data is :" + ListData.getText());

									ListData.click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										break member;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ covertype + "]")).click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) 
					{
						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("18 - 24 years")) {
								// reading members from test cases sheet
								// memberlist
					int Children = Integer.parseInt(TestCaseData[n][26].toString().trim());
					clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
					clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
					System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
								String Membersdetails = TestCaseData[n][16];
								if (Membersdetails.contains(",")) {

									// data taking form test case sheet
									// which is
									// 7,4,1,8,2,5
									BaseClass.membres = Membersdetails.split(",");
								} else {
									//BaseClass.membres = Membersdetails.split(" ");
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
									// System.out.println("Mdeatils is :
									// "+membres);

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown
									Thread.sleep(5000);
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {
										

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(5000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}
								
							}
						}
					}
				} 
				else{

					List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
					for (WebElement DropDowns : dropdowns) 
					{
						if (DropDowns.getText().contains("Individual")) 
						{
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")) {
			
							String Membersdetails = TestCaseData[n][16];
							if (Membersdetails.contains(",")) {
								BaseClass.membres = Membersdetails.split(",");
							} else 
							{
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
							
								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								Thread.sleep(5000);
								List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								for (WebElement ListData : List) {
									

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("List Data is :" + ListData.getText());
										Thread.sleep(5000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											break member;
										}

									}

								}
							}
							
						}
					}
				}
			}

			
			// Read the Value of Suminsured
			waitForElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
			int SumInsured = Integer.parseInt(TestCaseData[n][18].toString().trim());
			System.out.println(SumInsured);
			clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
			clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
			//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
			//logger.log(LogStatus.PASS, "Entered SumInsured is " + " - " + SumInsured + " " + "Lakhs");

			// Reading the value of Tenure from Excel
			int Tenure = Integer.parseInt(TestCaseData[n][19].toString().trim());
			System.out.println(Tenure);
			if (Tenure == 1) {
				clickbyHover(By.xpath(Radio_Tenure1_xpath));
				//clickElement(By.xpath(Radio_Tenure1_xpath));
			} else if (Tenure == 2) {
				clickbyHover(By.xpath(Radio_Tenure2_xpath));
				//clickElement(By.xpath(Radio_Tenure2_xpath));
			} else if (Tenure == 3) {
				clickbyHover(By.xpath(Radio_Tenure3_xpath));
				//clickElement(By.xpath(Radio_Tenure3_xpath));
			}

			// Reading the value of Addons from Excel
			BaseClass.scrollup();
			

			// NCB Super Addon Selection
			Thread.sleep(7000);
			String NCBSuper = TestCaseData[n][25].toString().trim();
			System.out.println(NCBSuper);
			Thread.sleep(5000);
			clickbyHover(By.xpath("//b[@class='ng-binding'][contains(text()," + "'" + NCBSuper + "'" + ")]"));
			//clickElement(By.xpath("//b[@class='ng-binding'][contains(text()," + "'" + NCBSuper + "'" + ")]"));

			
			//Verify TotalMembers from Excel and Quotation Page
			String TotalMemberPresentonQuotation = driver.findElement(By.xpath(TotalMemberPresentQuotaion_CareHNI_xpath)).getText();
			System.out.println("Total Number of Member on Quotation Page : "+TotalMemberPresentonQuotation);
			
			
			// Capture The Premium value
			Thread.sleep(5000);
			String Quotationpremium_value = driver.findElement(By.xpath("//span[@ng-if='getQuote.mobileNumber.$valid && iconloading==false']")).getText();
			System.out.println("Total Premium Value on Quotation Page is :" + " - "+ Quotationpremium_value.substring(1, Quotationpremium_value.length()));
			//logger.log(LogStatus.PASS, "Total Premium Value on Quotation Page is : "+ Quotationpremium_value.substring(1, Quotationpremium_value.length()));

			
			
			// Click on BuyNow Button
			try{
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				System.out.println("Sucessfully Clicked on Buy Now Button from Quotation Page.");
					}
					catch(Exception e){
					
						System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
					
					}

/*===============================================Proposer Detail Page========================================================*/

			// Premium verification
			Thread.sleep(10000);
			String Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
			String ProposalPremimPage_Value = Proposalpremium_value.substring(1, Proposalpremium_value.length());
			System.out.println("Total Premium Value on Proposal Page is : " + " - "
					+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
			logger.log(LogStatus.PASS, "Total Premium Value on Proposal Page is : "
					+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));

			try {
				Assert.assertEquals(Quotationpremium_value, Proposalpremium_value);
				logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
			} catch (AssertionError e) {
				System.out.println(Quotationpremium_value + " - failed");
				logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
				throw e;
			}

			String TotalMemberProposal = driver.findElement(By.xpath(TotalMemberProposal_xpath)).getText();
			System.out.println("Total Members on Proposal Page : " + TotalMemberProposal);
			try {
				Assert.assertEquals(TotalMemberPresentonQuotation, TotalMemberProposal);
				logger.log(LogStatus.INFO,
						"Number Of Members Verified on Quotation and ProposalPage Both are Same : "
								+ TotalMemberProposal);
			} catch (AssertionError e) {
				logger.log(LogStatus.INFO, "Number Of Members are diffrent on Quotation and ProposalPage");
			}
			//Edit starts Here
			String executionstatus=TestCase[n][4].toString().trim();
			if(executionstatus.equals("Edit")) {
				System.out.println("Edit starts here................");
				Edit.EditCareSuperSaver();
			}else {
				System.out.println("Edit not required.............");
			}
			
			
			
			
			/*String AddonV = driver.findElement(By.xpath("//div[@class='col-md-12 edit_qutote_summary_heding1 add_ons ng-scope']/button[contains(text(),'')]")).getText();
			System.out.println("Addons on Proposal Page : " + AddonV);*/


			// Reading Proposer Title from Excel
			String Title = TestCaseData[n][1].toString().trim();
			System.out.println("titel Name is:" + Title);
			clickElement(By.xpath(click_title_xpath));
			clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'" + Title + "'" + ")]"));

			// Entering DOB from Excel into dob field
			String DOB = TestCaseData[n][4].toString().trim();
			System.out.println("date is:" + DOB);
			clickElement(By.id(Dob_Proposer_id));
			enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));

			// Reading AddressLine 1 from Excel
			Thread.sleep(1000);
			String address1 = TestCaseData[n][7].toString().trim();
			System.out.println("Adress1 name is :" + address1);
			enterText(By.xpath(Textbox_AddressLine1_xpath), address1);

			// Reading AddressLine 2 from Excel
			String address2 = TestCaseData[n][8].toString().trim();
			System.out.println(address2);
			enterText(By.xpath(Textbox_AddressLine2_xpath), address2);

			// Reading Pincode from Excel
			int Pincode = Integer.parseInt(TestCaseData[n][9].toString().trim());
			System.out.println(Pincode);
			enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
			// logger.log(LogStatus.PASS, "Entered Pincode is " + " - " +
			// Pincode);

			// Reading Proposer Height in Feet from Excel
			String HeightProposer_Feet = TestCaseData[n][10].toString().trim();
			System.out.println("Height value from excel  is:" + HeightProposer_Feet);
			clickElement(By.xpath(DropDown_HeightFeet_xpath));
			clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text()," + HeightProposer_Feet + ")]"));

			// Reading Proposer Height in Inch from Excel
			String HeightProposer_Inch = TestCaseData[n][11].toString().trim();
			System.out.println("Inch value from excel  is:" + HeightProposer_Inch);
			clickElement(By.xpath(DropDown_HeightInch_xpath));
			clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text()," + HeightProposer_Inch + ")]"));

			// Reading Weight of Proposer from Excel
			String Weight = TestCaseData[n][12].toString().trim();
			System.out.println("Weight is :" + Weight);
			enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));

			// Reading Nominee Name from Excel
			String NomineeName = TestCaseData[n][13].toString().trim();
			System.out.println("Nominee name   is:" + NomineeName);
			enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);

			// Nominee Relation
			String NomineeRelation = TestCaseData[n][14].toString().trim();
			System.out.println("Nominee  relation from excel  is:" + NomineeRelation);
			clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
			clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"
					+ NomineeRelation + "'" + ")]"));

			// String pancard=TestCaseData[n][19].toString().trim();
			String PanCard = TestCaseData[n][20].toString().trim();
			System.out.println("pancard number is :" + PanCard);
			Boolean PanCardNumberPresence = driver
					.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input")).size() > 0;
			if (PanCardNumberPresence == true) {
				enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

			} else {
				System.out.println("PAN Card Field is not Present");
			}

			// Click on Next button
			try{
				clickElement(By.id(Button_NextProposer_id));
				System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
					System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
				}

			/*
			 * ===============================================Insured Details
			 * Page========================================================
			 */

		/*	Thread.sleep(3000);
			for (int i = 0; i <= BaseClass.membres.length - 1; i++) 
			{
				mcount = Integer.parseInt(BaseClass.membres[i].toString());*/
			for (int i = 0; i <= BaseClass.membres.length - 1 ; i++) {
				mcount = Integer.parseInt(BaseClass.membres[i].toString());
				if (i == 0) {
					clickElement(By.xpath(title1_xpath));
					// Select Self Primary
					BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
				} else {

				// String firstName= "fname"+i+"_xpath";
				String Date = FamilyData[mcount][5].toString().trim();
				// Date=Date.replaceAll("-", "/");
				// Relation
				BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
				// title
				BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
				enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
				enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
				clickElement(By.name("rel_dob" + i));
				enterText(By.name("rel_dob" + i), String.valueOf(Date));

				BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
				BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
				enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
			}
			}
			driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
			System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
			
/*===============================================HEALTHQUESTIONNAIRE Page========================================================*/

			String preExistingdeases=TestCaseData[n][21].toString().trim();
			Thread.sleep(5000);
			 BaseClass.scrollup();
			 System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" +preExistingdeases);
			 BaseClass.scrollup();	
			 if(preExistingdeases.contains("YES")) {
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 //Thread.sleep(2000);
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=13;qlist++) {
					 Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
					 years=QuestionSetData[n][qlist+qlist].toString().trim();
					 if(Details.equals("")) {
						 //break;
					 }else {
						 int detailsnumber = Integer.parseInt(Details);

						 //Will click on check box and select the month & year u
						 detailsnumber=detailsnumber+1;
						 System.out.println("Details and years are :"+Details+"----"+years);
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						 Thread.sleep(1000);
						 try {
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						 }catch(Exception e) 
						 {
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						 }
					 }
				 }	
				 }	else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
						clickElement(By.xpath(NoButton_xpath));
					}
							
/*					BaseClass.scrolldown();	
*/					
					String ChecksData = null;
					String[] ChckData = null;
					int datacheck = 0;
					for (int morechecks = 1; morechecks <= 3; morechecks++) {
						int mch = morechecks + 1;
						ChecksData = TestCaseData[n][21 + morechecks].toString().trim();
						if (ChecksData.equalsIgnoreCase("NO")) {
							System.out.println("Quatation set to NO");
							driver.findElement(By.xpath("//label[@for='question_"+mch+"_no']")).click();
						} /*else if (ChecksData.equals("YES") || ChecksData.equalsIgnoreCase("Yes")) {
							System.out.println("Quatation set to only YES");
							driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
						}*/ else 
						{
							driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
							if (ChecksData.contains(",")) {
								ChckData = ChecksData.split(",");
								for (String Chdata : ChckData) {
									datacheck = Integer.parseInt(Chdata);
									datacheck = datacheck - 1;
									driver.findElement(
											By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
									// input[@name='question2_4']
								}
							} else {
								datacheck = Integer.parseInt(ChecksData);
								datacheck = datacheck - 1;
								driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
							}
						}

					}

			
			BaseClass.HelathQuestionnairecheckbox();
			
			try{
				clickElement(By.xpath(proceed_to_pay_xpath));
				System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
					System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
				}


			BaseClass.ErroronHelathquestionnaire();

			String proposalSummarypremium_value = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']"))).getText();
			System.out.println("Total premium value is:" + proposalSummarypremium_value);
			
			//BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage(ProposalPremimPage_Value,proposalSummarypremium_value);
			BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage_afterEdit(Edit.afterEdit_Proposalpremium_value_super,proposalSummarypremium_value);
			System.out.println("Before Pay U");
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
			System.out.println("After Pay U");


			waitForElement(By.xpath(payu_proposalnum_xpath));	
			String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
			System.out.println(PayuProposalNum);
			
			waitForElement(By.xpath(payuAmount_xpath));
			String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
			String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
			System.out.println(FinalAmount);
			
			BaseClass.PayuPage_Credentials();
			
		    Thread.sleep(2000);
			//Scroll down the page
		    BaseClass.scrolldown();
		    
		    Thread.sleep(10000);
			try {
				String PayuTimeout = driver.findElement(By.xpath("/html/body/h1")).getText();
				logger.log(LogStatus.FAIL, "Test Case is Failed because  Payu is downn and getting : " + PayuTimeout);

			} catch (Exception e) {
				System.out.println("Test Case Conti...");
			}
		    
		    //Thankyou Page Message Verfictaion
		    //BaseClass.Thankyoupageverification();
			String expectedTitle = "Your payment transaction is successful !";
			Fluentwait(By.xpath(ExpectedMessage_xpath));
		     String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
				try{
					Assert.assertEquals(expectedTitle,actualTitle);
			          logger.log(LogStatus.INFO, actualTitle);
			     }catch(AssertionError e){
			          System.out.println("Payment Failed");
			          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
			          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
			     }
			
		    
		    String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
			System.out.println(ProposerName);
		     
			String Thankyoupagepremium_value = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
			System.out.println("Total premium value is:" + Thankyoupagepremium_value);

			try {
				Assert.assertEquals(proposalSummarypremium_value, Thankyoupagepremium_value);
				logger.log(LogStatus.INFO,"Proposal Summuary Premium and Thankyou page Premium is Verified and Both are Same i.e : "+ Thankyoupagepremium_value.substring(1, Thankyoupagepremium_value.length()));
			} catch (AssertionError e) {
				System.out.println(proposalSummarypremium_value + " - failed");
				logger.log(LogStatus.FAIL, "Proposal Summuary Premium and Thankyou page Premium are not Same");
				throw e;
			}

			//BaseClass.DBVerification(PayuProposalNum);

			waitForElement(By.xpath(PolProp_xpath));
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			
			BaseClass.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);
			
			WriteExcel.setCellData1("Test_Cases_SuperSaver", TestResult, ThankyoupageProposal_Pol_num, n, 2, 3);

			driver.close();

		} catch (Exception e) {
			WriteExcel.setCellData("Test_Cases_SuperSaver", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
			driver.close();
		}
					continue;
}
}
}