package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.SuperMediclaimCriticalFunctions;
import com.org.faveo.PolicyJourney.SuperMediclaimCancerPolicyJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestionSuperMediClaim;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class SuperMediclaimCritical1 extends SuperMediclaimCriticalFunctions{
	
	//public static int rowc;
	
	@Test(priority = 1, enabled = false)
	public static void supermediclaimcritical() throws Exception {

		System.out.println("Super Medical : Critical Script Execution Started");
		BaseClass.excel_Files("SuperMediclaimCriticalTestCase");
		System.out.println("rowCount is " + rowCount);
		System.out.println("Total number of rows : " + BaseClass.rowCount);
		 int rowc = rowCount;
		 
		 String[][] TestCase = BaseClass.excel_Files("SuperMediClaimTestCaseList");
        
		for(n=1;n<=1;n++) {
	   
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
			
		  /* SuperMediclaimCriticalFunctions.LaunchBrowser1();
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.login();*/

			try {
				LoginFn.LoginPage();
				Thread.sleep(5000);
				System.out.println("Total number of rows : " + rowCount);
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				SuperMediclaimCriticalFunctions.critical_verify_premium();
				//SuperMediclaimCriticalFunctions.Critical_proposerDetails();
				SuperMediclaimCriticalFunctions.critical_insuredDetails();
				// Reusable.critical_questionset();
				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				SuperMediclaimCriticalFunctions.Critical_Payment();
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//driver.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
			}
			continue;
		}

		}
	
	//************************ Test Cases for Shared Quote *******************************************
	// Test case - 1 - Verify after punching the policy through Shared Quotation, the Proposal should exit from the Quotes Tracker Page
	@Test(priority = 2, enabled = false)
	public static void verifySharedQuotationScenario_InSuperMediclaimCritical() throws Exception {
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");

		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		/*String TestCaseName = (TestCase[1][0].toString().trim() + " - " + TestCase[1][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share quotation -" + TestCaseName);
		System.out.println(TestCaseName);*/

		for(n=1; n<=6; n++){
			String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
			logger = extent.startTest("Super Medicliam Critical Share quotation -" + TestCaseName);
			System.out.println(TestCaseName);
			
		try {
			//Step 1 - Open Email and Delete any Old Email
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");

			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit();
           
			// Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
			System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
			logger.log(LogStatus.PASS, "Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");

			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
			SuperMediclaimCriticalFunctions.criticaldropdown();
			SuperMediclaimCriticalFunctions.critical_dragdrop();
			SuperMediclaimCriticalFunctions.Tenurecritical();
			SuperMediclaimCriticalFunctions.Monthly_frequency();
			AddonsforProducts.readPremiumFromFirstPage();

			ReadDataFromEmail.readAgeGroup();

			DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
			DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimCritical(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

			driver.quit();

			// Step 3 - Open Email and Verify Data in Email Body
			System.out.println("Step 3 - Open Email and Verify Data in Email Body");
			logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
			LaunchBrowser();
			DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimCritical(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

			// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
			System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
			logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");
			
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();

			SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
			SuperMediclaimCriticalFunctions.Critical_Payment();
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			driver.quit();

			// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
			System.out.println(
					" Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
			logger.log(LogStatus.PASS,
					" Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
			LoginFn.LoginPage1("Super Mediclaim");
			DataVerificationShareQuotationPage
					.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimritical(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

			// Step 6- Again Open the Email and Verify GUID Should be Reusable
			System.out.println("Again Open the Email and Verify GUID Should be Reusable");
			logger.log(LogStatus.PASS, "Again Open the Email and Verify GUID Should be Reusable");

			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

			// Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");

			// driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			// driver.quit();
		}
		continue;
		}
	}
	
	//******************************** Shared Proposals Test Case 1 ******************************************
	
	
	
	
	//Test Case 1 -  Verify that details the share Proposal details in Quotes Tracker
	@Test(priority = 3, enabled = true)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
        
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		
		ReadExcel read = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("SuperMediClainCancerTestCase");
		//n=2;
		for (int i=2; i <= 2; i++) {
			n=i;
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");
		//String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		
		 
			try {
				
				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS,"Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				Thread.sleep(5000);
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				//SuperMediclaimCriticalFunctions.critical_verify_premium();
				
				SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
				SuperMediclaimCriticalFunctions.critical_insuredDetails();
				
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCritical(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();

				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				
				driver.quit();
				
				// Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS,"Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCritical(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");

				driver.quit();
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
			}
		
         continue;
		}
	}
	
	@Test(priority = 4, enabled = false)
	public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
        
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		
		ReadExcel read = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = read.getRowCount("SuperMediClainCancerTestCase");
		//n=2;
		for (int i=1; i <= 1; i++) {
			n=i;
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");
		//String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("Super Medicliam Critical Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);
		
		 
			try {
				
				//Step 1 - Open Email and Delete Old Email
				System.out.println("Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit();
				
				//Step 2 - Go to Application and share a proposal
				System.out.println("Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Go to Application and share a proposal");
				
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				Thread.sleep(5000);
				SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
				
				SuperMediclaimCriticalFunctions.criticaldropdown();
				SuperMediclaimCriticalFunctions.critical_dragdrop();
				SuperMediclaimCriticalFunctions.Tenurecritical();
				SuperMediclaimCriticalFunctions.Monthly_frequency();
				
				AddonsforProducts.readPremiumFromFirstPage();
				ReadDataFromEmail.readAgeGroup();
				
				
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
				SuperMediclaimCriticalFunctions.critical_insuredDetails();
				
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				driver.quit();
				
				//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
				System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				
			    LaunchBrowser();
			    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForSuperMediclaimCrtical(n);
			    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Step 4 - Click on Buy Now button from Email Body and punch the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
				
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
				DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
				
				SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				driver.quit();
				
				
				// Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS,"Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCritical(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");

				driver.quit();
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				 //driver.quit();  
				
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//driver.quit();
			}
		
         continue;
		}
	}
	
	//*******************************************************************************************************************************
	
	/*Test Case 2 -  Verify the share quotation proposal details in Draft History and if it is avilable Click on Resume Policy and punch 
	the policy and verify That policy should be exit from draft */
	@Test(priority = 7, enabled = false)
	public static void verifyDataInQuotationTrackerAndPunchThePolicyOnClickResumeProposal_ForShraeProposal_InSuperMediclaimCancer()
			throws Exception {

		System.out.println("Super Medical : Critical Script Execution Started");
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");
		
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[2][0].toString().trim() + " - " + TestCase[2][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);

		n = 1;
		try {

			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
			SuperMediclaimCriticalFunctions.criticaldropdown();
			SuperMediclaimCriticalFunctions.critical_dragdrop();
			SuperMediclaimCriticalFunctions.Tenurecritical();
			SuperMediclaimCriticalFunctions.Monthly_frequency();
			SuperMediclaimCriticalFunctions.critical_verify_premium();
			SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);

			// Step 2 - Go to Draft History and verify Shared Proposal Data
			// should be available and complete punch the policy on Clicking
			// Resume Policy link
			System.out.println(
					"Go to Draft History and verify Shared Proposal Data should be available and complete punch the policy on Clicking Resume Policy link");
			logger.log(LogStatus.PASS,
					"Go to Draft History and verify Shared Proposal Data should be available and complete punch the policy on Clicking Resume Policy link");
			DataVerificationShareProposalPage.verifyDataInOfShareProposalInDrafthistoryAndPunchThePolicyOnClickResumePolicy_ForSuperMediclaimCritical(n);
			SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
			SuperMediclaimCriticalFunctions.Critical_Payment();
			driver.quit();
			
			//Step 3 - Again Login the application and verify shared proposal should not be available in Draft History
			System.out.println("Again Login the application and verify shared proposal should not be available in Draft History");
			logger.log(LogStatus.PASS, "Again Login the application and verify shared proposal should not be available in Draft History");
			
			LoginFn.LoginPage1("Super Mediclaim");
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCritical(n);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
		}

			}
	/*Test Case 3 -   Verify the Share Proposal details in the Mail */
	@Test(priority = 8, enabled = false)
	public static void verifyDataInEmail_ForShareProposal_InSuperMediclaimCancer()
			throws Exception {

		System.out.println("Super Medical : Critical Script Execution Started");
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[3][0].toString().trim() + " - " + TestCase[3][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);

		n = 1;
		try {
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Go to Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
			SuperMediclaimCriticalFunctions.criticaldropdown();
			SuperMediclaimCriticalFunctions.critical_dragdrop();
			SuperMediclaimCriticalFunctions.Tenurecritical();
			SuperMediclaimCriticalFunctions.Monthly_frequency();
			ReadDataFromEmail.readAgeGroup();
			SuperMediclaimCriticalFunctions.critical_verify_premium();
			SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			//ReadDataFromEmail.readAgeGroup();
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			driver.quit(); 
			
			//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
			System.out.println("Again Open the email and verify the data of Share Proposal in Email Body");
			logger.log(LogStatus.PASS, "Again Open the email and verify the data of Share Proposal in Email Body");
			
		    LaunchBrowser();
		    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForSuperMediclaimCrtical(n);
		    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
		}

			}
	
	/*Test Case 4 - Verify the Proposal Generated GUID should be reusable */
	@Test(priority = 9, enabled = false)
	public static void verifyProposalGeneratedGUIDShouldBeReusable_ForShareProposal_InSuperMediclaimCancer()
			throws Exception {

		System.out.println("Super Medical : Critical Script Execution Started");
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[4][0].toString().trim() + " - " + TestCase[4][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);

		n = 1;
		try {
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Go to Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
			SuperMediclaimCriticalFunctions.criticaldropdown();
			SuperMediclaimCriticalFunctions.critical_dragdrop();
			SuperMediclaimCriticalFunctions.Tenurecritical();
			SuperMediclaimCriticalFunctions.Monthly_frequency();
			ReadDataFromEmail.readAgeGroup();
			SuperMediclaimCriticalFunctions.critical_verify_premium();
			SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			//ReadDataFromEmail.readAgeGroup();
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			driver.quit(); 
			
			//Step 3 - Open Email and Click on Buy Now button and punch the policy
			System.out.println("Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Open Email and Click on Buy Now button and punch the policy");
			
			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
			SuperMediclaimCriticalFunctions.Critical_Payment();
			driver.quit();
			
			//Step 4 - Again Open the Email and click on Buy Now button and Verify Buy Now link button should be work/GUID should be reusable
			System.out.println("Again Open the Email and click on Buy Now button and Verify Buy Now link button should be work/GUID should be reusable");
			logger.log(LogStatus.PASS, "Again Open the Email and click on Buy Now button and Verify Buy Now link button should be work/GUID should be reusable");
			
			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
			
			//Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
		}

			}
	
	/*Test Case 5 - Verify after punching the policy through Shared Proposal, the Proposal should exit from the Quotes Tracker Page */
	@Test(priority = 10, enabled = false)
	public static void VerifyafterpunchingthepolicythroughSharedProposaltheProposalshouldexitfromtheDraftHistoryPage_ForShareProposal_InSuperMediclaimCancer()
			throws Exception {

		System.out.println("Super Medical : Critical Script Execution Started");
		BaseClass.excel_Files1("SuperMediclaimCriticalTestCase");

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String TestCaseName = (TestCase[5][0].toString().trim() + " - " + TestCase[5][1].toString().trim());
		logger = extent.startTest("Super Medicliam Cancer Share Proposal -" + TestCaseName);
		System.out.println(TestCaseName);

		n = 1;
		try {
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Go to Application and share a proposal");
			
			LoginFn.LoginPage1("Super Mediclaim");
			Thread.sleep(5000);
			SuperMediclaimCriticalFunctions.super_mediclaim_critical_Quatotion("super_mediclaim_critical");
			SuperMediclaimCriticalFunctions.criticaldropdown();
			SuperMediclaimCriticalFunctions.critical_dragdrop();
			SuperMediclaimCriticalFunctions.Tenurecritical();
			SuperMediclaimCriticalFunctions.Monthly_frequency();
			ReadDataFromEmail.readAgeGroup();
			SuperMediclaimCriticalFunctions.critical_verify_premium();
			SuperMediclaimCriticalFunctions.Critical_proposerDetails1();
			SuperMediclaimCriticalFunctions.critical_insuredDetails();
			//ReadDataFromEmail.readAgeGroup();
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			DataVerificationShareProposalPage.verifyDataInOfShareProposalInDraftHistory_ForSuperMediclaimCritical(n);
			driver.quit(); 
			
			//Step 3 - Open Email and Click on Buy Now button and punch the policy
			System.out.println("Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Open Email and Click on Buy Now button and punch the policy");
			
			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			SuperMediclaimCriticalFunctions.super_criticalQuestionSet();
			SuperMediclaimCriticalFunctions.Critical_Payment();
			driver.quit();
			
			//Step 4 - Again Moved to application and Verify Shared Proposal Data should be not available in Draft History
			System.out.println("Again Moved to application and Verify Shared Proposal Data should be not available in Draft History");
			logger.log(LogStatus.PASS, "Again Moved to application and Verify Shared Proposal Data should be not available in Draft History");
			
			LoginFn.LoginPage1("Super Mediclaim");
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMediclaimCritical(n);
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			
			driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			driver.quit();
		}

			}
	
	
	
}
