package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.CareHeartJourney;
import com.org.faveo.PolicyJourney.POSCareSmartSelectJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareHeart extends BaseClass implements AccountnSettingsInterface {
	 public static Integer n;
	
	@Test(priority=1, enabled=true)
	public static void careHeartJourney() throws Exception {
		
		String[][] TestCase=BaseClass.excel_Files("Care_Heart_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");
		
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Care_Heart_TestCase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		for(n=1;n<=1;n++) {
		
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
		System.out.println("Care Senior -" +TestCaseName);
		
		LaunchBrowser();
		//Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		HealthInsuranceDropDown.CareHeart();
		CareHeartJourney.careheartquotation();
		/*CareHeartJourney.careheartdp();
		CareHeartJourney.suminsuredCareHeart();*/
		/*
		CareHeartJourney.careHeartAddon();
		String ExecutionStatus=TestCase[n][4].toString().trim();
		if(ExecutionStatus.equals("Normal")) {
			CareHeartJourney.CareHeartwithNoEdit();
			}else if(ExecutionStatus.equals("Edit")){
				Edit.careheartEdit();
			}
	
		//QuotationandProposalVerification.PremiumAssertioncareheart();
		CareHeartJourney.careheartProposerdetails();
		CareHeartJourney.CareHeartInsuredDetails();
		CareHeartJourney.careHeartQuestionset();
		CareHeartJourney.careheartproposalsummaryandpayment();*/
		
   }
  }
	
	//*********************** Share Quote ********************************************
	@Test(priority=1, enabled=false)
	public static void careHeartJourney_ShareQuote() throws Exception {
		
		String[][] TestCase=BaseClass.excel_Files("Care_Heart_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");
		
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for(n=6; n<=6; n++) {
		
		try{
		
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
		System.out.println("Care Senior -" +TestCaseName);
		
		//Step 1 - Open Email and Delete any Old Email 
		System.out.println("Step 1 - Open Email and Delete Old Email");
		logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
	  
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit(); 
	   
		//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
		System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
		logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();  
		
		HealthInsuranceDropDown.CareHeart();
		CareHeartJourney.careheartquotation();
		CareHeartJourney.careheartdp();
		CareHeartJourney.suminsuredCareHeart();
		CareHeartJourney.careHeartAddon();
		
		
		AddonsforProducts.readPremiumFromFirstPage();
        //ReadDataFromEmail.readAgeGroup(); 

        DataVerificationShareQuotationPage.clickOnShareQuotationButton();
	    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForCareHeart(n);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();  
        
     // Step 3 - Open Email and Verify Data in Email Body
		System.out.println(" Open Email and Verify Data in Email Body");
		logger.log(LogStatus.PASS, " Open Email and Verify Data in Email Body");
		LaunchBrowser();
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForCareHeart(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));  
		
		// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
		System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
		logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");
		ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
	
		//QuotationandProposalVerification.PremiumAssertioncareheart();
		CareHeartJourney.careheartProposerdetails();
		CareHeartJourney.CareHeartInsuredDetails();
		CareHeartJourney.careHeartQuestionset();
		//CareHeartJourney.careheartproposalsummaryandpayment();
		
		try {
			(new WebDriverWait(driver, 40))
					.until(ExpectedConditions
							.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
					.click();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("unable to load psoposal summary payment method page :");
		}
		
		PayuPage_Credentials();
		driver.quit();
		
		 // Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
		System.out.println(
				" Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		logger.log(LogStatus.PASS,
				" Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
		LaunchBrowser();
		// Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareQuotationPage.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareHeart(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();  
		
		// Step 6- Again Open the Email and Verify GUID Should be Reusable
		System.out.println("Again Open the Email and Verify GUID Should be Reusable");
		logger.log(LogStatus.PASS, "Again Open the Email and Verify GUID Should be Reusable");

		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

		// Verify the Page Title
		DataVerificationShareProposalPage.verifyPageTitle();   

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		driver.quit();
		
   }
		catch(AssertionError e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			Thread.sleep(4000);
			driver.quit();
		}
		
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			Thread.sleep(4000);
			driver.quit();
		}
		
		continue;
	}
  }
	
	// ************ Test Case for Share Proposal ********************
	@Test(priority=2, enabled=false)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		
		String[][] TestCase=BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");
		
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Care_Heart_TestCase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		for(n=1;n<=1;n++) {
		
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
		System.out.println("Care Senior -" +TestCaseName);
		

		// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
		System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
		logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
	
		LaunchBrowser();
		//Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		HealthInsuranceDropDown.CareHeart();
		CareHeartJourney.careheartquotation();
		CareHeartJourney.careheartdp();
		CareHeartJourney.suminsuredCareHeart();
		CareHeartJourney.careHeartAddon();
		
		Thread.sleep(3000);
		clickElement(By.xpath(Button_Buynow_xpath));
	
		//QuotationandProposalVerification.PremiumAssertioncareheart();
		CareHeartJourney.careheartProposerdetails();
		CareHeartJourney.CareHeartInsuredDetails();
		
		//Set Details of Share Proposal Popup Box and Verify Data in Draft History
		DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
		DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForCareHeart(n);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		
		//Click on Resume Policy
		DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
		
		CareHeartJourney.careHeartQuestionset();
		
		try {
			(new WebDriverWait(driver, 40))
					.until(ExpectedConditions
							.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
					.click();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("unable to load psoposal summary payment method page :");
		}
		
		PayuPage_Credentials();
		driver.quit();
		//CareHeartJourney.careheartproposalsummaryandpayment();
		
		//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
		System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");

		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareHeart(n);

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");	
		
   }
  }
	
	
	// ************ Test Case for Share Proposal 2 ********************
		@Test(priority=3, enabled=false)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
			
			String[][] TestCase=BaseClass.excel_Files("ShareProposal");
			String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");
			
			
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("Care_Heart_TestCase");
			System.out.println("Total Number of Row in Sheet : "+rowCount);
			
			for(n=2;n<=2;n++) {
			try{
			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
			System.out.println("Care Senior -" +TestCaseName);
			

			//Step 1 - Open Email and Delete Old Email
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Step 2 - Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
			
			LaunchBrowser();
			//Login with the help of Login Case class
			LoginCase.LoginwithValidCredendial();
			HealthInsuranceDropDown.CareHeart();
			CareHeartJourney.careheartquotation();
			CareHeartJourney.careheartdp();
			CareHeartJourney.suminsuredCareHeart();
			CareHeartJourney.careHeartAddon();
			
			// Verify TotalMembers from Excel and Quotation Page
			Thread.sleep(5000);
			AddonsforProducts.readPremiumFromFirstPage();
            ReadDataFromEmail.readAgeGroup(); 
			
			Thread.sleep(3000);
			clickElement(By.xpath(Button_Buynow_xpath));
		
			//QuotationandProposalVerification.PremiumAssertioncareheart();
			CareHeartJourney.careheartProposerdetails();
			CareHeartJourney.CareHeartInsuredDetails();
			
			//Set Details of Share Proposal Popup Box and Verify Data in Draft History
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();
			
			//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
			System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			
		    LaunchBrowser();
		    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForCareHeart(n);
		    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			
			//Step 4 - Click on Buy Now button from Email Body and punch the policy
			System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
			
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
			
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			
			CareHeartJourney.careHeartQuestionset();
			
			try {
				(new WebDriverWait(driver, 40))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.out.println("unable to load psoposal summary payment method page :");
			}
			
			PayuPage_Credentials();
			driver.quit();
			//CareHeartJourney.careheartproposalsummaryandpayment();
			
			//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
			System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
			logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");

			LaunchBrowser();
			LoginCase.LoginwithValidCredendial();
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareHeart(n);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");	
			
			// Step 6- Again Open the Email and Verify GUID Should be Reusable
			System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
			logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

			// Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();   

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();  
	   }
		catch(Exception e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			Thread.sleep(4000);
			driver.quit();  

		}
			continue;
		}
			
		}
 }
