package com.org.faveo.healthinsurance;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class POSCareSuperSaver extends BaseClass implements AccountnSettingsInterface {

	public static  Integer n;
	@Test
	public static void POSSupersaver() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("TestCasePOSSuperSaverData");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembersPOSSuperSaver");
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSetPOSSuperSaver");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("TestCasePOSSuperSaverData");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for( n=1;n<rowCount;n++) {
		
			try{
		BaseClass.LaunchBrowser();
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("POS Care Super Saver -" + TestCaseName);
		System.out.println("POS Care Super Saver -" +TestCaseName);
		
		LoginCase.LoginwithValidCredendial();
		HealthInsuranceDropDown.POSSupersaverquot();
		
/*		driver.findElement(By.name("name")).clear();
		driver.findElement(By.name("name")).sendKeys(TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());*/
		// Reading Proposer Name from Excel
		String Name = (TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
		System.out.println("Entered Proposer Name is : " + Name);
		Fluentwait(By.xpath(Textbox_Name_xpath));
		try {
			enterText(By.xpath(Textbox_Name_xpath), Name);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Unable to click on Name Textbox on Quotation Page.");
		}

		logger.log(LogStatus.PASS, "Entered User Proposer Name: " + TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
		driver.findElement(By.name("ValidEmail")).clear();

		String email = TestCaseData[n][6].toString().trim();
		System.out.println("Email is :" + email);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		//logger = extent.startTest("Entered Email is :"+email);
		logger.log(LogStatus.PASS, "Entered Email is :"+email);

		Thread.sleep(5000);
		String mnumber = TestCaseData[n][5].toString().trim();
		int size = mnumber.length();
		System.out.println("mobile number is: " + mnumber);
		String format = "^[789]\\d{9}$";

		if (mnumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}
		logger.log(LogStatus.PASS, "Entered Mobile number is :"+mnumber);
		//Selecting all dropdowns
		

        String Membersize=TestCaseData[n][15].toString().trim();
		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[n][15].toString().trim() + "]")).click();//select 4 based on the excel data
					System.out.println("Total Number of Member Selected : " + TestCaseData[n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
			logger.log(LogStatus.PASS, "Selected total member is  :"+Membersize);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// again call the dropdown
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[1][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		try {
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[n][16];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By
											.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

									// List Age of members dropdown
									List<WebElement> List = driver
											.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());
						logger.log(LogStatus.PASS, "Selected covertype  is :"+TestCaseData[n][17].toString().trim());
						if (TestCaseData[n][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By
								.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
						.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[n][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println(
											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[n][26].toString().trim());
									clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
											+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[n][16];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							String Membersdetails = TestCaseData[n][16];
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							System.out.println("Drop down text is :"+DropDowns.getText());
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								System.out.println("Drop down text is :"+DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")) {
								//} else if (DropDowns.getText().equals("46 - 50 Yrs")) {
								System.out.println("text is "+Membersdetails);
								//String Membersdetails = TestCaseData[1][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										//List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
										List<WebElement> List = driver.findElements(By.xpath("//div[@class='form-group']//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li"));
										System.out.println("Total number of data is :"  +List.size());
										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}
										}
									}
							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Done ");
		}


	//Selecting Sum insured here
		
		String suminsure = TestCaseData[n][25];
		WebElement covertext=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a"));
		System.out.println("Cover Type is : " +covertext.getText());
		logger.log(LogStatus.PASS, "Entered sum insured value is :"+suminsure);

		if(covertext.getText().equals("Floater")) {

			WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[8]/div/div/div"));
			List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
			//String SliderValue=null;

			for(WebElement Slider:dragable) {
				//System.out.println("Policy type is : "+PolicyType);
				Thread.sleep(5000);
				if(	Slider.getText().equals(suminsure)) {
					Slider.click();
					break;
				}
			}

		}else {
			WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[7]/div/div/div"));
			List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
			//String SliderValue=null;

			for(WebElement Slider:dragable) {
			//	System.out.println("Policy type is : "+PolicyType);
				Thread.sleep(5000);
				if(	Slider.getText().equals(suminsure)) {
					Slider.click();
					break;
				}
			}
		}
//Selecting Tenure here
		
		int Tenure = Integer.parseInt(TestCaseData[n][18].toString().trim());
		BaseClass.scrolldown();
		System.out.println("Selected Tenure is : " +Tenure);
		if(Tenure==1)
		{
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			//clickElement(By.xpath(Radio_Tenure1_xpath));
		}
		else if(Tenure==2)
		{
			clickbyHover(By.xpath(Radio_Tenure2_xpath));	
			//clickElement(By.xpath(Radio_Tenure2_xpath));
		}
		else if(Tenure==3)
		{
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			//clickElement(By.xpath(Radio_Tenure3_xpath));
		}		
		logger.log(LogStatus.PASS,"Selected  Tenure  is :"+Tenure);

		BaseClass.scrollup();
		

		//	WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]/b[1]"));
		WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div/label[2]/span"));
		Thread.sleep(10000);
		String firstpage_PremiumValue=firstpage_Premium.getText();
		
		System.out.println("First page premium value is   :"  +firstpage_PremiumValue);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,150)", "");
		scrollup();
		clickElement(By.xpath(POSSuperSaver_xpath));
	//	Thread.sleep(5000);
		Thread.sleep(10000);
		jse.executeScript("window.scrollBy(0,-350)", "");
		//WebElement secondpage_premium=driver.findElement(By.xpath("//div[@class='col-md-12 tr_quotation_heading']/h4/span/span[@class='ng-binding']"));

		WebElement secondpage_premium=driver.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		String secondpage_PremiumValue=secondpage_premium.getText();
		Thread.sleep(10000);
		System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
		Assert.assertEquals(firstpage_PremiumValue, secondpage_PremiumValue);

	//Edit.EditPOSCaresupersaver();
		
		String executionstatus = TestCase[n][4].toString().trim();
		if (executionstatus.equals("Edit")) {
			Edit.EditPOSCaresupersaver();
		} else {
			System.out.println("Edit not required.............");
		}
scrollup();

		
		//Selecting Proposer Details Here
			
		String Title = TestCaseData[n][1].toString().trim();
		System.out.println("titel Name is:" + Title);
		clickElement(By.xpath(click_title_xpath));
		logger.log(LogStatus.PASS,"Entered Title name is :"+Title);

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[n][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));
		logger.log(LogStatus.PASS,"Entered DOB is :"+DOB);

		Thread.sleep(3000);
		// proposer_dob

		/*
		 * DOB_Proposer=readExcel("Health Insurance").getRow(1).getCell(4).
		 * getStringCellValue(); System.out.println(DOB_Proposer);
		 * enterText(By.id(Dob_Proposer_id),String.valueOf(DOB_Proposer));
		 * logger.log(LogStatus.PASS, "Loan Amount for PA is " + " - " +
		 * DOB_Proposer);
		 */

		final String address1 = TestCaseData[n][7].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		enterText(By.xpath(addressline1_xpath), address1);
		enterText(By.xpath(addressline2_xpath), TestCaseData[n][8].toString().trim());
		enterText(By.xpath(pincode_xpath), TestCaseData[1][9]);
		logger.log(LogStatus.PASS,"Entered Address name is :"+address1);
		logger.log(LogStatus.PASS,"Entered Address name is :"+TestCaseData[n][8].toString().trim());
		logger.log(LogStatus.PASS,"Entered nominee name is :"+TestCaseData[n][9]);

		// Height selection
		String Height = TestCaseData[n][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[n][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[n][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);
		logger.log(LogStatus.PASS,"Entered Height,Inch and Weight is :"+Height     +Inch   +Weight);

		String NomineeName = TestCaseData[n][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		logger.log(LogStatus.PASS,"Entered nominee name is :"+NomineeName);

		// Nominee Relation
		String Nrelation = TestCaseData[n][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
		logger.log(LogStatus.PASS,"Entered nominee relation is :"+Nrelation);

		String pancard=TestCaseData[n][19].toString().trim();

		String pospancard="KJHYS8977E";
		System.out.println("pancard number is :"+pospancard); try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).
			sendKeys(pospancard); }catch(Exception e) {
				System.out.println("Pan card field not visibled"); }
		logger.log(LogStatus.PASS,"Entered Pancard number  is :"+pospancard);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));

		clickElement(By.xpath(submit_xpath));

	
//Selecting insured details here
		

		/*String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembersPOSSuperSaver");*/
		int mcountindexsuper = 0;
		int mcountsuper;
		String Membersdetails = TestCaseData[n][16].toString().trim();
		
		mcountindexsuper = mcountindexsuper + 1;
	
		if (Membersdetails.contains(",")) {

			// data taking form test case sheet which is
			// 7,4,1,8,2,5
			BaseClass.membres = Membersdetails.split(",");
		} else {
			BaseClass.membres = Membersdetails.split(" ");
		}

		System.out.println("BaseClass.membres.length " + BaseClass.membres.length);
		member: 
			for (int i = 0; i <= BaseClass.membres.length-1; i++) {
				//for (int i = 0; i <= BaseClass.membres.length-2; i++) { for number of members=1

				mcountsuper = Integer.parseInt(BaseClass.membres[i].toString());
				if (i == 0) {
					clickElement(By.xpath(title1_xpath));
					// Select Self Primary
					BaseClass.selecttext("ValidRelation0", FamilyData[mcountsuper][1].toString().trim());
				} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcountsuper][5].toString().trim();
					// Date=Date.replaceAll("-", "/");
					// Relation
					BaseClass.selecttext("ValidRelation" + i, FamilyData[mcountsuper][1].toString().trim());
					// title
					BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcountsuper][2].toString().trim());
					enterText(By.name("RelFName" + i), FamilyData[mcountsuper][3].toString().trim());
					enterText(By.name("RelLName" + i), FamilyData[mcountsuper][4].toString().trim());
					clickElement(By.name("rel_dob" + i));
					enterText(By.name("rel_dob" + i), String.valueOf(Date));

					BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcountsuper][6].toString().trim());
					BaseClass.selecttext("relHeightInches" + i, FamilyData[mcountsuper][7].toString().trim());
					enterText(By.name("relWeight" + i), FamilyData[mcountsuper][8].toString().trim());
					//Thread.sleep(5000);
				}
			}
		clickElement(By.xpath("//*[@id='msform']/div[2]/fieldset[2]/input[2]"));
	
		
		//POS super saver question set starts here
		

		//String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String preExistingdeases = TestCaseData[n][20].toString().trim();
		Thread.sleep(5000);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			if(preExistingdeases.contains("YES")) {

				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				//Thread.sleep(2000);
				String years=null;
				String Details=null;		
				for(int qlist=1;qlist<=13;qlist++) {
					Details =QuestionSetData[1][qlist+(qlist-1)].toString().trim();
					years=QuestionSetData[1][qlist+qlist].toString().trim();
					if(Details.equals("")) {
						//break;
					}else 
					{
						int detailsnumber = Integer.parseInt(Details);

						//Will click on check box and select the month & year u
						detailsnumber=detailsnumber+1;
						System.out.println("Details and years are :"+Details+"----"+years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						}catch(Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						}
					}
				}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
			}
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}

		String ChecksData = null;
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) {
			int mch = morechecks + 1;
			ChecksData = TestCaseData[n][20 + morechecks].toString().trim();
			if (ChecksData.equalsIgnoreCase("NO")) {
				System.out.println("Quatation set to NO");
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_no']")).click();
			}  else 
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) {
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();

					}
				} else {
					System.out.println("Check Data is :"  +ChecksData);
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}

		}

		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//*[@id='termsCheckbox3']")).click();
		// driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
		driver.findElement(By.xpath("//*[@id=\"alertCheck\"]")).click();
		JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		// driver.findElement(By.xpath("//*[@id='siQues']/label[2]")).click();
		jse1.executeScript("window.scrollBy(0,250)", "");
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));

		clickElement(By.xpath(proceed_to_pay_xpath));
	
		//Payment for POS Super Saver
		

		clickElement(By.xpath(POS_PAYonline_xpath));	
		Thread.sleep(10000);
		BaseClass.PayuPage_Credentials();
		String message = driver.findElement(By.xpath("//div[@class='col-md-12']/p")).getText();
		System.out.println("Message is :" + message);
			String extitle=driver.getTitle();
			System.out.println("Title is :"+extitle);
			String expectedTitle = "Your payment transaction is successful !";
			Fluentwait(By.xpath(ExpectedMessage_xpath));
		     String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
				try{
					Assert.assertEquals(expectedTitle,actualTitle);
			          logger.log(LogStatus.INFO, actualTitle);
			     }catch(AssertionError e){
			          System.out.println("Payment Failed");
			          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
			          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
			          TestResult = "Fail";
			     }
		
		/*String[][] carddetails = BaseClass.excel_Files("carddetails");
		System.out.println("card number is: " + carddetails[1][0].toString().trim());


		//clickElement(By.xpath("//*[@id='drop_image_1']"));
		WebElement text=driver.findElement(By.xpath("//*[@id='manageCardLink']"));
		System.out.println("Text is :"  +text.toString().trim());
		// if(text.equals("Manage this card")){
		if(text.isDisplayed()){  
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

			String message = driver
				.findElement(By.xpath("//*[@class='ng-scope']/div[21]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
		System.out.println("Message is :" + message);
			String policy_Number;
			try {
				WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/p"));
				policy_Number = Policy.getText();
			} catch (Exception e) {

				WebElement ApplicationNumber=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]"));
				String Application_Number=ApplicationNumber.getText();
				System.out.println("Generated Application Number is : "+Application_Number);
				logger.log(LogStatus.PASS, "Generated Application Number is : "+Application_Number);
				
				WebElement Policy=driver.findElement(By.xpath("//p[@class='your_payment_sucess_p ng-binding']"));
				policy_Number = Policy.getText();

			}

			//System.out.println("Policy number is  :"   +policy_Number);

		}else{

			Thread.sleep(5000);
			enterText(By.xpath("//input[@name='ccard_number']"), carddetails[1][0].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			//JavascriptExecutor jse2 = (JavascriptExecutor) driver;
			jse1.executeScript("window.scrollBy(0,250)", "");
			String expmonth = carddetails[1][3].toString().trim();
			String expyear = carddetails[1][4].toString().trim();
			
			  WebElement elm=driver.findElement(By.xpath("//select[@id='cexpiry_date_month']")).click(); 
			  Select sel=new Select(elm); sel.selectByValue(month);
			 

			BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
			BaseClass.selecttext("cexpiry_date_year", expyear.toString());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();
		}
			String message = driver.findElement(By.xpath("//*[@class='ng-scope']/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
		System.out.println("Message is :" + message);
			String extitle=driver.getTitle();
			System.out.println("Title is :"+extitle);
			String expectedTitle = "Your payment transaction is successful !";
			Fluentwait(By.xpath(ExpectedMessage_xpath));
		     String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
				try{
					Assert.assertEquals(expectedTitle,actualTitle);
			          logger.log(LogStatus.INFO, actualTitle);
			     }catch(AssertionError e){
			          System.out.println("Payment Failed");
			          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
			          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
			          TestResult = "Fail";
			     }

		*/
		
		scrolldown();
		String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/p")).getText();
		System.out.println(ProposerName);
		 logger.log(LogStatus.PASS, "Proposer name is " + ProposerName);
		String Thankyoupagepremium_value = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
		System.out.println("Total premium value is:" + Thankyoupagepremium_value);
		 logger.log(LogStatus.PASS, " Total premium value is:" + Thankyoupagepremium_value);
		WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
		String policy_Number=Policy.getText();
		
		System.out.println("Policy number is  :"   +policy_Number);
		Thread.sleep(1000);
		 logger.log(LogStatus.PASS, "Policy number is  :"   +policy_Number);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
			
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			WriteExcel.setCellData1("Test_Cases_SuperSaver", TestResult, ThankyoupageProposal_Pol_num, n, 2, 3);
			Thread.sleep(4000);
			 driver.quit();

		}catch (Exception e) {
			WriteExcel.setCellData("TestCasePOSSuperSaverData", "Fail", 1, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed");
			Thread.sleep(4000);
			 driver.quit();

		
		}
		}
	
	}
	
}
