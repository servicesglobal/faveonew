package com.org.faveo.healthinsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareSmartSelectPolicy_ShareProposal extends BaseClass implements AccountnSettingsInterface {

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
public static Integer n;
	
	@Test(enabled=true, priority=1)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData = BaseClass.excel_Files("SmartSelect_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("SmartSelect_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("SmartSelect_QuestionSet");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for ( n = 1; n <=3; n++) 
		{
			
			try {
				
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Smart Select - " + TestCaseName);
				System.out.println("Care Smart Select - " + TestCaseName);
                
				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				// Clicking on CAREHNI Product dropdown
				Thread.sleep(5000);
				HealthInsuranceDropDown.CareSmartSelectPolicy();
				
				// Reading Proposer Name from Excel
				String Name = (TestCaseData[n][2].toString().trim());
				System.out.println("Proposer Name is : " + Name);
				Fluentwait(By.xpath(Textbox_Name_xpath));
				try{
				enterText(By.xpath(Textbox_Name_xpath), Name);
				}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Unable to click on Name Textbox.");
				}
				

				System.out.println("Value of N :"+n);
				
				// Reading Emailfrom Excel Sheet
				String Email = TestCaseData[n][3].toString().trim();
				System.out.println("Email of Proposer is :" + Email);
				Fluentwait(By.xpath(Textbox_Email_xpath));
				enterText(By.xpath(Textbox_Email_xpath), Email);

				// Reading Mobile Number from Excel
				Thread.sleep(2000);
				String MobileNumber = TestCaseData[n][4].toString().trim();
				int size = MobileNumber.length();
				System.out.println("Mobile number is: " + MobileNumber);
				if (isValid(MobileNumber)) {
					System.out.println("Is a valid number");
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
				} else {
					System.out.println("Not a valid Number");
				}

				
				// Enter The Value of Total members present in policy
				Thread.sleep(10000);
				List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try{
				for (WebElement DropDownName : dropdown) {
					DropDownName.click();
					if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["+ TestCaseData[n][5].toString().trim() + "]")).click();
						System.out.println("Total Number of Member Selected : " + TestCaseData[n][5].toString().trim());
						//Thread.sleep(10000);
						break;
					}
				}}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}
				
				
				
				// again call the dropdown
				Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				// Thread.sleep(3000);
				int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;
				System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3

				outer:

				for (WebElement DropDownName : dropdown) {
					// System.out.println("DropDownName is " +
					// DropDownName.getText());
					if (membersSize == 1) {

						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][6];
						System.out.println("Total Number of Member in Excel :" + Membersdetails);
						if (Membersdetails.contains("")) {

							// data taking form test case sheet which is
							// 7,4,1,8,2,5
							BaseClass.membres = Membersdetails.split("");

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
								// System.out.println("Mdeatils is :
								// "+membres);

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;
								System.out.println("Mcount Index : " + mcountindex);

			driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
								System.out.println("List Data is : " + List);

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										String SelectedAge=FamilyData[mcount][0].toString().trim();
										System.out.println("Selected Age is :"+SelectedAge);
										System.out.println("List Data is :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[n][8].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["+ covertype + "]")).click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) 
						{
							List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) 
							{
								if (DropDowns.getText().contains("Floater")) 
								{
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[n][5].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
						int Children = Integer.parseInt(TestCaseData[n][7].toString().trim());
						clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
						clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
						System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[n][6];
									if (Membersdetails.contains(",")) {

										// data taking form test case sheet
										// which is
										// 7,4,1,8,2,5
										BaseClass.membres = Membersdetails.split(",");
									} else {
										//BaseClass.membres = Membersdetails.split(" ");
										System.out.println("Hello");
									}

									member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {
										// System.out.println("Mdeatils is :
										// "+membres);

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										Thread.sleep(5000);
										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {
											

											if (ListData.getText()
													.contains(FamilyData[mcount][0].toString().trim())) {
												
												System.out.println("List Data is :" + ListData.getText());
												Thread.sleep(5000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}
									
								}
							}
						}
					} 
					else{

						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")) {
				
								String Membersdetails = TestCaseData[n][6];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else 
								{
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
								
									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown
									Thread.sleep(5000);
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {
										

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(5000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}
								
							}
						}
					}
				}

				Thread.sleep(5000);
				BaseClass.scrolldown();
				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][9].toString().trim());
				System.out.println(SumInsured);
				clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				
 
				// Reading the value of Tenure from Excel
				BaseClass.scrolldown();
				int Tenure = Integer.parseInt(TestCaseData[n][10].toString().trim());
				System.out.println(Tenure);
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					//clickElement(By.xpath(Radio_Tenure1_xpath));
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					//clickElement(By.xpath(Radio_Tenure2_xpath));
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					//clickElement(By.xpath(Radio_Tenure3_xpath));
				}

				// Scroll Window Up
				BaseClass.scrollup();
				BaseClass.scrollup();
				Thread.sleep(5000);

				// NCB Super Addon Selection
				String NCBSuper = TestCaseData[n][11].toString().trim();
				//waitForElement(By.xpath(Checkbox_CareWithNCB_xpath));
				if (NCBSuper.contains("for care with NCB Super")) {
					//clickbyHover(By.xpath(Checkbox_CareWithNCB_xpath));
					WebElement e1=driver.findElement(By.xpath("//div[contains(@class,'input_year your_premium_cont')]//div/div[2]/input//following-sibling::label[1]"));
					clickByJS(e1);
					System.out.println("Selected Addon is : " + NCBSuper);
				} else if (NCBSuper.contains("for care")) {
					//clickbyHover(By.xpath(Checkbox_Care_xpath));
					WebElement e1=driver.findElement(By.xpath("//div[contains(@class,'input_year your_premium_cont')]//div/div[1]/input//following-sibling::label[1]/img"));
					clickByJS(e1);
				}
				
				// logger.log(LogStatus.PASS, "Selection of NCB Super " + "
				// - " + NCBSuper);

				// UAR Addon Selection
				Thread.sleep(5000);
				String UAR = TestCaseData[n][28].toString().trim();
				System.out.println(UAR);
				Thread.sleep(3000);
				if (UAR.contains("Unlimited Recharge")) {
					clickbyHover(By.xpath(Checkbox_UAR_xpath));
					//clickElement(By.xpath(Checkbox_UAR_xpath));
				} else if (UAR.contains("No") || UAR.contains("NO")) {
					System.out.println("UAR Addon is not Selected");
				}

				// logger.log(LogStatus.PASS, "Selection of UAR Addon " + "
				// - " + UAR);

				// Everyday Care Addon Selection
				String EverydayCare = TestCaseData[n][29].toString().trim();
				System.out.println(EverydayCare);
				Thread.sleep(3000);
				if (EverydayCare.contains("Everyday Care")) {
					clickbyHover(By.xpath(Checkbox_EverydayCare_xpath));
					//clickElement(By.xpath(Checkbox_EverydayCare_xpath));
				} else if (EverydayCare.contains("No") || EverydayCare.contains("NO")) {
					System.out.println("EverydayCare Addon is not Selected");
				}


				// PA Care Addon Selection
				/*Thread.sleep(5000);
				String PA = TestCaseData[n][30].toString().trim();
				Thread.sleep(7000);
				if (PA.contains("Personal Accident")) {
					clickbyHover(By.xpath(Checkbox_PA__xpath));
					//clickElement(By.xpath(Checkbox_PA__xpath));
					System.out.println("User Selected : "+PA);
				} else if (PA.contains("No") || PA.contains("NO")) {
					System.out.println("PA Addon is not Selected");
				}*/
				// Daily Allowance Addon Selection
				/*String DA = TestCaseData[n][30].toString().trim();
				System.out.println(EverydayCare);
				Thread.sleep(3000);
				if (DA.contains("Daily Allowance")) {
					clickbyHover(By.xpath("//b[contains(text(),'Daily Allowance')]"));
					//clickElement(By.xpath(Checkbox_EverydayCare_xpath));
				} else if (DA.contains("No")) {
					System.out.println("Daily Allowance Addon is not Selected");
				}*/
				
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				Thread.sleep(5000);
				waitForElement(By.name("ValidTitle"));
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				//clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);
                BaseClass.selecttext("ValidTitle", Title.toString());
				
                Thread.sleep(3000);
				// Entering DOB from Excel into dob field
				String DOB = TestCaseData[n][12].toString().trim();
				System.out.println("date is:" + DOB);
				clickElement(By.id(Dob_Proposer_id));
				enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));

				// Reading AddressLine 1 from Excel
				Thread.sleep(3000);
				String address1 = TestCaseData[n][12].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				enterText(By.xpath(Textbox_AddressLine1_xpath), address1);

				// Reading AddressLine 2 from Excel
				String address2 = TestCaseData[n][14].toString().trim();
				System.out.println(address2);
				enterText(By.xpath(Textbox_AddressLine2_xpath), address2);
               
				Thread.sleep(2000);
				// Reading Pincode from Excel
				int Pincode = Integer.parseInt(TestCaseData[n][15].toString().trim());
				System.out.println(Pincode);
				enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
				// logger.log(LogStatus.PASS, "Entered Pincode is " + " - "
				// + Pincode);

				Thread.sleep(3000);
				// Reading Proposer Height in Feet from Excel
				String HeightProposer_Feet = TestCaseData[n][16].toString().trim();
				System.out.println("Height value from excel  is:" + HeightProposer_Feet);
				clickElement(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text(),"
						+ HeightProposer_Feet + ")]"));

				// Reading Proposer Height in Inch from Excel
				String HeightProposer_Inch = TestCaseData[n][17].toString().trim();
				System.out.println("Inch value from excel  is:" + HeightProposer_Inch);
				clickElement(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text(),"
						+ HeightProposer_Inch + ")]"));

				// Reading Weight of Proposer from Excel
				String Weight = TestCaseData[n][18].toString().trim();
				System.out.println("Weight is :" + Weight);
				enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));

				Thread.sleep(2000);
				// Reading Nominee Name from Excel
				String NomineeName = TestCaseData[n][19].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);

				// Nominee Relation
				waitForElement(By.xpath(Dropdown_Nomineerelation_xpath));
				String NomineeRelation = TestCaseData[n][20].toString().trim();
				System.out.println("Nominee  relation from excel  is:" + NomineeRelation);
				clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"
						+ NomineeRelation + "'" + ")]"));

				// Pan Card
				String PanCard = TestCaseData[n][21].toString().trim();
				System.out.println("pancard number is :" + PanCard);
				Boolean PanCardNumberPresence = driver
						.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"))
						.size() > 0;
				if (PanCardNumberPresence == true) {
					enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

				} else {
					System.out.println("PAN Card Field is not Present");
				}

				// Click on Next button
				try{
					clickElement(By.id(Button_NextProposer_id));
					System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
						System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
					}


/* ===============================================Insured Details Page===================================================== */
				
				/*Thread.sleep(3000);
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());

				Thread.sleep(5000);	
					String Date = FamilyData[mcount][5].toString().trim();*/
				Thread.sleep(5000);
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
					
					// Relation
					BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
					// title
					BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
					enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
					enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
					clickElement(By.name("rel_dob" + i));
					enterText(By.name("rel_dob" + i), String.valueOf(Date));

					BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
					BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
					enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
				}
				}

				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
				
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForCareSmartSelect(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
				
				
				//******** Health Questionnaires Elements **********************
				String preExistingdeases = TestCaseData[n][22].toString().trim();
				Thread.sleep(5000);
				System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				BaseClass.scrollup();
				try{
					 if(preExistingdeases.contains("YES")) {
						
						 waitForElements(By.xpath(YesButton_xpath));
						 clickElement(By.xpath(YesButton_xpath));
						 //Thread.sleep(2000);
						 String years=null;
						 String Details=null;		
						 for(int qlist=1;qlist<=13;qlist++) {
							 Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
							 years=QuestionSetData[n][qlist+qlist].toString().trim();
							 if(Details.equals("")) {
					 //break;
							 }else 
							 {
								 int detailsnumber = Integer.parseInt(Details);

					 //Will click on check box and select the month & year u
					 detailsnumber=detailsnumber+1;
					 System.out.println("Details and years are :"+Details+"----"+years);
					 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
					 Thread.sleep(1000);
					 try {
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
						 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
					 }catch(Exception e) {
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
						 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
					 }
				 }
			}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
				}
				}
				catch(Exception e)
						{
							logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
						}

				
				//BaseClass.scrolldown();
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 3; morechecks++) 
				{
					int mch = morechecks + 1;
					String ChecksData = TestCaseData[n][22 + morechecks].toString().trim();
					System.out.println("Check data is : "+ChecksData);
					try{
					if (ChecksData.contains("NO")) 
					{
						System.out.println("Quatation set to NO");
						System.out.println("//label[@for='question_"+mch+"_no']");
						clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
					} 
					else 
					{
						driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
						if (ChecksData.contains(",")) 
						{
							ChckData = ChecksData.split(",");
							for (String Chdata : ChckData) 
							{
								datacheck = Integer.parseInt(Chdata);
								datacheck = datacheck - 1;
								driver.findElement(
								By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
							}
						} else if (ChecksData.contains(""))
						{
							datacheck = Integer.parseInt(ChecksData);
							datacheck = datacheck - 1;
							driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
						}
					}
					}
					catch(Exception e){
						logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
					}

				}

				//Check Box on Health Questionnaire
				BaseClass.scrolldown();
				BaseClass.HelathQuestionnairecheckbox();

				try{
					clickElement(By.xpath(proceed_to_pay_xpath));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
						System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
					}
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				Thread.sleep(7000);
				BaseClass.PayuPage_Credentials();
				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareSmartSelect(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
		       driver.quit();

				
				} catch (Exception e) {
					//WriteExcel.setCellData("Test_Cases_SmartSelect", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Fail");
					driver.quit();
					
				}
				 continue; 
			
		}
	}
	
	//********** Share Proposal Test Case 2 *************************************************************
	
	@Test(enabled=false, priority=2)
	public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData = BaseClass.excel_Files("SmartSelect_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("SmartSelect_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("SmartSelect_QuestionSet");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for ( n = 4; n <=6; n++) 
		{
			
			try {
				
				String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
				logger = extent.startTest("Care Smart Select - " + TestCaseName);
				System.out.println("Care Smart Select - " + TestCaseName);
                
				//Step 1 - Open Email and Delete Old Email
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a proposal
				System.out.println("Step 2 - Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
				
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				// Clicking on CAREHNI Product dropdown
				Thread.sleep(5000);
				HealthInsuranceDropDown.CareSmartSelectPolicy();
				
				// Reading Proposer Name from Excel
				String Name = (TestCaseData[n][2].toString().trim());
				System.out.println("Proposer Name is : " + Name);
				Fluentwait(By.xpath(Textbox_Name_xpath));
				try{
				enterText(By.xpath(Textbox_Name_xpath), Name);
				}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Unable to click on Name Textbox.");
				}
				

				System.out.println("Value of N :"+n);
				
				// Reading Emailfrom Excel Sheet
				String Email = TestCaseData[n][3].toString().trim();
				System.out.println("Email of Proposer is :" + Email);
				Fluentwait(By.xpath(Textbox_Email_xpath));
				enterText(By.xpath(Textbox_Email_xpath), Email);

				// Reading Mobile Number from Excel
				Thread.sleep(2000);
				String MobileNumber = TestCaseData[n][4].toString().trim();
				int size = MobileNumber.length();
				System.out.println("Mobile number is: " + MobileNumber);
				if (isValid(MobileNumber)) {
					System.out.println("Is a valid number");
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
				} else {
					System.out.println("Not a valid Number");
				}

				
				// Enter The Value of Total members present in policy
				Thread.sleep(10000);
				List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try{
				for (WebElement DropDownName : dropdown) {
					DropDownName.click();
					if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["+ TestCaseData[n][5].toString().trim() + "]")).click();
						System.out.println("Total Number of Member Selected : " + TestCaseData[n][5].toString().trim());
						//Thread.sleep(10000);
						break;
					}
				}}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}
				
				
				
				// again call the dropdown
				Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				// Thread.sleep(3000);
				int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;
				System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3

				outer:

				for (WebElement DropDownName : dropdown) {
					// System.out.println("DropDownName is " +
					// DropDownName.getText());
					if (membersSize == 1) {

						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][6];
						System.out.println("Total Number of Member in Excel :" + Membersdetails);
						if (Membersdetails.contains("")) {

							// data taking form test case sheet which is
							// 7,4,1,8,2,5
							BaseClass.membres = Membersdetails.split("");

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
								// System.out.println("Mdeatils is :
								// "+membres);

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;
								System.out.println("Mcount Index : " + mcountindex);

			driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
								System.out.println("List Data is : " + List);

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										String SelectedAge=FamilyData[mcount][0].toString().trim();
										System.out.println("Selected Age is :"+SelectedAge);
										System.out.println("List Data is :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[n][8].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["+ covertype + "]")).click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) 
						{
							List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) 
							{
								if (DropDowns.getText().contains("Floater")) 
								{
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[n][5].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
						int Children = Integer.parseInt(TestCaseData[n][7].toString().trim());
						clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
						clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
						System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[n][6];
									if (Membersdetails.contains(",")) {

										// data taking form test case sheet
										// which is
										// 7,4,1,8,2,5
										BaseClass.membres = Membersdetails.split(",");
									} else {
										//BaseClass.membres = Membersdetails.split(" ");
										System.out.println("Hello");
									}

									member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {
										// System.out.println("Mdeatils is :
										// "+membres);

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										Thread.sleep(5000);
										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {
											

											if (ListData.getText()
													.contains(FamilyData[mcount][0].toString().trim())) {
												
												System.out.println("List Data is :" + ListData.getText());
												Thread.sleep(5000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}
									
								}
							}
						}
					} 
					else{

						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")) {
				
								String Membersdetails = TestCaseData[n][6];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else 
								{
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
								
									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown
									Thread.sleep(5000);
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {
										

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(5000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}
								
							}
						}
					}
				}

				Thread.sleep(5000);
				BaseClass.scrolldown();
				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][9].toString().trim());
				System.out.println(SumInsured);
				clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				
 
				// Reading the value of Tenure from Excel
				BaseClass.scrolldown();
				int Tenure = Integer.parseInt(TestCaseData[n][10].toString().trim());
				System.out.println(Tenure);
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					//clickElement(By.xpath(Radio_Tenure1_xpath));
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					//clickElement(By.xpath(Radio_Tenure2_xpath));
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					//clickElement(By.xpath(Radio_Tenure3_xpath));
				}

				// Scroll Window Up
				BaseClass.scrollup();
				BaseClass.scrollup();
				Thread.sleep(5000);

				// NCB Super Addon Selection
				String NCBSuper = TestCaseData[n][11].toString().trim();
				//waitForElement(By.xpath(Checkbox_CareWithNCB_xpath));
				if (NCBSuper.contains("for care with NCB Super")) {
					//clickbyHover(By.xpath(Checkbox_CareWithNCB_xpath));
					WebElement e1=driver.findElement(By.xpath("//div[contains(@class,'input_year your_premium_cont')]//div/div[2]/input//following-sibling::label[1]"));
					clickByJS(e1);
					System.out.println("Selected Addon is : " + NCBSuper);
				} else if (NCBSuper.contains("for care")) {
					//clickbyHover(By.xpath(Checkbox_Care_xpath));
					WebElement e1=driver.findElement(By.xpath("//div[contains(@class,'input_year your_premium_cont')]//div/div[1]/input//following-sibling::label[1]/img"));
					clickByJS(e1);
				}
				
				// logger.log(LogStatus.PASS, "Selection of NCB Super " + "
				// - " + NCBSuper);

				// UAR Addon Selection
				Thread.sleep(5000);
				String UAR = TestCaseData[n][28].toString().trim();
				System.out.println(UAR);
				Thread.sleep(3000);
				if (UAR.contains("Unlimited Recharge")) {
					clickbyHover(By.xpath(Checkbox_UAR_xpath));
					//clickElement(By.xpath(Checkbox_UAR_xpath));
				} else if (UAR.contains("No") || UAR.contains("NO")) {
					System.out.println("UAR Addon is not Selected");
				}

				// Everyday Care Addon Selection
				String EverydayCare = TestCaseData[n][29].toString().trim();
				System.out.println(EverydayCare);
				Thread.sleep(3000);
				if (EverydayCare.contains("Everyday Care")) {
					clickbyHover(By.xpath(Checkbox_EverydayCare_xpath));
					//clickElement(By.xpath(Checkbox_EverydayCare_xpath));
				} else if (EverydayCare.contains("No") || EverydayCare.contains("NO")) {
					System.out.println("EverydayCare Addon is not Selected");
				}
               
				AddonsforProducts.readPremiumFromFirstPage();
                
				//Read Age Group Based on Cover Type
				String CoverType = TestCaseData[n][8].toString().trim();
				if(CoverType.equalsIgnoreCase("Individual")){
                ReadDataFromEmail.readAgeGroup(); 
				}
				else{
					System.out.println("Age Group Value not Read");
				}
				
				//ReadDataFromEmail.readAgeGroup(); 
				
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				Thread.sleep(5000);
				waitForElement(By.name("ValidTitle"));
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				//clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);
                BaseClass.selecttext("ValidTitle", Title.toString());
				
                Thread.sleep(3000);
				// Entering DOB from Excel into dob field
				String DOB = TestCaseData[n][12].toString().trim();
				System.out.println("date is:" + DOB);
				clickElement(By.id(Dob_Proposer_id));
				enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));

				// Reading AddressLine 1 from Excel
				Thread.sleep(3000);
				String address1 = TestCaseData[n][12].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				enterText(By.xpath(Textbox_AddressLine1_xpath), address1);

				// Reading AddressLine 2 from Excel
				String address2 = TestCaseData[n][14].toString().trim();
				System.out.println(address2);
				enterText(By.xpath(Textbox_AddressLine2_xpath), address2);
               
				Thread.sleep(2000);
				// Reading Pincode from Excel
				int Pincode = Integer.parseInt(TestCaseData[n][15].toString().trim());
				System.out.println(Pincode);
				enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
				// logger.log(LogStatus.PASS, "Entered Pincode is " + " - "
				// + Pincode);

				Thread.sleep(3000);
				// Reading Proposer Height in Feet from Excel
				String HeightProposer_Feet = TestCaseData[n][16].toString().trim();
				System.out.println("Height value from excel  is:" + HeightProposer_Feet);
				clickElement(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text(),"
						+ HeightProposer_Feet + ")]"));

				// Reading Proposer Height in Inch from Excel
				String HeightProposer_Inch = TestCaseData[n][17].toString().trim();
				System.out.println("Inch value from excel  is:" + HeightProposer_Inch);
				clickElement(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text(),"
						+ HeightProposer_Inch + ")]"));

				// Reading Weight of Proposer from Excel
				String Weight = TestCaseData[n][18].toString().trim();
				System.out.println("Weight is :" + Weight);
				enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));

				Thread.sleep(2000);
				// Reading Nominee Name from Excel
				String NomineeName = TestCaseData[n][19].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);

				// Nominee Relation
				waitForElement(By.xpath(Dropdown_Nomineerelation_xpath));
				String NomineeRelation = TestCaseData[n][20].toString().trim();
				System.out.println("Nominee  relation from excel  is:" + NomineeRelation);
				clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"
						+ NomineeRelation + "'" + ")]"));

				// Pan Card
				String PanCard = TestCaseData[n][21].toString().trim();
				System.out.println("pancard number is :" + PanCard);
				Boolean PanCardNumberPresence = driver
						.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"))
						.size() > 0;
				if (PanCardNumberPresence == true) {
					enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

				} else {
					System.out.println("PAN Card Field is not Present");
				}

				// Click on Next button
				try{
					clickElement(By.id(Button_NextProposer_id));
					System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
						System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
					}


/* ===============================================Insured Details Page===================================================== */
				
				Thread.sleep(5000);
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
					
					// Relation
					BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
					// title
					BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
					enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
					enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
					clickElement(By.name("rel_dob" + i));
					enterText(By.name("rel_dob" + i), String.valueOf(Date));

					BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
					BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
					enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
				}
				}

				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
				
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				driver.quit();
				
				//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
				System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				
			    LaunchBrowser();
			    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForCareSmartSelect(n);
			    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Step 4 - Click on Buy Now button from Email Body and punch the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
				
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
				DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
				
				
				
				//******** Health Questionnaires Elements **********************
				String preExistingdeases = TestCaseData[n][22].toString().trim();
				Thread.sleep(5000);
				System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				BaseClass.scrollup();
				try{
					 if(preExistingdeases.contains("YES")) {
						
						 waitForElements(By.xpath(YesButton_xpath));
						 clickElement(By.xpath(YesButton_xpath));
						 //Thread.sleep(2000);
						 String years=null;
						 String Details=null;		
						 for(int qlist=1;qlist<=13;qlist++) {
							 Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
							 years=QuestionSetData[n][qlist+qlist].toString().trim();
							 if(Details.equals("")) {
					 //break;
							 }else 
							 {
								 int detailsnumber = Integer.parseInt(Details);

					 //Will click on check box and select the month & year u
					 detailsnumber=detailsnumber+1;
					 System.out.println("Details and years are :"+Details+"----"+years);
					 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
					 Thread.sleep(1000);
					 try {
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
						 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
					 }catch(Exception e) {
						 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
						 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
					 }
				 }
			}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
				}
				}
				catch(Exception e)
						{
							logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
						}

				
				//BaseClass.scrolldown();
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 3; morechecks++) 
				{
					int mch = morechecks + 1;
					String ChecksData = TestCaseData[n][22 + morechecks].toString().trim();
					System.out.println("Check data is : "+ChecksData);
					try{
					if (ChecksData.contains("NO")) 
					{
						System.out.println("Quatation set to NO");
						System.out.println("//label[@for='question_"+mch+"_no']");
						clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
					} 
					else 
					{
						driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
						if (ChecksData.contains(",")) 
						{
							ChckData = ChecksData.split(",");
							for (String Chdata : ChckData) 
							{
								datacheck = Integer.parseInt(Chdata);
								datacheck = datacheck - 1;
								driver.findElement(
								By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
							}
						} else if (ChecksData.contains(""))
						{
							datacheck = Integer.parseInt(ChecksData);
							datacheck = datacheck - 1;
							driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
						}
					}
					}
					catch(Exception e){
						logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
					}

				}

				//Check Box on Health Questionnaire
				BaseClass.scrolldown();
				BaseClass.HelathQuestionnairecheckbox();

				try{
					clickElement(By.xpath(proceed_to_pay_xpath));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
						System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
					}
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 40)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				Thread.sleep(7000);
				BaseClass.PayuPage_Credentials();
				driver.quit();
				
				//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareSmartSelect(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
		       driver.quit();
		       
		    // Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				 driver.quit();  

				
				} 
			
			catch (AssertionError e) {
				//WriteExcel.setCellData("Test_Cases_SmartSelect", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Fail");
				driver.quit();
				
			}
			
			
			catch (Exception e) {
					//WriteExcel.setCellData("Test_Cases_SmartSelect", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Fail");
					driver.quit();
					
				}
				 continue; 
			
		}
	}
}
