package com.org.faveo.healthinsurance;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareFreedomPolicy_ShareQuote extends BaseClass implements AccountnSettingsInterface{
	
	public static Logger log = Logger.getLogger(CareFreedomPolicy_ShareQuote.class);


	public static Integer n;
	@Test(priority=1, enabled=true)
	public static void CareFreedom_ShareQuote() throws Exception
	{
		String[][] TestCase=BaseClass.excel_Files("ShareQuotation");
		String[][] TestCaseData=BaseClass.excel_Files("Care_Freedom_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Care_Freedom_Insured_Deatils");
		String[][] QuestionSetData=BaseClass.excel_Files("Care_Freedom_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		for( n=1;n<=1;n++)
	{
			try{

			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("CareFreedom -" + TestCaseName);
			System.out.println("Care Freedom -" +TestCaseName);
			
			/*//Step 1 - Open Email and Delete any Old Email 
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
            LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			*/
			//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
			System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
			logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
			LaunchBrowser();
			LoginCase.LoginwithValidCredendial();
			
			//Clicking on CAREFreedom Product dropdown
			Thread.sleep(5000);
			HealthInsuranceDropDown.CareFreedomPolicy();
		
			// Reading Proposer Name from Excel
		
				String Name = (TestCaseData[n][2].toString().trim() + "  " + TestCaseData[n][3].toString().trim());
				waitForElement(By.xpath(Textbox_Name_xpath));
				enterText(By.xpath(Textbox_Name_xpath), Name);
				System.out.println("Proposer Name is : " + Name);
						
						
				// Reading Emailfrom Excel Sheet
				waitForElement(By.xpath(Textbox_Email_xpath));
				String Email = TestCaseData[n][6];
				System.out.println("Email of Proposer is :" + Email);
				enterText(By.xpath(Textbox_Email_xpath), Email);
						
						
						
				// Reading Mobile Number from Excel
				waitForElement(By.xpath(Textbox_Mobile_xpath));
				String MobileNumber = TestCaseData[n][5].toString().trim();
				int size = MobileNumber.length();
				System.out.println("Mobile number is: " + MobileNumber);
				if (isValid(MobileNumber)) {
					System.out.println("Is a valid number");
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
				} else {
					System.out.println("Not a valid Number");
				}
					
				// Enter The Value of Total members present in policy
				Thread.sleep(10000);
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						DropDownName.click();
						if (DropDownName.getText().equals("1")) {
							driver.findElement(
									By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
											+ TestCaseData[n][15].toString().trim() + "]"))
									.click();
							Thread.sleep(10000);
							break;
						}
					}
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}

				// again call the dropdown
				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][15].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				outer:

				for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][16];
						if (Membersdetails.contains("")) {

							// data taking form test case sheet which is
							// 7,4,1,8,2,5
							BaseClass.membres = Membersdetails.split("");

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
								// System.out.println("Mdeatils is :
								// "+membres);

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
										.click();
								// List Age of members dropdown
								List<WebElement> List = driver
										.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else if (DropDownName.getText().contains("Individual")) {

						if (TestCaseData[n][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By.xpath(
								"//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
								.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[n][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("1")) {
									System.out.println("Total DropDownName Present on Quotation page are : "
											+ DropDowns.getText());
								} else if (DropDowns.getText().equals("90 Days - 5 Yrs")
										|| DropDowns.getText().equals("6 - 17 Yrs")
										|| DropDowns.getText().equals("18 - 35 Yrs")
										|| DropDowns.getText().equals("36 - 40 Yrs")
										|| DropDowns.getText().equals("41 - 45 Yrs")
										|| DropDowns.getText().equals("46 - 50 Yrs")
										|| DropDowns.getText().equals("51 - 55 Yrs")
										|| DropDowns.getText().equals("56 - 60 Yrs")
										|| DropDowns.getText().equals("61 - 65 Yrs")
										|| DropDowns.getText().equals("66 - 70 Yrs")
										|| DropDowns.getText().equals("71 - 75 Yrs")
										|| DropDowns.getText().equals("76 - 80 Yrs")
										|| DropDowns.getText().equals("Above 80 Yrs")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[n][25].toString().trim());
									clickElement(By
											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By
											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[n][16];
									if (Membersdetails.contains(",")) {

										// data taking form test case sheet
										// which is
										// 7,4,1,8,2,5
										BaseClass.membres = Membersdetails.split(",");
									} else {
										// BaseClass.membres =
										// Membersdetails.split(" ");
										System.out.println("Hello");
									}

									member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {
										// System.out.println("Mdeatils is :
										// "+membres);

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										Thread.sleep(5000);
										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(5000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("90 Days - 5 Yrs")
									|| DropDowns.getText().equals("6 - 17 Yrs")
									|| DropDowns.getText().equals("18 - 35 Yrs")
									|| DropDowns.getText().equals("36 - 40 Yrs")
									|| DropDowns.getText().equals("41 - 45 Yrs")
									|| DropDowns.getText().equals("46 - 50 Yrs")
									|| DropDowns.getText().equals("51 - 55 Yrs")
									|| DropDowns.getText().equals("56 - 60 Yrs")
									|| DropDowns.getText().equals("61 - 65 Yrs")
									|| DropDowns.getText().equals("66 - 70 Yrs")
									|| DropDowns.getText().equals("71 - 75 Yrs")
									|| DropDowns.getText().equals("76 - 80 Yrs")
									|| DropDowns.getText().equals("Above 80 Yrs")) {

								String Membersdetails = TestCaseData[n][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown
									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is :" + ListData.getText());
											Thread.sleep(2000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}

							}
						}
					}

				}

				Thread.sleep(5000);
				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][18].toString().trim());
				clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
				// " + SumInsured + ")]"));
				System.out.println("Entered Sum Insured is : " + SumInsured);

				// Reading the value of Tenure from Excel
				int Tenure = Integer.parseInt(TestCaseData[n][19].toString().trim());
				System.out.println(Tenure);
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					// clickElement(By.xpath(Radio_Tenure1_xpath));
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					// clickElement(By.xpath(Radio_Tenure2_xpath));
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					// clickElement(By.xpath(Radio_Tenure3_xpath));
				}

				// Reading the value of Addons from Excel
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,-250)", "");
				Thread.sleep(7000);
				String Addons = TestCaseData[n][20].toString().trim();
				System.out.println("Selected Addon is : " + Addons);
				if (Addons.contains("For Care Freedom")) {
					clickbyHover(By.xpath(ForCareFreedom_Addon_xpath));
					// clickElement(By.xpath(ForCareFreedom_Addon_xpath));
					/*
					 * logger.log(LogStatus.PASS, "User has Selected : " +
					 * Addons + "Addon");
					 */
				} else if (Addons.contains("With Health Check Plus")) {
					clickbyHover(By.xpath(WithHealthCheckupPlus_Addon_xpath));
					// clickElement(By.xpath(WithHealthCheckupPlus_Addon_xpath));
					/*
					 * logger.log(LogStatus.PASS, "User has Selected : " +
					 * Addons + "Addon");
					 */
				} else if (Addons.contains("With Home Care")) {
					// clickElement(By.xpath(WithHomeCare_Addon_xpath));
					clickbyHover(By.xpath(WithHomeCare_Addon_xpath));
					/*
					 * logger.log(LogStatus.PASS, "User has Selected : " +
					 * Addons + "Addon");
					 */
				} else if (Addons.contains("With Home Care, Health Check Plus")) {
					clickbyHover(By.xpath(WithHomeCareandHelathCheckplus_xpath));
					// clickElement(By.xpath(WithHomeCareandHelathCheckplus_xpath));
					/*
					 * logger.log(LogStatus.PASS, "User has Selected : " +
					 * Addons + "Addon");
					 */
				}
				AddonsforProducts.readPremiumFromFirstPage();
                ReadDataFromEmail.readAgeGroup(); 
      
                DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForCareFreedom(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		        driver.quit();
		        
		     // Step 3 - Open Email and Verify Data in Email Body
				System.out.println("Step 3 -  Open Email and Verify Data in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForCareFreedom(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
				System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				
				//Reading Proposer Title from Excel
				/*waitForElement(By.xpath(click_title_xpath));
				String Title=TestCaseData[n][1].toString().trim();
				System.out.println("Title is:" +Title);
				clickElement(By.xpath(click_title_xpath));
				clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"+ Title + "'"+")]"));*/
				
				waitForElement(By.name("ValidTitle"));
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				//clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);
                BaseClass.selecttext("ValidTitle", Title.toString());
				
                Thread.sleep(5000);
               
				//Entering DOB from Excel into dob field
				String DOB=TestCaseData[n][4].toString().trim();
				System.out.println("date is:" +DOB);
				clickElement(By.id(Dob_Proposer_id));
				enterText(By.id(Dob_Proposer_id),String.valueOf(DOB));
               
               

				
				//Reading AddressLine 1 from Excel
				Thread.sleep(3000);
				String address1=TestCaseData[n][7].toString().trim();
				System.out.println("Adress1 name is :"+address1);
				enterText(By.xpath(Textbox_AddressLine1_xpath), address1);
				
				
				//Reading AddressLine 2 from Excel
				String address2=TestCaseData[n][8].toString().trim();
				System.out.println(address2);
				enterText(By.xpath(Textbox_AddressLine2_xpath), address2);
				
				
				//Reading Pincode from Excel
				int Pincode=Integer.parseInt(TestCaseData[n][9].toString().trim());
				System.out.println(Pincode);
				enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
				//logger.log(LogStatus.PASS, "Entered Pincode is " + " - " + Pincode);
				
				
				//Reading Proposer Height in Feet from Excel
				String HeightProposer_Feet=TestCaseData[n][10].toString().trim();
				System.out.println("Height value from excel  is:" +HeightProposer_Feet);
				clickElement(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text()," + HeightProposer_Feet + ")]"));
				
				//Reading Proposer Height in Inch from Excel
				String HeightProposer_Inch=TestCaseData[n][11].toString().trim();
				System.out.println("Inch value from excel  is:" +HeightProposer_Inch);
				clickElement(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text()," + HeightProposer_Inch + ")]"));
				
				
				//Reading Weight of Proposer from Excel
				String Weight=TestCaseData[n][12].toString().trim();
				System.out.println("Weight is :"+Weight);
				enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));

				Thread.sleep(2000);
				//Reading Nominee Name from Excel
				String NomineeName=TestCaseData[n][13].toString().trim();
				System.out.println("Nominee name   is:" +NomineeName);
				enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);
				
				Thread.sleep(2000);
				//Nominee Relation
				String NomineeRelation =TestCaseData[n][14].toString().trim();
				System.out.println("Nominee  relation from excel  is:" +NomineeRelation);
				clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text(),"+"'" + NomineeRelation +"'"+ ")]"));

				//String pancard=TestCaseData[n][19].toString().trim();
				String PanCard =TestCaseData[n][21].toString().trim();
				System.out.println("pancard number is :"+PanCard);
				Boolean PanCardNumberPresence = driver.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input")).size() > 0;
				if(PanCardNumberPresence==true)
				{
					enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"),PanCard);
					
				}
				else{
					System.out.println("PAN Card Field is not Present");
				}
				

				//Click on Next button
				try{
					Thread.sleep(5000);
					waitTillElementToBeClickable(By.id(Button_NextProposer_id));
					clickElement(By.id(Button_NextProposer_id));
					System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
						System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
					}

				
		/*===============================================Insured Details Page========================================================*/


			/*	for(int i=0;i<=BaseClass.membres.length-1;i++) {
					mcount= Integer.parseInt(BaseClass.membres[i].toString());
					
						String Date= FamilyData[mcount][5].toString().trim();*/
				
					Thread.sleep(5000);
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
						BaseClass.selecttext("ValidRelation"+i,FamilyData[mcount][1].toString().trim());
						
						BaseClass.selecttext("ValidRelTitle"+i,FamilyData[mcount][2].toString().trim());
						enterText(By.name("RelFName"+i), FamilyData[mcount][3].toString().trim());
						
						enterText(By.name("RelLName"+i), FamilyData[mcount][4].toString().trim());
						clickElement(By.name("rel_dob"+i));
						
						enterText(By.name("rel_dob"+i), String.valueOf(Date));
						BaseClass.selecttext("relHeightFeet"+i,FamilyData[mcount][6].toString().trim());

						BaseClass.selecttext("relHeightInches"+i,FamilyData[mcount][7].toString().trim());
						enterText(By.name("relWeight"+i), FamilyData[mcount][8].toString().trim());
					}
				}
				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
					
				

		/*===============================================HEALTH QUESTIONNAIRE Page========================================================*/
				
				String preExistingdeases=TestCaseData[n][22].toString().trim();
				Thread.sleep(3000);
				System.out.println("Has any Proposed to be Insured within the past 48 months :" +preExistingdeases);
				BaseClass.scrollup();
					 try{
						 if(preExistingdeases.contains("YES")) 
						 {
						waitForElements(By.xpath(YesButton_xpath));
						 clickElement(By.xpath(YesButton_xpath));
			//Thread.sleep(2000);
			String years=null;
			String Details=null;		
			for(int qlist=1;qlist<=12;qlist++) {
				Details =QuestionSetData[n][qlist].toString().trim();
				
				 if(Details.equals("")) {
					 //break;
				 }else {
					 int detailsnumber = Integer.parseInt(Details);

					 //Will click on check box and select the month & year u
					 detailsnumber=detailsnumber+1;
					 System.out.println("Details and years are :"+Details);
					 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
					 
				 }
			}	
			}else if(preExistingdeases.contains("NO")){
					clickElement(By.xpath(NoButton_xpath));
				}}
					 catch(Exception e){
						 logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
					 }

				String ChecksData = null;
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 2; morechecks++) 
				{
					int mch = morechecks + 1;
					ChecksData = TestCaseData[n][22 + morechecks].toString().trim();
					try{
					if (ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
					{
						System.out.println("Quatation set to NO");
						clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
					} 
					else
					{
						driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
						if (ChecksData.contains(",")) 
						{
							ChckData = ChecksData.split(",");
							for (String Chdata : ChckData) 
							{
								datacheck = Integer.parseInt(Chdata);
								datacheck = datacheck - 1;
								driver.findElement(
								By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
							}
						} else if (ChecksData.contains(""))
						{
							datacheck = Integer.parseInt(ChecksData);
							datacheck = datacheck - 1;
							driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
						}
					}
					}
					catch(Exception e){
						logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
					}

				}
				//Check Box on Health Questionnaire
				BaseClass.scrolldown();
				BaseClass.HelathQuestionnairecheckbox();
				try{
					clickElement(By.xpath(proceed_to_pay_xpath));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
					}
					catch(Exception e)
					{
						logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
						System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
					}
				
			   BaseClass.ErroronHelathquestionnaire();
				
				
				String proposalSummarypremium_value=(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']"))).getText();
				System.out.println("Total premium value is:"+proposalSummarypremium_value);
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				//wait.until(ExpectedConditions.presenceOfElementLocated(By.className("CwaK978"))).click();

				Thread.sleep(7000);
					
				waitForElement(By.xpath(payu_proposalnum_xpath));
				String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
				System.out.println(PayuProposalNum);
					
				waitForElement(By.xpath(payuAmount_xpath));
				String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
				String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
				System.out.println(FinalAmount);
				
				
				
				BaseClass.PayuPage_Credentials();
				driver.quit();
				
				 // Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
				System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareFreedom(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				driver.quit();  
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
		        driver.quit();
				
			}catch (Exception e) {
				//WriteExcel.setCellData("Test_Cases_CareFreedom", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.quit();
				
			}
			continue;
		}

	}


}
