package com.org.faveo.healthinsurance;

public interface AccountnSettingsInterface {


		//Interface for Login
		public String Textbox_UserName_xpath="//input[@name='userId']"; 
		public String Textbox_Pasword_xpath="//input[@id='pwd']";
		public String Button_Signin_xpath="//button[@id='sign_in_btn']";
		public String Button_Signin_id="sign_in_btn";
		public String WebServiceDown_xpath="//div[@class='alert alert-danger text-center']";
		
		//Interface for Reset password
		public String DropDown_Value_xpath="//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String Quotationpage_premium_xpath="//span[@ng-if='getQuote.mobileNumber.$valid && iconloading==false']";
		public String Proposalpage_premium="//p[@class='amount ng-binding']";
		/*public String Button_Resetpassword_xpath="//a[@href='javascript:;'][contains(text(),'Reset password / Unlock account')]";
		public String Textbox_UserName_xpath1="//input[@name='userId']";
		public String Button_GenerateOtp_id="gen_otp_btn";
		public String Button_GenerateOtp_xpath="//button[@id='gen_otp_btn']";
		public String Textbox_EnterOtp_xpath="//input[@name='otp']";
		public String Button_VerifyOtp_id="verfy_otp_btn";
		public String Button_VerifyOtp_xpath="//button[@id='verfy_otp_btn']";
		public String Button_ResendOtp_xpath="//a[@href='javascript:;']";
		public String Textbox_NewPassword_xpath="//input[@name='new_pwd']";
		public String Textbox_ConfirmPassword_xpath="//input[@name='conf_pwd']";
		public String Button_ChangePassword_id="id='cpwd_btn";
		public String Button_ChangePassword_xpath="//button[@id='cpwd_btn']";
		public String Text_PasswordMessage_xpath="//div[@class='alert alert-success text-center ng-binding ng-scope']";*/
		
		
		//Health Insurance DropDown Care With NCB 
		public String DropDown_HealthInsurance_xpath="//html//li[@class='dropdown open']/a[contains(text(),'Health Insurance')]";//"//html//div[2]/ul[1]/li[2]/a[1]/span[2]";
		public String Carevariant_xpath="//li[@class='dropdown-submenu']//a[@class='dropdown-toggle dropdown11 dropdown_focus']";
		public String CareVarient_xpath="//li[@class='dropdown-submenu']//a[@class='dropdown-toggle dropdown11 dropdown_focus']//span[@class='sprite down_arrow_png caret_up']";
		public String CareWithNCB_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_CARE); setSelectedMenu($event, true);']";
		public String CareSuperSaver_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_SUPERSAVER); setSelectedMenu($event, true);']";
		public String CareFreedom_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_FREEDOM); setSelectedMenu($event, true);'][contains(text(),'Care Freedom')]";
		public String CareSmartSelect_xpath="//ul[@class='dropdown-menu']//li//a[@ng-click='goToQuotation(ABACUS_ID_SMART_SELECT); setSelectedMenu($event, true);'][contains(text(),'Care with Smart Select')]";
		public String CareHNI_xpath="//ul[@class='dropdown-menu']//li//a[@ng-click='goToQuotation(ABACUS_ID_HNI); setSelectedMenu($event, true);'][contains(text(),'Care For HNI')]";
		public String CareGlobal_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_GLOBAL); setSelectedMenu($event, true);'][contains(text(),'Care Global')]";
		public String QuotationPage_premium_xpath="//span[@ng-if='getQuote.mobileNumber.$valid && iconloading==false']";
		
		//Fixed Benefit Insurance
		//public String FixedBenefitInsurance_xpath="//html//div[2]/ul[1]/li[3]/a[contains(text(),'Fixed Benefit Insurance')]";
		public String Assure_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_ASSURE); setSelectedMenu($event, true);'][contains(text(),'Assure')]";
		public String Assure_Addon1_xpath="//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div[1]/label[1]";
		public String Assure_Addon2_xpath="//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div[2]/label[1]/img";
		public String Assure_Addon1_premium_xpath="//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div[1]/label[2]/span";
		public String Assure_Addon2_premium_xpath="//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div[2]/label[2]/span";
		public String Secure_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_SECURE); setSelectedMenu($event, true);'][contains(text(),'Secure')]";
		public String Enhance_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_ENHANCE); setSelectedMenu($event, true);'][contains(text(),'Enhance')]";
		
		//Quotation Page Elements
		public String MemberDropdown_xpath="//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String AllDropdown_xpath="//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String Textbox_Name_xpath="//p[@class='p_marg']//input[@type='text']";
		public String Entername_Message_xpath="//span[@class='error-message'][contains(text(),'Please enter Name.')]";
		public String EnterValidname_Message_xpath="//span[@class='error-message'][contains(text(),'Please enter valid Name.')]";
		public String MaxCharacterLength_Message_xpath="//span[@class='error-message ng-hide'][contains(text(),'Max character length reached.')]";
		public String MaxChar_Assure_Name_xpath="//span[@class='error-message'][contains(text(),'Max character length reached.')]";
		public String Min3Character_Message_xpath="//span[@class='error-message ng-hide'][contains(text(),'Min 3 characters required.')]";
		public String Textbox_Email_xpath="//p[@class='p_marg']//input[@type='email']";
		public String Textbox_Mobile_xpath="//input[@type='tel']";
		public String Dropdown_xpath="//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String Dropdown_TotalMembers_xpath="//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a";
		//public String Dropdown_CoverType_xpath="//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a";
		public String Dropdown_ValueIndividual_xpath="//a[@ng-click='selectVal(item)'][contains(text(),'Individual')]";
		public String Dropdown_valueFamilyFloater_xpath="//a[@ng-click='selectVal(item)'][contains(text(),'Floater')]";
		public String Dropdown_AgeDrop_xpath="//*[@id='getquote']/form/div[1]/div[6]/div[1]/div/div/div/a";
		public String gjsd_xpath="//*[@id='getquote']/form/div[1]/div[6]/div[2]/div/div/div/a";
		public String Dropdown_Ageofmember1_xpath="//*[@id='getquote']/form/div[1]/div[6]/div[1]/div/div/div/ul/li[1]/a";
		public String Dropdown_Ageofmember2_xpath="//*[@id='getquote']/form/div[1]/div[6]/div[1]/div/div/div/ul/li[2]/a";
		public String SumInsured_xpath="//p[@class='sum_insured_heding ng-binding']";
		public String Radio_Tenure1_xpath="//label[@for='Radio0q']"; 
		public String Radio_Tenure2_xpath="//label[@for='Radio1q']";
		public String Radio_Tenure3_xpath="//label[@for='Radio2q']";
		public String Checkbox_Care_xpath="//label[@for='premiumRadio0q']//b[@class='ng-binding'][contains(text(),'for care')]";//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div[1]/label[1]
		public String Checkbox_CareWithNCB_xpath="//b[@class='ng-binding'][contains(text(),'for care with NCB Super')]";//*[@id='getquote']/form/div[2]/div[1]/div/div/div/div[2]/label[1]"
		public String Checkbox_UAR_xpath="//b[contains(text(),'Unlimited Recharge')]";
		public String Checkbox_EverydayCare_xpath="//b[contains(text(),'Everyday Care')]";
		public String Checkbox_PA__xpath="//b[contains(text(),'Personal Accident')]";
		public String GetPremiumValue_xpath="//*[@id='getquote']/form/div[2]/div[3]/div/div[1]/b/p[1]/span";
		public String Textbox_PAValue_xpath="//*[@id='getquote']/form/div[2]/div[2]/div/div/span/div/div/div/input";
		public String Button_Buynow_xpath="//button[@class='btn']";
		public String AllDropdown_Xpath="//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String Quotationfinalpremium_xpath="//span[@ng-if='getQuote.mobileNumber.$valid && iconloading==false']";
		
		
		//PROPOSER DETAILS Page Elements
		public String Dropdown_Title_xpath="//select[@ng-model='formParams.ValidTitle']";
		public String Dropdown_TitleMr_xpath="//option[@value='MR']";
		public String Dropdown_TitleMs_xpath="//option[@value='MS']";
		public String Dob_Proposer_id="proposer_dob";
		public String Textbox_AddressLine1_xpath="//html//div[2]/div[2]/div[1]/input[1]";
		public String Textbox_AddressLine2_xpath="//html//div[2]/div[3]/div[1]/input[1]";
		public String Textbox_Pincode_xpath="//html//div[4]/div[1]/input[1]";
		public String DropDown_CityName_xpath="//select[@ng-model='formParams.ValidCityName']";
		public String DropDown_HeightFeet_xpath="//select[@ng-model='formParams.heightFeet']";
		public String DropDown_HeightInch_xpath="//select[@ng-model='formParams.heightInches']";
		public String Textbox_Weight_xpath="//html//div[3]/div[5]/div[1]/input[1]";
		public String Textbox_NomineeName_xpath="//html//div[4]/div[1]/div[1]/input[1]";
		public String Dropdown_Nomineerelation_xpath="//select[@ng-model='formParams.nomineeRelation']";
		public String Textbox_Pancard_xpath="//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input";
		public String Button_NextProposer_id="previous1";
		
		//Insured Details Page Elements
		public String InsuredNextbtn_xpath="//input[@type='button' and @name='next']";
		
		public String Button_InsuredPage_xpath="//html//fieldset[@ng-form='proposalForm_insured']/input[2]";
		
		//Abacus Page Elements
		public String Dropdown_Planname_id="sbSelector_33169012";
	
		
		public String CareSenior_Xpath = "//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li/ul/li/a[contains(text(),'Care Senior')]";
	
				
				
			//Interface for care
				/*public String Health_Insurance_xppath="//*[@id='navbar']/ul/li[2]/a";
				public String care_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/a";
				public String care_with_NCB_xpath="//*[@id=\"navbar\"]/ul/li[2]/ul/li[1]/ul/li[1]/a";
				public String Name_xpath="//*[@id=\"getquote\"]/form/div[1]/div[1]";
				public String Email_ID_xpath="//*[@id=\"getquote\"]/form/div[1]/div[2]";
				public String Mobile_Number_xpath="//*[@id=\"getquote\"]/form/div[1]/div[3]";
				public String slider_xpth=".//*[@id='getquote']/form/div[1]/div[7]/div/div/div/span[1]";
				public String Sharequtation_xpath="//*[@id='getquote']/form/div[2]/div[3]/div/div[2]/div[1]/button";
				public String buynow_xpath="//*[@id=\"getquote\"]/form/div[2]/div[3]/div/div[2]/div[2]/button";                              
			*/
			/*public String click_title_xpath="//select[@name='ValidTitle']";
			public String dob_xpath="//input[@placeholder='DOB - DD/MM/YYYY']";
			public String addressline1_xpath="//input[@placeholder='Address Line1']";
			public String addressline2_xpath="//input[@placeholder='Address Line2']";
			public String pincode_xpath="//input[@placeholder='Pincode']";
			public String height_xpath="//select[@name='heightFeet']";
			public String inch_xpath="//select[@name='heightInches']";
			public String weight_xpath="//input[@placeholder='Weight']";
			public String Nominee_Name_xpqth="//input[@placeholder='Nominee Name']";
			public String Nominee_relation_xpath="//select[@name='nomineeRelation']";
			public String pan_xpath="//input[@placeholder='Pan Card']";
			public String submit_xpath="//input[@type='submit']";*/
			
			//=================================================2nd page==================================================================
			/*
			public String title1_xpath="//*[@id=\"insured1\"]/div[1]/div[1]/div/select";
			public String title2_xpath="//*[@id=\"insured2\"]/div[1]/div[1]/div/select";
			public String click_title2_xpath="//*[@id=\"ValidRelTitle1\"]";
			public String fname1_xpath="//input[@name='RelFName1']";
			public String lname1_xpath="//input[@name='RelLName1']";
			public String Height1_xpath="//*[@id=\"relHeightFeet1\"]";
			public String Inch1_xpath="//*[@id=\"relHeightInches1\"]";
			public String Weight1_xpath="//*[@id=\"relWeight1\"]";
			//=============================================3rd member path===============================================================
			
			public String title3_xpath="//*[@id=\"insured3\"]/div[1]/div[1]/div/select";
			public String Realation3_xpath="//*[@id=\"insured3\"]/div[1]/div[1]/div/select";
			public String click_title3_xpath="//*[@id=\"ValidRelTitle2\"]";
			public String fname3_xpath="//input[@name='RelFName2']";
			public String flname3_xpath="//input[@name='RelLName2']";
			public String Height3_xpath="//input[@name='RelLName2']";
			public String Inch3_xpath="//input[@name='RelLName2']";
			public String Weight3_xpath="//input[@name='RelLName2']";
*/
		  //===============================================4th number ===================================================================
			
			/*public String Realation4_xpath="//*[@id=\"insured4\"]/div[1]/div[1]/div/select";
			public String title4_xpath="//*[@name='ValidRelation3']";
			public String click_title4_xpath="//*[@id=\"ValidRelTitle3\"]";
			public String fname4_xpath="//*[@id=\"RelFName3\"]";
			public String flname4_xpath="//*[@id=\"RelLName3\"]";
			public String Height4_xpath="//*[@id=\"relHeightFeet3\"]";
			public String Inch4_xpath="//*[@id=\"relHeightInches3\"]";
			public String Weight4_xpath="//*[@id=\"relWeight3\"]";

*/			//========================================5th member =========================================================================
			
		/*	public String Realation5_xpath="//*[@id=\"insured5\"]/div[1]/div[1]/div/select";
			public String title5_xpath="//*[@id=\"ValidRelTitle4\"]";
			public String click_title5_xpath="//*[@id=\"ValidRelTitle4\"]";
			public String fname5_xpath="//*[@id=\"RelFName4\"]";
			public String flname5_xpath="//*[@id=\"RelLName4\"]";
			public String Height5_xpath="//*[@id=\"relHeightFeet4\"]";
			public String Inch5_xpath="//*[@id=\"relHeightInches4\"]";
			public String Weight5_xpath="//*[@id=\"relWeight4\"]";
			

*/			//========================================5th member =========================================================================
			
		/*	public String Realation6_xpath="//*[@id=\"insured6\"]/div[1]/div[1]/div/select";
			public String title6_xpath="//*[@id=\"ValidRelTitle5\"]";
			public String click_title6_xpath="//*[@id=\"ValidRelTitle5\"]";
			public String fname6_xpath="//*[@id=\"RelFName5\"]";
			public String flname6_xpath="//*[@id=\"RelLName5\"]";
			public String Height6_xpath="//*[@id=\"relHeightFeet5\"]";
			public String Inch6_xpath="//*[@id=\"relHeightInches5\"]";
			public String Weight6_xpath="//*[@id=\"relWeight5\"]";
		*/	
			
			//================Next button xpath===============
		//	public String Nextbutton_xpath="//li[@id=\"label-collapse1-1\"]/a";
			public String YesButton_xpath="//label[@for='question_1_yes']";
			public String NoButton_xpath="//label[@for='question_1_no']";
			
		
			//Addons care Freedom
			public String ForCareFreedom_Addon_xpath="//label[@for='premiumRadio0q']//img[@src='assets/img/correct_signal.png']";
			public String WithHealthCheckupPlus_Addon_xpath="//label[@for='premiumRadio2q']//img[@src='assets/img/correct_signal.png']";
			public String WithHomeCare_Addon_xpath="//label[@for='premiumRadio1q']//img[@src='assets/img/correct_signal.png']";
			public String WithHomeCareandHelathCheckplus_xpath="//label[@for='premiumRadio3q']//img[@src='assets/img/correct_signal.png']";
			
			
			//2nd member click
			/*public String secmember_cancer_xpath="//li[@id=\"label-collapse1-1\"]/a";
			public String HBP_xpath="//input[@type=\"checkbox\" and @name=\"qs_207_3\"]";
			//input[@type=\"checkbox\" and @name=\"qs_114_1\"]
			
			public String cardio_xpath="//input[@type='checkbox' and @name='qs_143_0']";
			public String respiratory_xpath="//input[@type='checkbox' and @name='qs_250_2']";
			public String disorder_xpath="//input[@type=\"checkbox\" and @id=\"qs_222_3\"]";
			public String cancer_xpath="//input[@type=\"checkbox\" and @id=\"qs_114_0\"]";
			public String diabetis_xpath="//input[@type=\"checkbox\" and @name=\"qs_205_1\"]";
		public String neuromascular_xpath="//input[@type=\"checkbox\" and @name=\"qs_164_2\"]";
		public String pancreatic_xpath="//input[@type=\"checkbox\" and @name=\"qs_232_1\"]";
		public String kidney_xpath="//input[@type=\"checkbox\" and @name=\"qs_129_1\"]";
		public String bloodimmunity_xpath="//input[@type=\"checkbox\" and @name=\"qs_129_1\"]";
		public String smoke_xpath="//input[@type=\"checkbox\" and @name=\"qs_504_0\"]";
		public String anyotherdeases_xpath="//input[@type=\"checkbox\" and @name=\"qs_210_0\"]";
*/

	/*	public String q2_yes_xpath="//a[@class='toggle_yes_no-2 yes']"; 
		public String q3_yes_xpath="//*[@class=\"toggle_yes_no-3 yes\"]";
		public String q4_No_xpath=" //*[@id=\"label-collapse4-0\"]";
		public String q5_No_xpath=" //*[@id=\"label-collapse5-0\"]";
		public String proceed_to_pay_xpath="//button[@class='btn']";
		public String pay_online_xpath="//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li[2]/a"; 
		public String card_name_xpath="//input[@name=\"cname_on_card\"]"; 
		public String cvv_xpath="//input[@name='ccvv_number']";
		public String month_xpath="//select[@name=\"cexpiry_date_month\"]";
		public String year_xpath="//select[@name=\"cexpiry_date_year\"]";
		public String pay_button="//input[@class=' credit_pay_button' and @value='Pay Now']";*/
		

		//Quotation Page Member Verification
		public String TotalMemberQuotation_xpath="//div[@class='dropdown year_drop_slect master open']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String TotalMemberPresentQuotaion_CareHNI_xpath="//ui-dropdown[@ng-model='quoteParams.postedField.field_1']//div[@class='form-group ng-scope']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String TotalMemberPresentQuotaion_CareFreedom_xpath="//ui-dropdown[@ng-model='quoteParams.postedField.field_1']//div[@class='form-group ng-scope']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String TotalMemberProposal_xpath="//*[@id='users']/tbody/tr[2]/td[2]/span";							
		
		
		//Thankyou page Message Verification
		public String ExpectedMessage_xpath="//div[contains(@class,'payment_tahnku_container')]//p[contains(@class,'your_payment_sucess_p')]";
		public String PolProp_xpath="/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]";
		public String Renewal_PolProp_xpath="//html//section[@id='succ_case1']//div[@class='applicationNoTable applicationTablenone']//td[1]";
		public String proposalsummary_thaankyou_xpath="//p[@class='styl_p ng-binding']";
		public String Renewal_proposalsummary_page_xpath="//*[@id='succ_case1']/div[1]/div[2]/div[1]/div[2]/table/tbody/tr[1]/th[1]";
		public String Downloadpdf_Thankyou_xpath="//span[contains(text(),'Download PDF')]";
		public String Downloadpdf_Travel_Thankyou_xpath="//div[@ng-show='thankUDataObj.productFamilyId == PRODUCT_FAMILY_ID_TRAVEL']//button[@ng-click='downloadPolicyPdf(policy_number)']//span[contains(text(),'Download PDF')]";
		public String TransNumMessage_xpath="//p[contains(text(),'Your Transaction number is')]";
		public String TransSuccess_xpath="//p[@class='your_payment_sucess_p ng-binding']";
		

		//Health Questionnarire Term CheckBox
		public String Term1checkbox_id="termsCheckbox1";
		public String Term3checkbox_id="termsCheckbox3";
		public String Term2checkbox_id="termsCheckbox2";
		public String alertcheckbox_id="alertCheck";
		
		//Payu PageElements
		public String payu_proposalnum_xpath="//*[@id='txndiv']/span";
		public String payuAmount_xpath="//span[@id='totalAmount']";

		//Secure Page Elements
		//public String AnnualIncome_xpath="//div[@ng-if='quoteParams.abacusId==ABACUS_ID_SECURE']//div[@class='col-md-12 quotqtion_client_white']//p[@class='p_marg']//input[@type='tel']";
		public String AnnualIncome_xpath="//input[@placeholder='Annual Income']";
		public String Salaried_xpath="//label[@for='RadioJ0q']";
		public String SelfEmployed_xpath="//label[@for='RadioJ1q']";
		public String Secure_Addon_xpath="//b[contains(text(),'Accidental Hospitalization Expenses')]";
		public String Occupationid_xpath="//select[@id='occupationClass0']";
		public String DescriptionOthers_xpath="//input[@id='otherOccupation0']";
		//public String secureYes_question_xpath="//label[@for='qs_P002_yes']";
		//public String secureNo_question_xpath="//label[@for='qs_P002_no']";
		public String Insurer_Details_xpath="//textarea[@name='qs_insurerName']";
		public String Policynumber_xpath="//textarea[@name='qs_policyNo']";
		public String PlanName_xpath="//textarea[@name='qs_planName']";
		public String Sum_Insured_xpath="//textarea[@name='qs_sumInsured']";
		public String political_party_officials_Question_Yes_xpath="//label[@for='PEPDEC_yes']";
		public String political_party_officials_Question_No_xpath="//label[@for='PEPDEC_no']";
		public String Hazardous_activity_Yes_xpath="//label[@for='qs_P007_yes']";
		public String Hazardous_activity_No_xpath="//label[@for='qs_P007_no']";
		public String illness_or_disease_Yes_xpath="//label[@for='qs_P001_yes']";
		public String illness_or_disease_No_xpath="//label[@for='qs_P001_no']";
		public String Personal_Accident_Yes_xpath="//label[@for='qs_P003_yes']";
		public String Personal_Accident_No_xpath="//label[@for='qs_P003_no']";
		public String extreme_sports_Yes_xpath="//label[@for='qs_P009_yes']";
		public String extreme_sports_No_xpath="//label[@for='qs_P009_no']";
		
		
/*====================================================Travel==================================================	*/
	
		//Explore Travel Elements
		public String TravelInsurance_xpath_Dashboard="//*[@id='navbar']//a[contains(text(),'Travel Insurance')]";
		//public String Explore_xpath="//li[@class='dropdown open']//li[1]/a[contains(text(),'Explore')]";
		public String Explore_xpath="//ul[@class='dropdown-menu dropdown_menu_focus travel_insurance show']//li[1]/a[contains(text(),'Explore')]";
		public String TravellingAllDropdowns_xpath="//div[@class='col-md-7 padding_left0 total_right_border explore_class']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String TripType_xpath = "//ui-dropdown[@ng-model='quoteParams.postedField.field_20']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		public String MaxtripDuration_xpath="//ui-dropdown[@ng-model='quoteParams.postedField.field_MTD']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
		//public String StartDateCalander_Id="start_date";
		public String StartDateCalander_Id="//input[@id='start_date']";
		public String Peddropdown_xpath="//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/a";
		public String MobileNumber_xpath="//input[@type='tel']";
		public String Emailid_xpath="//input[@name='ValidEmail']";
		public String NextButtonCalender_xpath="//span[@class='ui-icon ui-icon-circle-triangle-e']";
		public String EndDateCalender_Id="end_date";
		public String NoAddon_xpath="//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[7]/div/div/div/div/label[2]/b[1]";
		public String noAddon_edit_xpath="//label[@class='Radio32 inline']";
		public String GoldPlanAddon_xpath="//b[@class='ng-binding'][contains(text(),'for gold plan')]";
		public String PlatinumAddon_xpath="//b[@class='ng-binding'][contains(text(),'with platinum plan')]";
		public String BuyNowTravel_xpath="//button[@class='btn btn-success margin-bottom-20'][contains(text(),'Buy Now')]";
		public String NationalityDropdown_xpath="//select[@ng-model='formParams.citizenshipCd']";
		public String PassportNumber_xpath="//input[@name='passportNo']";
		public String Title_xpath="//select[@ng-model='formParams.ValidTitle']";
		public String FirstName_xpath="//input[@ng-model='formParams.FName']";
		public String LastName_xpath="//input[@ng-model='formParams.LName']";
		public String DOBCalender_xpath="//input[@name='proposer_dob']";
		public String Month_Calender_xpath="//select[@class='ui-datepicker-month']";
		public String Year_Calender_xpath="//select[@class='ui-datepicker-year']";
		public String AddressLine1_xpath="//input[@name='ValidAddressOne']";
		public String AddressLine2_xpath="//input[@name='ValidAddressTwo']";
		public String Pincode_xpath="//input[@name='ValidPinCode']";
		public String Nomineename_xpath="//input[@name='nomineeName']";
		public String PurposeofVisit_xpath="//select[@ng-model='formParams.visitPurposeCd']";
		public String PEDYesButton_xpath="//input[@ng-checked='ped.question_1 == true']";
		public String PanCard_xpath="//input[@name='panCard']";
		public String PEDNoButton_xpath="//input[@ng-checked='ped.question_1 == false']";
		public String PED_2_YesButton_xpath="//input[@ng-checked='ped.question_2 == true']";
		public String PED_2_NoButton_xpath="//input[@ng-checked='ped.question_2 == false']";		
		public String PED_3_YesButton_xpath="//input[@ng-checked='ped.question_3 == true']";
		public String PED_3_NoButton_xpath="//input[@ng-checked='ped.question_3 == false']";	
		
		
		
		
		
		//Enhance
		public String Enhance1_plan_type="//label[@for='RadioJ0q']//img[@src='assets/img/correct_signal.png']";
		public String Enhance2_plan_type="//label[@for='RadioJ1q']//img[@src='assets/img/correct_signal.png']";
		public String Addon_Benifit_xpath="//label[@for='premiumRadio1q']//img[@src='assets/img/correct_signal.png']";
		public String No_Addon_Assure_xpath="//label[@for='premiumRadio0q']//img[@src='assets/img/correct_signal.png']";
		public String Total_Premium_Quotation_xpath="//span[@ng-if='getQuote.mobileNumber.$valid && iconloading==false']";
		public String AgeofEldestMember_xpath="//ui-dropdown[@ng-model='quoteParams.postedField.field_3']//div[@class='form-group ng-scope']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']//span[@class='sprite down_arrow_png caret_up']";
		
		
		//Proposal Page Elements
		public String ViewProposal_xpath="//button[@class='button view_proposals_btn'][contains(text(),'View Proposal')]";
		public String FromdateCalender_xpath="//div[@id='datetimepicker1']/input[1]";
		public String ToDateCalender_xpath="//div[@id='datetimepicker2']/input[1]";
		public String Search_Dropdown_xpath="//a[@ng-bind-html='dropDownValue']";
		public String SerachDropdownOptionvalue_xpath="//ul[@class='dropdown-menu dropdown_menu_focus month_year_by']//a[contains(text(),'By Proposal No.')]";
		public String ErrorMessageonSearch_xpath="//div[@ng-if='errorMessage']";
		public String Search_Textbox_xpath="//div[@class='form-group has-feedback']//input[@type='text']";
		public String Search_Icon_xpath="//i[@class='glyphicon sprite search_icon_png form-control-feedback search-icon']";
		public String Last10_Search_id="lastest10";
		public String Current_Month_id="currMonth";
		public String Current_Year_id="currYear";
		public String Show_All_id="All";
		public String pdf_link_xpath="//span[@claas='pdf_span']";
		public String Excel_link_xpath="//span[@class='excell_span']";
		
		public String TotalMember_Xpath = "//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding'][contains(text(),'1')]";
		public String CareHni_Member_Xpath = "//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding'][contains(text(),'2')]";
		public String slider_xpath = "//p[@class='sum_insured_heding ng-binding']";
		
		//Renewal Tab 
		public String SideMenuBar_xpath="//span[@class='sprite nav_png']";
		public String RenewalTab_Button_xpath="//a[@class='list-group-item list-group-item-success collapse dropdown11'][contains(text(),'Renewal')]";
		public String PolicyNum_id="policyNum";
		public String Error_Message_Renewal="//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-error']//span";
		public String Modify_and_Renew_xpath="//button[@id='btn_modify_renew'][contains(text(),'Modify & Renew')]";
		public String SumInsured_PolicySum_xpath="//select[@id='sum_insured']";
		public String tenure_PolicySum_xpath="//select[@id='tenure']";
		public String Cover_Type_xpath="//select[@id='cover_type']";
		public String Button_Submit_xpath="//div[@class='col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right']//button[contains(text(),'Submit')]";
		public String ApplicationNumber_Renewal_Policy_Summary_xpath="//td[@data-title='Application No']";
		public String Renewal_Premium_Summary_xpath="//span[@id='ren_prem']";
		public String Popup_proposal_xpath="//div[@class='fancybox-inner']//div[@class='middleContainerIn rpsSection']//form[@name='policy']//div[@class='row noMargin']//div[@class='col-md-12']//table[@id='listview']//tbody//tr//td[@data-title='Application No']";
		public String Popup_Premium_xpath="//div[@class='fancybox-inner']//div[@class='middleContainerIn rpsSection']//form[@name='policy']//div[@class='row noMargin']//div[@id='new_ren_prem']//h6//b//span[@id='premiumamount']//span[@id='ren_prem1']";
		public String Online_Payment_to_Renew="//div[@class='fancybox-inner']//div[@class='middleContainerIn rpsSection']//form[@name='policy']//div[@id='submit_buttons2']//div[@id='test2']//button[@class='btn btn-success btn-renew btn-renew-width'][contains(text(),'Online Payment to Renew')]";
		public String Renewal_Thankyoupage_PolicyNum="//html//section[@id='succ_case1']//div[@class='applicationNoTable applicationTablenone']//td[1]";
		public String Renewal_Thankyoupage_Premium="//section[@id='succ_case1']//div[@class='sectionRight']//div[@class='brochureOuter stepOuter']//div[@class='brochureOuterIn']//div[@class='proposalSummarySumInsured']//div[@class='proposalSummarySumInsuredIn']//div[@class='premiumGreenContent']//strong[@class='ng-binding']";
		public String Download_Renewal_pdf="//img[@title='Download Your Policy' ]";
		public String Congratulations_xpath="//span[contains(text(),'Congratulations')]";
		public String Standing_Instruction_xpath="//input[@id='siforCreditcard']";
		public String AadharNumber_xpath="//input[@id='adNum']";
		public String GoGreen_Initiative_xpath="//input[@id='goGreen']";
		public String UAR_Addon_xpath="//input[@id='unlimitedrecharge1029']";
		public String CareWithNcb_xpath="//input[@id='unlimitedrecharge1016']";
		public String EverydayCare_xpath="//input[@id='unlimitedrecharge1012']";
		public String PA_xpath="//input[@id='unlimitedrecharge3046']";
		public String OPDCARE_xpath="//input[@id='unlimitedrecharge1032']";
		public String AddMember_Renewal_xpath="//a[@href='javascript:void(0);']//img[@align='absmiddle']";
		public String Transaction_Refrence_num_Xpath="//div[@class='transRefBox']";
		public String Cong_xpath="//section[@id='succ_case1']//div[@class='sectionRight']//h1//span";
		public String PED_Question1_No_xpath="//label[@id='label-id1-0']";
		public String PED_Question1_Yes_xpath="//label[@id='label-id1-1']";
		public String Error_Submit="//div[@class='notifyjs-corner']//div[@class='notifyjs-wrapper notifyjs-hidable']//div[@class='notifyjs-container']//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-error']//span";
		public String NCD_Declaration_xpath="//input[@id='tagidentified']";
		public String Button_IAgree="//div[@class='fancybox-inner']//div[@class='middleContainerIn rpsSection']//form[@name='NcdDeclaration']//div[@class='row noMargin']//div[@class='col-md-12']//button[@onclick='doSendLinkclose()']";
		public String GHD_NO_xpath="//label[@id='label-ghd-0']";
		public String GHD_Yes_xpath="//label[@id='label-ghd-1']";
		public String Submit_GHD_xpath="//div[@id='GHD_btn']//div[@class='col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center']//button[@type='button']";
		
		//New xpath
	


			//Interface for Login
				public String Textbox_Username_Xpath="//input[@name='userId']"; 
				//input[@placeholder='User ID']
				public String Textbox_Pasword_Xpath="//input[@id='pwd']";
				public String Button_Signin_Xpath="//button[@id='sign_in_btn']";
				public String Button_Signin_Id="sign_in_btn";
				
			//Interface for Reset password
				public String Button_Resetpassword_xpath="//a[@href='javascript:;'][contains(text(),'Reset password / Unlock account')]";
				public String Textbox_UserName_xpath1="//input[@name='userId']";
				public String Button_GenerateOtp_id="gen_otp_btn";
				public String Button_GenerateOtp_xpath="//button[@id='gen_otp_btn']";
				public String Textbox_EnterOtp_xpath="//input[@name='otp']";
				public String Button_VerifyOtp_id="verfy_otp_btn";
				public String Button_VerifyOtp_xpath="//button[@id='verfy_otp_btn']";
				public String Button_ResendOtp_xpath="//a[@href='javascript:;']";
				public String Textbox_NewPassword_xpath="//input[@name='new_pwd']";
				public String Textbox_ConfirmPassword_xpath="//input[@name='conf_pwd']";
				public String Button_ChangePassword_id="id='cpwd_btn";
				public String Button_ChangePassword_xpath="//button[@id='cpwd_btn']";
				public String Text_PasswordMessage_xpath="//div[@class='alert alert-success text-center ng-binding ng-scope']";
				
				
			//Interface for care
				public String Health_Insurance_xppath="//*[@id='navbar']/ul/li[2]/a";
				public String care_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/a";
				
				public String CareHeart_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[1]/ul/li[7]/a";
				public String care_with_NCB_xpath="//*[@id=\"navbar\"]/ul/li[2]/ul/li[1]/ul/li/ul/li[1]/a";
				public static String careWithOPD_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[1]/ul/li/a[contains(text(),'Care (with OPD)')]"; 
				//public String care_with_NCB_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[1]/a";
				public String Name_xpath="//*[@id='getquote']/form/div[1]/div[1]";
				public String Email_ID_xpath="//*[@id=\"getquote\"]/form/div[1]/div[2]";
				public String Mobile_Number_xpath="//*[@id=\"getquote\"]/form/div[1]/div[3]";
				public String slider_xpth=".//*[@id='getquote']/form/div[1]/div[7]/div/div/div/span[1]";
				public String Sharequtation_xpath="//*[@id='getquote']/form/div[2]/div[3]/div/div[2]/div[1]/button";
				public String buynow_xpath="//*[@id=\"getquote\"]/form/div[2]/div[3]/div/div[2]/div[2]/button";  
				     public String Enhance_buynow_xpath=" //*[@id='getquote']/form/div[2]/div[2]/div/div[2]/div[2]/button";                       
			
			public String click_title_xpath="//select[@name='ValidTitle']";
			public String dob_xpath="//input[@placeholder='DOB - DD/MM/YYYY']";
			public String addressline1_xpath="//input[@placeholder='Address Line1']";
			public String addressline2_xpath="//input[@placeholder='Address Line2']";
			public String pincode_xpath="//input[@placeholder='Pincode']";
			public String height_xpath="//select[@name='heightFeet']";
			public String inch_xpath="//select[@name='heightInches']";
			public String weight_xpath="//input[@placeholder='Weight']";
			public String Nominee_Name_xpqth="//input[@placeholder='Nominee Name']";
			public String Nominee_relation_xpath="//select[@name='nomineeRelation']";
			public String pan_xpath="//input[@placeholder='Pan Card']";
			//public String submit_xpath="//input[@type='submit']";
			public String submit_xpath="//input[@name='next']";
			
			//=================================================2nd page==================================================================
			
			public String title1_xpath="//*[@id=\"insured1\"]/div[1]/div[1]/div/select";
			public String title2_xpath="//*[@id=\"insured2\"]/div[1]/div[1]/div/select";
			public String click_title2_xpath="//*[@id=\"ValidRelTitle1\"]";
			public String fname1_xpath="//input[@name='RelFName1']";
			public String lname1_xpath="//input[@name='RelLName1']";
			public String Height1_xpath="//*[@id=\"relHeightFeet1\"]";
			public String Inch1_xpath="//*[@id=\"relHeightInches1\"]";
			public String Weight1_xpath="//*[@id=\"relWeight1\"]";
			//=============================================3rd member path===============================================================
			
			public String title3_xpath="//*[@id=\"insured3\"]/div[1]/div[1]/div/select";
			public String Realation3_xpath="//*[@id=\"insured3\"]/div[1]/div[1]/div/select";
			public String click_title3_xpath="//*[@id=\"ValidRelTitle2\"]";
			public String fname3_xpath="//input[@name='RelFName2']";
			public String flname3_xpath="//input[@name='RelLName2']";
			public String Height3_xpath="//input[@name='RelLName2']";
			public String Inch3_xpath="//input[@name='RelLName2']";
			public String Weight3_xpath="//input[@name='RelLName2']";

		  //===============================================4th number ===================================================================
			
			public String Realation4_xpath="//*[@id=\"insured4\"]/div[1]/div[1]/div/select";
			public String title4_xpath="//*[@name='ValidRelation3']";
			public String click_title4_xpath="//*[@id=\"ValidRelTitle3\"]";
			public String fname4_xpath="//*[@id=\"RelFName3\"]";
			public String flname4_xpath="//*[@id=\"RelLName3\"]";
			public String Height4_xpath="//*[@id=\"relHeightFeet3\"]";
			public String Inch4_xpath="//*[@id=\"relHeightInches3\"]";
			public String Weight4_xpath="//*[@id=\"relWeight3\"]";

			//========================================5th member =========================================================================
			
			public String Realation5_xpath="//*[@id=\"insured5\"]/div[1]/div[1]/div/select";
			public String title5_xpath="//*[@id=\"ValidRelTitle4\"]";
			public String click_title5_xpath="//*[@id=\"ValidRelTitle4\"]";
			public String fname5_xpath="//*[@id=\"RelFName4\"]";
			public String flname5_xpath="//*[@id=\"RelLName4\"]";
			public String Height5_xpath="//*[@id=\"relHeightFeet4\"]";
			public String Inch5_xpath="//*[@id=\"relHeightInches4\"]";
			public String Weight5_xpath="//*[@id=\"relWeight4\"]";
			

			//========================================5th member =========================================================================
			
			public String Realation6_xpath="//*[@id=\"insured6\"]/div[1]/div[1]/div/select";
			public String title6_xpath="//*[@id=\"ValidRelTitle5\"]";
			public String click_title6_xpath="//*[@id=\"ValidRelTitle5\"]";
			public String fname6_xpath="//*[@id=\"RelFName5\"]";
			public String flname6_xpath="//*[@id=\"RelLName5\"]";
			public String Height6_xpath="//*[@id=\"relHeightFeet5\"]";
			public String Inch6_xpath="//*[@id=\"relHeightInches5\"]";
			public String Weight6_xpath="//*[@id=\"relWeight5\"]";
			
			
			//================Next button xpath===============
			public String Nextbutton_xpath="//li[@id=\"label-collapse1-1\"]/a";
			public String yes_button_xpath="//*[@id=\"headingOne\"]/h4/div[2]/div/label[1]";
			//2nd member click
			public String secmember_cancer_xpath="//li[@id=\"label-collapse1-1\"]/a";
			public String HBP_xpath="//input[@type=\"checkbox\" and @name=\"qs_207_3\"]";
			//input[@type=\"checkbox\" and @name=\"qs_114_1\"]
			
			public String cardio_xpath="//input[@type='checkbox' and @name='qs_143_0']";
			public String respiratory_xpath="//input[@type='checkbox' and @name='qs_250_2']";
			public String disorder_xpath="//input[@type=\"checkbox\" and @id=\"qs_222_3\"]";
			public String cancer_xpath="//input[@type=\"checkbox\" and @id=\"qs_114_0\"]";
			public String diabetis_xpath="//input[@type=\"checkbox\" and @name=\"qs_205_1\"]";
		public String neuromascular_xpath="//input[@type=\"checkbox\" and @name=\"qs_164_2\"]";
		public String pancreatic_xpath="//input[@type=\"checkbox\" and @name=\"qs_232_1\"]";
		public String kidney_xpath="//input[@type=\"checkbox\" and @name=\"qs_129_1\"]";
		public String bloodimmunity_xpath="//input[@type=\"checkbox\" and @name=\"qs_129_1\"]";
		public String smoke_xpath="//input[@type=\"checkbox\" and @name=\"qs_504_0\"]";
		public String anyotherdeases_xpath="//input[@type=\"checkbox\" and @name=\"qs_210_0\"]";

		public String Dropdown_CoverType_xpath="//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a";
		public String q2_yes_xpath="//a[@class='toggle_yes_no-2 yes']"; 
		public String q3_yes_xpath="//*[@class=\"toggle_yes_no-3 yes\"]";
		public String q4_No_xpath=" //*[@id=\"label-collapse4-0\"]";
		public String q5_No_xpath=" //*[@id=\"label-collapse5-0\"]";
		public String proceed_to_pay_xpath="//button[text()='Proceed to Pay']";
		public String pay_online_xpath="//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li[2]/a"; 
		public String card_name_xpath="//input[@name=\"cname_on_card\"]"; 
		public String cvv_xpath="//input[@name='ccvv_number']";
		public String month_xpath="//select[@name=\"cexpiry_date_month\"]";
		public String year_xpath="//select[@name=\"cexpiry_date_year\"]";
		public String pay_button="//input[@class=' credit_pay_button' and @value='Pay Now']";

		//============================================Care Super Saver====================



		public String care_super_saver_xpath="//*[@id=\"navbar\"]/ul/li[2]/ul/li[1]/ul/li[3]/a";
		public String premium_xpath="//*[@id=\"getquote\"]/form/div[2]/div[2]/div/div[1]/b/p[1]/span";
		public String fst_buy_now_xpath="//*[@class=\"btn\"]";
		public String sec_premium_xpath="//*[@id=\"msform\"]/div[1]/div/div/div/div[4]/p[1]";
		public String pay_pre_online_xpath="//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li[2]/a";


		//=============================================CAre for HNI===========================

		public String   care_for_HNI_xpath ="//*[@id=\"navbar\"]/ul/li[2]/ul/li[1]/ul/li[9]/a";
		public String Next_xpath="//input[@type='button' and @name='next']";
		public String HNI_Payonline_xpath="//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li";
		public String HNI_NExt_xpath="//*[@id=\"headingOne\"]/h4/div[2]/div/label[1]";


		//=============================Assure=========================================================
		public String Fixed_benifit_nsurance_xpath="//*[@id='navbar']/ul/li[3]/a";
		public String assure_xpath="//*[@class='dropdown-menu dropdown_menu_focus fixed_benift_insurance']/li[1]/a";

		//************************ Secure *****************************
		public String FixedBenefitInsurance_xpath="//*[@class='navbar-collapse collapse heder_nav_dropdown heder_nav_head']/ul/li[3]/a";
		public String securebutton_xpath="//*[@class='dropdown-menu dropdown_menu_focus fixed_benift_insurance']/li[3]/a";
		public String POS_Secure_xpath="//ul[@class='dropdown-menu dropdown_menu_focus fixed_benift_insurance show']/li/a[contains(text(),'POS Secure')]";
		//public String securebutton_xpath="/*[@id='navbar']/ul/li[3]/ul/li[3]/a";
		public String Jobtype1_xpath="//input[@id='RadioJ0q']";
		public String Jobtype2_xpath="//input[@id='RadioJ1q']";
		public String secureaddon_xpath="//*[@id=\"getquote\"]/form/div[2]/div[2]/div/div/label/span[2]/span";
		public String secureYes_question_xpath="//*[@id=\"headingOne\"]/h4/div[2]/div/label[1]";
		public String secureNo_question_xpath="//*[@id=\"headingOne\"]/h4/div[2]/div/label[2]";
		public String insurer_xpath="//*[@id='collapse1']/div[2]/table/tbody/tr[1]/td[2]/label/textarea";
		public String policynumber_xpath ="//*[@id='collapse1']/div[2]/table/tbody/tr[2]/td[2]/label/textarea";
		public String plan_name_xpath="//*[@id='collapse1']/div[2]/table/tbody/tr[3]/td[2]/label/textarea";
		public String sum_insured_xpath="//*[@id=\"collapse1\"]/div[2]/table/tbody/tr[4]/td[2]/label/textarea";
		public String question2_Yes_xpath="//*[@id=\"headingTwo\"]/h4/div[2]/div/label[1]";
		public String question2_No_xpath="//*[@id=\"headingTwo\"]/h4/div[2]/div/label[2]";
		public String question3_Yes_xpath="//*[@id=\"headingThree\"]/h4/div[2]/div/label[1]";
		public String question3_No_xpath="//*[@id=\"headingThree\"]/h4/div[2]/div/label[2]";
		public String question4_Yes_xpath="//input[@id='qs_P001_yes']/following-sibling::label[text()='Yes']";
		public String question4_No_xpath="//input[@id='qs_P001_yes']/following-sibling::label[text()='No']";
		public String question5_Yes_xpath="//input[@id='qs_P003_yes']/following-sibling::label[text()='Yes']";
		public String question5_No_xpath="//input[@id='qs_P003_yes']/following-sibling::label[text()='Yes']";

		public String question6_Yes_xpath="//input[@id='qs_P009_yes']/following-sibling::label[text()='Yes']";
		public String question6_No_xpath="//input[@id='qs_P009_yes']/following-sibling::label[text()='No']";
		public String Agree_xpath="//input[@id=\"termsCheckbox1\"]";
		public String proposer_xpath="//input[@id=\"termsCheckbox3\"]";
		public String proceed_to_pay_secure_xpath="//*[@id=\"msform\"]/div[2]/fieldset[3]/span/button";

		//*****************Enhance**********************************
		public String enhancebutton_xpath="//*[@id='navbar']/ul/li[2]/ul/li[3]/a";
		public String member_xpath="//*[@id='numberOfMembers_id']/option";
		public String Enhance_pay_online_xpath="//ul[@class=\"nav nav-tabs text-center nav_nav_tabs_child\"]/li";

		public String PAHealthQuestion_Yes_xpath = "//label[@for='question_5_yes']";
		public String PAHealthQuestion_No_xpath = "//label[@for='question_5_no']";

		//====================================interface of Faveo OTC==========================

		public String dashboard_xpath="//*[@class='sprite nav_png']";
		public String OTC_Menu_xpath="//*[@id=\"MainMenu\"]/ul[12]/li/a";
		public String Generate_slip_xpath="//*[@id=\"otcSideMenu\"]/li[2]/a";
		public String depositedate_xpath="//*[@id='depositDate']";
		public String praposal_xapth="//*[@id='MainMenu']/ul[8]/li/a";
		
		//=========================Interface of PosCare(WIth NCB)===============================
		public String poscare_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[13]/a";
        public String posfreedom_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[15]/a";

        //================Group Explore Interface================================================
        //public String travelinsurance_xpath="//ul[@class='nav navbar-nav']/li[1]/a"; 
        public String travelinsurance_xpath="//*[@id='navbar']/ul/li[1]/a";
       // public String GroupExplore_xpath ="//*[@id='navbar']/ul/li[1]/ul/li[7]/a";
       // public String GroupExplore_xpath="//ul[@class='dropdown-menu dropdown_menu_focus travel_insurance']/li/a[contains(text(),'Group Explore')]";
        public String GroupExplore_xpath="//*[@id='navbar']/ul/li[1]/ul/li/a[contains(text(),'Group Explore')]";
		public String Group_BuyNow_xpath="//div[@class='col-md-12 padding0 tr_no_btn text-center']/button[@class='btn btn-success margin-bottom-20']";
public String POSSuperSaver_xpath="//*[@id='getquote']/form/div[2]/div[2]/div/div[2]/div[2]/button";
String GroupExplore_buynow_xpath="//*[@id=\"tr_quotation_full_cont\"]/div[2]/form/div/div/div/div[8]/button[2]";
//===================Care Senior Interface=====================
		public String CareSenior_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[1]/ul/li[7]/a";
//===================StudentExplorer==============================================================
		
        public String StudentExplorer_xpath="//*[@id='navbar']/ul/li[1]/ul/li[2]/a";
        public String nomineRelation_xpath="//select[@name='nomineeRelation']";
        public String TripStartDate_xpath="//*[@id='trip_start_date']";
        
        
        //========================POS care super saver=========================================
        public String POsCareSUperSaver_xpath ="//ul[@class='dropdown-menu-custom']/li/a[contains(text(),'POS Care Super Saver')]";
        public String POS_PAYonline_xpath="//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li[2]/a";
        
        //========================POS Care With NCB==================================================
        public String POS_CarewithNCB_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[2]/ul/li/a[contains(text(),'POS Care (with NCB)')]";
        //=======================Care superMediclaim==============
        public String mediclaimcare_xpath="//*[@id='navbar']/ul/li[2]/ul/li[2]/a/p";
        public String supermediclaim_xpath="//*[@id='navbar']/ul/li[2]/ul/li[2]/ul/li/ul/li[1]/a";
        public String critical_xpath="//*[@class='share_qution_btn share']/button[contains(text(),'Buy Now')]";
        public String critical_PAYonline_xpath ="//*[@class='tabbable-line']/ul/li/a";
       
        public String critical_edit_xpath="//*[@id='msform']/div[1]/div/div/div/div[2]/div/a";
        public String critical_updatepremium_xpath="//*[@id='editGetQuot']/div/div/div/div[3]/form/div[2]/div/button";
        public String PosCare_Freedom_xpath="//a[@ng-click='goToQuotation(ABACUS_ID_POS_FREEDOM); setSelectedMenu($event, true);']";
        //Edit for Care With NCB
        
     public String Edit_careWithNCB_xpath= "//a[@class='btn edit']";
     public String Edit_CareSuperSaver_xpath="//a[@class='btn edit']";
        
                public String Edit_update_premium_xpath="//button[@data-dismiss='modal']";
        
        public String EditProceed_to_payOnline_xpath="//*[@class='tabbable-line']/ul/li[2]/a[contains(text(),'PAY ONLINE')]";
        
        //new edited data
        public String shareProposal_Xpath="//input[@id='shareProposalId']";
        public String shareProposalTravel_Xpath="//button[@ng-show='shareProposalBtnDisplay']";
        public String emailShareProposal_Xpath="//h4[text()='Save & Share Proposal']//following::div[1]//input[@id='usr']";
        public String phoneShareProposalXpath="//h4[text()='Save & Share Proposal']//following::div[1]//input[@placeholder='Mobile Number']";
        public String smsSliderShareProposalXpath="//h4[text()='Save & Share Proposal']//following::div[1]//input[@id='Radioq2']//following-sibling::div";
        public String saveAndShareProposalButton_Xpath="//h4[text()='Save & Share Proposal']//following::div[1]//button";
        public String closeShareProposalPopupBox_Xpath="//h4[text()='Save & Share Proposal']//following-sibling::p/img";
        public String txtvalue1_ShareProposal_Xpath="//h4[text()='Save & Share Proposal']//following::div[@ng-show='saveContinueEmailStatus.showSuccessMessage']/h4[1]";
        public String txtvalue2_ShareProposal_Xpath="//h4[text()='Save & Share Proposal']//following::div[@ng-show='saveContinueEmailStatus.showSuccessMessage']/h4[2]";
        public String draftHistory_Xpath="//nav[@id='sidebar-wrapper']//ul//li[@class='side_draft_navigation']//a[@ng-click='goToDraftHistory(); setSelectedMenu($event, true);']";
        public String resumeProposalLink_Xpath="//div[contains(@class,'commission_due_report_table_main_container')]//table//tbody/tr[1]//td[4]/span/a";
        
        
        
        public String userName_Xpath="//input[@id='username']";
        public String password_Xpath="//input[@id='password']";
       // public String signIN_Xpath="//input[@value='Sign in']";
        public String signIN_Xpath="//span[text()='sign in']";
       // public String shareQuotationFolder_Xpath="//div[@id='divTrNdCC']//span[text()='SharedQuotation']//ancestor::a";
        public String shareQuotationFolder_Xpath="//span[text()='SharedQuotation']";
        public String shareQuotationFolder_Xpath1="//span[text()='Junk E-Mail']";
        
        public String deleteButton_In_Mail_Xpath="//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divMsgItemTB']/div[3]/a";
       // public String popserName_In_MailBody_Xpath="//div[@id='divMainView']/div[2]//div[@id='ifRP']//div[@id='divItmPrtsScr']//div[@id='divBdy']//div[1]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr[1]//span";
        public String popserName_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table//tr[3]/td[1]/table/tbody/tr[2]//table/tbody/tr[1]/td";
       //public String totalNumberOfMembers_In_MailBody_Xpath="//div[@id='divMainView']/div[2]//div[@id='ifRP']//div[@id='divItmPrtsScr']//div[@id='divBdy']//div[1]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr[2]//td[3]";
        public String totalNumberOfMembers_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table//tr[3]/td[1]/table/tbody/tr[2]//table/tbody/tr[2]/td[3]";
        
        //public String sumInsured_In_MailBody_Xpath="//div[@id='divMainView']/div[2]//div[@id='ifRP']//div[@id='divItmPrtsScr']//div[@id='divBdy']//div[1]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr[5]//td[3]";
        public String sumInsured_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table//tr[3]/td[1]/table/tbody/tr[2]//table/tbody/tr[5]/td[3]";
       
       //public String premiumAndTenure_In_MailBody_Xpath="//div[@id='divMainView']/div[2]//div[@id='ifRP']//div[@id='divItmPrtsScr']//div[@id='divBdy']//div[1]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr[7]//td[3]//b";
        public String premiumAndTenure_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table//tr[3]/td[1]/table/tbody/tr[2]//table/tbody/tr[7]/td[3]/strong";
       
        //public String groupMembers_In_MailBody_Xpath="//div[@id='divMainView']/div[2]//div[@id='ifRP']//div[@id='divItmPrtsScr']//div[@id='divBdy']//div[1]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr[4]//td[3]";
        public String groupMembers_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table//tr[3]/td[1]/table/tbody/tr[2]//table/tbody/tr[4]/td[3]";
        public String groupMembers_Travel_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div[contains(@class,'_rp_z3')]/div/div/table/tbody/tr[3]/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td[3]";
        
      //public String buyNow_LinkButton_In_MailBody_Xpath="//div[@id='divMainView']/div[2]//div[@id='ifRP']//div[@id='divItmPrtsScr']//div[@id='divBdy']//div[1]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody//tr[7]//following::tr[1]//td/div[1]/a";
        public String buyNow_LinkButton_In_MailBody_Xpath="//div[@class='_rp_e allowTextSelection']//div[@id='Item.MessagePartBody']/div/div//following-sibling::div/table//tr[3]/td[1]/table/tbody/tr[3]/td/p[1]//following::a[1]";
        
        public String sharedProposalFolder_Xpath="//div[@id='divTrNdCC']//span[text()='SharedProposal']//ancestor::a";
        public String deleteButton_ForShareProposalEmails_In_Mail_Xpath="//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]/div[@id='divVw']/div[2]/div[@id='divLVContainer']//div[@id='divMsgItemTB']/div[3]/a";
      
    	public String nextButtonSuperMediclaim_Xpath="//input[@id='previous2']//following-sibling::input[1]";
    	
    	//Pan Card Required Premium Error if More than 50000
    	public String PanCardRequired_premiumErrorAmount_Xpath="//div[@class='col-md-12 text-center last-child']//h5[2]//b[2]";
    	//Close Pan Card Required Premium error poup
    	public String closePanError_Xpath="//div[@class='col-md-12 text-center last-child']//h5[2]//ancestor::div[1]//preceding::div[1]//p/img";
    	public String pancardTextField_Xpath="//input[@name='panCard']";
    	
    	public String nextButton_QuotationPage_Xpath="//input[@id='previous1']";
    	public String Dashboard_logoImage_Xpath="//nav[contains(@class,'navbar-fixed-top')]//a[@ng-click='goToDashboard();']";
    	public String Proposal_Dashboard_xpath = "//h5[@class='text-center'][contains(text(),'Your Proposals')]";
    	
    	public String Logout_Grid_xpath = "//a[@href='javascript:;']//span[@class='sprite dd_png']";
    	public String Logout_Button_xpath = "//li[@class='logout']//a[contains(text(),'Log Out')]";
    	public String Logout_message_xpath = "//div[@class='alert alert-success text-center ng-binding ng-scope']";

    	//Share Quotation
    	public String shareQuotation_Xpath="//button[text()='Share Quotation']";
    	//public String emailShareQuotation_Xpath="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']//input[@type='email']";
    	public String emailShareQuotation_Xpath="//h4[text()='Share Quotation']//following::div[1]//input[@type='email']";
    	//public String SmsQuotation_Xpath="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']//input[@name='mobileNumber']";
    	public String SmsQuotation_Xpath="//h4[text()='Share Quotation']//following::div[1]//input[@type='tel']";
    	
    	public String EmailSliderShareQuotation_Xpath="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']//input[@name='EmailRadios']//ancestor::label";
    	public String SmsSliderShareQuotation_Xpath="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']//input[@name='SMSRadios']//ancestor::label";
    	//public String closeShareQuotationPoupBox="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']/div/div/div/div/h4/following-sibling::p/img";
    	public String closeShareQuotationPoupBox="//h4[text()='Share Quotation']//following-sibling::p/img";
    	
    	//public String emailTextValueInPopupBox="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']/div/div/div/div/h4//following::div[@ng-show='quotationEmailStatus.showSuccessMessage']//b[@ng-if='quotePopupParams.emailFlag']";
    	 public String emailTextValueInPopupBox="//h4[text()='Share Quotation']//following::b[1]";
       //public String SmsTextValueInPopupBox="//h4[text()='Share Quotation']//following::div[@id='myModalMail_bel' and @visible='showQuoteEmailModal']/div/div/div/div/h4//following::div[@ng-show='quotationEmailStatus.showSuccessMessage']//b[@ng-if='quotePopupParams.smsFlag']";
        public String SmsTextValueInPopupBox="//h4[text()='Share Quotation']//following::b[2]";
       
        public String hamburgerMenu_Xpath="//nav[contains(@class,'navbar-fixed-top')]//a[contains(@class,'hamburger')]";
        public String quotationTracker_Xpath="//nav[@id='sidebar-wrapper']//ul//li[@class='side_proposal_navigation']//a[@ng-click='goToQuotesTracker(); setSelectedMenu($event, true);']";
        public String lastSevenDaysFilter_Xpath="//div[contains(@class,'Check_buttons')]//button[1]";
        public String searchByEmailTextBox_Xpath="//input[@name='searchEmail']";
        public String Quotation_searchButton_Xpath="//input[@name='searchEmail']//ancestor::div[1]//i";
      
        public String emailShareQuotationTravel_Xpath="//input[@name='qutationEmail']";
        public String emailShareProposalTravel_Xpath="//input[@ng-model='saveContinueForm.emailID']";
        public String SmsQuotation_Travel_Xpath="//h4[text()='Share Quotation']//following::div[1]/form//div//input[@placeholder='Mobile Number']";
        public String sms_ShareProposal_Travel_Xpath="//input[@ng-model='saveContinueForm.mobileNumber']";
        public String closeShareQuotationPoupBox_TravelInsurance="//h4[text()='Share Quotation']//following-sibling::p/img";
        public String emailTextValueInPopupBox_TravelInsurance="//h4[text()='Share Quotation']//following::b[1]";
        public String SmsTextValueInPopupBox_TravelInsurance="//h4[text()='Share Quotation']//following::b[2]";
    	//******  Super Mediclaim UI Elements ****************************************************************
    	public String Health_Insurance_xppath1 = "//div[contains(@class,'heder_nav_dropdown')][1]//ul[starts-with(@class,'nav')]//li[2]//a[@role='button']";
    	// Cancer
    	public String superMediClaim_Xpath="//div[@id='navbar']/ul/li[2]/ul/li[2]/a";
    	public String superMediClaim_Cancer_Xpath="//a[text()='Cancer']";
    	
    	public String superMediClaim_tenure2Year_Xpath="//div[@class='input_year']//label//following-sibling::label[text()='2 Year']//preceding-sibling::label";
    	public String superMediClaim_tenure3Year_Xpath="//div[@class='input_year']//label//following-sibling::label[text()='3 Year']//preceding-sibling::label";
    	
    	public String superMediclaim_BuyNowButton_Xpath="//button[text()='Buy Now']";
    	public String superMediclaim_PaymentFrequency_Monthly_Xpath="//div[@class='progress_slider_container']//label[text()='Monthly']//ancestor::div[1]";
    	public String superMediclaim_PaymentFrequency_Quarterly_Xpath="//div[@class='progress_slider_container']//label[text()='Quarterly']//ancestor::div[1]";
    	
    	//Insured Details 1 
    	public String superMediclaim_Insured_1_Relation_Xpath="ValidRelation0";
    	public String superMediclaim_Insured_1_FName_Xpath="//input[@name='RelFName0']";
    	public String superMediclaim_Insured_1_LName_Xpath="//input[@name='RelLName0']";
    	public String superMediclaim_Insured_1_DOB_Xpath="//input[@name='rel_dob0']";
    	public String superMediclaim_Insured_1_Height_Xpath="relHeightFeet0";
    	public String superMediclaim_Insured_1_Inch_Xpath="relHeightInches0";
    	public String superMediclaim_Insured_1_Weight_Xpath="//input[@name='relWeight0']";
    	
    	// Insured Details 2
    	public String superMediclaim_Insured_2_Relation_Xpath = "ValidRelation1";
    	public String superMediclaim_Insured_2_FName_Xpath = "//input[@name='RelFName1']";
    	public String superMediclaim_Insured_2_LName_Xpath = "//input[@name='RelLName1']";
    	public String superMediclaim_Insured_2_DOB_Xpath = "//input[@name='rel_dob1']";
    	public String superMediclaim_Insured_2_Height_Xpath = "relHeightFeet1";
    	public String superMediclaim_Insured_2_Inch_Xpath = "relHeightInches1";
    	public String superMediclaim_Insured_2_Weight_Xpath = "//input[@name='relWeight1']";
    	
    	// Insured Details 3
    	public String superMediclaim_Insured_3_Relation_Xpath = "ValidRelation2";
    	public String superMediclaim_Insured_3_FName_Xpath = "//input[@name='RelFName2']";
    	public String superMediclaim_Insured_3_LName_Xpath = "//input[@name='RelLName2']";
    	public String superMediclaim_Insured_3_DOB_Xpath = "//input[@name='rel_dob2']";
    	public String superMediclaim_Insured_3_Height_Xpath = "relHeightFeet2";
    	public String superMediclaim_Insured_3_Inch_Xpath = "relHeightInches2";
    	public String superMediclaim_Insured_3_Weight_Xpath = "//input[@name='relWeight2']";
    	
    	// Insured Details 4
    	public String superMediclaim_Insured_4_Relation_Xpath = "ValidRelation3";
    	public String superMediclaim_Insured_4_FName_Xpath = "//input[@name='RelFName3']";
    	public String superMediclaim_Insured_4_LName_Xpath = "//input[@name='RelLName3']";
    	public String superMediclaim_Insured_4_DOB_Xpath = "//input[@name='rel_dob3']";
    	public String superMediclaim_Insured_4_Height_Xpath = "relHeightFeet3";
    	public String superMediclaim_Insured_4_Inch_Xpath = "relHeightInches3";
    	public String superMediclaim_Insured_4_Weight_Xpath = "//input[@name='relWeight3']";
    	
    	// Insured Details 5
    	public String superMediclaim_Insured_5_Relation_Xpath = "ValidRelation4";
    	public String superMediclaim_Insured_5_FName_Xpath = "//input[@name='RelFName4']";
    	public String superMediclaim_Insured_5_LName_Xpath = "//input[@name='RelLName4']";
    	public String superMediclaim_Insured_5_DOB_Xpath = "//input[@name='rel_dob4']";
    	public String superMediclaim_Insured_5_Height_Xpath = "relHeightFeet4";
    	public String superMediclaim_Insured_5_Inch_Xpath = "relHeightInches4";
    	public String superMediclaim_Insured_5_Weight_Xpath = "//input[@name='relWeight4']";
    	
    	// Insured Details 6
    	public String superMediclaim_Insured_6_Relation_Xpath = "ValidRelation5";
    	public String superMediclaim_Insured_6_FName_Xpath = "//input[@name='RelFName5']";
    	public String superMediclaim_Insured_6_LName_Xpath = "//input[@name='RelLName5']";
    	public String superMediclaim_Insured_6_DOB_Xpath = "//input[@name='rel_dob5']";
    	public String superMediclaim_Insured_6_Height_Xpath = "relHeightFeet5";
    	public String superMediclaim_Insured_6_Inch_Xpath = "relHeightInches5";
    	public String superMediclaim_Insured_6_Weight_Xpath = "//input[@name='relWeight5']";



    	//Health Questions Super Mediclaim Cancer
    	public String superMediclaimCancer_Quest1_YesButton_Xpath="//label[@for='question_1_yes']";
    	public String superMediclaimCancer_Quest1_NoButton_Xpath="//label[@for='question_1_no']";
    	
    	public String superMediclaimCancer_Quest2_YesButton_Xpath="//label[@for='question_3_yes']";
    	public String superMediclaimCancer_Quest2_NoButton_Xpath="//label[@for='question_3_no']";
    	
    	public String superMediclaimCancer_Quest3_YesButton_Xpath="//label[@for='question_5_yes']";
    	public String superMediclaimCancer_Quest3_NoButton_Xpath="//label[@for='question_5_no']";
    	
    	public String superMediclaimCancer_Quest4_YesButton_Xpath="//label[@for='question_6_yes']";
    	public String superMediclaimCancer_Quest4_NoButton_Xpath="//label[@for='question_6_no']";
    	
    	public String superMediclaimCancer_Quest5_YesButton_Xpath="//label[@for='question_4_yes']";
    	public String superMediclaimCancer_Quest5_NoButton_Xpath="//label[@for='question_4_no']";
    	
    	public String superMediclaimCancer_Quest6_YesButton_Xpath="//label[@for='question_2_yes']";
    	public String superMediclaimCancer_Quest6_NoButton_Xpath="//label[@for='question_2_no']";
    	
    	//UI Elements of Check Box and Since Existing Disease Dates UI Elements for Sub Question 1 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber1_Xpath="//input[@id='qs_114_0']";
    	public String sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber1_Xpath="//input[@id='qs_114_0']//following::div[1]/label/input";
    	
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber2_Xpath="//input[@id='qs_114_1']";
    	public String sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber2_Xpath="//input[@id='qs_114_1']//following::div[1]/label/input";
    	
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber3_Xpath="//input[@id='qs_114_2']";
    	public String sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber3_Xpath="//input[@id='qs_114_2']//following::div[1]/label/input";
    	
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber4_Xpath="//input[@id='qs_114_3']";
    	public String sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber4_Xpath="//input[@id='qs_114_3']//following::div[1]/label/input";
    	
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber5_Xpath="//input[@id='qs_114_4']";
    	public String sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber5_Xpath="//input[@id='qs_114_4']//following::div[1]/label/input";
    	
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues1_ForMemeber6_Xpath="//input[@id='qs_114_5']";
    	public String sinceExistingDiseaseDate_forSubQuest1_UnderQues1_ForMemeber6_Xpath="//input[@id='qs_114_5']//following::div[1]/label/input";
    	
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question Under 2 Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_143_0']";
    	public String sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_143_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_143_1']";
    	public String sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_143_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_143_2']";
    	public String sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_143_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_143_3']";
    	public String sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_143_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_143_4']";
    	public String sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_143_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_143_5']";
    	public String sinceExistingDiseaseDate_forSubQuest2_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_143_5']//following::div[1]/label/input";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements forSub Question 3 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_207_0']";
    	public String sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_207_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_207_1']";
    	public String sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_207_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_207_2']";
    	public String sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_207_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_207_3']";
    	public String sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_207_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_207_4']";
    	public String sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_207_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_207_5']";
    	public String sinceExistingDiseaseDate_forSubQuest3_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_207_5']//following::div[1]/label/input";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements forSub Question 4 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_222_0']";
    	public String sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_222_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_222_1']";
    	public String sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_222_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_222_2']";
    	public String sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_222_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_222_3']";
    	public String sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_222_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_222_4']";
    	public String sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_222_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest4_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_222_5']";
    	public String sinceExistingDiseaseDate_forSubQuest4_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_222_5']//following::div[1]/label/input";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements
    	// forSub Question 5 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_205_0']";
    	public String sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_205_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_205_1']";
    	public String sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_205_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_205_2']";
    	public String sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_205_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_205_3']";
    	public String sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_205_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_205_4']";
    	public String sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_205_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest5_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_205_5']";
    	public String sinceExistingDiseaseDate_forSubQuest5_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_205_5']//following::div[1]/label/input";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements
    	// forSub Question 6 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_105_0']";
    	public String sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_105_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_105_1']";
    	public String sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_105_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_105_2']";
    	public String sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_105_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_105_3']";
    	public String sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_105_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_105_4']";
    	public String sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_105_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest6_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_105_5']";
    	public String sinceExistingDiseaseDate_forSubQuest6_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_105_5']//following::div[1]/label/input";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements
    	// forSub Question 7 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_164_0']";
    	public String sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_164_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_164_1']";
    	public String sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_164_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_164_2']";
    	public String sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_164_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_164_3']";
    	public String sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_164_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_164_4']";
    	public String sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_164_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest7_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_164_5']";
    	public String sinceExistingDiseaseDate_forSubQuest7_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_164_5']//following::div[1]/label/input";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements
    	// forSub Question 8 Under Question 1
    	public String sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_502_0']";
    	public String sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber1_Xpath = "//input[@id='qs_502_0']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_502_1']";
    	public String sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber2_Xpath = "//input[@id='qs_502_1']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_502_2']";
    	public String sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber3_Xpath = "//input[@id='qs_502_2']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_502_3']";
    	public String sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber4_Xpath = "//input[@id='qs_502_3']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_502_4']";
    	public String sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber5_Xpath = "//input[@id='qs_502_4']//following::div[1]/label/input";

    	public String sinceExistingDiseaseCheckBox_forSubQuest8_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_502_5']";
    	public String sinceExistingDiseaseDate_forSubQuest8_UnderQues1_ForMemeber6_Xpath = "//input[@id='qs_502_5']//following::div[1]/label/input";
    	
    	public String ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber1_Xpath = "//textarea[@id='qs_illnessDetails_0']";
    	public String ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber2_Xpath = "//textarea[@id='qs_illnessDetails_1']";
    	public String ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber3_Xpath = "//textarea[@id='qs_illnessDetails_2']";
    	public String ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber4_Xpath = "//textarea[@id='qs_illnessDetails_3']";
    	public String ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber5_Xpath = "//textarea[@id='qs_illnessDetails_4']";
    	public String ProvideDetails_TextBox_forSubQuest8_UnderQues1_ForMemeber6_Xpath = "//textarea[@id='qs_illnessDetails_5']";
    	
    	//UI Elements for Question Set 2
    	public String CheckBox_UnderQues2_ForMemeber1_Xpath = "//input[@name='qs_503_0']";
    	public String CheckBox_UnderQues2_ForMemeber2_Xpath = "//input[@name='qs_503_1']";
    	public String CheckBox_UnderQues2_ForMemeber3_Xpath = "//input[@name='qs_503_2']";
    	public String CheckBox_UnderQues2_ForMemeber4_Xpath = "//input[@name='qs_503_3']";
    	public String CheckBox_UnderQues2_ForMemeber5_Xpath = "//input[@name='qs_503_4']";
    	public String CheckBox_UnderQues2_ForMemeber6_Xpath = "//input[@name='qs_503_5']";
    	
    	// UI Elements for Question Set 3
    	public String CheckBox_UnderQues3_ForMemeber1_Xpath = "//input[@name='qs_P010_0']";
    	public String CheckBox_UnderQues3_ForMemeber2_Xpath = "//input[@name='qs_P010_1']";
    	public String CheckBox_UnderQues3_ForMemeber3_Xpath = "//input[@name='qs_P010_2']";
    	public String CheckBox_UnderQues3_ForMemeber4_Xpath = "//input[@name='qs_P010_3']";
    	public String CheckBox_UnderQues3_ForMemeber5_Xpath = "//input[@name='qs_P010_4']";
    	public String CheckBox_UnderQues3_ForMemeber6_Xpath = "//input[@name='qs_P010_5']";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question 1 Under Question 4
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber1_Xpath = "//input[@id='qs_232_0']";
        public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber2_Xpath = "//input[@id='qs_232_1']";
        public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber3_Xpath = "//input[@id='qs_232_2']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber4_Xpath = "//input[@id='qs_232_3']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber5_Xpath = "//input[@id='qs_232_4']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues4_ForMemeber6_Xpath = "//input[@id='qs_232_5']";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question 2 Under Question 4
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber1_Xpath = "//input[@id='qs_129_0']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber2_Xpath = "//input[@id='qs_129_1']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber3_Xpath = "//input[@id='qs_129_2']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber4_Xpath = "//input[@id='qs_129_3']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber5_Xpath = "//input[@id='qs_129_4']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues4_ForMemeber6_Xpath = "//input[@id='qs_129_5']";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question 3 Under Question 4
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber1_Xpath = "//input[@id='qs_147_0']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber2_Xpath = "//input[@id='qs_147_1']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber3_Xpath = "//input[@id='qs_147_2']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber4_Xpath = "//input[@id='qs_147_3']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber5_Xpath = "//input[@id='qs_147_4']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues4_ForMemeber6_Xpath = "//input[@id='qs_147_5']";
    	
    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question 1 Under Question 5
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber1_Xpath = "//input[@id='qs_210_0']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber2_Xpath = "//input[@id='qs_210_1']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber3_Xpath = "//input[@id='qs_210_2']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber4_Xpath = "//input[@id='qs_210_3']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber5_Xpath = "//input[@id='qs_210_4']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest1_UnderQues5_ForMemeber6_Xpath = "//input[@id='qs_210_5']";
    	
    	public String textAra_forSubQuest1_UnderQues5_ForMemeber1_Xpath = "//textarea[@id='qs_otherDiseasesDescription_0']";
    	public String textAra_forSubQuest1_UnderQues5_ForMemeber2_Xpath = "//textarea[@id='qs_otherDiseasesDescription_1']";
    	public String textAra_forSubQuest1_UnderQues5_ForMemeber3_Xpath = "//textarea[@id='qs_otherDiseasesDescription_2']";
    	public String textAra_forSubQuest1_UnderQues5_ForMemeber4_Xpath = "//textarea[@id='qs_otherDiseasesDescription_3']";
    	public String textAra_forSubQuest1_UnderQues5_ForMemeber5_Xpath = "//textarea[@id='qs_otherDiseasesDescription_4']";
    	public String textAra_forSubQuest1_UnderQues5_ForMemeber6_Xpath = "//textarea[@id='qs_otherDiseasesDescription_5']";
    	
    	

    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question 2 Under Question 5
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber1_Xpath = "//input[@id='qs_504_0']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber2_Xpath = "//input[@id='qs_504_1']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber3_Xpath = "//input[@id='qs_504_2']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber4_Xpath = "//input[@id='qs_504_3']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber5_Xpath = "//input[@id='qs_504_4']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest2_UnderQues5_ForMemeber6_Xpath = "//input[@id='qs_504_5']";
    	
    	public String textAra_forSubQuest2_UnderQues5_ForMemeber1_Xpath = "//textarea[@id='qs_OtherSmokeDetails_0']";
    	public String textAra_forSubQuest2_UnderQues5_ForMemeber2_Xpath = "//textarea[@id='qs_OtherSmokeDetails_1']";
    	public String textAra_forSubQuest2_UnderQues5_ForMemeber3_Xpath = "//textarea[@id='qs_OtherSmokeDetails_2']";
    	public String textAra_forSubQuest2_UnderQues5_ForMemeber4_Xpath = "//textarea[@id='qs_OtherSmokeDetails_3']";
    	public String textAra_forSubQuest2_UnderQues5_ForMemeber5_Xpath = "//textarea[@id='qs_OtherSmokeDetails_4']";
    	public String textAra_forSubQuest2_UnderQues5_ForMemeber6_Xpath = "//textarea[@id='qs_OtherSmokeDetails_5']";

    	// UI Elements of Check Box and Since Existing Disease Dates UI Elements for
    	// Sub Question 3 Under Question 5
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber1_Xpath = "//input[@id='qs_028_0']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber2_Xpath = "//input[@id='qs_028_1']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber3_Xpath = "//input[@id='qs_028_2']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber4_Xpath = "//input[@id='qs_028_3']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber5_Xpath = "//input[@id='qs_028_4']";
    	public String sinceExistingDiseaseCheckBox_forSubQuest3_UnderQues5_ForMemeber6_Xpath = "//input[@id='qs_028_5']";
    	
    	public String textAra_forSubQuest3_UnderQues5_ForMemeber1_Xpath = "//textarea[@id='qs_OtherAlcoholDetails_0']";
    	public String textAra_forSubQuest3_UnderQues5_ForMemeber2_Xpath = "//textarea[@id='qs_OtherAlcoholDetails_1']";
    	public String textAra_forSubQuest3_UnderQues5_ForMemeber3_Xpath = "//textarea[@id='qs_OtherAlcoholDetails_2']";
    	public String textAra_forSubQuest3_UnderQues5_ForMemeber4_Xpath = "//textarea[@id='qs_OtherAlcoholDetails_3']";
    	public String textAra_forSubQuest3_UnderQues5_ForMemeber5_Xpath = "//textarea[@id='qs_OtherAlcoholDetails_4']";
    	public String textAra_forSubQuest3_UnderQues5_ForMemeber6_Xpath = "//textarea[@id='qs_OtherAlcoholDetails_5']";
    	
    	// UI Elements for Question Set 6
    	public String CheckBox_UnderQues6_ForMemeber1_Xpath = "//input[@name='qs_H002_0']";
    	public String CheckBox_UnderQues6_ForMemeber2_Xpath = "//input[@name='qs_H002_1']";
    	public String CheckBox_UnderQues6_ForMemeber3_Xpath = "//input[@name='qs_H002_2']";
    	public String CheckBox_UnderQues6_ForMemeber4_Xpath = "//input[@name='qs_H002_3']";
    	public String CheckBox_UnderQues6_ForMemeber5_Xpath = "//input[@name='qs_H002_4']";
    	public String CheckBox_UnderQues6_ForMemeber6_Xpath = "//input[@name='qs_H002_5']";
    	
    	//UI Elements for Terms and Conditions 
    	public String termsAndConditions1_Xpath="//input[@id='termsCheckbox1']";
    	public String termsAndConditions2_Xpath="//input[@id='termsCheckbox2']";
    	public String termsAndConditions3_Xpath="//input[@id='termsCheckbox3']";
    	public String termsAndConditions4_Xpath="//input[@id='alertCheck']";
    	
    	//UI Elements Payment Page
    	public String payOnlineButton_Xpath="//a[@ng-click='payOnlinePayment(summaryObj);']";
    	public String cardNumber_Xpath="//input[@id='ccard_number']";
    	public String nameOnCard_Xpath="//input[@id='cname_on_card']";
    	public String cvvNumber_Xpath="//input[@id='ccvv_number']";
    	public String expirayMonth_Xpath="cexpiry_date_month";
    	public String expirayYear_Xpath="cexpiry_date_year";
    	public String PayNowbutton_CreditCard_Xpath="//div[contains(@class,'payment-buttons')]//input[@class=' credit_pay_button']";
    	public String manageThisCardString_Xpath="//a[@id='manageCardLink']";
    
    	
    	public String Enter_ValidEmail_Error_Xpath = "//span[@class='error-message'][contains(text(),'Please enter valid Email Id')]";
    	public String Enter_Emailid_Error_xpath = "//span[@class='error-message ng-hide'][contains(text(),'Please enter Email Id')]";
    	
    	//Verify Policy details
    	public String viewProposalsDashborad_Xpath="//button[@ng-click='getAllProposalsDetails()']";
    	
    	public String fromDateViewPropsal_Xpath="//input[@id='from_date' and @placeholder='DD/MM/YYYY']";
    	public String toDateViewPropsal_Xpath="//input[@id='to_date' and @placeholder='DD/MM/YYYY']";
    	public String SearchDropDown_Xpath="//div[contains(@class,'tax_description_drop_search_body padding0')]//ul[contains(@class,'month_year_by')]//preceding-sibling::a";
            
   //xpath list edit secure
    public String Edit_QuotationPage_premium_xpath	="//span[@ng-if='iconloading==false'] ";
    	
    //Edit xpath list for Explore
    public String Exploreedit_xpath="//button[@class='quot_edit_btn']";
	public String Edit_TravellingAllDropdowns_xpath="//div[@class='col-md-6 padding_left0 total_right_border explore_class']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']";
   
        
     //pos Explore xpath list
	public static String POSExplore_xpath="//*[@id='navbar']/ul/li[1]/ul/li/a[contains(text(),'POS Explore')]";
    	public static String pos_buy_now_xpath="//div[@class='col-md-12 padding0 tr_no_btn text-center']/button[contains(text(),'Buy Now')]";
    	 public String POSExploreedit_xpath="//button[@class='quot_edit_btn']";
    	
//POS student explore xpath list
    	 
    	 public static String posstudentExplore_xpath="//*[@id='navbar']/ul/li[1]/ul/li/a[contains(text(),'POS Student Explore')]";
    	 public static String Optional_cover_xpath="//input[@name='optional_cover']";
    	public static String optionalcover1_xpath="//input[@name='opt_cvr_SII']";
    	public static String optionalcover2_xpath="//input[@name='opt_cvr_VC']"; 
    	public static String optionalcover3_xpath="//input[@name='opt_cvr_ASI']";
    	public static String optionalcover4_xpath="//input[@name='opt_cvr_HIV']";
    	public static String optionalcover5_xpath="//input[@name='opt_cvr_MNBC']";
    	public static String optionalcover6_xpath="//input[@name='opt_cvr_PEDC']";
    	public static String posStudent_buynow_xpath="//button[@ng-click='buyNow();']";
    	
    	public static String travelingto_xpath="//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a";
    	public static String plantypeEdit_xpath="//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/div[1]/div/ui-dropdown/div/div/a";
    	public static String policy_tenure_xpath="//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/div[2]/div/ui-dropdown/div/div/a";
    	public static String Edit_Update_premium_button_xpath="//*[@class='share_qution_btn padding-top-15']/button[contains(text(),'Update Premium')]";
    	 
    	 
    	 
    	 //Super Mediclaim Heart xpath list

    	 public static String supermediclaimnew_xpath="//*[@class='dropdown-menu dropdown_menu_focus health_insurance']/li/a/p[contains(text(),'Super Mediclaim')]";
    	 public static String Heart_xpath="//*[@id='navbar']/ul/li[2]/ul/li[2]/ul/li/ul/li/a[contains(text(),'Heart')]";


    	 //Operation XpathList

    	 public static String Operation_xpath="//*[@id='navbar']/ul/li[2]/ul/li[2]/ul/li/ul/li/a[contains(text(),'Operation')]";

    	//**************************Group STudent Explore Properties*************************************************
    	 public static String groupstudentexplore_xpath="//ul[@class='dropdown-menu dropdown_menu_focus travel_insurance show']/li/a[contains(text(),'Group Student Explore')]";

    	//**************************Domestic Travel Properties********************************************************

    	 public static String Domestic_Travel_xpath="//ul[@class='dropdown-menu dropdown_menu_focus travel_insurance show']/li/a[contains(text(),'Domestic Travel')]";
    	 public String DomestiTravelExpectedMessage_xpath="/html/body/div[2]/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p";
    	 public String PolProp2_xpath="/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]";

    	 //***************************** Pos care Senior ****************************
    	 public static String poscareSenior_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[2]/ul/li[3]/a";
    	 
    	//++++++++++++++++++++++++++++POS smart Selct Properties++++++++++++++++++++++++++++++++++++++++++++++++++++
    	 public static String posSmartSelct_xpath="//*[@id='navbar']/ul/li[2]/ul/li[1]/ul/li[2]/ul/li[4]/a"; 
    	 public String checkbox_forcare_xpath="//label[@for='premiumRadio0q']//b/span[contains(text(),'for care')]";
    	 public String Checkbox_carewithNCBSuper_xpath="//label[@for='premiumRadio1q']/b/span[contains(text(),'for care with NCB Super')]";

         public String Close_VideoPopup_Xpath="//button[text()='Close']";

}





			





		
		

