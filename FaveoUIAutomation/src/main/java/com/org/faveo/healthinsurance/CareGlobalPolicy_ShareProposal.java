package com.org.faveo.healthinsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareGlobalPolicy_ShareProposal extends BaseClass implements AccountnSettingsInterface {
	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static Integer n;

	@Test(enabled=false, priority=1)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData = BaseClass.excel_Files("CareGlobal_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareGlobal_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("CareGlobal_QuestionSet");
		String[][] editcarefredom = BaseClass.excel_Files("CareFredom_Edit");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 4; n <= 6; n++) {
			try {

				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Global - " + TestCaseName);
				System.out.println("Care Global - " + TestCaseName);

				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				
				BaseClass.LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));


				// Clicking on CAREHNI Product dropdown
				HealthInsuranceDropDown.CareGlobalPolicy();

				// ===============================================QuotationPage========================================================

				// Reading Proposer Name from Excel
				String Name = TestCaseData[n][2].toString().trim();
				enterText(By.xpath(Textbox_Name_xpath), Name);
				System.out.println("Proposer Name is : " + Name);
				// logger.log(LogStatus.PASS, "Entered UserName is " + " - " +
				// Name);

				// Reading Emailfrom Excel Sheet
				driver.findElement(By.name("ValidEmail")).clear();
				String Email = TestCaseData[n][3].toString().trim();
				System.out.println("Email of Proposer is :" + Email);
				enterText(By.xpath(Textbox_Email_xpath), Email);

				// Reading Mobile Number from Excel

				String MobileNumber = TestCaseData[n][4].toString().trim();
				int size = MobileNumber.length();
				if (isValid(MobileNumber)) {
					System.out.println("Mobile number is: " + MobileNumber + "Is a alid number");
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
				} else {
					System.out.println("Not a valid Number");
				}

				// Enter The Value of Total members present in policy
				Thread.sleep(10000);
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						DropDownName.click();
						if (DropDownName.getText().equals("1")) {
							driver.findElement(
									By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["
											+ TestCaseData[n][5].toString().trim() + "]"))
									.click();
							Thread.sleep(10000);
							break;
						}
					}
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}

				// again call the dropdown
				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;
				System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3
				// dropdowns
				outer:

				for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][6];
						System.out.println("Total Number of Member in Excel :" + Membersdetails);
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member: for (int i = 0; i <= BaseClass.membres.length; i++) {

								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;
								System.out.println("Mcount Index : " + mcountindex);

								driver.findElement(By
										.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
										.click();

								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("List Data is :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("18 - 24 years")) {

								int Children = Integer.parseInt(TestCaseData[n][7].toString().trim());
								clickElement(
										By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String Membersdetails = TestCaseData[n][6];
								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");
								} else {
									// BaseClass.membres =
									// Membersdetails.split(" ");
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
									// System.out.println("Mdeatils is :
									// "+membres);

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();

									// List Age of members dropdown
									Thread.sleep(5000);
									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(3000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				}

				Thread.sleep(3000);
				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][8].toString().trim());
				System.out.println("Selected SumInsured is : " + SumInsured);
				clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
				// " + SumInsured + ")]"));

				// Reading the value of Tenure from Excel
				int Tenure = Integer.parseInt(TestCaseData[n][9].toString().trim());
				System.out.println("Selected Tenure is : " + Tenure);
				clickbyHover(By.xpath(Radio_Tenure3_xpath));
				Thread.sleep(5000);
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					// clickElement(By.xpath(Radio_Tenure1_xpath));
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					// clickElement(By.xpath(Radio_Tenure2_xpath));
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					// clickElement(By.xpath(Radio_Tenure3_xpath));
				}

				// Scroll Window Up
				BaseClass.scrollup();

				// Reading the value of Addons from Excel
				String Addons = TestCaseData[n][10].toString().trim();
				System.out.println("Selected Addon is : " + Addons);
				Thread.sleep(5000);
				clickbyHover(By.xpath("//b[@class='ng-binding'][contains(text()," + "'" + Addons + "'" + ")]"));
				// driver.findElement(By.xpath("//b[@class='ng-binding'][contains(text(),"+"'"+Addons+"'"+")]")).click();
				logger.log(LogStatus.PASS, "User has Selected : " + Addons + " Addon");

				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));

				// ===============================================Proposer Detail Page========================================================

				Thread.sleep(5000);
				// Reading Proposer Title from Excel
				waitForElement(By.name("ValidTitle"));
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				// clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);
				BaseClass.selecttext("ValidTitle", Title.toString());

				Thread.sleep(5000);
				// Entering DOB from Excel into dob field
				String DOB = TestCaseData[n][11].toString().trim();
				System.out.println("date is:" + DOB);
				clickElement(By.id(Dob_Proposer_id));
				enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));

				// Reading AddressLine 1 from Excel
				Thread.sleep(3000);
				String address1 = TestCaseData[n][12].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				enterText(By.xpath(Textbox_AddressLine1_xpath), address1);

				// Reading AddressLine 2 from Excel
				String address2 = TestCaseData[n][13].toString().trim();
				System.out.println(address2);
				enterText(By.xpath(Textbox_AddressLine2_xpath), address2);

				// Reading Pincode from Excel
				int Pincode = Integer.parseInt(TestCaseData[n][14].toString().trim());
				System.out.println(Pincode);
				enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
				// logger.log(LogStatus.PASS, "Entered Pincode is " + " - " +
				// Pincode);

				// Reading Proposer Height in Feet from Excel
				String HeightProposer_Feet = TestCaseData[n][15].toString().trim();
				System.out.println("Height value from excel  is:" + HeightProposer_Feet);
				clickElement(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath(
						"//option[@ng-repeat='data in HEIGHT_FEET'][contains(text()," + HeightProposer_Feet + ")]"));

				// Reading Proposer Height in Inch from Excel
				String HeightProposer_Inch = TestCaseData[n][16].toString().trim();
				System.out.println("Inch value from excel  is:" + HeightProposer_Inch);
				clickElement(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath(
						"//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text()," + HeightProposer_Inch + ")]"));

				// Reading Weight of Proposer from Excel
				String Weight = TestCaseData[n][17].toString().trim();
				System.out.println("Weight is :" + Weight);
				enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));

				Thread.sleep(2000);
				// Reading Nominee Name from Excel
				String NomineeName = TestCaseData[n][18].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);

				Thread.sleep(2000);
				// Nominee Relation
				String NomineeRelation = TestCaseData[n][19].toString().trim();
				System.out.println("Nominee  relation from excel  is:" + NomineeRelation);
				clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"
						+ NomineeRelation + "'" + ")]"));

				// String pancard=TestCaseData[n][19].toString().trim();
				String PanCard = TestCaseData[n][20].toString().trim();
				System.out.println("pancard number is :" + PanCard);
				Boolean PanCardNumberPresence = driver
						.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"))
						.size() > 0;
				if (PanCardNumberPresence == true) {
					enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

				} else {
					System.out.println("PAN Card Field is not Present");
				}

				// Click on Next button
				try {
					Thread.sleep(5000);
					waitTillElementToBeClickable(By.id(Button_NextProposer_id));
					clickElement(By.id(Button_NextProposer_id));
					System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
					System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
				}

				// ===============================================Insured Details Page========================================================
				Thread.sleep(5000);

				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {
						// String firstName= "fname"+i+"_xpath";
						String Date = FamilyData[mcount][5].toString().trim();
						// String Date= FamilyData[mcount][5].toString().trim();
						BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());

						BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
						enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());

						enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
						clickElement(By.name("rel_dob" + i));

						enterText(By.name("rel_dob" + i), String.valueOf(Date));
						BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());

						BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
						enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

					}
				}

				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForCareGlobal(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();

				// ===============================================HEALTH QUESTIONNAIRE Page========================================================

				// Health Questionnarire Elements
				String preExistingdeases = TestCaseData[n][21].toString().trim();
				Thread.sleep(5000);
				System.out.println(
						"Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				BaseClass.scrollup();
				try {
					if (preExistingdeases.contains("YES")) {

						waitForElements(By.xpath(YesButton_xpath));
						clickElement(By.xpath(YesButton_xpath));
						// Thread.sleep(2000);
						String years = null;
						String Details = null;
						for (int qlist = 1; qlist <= 13; qlist++) {
							Details = QuestionSetData[n][qlist + (qlist - 1)].toString().trim();
							years = QuestionSetData[n][qlist + qlist].toString().trim();
							if (Details.equals("")) {
								// break;
							} else {
								int detailsnumber = Integer.parseInt(Details);

								// Will click on check box and select the month
								// & year u
								detailsnumber = detailsnumber + 1;
								System.out.println("Details and years are :" + Details + "----" + years);
								clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
										+ detailsnumber + "]//input[@type='checkbox']"));
								Thread.sleep(1000);
								try {
									clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label"));
									enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label"), years);
								} catch (Exception e) {
									clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label[@class='monthYear']"));
									enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label[@class='monthYear']"), years);
								}
							}
						}
					} else if (preExistingdeases.contains("NO")) {
						clickElement(By.xpath(NoButton_xpath));
					}
				} catch (Exception e) {
					System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
				}

				String ChecksData = null;
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 3; morechecks++) {
					int mch = morechecks + 1;
					ChecksData = TestCaseData[n][21 + morechecks].toString().trim();
					if (ChecksData.equalsIgnoreCase("NO")) {
						System.out.println("Quatation set to NO");
						driver.findElement(By.xpath("//label[@for='question_" + mch + "_no']")).click();
					} else if (ChecksData.equals("YES") || ChecksData.equalsIgnoreCase("Yes")) {
						System.out.println("Quatation set to only YES");
						driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
					} else {
						driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
						if (ChecksData.contains(",")) {
							ChckData = ChecksData.split(",");
							for (String Chdata : ChckData) {
								datacheck = Integer.parseInt(Chdata);
								datacheck = datacheck - 1;
								driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
										.click();

							}
						} else {
							datacheck = Integer.parseInt(ChecksData);
							datacheck = datacheck - 1;
							driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
									.click();
						}
					}

				}

				BaseClass.HelathQuestionnairecheckbox();

				try {
					clickElement(By.xpath("//button[text()='Proceed to Pay']"));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
					System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
				}

				BaseClass.ErroronHelathquestionnaire();
				waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
				clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));

				BaseClass.PayuPage_Credentials();

				driver.quit();

				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareGlobal(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				driver.quit();

			} catch (Exception e) {
				// WriteExcel.setCellData("Test_Cases_CareGlobal", "Fail", n,
				// 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.quit();
			}
			continue;
		}
	}
	
	//***************** Share Proposal 2 ************************************************************
	
	@Test(enabled=true, priority=2)
	public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData = BaseClass.excel_Files("CareGlobal_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareGlobal_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("CareGlobal_QuestionSet");
		String[][] editcarefredom = BaseClass.excel_Files("CareFredom_Edit");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 4; n <= 6; n++) {
			try {

				String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
				logger = extent.startTest("Care Global - " + TestCaseName);
				System.out.println("Care Global - " + TestCaseName);

				//Step 1 - Open Email and Delete Old Email
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
			
				//Step 2 - Go to Application and share a proposal
				System.out.println("Step 2 - Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
                clickElement(By.xpath(Close_VideoPopup_Xpath));

				// Clicking on CAREHNI Product dropdown
				HealthInsuranceDropDown.CareGlobalPolicy();

				// ===============================================QuotationPage========================================================

				// Reading Proposer Name from Excel
				String Name = TestCaseData[n][2].toString().trim();
				enterText(By.xpath(Textbox_Name_xpath), Name);
				System.out.println("Proposer Name is : " + Name);
				// logger.log(LogStatus.PASS, "Entered UserName is " + " - " +
				// Name);

				// Reading Emailfrom Excel Sheet
				driver.findElement(By.name("ValidEmail")).clear();
				String Email = TestCaseData[n][3].toString().trim();
				System.out.println("Email of Proposer is :" + Email);
				enterText(By.xpath(Textbox_Email_xpath), Email);

				// Reading Mobile Number from Excel

				String MobileNumber = TestCaseData[n][4].toString().trim();
				int size = MobileNumber.length();
				if (isValid(MobileNumber)) {
					System.out.println("Mobile number is: " + MobileNumber + "Is a alid number");
					enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
				} else {
					System.out.println("Not a valid Number");
				}

				// Enter The Value of Total members present in policy
				Thread.sleep(10000);
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						DropDownName.click();
						if (DropDownName.getText().equals("1")) {
							driver.findElement(
									By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["
											+ TestCaseData[n][5].toString().trim() + "]"))
									.click();
							Thread.sleep(10000);
							break;
						}
					}
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
					BaseClass.AbacusURL();
				}

				// again call the dropdown
				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;
				System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3
				// dropdowns
				outer:

				for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][6];
						System.out.println("Total Number of Member in Excel :" + Membersdetails);
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member: for (int i = 0; i <= BaseClass.membres.length; i++) {

								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;
								System.out.println("Mcount Index : " + mcountindex);

								driver.findElement(By
										.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
										.click();

								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("List Data is :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("18 - 24 years")) {

								int Children = Integer.parseInt(TestCaseData[n][7].toString().trim());
								clickElement(
										By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String Membersdetails = TestCaseData[n][6];
								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");
								} else {
									// BaseClass.membres =
									// Membersdetails.split(" ");
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
									// System.out.println("Mdeatils is :
									// "+membres);

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();

									// List Age of members dropdown
									Thread.sleep(5000);
									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(3000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				}

				Thread.sleep(3000);
				// Read the Value of Suminsured
				int SumInsured = Integer.parseInt(TestCaseData[n][8].toString().trim());
				System.out.println("Selected SumInsured is : " + SumInsured);
				clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
				clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
				// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
				// " + SumInsured + ")]"));

				// Reading the value of Tenure from Excel
				int Tenure = Integer.parseInt(TestCaseData[n][9].toString().trim());
				System.out.println("Selected Tenure is : " + Tenure);
				clickbyHover(By.xpath(Radio_Tenure3_xpath));
				Thread.sleep(5000);
				if (Tenure == 1) {
					clickbyHover(By.xpath(Radio_Tenure1_xpath));
					// clickElement(By.xpath(Radio_Tenure1_xpath));
				} else if (Tenure == 2) {
					clickbyHover(By.xpath(Radio_Tenure2_xpath));
					// clickElement(By.xpath(Radio_Tenure2_xpath));
				} else if (Tenure == 3) {
					clickbyHover(By.xpath(Radio_Tenure3_xpath));
					// clickElement(By.xpath(Radio_Tenure3_xpath));
				}

				// Scroll Window Up
				BaseClass.scrollup();

				// Reading the value of Addons from Excel
				String Addons = TestCaseData[n][10].toString().trim();
				System.out.println("Selected Addon is : " + Addons);
				Thread.sleep(5000);
				clickbyHover(By.xpath("//b[@class='ng-binding'][contains(text()," + "'" + Addons + "'" + ")]"));
				// driver.findElement(By.xpath("//b[@class='ng-binding'][contains(text(),"+"'"+Addons+"'"+")]")).click();
				logger.log(LogStatus.PASS, "User has Selected : " + Addons + " Addon");

				
				AddonsforProducts.readPremiumFromFirstPage();
               // ReadDataFromEmail.readAgeGroup(); 
				
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));

				// ===============================================Proposer Detail Page========================================================

				Thread.sleep(5000);
				// Reading Proposer Title from Excel
				waitForElement(By.name("ValidTitle"));
				String Title = TestCaseData[n][1].toString().trim();
				System.out.println("Titel Name is:" + Title);
				// clickElement(By.name("ValidTitle"));
				logger.log(LogStatus.PASS, "Selected Title  is :" + Title);
				BaseClass.selecttext("ValidTitle", Title.toString());

				Thread.sleep(5000);
				// Entering DOB from Excel into dob field
				String DOB = TestCaseData[n][11].toString().trim();
				System.out.println("date is:" + DOB);
				clickElement(By.id(Dob_Proposer_id));
				enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));

				// Reading AddressLine 1 from Excel
				Thread.sleep(3000);
				String address1 = TestCaseData[n][12].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				enterText(By.xpath(Textbox_AddressLine1_xpath), address1);

				// Reading AddressLine 2 from Excel
				String address2 = TestCaseData[n][13].toString().trim();
				System.out.println(address2);
				enterText(By.xpath(Textbox_AddressLine2_xpath), address2);

				// Reading Pincode from Excel
				int Pincode = Integer.parseInt(TestCaseData[n][14].toString().trim());
				System.out.println(Pincode);
				enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
				// logger.log(LogStatus.PASS, "Entered Pincode is " + " - " +
				// Pincode);

				// Reading Proposer Height in Feet from Excel
				String HeightProposer_Feet = TestCaseData[n][15].toString().trim();
				System.out.println("Height value from excel  is:" + HeightProposer_Feet);
				clickElement(By.xpath(DropDown_HeightFeet_xpath));
				clickElement(By.xpath(
						"//option[@ng-repeat='data in HEIGHT_FEET'][contains(text()," + HeightProposer_Feet + ")]"));

				// Reading Proposer Height in Inch from Excel
				String HeightProposer_Inch = TestCaseData[n][16].toString().trim();
				System.out.println("Inch value from excel  is:" + HeightProposer_Inch);
				clickElement(By.xpath(DropDown_HeightInch_xpath));
				clickElement(By.xpath(
						"//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text()," + HeightProposer_Inch + ")]"));

				// Reading Weight of Proposer from Excel
				String Weight = TestCaseData[n][17].toString().trim();
				System.out.println("Weight is :" + Weight);
				enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));

				Thread.sleep(2000);
				// Reading Nominee Name from Excel
				String NomineeName = TestCaseData[n][18].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);

				Thread.sleep(2000);
				// Nominee Relation
				String NomineeRelation = TestCaseData[n][19].toString().trim();
				System.out.println("Nominee  relation from excel  is:" + NomineeRelation);
				clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
				clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"
						+ NomineeRelation + "'" + ")]"));

				// String pancard=TestCaseData[n][19].toString().trim();
				String PanCard = TestCaseData[n][20].toString().trim();
				System.out.println("pancard number is :" + PanCard);
				Boolean PanCardNumberPresence = driver
						.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"))
						.size() > 0;
				if (PanCardNumberPresence == true) {
					enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

				} else {
					System.out.println("PAN Card Field is not Present");
				}

				// Click on Next button
				try {
					Thread.sleep(5000);
					waitTillElementToBeClickable(By.id(Button_NextProposer_id));
					clickElement(By.id(Button_NextProposer_id));
					System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
					System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
				}
                         
				// ===============================================Insured Details Page========================================================
				Thread.sleep(5000);

				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {
						clickElement(By.xpath(title1_xpath));
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {
						// String firstName= "fname"+i+"_xpath";
						String Date = FamilyData[mcount][5].toString().trim();
						// String Date= FamilyData[mcount][5].toString().trim();
						BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());

						BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
						enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());

						enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
						clickElement(By.name("rel_dob" + i));

						enterText(By.name("rel_dob" + i), String.valueOf(Date));
						BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());

						BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
						enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

					}
				}

				driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
				System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
				
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				driver.quit();
				                          
				//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
				System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				
			    LaunchBrowser();
			    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForCareGlobal(n);
			    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Step 4 - Click on Buy Now button from Email Body and punch the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
				
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
				DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();

				// ===============================================HEALTH QUESTIONNAIRE Page========================================================

				// Health Questionnarire Elements
				String preExistingdeases = TestCaseData[n][21].toString().trim();
				Thread.sleep(5000);
				System.out.println(
						"Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				BaseClass.scrollup();
				try {
					if (preExistingdeases.contains("YES")) {

						waitForElements(By.xpath(YesButton_xpath));
						clickElement(By.xpath(YesButton_xpath));
						// Thread.sleep(2000);
						String years = null;
						String Details = null;
						for (int qlist = 1; qlist <= 13; qlist++) {
							Details = QuestionSetData[n][qlist + (qlist - 1)].toString().trim();
							years = QuestionSetData[n][qlist + qlist].toString().trim();
							if (Details.equals("")) {
								// break;
							} else {
								int detailsnumber = Integer.parseInt(Details);

								// Will click on check box and select the month
								// & year u
								detailsnumber = detailsnumber + 1;
								System.out.println("Details and years are :" + Details + "----" + years);
								clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
										+ detailsnumber + "]//input[@type='checkbox']"));
								Thread.sleep(1000);
								try {
									clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label"));
									enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label"), years);
								} catch (Exception e) {
									clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label[@class='monthYear']"));
									enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
											+ detailsnumber + "]//label[@class='monthYear']"), years);
								}
							}
						}
					} else if (preExistingdeases.contains("NO")) {
						clickElement(By.xpath(NoButton_xpath));
					}
				} catch (Exception e) {
					System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
				}

				String ChecksData = null;
				String[] ChckData = null;
				int datacheck = 0;
				for (int morechecks = 1; morechecks <= 3; morechecks++) {
					int mch = morechecks + 1;
					ChecksData = TestCaseData[n][21 + morechecks].toString().trim();
					if (ChecksData.equalsIgnoreCase("NO")) {
						System.out.println("Quatation set to NO");
						driver.findElement(By.xpath("//label[@for='question_" + mch + "_no']")).click();
					} else if (ChecksData.equals("YES") || ChecksData.equalsIgnoreCase("Yes")) {
						System.out.println("Quatation set to only YES");
						driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
					} else {
						driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
						if (ChecksData.contains(",")) {
							ChckData = ChecksData.split(",");
							for (String Chdata : ChckData) {
								datacheck = Integer.parseInt(Chdata);
								datacheck = datacheck - 1;
								driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
										.click();

							}
						} else {
							datacheck = Integer.parseInt(ChecksData);
							datacheck = datacheck - 1;
							driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
									.click();
						}
					}

				}

				BaseClass.HelathQuestionnairecheckbox();

				try {
					clickElement(By.xpath("//button[text()='Proceed to Pay']"));
					System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
					System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
				}

				BaseClass.ErroronHelathquestionnaire();
				waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
				clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));

				BaseClass.PayuPage_Credentials();

				//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareGlobal(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				 driver.quit();  


			} catch (Exception e) {
				// WriteExcel.setCellData("Test_Cases_CareGlobal", "Fail", n,
				// 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.quit();
			}
			continue;
		}
	}
}