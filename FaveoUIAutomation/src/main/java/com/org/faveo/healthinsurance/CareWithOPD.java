package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.CarewithOPDPageJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareWithOPD extends BaseClass implements AccountnSettingsInterface {
public static Integer n;
	
    @Test(priority=1, enabled=false)
	public static void CareWithOPDJourney() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("CareWithOPD_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("CareWithOPD_Quotation");
		String[][] OPDEdit=BaseClass.excel_Files("CareWithOPD_Testcase");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("CareWithOPD_Testcase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for(n=1;n<=rowCount;n++) {
			try{
			BaseClass.LaunchBrowser();
			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
			System.out.println("Care Senior -" +TestCaseName);
			LoginCase.LoginwithValidCredendial();
			HealthInsuranceDropDown.careWithOPDselection();
			CarewithOPDPageJourney.careWithOPDquotation();
			CarewithOPDPageJourney.careWithOPDDropdown();
			CarewithOPDPageJourney.suminsuredopd();
			String ExecutionStatus=OPDEdit[n][4].toString().trim();
			System.out.println("Execution Status is :"+ExecutionStatus);
			if(ExecutionStatus.equals("Normal")) {
				CarewithOPDPageJourney.primiumwithoutEdit();
			}else {
				Edit.CareOPDEdit();
			}
			CarewithOPDPageJourney.carewithOPDproposerDetailspage();
			CarewithOPDPageJourney.carewithOPDinsuredDetailsPage();
			CarewithOPDPageJourney.carewithOPDquestionset();
			CarewithOPDPageJourney.proposalSummaryCarewithOPD();
			
			WriteExcel.setCellData("CareWithOPD_Testcase", "PASS", n, 3);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
			
			}catch(Exception e){
				WriteExcel.setCellData("CareWithOPD_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				 driver.quit();
			}
			continue;
		}
	}
	
	//*************************** Share Quote ************************************
	@Test(priority=2, enabled=false)
	public static void CareWithOPDJourney_ShareQuote() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("ShareQuotation");
		String[][] TestCaseData=BaseClass.excel_Files("CareWithOPD_Quotation");
		String[][] OPDEdit=BaseClass.excel_Files("CareWithOPD_Testcase");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for(n=1;n<=6;n++) {
			try{
			
			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("Care OPD test case name is  -" + TestCaseName);
			System.out.println("Care Senior -" +TestCaseName);
			
			// Step 1 - Open Email and Delete all Old Emails
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			driver.quit();
			
			
			// Step 2 - Go to Application and share a Quote and Verify Data
			// in Quotation Tracker
			System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
			logger.log(LogStatus.PASS,
					"Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
			LaunchBrowser();
			LoginCase.LoginwithValidCredendial();
			HealthInsuranceDropDown.careWithOPDselection();
			CarewithOPDPageJourney.careWithOPDquotation();
			CarewithOPDPageJourney.careWithOPDDropdown();
			CarewithOPDPageJourney.suminsuredopd();
			
			AddonsforProducts.readPremiumFromFirstPage();
			//ReadDataFromEmail.readAgeGroup();

			DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
			DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForCareOPD(n);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			driver.quit();
			
			// Step 3 - Open Email and Verify Data in Email Body
			System.out.println(" Open Email and Verify Data in Email Body");
			logger.log(LogStatus.PASS, " Open Email and Verify Data in Email Body");
			LaunchBrowser();
			DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForCareOPD(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			
			// Step 4 - Click on Buy Now Button from Email Body and punch
			// the Policy
			System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
			logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
			
			CarewithOPDPageJourney.carewithOPDproposerDetailspage();
			CarewithOPDPageJourney.carewithOPDinsuredDetailsPage();
			CarewithOPDPageJourney.carewithOPDquestionset();
			
			try {
				(new WebDriverWait(driver, 40)).until(ExpectedConditions
						.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.out.println("unable to load psoposal summary payment method page :");
			}

			PayuPage_Credentials();

			driver.quit();
			
			// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
			System.out.println(
					" Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
			logger.log(LogStatus.PASS,
					" Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
			LaunchBrowser();
			// Login with the help of Login Case class
			LoginCase.LoginwithValidCredendial();
			DataVerificationShareQuotationPage
					.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareOPD(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			driver.quit();

			// Step 6- Again Open the Email and Verify GUID Should be
			// Reusable
			System.out.println("Again Open the Email and Verify GUID Should be Reusable");
			logger.log(LogStatus.PASS, "Again Open the Email and Verify GUID Should be Reusable");

			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

			// Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			
			
			//WriteExcel.setCellData("CareWithOPD_Testcase", "PASS", n, 3);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
			
			driver.quit();
			}
			catch(AssertionError e){
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				 driver.quit();
			}
			
			catch(Exception e){
				//WriteExcel.setCellData("CareWithOPD_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				 driver.quit();
				
			}
			
			continue;
		}
	}
	
	
	//*************************** Share Proposal Test Case 1 ************************************
		@Test(priority=3, enabled=false)
		public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
			String[][] TestCase=BaseClass.excel_Files("ShareProposal");
			String[][] TestCaseData=BaseClass.excel_Files("CareWithOPD_Quotation");
			String[][] OPDEdit=BaseClass.excel_Files("CareWithOPD_Testcase");
			
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("ShareProposal");
			System.out.println("Total Number of Row in Sheet : "+rowCount);
			
			for(n=1; n<=6; n++) {
				try{
				
				String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
				logger = extent.startTest("Care OPD test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" +TestCaseName);
				
				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				HealthInsuranceDropDown.careWithOPDselection();
				CarewithOPDPageJourney.careWithOPDquotation();
				CarewithOPDPageJourney.careWithOPDDropdown();
				CarewithOPDPageJourney.suminsuredopd();
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				
				
				CarewithOPDPageJourney.carewithOPDproposerDetailspage();
				CarewithOPDPageJourney.carewithOPDinsuredDetailsPage();
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForCareWithOPD(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
				Thread.sleep(3000);
				
				CarewithOPDPageJourney.carewithOPDquestionset();
				
				try {
					(new WebDriverWait(driver, 40)).until(ExpectedConditions
							.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
							.click();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println("unable to load psoposal summary payment method page :");
				}

				PayuPage_Credentials();

				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareFreedom(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				driver.quit();
				
				
				}
				catch(AssertionError e){
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					 driver.quit();
				}
				
				catch(Exception e){
					//WriteExcel.setCellData("CareWithOPD_Testcase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					driver.quit();
					
				}
				
				continue;
			}
		}
		
		
		//*************************** Share Proposal Test Case 1 ************************************
		@Test(priority=3, enabled=true)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
			String[][] TestCase=BaseClass.excel_Files("ShareProposal");
			String[][] TestCaseData=BaseClass.excel_Files("CareWithOPD_Quotation");
			String[][] OPDEdit=BaseClass.excel_Files("CareWithOPD_Testcase");
			
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("ShareProposal");
			System.out.println("Total Number of Row in Sheet : "+rowCount);
			
			for(n=5; n<=5; n++) {
				try{
				
				String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
				logger = extent.startTest("Care OPD test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" +TestCaseName);
				
				//Step 1 - Open Email and Delete Old Email
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a proposal
				System.out.println("Step 2 - Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
				
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				HealthInsuranceDropDown.careWithOPDselection();
				CarewithOPDPageJourney.careWithOPDquotation();
				CarewithOPDPageJourney.careWithOPDDropdown();
				CarewithOPDPageJourney.suminsuredopd();
				Thread.sleep(3000);
				
				//Read Premium and Age Group
				AddonsforProducts.readPremiumFromFirstPage();
				
				String[][] TestCaseData1 = BaseClass.excel_Files("CareWithOPD_Quotation");
				//Read Age Group Based on Cover Type
				String CoverType = TestCaseData1[n][8].toString().trim();
				if(CoverType.equalsIgnoreCase("Individual")){
                ReadDataFromEmail.readAgeGroup(); 
				}
				else{
					System.out.println("Age Group Value not Read");
				}
				
				clickElement(By.xpath(Button_Buynow_xpath));
				
				
				CarewithOPDPageJourney.carewithOPDproposerDetailspage();
				CarewithOPDPageJourney.carewithOPDinsuredDetailsPage();
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				driver.quit();
				
				//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
				System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				
			    LaunchBrowser();
			    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForCareWithOPD(n);
			    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Step 4 - Click on Buy Now button from Email Body and punch the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
				
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
				DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
				
				CarewithOPDPageJourney.carewithOPDquestionset();
				
				try {
					(new WebDriverWait(driver, 40)).until(ExpectedConditions
							.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
							.click();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println("unable to load psoposal summary payment method page :");
				}

				PayuPage_Credentials();

				driver.quit();
				
				//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSCareFreedom(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				driver.quit();
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				 driver.quit();  
				
				}
				catch(AssertionError e){
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					 driver.quit();
				}
				
				catch(Exception e){
					//WriteExcel.setCellData("CareWithOPD_Testcase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);
					driver.quit();
					
				}
				
				continue;
			}
		}
}
