package com.org.faveo.healthinsurance;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.CareNCBPageJourney;
import com.org.faveo.PolicyJourney.PosCareFreedomPageJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.login.LogoutFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class PosCareFreedomPolicy_ShareQuote extends BaseClass implements AccountnSettingsInterface {

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static Integer n;

	@Test
	public static void PosCareFreedom() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Test_Case_PosFreedom");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n =2; n <= 2; n++) {
			try {

				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Pos Care Freedom -" + TestCaseName);
				System.out.println("Pos Care Freedom -" + TestCaseName);
				
				//Step 1 - Open Email and Delete any Old Email 
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
	            LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
				System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
				logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				// Clicking on CAREHNI Product dropdown
				HealthInsuranceDropDown.PosCareFreedom();
				
				DataVerificationShareQuotationPage.QuotationPageJourney_POSCAREFreedom(n);
				
				AddonsforProducts.readPremiumFromFirstPage();
                ReadDataFromEmail.readAgeGroup(); 
      
                DataVerificationShareQuotationPage.clickOnShareQuotationButton();
			    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForPOSCareFreedom(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		        driver.quit();
		        
		        
		     // Step 3 - Open Email and Verify Data in Email Body
				System.out.println("Step 3 -  Open Email and Verify Data in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForPOSCareFreedom(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
				System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
				
				PosCareFreedomPageJourney.ProposalPageJourney(n);
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				//PosCareFreedomPageJourney.ProposalSummaryPage_ShareQuote("Test_Case_PosFreedom",n);
				BaseClass.PayuPage_Credentials();
				driver.quit();
				
				 // Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
				System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSCareFreedom(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				driver.quit();  
				
				// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
		        driver.quit();
		       
			} catch (Exception e) {
				// WriteExcel.setCellData("Test_Case_PosFreedom", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				// Thread.sleep(4000);
				 driver.quit();

			}
			continue;

		}
	}
}
