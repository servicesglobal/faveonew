package com.org.faveo.healthinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.CareSeniorNewJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareSenior extends BaseClass implements AccountnSettingsInterface{

	public static Integer n;
	
	@Test(enabled = true, priority = 1)
	public static void senior() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("Care_Senior_Testcase");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Care_Senior_Testcase");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 1; n < rowCount; n++) {
			try {
				BaseClass.LaunchBrowser();
				driver.navigate().refresh();
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" + TestCaseName);

				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));
				HealthInsuranceDropDown.CareSenior();
				CareSeniorNewJourney.seniorquotation();
				CareSeniorNewJourney.senior_dropdown();
				CareSeniorNewJourney.Suminsure();
				CareSeniorNewJourney.seniorAddons();
				QuotationandProposalVerification.PremiumAssertion();
				CareSeniorNewJourney.seniorproposerdetails();
				CareSeniorNewJourney.seniorInsuredDetails();
				CareSeniorNewJourney.seniorHealthQuestion();
				CareSeniorNewJourney.seniorProposalSummaryPage();
				driver.quit();

			} catch (Exception e) {
				WriteExcel.setCellData("Care_Senior_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				//Thread.sleep(4000);
				driver.quit();
			}
			continue;
		}
	}
	//***********  Share Quote ********************************
	@Test(enabled = false, priority=2)
	public static void VerifyDataInShareQuote_CareSenior() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareQuotation");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 1; n <= 3; n++) {
			try {
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" + TestCaseName);

				// Step 1 - Open Email and Delete all Old Emails
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 2 - Go to Application and share a Quote and Verify Data
				// in Quotation Tracker
				System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				HealthInsuranceDropDown.CareSenior();
				CareSeniorNewJourney.seniorquotation();
				CareSeniorNewJourney.senior_dropdown();
				CareSeniorNewJourney.Suminsure();
				CareSeniorNewJourney.seniorAddons();

				AddonsforProducts.readPremiumFromFirstPage();
				ReadDataFromEmail.readAgeGroup();

				DataVerificationShareQuotationPage.clickOnShareQuotationButton();
				DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForCareSenior(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 3 - Open Email and Verify Data in Email Body
				System.out.println(" Open Email and Verify Data in Email Body");
				logger.log(LogStatus.PASS, " Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForCareSenior(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

				// Step 4 - Click on Buy Now Button from Email Body and punch
				// the Policy
				System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
				logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();

				CareSeniorNewJourney.seniorproposerdetails();
				CareSeniorNewJourney.seniorInsuredDetails();
				CareSeniorNewJourney.seniorHealthQuestion();
				// CareSeniorNewJourney.seniorProposalSummaryPage();
				try {
					(new WebDriverWait(driver, 40)).until(ExpectedConditions
							.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
							.click();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println("unable to load psoposal summary payment method page :");
				}

				PayuPage_Credentials();

				driver.quit();

				// Step 5 - Again Login the application and verify after punch
				// the policy data should be not available in quotation Tracker
				System.out.println(
						" Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						" Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage
						.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForCareSenior(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 6- Again Open the Email and Verify GUID Should be
				// Reusable
				System.out.println("Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				driver.quit();

			} catch (Exception e) {
				// WriteExcel.setCellData("Care_Senior_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				driver.quit();
			}
			continue;
		}
	}
	//*************** Share Proposal Test Cases *******************************************************8
	@Test(enabled = false, priority=3)
	public static void verifyDataOfShareProposalInDraftHistory_CareSenior() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 4; n <= 6; n++) {
			try {
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" + TestCaseName);

				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				LaunchBrowser();
				
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				HealthInsuranceDropDown.CareSenior();
				CareSeniorNewJourney.seniorquotation();
				CareSeniorNewJourney.senior_dropdown();
				CareSeniorNewJourney.Suminsure();
				CareSeniorNewJourney.seniorAddons();
				QuotationandProposalVerification.PremiumAssertion();
				CareSeniorNewJourney.seniorproposerdetails();
				CareSeniorNewJourney.seniorInsuredDetails();
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForCareSenior(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
				
				CareSeniorNewJourney.seniorHealthQuestion();
				
				
				try {
					(new WebDriverWait(driver, 40))
							.until(ExpectedConditions
									.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
							.click();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println("unable to load psoposal summary payment method page :");
				}
				
				PayuPage_Credentials();
				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				clickElement(By.xpath(Close_VideoPopup_Xpath));
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareSenior(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				driver.quit();


			} catch (Exception e) {
				// WriteExcel.setCellData("Care_Senior_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				
				//driver.quit();
			}
			continue;
		}
	}
	
	
	//*************** Share Quote Test case 2 *******************************************************8
		@Test(enabled = false,  priority=4)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail_CareSenior() throws Exception {
			String[][] TestCase = BaseClass.excel_Files("ShareProposal");
			
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("ShareProposal");
			System.out.println("Total Number of Row in Sheet : " + rowCount);

			for (n = 6; n <= 6; n++) {
				try {
					String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
					logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
					System.out.println("Care Senior -" + TestCaseName);
					
					//Step 1 - Open Email and Delete Old Email
					System.out.println("Step 1 - Open Email and Delete Old Email");
					logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
					
					LaunchBrowser();
					ReadDataFromEmail.openAndDeleteOldEmail();
					driver.quit(); 

					//Step 2 - Go to Application and share a proposal
					System.out.println("Step 2 - Go to Application and share a proposal");
					logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
					LaunchBrowser();

					// Login with the help of Login Case class
					LoginCase.LoginwithValidCredendial();
					clickElement(By.xpath(Close_VideoPopup_Xpath));
					HealthInsuranceDropDown.CareSenior();
					CareSeniorNewJourney.seniorquotation();
					CareSeniorNewJourney.senior_dropdown();
					CareSeniorNewJourney.Suminsure();
					CareSeniorNewJourney.seniorAddons();
					
					// Verify TotalMembers from Excel and Quotation Page
					Thread.sleep(5000);
					AddonsforProducts.readPremiumFromFirstPage();
	                ReadDataFromEmail.readAgeGroup(); 
	                
	             // Click on BuyNow Button
					try {
						Thread.sleep(3000);
						clickElement(By.xpath(Button_Buynow_xpath));
						System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
					} catch (Exception e) {
						System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
					}
					
					CareSeniorNewJourney.seniorproposerdetails();
					CareSeniorNewJourney.seniorInsuredDetails();
					
					//Set Details of Share Proposal Popup Box and Verify Data in Draft History
					DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					driver.quit();
				
					//Step 3- Again Open the email and verify the data of Share Proposal in Email Body 
					System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
					logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
					
				    LaunchBrowser();
				    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForCareSenior(n);
				    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					
					//Step 4 - Click on Buy Now button from Email Body and punch the policy
					System.out.println("Step 4 - Click on Buy Now button from Email Body and punch the policy");
					logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now button from Email Body and punch the policy");
					
					ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
					
					DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
					DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
					
					CareSeniorNewJourney.seniorHealthQuestion();
					
					try {
						(new WebDriverWait(driver, 40))
								.until(ExpectedConditions
										.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
								.click();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("unable to load psoposal summary payment method page :");
					}
					
					PayuPage_Credentials();
					driver.quit();
					
					//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
					System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
					logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
		
					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					clickElement(By.xpath(Close_VideoPopup_Xpath));
					DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareSenior(n);

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					driver.quit();
					
					// Step 6- Again Open the Email and Verify GUID Should be Reusable
					System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
					logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

					LaunchBrowser();
					ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

					// Verify the Page Title
					DataVerificationShareProposalPage.verifyPageTitle();   

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					driver.quit();  


				} catch (Exception e) {
					 WriteExcel.setCellData("Care_Senior_Testcase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);
					driver.quit();
				}
				continue;
			}
		}
	

}