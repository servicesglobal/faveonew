package com.org.faveo.Assertions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.utility.DbManager;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationinDb extends BaseClass

{

	public static void DBVerification(String PayuProposalNum) throws ClassNotFoundException, SQLException
	{
		DbManager.setDbConnection();
		String[][] TestCaseData1;
		String Environment = null;
		try {
			TestCaseData1 = BaseClass.excel_Files("Credentials");
			Environment = TestCaseData1[3][1].toString().trim();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(Environment.contains("QC")){
		List<String> DBValues = DbManager.getSqlQuery("Select policynum,proposalnum,PROCESSSTATUSCD from rhqc.policy Where proposalnum ='"+ PayuProposalNum + "'");
		System.out.println(DBValues);
		logger.log(LogStatus.INFO,"Proposal Number/Policy Num and Premium is Verified in DB : " + DBValues);
		}else if(Environment.contains("UAT")){
			List<String> DBValues = DbManager.getSqlQuery("Select policynum,proposalnum,PROCESSSTATUSCD from rhuat.policy Where proposalnum ='"+ PayuProposalNum + "'");
			System.out.println(DBValues);
			logger.log(LogStatus.INFO,"Proposal Number/Policy Num and Premium is Verified in DB : " + DBValues);
		}
	}
	
	
}
