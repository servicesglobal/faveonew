package com.org.faveo.Assertions;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.net.imap.IMAP.IMAPChunkListener;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class PdfMatch extends BaseClass implements AccountnSettingsInterface
{

	public static void pdfmatchcode(String FinalAmount,String PayuProposalNum,String proposalSummarypremium_value) throws InterruptedException
	{
		waitForElement(By.xpath(PolProp_xpath));
		String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
		String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
		System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
		if (ProposalSummary.contains("Policy No.")) {
			Thread.sleep(15000);
			clickElement(By.xpath(Downloadpdf_Thankyou_xpath));
			Thread.sleep(25000);
				try{
					
				String UserName = System.getProperty("user.home");
				System.out.println(UserName);

				File file = new File(UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");

				FileInputStream input = new FileInputStream(file);

				PDFParser parser = new PDFParser(input);

				parser.parse();

				COSDocument cosDoc = parser.getDocument();

				PDDocument pdDoc = new PDDocument(cosDoc);

				PDFTextStripper strip = new PDFTextStripper();
				
				strip.setStartPage(1);
				strip.setEndPage(2);

				String data = strip.getText(pdDoc);

				Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
				/*Assert.assertTrue(data.contains(FinalAmount));*/
				//Assert.assertTrue(data.contains(ProposerName));
				
				cosDoc.close();
				pdDoc.close();
				System.out.println("Text Found on the pdf File...");
				logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
				TestResult = "Pass";
			} catch (Exception e) {

				String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
				System.out.println(ErrorMessage);
				logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
				TestResult = "Fail";
			}


		} else if (ProposalSummary.contains("Application No.")) {
			try {
				Assert.assertEquals(PayuProposalNum, ThankyoupageProposal_Pol_num);
				TestResult = "Pass";
				logger.log(LogStatus.INFO,"Proposal number on Payu page and Thankyour Page is Verified and Both are Same.");
			} catch (AssertionError e) {
				System.out.println(proposalSummarypremium_value + " - failed");
				TestResult = "Fail";
				logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same.");
				throw e;
			}
		}

	}
	
}
