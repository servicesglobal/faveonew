package com.org.faveo.travelInsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.TravelInsuranceDropDown;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Explore extends BaseClass implements AccountnSettingsInterface {

	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static Integer n;

	@Test
	public static void Explore() throws Exception {

		/*
		 * String[][] TestCase=BaseClass.excel_Files1("Explore_Test_Cases");
		 * String[][] TestCaseData=BaseClass.excel_Files1("Explore_Quotation");
		 * String[][]
		 * FamilyData=BaseClass.excel_Files1("Explore_Insured_Details");
		 * String[][]
		 * QuestionSetData=BaseClass.excel_Files1("Explore_Question_Set");
		 * 
		 * String[][] TestcaseEn=BaseClass.excel_Files("Test_Cases_Care");
		 * String[][] loginData=BaseClass.excel_Files("Credentials"); String
		 * Environment = loginData[3][1].toString().trim();
		 */
		/*
		 * ReadExcel fis = new ReadExcel(".\\TestData\\BVT_Sheet.xlsx"); int
		 * rowCount = fis.getRowCount("Explore_Test_Cases");
		 * System.out.println("Total Number of Row in Sheet : "+rowCount);
		 */
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Explore_Test_Cases");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (n = 1; n < rowCount; n++) {

			try {

				String[][] TestCase = BaseClass.excel_Files1("Explore_Test_Cases");
				String[][] TestCaseData = BaseClass.excel_Files1("Explore_Quotation");
				String[][] FamilyData = BaseClass.excel_Files1("Explore_Insured_Details");
				String[][] QuestionSetData = BaseClass.excel_Files1("Explore_Question_Set");

				String[][] TestcaseEn = BaseClass.excel_Files("Test_Cases_Care");
				String[][] loginData = BaseClass.excel_Files("Credentials");

				System.out.println("Total Number of Row in Sheet : " + rowCount);

				// ReadExcel read = new
				// ReadExcel("D:\\Test_Data_Faveo_Automation\\Favio_Framework.xlsx");

				// We are Taking Test Case name from Excel and Printing it in
				// our Report
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Explore Travel " + TestCaseName);
				System.out.println("Explore Travel " + TestCaseName);
				// From Here we are Calling Base Class Method to Open the
				// Browser
				// BaseClass.LaunchBrowser();
				// From Here we are Calling Login Class Method to Login in Faveo
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				//clickElement(By.xpath(Close_VideoPopup_Xpath));
				
				// From Here we are Calling TravelInsuranceDropdown for Explore
				// product
				TravelInsuranceDropDown.ExploreTravelPolicy();

				/*
				 * <----------------------Quotation Page Journey Started From
				 * Here----------------->
				 */

				// Selecting Value of Travelling to from Excel on Quotation Page
				// Fluentwait(By.xpath(TravellingAllDropdowns_xpath));
				Thread.sleep(10000);
				clickElement(By.xpath(TravellingAllDropdowns_xpath));
				// *[@class='toolbar_plan_name_input']/ui-dropdown/div[1]/div/a[contains(text(),'Asia')]
				String TravellingTo = TestCaseData[n][1].toString().trim();
				System.out.println(TravellingTo);
				// clickElement(By.xpath("//div[2]/form/div/div/div/div[1]/div/div/div/ui-dropdown/div/div/ul//a[contains(text(),"+"'"+TravellingTo+"'"+")]"));
				clickElement(By
						.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/ul/li//a[contains(text(),"
								+ "'" + TravellingTo + "'" + ")]"));
				System.out.println("User is Travelling To : " + TravellingTo);

				// Selecting Trip Type on The Basis of Travelling To
				try {
					if (TravellingTo.contains("WW Excl US & Canada") || TravellingTo.contains("Worldwide")) {
						String TripType = TestCaseData[n][2].toString().trim();
						if (TripType.contains("Single")) {
							clickElement(By.xpath(TripType_xpath));
							clickElement(By.xpath(
									"//a[@ng-click='selectVal(item)'][contains(text()," + "'" + TripType + "'" + ")]"));
							System.out.println("User has Selected Trip Type as : " + TripType);
						} else if (TripType.contains("Multi")) {
							clickElement(By.xpath(TripType_xpath));
							clickElement(By.xpath(
									"//a[@ng-click='selectVal(item)'][contains(text()," + "'" + TripType + "'" + ")]"));
							System.out.println("User has Selected Trip Type as : " + TripType);
							int MaxTripDuration = Integer.parseInt(TestCaseData[n][3].toString().trim());
							clickElement(By.xpath(MaxtripDuration_xpath));
							clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text()," + "'"
									+ MaxTripDuration + "'" + ")]"));
							System.out.println("Selceted Max Trip Duration is : " + MaxTripDuration);
						}

					} else {
						System.out.println("No Need to Select Trip Type because user is Travelling To " + TravellingTo);
					}
				} catch (Exception e) {
					System.out.println("No Need to Select Trip Type because user is Travelling To " + TravellingTo);
				}

				// Select Start Date from Excel and Write it on UI
				Thread.sleep(5000);
				String StartDate = TestCaseData[n][4].toString().trim();
				try {
					Thread.sleep(4000);
					clickElement(By.xpath(StartDateCalander_Id));
					Thread.sleep(2000);
					String spilitter[] = StartDate.split(",");
					String eday = spilitter[0];
					String emonth = spilitter[1];
					String eYear = spilitter[2];
					String oMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]"))
							.getText();
					String oYear = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]"))
							.getText();
					WebElement Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																										// Button
					while (!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth)))) {
						Thread.sleep(2000);
						Next.click();
						oMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
						oYear = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
						Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
					}
					driver.findElement(By
							.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
									+ "'" + eday + "'" + ")]"))
							.click();
					System.out.println("Entered Start_Date on Faveo UI is :" + StartDate);
				} catch (Exception e) {
					System.out.println("Unable to Enter Start Date.");
				}

				// Select End Date from Excel and Write it on UI
				clickElement(By.id(EndDateCalender_Id));
				String EndDate = TestCaseData[n][5].toString().trim();
				String spilitter1[] = EndDate.split(",");
				String eday1 = spilitter1[0];
				String emonth1 = spilitter1[1];
				String eYear1 = spilitter1[2];
				String oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																									// Button
				while (!((eYear1.contentEquals(oYear1)) && (emonth1.contentEquals(oMonth1)))) {
					Thread.sleep(2000);
					Next1.click();
					oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + eday1 + "'" + ")]"))
						.click();
				System.out.println("Entered End_Date on Faveo UI is :" + EndDate);

				// Select PED and Non Ped from Excel and Select it on UI
				String PED = TestCaseData[n][7].toString().trim();
				clickElement(By.xpath(Peddropdown_xpath));
				clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text()," + "'" + PED + "'" + ")]"));
				System.out.println("User has Selected PED as : " + PED);

				// Select Travellers from Excel
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						if (DropDownName.getText().equals("1")) {
							DropDownName.click();
							int Travellers = Integer.parseInt(TestCaseData[n][8].toString().trim());
							Thread.sleep(2000);
							clickElement(By
									.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"
											+ "'" + Travellers + "'" + ")]"));
							// clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
							System.out.println("Total Number of Travellers Selected : " + Travellers);
							break;
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Select Member.");
				}

				// Select Age of Members from Excel
				// again call the dropdown
				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][8].toString().trim());
				System.out.println("Total Number of Member in Excel : " + membersSize);
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				outer:

				for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));

						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][9];
						System.out.println("Members details is : " + Membersdetails);

						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split(",");
							// System.out.println("Base Class Member :
							// "+BaseClass.membres);
							// System.out.println("Base Class Member :
							// "+BaseClass.membres.length);

							member: for (int i = 0; i <= BaseClass.membres.length; i++) {

								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								System.out.println("value of Mcount Index : " + mcount);
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'up to 40 Years')]"))
										.click();

								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));

								String text = FamilyData[mcount][0].toString().trim();

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

										Thread.sleep(1000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
											// break outer;
										}

									}

								}
							}
						}

					}

					else

					{
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().equals("up to 40 Years")) {

								String Membersdetails = TestCaseData[n][9];
								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");
								} else {
									// BaseClass.membres =
									// Membersdetails.split(" ");
									System.out.println("Hello");
								}
								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();

									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(1000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}

							}
						}
					}
				}

				// Reading Mobile Number from Excel and Sending it on Faveo UI
				Thread.sleep(2000);
				String MobileNumber = TestCaseData[n][10].toString().trim();
				try {
					int size = MobileNumber.length();
					System.out.println("Entered Mobile number is: " + MobileNumber);
					if (isValid(MobileNumber)) {
						System.out.println("Entered Number is a valid number");
						enterText(By.xpath(MobileNumber_xpath), String.valueOf(MobileNumber));
					} else {
						System.out.println("Entered Number is Not a valid Number");
					}
				} catch (Exception e) {
					System.out.println("Unable to Enter Mobile Number.");
				}

				// Reading Email from Excel and Sending it on Faveo UI
				String Email = TestCaseData[n][11].toString().trim();
				System.out.println("Entered Email-id is  :" + Email);
				enterText(By.xpath(Emailid_xpath), Email);

				// Reading SumInsured from Excel and Sending it on Faveo UI
				int SumInsured = Integer.parseInt(TestCaseData[n][12].toString().trim());
				try {
					clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
					clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
					clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
					System.out.println("Entered SumInsured is : " + SumInsured);
				} catch (Exception e) {
					System.out.println("Unable to Select SumInsured.");
				}

				// Reading Addon from Excel and Selcting it on Faveo UI
				String Addon = TestCaseData[n][13];
				String executionstatusbeforeedit = TestCase[n][4].toString().trim();
				if (executionstatusbeforeedit.equalsIgnoreCase("No-Edit")) {

					String Premium = null;
					String Premium1 = null;
					String Premium2 = null;
					if (Addon.contains("No Addon")) {
						Thread.sleep(2000);
						clickElement(By.xpath(NoAddon_xpath));
						Thread.sleep(2000);
						Premium = driver
								.findElement(By
										.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[7]/div/div/div/div/label[2]/b[1]"))
								.getText();
						System.out.println("Total Premium Value on Quotation Page is : " + " - "
								+ Premium.substring(0, Premium.length()));

					} else if (Addon.contains("for gold plan")) {
						Thread.sleep(2000);
						clickElement(By.xpath(GoldPlanAddon_xpath));
						Thread.sleep(2000);
						Premium1 = driver
								.findElement(By
										.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[7]/div/div/div/div[1]/label[2]/b[1]"))
								.getText();
						System.out.println("Total Premium Value on Quotation Page is : " + " - "
								+ Premium1.substring(0, Premium1.length()));
					} else if (Addon.contains("with platinum plan")) {
						Thread.sleep(2000);
						clickElement(By.xpath(PlatinumAddon_xpath));
						Thread.sleep(2000);
						Premium2 = driver
								.findElement(By
										.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[7]/div/div/div/div[2]/label[2]/b[1]"))
								.getText();
						System.out.println("Total Premium Value on Quotation Page is : " + " - "
								+ Premium2.substring(0, Premium2.length()));
					}
					System.out.println("Selected Addon is : " + Addon);

					Thread.sleep(5000);
					try {
						System.out.println("Before Click on BuyNow Button.");
						clickElement(By.xpath(BuyNowTravel_xpath));
						System.out.println("After Click on BuyNow Buton.");
					} catch (Exception e) {
						System.out.println("Unable to click on BuyNow.");
					}
					Thread.sleep(3000);
					// Verifying Premium of Quotation and Proposal Page
					String ProposalPagePremium = driver
							.findElement(By
									.xpath("/html/body/div[2]/div[30]/div[1]/div[1]/div/div/div/div[1]/div/h4/span/span"))
							.getText();
					System.out.println("Total Premium Value on Proposal Page : " + ProposalPagePremium);
					if (Addon.contains("No Addon")) {
						try {
							Assert.assertEquals(Premium, ProposalPagePremium);
							logger.log(LogStatus.INFO,
									"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
											+ ProposalPagePremium);
						} catch (AssertionError e) {
							System.out.println(Premium + " - failed");
							logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
							throw e;
						}

					} else if (Addon.contains("for gold plan")) {
						try {
							Assert.assertEquals(Edit.Premium1, ProposalPagePremium);
							logger.log(LogStatus.INFO,
									"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
											+ ProposalPagePremium);
						} catch (AssertionError e) {
							System.out.println(Premium1 + " - failed");
							logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
							throw e;
						}
					} else if (Addon.contains("with platinum plan")) {
						try {
							Assert.assertEquals(Premium2, ProposalPagePremium);
							logger.log(LogStatus.INFO,
									"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
											+ ProposalPagePremium);
						} catch (AssertionError e) {
							System.out.println(Premium2 + " - failed");
							logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
							throw e;
						}
					}
				}
				// Click on Buy Now Button

				scrollup();
				// Edit starts here
				Thread.sleep(15000);
				String executionstatus = TestCase[n][4].toString().trim();
				if (executionstatus.equals("Edit")) {
					//Thread.sleep(5000);
					try {
						System.out.println("Before Click on BuyNow Button.");
						clickElement(By.xpath(BuyNowTravel_xpath));
						System.out.println("After Click on BuyNow Buton.");
					} catch (Exception e) {
						System.out.println("Unable to click on BuyNow.");
					}
					Edit.ExploreEditStart();
					Thread.sleep(3000);
					// Verifying Premium of Quotation and Proposal Page
					//String ProposalPagePremium = driver.findElement(By.xpath("/html/body/div[2]/div[30]/div[1]/div[1]/div/div/div/div[1]/div/h4/span/span")).getText();
					String ProposalPagePremium = driver.findElement(By.xpath("//div[contains(@class,'quotation_head_fixed_details')]//div[contains(@class,'tr_quotation_heading')]//span[@class='ng-binding']")).getText();
					System.out.println("Total Premium Value on Proposal Page : " + ProposalPagePremium);
					if (Addon.contains("No Addon")) {
						try {
							Assert.assertEquals(Edit.Premium, ProposalPagePremium);
							logger.log(LogStatus.INFO,
									"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
											+ ProposalPagePremium);
						} catch (AssertionError e) {
							System.out.println(Edit.Premium + " - failed");
							logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
							throw e;
						}

					} else if (Addon.contains("for gold plan")) {
						try {
							Assert.assertEquals(Edit.Premium1, ProposalPagePremium);
							logger.log(LogStatus.INFO,
									"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
											+ ProposalPagePremium);
						} catch (AssertionError e) {
							System.out.println(Edit.Premium1 + " - failed");
							logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
							throw e;
						}
					} else if (Addon.contains("with platinum plan")) {
						try {
							Assert.assertEquals(Edit.Premium2, ProposalPagePremium);
							logger.log(LogStatus.INFO,
									"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
											+ ProposalPagePremium);
						} catch (AssertionError e) {
							System.out.println(Edit.Premium2 + " - failed");
							logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
							throw e;
						}
					}

				} else {
					System.out.println("Edit not required.............");
				}
				/*
				 * <----------------------Proposal Page Journey Started From
				 * Here----------------->
				 */

				// Verifying Travelling To and PED on Quotation and Proposal
				// Page
				
				try {
					//String TravellingToProposal = driver.findElement(By.xpath("/html/body/div[2]/div[30]/div[1]/div[1]/div/div/div/div[2]/div[3]/p/span[2]")).getText();
					String TravellingToProposal = driver.findElement(By.xpath("//div[contains(@class,'quotation_head_fixed_details')]//div[@class='col-md-3 ng-scope'][3]//span[2]")).getText();
					Assert.assertEquals(TravellingTo, TravellingToProposal);
					logger.log(LogStatus.PASS,
							"Quotaion Page Travelling To and Proposal Page Travelling To is Verified and Both are Same : "
									+ TravellingToProposal);
					System.out.println("Quotaion Page Travelling To and Proposal Page Travelling To is Verified and Both are Same : "
									+ TravellingToProposal);
				} catch (Exception e) {
					System.out.println(TravellingTo + " - failed");
					logger.log(LogStatus.FAIL,
							"Quotaion Page Travelling To and Proposal Page Travelling To are not Same.");
					throw e;
				}

				// Verifying PED on Quotation and Proposal Page
				//String PEDProposal = driver.findElement(By.xpath("/html/body/div[2]/div[30]/div[1]/div[1]/div/div/div/div[2]/div[4]/p/span[2]")).getText();
				try {
					//String PEDProposal = driver.findElement(By.xpath("/html/body/div[2]/div[30]/div[1]/div[1]/div/div/div/div[2]/div[4]/p/span[2]")).getText();
					String PEDProposal = driver.findElement(By.xpath("//div[contains(@class,'quotation_head_fixed_details')]//div[@class='col-md-2']//span[2]")).getText();
					Assert.assertEquals(PED, PEDProposal);
					logger.log(LogStatus.PASS,
							"Quotaion Page PED Value and Proposal Page PED Value is Verified and Both are Same : "
									+ PEDProposal);
				} catch (Exception e) {
					System.out.println(PED + " - failed");
					logger.log(LogStatus.FAIL, "Quotaion Page PED and Proposal Page PED are not Same.");
					throw e;
				}

				// Reading Nationality from Excel and Selcting it on UI
				Thread.sleep(6000);
				String Nationality = TestCaseData[n][14];
				clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
				clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text()," + "'"
						+ Nationality + "'" + ")]"));
				System.out.println("Entered Nationality is : " + Nationality);

				// Reading Passport Number from Excel and Pass it on Faveo UI
				String Passport = TestCaseData[n][15];
				clearTextfield(By.xpath(PassportNumber_xpath));
				ExplicitWait1(By.xpath(PassportNumber_xpath));
				enterText(By.xpath(PassportNumber_xpath), Passport);
				System.out.println("Entered Passport Number : " + Passport);
				NextTab(By.xpath(PassportNumber_xpath));

				// Reading Title from Excel and Pass it on Faveo UI
				String Title = TestCaseData[n][16].toString().trim();
				try {
					Thread.sleep(10000);
					clickElement(By.xpath(Title_xpath));
					clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"
							+ Title + "'" + ")]"));
					System.out.println("Entered Title is : " + Title);
				} catch (Exception e) {
					System.out.println("Unable to Selected Title for Proposer.");
					// logger.log(LogStatus.FAIL, "Test Case is Failed because
					// Unable to Selected Title for Proposer.");
				}

				// Reading FirstName and LastName from Excel and Pass it on
				// Faveo UI
				String FirstName = TestCaseData[n][17];
				clearTextfield(By.xpath(FirstName_xpath));
				ExplicitWait1(By.xpath(FirstName_xpath));
				enterText(By.xpath(FirstName_xpath), FirstName);
				System.out.println("Entered First Name is : " + FirstName);
				String LastName = TestCaseData[n][18];
				clearTextfield(By.xpath(LastName_xpath));
				ExplicitWait1(By.xpath(LastName_xpath));
				enterText(By.xpath(LastName_xpath), LastName);
				System.out.println("Entered Last Name is : " + LastName);

				// Reading DOB from Excel and Pass it on UI
				String DOB = TestCaseData[n][19].toString().trim();
				try {
					clickElement(By.xpath(DOBCalender_xpath));
					clearTextfield(By.xpath(DOBCalender_xpath));
					ExplicitWait1(By.xpath(DOBCalender_xpath));
					enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
					System.out.println("Entered Proposer DOB is : " + DOB);
				} catch (Exception e) {
					System.out.println("Unable to Enter Date of Birth Date.");
				}

				// Reading AddressLine from Excel and Pass it on Faveo UI
				String AddressLine1 = TestCaseData[n][20].toString().trim();
				clearTextfield(By.xpath(AddressLine1_xpath));
				ExplicitWait1(By.xpath(AddressLine1_xpath));
				clickElement(By.xpath(AddressLine1_xpath));
				enterText(By.xpath(AddressLine1_xpath), AddressLine1);
				System.out.println("Entered AddressLine1 is : " + AddressLine1);
				String AddressLine2 = TestCaseData[n][21].toString().trim();
				clearTextfield(By.xpath(AddressLine2_xpath));
				ExplicitWait1(By.xpath(AddressLine2_xpath));
				clickElement(By.xpath(AddressLine2_xpath));
				enterText(By.xpath(AddressLine2_xpath), AddressLine2);
				System.out.println("Entered AddressLine1 is : " + AddressLine2);

				// Reading Pincode from Excel and Pass it on Faveo UI
				int Pincode = Integer.parseInt(TestCaseData[n][22].toString().trim());
				clearTextfield(By.xpath(Pincode_xpath));
				ExplicitWait1(By.xpath(Pincode_xpath));
				enterText(By.xpath(Pincode_xpath), String.valueOf(Pincode));
				System.out.println("Entered Pincode is : " + Pincode);

				// Reading Nominee Name from Excel and Pass it on Faveo UI
				String NomineeName = TestCaseData[n][23];
				clearTextfield(By.xpath(Nomineename_xpath));
				ExplicitWait1(By.xpath(Nomineename_xpath));
				enterText(By.xpath(Nomineename_xpath), NomineeName);
				System.out.println("Entered Nominee Name is : " + NomineeName);

				// Reading Purpose Of Visit from Excel and Pass it on Faveo UI
				String PurposeofVisit = TestCaseData[n][24];
				clickElement(By.xpath(PurposeofVisit_xpath));
				clickElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text()," + "'"
						+ PurposeofVisit + "'" + ")]"));
				System.out.println("Selected Purpose of Visit is : " + PurposeofVisit);

				String Pancardnumber = TestCaseData[n][30].toString().trim();
				try {
					clickElement(By.xpath(PanCard_xpath));
					enterText(By.xpath(PanCard_xpath), Pancardnumber);
				} catch (Exception e) {
					System.out.println("Pan card Field is not Present on UI.");
				}

				// <----------------------Insured details Page Journey Started
				// From Here----------------->

				System.out.println(
						"<----------------------Insured details Page Journey Started From Here----------------->");

				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());

					String Relation = FamilyData[mcount][1].toString().trim();
					String InsuredNationality = FamilyData[mcount][2].toString().trim();
					String InsuredPassport = FamilyData[mcount][3].toString().trim();
					String InsuredTitle = FamilyData[mcount][4].toString().trim();
					System.out.println("Value of Mcount : " + mcount);
					System.out.println("Insured Title is : " + InsuredTitle);
					String InsuredFirstName = FamilyData[mcount][5].toString().trim();
					String InsuredLastName = FamilyData[mcount][6].toString().trim();
					String InsuredDOB = FamilyData[mcount][7].toString().trim();

					if (Relation.contains("Self-primary")) {
						clickElement(By.xpath("//select[@name='ValidRelation" + i + "']"));
						clickElement(By.xpath("//select[@name='ValidRelation0']//option[@value='SELF']"));
					} else {

						BaseClass.selecttext("ValidRelation" + i, Relation);
						System.out.println("Selected Relation is : " + Relation);

						BaseClass.selecttext("rel_nationality" + i, InsuredNationality);
						System.out.println("Selected Nationality is : " + InsuredNationality);

						enterText(By.name("rel_passport" + i), InsuredPassport);
						System.out.println("Entered Passport is : " + InsuredPassport);

						try {
							System.out.println("//select[@id='ValidRelTitle" + i + "']//option[@value=" + "'"
									+ InsuredTitle + "'" + "]");
							clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']"));
							Thread.sleep(2000);
							clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']//option[@value=" + "'"
									+ InsuredTitle + "'" + "]"));
							System.out.println("Selcted Title is : " + InsuredTitle);
						} catch (Exception e) {
							System.out.println("Unable to Select Title.");
						}

						if (Relation.contains("Self-primary")) {
							enterText(By.name("RelFName" + i), InsuredFirstName);
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						} else {
							clearTextfield(By.name("RelFName" + i));
							enterText(By.name("RelFName" + i), InsuredFirstName);
							clearTextfield(By.name("RelLName" + i));
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						}

						if (Relation.contains("Self-primary")) {
							try {
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						} else {
							try {
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								clearTextfield(By.xpath("//input[@name='rel_dob" + i + "']"));
								Thread.sleep(2000);
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						}
					}
				}

				// <----------------------PED Questions details Started From
				// Here----------------->

				// Health Questionnarire Elements
				BaseClass.scrolldown();
				String preExistingdeases = TestCaseData[n][25].toString().trim();
				int Travellers = Integer.parseInt(TestCaseData[n][8].toString().trim());
				Thread.sleep(2000);
				System.out.println(
						"Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				try {
					if (preExistingdeases.contains("YES") || preExistingdeases.contains("Yes")
							|| preExistingdeases.contains("yes")) {

						waitForElements(By.xpath(PEDYesButton_xpath));
						clickElement(By.xpath(PEDYesButton_xpath));

						String[] ChckData = null;
						int datacheck1 = 0;
						String HealthQuestion = QuestionSetData[n][1].toString().trim();
						System.out.println("Member Having PED : " + HealthQuestion);
						for (int i = 0; i < Travellers; i++) {
							clickElement(By.xpath("//select[@id='qs_pedYesNo_" + i + "']"));
							clickElement(By.xpath("//select[@id='qs_pedYesNo_" + i + "']//option[@value='NO']"));
						}

						if (HealthQuestion.contains(",")) {

							ChckData = HealthQuestion.split(",");
							for (String Chdata : ChckData) {
								datacheck1 = Integer.parseInt(Chdata);
								datacheck1 = datacheck1 - 1;

								clickElement(By.xpath("//select[@id='qs_pedYesNo_" + datacheck1 + "']"));
								clickElement(By
										.xpath("//select[@id='qs_pedYesNo_" + datacheck1 + "']//option[@value='YES']"));

							}
						} else if (HealthQuestion.contains("")) {
							datacheck1 = Integer.parseInt(HealthQuestion);
							datacheck1 = datacheck1 - 1;

							clickElement(By.xpath("//select[@id='qs_pedYesNo_" + datacheck1 + "']"));
							clickElement(
									By.xpath("//select[@id='qs_pedYesNo_" + datacheck1 + "']//option[@value='YES']"));
						}

						String[] ChckData1 = null;
						int detailsnumber = 0;

						for (int qlist = 2; qlist <= 6; qlist++) {

							String Details = QuestionSetData[n][qlist].toString().trim();
							int q1list = qlist + 1;

							if (Details.equals("")) {
								System.out.println("Please Enter Some value in Excel to Select PED.");
							} else if (Details.contains(",")) {

								ChckData1 = Details.split(",");

								for (String Chdata1 : ChckData1) {

									detailsnumber = Integer.parseInt(Chdata1);
									clickElement(
											By.xpath("//html//div[" + q1list + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
													+ detailsnumber + "]/div[1]//input[@type='checkbox']"));

								}
							}

							else if (Details.contains("")) {
								detailsnumber = Integer.parseInt(Details);
								try {
									driver.findElement(
											By.xpath("//html//div[" + qlist + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
													+ detailsnumber + "]/div[1]//input[@type='checkbox']"))
											.click();
								} catch (Exception e) {
									System.out.println("Not able to click on Sub Questions Values of Question 1.");
								}

							}

						}

						String[] AlimnetsQuestionData = null;
						for (int qlist = 7; qlist <= 7; qlist++) {
							String Details = QuestionSetData[n][qlist].toString().trim();
							String AlimnetsQuestion = QuestionSetData[n][7].toString().trim();
							String OtherQuestionDetails = QuestionSetData[n][8].toString().trim();

							if (AlimnetsQuestion.equals("")) {
								System.out.println(
										"No Need to Select question : Any other diseases or ailments not mentioned above.");

							} else if (AlimnetsQuestion.contains(",")) {
								AlimnetsQuestionData = AlimnetsQuestion.split(",");
								for (String AlimntQuesnData : AlimnetsQuestionData) {
									detailsnumber = Integer.parseInt(AlimntQuesnData);
									int Num = detailsnumber - 1;
									int q1list = qlist + 1;
									clickElement(
											By.xpath("//html//div[" + q1list + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
													+ detailsnumber + "]/div[1]//input[@type='checkbox']"));
									enterText(By.xpath("//textarea[@id='otherDiseasesDescription_" + Num + "']"),
											OtherQuestionDetails);
								}
							} else if (AlimnetsQuestion.contains("")) {
								detailsnumber = Integer.parseInt(AlimnetsQuestion);
								int Num = detailsnumber - 1;
								int q1list = qlist + 1;
								clickElement(By.xpath("//html//div[" + q1list + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
										+ detailsnumber + "]/div[1]//input[@type='checkbox']"));
								enterText(By.xpath("//textarea[@id='otherDiseasesDescription_" + Num + "']"),
										OtherQuestionDetails);
							}
						}
					} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No")
							|| preExistingdeases.contains("no")) {
						clickElement(By.xpath(PEDNoButton_xpath));
					}
				} catch (Exception e) {
					System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
					// logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse
					// User is unable to click on Health Question");
				}

				// 2nd Question injury during the last 48 months?
				String QuestionNumber2 = TestCaseData[n][26].toString().trim();
				String QuestionNumber2Details = TestCaseData[n][27].toString().trim();
				String[] CheckData = null;
				int DataCheck = 0;
				try {
					if (QuestionNumber2.contains("NO") || QuestionNumber2.contains("No")
							|| QuestionNumber2.contains("no")) {
						System.out.println(
								"Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
						clickElement(By.xpath(PED_2_NoButton_xpath));
					} else {
						System.out.println(
								"Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");
						System.out.println("Member Who have injured During last 48 months : " + QuestionNumber2);
						clickElement(By.xpath(PED_2_YesButton_xpath));

						for (int i = 0; i < Travellers; i++) {
							driver.findElement(By.xpath("//select[@id='qs_T001_" + i + "']")).click();
							driver.findElement(
									By.xpath("//select[@id='qs_T001_" + i + "']//option[contains(text(),'No')]"))
									.click();
						}

						if (QuestionNumber2.contains(",")) {
							CheckData = QuestionNumber2.split(",");
							for (String Chdata : CheckData) {

								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T001_" + DataCheck + "']")).click();
								driver.findElement(
										By.xpath("//select[@id='qs_T001_" + DataCheck + "']//option[@value='YES']"))
										.click();
								enterText(By.xpath("//textarea[@id='qs_T001Desc_" + DataCheck + "']"),
										QuestionNumber2Details);
								System.out.println("Reason of Injury : " + QuestionNumber2Details);

							}
						} else if (QuestionNumber2.contains("")) {
							DataCheck = Integer.parseInt(QuestionNumber2);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T001_" + DataCheck + "']")).click();
							driver.findElement(
									By.xpath("//select[@id='qs_T001_" + DataCheck + "']//option[@value='YES']"))
									.click();
							enterText(By.xpath("//textarea[@id='qs_T001Desc_" + DataCheck + "']"),
									QuestionNumber2Details);
							System.out.println("Reason of Injury : " + QuestionNumber2Details);
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Seclect Question 2.");
				}

				// 3rd Question injury during the last 48 months?
				String QuestionNumber3 = TestCaseData[n][28].toString().trim();
				String QuestionNumber3Details = TestCaseData[n][29].toString().trim();
				try {
					if (QuestionNumber3.contains("NO") || QuestionNumber3.contains("No")
							|| QuestionNumber3.contains("no")) {
						System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
						clickElement(By.xpath(PED_3_NoButton_xpath));
					} else {
						System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");
						System.out.println("Member claimed under any travel policy? : " + QuestionNumber3);
						clickElement(By.xpath(PED_3_YesButton_xpath));

						for (int i = 0; i < Travellers; i++) {
							driver.findElement(By.xpath("//select[@id='qs_T002_" + i + "']")).click();
							driver.findElement(
									By.xpath("//select[@id='qs_T002_" + i + "']//option[contains(text(),'No')]"))
									.click();
						}
						if (QuestionNumber3.contains(",")) {
							CheckData = QuestionNumber3.split(",");
							for (String Chdata : CheckData) {
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T002_" + DataCheck + "']")).click();
								driver.findElement(
										By.xpath("//select[@id='qs_T002_" + DataCheck + "']//option[@value='YES']"))
										.click();
								enterText(By.xpath("//textarea[@id='qs_T002Desc_" + DataCheck + "']"),
										QuestionNumber3Details);
							}
						} else if (QuestionNumber3.contains("")) {
							DataCheck = Integer.parseInt(QuestionNumber3);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T002_" + DataCheck + "']")).click();
							driver.findElement(
									By.xpath("//select[@id='qs_T002_" + DataCheck + "']//option[@value='YES']"))
									.click();
							enterText(By.xpath("//textarea[@id='qs_T002Desc_" + DataCheck + "']"),
									QuestionNumber3Details);
							System.out.println("Reason of Injury : " + QuestionNumber3Details);
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Seclect Question 3.");
				}

				clickElement(By.xpath("//input[@id='termsCheckbox1']"));
				clickElement(By.xpath("//input[@id='tripStart']"));
				clickElement(By.xpath("//input[@id='termsCheckbox2']"));
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
				//clickElement(By.xpath("//button[@class='btn btn-success']"));
				clickElement(By.xpath("//button[text()='Proceed to Pay']"));

				try {
					Fluentwait1(By.xpath("//div[@class='alert alert-danger ng-binding']"));
					String ErrorFill = driver.findElement(By.xpath("//div[@class='alert alert-danger ng-binding']"))
							.getText();
					System.out.println("Test Case is failed Because Getting Error : " + ErrorFill);
					logger.log(LogStatus.FAIL, "Test Case is failed Because Getting Error : " + ErrorFill);
				} catch (Exception e) {
					System.out.println("Everything is Filled on Quotation Page.");
				}

				//BaseClass.ErroronHelathquestionnaire();
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
				String proposalSummarypremium_value = (new WebDriverWait(driver, 30))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']")))
						.getText();
				System.out.println("Total premium value is : " + proposalSummarypremium_value);

				try {
					String PremiumV = driver.findElement(By.xpath("//p[@class='styl_p padding-tb ng-binding']"))
							.getText();
					logger.log(LogStatus.FAIL, "Test case is Failed because -" + PremiumV);
				} catch (Exception e) {
					System.out.println("Premium is Okay");
				}

				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
				System.out.println("After Pay U");
				// wait.until(ExpectedConditions.presenceOfElementLocated(By.className("CwaK978"))).click();

				Thread.sleep(7000);

				waitForElement(By.xpath(payu_proposalnum_xpath));
				String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
				System.out.println(PayuProposalNum);

				waitForElement(By.xpath(payuAmount_xpath));
				String PayuPremium = driver.findElement(By.xpath(payuAmount_xpath)).getText();
				String FinalAmount = PayuPremium.substring(0, PayuPremium.length() - 3);
				System.out.println(FinalAmount);
               
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
				BaseClass.TravelPayuPage_Credentials();
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));

				Thread.sleep(2000);
				BaseClass.scrolldown();

				Thread.sleep(2000);
				try {
					String PayuTimeout = driver.findElement(By.xpath("/html/body/h1")).getText();
					logger.log(LogStatus.FAIL,
							"Test Case is Failed because  Payu is downn and getting : " + PayuTimeout);

				} catch (Exception e) {
					System.out.println("Test Case Conti...");
				}

				// Thankyou Page Message Verfictaion
				
				try {
					String expectedTitle = "Your payment transaction is successful !";
					Fluentwait(By.xpath(ExpectedMessage_xpath));
					String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
					Assert.assertEquals(expectedTitle, actualTitle);
					
					logger.log(LogStatus.PASS, actualTitle);
				} catch (Exception e) {
					System.out.println("Payment Failed");
					//String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
					//logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
					//TestResult = "Fail";
				}

				
				try {
					String ProposerName = driver
							.findElement(By
									.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p"))
							.getText();
					System.out.println("Proposer Name : " + ProposerName);

					String Thankyoupagepremium_value = driver
							.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
					System.out.println("Total premium value is:" + Thankyoupagepremium_value);
					Assert.assertEquals(proposalSummarypremium_value, Thankyoupagepremium_value);
					logger.log(LogStatus.PASS,
							"Proposal Summuary Premium and Thankyou page Premium is Verified and Both are Same i.e : "
									+ Thankyoupagepremium_value.substring(0, Thankyoupagepremium_value.length()));
				} catch (Exception e) {
					System.out.println("Proposal Summuary Premium and Thankyou page Premium are not Same.");
					//logger.log(LogStatus.FAIL, "Proposal Summuary Premium and Thankyou page Premium are not Same");
					//throw e;
				}
               
				try{
				BaseClass.DBVerification(PayuProposalNum);
				}
				catch(Exception e){
					System.out.println("DB not verified");
				}

				/*waitForElement(By.xpath(PolProp_xpath));
				String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
				String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
				System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
				if (ProposalSummary.contains("Policy No.")) {
					Thread.sleep(5000);
					clickElement(By.xpath(Downloadpdf_Travel_Thankyou_xpath));
					Thread.sleep(20000);
					try {

						try {
							String UserName = System.getProperty("user.home");
							System.out.println(UserName);

							File file = new File(UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");

							FileInputStream input = new FileInputStream(file);

							PDFParser parser = new PDFParser(input);

							parser.parse();

							COSDocument cosDoc = parser.getDocument();

							PDDocument pdDoc = new PDDocument(cosDoc);

							PDFTextStripper strip = new PDFTextStripper();

							strip.setStartPage(1);
							strip.setEndPage(3);

							String data = strip.getText(pdDoc);

							// System.out.println(data);

							Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
							Assert.assertTrue(data.contains(FinalAmount));

							cosDoc.close();
							pdDoc.close();
							System.out.println("Text Found on the pdf File...");
							logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
							TestResult = "Pass";

						} catch (Exception e) {
							String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
							System.out.println(ErrorMessage);
							logger.log(LogStatus.FAIL,
									"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
							TestResult = "Fail";
						}

					} catch (Exception e) {

						String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
						System.out.println(ErrorMessage);
						logger.log(LogStatus.FAIL,
								"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
						TestResult = "Fail";
					}

				} else if (ProposalSummary.contains("Application No.")) {
					System.out.println(ThankyoupageProposal_Pol_num);
					try {
						Assert.assertEquals(PayuProposalNum, ThankyoupageProposal_Pol_num);
						TestResult = "Pass";
						logger.log(LogStatus.INFO,
								"Proposal number on Payu page and Thankyour Page is Verified and Both are Same");
					} catch (AssertionError e) {
						System.out.println(proposalSummarypremium_value + " - failed");
						TestResult = "Fail";
						logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same");
						throw e;
					}
				}

				TestResult = "Pass";*/
				// WriteExcel.setCellData1("Test_Cases_Care",TestResult,
				// ThankyoupageProposal_Pol_num, n, 2, 3);
				/*
				 * WriteExcel.setCellData("Test_Cases_Care", "Skip", n, 3);
				 * Thread.sleep(5000); driver.quit();
				 */
				//WriteExcel.setCellData("Test_Cases_Care", ThankyoupageProposal_Pol_num, n, 2);
				//WriteExcel.setCellData("Test_Cases_Care", "Pass", n, 3);
				driver.quit();

			} 
			
			catch (AssertionError e) {
				//WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				 driver.quit();
			}
			
			catch (Exception e) {
				//WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				 driver.quit();
			}
			continue;
		}

	}

}
