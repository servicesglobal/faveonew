package com.org.faveo.travelInsurance;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.ReadDataFromEmail_TravelInsurance;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class GroupExplore_ShareProposal extends BaseClass implements AccountnSettingsInterface {
	public static String PolicyType = null;
	public static String PolicyType2 = null;
	public static String testName = null;
	public static String covertype = null;
	public static String persontitle = null;
	public static Integer n;
	public static String TypeTest = null;

	public static Logger log = Logger.getLogger("devpinoyLogger");

	@Test(enabled=false, priority=1)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData = BaseClass.excel_Files("GroupExplore_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("GroupExplore_FamilyData");
		String[][] Group_Question = BaseClass.excel_Files("Group_Question");

		for (n = 1; n <=6; n++) {

			try {

				Thread.sleep(5000);
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("GroupExplore Travel " + TestCaseName);
				System.out.println("GroupExplore Travel " + TestCaseName);

				// From Here we are Calling Login Class Method to Login in Faveo

				System.out.println("++++++++++++++Group Expolre starts here+++++++++++++++");

				
				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				
			
				BaseClass.LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				Thread.sleep(5000);
				clickElement(By.xpath(travelinsurance_xpath));
				Thread.sleep(4000);
				clickElement(By.xpath(GroupExplore_xpath));
				Thread.sleep(5000);
				List<WebElement> traveldropdown = driver.findElements(
						By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
				int dropsize = traveldropdown.size();
				System.out.println("Total dropdowns are :" + dropsize);
				String travelingto = TestCaseData[n][1].toString().trim();
				String scheme = TestCaseData[n][2].toString().trim();
				//logger = extent.startTest("Selected Travelling to place is  " + travelingto);
				//logger = extent.startTest("Selected Scheme is  " + scheme);

				Thread.sleep(5000);
				for (WebElement dropdownvalue : traveldropdown) {
					dropdownvalue.click();
					// System.out.println("Text is : "+dropdownvalue.getText());
					Thread.sleep(3000);
					if (dropdownvalue.getText().contains("Asia")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/ul/li//a[contains(text(),'"
										+ travelingto + "')]"))
								.click();// select 4 based on the excel data
						Thread.sleep(5000);
						// break;
					}
					if (dropdownvalue.getText().contains("Budget") || dropdownvalue.getText().contains("Explore")
							|| dropdownvalue.getText().contains("Premium")
							|| dropdownvalue.getText().contains("Premium Plus")
							|| dropdownvalue.getText().contains("Saver")
							|| dropdownvalue.getText().contains("Economy")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[2]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ scheme + "')]"))
								.click();// select 4 based on the excel data
						Thread.sleep(5000);
						break;
					}

				}
				
				String StartDate = TestCaseData[n][3].toString().trim();
				//logger = extent.startTest("Selected Start date is  " + StartDate);
				System.out.println("Excel from date is  :" + StartDate);
				String monthdate[] = StartDate.split(",");
				String excelday = monthdate[0];
				String excelmonth = monthdate[1];
				String excelYear = monthdate[2];
				driver.findElement(By.xpath("//input[@id='start_date']")).click();
				String CMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String CYears = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				while (!((excelYear.contentEquals(CYears)) && (excelmonth.contentEquals(CMonth)))) {
					Thread.sleep(5000);
					next.click();
					CMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					CYears = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + excelday + "'" + ")]"))
						.click();
				System.out.println("Entered Start_Date on Faveo UI is for Group Explore is:" + StartDate);

				String EndDate = TestCaseData[n][4].toString().trim();
				String spilitter1[] = EndDate.split(",");
				String endday = spilitter1[0];
				String endmonth = spilitter1[1];
				String endYear = spilitter1[2];

				driver.findElement(By.xpath("//input[@id='end_date']")).click();
				String oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																									// Button
				while (!((endYear.contentEquals(oYear1)) && (endmonth.contentEquals(oMonth1)))) {
					Thread.sleep(2000);
					Next1.click();
					oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + endday + "'" + ")]"))
						.click();
				System.out.println("Entered End_Date on Faveo UI is :" + EndDate);

				String pedcase = TestCaseData[n][5].toString().trim(); // Yes or
																		// No
				//logger = extent.startTest("Selected pedcase is  " + pedcase);
				
				System.out.println("Ped case is  :" + pedcase);
				List<WebElement> peddropdown = driver.findElements(By.xpath(
						"//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/a"));
				int total_pedvalue = peddropdown.size();
			
				for (WebElement pedtypes : peddropdown) {
					
					pedtypes.click();
					if (pedcase.equalsIgnoreCase("No")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ pedcase + "')]"))
								.click();

					} else if (pedcase.equalsIgnoreCase("YES")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ pedcase + "')]"))
								.click();
					}
				}

				// Select Travellers from Excel
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						if (DropDownName.getText().equals("1")) {
							DropDownName.click();
							int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
							Thread.sleep(2000);
							clickElement(By
									.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"
											+ "'" + Travellers + "'" + ")]"));
							// clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
							System.out.println("Total Number of Travellers Selected : " + Travellers);
							break;
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Select Member.");
				}

				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][6].toString().trim());
				System.out.println("Total Number of Member in Excel : " + membersSize);
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				outer: for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));

						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][7];
						System.out.println("Members details is : " + Membersdetails);

						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split(",");
							member: for (int i = 0; i <= BaseClass.membres.length; i++) {

								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								System.out.println("value of Mcount Index : " + mcount);
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'Up to 40 years')]"))
										.click();

								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

								String text = FamilyData[mcount][0].toString().trim();

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

										Thread.sleep(1000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
											// break outer;
										}

									}

								}
							}
						}

					}

					else

					{
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().equals("Up to 40 years")) {

								String Membersdetails = TestCaseData[n][7];
								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");
								} else {
									// BaseClass.membres =
									// Membersdetails.split(" ");
									System.out.println("Hello");
								}
								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();

									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(1000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}

							}
						}
					}
				}

				System.out.println("Age selected");

				// Reading Mobile Number from Excel and Sending it on Faveo UI
				Thread.sleep(2000);
				String MobileNumber = TestCaseData[n][8].toString().trim();
				try {
					int size = MobileNumber.length();
					System.out.println("Entered Mobile number is: " + MobileNumber);
					if (isValid(MobileNumber)) {
						System.out.println("Entered Number is a valid number");
						enterText(By.xpath(MobileNumber_xpath), String.valueOf(MobileNumber));
					} else {
						System.out.println("Entered Number is Not a valid Number");
					}
				} catch (Exception e) {
					System.out.println("Unable to Enter Mobile Number.");
				}

				// Reading email from excel
				String email = TestCaseData[n][9].toString().trim();
				System.out.println("Email is :" + email);
				//logger = extent.startTest("Entered Email Id is  " + email);

				if (email.contains("@")) {
					driver.findElement(By.name("ValidEmail")).sendKeys(email);
				} else {
					System.out.println("Not a valid email");
				}
				Thread.sleep(5000);
				String suminsured_value = TestCaseData[n][10].toString().trim();
				System.out.println("Insured Value is  :" + suminsured_value);
				//logger = extent.startTest("Entered sum insured value is  " + suminsured_value);

				WebElement progress = driver
						.findElement(By.xpath("//*[@class='progress_slider_container col-md-12']/div/div"));

				List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
				String SliderValue = null;
				for (WebElement Slider : Slidnumber) {
					//System.out.println("Policy type is : " + Slider);
					SliderValue = suminsured_value;
					if (Slider.getText().equals(SliderValue.toString())) {
						Slider.click();
						break;
					}
				}
				
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,150)", "");
				clickElement(By.xpath("//button[text()='Buy Now']"));
				Thread.sleep(5000);
				
				// ********Proposer Details *****************************
				// proposer details script
				Thread.sleep(6000);
				// selecting Nationality and passort number based on excel
				String nationality = TestCaseData[n][11].toString().trim();
				System.out.println("Nationality is  : " + nationality);

				clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
				clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text()," + "'"
						+ nationality + "'" + ")]"));
				System.out.println("Entered Nationality is : " + nationality);

				String Passport = TestCaseData[n][12];
				// clearTextfield(By.xpath(PassportNumber_xpath));
				ExplicitWait1(By.xpath(PassportNumber_xpath));
				enterText(By.xpath(PassportNumber_xpath), Passport);
				Thread.sleep(2000);
				System.out.println("Entered Passport Number : " + Passport);
				String Title = TestCaseData[n][13].toString().trim();
				clickElement(By.xpath("//select[@name='ValidTitle']"));
				clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"
						+ Title + "'" + ")]"));
				Thread.sleep(3000);
				System.out.println("Entered Title is : " + Title);

				BaseClass.selecttext("ValidTitle", Title.toString());

				String FirstName = TestCaseData[n][14];
				clearTextfield(By.xpath(FirstName_xpath));
				enterText(By.xpath(FirstName_xpath), FirstName);
				Thread.sleep(3000);
				System.out.println("Entered First Name is : " + FirstName);
				String LastName = TestCaseData[n][15];
				clearTextfield(By.xpath(LastName_xpath));
				enterText(By.xpath(LastName_xpath), LastName);
				System.out.println("Entered Last Name is : " + LastName);

				// Entering DOB from Excel into dob field

				String DOB = TestCaseData[n][16].toString().trim();
				System.out.println("date is:" + DOB);

				try {
					clickElement(By.xpath(DOBCalender_xpath));
					clearTextfield(By.xpath(DOBCalender_xpath));
					enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
					driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

					System.out.println("Entered Proposer DOB is : " + DOB);
					Thread.sleep(3000);
				} catch (Exception e) {
					System.out.println("Unable to Enter Date of Birth Date.");
				}

				final String address1 = TestCaseData[n][17].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				clearTextfield(By.xpath(addressline1_xpath));
				enterText(By.xpath(addressline1_xpath), address1);
				clearTextfield(By.xpath(addressline2_xpath));
				enterText(By.xpath(addressline2_xpath), TestCaseData[n][18].toString().trim());
				clearTextfield(By.xpath(pincode_xpath));
				enterText(By.xpath(pincode_xpath), TestCaseData[n][19]);

				String NomineeName = TestCaseData[n][20].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
				enterText(By.xpath(Nominee_Name_xpqth), NomineeName);

				Actions action = new Actions(driver);
				action.sendKeys(Keys.ESCAPE);
				Thread.sleep(2000);

				String PurposeofVisit = TestCaseData[n][21];
				clickElement(By.xpath(PurposeofVisit_xpath));
				Thread.sleep(2000);
				driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"
						+ "'" + PurposeofVisit + "'" + ")]")).click();
				System.out.println("Selected Purpose of Visit is : " + PurposeofVisit);

				String pannumber = TestCaseData[n][27];
				try {
					WebElement pancard = driver.findElement(By.xpath("//*[@name='panCard']"));
					pancard.sendKeys(pannumber);
				} catch (Exception e1) {
					System.out.println("GroupExplore Travel pancard not available");
				}

				// ****** Insured Details Script here
				// ***************************************
				String Membersdetails = TestCaseData[n][7].toString().trim();
				int mcounti;

				if (Membersdetails.contains(",")) {

					// data taking form test case sheet which is
					// 7,4,1,8,2,5
					BaseClass.membres = Membersdetails.split(",");
				} else {
					BaseClass.membres = Membersdetails.split(" ");
				}
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {

						driver.findElement(By.xpath("//select[@name='ValidRelation0']")).click();
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {
						mcounti = Integer.parseInt(BaseClass.membres[i].toString());

						String Relation = FamilyData[mcounti][1].toString().trim();
						String InsuredNationality = FamilyData[mcounti][2].toString().trim();
						String InsuredPassport = FamilyData[mcounti][3].toString().trim();
						String InsuredTitle = FamilyData[mcounti][4].toString().trim();
						String InsuredFirstName = FamilyData[mcounti][5].toString().trim();
						String InsuredLastName = FamilyData[mcounti][6].toString().trim();
						String InsuredDOB = FamilyData[mcounti][7].toString().trim();

						BaseClass.selecttext("ValidRelation" + i, Relation);
						System.out.println("Selected Relation is : " + Relation);

						BaseClass.selecttext("rel_nationality" + i, InsuredNationality);
						System.out.println("Selected Nationality is : " + InsuredNationality);

						enterText(By.name("rel_passport" + i), InsuredPassport);
						System.out.println("Entered Passport is : " + InsuredPassport);

						clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']"));
						Thread.sleep(2000);
						// clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+Group_Travel[mcount][4].toString().trim()+"'"+"]"));
						String GInsuredTitle = FamilyData[mcounti][4].toString().trim();
						int persontitle;
						if (GInsuredTitle.contains("Mr")) {
							persontitle = 2;
						} else {
							persontitle = 3;
						}

						clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']//option[" + persontitle + "]"));
						Thread.sleep(5000);
						if (Relation.contains("Self-primary")) {
							enterText(By.name("RelFName" + i), InsuredFirstName);
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						} else {
							clearTextfield(By.name("RelFName" + i));
							enterText(By.name("RelFName" + i), InsuredFirstName);
							clearTextfield(By.name("RelLName" + i));
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						}

						if (Relation.contains("Self-primary")) {
							try {
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						} else {
							try {
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								clearTextfield(By.xpath("//input[@name='rel_dob" + i + "']"));
								Thread.sleep(2000);
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						}

					}
				}
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox_TravelInsurance(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForTravelGroupExplore(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicyTravel();
				
			//==================================Health questionnaires=====================
				BaseClass.scrolldown();
				String preExistingdeases = TestCaseData[n][22].toString().trim();
				int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
				Thread.sleep(2000);
				System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				try{
					 if(preExistingdeases.equalsIgnoreCase("yes"))
					 {
						
						 waitForElements(By.xpath(PEDYesButton_xpath));
						 clickElement(By.xpath(PEDYesButton_xpath));
					
							String[] ChckData = null;
							int datacheck1 = 0;
								String HealthQuestion = Group_Question[n][1].toString().trim();
								System.out.println("Member Having PED : "+HealthQuestion);
								for(int i=0;i<Travellers;i++)
								{
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='NO']"));
								}	
								
								if (HealthQuestion.contains(",")) 
									{
						
										ChckData = HealthQuestion.split(",");
										for (String Chdata : ChckData) 
										{
											datacheck1 = Integer.parseInt(Chdata);
											datacheck1 = datacheck1 - 1;
											
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
											
										}
									}
									else if (HealthQuestion.contains(""))
									{
										datacheck1 = Integer.parseInt(HealthQuestion);
										datacheck1 = datacheck1-1;
										
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
									}
								
										String[] ChckData1 = null;
										int detailsnumber = 0;
										
										for(int qlist=2;qlist<=7;qlist++) 
										{
											
											String Details =Group_Question[n][qlist].toString().trim();
											int q1list = qlist+1;
											
											 if(Details.equals("")) 
											 {
												 System.out.println("Please Enter Some value in Excel to Select PED.");
											 }
											 else if(Details.contains(",")) 
											 {
												 
												 ChckData1 = Details.split(",");
													
												 for (String Chdata1 : ChckData1) 
												{
													 
												 detailsnumber = Integer.parseInt(Chdata1);
												 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
												
												}
											 }

												else if (Details.contains(""))
												{
													detailsnumber = Integer.parseInt(Details);
													 try{
													 driver.findElement(By.xpath("//html//div["+qlist+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']")).click();
													 }catch(Exception e)
													 {
													System.out.println("Not able to click on Sub Questions Values of Question 1.");	 
													 }
													 
													 }

										}
										
										
										String[] AlimnetsQuestionData = null;
										for(int qlist=7;qlist<=7;qlist++) 
										{
										String Details =Group_Question[n][qlist].toString().trim();
										String AlimnetsQuestion =Group_Question[n][7].toString().trim();
										String OtherQuestionDetails =Group_Question[n][8].toString().trim();

										if(AlimnetsQuestion.equals("")) 
										{
											System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
											
										 }else if(AlimnetsQuestion.contains(","))
										 {
											 AlimnetsQuestionData = AlimnetsQuestion.split(",");
											 for(String AlimntQuesnData : AlimnetsQuestionData)
											{
											 detailsnumber = Integer.parseInt(AlimntQuesnData);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
											}
											 }
										 else if(AlimnetsQuestion.contains(""))
										 {
											 detailsnumber = Integer.parseInt(AlimnetsQuestion);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
										 }
					}									
					 }
				else if (preExistingdeases.equalsIgnoreCase("no")) 
				{
							 	clickElement(By.xpath(PEDNoButton_xpath));
				}
				}
				catch(Exception e)
						{
						System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
							//logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
						}
				
				
				//2nd Question injury during the last 48 months?
				String HealthQuestion = Group_Question[n][1].toString().trim();
				System.out.println("Member Having PED : "+HealthQuestion);
				
				String QuestionNumber2 = TestCaseData[n][23].toString().trim();
				String QuestionNumber2Details = TestCaseData[n][24].toString().trim();
				String[] CheckData = null;
				int DataCheck=0;
				try{
					if(QuestionNumber2.equalsIgnoreCase("no"))
					{
						System.out.println("Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
						clickElement(By.xpath(PED_2_NoButton_xpath));
					}else
					{
						System.out.println("Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");				
						System.out.println("Member Who have injured During last 48 months : "+QuestionNumber2);
						clickElement(By.xpath(PED_2_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'No')]")).click();
						}
						
						if (HealthQuestion.contains(",")) 
						{
							CheckData = HealthQuestion.split(",");
							for (String Chdata : CheckData) 
							{
								
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
								System.out.println("Reason of Injury : "+QuestionNumber2Details);
									
						}
						} 
						else if (HealthQuestion.contains(""))
						{
							DataCheck = Integer.parseInt(HealthQuestion);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
							System.out.println("Reason of Injury : "+QuestionNumber2Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 2.");
				}
				

				//3rd Question injury during the last 48 months?
				
				String QuestionNumber3 = TestCaseData[n][25].toString().trim();
				String QuestionNumber3Details = TestCaseData[n][26].toString().trim();
				try{
					if(QuestionNumber3.equalsIgnoreCase("no"))
					{
						System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
						clickElement(By.xpath(PED_3_NoButton_xpath));
					}else
					{
						System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
						System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
						clickElement(By.xpath(PED_3_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']//option[contains(text(),'No')]")).click();
						}
						if (HealthQuestion.contains(",")) 
						{
							CheckData = HealthQuestion.split(",");
							for (String Chdata : CheckData) 
							{
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
						}
						} 
						else if (HealthQuestion.contains(""))
						{
							DataCheck = Integer.parseInt(HealthQuestion);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
							System.out.println("Reason of Injury : "+QuestionNumber3Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 3.");
				}
				clickElement(By.xpath("//input[@id='termsCheckbox1']"));
				clickElement(By.xpath("//input[@id='tripStart']"));
				clickElement(By.xpath("//input[@id='termsCheckbox2']"));
				
				Thread.sleep(2000);
				clickElement(By.xpath("//button[text()='Proceed to Pay']"));
				
				BaseClass.ErroronHelathquestionnaire();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelGroupExplore(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				driver.quit();
			}

			catch (Exception e) {
				// WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.quit();
			}

		}

	}
	
	//********* Test Case 2 Verify Data of Share Proposal in Draft History and Email **************************************************
	@Test(enabled=true, priority=2)
	public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		String[][] TestCase = BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData = BaseClass.excel_Files("GroupExplore_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("GroupExplore_FamilyData");
		String[][] Group_Question = BaseClass.excel_Files("Group_Question");

		for (n = 1; n <=6; n++) {

			try {

				Thread.sleep(5000);
				String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
				logger = extent.startTest("GroupExplore Travel " + TestCaseName);
				System.out.println("GroupExplore Travel " + TestCaseName);

				// From Here we are Calling Login Class Method to Login in Faveo

				System.out.println("++++++++++++++Group Expolre starts here+++++++++++++++");

				
				//Step 1 - Open Email and Delete Old Email
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
				
				LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a proposal
				System.out.println("Step 2 - Go to Application and share a proposal");
				logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				Thread.sleep(5000);
				clickElement(By.xpath(travelinsurance_xpath));
				Thread.sleep(4000);
				clickElement(By.xpath(GroupExplore_xpath));
				Thread.sleep(5000);
				List<WebElement> traveldropdown = driver.findElements(
						By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
				int dropsize = traveldropdown.size();
				System.out.println("Total dropdowns are :" + dropsize);
				String travelingto = TestCaseData[n][1].toString().trim();
				String scheme = TestCaseData[n][2].toString().trim();
				//logger = extent.startTest("Selected Travelling to place is  " + travelingto);
				//logger = extent.startTest("Selected Scheme is  " + scheme);

				Thread.sleep(5000);
				for (WebElement dropdownvalue : traveldropdown) {
					dropdownvalue.click();
					// System.out.println("Text is : "+dropdownvalue.getText());
					Thread.sleep(3000);
					if (dropdownvalue.getText().contains("Asia")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/ul/li//a[contains(text(),'"
										+ travelingto + "')]"))
								.click();// select 4 based on the excel data
						Thread.sleep(5000);
						// break;
					}
					if (dropdownvalue.getText().contains("Budget") || dropdownvalue.getText().contains("Explore")
							|| dropdownvalue.getText().contains("Premium")
							|| dropdownvalue.getText().contains("Premium Plus")
							|| dropdownvalue.getText().contains("Saver")
							|| dropdownvalue.getText().contains("Economy")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[2]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ scheme + "')]"))
								.click();// select 4 based on the excel data
						Thread.sleep(5000);
						break;
					}

				}
				
				String StartDate = TestCaseData[n][3].toString().trim();
				//logger = extent.startTest("Selected Start date is  " + StartDate);
				System.out.println("Excel from date is  :" + StartDate);
				String monthdate[] = StartDate.split(",");
				String excelday = monthdate[0];
				String excelmonth = monthdate[1];
				String excelYear = monthdate[2];
				driver.findElement(By.xpath("//input[@id='start_date']")).click();
				String CMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String CYears = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				while (!((excelYear.contentEquals(CYears)) && (excelmonth.contentEquals(CMonth)))) {
					Thread.sleep(5000);
					next.click();
					CMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					CYears = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + excelday + "'" + ")]"))
						.click();
				System.out.println("Entered Start_Date on Faveo UI is for Group Explore is:" + StartDate);

				String EndDate = TestCaseData[n][4].toString().trim();
				String spilitter1[] = EndDate.split(",");
				String endday = spilitter1[0];
				String endmonth = spilitter1[1];
				String endYear = spilitter1[2];

				driver.findElement(By.xpath("//input[@id='end_date']")).click();
				String oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																									// Button
				while (!((endYear.contentEquals(oYear1)) && (endmonth.contentEquals(oMonth1)))) {
					Thread.sleep(2000);
					Next1.click();
					oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + endday + "'" + ")]"))
						.click();
				System.out.println("Entered End_Date on Faveo UI is :" + EndDate);

				String pedcase = TestCaseData[n][5].toString().trim(); // Yes or
																		// No
				//logger = extent.startTest("Selected pedcase is  " + pedcase);
				
				System.out.println("Ped case is  :" + pedcase);
				List<WebElement> peddropdown = driver.findElements(By.xpath(
						"//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/a"));
				int total_pedvalue = peddropdown.size();
			
				for (WebElement pedtypes : peddropdown) {
					
					pedtypes.click();
					if (pedcase.equalsIgnoreCase("No")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ pedcase + "')]"))
								.click();

					} else if (pedcase.equalsIgnoreCase("YES")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ pedcase + "')]"))
								.click();
					}
				}

				// Select Travellers from Excel
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						if (DropDownName.getText().equals("1")) {
							DropDownName.click();
							int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
							Thread.sleep(2000);
							clickElement(By
									.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"
											+ "'" + Travellers + "'" + ")]"));
							// clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
							System.out.println("Total Number of Travellers Selected : " + Travellers);
							break;
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Select Member.");
				}

				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][6].toString().trim());
				System.out.println("Total Number of Member in Excel : " + membersSize);
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				outer: for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));

						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][7];
						System.out.println("Members details is : " + Membersdetails);

						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split(",");
							member: for (int i = 0; i <= BaseClass.membres.length; i++) {

								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								System.out.println("value of Mcount Index : " + mcount);
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'Up to 40 years')]"))
										.click();

								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

								String text = FamilyData[mcount][0].toString().trim();

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

										Thread.sleep(1000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
											// break outer;
										}

									}

								}
							}
						}

					}

					else

					{
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().equals("Up to 40 years")) {

								String Membersdetails = TestCaseData[n][7];
								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");
								} else {
									// BaseClass.membres =
									// Membersdetails.split(" ");
									System.out.println("Hello");
								}
								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();

									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(1000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}

							}
						}
					}
				}

				System.out.println("Age selected");

				// Reading Mobile Number from Excel and Sending it on Faveo UI
				Thread.sleep(2000);
				String MobileNumber = TestCaseData[n][8].toString().trim();
				try {
					int size = MobileNumber.length();
					System.out.println("Entered Mobile number is: " + MobileNumber);
					if (isValid(MobileNumber)) {
						System.out.println("Entered Number is a valid number");
						enterText(By.xpath(MobileNumber_xpath), String.valueOf(MobileNumber));
					} else {
						System.out.println("Entered Number is Not a valid Number");
					}
				} catch (Exception e) {
					System.out.println("Unable to Enter Mobile Number.");
				}

				// Reading email from excel
				String email = TestCaseData[n][9].toString().trim();
				System.out.println("Email is :" + email);
				//logger = extent.startTest("Entered Email Id is  " + email);

				if (email.contains("@")) {
					driver.findElement(By.name("ValidEmail")).sendKeys(email);
				} else {
					System.out.println("Not a valid email");
				}
				Thread.sleep(5000);
				String suminsured_value = TestCaseData[n][10].toString().trim();
				System.out.println("Insured Value is  :" + suminsured_value);
				//logger = extent.startTest("Entered sum insured value is  " + suminsured_value);

				WebElement progress = driver
						.findElement(By.xpath("//*[@class='progress_slider_container col-md-12']/div/div"));

				List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
				String SliderValue = null;
				for (WebElement Slider : Slidnumber) {
					//System.out.println("Policy type is : " + Slider);
					SliderValue = suminsured_value;
					if (Slider.getText().equals(SliderValue.toString())) {
						Slider.click();
						break;
					}
				}
				
				
				ReadDataFromEmail_TravelInsurance.readDates();
				ReadDataFromEmail_TravelInsurance.readAgeGroupTravel();
				AddonsforProducts.readPremiumFromFirstPage_Travel();
				
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,150)", "");
				clickElement(By.xpath("//button[text()='Buy Now']"));
				Thread.sleep(5000);
				
				// ********Proposer Details *****************************
				// proposer details script
				Thread.sleep(6000);
				// selecting Nationality and passort number based on excel
				String nationality = TestCaseData[n][11].toString().trim();
				System.out.println("Nationality is  : " + nationality);

				clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
				clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text()," + "'"
						+ nationality + "'" + ")]"));
				System.out.println("Entered Nationality is : " + nationality);

				String Passport = TestCaseData[n][12];
				// clearTextfield(By.xpath(PassportNumber_xpath));
				ExplicitWait1(By.xpath(PassportNumber_xpath));
				enterText(By.xpath(PassportNumber_xpath), Passport);
				Thread.sleep(2000);
				System.out.println("Entered Passport Number : " + Passport);
				String Title = TestCaseData[n][13].toString().trim();
				clickElement(By.xpath("//select[@name='ValidTitle']"));
				clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"
						+ Title + "'" + ")]"));
				Thread.sleep(3000);
				System.out.println("Entered Title is : " + Title);

				BaseClass.selecttext("ValidTitle", Title.toString());

				String FirstName = TestCaseData[n][14];
				clearTextfield(By.xpath(FirstName_xpath));
				enterText(By.xpath(FirstName_xpath), FirstName);
				Thread.sleep(3000);
				System.out.println("Entered First Name is : " + FirstName);
				String LastName = TestCaseData[n][15];
				clearTextfield(By.xpath(LastName_xpath));
				enterText(By.xpath(LastName_xpath), LastName);
				System.out.println("Entered Last Name is : " + LastName);

				// Entering DOB from Excel into dob field

				String DOB = TestCaseData[n][16].toString().trim();
				System.out.println("date is:" + DOB);

				try {
					clickElement(By.xpath(DOBCalender_xpath));
					clearTextfield(By.xpath(DOBCalender_xpath));
					enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
					driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

					System.out.println("Entered Proposer DOB is : " + DOB);
					Thread.sleep(3000);
				} catch (Exception e) {
					System.out.println("Unable to Enter Date of Birth Date.");
				}

				final String address1 = TestCaseData[n][17].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				clearTextfield(By.xpath(addressline1_xpath));
				enterText(By.xpath(addressline1_xpath), address1);
				clearTextfield(By.xpath(addressline2_xpath));
				enterText(By.xpath(addressline2_xpath), TestCaseData[n][18].toString().trim());
				clearTextfield(By.xpath(pincode_xpath));
				enterText(By.xpath(pincode_xpath), TestCaseData[n][19]);

				String NomineeName = TestCaseData[n][20].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
				enterText(By.xpath(Nominee_Name_xpqth), NomineeName);

				Actions action = new Actions(driver);
				action.sendKeys(Keys.ESCAPE);
				Thread.sleep(2000);

				String PurposeofVisit = TestCaseData[n][21];
				clickElement(By.xpath(PurposeofVisit_xpath));
				Thread.sleep(2000);
				driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"
						+ "'" + PurposeofVisit + "'" + ")]")).click();
				System.out.println("Selected Purpose of Visit is : " + PurposeofVisit);

				String pannumber = TestCaseData[n][27];
				try {
					WebElement pancard = driver.findElement(By.xpath("//*[@name='panCard']"));
					pancard.sendKeys(pannumber);
				} catch (Exception e1) {
					System.out.println("GroupExplore Travel pancard not available");
				}

				// ****** Insured Details Script here
				// ***************************************
				String Membersdetails = TestCaseData[n][7].toString().trim();
				int mcounti;

				if (Membersdetails.contains(",")) {

					// data taking form test case sheet which is
					// 7,4,1,8,2,5
					BaseClass.membres = Membersdetails.split(",");
				} else {
					BaseClass.membres = Membersdetails.split(" ");
				}
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {

						driver.findElement(By.xpath("//select[@name='ValidRelation0']")).click();
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {
						mcounti = Integer.parseInt(BaseClass.membres[i].toString());

						String Relation = FamilyData[mcounti][1].toString().trim();
						String InsuredNationality = FamilyData[mcounti][2].toString().trim();
						String InsuredPassport = FamilyData[mcounti][3].toString().trim();
						String InsuredTitle = FamilyData[mcounti][4].toString().trim();
						String InsuredFirstName = FamilyData[mcounti][5].toString().trim();
						String InsuredLastName = FamilyData[mcounti][6].toString().trim();
						String InsuredDOB = FamilyData[mcounti][7].toString().trim();

						BaseClass.selecttext("ValidRelation" + i, Relation);
						System.out.println("Selected Relation is : " + Relation);

						BaseClass.selecttext("rel_nationality" + i, InsuredNationality);
						System.out.println("Selected Nationality is : " + InsuredNationality);

						enterText(By.name("rel_passport" + i), InsuredPassport);
						System.out.println("Entered Passport is : " + InsuredPassport);

						clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']"));
						Thread.sleep(2000);
						// clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+Group_Travel[mcount][4].toString().trim()+"'"+"]"));
						String GInsuredTitle = FamilyData[mcounti][4].toString().trim();
						int persontitle;
						if (GInsuredTitle.contains("Mr")) {
							persontitle = 2;
						} else {
							persontitle = 3;
						}

						clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']//option[" + persontitle + "]"));
						Thread.sleep(5000);
						if (Relation.contains("Self-primary")) {
							enterText(By.name("RelFName" + i), InsuredFirstName);
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						} else {
							clearTextfield(By.name("RelFName" + i));
							enterText(By.name("RelFName" + i), InsuredFirstName);
							clearTextfield(By.name("RelLName" + i));
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						}

						if (Relation.contains("Self-primary")) {
							try {
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						} else {
							try {
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								clearTextfield(By.xpath("//input[@name='rel_dob" + i + "']"));
								Thread.sleep(2000);
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						}

					}
				}
				
				//Set Details of Share Proposal Popup Box
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox_TravelInsurance(n);
		
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				driver.quit();
				
				 //Step 3- Again Open the email and verify the data of Share Proposal in Email Body
				System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
				
			    LaunchBrowser();
			    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForTravelGroupExplore(n);
			    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Step 4 - Click on Buy Now button from Email Body and punch the policy
				System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
				logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
				
				ReadDataFromEmail_TravelInsurance.ClickOnBuyNowButtonFromEmail();
				
			//==================================Health questionnaires=====================
				
				String preExistingdeases = TestCaseData[n][22].toString().trim();
				int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
				Thread.sleep(5000);
				BaseClass.scrolldown();
				System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				try{
					 if(preExistingdeases.equalsIgnoreCase("yes"))
					 {
						
						 waitForElements(By.xpath(PEDYesButton_xpath));
						 clickElement(By.xpath(PEDYesButton_xpath));
					
							String[] ChckData = null;
							int datacheck1 = 0;
								String HealthQuestion = Group_Question[n][1].toString().trim();
								System.out.println("Member Having PED : "+HealthQuestion);
								for(int i=0;i<Travellers;i++)
								{
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='NO']"));
								}	
								
								if (HealthQuestion.contains(",")) 
									{
						
										ChckData = HealthQuestion.split(",");
										for (String Chdata : ChckData) 
										{
											datacheck1 = Integer.parseInt(Chdata);
											datacheck1 = datacheck1 - 1;
											
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
											
										}
									}
									else if (HealthQuestion.contains(""))
									{
										datacheck1 = Integer.parseInt(HealthQuestion);
										datacheck1 = datacheck1-1;
										
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
									}
								
										String[] ChckData1 = null;
										int detailsnumber = 0;
										
										for(int qlist=2;qlist<=7;qlist++) 
										{
											
											String Details =Group_Question[n][qlist].toString().trim();
											int q1list = qlist+1;
											
											 if(Details.equals("")) 
											 {
												 System.out.println("Please Enter Some value in Excel to Select PED.");
											 }
											 else if(Details.contains(",")) 
											 {
												 
												 ChckData1 = Details.split(",");
													
												 for (String Chdata1 : ChckData1) 
												{
													 
												 detailsnumber = Integer.parseInt(Chdata1);
												 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
												
												}
											 }

												else if (Details.contains(""))
												{
													detailsnumber = Integer.parseInt(Details);
													 try{
													 driver.findElement(By.xpath("//html//div["+qlist+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']")).click();
													 }catch(Exception e)
													 {
													System.out.println("Not able to click on Sub Questions Values of Question 1.");	 
													 }
													 
													 }

										}
										
										
										String[] AlimnetsQuestionData = null;
										for(int qlist=7;qlist<=7;qlist++) 
										{
										String Details =Group_Question[n][qlist].toString().trim();
										String AlimnetsQuestion =Group_Question[n][7].toString().trim();
										String OtherQuestionDetails =Group_Question[n][8].toString().trim();

										if(AlimnetsQuestion.equals("")) 
										{
											System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
											
										 }else if(AlimnetsQuestion.contains(","))
										 {
											 AlimnetsQuestionData = AlimnetsQuestion.split(",");
											 for(String AlimntQuesnData : AlimnetsQuestionData)
											{
											 detailsnumber = Integer.parseInt(AlimntQuesnData);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
											}
											 }
										 else if(AlimnetsQuestion.contains(""))
										 {
											 detailsnumber = Integer.parseInt(AlimnetsQuestion);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
										 }
					}									
					 }
				else if (preExistingdeases.equalsIgnoreCase("no")) 
				{
							 	clickElement(By.xpath(PEDNoButton_xpath));
				}
				}
				catch(Exception e)
						{
						System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
							//logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
						}
				
				
				//2nd Question injury during the last 48 months?
				String HealthQuestion = Group_Question[n][1].toString().trim();
				System.out.println("Member Having PED : "+HealthQuestion);
				
				String QuestionNumber2 = TestCaseData[n][23].toString().trim();
				String QuestionNumber2Details = TestCaseData[n][24].toString().trim();
				String[] CheckData = null;
				int DataCheck=0;
				try{
					if(QuestionNumber2.equalsIgnoreCase("no"))
					{
						System.out.println("Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
						clickElement(By.xpath(PED_2_NoButton_xpath));
					}else
					{
						System.out.println("Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");				
						System.out.println("Member Who have injured During last 48 months : "+QuestionNumber2);
						clickElement(By.xpath(PED_2_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'No')]")).click();
						}
						
						if (HealthQuestion.contains(",")) 
						{
							CheckData = HealthQuestion.split(",");
							for (String Chdata : CheckData) 
							{
								
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
								System.out.println("Reason of Injury : "+QuestionNumber2Details);
									
						}
						} 
						else if (HealthQuestion.contains(""))
						{
							DataCheck = Integer.parseInt(HealthQuestion);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
							System.out.println("Reason of Injury : "+QuestionNumber2Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 2.");
				}
				

				//3rd Question injury during the last 48 months?
				
				String QuestionNumber3 = TestCaseData[n][25].toString().trim();
				String QuestionNumber3Details = TestCaseData[n][26].toString().trim();
				try{
					if(QuestionNumber3.equalsIgnoreCase("no"))
					{
						System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
						clickElement(By.xpath(PED_3_NoButton_xpath));
					}else
					{
						System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
						System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
						clickElement(By.xpath(PED_3_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']//option[contains(text(),'No')]")).click();
						}
						if (HealthQuestion.contains(",")) 
						{
							CheckData = HealthQuestion.split(",");
							for (String Chdata : CheckData) 
							{
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
						}
						} 
						else if (HealthQuestion.contains(""))
						{
							DataCheck = Integer.parseInt(HealthQuestion);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
							System.out.println("Reason of Injury : "+QuestionNumber3Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 3.");
				}
				clickElement(By.xpath("//input[@id='termsCheckbox1']"));
				clickElement(By.xpath("//input[@id='tripStart']"));
				clickElement(By.xpath("//input[@id='termsCheckbox2']"));
				
				Thread.sleep(2000);
				clickElement(By.xpath("//button[text()='Proceed to Pay']"));
				
				BaseClass.ErroronHelathquestionnaire();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				driver.quit();
				
				
				
				 //Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
	
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelGroupExplore(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				driver.quit();
				
				 //Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail_TravelInsurance.openEmailAndClickOnBuyNowButtonFromEmail();

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();   

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				driver.quit();  

			}
			
			catch (AssertionError e) {
				// WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.quit();
			}


			catch (Exception e) {
				// WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.quit();
			}

		}

	}
}