package com.org.faveo.travelInsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.ReadDataFromEmail_TravelInsurance;
import com.org.faveo.Base.TravelInsuranceDropDown;
import com.org.faveo.PolicyJourney.DomesticTravelPageJourney;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class DomesticTravel extends BaseClass implements AccountnSettingsInterface{
	public static Integer n;
	
	@Test(priority=1, enabled=false)
	public static void domesticTravel() throws Exception {
		System.out.println("++++++++++++++Domestic Travel Execution starts here+++++++++++++++");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("DomesticTravel_Testcase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		String[][] TestCase=BaseClass.excel_Files("DomesticTravel_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");
		
		for(n=1;n<rowCount;n++) {
			try {
							
		BaseClass.LaunchBrowser();
		Thread.sleep(5000);
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Domestic Travel " + TestCaseName);
		System.out.println("Domestic Travel " + TestCaseName);
		
		//From Here we are Calling Login Class Method to Login in Faveo 
		LoginCase.LoginwithValidCredendial();
		
		TravelInsuranceDropDown.DomesticTravelpolicy();
		DomesticTravelPageJourney.DomesticTravelQuotationdetails();
		DomesticTravelPageJourney.DomesticTravelPremium();
		DomesticTravelPageJourney.DomesticTravelproposerDetails();
		DomesticTravelPageJourney.DomesticTravelInsuredDetails();
		DomesticTravelPageJourney.DomesticTravelProposalSummaryDetailsWithPayment();
		
			}catch(Exception e) {
				WriteExcel.setCellData("DomesticTravel_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				 driver.quit();
			}
			continue;
		}
}
	
	//******************************** Share Quote Test Case ********************************************
	
	public static String[][] TestCaseData;
	@Test(priority=2, enabled=false)
	public static void domesticTravel_ShareQuote() throws Exception {
		System.out.println("++++++++++++++Domestic Travel Execution starts here+++++++++++++++");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		String[][] TestCase=BaseClass.excel_Files("ShareQuotation");
		 TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");
		
		for(n=4;n<=5;n++) {
			try {
							
		
		Thread.sleep(5000);
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Domestic Travel " + TestCaseName);
		System.out.println("Domestic Travel " + TestCaseName);
		
		// Step 1 - Open Email and Delete any Old Email
		System.out.println("Step 1 - Open Email and Delete Old Email");
		logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit();

		// Step 2 - Go to Application and share a Quote and Verify Data
		// in Quotation Tracker
		System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
		logger.log(LogStatus.PASS,
		"Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
				
		TravelInsuranceDropDown.DomesticTravelpolicy();
		DomesticTravelPageJourney.DomesticTravelQuotationdetails();
		
		//This logic implemented because once select one way trip another calendar is getting hide
		String triptype=TestCaseData[n][1].toString().trim();
		if(triptype.equalsIgnoreCase("One Way Trip")){
		System.out.println("Single Trip Date Selected");
		}
		else{
			ReadDataFromEmail_TravelInsurance.readDates();
		}
		
		
		ReadDataFromEmail_TravelInsurance.readAgeGroupTravel();
		AddonsforProducts.readPremiumFromFirstPage_Travel();
		
		DataVerificationShareQuotationPage.clickOnShareQuotationButton();
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox_TravelInsurance(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_DomesticTravel(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();  
        
        // Step 3 - Open Email and Verify Data in Email Body
     	System.out.println("Step 3 - Open Email and Verify Data in Email Body");
     	logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
     	LaunchBrowser();
     	DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForTravelInsurance_DomesticTravel(n);
     	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
     	
     // Step 4 - Click on Buy Now Button from Email Body and punch
    	// the Policy
    	System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
    	logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
    	ReadDataFromEmail_TravelInsurance.ClickOnBuyNowButtonFromEmail();
    	
    	DomesticTravelPageJourney.DomesticTravelproposerDetails();
    	DomesticTravelPageJourney.DomesticTravelInsuredDetails();
    	
    	System.out.println("Before Pay U");
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		System.out.println("After Pay U");
		
		PayuPage_Credentials();
		driver.quit();
		
		// Step 5 - Again Login the application and verify after punch
		// the policy data should be not available in quotation Tracker
		System.out.println(
				"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		logger.log(LogStatus.PASS,
				"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
		LaunchBrowser();
		// Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareQuotationPage
				.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_DomesticTravel(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();

		// Step 6- Again Open the Email and Verify GUID Should be
		// Reusable
		System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
		logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

		LaunchBrowser();
		ReadDataFromEmail_TravelInsurance.openEmailAndClickOnBuyNowButtonFromEmail();

		// Verify the Page Title
		DataVerificationShareProposalPage.verifyPageTitle();

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
	     driver.quit();   
		
			}catch(Exception e) {
				//WriteExcel.setCellData("DomesticTravel_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
			    driver.quit();
			}
			continue;
		}
}
	
//******************* Share Proposal Test Cases **************************************************
	
	@Test(priority=2, enabled=true)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		String[][] TestCase=BaseClass.excel_Files("ShareProposal");
		 TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");
		
		for(n=1;n<=1;n++) {
		try {
							
		
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Domestic Travel " + TestCaseName);
		System.out.println("Domestic Travel " + TestCaseName);
		
		// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
		System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
		logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
		
		LaunchBrowser();
		
		Thread.sleep(5000);
		LoginCase.LoginwithValidCredendial();
				
		TravelInsuranceDropDown.DomesticTravelpolicy();
		DomesticTravelPageJourney.DomesticTravelQuotationdetails();
		
		//This logic implemented because once select one way trip another calendar is getting hide
		/*String triptype=TestCaseData[n][1].toString().trim();
		
		if(triptype.equalsIgnoreCase("One Way Trip")){
		
		System.out.println("Single Trip Date Selected");
		}
		else{
			ReadDataFromEmail_TravelInsurance.readDates();
		}*/
		
		
		//ReadDataFromEmail_TravelInsurance.readAgeGroupTravel();
		//AddonsforProducts.readPremiumFromFirstPage_Travel();
		
		/*DataVerificationShareQuotationPage.clickOnShareQuotationButton();
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox_TravelInsurance(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_DomesticTravel(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();  */
		
		waitForElement(By.xpath(posStudent_buynow_xpath));
		clickElement(By.xpath(posStudent_buynow_xpath));
        
    	DomesticTravelPageJourney.DomesticTravelproposerDetails();
    	DomesticTravelPageJourney.DomesticTravelInsuredDetails2();
    	
    	//Set Details of Share Proposal Popup Box and Verify Data in Draft History
    	DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox_TravelInsurance(n);
    	DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForDomesticTravel(n);
    			
    	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
    	logger.log(LogStatus.PASS, "Test Case Passed");
    			
    	//Click on Resume Policy
    	DataVerificationShareProposalPage.setDetailsAfterResumePolicyTravel();
    	
    	DomesticTravelPageJourney.DomesticTravelproposerDetails();
    	//DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
    	DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
    	
    	
    	Thread.sleep(5000);
    	JavascriptExecutor js1 = (JavascriptExecutor) driver;
    	js1.executeScript("window.scrollBy(0,400)");
    	driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
    	driver.findElement(By.xpath("//input[@id='tripStart']")).click();
    	driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
    			
    	
    	System.out.println("Before Pay U");
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		System.out.println("After Pay U");
		
		PayuPage_Credentials();
		driver.quit();
		
		//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
		System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");

		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForDomesticTravel(n);

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");	
		driver.quit();
		
		
			} catch (Exception e) {
				// WriteExcel.setCellData("DomesticTravel_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				//driver.quit();
			}
			continue;
		}
}
	
}