package com.org.faveo.travelInsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class POSStudentExplore  extends BaseClass implements AccountnSettingsInterface{

	public static String PolicyType = null;
	public static String TypeTest =null;
	
	@Test
	public static void StudentExploreTravel() throws Exception
	{
	
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("POS_StudentExplore_TestCase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		String[][] TestCase=BaseClass.excel_Files("POS_StudentExplore_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("POS_StudentExplore_Quotation");
		String[][] StudentExploreInsuredDetails = BaseClass.excel_Files("POSStudentExploreInsuredDetails");
		
	for (int n=2;n<2;n++) {
		try {
			String data=TestCase[n][0].toString().trim();
			System.out.println(data);
		BaseClass.LaunchBrowser();
		//We are Taking Test Case name from Excel and Printing it in our Report
		Thread.sleep(5000);
				String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
				logger = extent.startTest("POS Student Explore Travel " + TestCaseName);
				System.out.println("POS Student Explore Travel " + TestCaseName);
				
				//From Here we are Calling Login Class Method to Login in Faveo 
				LoginCase.LoginwithValidCredendial();
				
				System.out.println("++++++++++++++Student Expolre starts here+++++++++++++++");
				
				PolicyType = TypeTest;
			
				
					Thread.sleep(10000);
					clickElement(By.xpath(travelinsurance_xpath));
					Thread.sleep(5000);
					clickElement(By.xpath(posstudentExplore_xpath));
					Thread.sleep(5000);
				String email=TestCaseData[n][1].toString().trim();
				String mobilenumber=TestCaseData[n][2].toString().trim();
				System.out.println("Email is :" + email);
				Thread.sleep(4000);
				if (email.contains("@")) {
					driver.findElement(By.name("ValidEmail")).sendKeys(email);
				} else {
					System.out.println("Not a valid email");
				}
				logger.log(LogStatus.PASS, "Entered Email id: " + email);
				int size = mobilenumber.length();
				System.out.println("mobile number is: " + mobilenumber);
				String format = "^[789]\\d{9}$";

				Thread.sleep(4000);

				if (mobilenumber.matches(format) && size == 10) {
					driver.findElement(By.name("mobileNumber")).sendKeys(mobilenumber);
				} else {
					System.out.println(" Not a valid mobile  Number");
				}
				logger.log(LogStatus.PASS, "Entered Mobile nunmber is : " + mobilenumber);
				String GeographicalScope=TestCaseData[n][4].toString().trim();	
				System.out.println("Scope is : "+GeographicalScope);
				logger.log(LogStatus.PASS, "Selected geographical scope is : " + GeographicalScope);
				String PolicyTenure=TestCaseData[n][5].toString().trim();
				System.out.println("Policy Tenure is  : "+ PolicyTenure);
				logger.log(LogStatus.PASS, "Selected tenure is : " + PolicyTenure);
				String planType=TestCaseData[n][6].toString().trim();
				System.out.println(" Plan type is :  "+ planType);	
				logger.log(LogStatus.PASS, "Selected planType is : " + planType);
				String ped=TestCaseData[n][7].toString().trim();
				System.out.println(" Plan type is :  "+ ped);
				logger.log(LogStatus.PASS, "Selected ped is : " + ped);

				String suminsured=TestCaseData[n][8].toString().trim();
				System.out.println("Sum Insured is   :" +suminsured);
				logger.log(LogStatus.PASS, "Selected suminsured is : " + suminsured);
				List<WebElement> ddowns=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
				int dropsize=ddowns.size();
				System.out.println("Total dropdowns are :"  +dropsize);
				int x=0;
				for(WebElement dropdownvalue:ddowns){	
					dropdownvalue.click();
					Thread.sleep(3000);
					String Ageexvalue=TestCaseData[n][3+x].toString().trim();
					System.out.println("Age is  :" +Ageexvalue);
					List<WebElement> ddlist=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div//ul/li"));
					for(int j=0;j<=ddlist.size()-1;j++) {
						System.out.println("Text is : "+ddlist.get(j).getText());
						if(ddlist.get(j).getText().contains(Ageexvalue)) {
							//System.out.println("Click value is : "+ddlist.get(j).getText());
							Thread.sleep(5000);
							ddlist.get(j).click();
							Thread.sleep(5000);
							break;
						}
					}
					x=x+1;


				}
				//WebElement progress= driver.findElement(By.xpath("//*[@class='wrapper slider2']/div"));
				WebElement progress= driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[2]/div/div/div"));

				List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
			
				int total_size=Slidnumber.size();			
				System.out.println("Slider numbers are :  " +total_size );
				String SliderValue=null;
				for(WebElement Slider:Slidnumber) {
				SliderValue=suminsured;
                System.out.println(Slider.getText());
					Thread.sleep(3000);
					if(Slider.getText().equals(SliderValue.toString())) {
						Thread.sleep(5000);
					
						Slider.click();
						break;
					}
				}
				scrolldown();
				//Selectecting optional covers here
				if(planType.equals("Explore Start")) {

					System.out.println("Optional cobver is not required for this plan type");
				}else {
				clickElement(By.xpath(Optional_cover_xpath));
			
				String optionalcover1=TestCaseData[n][28].toString().trim();
				
				if(optionalcover1.equals("Yes")) {
					clickElement(By.xpath(optionalcover1_xpath));
				}else {
					System.out.println("Optional cover1  not required");
				}
               String optionalcover2=TestCaseData[n][29].toString().trim();
				
				if(optionalcover2.equals("Yes")) {
					clickElement(By.xpath(optionalcover2_xpath));
				}else {
					System.out.println("Optional cover2  not required");
				}
                String optionalcover3=TestCaseData[n][30].toString().trim();
				
				if(optionalcover3.equals("Yes")) {
					clickElement(By.xpath(optionalcover3_xpath));
				}else {
					System.out.println("Optional cover3  not required");
				}
               String optionalcover4=TestCaseData[n][31].toString().trim();
				
				if(optionalcover4.equals("Yes")) {
					clickElement(By.xpath(optionalcover4_xpath));
				}else {
					System.out.println("Optional cover4  not required");
				}
               String optionalcover5=TestCaseData[n][32].toString().trim();
				
				if(optionalcover5.equals("Yes")) {
					clickElement(By.xpath(optionalcover5_xpath));
				}else {
					System.out.println("Optional cover5  not required");
				}
				String optionalcover6=TestCaseData[n][33].toString().trim();
				
				if(optionalcover6.equals("Yes")) {
					clickElement(By.xpath(optionalcover6_xpath));
				}else {
					System.out.println("Optional cover6  not required");
				}
				}
				
				scrolldown();
				Thread.sleep(5000);
				WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]"));
				String BeforeEditfirstpage_PremiumValue=firstpage_Premium.getText();				
				System.out.println("First page premium value is   :"  +BeforeEditfirstpage_PremiumValue);
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("window.scrollBy(0,150)", "");
				//clickElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[6]/button[2]"));
				waitForElement(By.xpath(posStudent_buynow_xpath));
				clickElement(By.xpath(posStudent_buynow_xpath));
				// Verify premium here
				scrollup();
			String executionstatus=	TestCase[n][4].toString().trim();	
			if(executionstatus.contentEquals("Edit")) {
				
				Edit.posstudentexploreedit();
				Thread.sleep(5000);
				WebElement Editfirstpage_Premium=driver.findElement(By.xpath("//*[@class='get_quot_total_premium']/span"));
				String Editfirstpage_PremiumValue=Editfirstpage_Premium.getText();				
				System.out.println("After Edit First page premium value is   :"  +Editfirstpage_PremiumValue);
				JavascriptExecutor jsee = (JavascriptExecutor)driver;
				clickElement(By.xpath(Edit_Update_premium_button_xpath));
				jse.executeScript("window.scrollBy(0,-350)", "");
                scrollup();
                Thread.sleep(4000);
				WebElement secondpage_premium=driver.findElement(By.xpath("//*[@class='tr_premium_val']/span[1]"));
				String secondpage_PremiumValue=secondpage_premium.getText();
				System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
				Assert.assertEquals(Editfirstpage_PremiumValue, secondpage_PremiumValue);
                System.out.println("Second premium value is same as first premium value  : "+secondpage_PremiumValue);
			}else {
				System.out.println("Edit is not required");
				
				jse.executeScript("window.scrollBy(0,-350)", "");
                scrollup();
                Thread.sleep(4000);
				WebElement secondpage_premium=driver.findElement(By.xpath("//*[@class='tr_premium_val']/span[1]"));
				String secondpage_PremiumValue=secondpage_premium.getText();
				System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
				Assert.assertEquals(BeforeEditfirstpage_PremiumValue, secondpage_PremiumValue);
                System.out.println("Second premium value is same as first premium value");
          
                logger.log(LogStatus.INFO,"Quotaion Premium and Proposal Premium is Verified and Both are Same without Edit : "+ secondpage_PremiumValue     +BeforeEditfirstpage_PremiumValue);
          
			}
			
			
			//Here Selecting proposer details
			
			//selecting Nationality and passort number based on  excel
	   		Thread.sleep(500);
	   		String nationality=TestCaseData[n][12].toString().trim();
	   		System.out.println("Nationality is  : "  +nationality);
	   		logger.log(LogStatus.PASS, "Selected nationality is : " + nationality);
	   		clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
	   		clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text(),"+"'"+nationality+"'"+")]"));
	   		System.out.println("Entered Nationality is : " +nationality);

	   		String Passport = TestCaseData[n][13];
	   		//clearTextfield(By.xpath(PassportNumber_xpath));
	   		ExplicitWait1(By.xpath(PassportNumber_xpath));
	   		enterText(By.xpath(PassportNumber_xpath), Passport);
	   		Thread.sleep(2000);
	   		System.out.println("Entered Passport Number : "+Passport);
	   		logger.log(LogStatus.PASS, "Entered Passport Number : "+Passport);
	   		String Title = TestCaseData[n][14].toString().trim();
	   		clickElement(By.xpath("//select[@name='ValidTitle']"));
	   		clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text(),"+"'"+Title+"'"+")]"));
	   		Thread.sleep(3000);
	   		System.out.println("Entered Title is : " +Title);
	   		logger.log(LogStatus.PASS, "slected title is : "+Title);
	   		BaseClass.selecttext("ValidTitle", Title.toString());

	   		String FirstName = TestCaseData[n][15];
	   		clearTextfield(By.xpath(FirstName_xpath));
	   		enterText(By.xpath(FirstName_xpath), FirstName);
	   		Thread.sleep(3000);
	   		System.out.println("Entered First Name is : " +FirstName);
	   		logger.log(LogStatus.PASS, "Entered first name is : "+FirstName);
	   		String LastName = TestCaseData[n][16];
	   		clearTextfield(By.xpath(LastName_xpath));
	   		enterText(By.xpath(LastName_xpath), LastName);
	   		System.out.println("Entered Last Name is : "+LastName);
	   		logger.log(LogStatus.PASS, "Entered last name is  : "+LastName);
	   		// Entering DOB from Excel into dob field

	   		String DOB = TestCaseData[n][17].toString().trim();
	   		System.out.println("date is:" + DOB);

	   		try{
	   			clickElement(By.xpath(DOBCalender_xpath));
	   			clearTextfield(By.xpath(DOBCalender_xpath));
	   			enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
	   			driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

	   			System.out.println("Entered Proposer DOB is : "+ DOB);
	   			Thread.sleep(3000);
	   		}
	   		catch(Exception e)
	   		{
	   			System.out.println("Unable to Enter Date of Birth Date.");
	   		}

	   		logger.log(LogStatus.PASS, "Selected date of birth is  : "+DOB);

	   		final String address1 = TestCaseData[n][18].toString().trim();
	   		System.out.println("Adress1 name is :" + address1);
	   		clearTextfield(By.xpath(addressline1_xpath));
	   		enterText(By.xpath(addressline1_xpath), address1);
	   		clearTextfield(By.xpath(addressline2_xpath));
	   		enterText(By.xpath(addressline2_xpath), TestCaseData[n][19].toString().trim());
	   		clearTextfield(By.xpath(pincode_xpath));
	   		enterText(By.xpath(pincode_xpath), TestCaseData[n][20]);
	   		logger.log(LogStatus.PASS, "Entered address one  is  : "+address1);
	   		logger.log(LogStatus.PASS, "Entered address two is  : "+TestCaseData[n][20].toString().trim());
	   		logger.log(LogStatus.PASS, "Entered pincode is  : "+TestCaseData[n][20]);
	   		String NomineeName = TestCaseData[n][21].toString().trim();
	   		System.out.println("Nominee name   is:" + NomineeName);
	   		driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
	   		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
	   		logger.log(LogStatus.PASS, "Entered NomineeName is   : "+TestCaseData[n][21]);
	   		Actions action = new Actions(driver);
	   		action.sendKeys(Keys.ESCAPE);
	   		Thread.sleep(2000);
	   		
	   		String nominiRelation=TestCaseData[n][22];
	   		clickElement(By.xpath(nomineRelation_xpath));
	   		Thread.sleep(2000);
	   		driver.findElement(By.xpath("//select[@name='nomineeRelation']/option[contains(text(),"+"'"+nominiRelation+"'"+")]")).click();
	   		// driver.findElement(By.xpath("//*[@id=\"tr_details_full_cont\"]/div[1]/div[2]/div[3]/div[4]/div/select/option"));
	   		System.out.println("Selected nominee relation is : "+nominiRelation);
	   		logger.log(LogStatus.PASS, "Selected nominee relation is  : "+nominiRelation);
	   		String PurposeofVisit = TestCaseData[n][23];
	   		clickElement(By.xpath(PurposeofVisit_xpath));
	   		Thread.sleep(2000);
	   		driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"+"'"+PurposeofVisit+"'"+")]")).click();
	   		System.out.println("Selected Purpose of Visit is : "+PurposeofVisit);
	   		logger.log(LogStatus.PASS, "Selected Purpose of Visit is : "+PurposeofVisit);
	   		
	   		
	   		// Nominee Relation
	   	//Trip start date
	   		String TripstartDate=TestCaseData[n][24];
		
			try{
				Thread.sleep(4000);
					clickElement(By.xpath("//*[@name='trip_start_date']"));
					Thread.sleep(2000);
					String spilitter[]=TripstartDate.split(",");			
					String eday = spilitter[0];
					String emonth = spilitter[1];
					String eYear = spilitter[2];
					String oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					String oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					WebElement Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next Button
					while(!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth))))
					  {
						Thread.sleep(2000);
					    Next.click();
					    oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					    oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					    Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
					  }
					driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+eday+"'"+")]")).click();
					System.out.println("Entered Start_Date on Faveo UI is :"+TripstartDate);	
					}
					catch(Exception e)
					{
						System.out.println("Unable to Enter Start Date.");
					}
			Thread.sleep(5000);
			Actions actionpos=new Actions(driver);
			actionpos.sendKeys(Keys.ESCAPE);
			
	   		
	   		//*******************insured details start here*******************************
	   		

			
			String Relation=StudentExploreInsuredDetails[n][0].toString().trim();

			//String Relation2=StudentExplore_InsuredDetails[1][0].toString().trim();

			String passport=StudentExploreInsuredDetails[n][1].toString().trim();

			String title=StudentExploreInsuredDetails[n][2].toString().trim();

			String Fname=StudentExploreInsuredDetails[n][3].toString().trim();

			String Lname=StudentExploreInsuredDetails[n][4].toString().trim();

			String Dob=StudentExploreInsuredDetails[n][5].toString().trim();
			
			List<WebElement>Rel=driver.findElements(By.xpath("//select[@name='ValidRelation0']/option"));
			int size1=Rel.size();
			System.out.println("List size is : "+size1);
			for(WebElement Relations:Rel) {
				if(Relations.getText().contains("Self-primary")&&Relation.contains("Self-primary")) {
					Relations.click();
					System.out.println("Selected relation is :"  +Relation);
					Actions action1 = new Actions(driver);
					action1.sendKeys(Keys.ESCAPE);
					break;
				}else if ((!Relations.getText().contains("Self-primary")) && Relations.getText().contains(Relation)) {
					Relations.click();
					driver.findElement(By.xpath("//*[@name='rel_passport0']")).sendKeys(passport);
					clickElement(By.xpath("//*[@id='ValidRelTitle0']"));
					Thread.sleep(3000);

					BaseClass.selecttext("ValidRelTitle0", title.toString());
					driver.findElement(By.xpath("//*[@id='RelFName0']")).clear();
					Thread.sleep(3000);
					driver.findElement(By.xpath("//*[@id='RelFName0']")).sendKeys(Fname);
					driver.findElement(By.xpath("//*[@id='RelLName0']")).clear();
					Thread.sleep(3000);
					driver.findElement(By.xpath("//*[@id='RelLName0']")).sendKeys(Lname);
					Thread.sleep(3000);
					driver.findElement(By.xpath("//*[@name='rel_dob0']")).clear();
					driver.findElement(By.xpath("//*[@name='rel_dob0']")).click();
					Thread.sleep(3000);
					enterText(By.name("rel_dob0"), String.valueOf(Dob));

					Thread.sleep(3000);
					break;
				}

			}
			//****************Student explore education details start here*************************
			


			String[][] StudentEducationDetails = BaseClass.excel_Files("POSStudentExploreEducation");
			//String InstitutionName=EducationDetails[1][0].toString().trim();
			String CourseName=StudentEducationDetails[n][1].toString().trim();
			String InstituteAddress=StudentEducationDetails[n][2].toString().trim();
			String Country=StudentEducationDetails[n][3].toString().trim();
			System.out.println("Excel values are :"  +CourseName +"," +InstituteAddress +"," +Country);
			logger.log(LogStatus.PASS, "Selected course is  : "+CourseName);
			logger.log(LogStatus.PASS, "Entered institute is  : "+InstituteAddress);
			logger.log(LogStatus.PASS, "Selecte country is  : "+Country);
			

			List<WebElement> Textbox=driver.findElements(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[8]/descendant::div[2]/div//input"));
			int totaltextbox=Textbox.size();
			System.out.println("Text box size is :"+totaltextbox );
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;

			jse1.executeScript("window.scrollBy(0,150)", "");

			int a=0;
			for(WebElement textdata : Textbox ) {
				String InstitutionName=StudentEducationDetails[n][a].toString().trim();
				System.out.println("Selected text data is : "+InstitutionName);
				textdata.sendKeys(InstitutionName);
				a++;
			}
		
				//******************Sponcer details here*****************************
			
			String SponserName=StudentEducationDetails[n][4].toString().trim();
			String SponserDOB=StudentEducationDetails[n][5].toString().trim();
			String SponserRelation=StudentEducationDetails[n][6].toString().trim();
			logger.log(LogStatus.PASS, "Selected Sponser name is  : "+SponserName);
			logger.log(LogStatus.PASS, "Selected Sponser date of birth  is  : "+SponserDOB);
			logger.log(LogStatus.PASS, "Selecte Sponser Relation is  : "+SponserRelation);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,200)");
			driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[8]/descendant::div[8]/div/div//p/input")).sendKeys(SponserName);
			driver.findElement(By.xpath("//*[@name='sponsor_dob']")).click();
			Thread.sleep(3000);
			enterText(By.name("sponsor_dob"), String.valueOf(SponserDOB));
			Thread.sleep(3000);
			
			clickElement(By.xpath("//*[@name='sponsorRelation']"));
			//  Thread.sleep(5000);
			driver.findElement(By.xpath("//select[@name='sponsorRelation']/option[contains(text(),"+"'"+SponserRelation+"'"+")]")).click();
			// driver.findElement(By.xpath("//*[@id=\"tr_details_full_cont\"]/div[1]/div[2]/div[3]/div[4]/div/select/option"));
			System.out.println("Selected nominee relation is : "+SponserRelation);
			Thread.sleep(3000);
			Actions acti = new Actions(driver);
			acti.sendKeys(Keys.ESCAPE);
			
			
			System.out.println("****************Student Explore Question part start here*********************");
			


			
			String[][] StudentExploreQuestions=BaseClass.excel_Files("POSstudentExploreQuestions");

			WebElement pedtext=driver.findElement(By.xpath("//*[@class='col-md-12 padding0']/div[4]/p/span[2]"));
			System.out.println("PED text is :  "   +pedtext.getText());
			if(pedtext.equals("No")) {

				System.out.println("Not required to select any questions ");


			}else if(pedtext.getText().equals("Yes")){
				String preExistingdeases = TestCaseData[n][26].toString().trim();
				logger.log(LogStatus.PASS, "Selected pre-Existing diseases is  : "+preExistingdeases);
				System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				Thread.sleep(5000);
				driver.findElement(By.xpath("//input[@ng-checked='ped.question_1 == true']")).click();
				int i=0;
				clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
				clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='YES']"));

				List<WebElement> checkbox_list=driver.findElements(By.xpath("//input[@type='checkbox' and contains(@id,'qs_')]"));
				int total=checkbox_list.size();

				for(WebElement checkboxclick:checkbox_list) {
					checkboxclick.click();

				}
				String reason =StudentExploreQuestions[n][8].toString();
				logger.log(LogStatus.PASS, "Entered reason  is  : "+reason);
				driver.findElement(By.xpath("//*[@id='otherDiseasesDescription_0']")).sendKeys(reason);

				//Question two selection
				String QuestionNumber3 = StudentExploreQuestions[n][8].toString().trim();
				String QuestionNumber3Details = StudentExploreQuestions[n][9].toString().trim();
				System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
				System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
				driver.findElement(By.xpath("//input[@ng-checked='ped.question_2 == true']")).click();

				//*[@id="qs_T001_0"]/option[2]
				driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
				driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'Yes')]")).click();

				WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T001Desc_"+i+"']"));
				reasontextbox.clear();
				//String reason =StudentExploreQuestion[1][8].toString();
				reasontextbox.sendKeys(reason);

			}
			
			System.out.println("******************************************Student Explore payment start here*****************************************");
			

			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("window.scrollBy(0,400)");
			driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
			driver.findElement(By.xpath("//input[@id='tripStart']")).click();

			//  js.executeScript("window.scrollBy(0,400)");
			driver.findElement(By.name("optCovers")).click();
			driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
			waitForElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));
			//clickElement(By.xpath(pay_online_xpath));
			clickElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));

			//clickElement(By.xpath(Enhance_pay_online_xpath));

			
			
			
			String[][] carddetails = BaseClass.excel_Files("carddetails");
			System.out.println("card number is: " + carddetails[1][0].toString().trim());
            String text="Manage this card";
		/*	WebElement text=driver.findElement(By.xpath("//*[@id='manageCardLink']"));
			System.out.println("Text is :"  +text.toString().trim());*/
			// if(text.equals("Manage this card")){
			if(text.equals(driver.findElement(By.xpath("//*[@id='manageCardLink']")))){  
				Thread.sleep(5000);
				enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
				driver.findElement(By.xpath("//input[@type=\"submit\" and @name=\"pay_button\"]")).click();

				String message = driver
						.findElement(By.xpath("//div[@class='your_payment_sucess_p ng-binding']"))
						.getText();
				System.out.println("Message is :" + message);
				String policy_Number;
				try {
					WebElement Policy=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]"));
								policy_Number = Policy.getText();
				} catch (Exception e) {

					WebElement Policy=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]"));
					policy_Number = Policy.getText();

				}

				System.out.println("Policy number is  :"   +policy_Number);

			}else{

			Thread.sleep(5000);
			clearTextfield(By.xpath("//input[@name='ccard_number']"));
			Thread.sleep(5000);
			enterText(By.xpath("//input[@name='ccard_number']"), carddetails[1][0].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
			Thread.sleep(5000);
			enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
			
			jse1.executeScript("window.scrollBy(0,250)", "");
			String expmonth = carddetails[1][3].toString().trim();
			String expyear = carddetails[1][4].toString().trim();
			/*
			 * WebElement
			 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
			 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
			 */

			BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
			BaseClass.selecttext("cexpiry_date_year", expyear.toString());
			driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();
			Thread.sleep(10000);
			}
			
			 String expectedTitle = "Your payment transaction is successful !";
				Fluentwait(By.xpath(ExpectedMessage_xpath));
			     String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
					try{
						Assert.assertEquals(expectedTitle,actualTitle);
				          logger.log(LogStatus.INFO, actualTitle);
				     }catch(AssertionError e){
				          System.out.println("Payment Failed");
				          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
				          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
				          TestResult="Fail";
				     }

					
			WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
			String policy_Number=Policy.getText();
			System.out.println("Policy number is  :"   +policy_Number);
			
			Thread.sleep(5000);
		
			 String ProposerName=driver.findElement(By.xpath("//div[@class='col-md-5 person_dob_cont ng-scope']/div[2]/div[1]/div/div/p")).getText();
			    System.out.println("Proposer Name : "+ProposerName);
			    
			    
				String Thankyoupagepremium_value=driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
				System.out.println("Total premium value is:"+Thankyoupagepremium_value);
				
		
		
				
			//	BaseClass.DBVerification(policy_Number);

				waitForElement(By.xpath(PolProp_xpath));
				String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
				String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
				System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
				if (ProposalSummary.contains("Policy No.")) {
					Thread.sleep(8000);
					clickElement(By.xpath(Downloadpdf_Travel_Thankyou_xpath));
					Thread.sleep(20000);
					try {
					
						try{
						String UserName = System.getProperty("user.home");
						System.out.println(UserName);

						File file = new File(UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");
						
						FileInputStream input = new FileInputStream(file);

						PDFParser parser = new PDFParser(input);

						parser.parse();

						COSDocument cosDoc = parser.getDocument();

						PDDocument pdDoc = new PDDocument(cosDoc);

						PDFTextStripper strip = new PDFTextStripper();
						
						strip.setStartPage(1);
						strip.setEndPage(3);

						String studentdata = strip.getText(pdDoc);

						//System.out.println(data);
						
						
						Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
						//Assert.assertTrue(data.contains(FinalAmount));
						
						cosDoc.close();
						pdDoc.close();
						System.out.println("Text Found on the pdf File...");
						logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
						TestResult = "Pass";

						}catch(Exception e)
						{
							String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
							System.out.println(ErrorMessage);
							logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
							TestResult = "Fail";
						}


					} catch (Exception e) {

						String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
						System.out.println(ErrorMessage);
						logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
						TestResult = "Fail";
					}
					
					


				} else if (ProposalSummary.contains("Application No.")) {
					System.out.println(ThankyoupageProposal_Pol_num);
					try {
						Assert.assertEquals(policy_Number, ThankyoupageProposal_Pol_num);
						TestResult = "Pass";
						logger.log(LogStatus.INFO,"Proposal number on Payu page and Thankyour Page is Verified and Both are Same");
					} catch (AssertionError e) {
						System.out.println(policy_Number + " - failed");
						TestResult = "Fail";
						logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same");
						throw e;
					}
				}
				TestResult = "Pass";
				 WriteExcel.setCellData1("StudentExplore_TestCase",TestResult, ThankyoupageProposal_Pol_num, 1, 2, 3);
				
				 Thread.sleep(5000);
				 driver.quit();
						
					}catch (Exception e) {
						WriteExcel.setCellData("StudentExplore_TestCase", "Fail", 1, 3);
						System.out.println(e.getMessage());
						logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
						logger.log(LogStatus.FAIL, e.getMessage());
						logger.log(LogStatus.FAIL, "Test Case is Failed");
						driver.quit();
					}
	continue;	
	}
}
}
