package com.org.faveo.travelInsurance;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class GroupExplore extends BaseClass implements AccountnSettingsInterface {
	public static String PolicyType = null;
	public static String PolicyType2 = null;
	public static String testName = null;
	public static String covertype = null;
	public static String persontitle = null;
	public static Integer n;
	public static String TypeTest = null;

	public static Logger log = Logger.getLogger("devpinoyLogger");

	@Test
	public static void GroupExploreTravel() throws Exception {

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("GroupExplore_TestCase");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		String[][] TestCase = BaseClass.excel_Files("GroupExplore_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files("GroupExplore_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("GroupExplore_FamilyData");
		String[][] Group_Question = BaseClass.excel_Files("Group_Question");

		for (n = 5; n <= 5; n++) {

			try {

				Thread.sleep(5000);
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("GroupExplore Travel " + TestCaseName);
				System.out.println("GroupExplore Travel " + TestCaseName);

				// From Here we are Calling Login Class Method to Login in Faveo

				System.out.println("++++++++++++++Group Expolre starts here+++++++++++++++");

				PolicyType = TypeTest;
				// PolicyType2 = TestType;

				BaseClass.LaunchBrowser();
				// We are Taking Test Case name from Excel and Printing it in
				// our Report
				LoginCase.LoginwithValidCredendial();
				// String[][]
				// Group_Travel=BaseClass.excel_Files("GroupExplore_FamilyData");
				Thread.sleep(10000);
				clickElement(By.xpath(travelinsurance_xpath));
				Thread.sleep(4000);
				clickElement(By.xpath(GroupExplore_xpath));
				Thread.sleep(5000);
				List<WebElement> traveldropdown = driver.findElements(
						By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
				int dropsize = traveldropdown.size();
				System.out.println("Total dropdowns are :" + dropsize);
				String travelingto = TestCaseData[n][1].toString().trim();
				String scheme = TestCaseData[n][2].toString().trim();
				logger = extent.startTest("Selected Travelling to place is  " + travelingto);
				logger = extent.startTest("Selected Scheme is  " + scheme);

				Thread.sleep(5000);
				for (WebElement dropdownvalue : traveldropdown) {
					dropdownvalue.click();
					// System.out.println("Text is : "+dropdownvalue.getText());
					Thread.sleep(3000);
					if (dropdownvalue.getText().contains("Asia")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/ul/li//a[contains(text(),'"
										+ travelingto + "')]"))
								.click();// select 4 based on the excel data
						Thread.sleep(5000);
						// break;
					}
					if (dropdownvalue.getText().contains("Budget") || dropdownvalue.getText().contains("Explore")
							|| dropdownvalue.getText().contains("Premium")
							|| dropdownvalue.getText().contains("Premium Plus")
							|| dropdownvalue.getText().contains("Saver")
							|| dropdownvalue.getText().contains("Economy")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[2]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ scheme + "')]"))
								.click();// select 4 based on the excel data
						Thread.sleep(5000);
						break;
					}

				}

				// String[][] FamilyData =
				// BaseClass.excel_Files("FamilyMembers");
				// String[][] TestCaseData = BaseClass.excel_Files("TestCases");
				String StartDate = TestCaseData[n][3].toString().trim();
				logger = extent.startTest("Selected Start date is  " + StartDate);
				System.out.println("Excel from date is  :" + StartDate);
				String monthdate[] = StartDate.split(",");
				String excelday = monthdate[0];
				String excelmonth = monthdate[1];
				String excelYear = monthdate[2];
				driver.findElement(By.xpath("//input[@id='start_date']")).click();
				String CMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String CYears = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				while (!((excelYear.contentEquals(CYears)) && (excelmonth.contentEquals(CMonth)))) {
					Thread.sleep(5000);
					next.click();
					CMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					CYears = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + excelday + "'" + ")]"))
						.click();
				System.out.println("Entered Start_Date on Faveo UI is for Group Explore is:" + StartDate);

				String EndDate = TestCaseData[n][4].toString().trim();
				String spilitter1[] = EndDate.split(",");
				String endday = spilitter1[0];
				String endmonth = spilitter1[1];
				String endYear = spilitter1[2];

				driver.findElement(By.xpath("//input[@id='end_date']")).click();
				String oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																									// Button
				while (!((endYear.contentEquals(oYear1)) && (endmonth.contentEquals(oMonth1)))) {
					Thread.sleep(2000);
					Next1.click();
					oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
					oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
					Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				}
				driver.findElement(By
						.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
								+ "'" + endday + "'" + ")]"))
						.click();
				System.out.println("Entered End_Date on Faveo UI is :" + EndDate);

				String pedcase = TestCaseData[n][5].toString().trim(); // Yes or
																		// No
				logger = extent.startTest("Selected pedcase is  " + pedcase);
				// String[][] FamilyData =
				// BaseClass.excel_Files("FamilyMembers");
				System.out.println("Ped case is  :" + pedcase);
				List<WebElement> peddropdown = driver.findElements(By.xpath(
						"//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/a"));
				int total_pedvalue = peddropdown.size();
				// System.out.println("Total size is :" +total_pedvalue
				// +peddropdown.get(1).getText());
				for (WebElement pedtypes : peddropdown) {
					// System.out.println("Total types is : "
					// +pedtypes.getText());
					pedtypes.click();
					if (pedcase.equalsIgnoreCase("No")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ pedcase + "')]"))
								.click();

					} else if (pedcase.equalsIgnoreCase("YES")) {
						driver.findElement(By
								.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
										+ pedcase + "')]"))
								.click();
					}
				}

				// Select Travellers from Excel
				List<WebElement> dropdown = driver.findElements(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				try {
					for (WebElement DropDownName : dropdown) {
						if (DropDownName.getText().equals("1")) {
							DropDownName.click();
							int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
							Thread.sleep(2000);
							clickElement(By
									.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"
											+ "'" + Travellers + "'" + ")]"));
							// clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
							System.out.println("Total Number of Travellers Selected : " + Travellers);
							break;
						}
					}
				} catch (Exception e) {
					System.out.println("Unable to Select Member.");
				}

				Fluentwait(By.xpath(
						"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
				int membersSize = Integer.parseInt(TestCaseData[n][6].toString().trim());
				System.out.println("Total Number of Member in Excel : " + membersSize);
				int count = 1;
				int mcount;
				int mcountindex = 0;
				int covertype;

				outer: for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));

						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[n][7];
						System.out.println("Members details is : " + Membersdetails);

						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split(",");
							// System.out.println("Base Class Member :
							// "+BaseClass.membres);
							// System.out.println("Base Class Member :
							// "+BaseClass.membres.length);

							member: for (int i = 0; i <= BaseClass.membres.length; i++) {

								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								System.out.println("value of Mcount Index : " + mcount);
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'Up to 40 years')]"))
										.click();

								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

								String text = FamilyData[mcount][0].toString().trim();

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {

										Thread.sleep(1000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
											// break outer;
										}

									}

								}
							}
						}

					}

					else

					{
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().equals("Up to 40 years")) {

								String Membersdetails = TestCaseData[n][7];
								if (Membersdetails.contains(",")) {

									BaseClass.membres = Membersdetails.split(",");
								} else {
									// BaseClass.membres =
									// Membersdetails.split(" ");
									System.out.println("Hello");
								}
								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();

									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(1000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}

							}
						}
					}
				}

				System.out.println("Age selected");

				// Reading Mobile Number from Excel and Sending it on Faveo UI
				Thread.sleep(2000);
				String MobileNumber = TestCaseData[n][8].toString().trim();
				try {
					int size = MobileNumber.length();
					System.out.println("Entered Mobile number is: " + MobileNumber);
					if (isValid(MobileNumber)) {
						System.out.println("Entered Number is a valid number");
						enterText(By.xpath(MobileNumber_xpath), String.valueOf(MobileNumber));
					} else {
						System.out.println("Entered Number is Not a valid Number");
					}
				} catch (Exception e) {
					System.out.println("Unable to Enter Mobile Number.");
				}

				// Reading email from excel
				String email = TestCaseData[n][9].toString().trim();
				System.out.println("Email is :" + email);
				logger = extent.startTest("Entered Email Id is  " + email);

				if (email.contains("@")) {
					driver.findElement(By.name("ValidEmail")).sendKeys(email);
				} else {
					System.out.println("Not a valid email");
				}
				Thread.sleep(5000);
				String suminsured_value = TestCaseData[n][10].toString().trim();
				System.out.println("Insured Value is  :" + suminsured_value);
				logger = extent.startTest("Entered sum insured value is  " + suminsured_value);

				WebElement progress = driver
						.findElement(By.xpath("//*[@class='progress_slider_container col-md-12']/div/div"));

				List<WebElement> Slidnumber = progress.findElements(By.xpath("//span[@class='ui-slider-number']"));
				String SliderValue = null;
				for (WebElement Slider : Slidnumber) {

					// System.out.println("Slider get Text is :
					// "+Slider.getText());
					System.out.println("Policy type is : " + PolicyType);

					SliderValue = suminsured_value;
					if (Slider.getText().equals(SliderValue.toString())) {
						Slider.click();
						break;
					}
				}

				// WebElement
				// firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year
				// your_premium_cont']/div/div/label[2]/b[1]"));
				WebElement firstpage_Premium = driver.findElement(By.xpath(
						"//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[7]/div/div/div/div/label[2]/b[1]"));
				Thread.sleep(10000);
				String firstpage_PremiumValue = firstpage_Premium.getText();
				Thread.sleep(5000);
				System.out.println("First page premium value is   :" + firstpage_PremiumValue);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,150)", "");
				// clickElement(By.xpath(Group_BuyNow_xpath));
				clickElement(By.xpath(GroupExplore_buynow_xpath));
				Thread.sleep(5000);
				jse.executeScript("window.scrollBy(0,-350)", "");
				// WebElement
				// secondpage_premium=driver.findElement(By.xpath("//div[@class='col-md-12
				// tr_quotation_heading']/h4/span/span[@class='ng-binding']"));

				WebElement secondpage_premium = driver
						.findElement(By.xpath("//*[@class='col-md-12 tr_quotation_heading']/h4/span/span[1]"));
				String secondpage_PremiumValue = secondpage_premium.getText();
				Thread.sleep(10000);
				System.out.println("Second Page Premium value is  : " + secondpage_PremiumValue);
				// Assert.assertEquals(firstpage_PremiumValue,
				// secondpage_PremiumValue);

				// proposer details script

				// selecting Nationality and passort number based on excel
				String nationality = TestCaseData[n][11].toString().trim();
				System.out.println("Nationality is  : " + nationality);

				clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
				clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text()," + "'"
						+ nationality + "'" + ")]"));
				System.out.println("Entered Nationality is : " + nationality);

				String Passport = TestCaseData[n][12];
				// clearTextfield(By.xpath(PassportNumber_xpath));
				ExplicitWait1(By.xpath(PassportNumber_xpath));
				enterText(By.xpath(PassportNumber_xpath), Passport);
				Thread.sleep(2000);
				System.out.println("Entered Passport Number : " + Passport);
				String Title = TestCaseData[n][13].toString().trim();
				clickElement(By.xpath("//select[@name='ValidTitle']"));
				clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"
						+ Title + "'" + ")]"));
				Thread.sleep(3000);
				System.out.println("Entered Title is : " + Title);

				BaseClass.selecttext("ValidTitle", Title.toString());

				String FirstName = TestCaseData[n][14];
				clearTextfield(By.xpath(FirstName_xpath));
				enterText(By.xpath(FirstName_xpath), FirstName);
				Thread.sleep(3000);
				System.out.println("Entered First Name is : " + FirstName);
				String LastName = TestCaseData[n][15];
				clearTextfield(By.xpath(LastName_xpath));
				enterText(By.xpath(LastName_xpath), LastName);
				System.out.println("Entered Last Name is : " + LastName);

				// Entering DOB from Excel into dob field

				String DOB = TestCaseData[n][16].toString().trim();
				System.out.println("date is:" + DOB);

				try {
					clickElement(By.xpath(DOBCalender_xpath));
					clearTextfield(By.xpath(DOBCalender_xpath));
					enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
					driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

					System.out.println("Entered Proposer DOB is : " + DOB);
					Thread.sleep(3000);
				} catch (Exception e) {
					System.out.println("Unable to Enter Date of Birth Date.");
				}

				final String address1 = TestCaseData[n][17].toString().trim();
				System.out.println("Adress1 name is :" + address1);
				clearTextfield(By.xpath(addressline1_xpath));
				enterText(By.xpath(addressline1_xpath), address1);
				clearTextfield(By.xpath(addressline2_xpath));
				enterText(By.xpath(addressline2_xpath), TestCaseData[n][18].toString().trim());
				clearTextfield(By.xpath(pincode_xpath));
				enterText(By.xpath(pincode_xpath), TestCaseData[n][19]);

				String NomineeName = TestCaseData[n][20].toString().trim();
				System.out.println("Nominee name   is:" + NomineeName);
				driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
				enterText(By.xpath(Nominee_Name_xpqth), NomineeName);

				Actions action = new Actions(driver);
				action.sendKeys(Keys.ESCAPE);
				Thread.sleep(2000);

				String PurposeofVisit = TestCaseData[n][21];
				clickElement(By.xpath(PurposeofVisit_xpath));
				Thread.sleep(2000);
				driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"
						+ "'" + PurposeofVisit + "'" + ")]")).click();
				System.out.println("Selected Purpose of Visit is : " + PurposeofVisit);

				String pannumber = TestCaseData[n][27];
				try {
					WebElement pancard = driver.findElement(By.xpath("//*[@name='panCard']"));
					pancard.sendKeys(pannumber);
				} catch (Exception e1) {
					System.out.println("GroupExplore Travel pancard not available");
				}
				/*
				 * if(pancard.isDisplayed()) { pancard.sendKeys(pannumber);
				 * logger =
				 * extent.startTest("GroupExplore Travel pancard is available "
				 * +pannumber ); }else if(!pancard.isDisplayed()) {
				 * System.out.println("Pancard number field is not available");
				 * logger =
				 * extent.startTest("GroupExplore Travel pancard not available "
				 * ); }
				 */

				// Insured Details Script here
				String Membersdetails = TestCaseData[n][7].toString().trim();
				int mcounti;

				if (Membersdetails.contains(",")) {

					// data taking form test case sheet which is
					// 7,4,1,8,2,5
					BaseClass.membres = Membersdetails.split(",");
				} else {
					BaseClass.membres = Membersdetails.split(" ");
				}
				for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					if (i == 0) {

						driver.findElement(By.xpath("//select[@name='ValidRelation0']")).click();
						// Select Self Primary
						BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
					} else {
						mcounti = Integer.parseInt(BaseClass.membres[i].toString());

						String Relation = FamilyData[mcounti][1].toString().trim();
						String InsuredNationality = FamilyData[mcounti][2].toString().trim();
						String InsuredPassport = FamilyData[mcounti][3].toString().trim();
						String InsuredTitle = FamilyData[mcounti][4].toString().trim();
						String InsuredFirstName = FamilyData[mcounti][5].toString().trim();
						String InsuredLastName = FamilyData[mcounti][6].toString().trim();
						String InsuredDOB = FamilyData[mcounti][7].toString().trim();

						BaseClass.selecttext("ValidRelation" + i, Relation);
						System.out.println("Selected Relation is : " + Relation);

						BaseClass.selecttext("rel_nationality" + i, InsuredNationality);
						System.out.println("Selected Nationality is : " + InsuredNationality);

						enterText(By.name("rel_passport" + i), InsuredPassport);
						System.out.println("Entered Passport is : " + InsuredPassport);
						// String GInsuredTitle =
						// FamilyData[mcount][4].toString().trim();
						/*
						 * try{
						 * 
						 * System.out.println("//select[@id='ValidRelTitle"+i+
						 * "']//option[@value="+"'"+InsuredTitle+"'"+"]");
						 * clickElement(By.xpath("//select[@id='ValidRelTitle"+i
						 * +"']")); Thread.sleep(2000);
						 * clickElement(By.xpath("//select[@id='ValidRelTitle"+i
						 * +"']//option[@value="+"'"+InsuredTitle+"'"+"]"));
						 * System.out.println("Selcted Title is : "+InsuredTitle
						 * );
						 * 
						 * }catch(Exception e) {
						 * System.out.println("Unable to Select Title."); }
						 */
						clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']"));
						Thread.sleep(2000);
						// clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+Group_Travel[mcount][4].toString().trim()+"'"+"]"));
						String GInsuredTitle = FamilyData[mcounti][4].toString().trim();
						int persontitle;
						if (GInsuredTitle.contains("Mr")) {
							persontitle = 2;
						} else {
							persontitle = 3;
						}

						clickElement(By.xpath("//select[@id='ValidRelTitle" + i + "']//option[" + persontitle + "]"));
						Thread.sleep(5000);
						if (Relation.contains("Self-primary")) {
							enterText(By.name("RelFName" + i), InsuredFirstName);
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						} else {
							Thread.sleep(2000);
							clearTextfield(By.name("RelFName" + i));
							enterText(By.name("RelFName" + i), InsuredFirstName);
							Thread.sleep(2000);
							clearTextfield(By.name("RelLName" + i));
							enterText(By.name("RelLName" + i), InsuredLastName);
							System.out.println("Entered Name is : " + InsuredFirstName + " " + InsuredLastName);
						}

						if (Relation.contains("Self-primary")) {
							try {
								Thread.sleep(2000);
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								Thread.sleep(1000);
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						} else {
							try {
								Thread.sleep(2000);
								clickElement(By.xpath("//input[@name='rel_dob" + i + "']"));
								clearTextfield(By.xpath("//input[@name='rel_dob" + i + "']"));
								Thread.sleep(1000);
								enterText(By.xpath("//input[@name='rel_dob" + i + "']"), String.valueOf(InsuredDOB));
								System.out.println("Entered DateofBirth is : " + InsuredDOB);
							} catch (Exception e) {
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");

							}
						}

					}
				}

				// ==================================Health questionaries=====================
				/*int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
				Thread.sleep(2000);

				WebElement pedtext = driver.findElement(By.xpath("//*[@class='col-md-2']/p/span[2]"));
				String PEDText = pedtext.getText();
				System.out.println("Ped Text is :  " + PEDText);
				if (PEDText.contains("Yes")) {
					try {
						String preExistingdeases = TestCaseData[n][22].toString().trim();
						System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :"
								+ preExistingdeases);

						if (preExistingdeases.equalsIgnoreCase("YES")) {

							Thread.sleep(5000);
							driver.findElement(By.xpath("//input[@ng-checked='ped.question_1 == true']")).click();
							Thread.sleep(3000);
							String[] ChckData = null;
							int datacheck1 = 0;
							String HealthQuestion = Group_Question[n][1].toString().trim();
							// jse.executeScript("window.scrollBy(0,1550)");
							System.out.println("Member Having PED : " + HealthQuestion);
							scrolldown();
							for (int i = 0; i < Travellers; i++) {

								clickElement(By.xpath("//select[@id='qs_pedYesNo_" + i + "']"));

								clickElement(By.xpath("//select[@id='qs_pedYesNo_" + i + "']//option[@value='YES']"));

							} // 1st question done

							String[] ChckData1 = null;
							int detailsnumber = 0;

							for (int qlist = 2; qlist <= 7; qlist++) {

								String Details = Group_Question[n][qlist].toString().trim();
								int q1list = qlist + 1;

								if (Details.equals("")) {
									System.out.println("Please Enter Some value in Excel to Select PED.");
								} else if (Details.contains(",")) {

									ChckData1 = Details.split(",");

									for (String Chdata1 : ChckData1) {

										detailsnumber = Integer.parseInt(Chdata1);
										clickElement(By
												.xpath("//html//div[" + q1list + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
														+ detailsnumber + "]/div[1]//input[@type='checkbox']"));

									}
								}

								else if (Details.contains("")) {
									detailsnumber = Integer.parseInt(Details);
									try {
										// Thread.sleep(3000);
										driver.findElement(
												By.xpath("//html//div[" + qlist + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
														+ detailsnumber + "]/div[1]//input[@type='checkbox']"))
												.click();
									} catch (Exception e) {
										System.out.println("Not able to click on Sub Questions Values of Question 1.");
									}

								}

							}
							scrolldown();
							String[] AlimnetsQuestionData = null;
							for (int qlist = 1; qlist <= 7; qlist++) {
								String Details = Group_Question[n][qlist].toString().trim();
								String AlimnetsQuestion = Group_Question[n][7].toString().trim();
								String OtherQuestionDetails = Group_Question[n][8].toString().trim();

								if (AlimnetsQuestion.equals("")) {
									System.out.println(
											"No Need to Select question : Any other diseases or ailments not mentioned above.");

								} else if (AlimnetsQuestion.contains(",")) {
									AlimnetsQuestionData = AlimnetsQuestion.split(",");
									for (String AlimntQuesnData : AlimnetsQuestionData) {
										detailsnumber = Integer.parseInt(AlimntQuesnData);
										int Num = detailsnumber - 1;
										int q1list = qlist + 1;
										// clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
										clickElement(By.xpath("//*[@id='otherDiseasesDescription_" + Num + "']"));
										enterText(By.xpath("//textarea[@id='otherDiseasesDescription_" + Num + "']"),
												OtherQuestionDetails);

									}
								} else if (AlimnetsQuestion.contains("")) {
									detailsnumber = Integer.parseInt(AlimnetsQuestion);
									int Num = detailsnumber - 1;
									int q1list = qlist + 1;
									clickElement(
											By.xpath("//html//div[" + q1list + "]/div[2]/table[1]/tbody[1]/tr[1]/td["
													+ detailsnumber + "]/div[1]//input[@type='checkbox']"));
									enterText(By.xpath("//textarea[@id='otherDiseasesDescription_" + Num + "']"),
											OtherQuestionDetails);
								}

							}
						} else if (preExistingdeases.equalsIgnoreCase("NO")) {
							clickElement(By.xpath(PEDNoButton_xpath));
						}
					} catch (Exception e) {
						System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
						// logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse
						// User is unable to click on Health Question");
					}

					// 2nd Question injury during the last 48 months?
					String QuestionNumber2 = TestCaseData[n][23].toString().trim();

					String QuestionNumber2Details = TestCaseData[n][24].toString().trim();
					String[] CheckData = null;
					int DataCheck = 0;
					try {
						if (QuestionNumber2.contains("No")) {
							System.out.println(
									"Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
							clickElement(By.xpath(PED_2_NoButton_xpath));
						} else {
							System.out.println(
									"Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");
							System.out.println("Member Who have injured During last 48 months : " + QuestionNumber2);
							clickElement(By.xpath(PED_2_YesButton_xpath));

							for (int i = 0; i < Travellers; i++) {
								Select SelectBox = new Select(
										driver.findElement(By.xpath("//select[@id='qs_T001_" + i + "']")));
								SelectBox.selectByValue("YES");
								driver.findElement(By.xpath("//select[@id='qs_T001_" + i + "']")).click();
								driver.findElement(
										By.xpath("//select[@id='qs_T001_" + i + "']//option[contains(text(),'Yes')]"))
										.click();

								if (QuestionNumber2Details.contains(",")) {
									Thread.sleep(5000);
									String Reason[] = QuestionNumber2Details.split(",");
									WebElement reasontextbox = driver
											.findElement(By.xpath("//*[@name='qs_T001Desc_" + i + "']"));
									reasontextbox.clear();
									reasontextbox.sendKeys(Reason[i]);
								} else {
									String Reason[] = QuestionNumber2Details.split("");
									WebElement reasontextbox = driver
											.findElement(By.xpath("//*[@name='qs_T001Desc_" + i + "']"));
									reasontextbox.clear();
									reasontextbox.sendKeys(Reason[i]);
								}
							}
						}
					} catch (Exception e) {
						System.out.println("Unable to Seclect Question 2.");
					}

					// 3rd Question injury during the last 48 months?
					String QuestionNumber3 = TestCaseData[n][25].toString().trim();
					String QuestionNumber3Details = TestCaseData[n][26].toString().trim();
					try {
						if (QuestionNumber3.equalsIgnoreCase("No")) {
							System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
							clickElement(By.xpath(PED_3_NoButton_xpath));
						} else {
							System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");
							System.out.println("Member claimed under any travel policy? : " + QuestionNumber3);
							clickElement(By.xpath(PED_3_YesButton_xpath));

							for (int i = 0; i < Travellers; i++) {

								Select SelectBox = new Select(
										driver.findElement(By.xpath("//select[@id='qs_T002_" + i + "']")));
								SelectBox.selectByValue("YES");
								driver.findElement(By.xpath("//select[@id='qs_T001_" + i + "']")).click();
								driver.findElement(
										By.xpath("//select[@id='qs_T001_" + i + "']//option[contains(text(),'Yes')]"))
										.click();
								if (QuestionNumber3Details.contains(",")) {

									String thirdquestionReason[] = QuestionNumber3Details.split(",");
									WebElement reasontextbox = driver
											.findElement(By.xpath("//*[@name='qs_T002Desc_" + i + "']"));
									reasontextbox.clear();
									reasontextbox.sendKeys(thirdquestionReason[i]);
								} else {
									String thirdquestionReason[] = QuestionNumber2Details.split("");
									WebElement reasontextbox = driver
											.findElement(By.xpath("//*[@name='qs_T002Desc_" + i + "']"));
									reasontextbox.clear();
									reasontextbox.sendKeys(thirdquestionReason[i]);
								}
							}
						}
					} catch (Exception e) {
						System.out.println("Unable to Seclect Question 3.");
					}

					clickElement(By.xpath("//input[@id='termsCheckbox1']"));
					clickElement(By.xpath("//input[@id='tripStart']"));
					// clickElement(By.xpath("//input[@id='termsCheckbox2']"));

					clickElement(By.xpath("//button[@class='btn btn-success']"));
				}

				else if (PEDText.contains("No")) {
					JavascriptExecutor jseg = (JavascriptExecutor) driver;
					jseg.executeScript("window.scrollBy(0,1550)", "");
					Thread.sleep(5000);
					WebElement textmessage = driver.findElement(By.xpath("//*[@id='accordion']/div[2]"));
					String Errortext = textmessage.getText();
					System.out.println("Error text after selecting Ped  No : " + Errortext);
					clickElement(By.xpath("//input[@id='termsCheckbox1']"));
					clickElement(By.xpath("//input[@id='tripStart']"));
					// clickElement(By.xpath("//input[@id='termsCheckbox2']"));

					clickElement(By.xpath("//button[@class='btn btn-success']"));

				}*/

				
				//Health Questionnarire Elements 
				BaseClass.scrolldown();
				String preExistingdeases = TestCaseData[n][22].toString().trim();
				int Travellers = Integer.parseInt(TestCaseData[n][6].toString().trim());
				Thread.sleep(2000);
				System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				try{
					 if(preExistingdeases.equalsIgnoreCase("yes"))
					 {
						
						 waitForElements(By.xpath(PEDYesButton_xpath));
						 clickElement(By.xpath(PEDYesButton_xpath));
					
							String[] ChckData = null;
							int datacheck1 = 0;
								String HealthQuestion = Group_Question[n][1].toString().trim();
								System.out.println("Member Having PED : "+HealthQuestion);
								for(int i=0;i<Travellers;i++)
								{
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='NO']"));
								}	
								
								if (HealthQuestion.contains(",")) 
									{
						
										ChckData = HealthQuestion.split(",");
										for (String Chdata : ChckData) 
										{
											datacheck1 = Integer.parseInt(Chdata);
											datacheck1 = datacheck1 - 1;
											
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
											
										}
									}
									else if (HealthQuestion.contains(""))
									{
										datacheck1 = Integer.parseInt(HealthQuestion);
										datacheck1 = datacheck1-1;
										
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
									}
								
										String[] ChckData1 = null;
										int detailsnumber = 0;
										
										for(int qlist=2;qlist<=7;qlist++) 
										{
											
											String Details =Group_Question[n][qlist].toString().trim();
											int q1list = qlist+1;
											
											 if(Details.equals("")) 
											 {
												 System.out.println("Please Enter Some value in Excel to Select PED.");
											 }
											 else if(Details.contains(",")) 
											 {
												 
												 ChckData1 = Details.split(",");
													
												 for (String Chdata1 : ChckData1) 
												{
													 
												 detailsnumber = Integer.parseInt(Chdata1);
												 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
												
												}
											 }

												else if (Details.contains(""))
												{
													detailsnumber = Integer.parseInt(Details);
													 try{
													 driver.findElement(By.xpath("//html//div["+qlist+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']")).click();
													 }catch(Exception e)
													 {
													System.out.println("Not able to click on Sub Questions Values of Question 1.");	 
													 }
													 
													 }

										}
										
										
										String[] AlimnetsQuestionData = null;
										for(int qlist=7;qlist<=7;qlist++) 
										{
										String Details =Group_Question[n][qlist].toString().trim();
										String AlimnetsQuestion =Group_Question[n][7].toString().trim();
										String OtherQuestionDetails =Group_Question[n][8].toString().trim();

										if(AlimnetsQuestion.equals("")) 
										{
											System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
											
										 }else if(AlimnetsQuestion.contains(","))
										 {
											 AlimnetsQuestionData = AlimnetsQuestion.split(",");
											 for(String AlimntQuesnData : AlimnetsQuestionData)
											{
											 detailsnumber = Integer.parseInt(AlimntQuesnData);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
											}
											 }
										 else if(AlimnetsQuestion.contains(""))
										 {
											 detailsnumber = Integer.parseInt(AlimnetsQuestion);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
										 }
					}									
					 }
				else if (preExistingdeases.equalsIgnoreCase("no")) 
				{
							 	clickElement(By.xpath(PEDNoButton_xpath));
				}
				}
				catch(Exception e)
						{
						System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
							//logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
						}
				
				
				//2nd Question injury during the last 48 months?
				String HealthQuestion = Group_Question[n][1].toString().trim();
				System.out.println("Member Having PED : "+HealthQuestion);
				
				String QuestionNumber2 = TestCaseData[n][23].toString().trim();
				String QuestionNumber2Details = TestCaseData[n][24].toString().trim();
				String[] CheckData = null;
				int DataCheck=0;
				try{
					if(QuestionNumber2.equalsIgnoreCase("no"))
					{
						System.out.println("Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
						clickElement(By.xpath(PED_2_NoButton_xpath));
					}else
					{
						System.out.println("Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");				
						System.out.println("Member Who have injured During last 48 months : "+QuestionNumber2);
						clickElement(By.xpath(PED_2_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'No')]")).click();
						}
						
						if (HealthQuestion.contains(",")) 
						{
							CheckData = HealthQuestion.split(",");
							for (String Chdata : CheckData) 
							{
								
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
								System.out.println("Reason of Injury : "+QuestionNumber2Details);
									
						}
						} 
						else if (HealthQuestion.contains(""))
						{
							DataCheck = Integer.parseInt(HealthQuestion);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
							System.out.println("Reason of Injury : "+QuestionNumber2Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 2.");
				}
				

				//3rd Question injury during the last 48 months?
				
				String QuestionNumber3 = TestCaseData[n][25].toString().trim();
				String QuestionNumber3Details = TestCaseData[n][26].toString().trim();
				try{
					if(QuestionNumber3.equalsIgnoreCase("no"))
					{
						System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
						clickElement(By.xpath(PED_3_NoButton_xpath));
					}else
					{
						System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
						System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
						clickElement(By.xpath(PED_3_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']//option[contains(text(),'No')]")).click();
						}
						if (HealthQuestion.contains(",")) 
						{
							CheckData = HealthQuestion.split(",");
							for (String Chdata : CheckData) 
							{
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
						}
						} 
						else if (HealthQuestion.contains(""))
						{
							DataCheck = Integer.parseInt(HealthQuestion);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
							System.out.println("Reason of Injury : "+QuestionNumber3Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 3.");
				}
				clickElement(By.xpath("//input[@id='termsCheckbox1']"));
				clickElement(By.xpath("//input[@id='tripStart']"));
				clickElement(By.xpath("//input[@id='termsCheckbox2']"));
				
				clickElement(By.xpath("//button[@class='btn btn-success']"));
				
				try{
					Fluentwait1(By.xpath("//div[@class='alert alert-danger ng-binding']"));
					String ErrorFill = driver.findElement(By.xpath("//div[@class='alert alert-danger ng-binding']")).getText();
					System.out.println("Test Case is failed Because Getting Error : "+ErrorFill);
					logger.log(LogStatus.FAIL, "Test Case is failed Because Getting Error : "+ErrorFill);
				}catch(Exception e){
					System.out.println("Everything is Filled on Quotation Page.");
				}
				
				BaseClass.ErroronHelathquestionnaire();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20))
						.until(ExpectedConditions
								.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
						.click();
				System.out.println("After Pay U");

				PayuPage_Credentials();
			}

			catch (Exception e) {
				// WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
			}

		}

	}
}