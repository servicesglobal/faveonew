package com.org.faveo.travelInsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.ReadDataFromEmail_TravelInsurance;
import com.org.faveo.Base.TravelInsuranceDropDown;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Explore_ShareQuote extends BaseClass implements AccountnSettingsInterface 
{
	public static Logger log = Logger.getLogger("devpinoyLogger");
    public static Integer n;
	@Test
	public static void Explore() throws Exception
	{
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		for( n=3; n<=6; n++)
		{
			
	 try
	  {

		String[][] TestCase=BaseClass.excel_Files1("ShareQuotation");
		String[][] TestCaseData=BaseClass.excel_Files1("Explore_Quotation");
		String[][] FamilyData=BaseClass.excel_Files1("Explore_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files1("Explore_Question_Set");
	    System.out.println("Total Number of Row in Sheet : " + rowCount);

		//We are Taking Test Case name from Excel and Printing it in our Report
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Explore Travel " + TestCaseName);
		System.out.println("Explore Travel " + TestCaseName);
		
		//Step 1 - Open Email and Delete any Old Email
		System.out.println("Step 1 - Open Email and Delete Old Email");
		logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit();
		
		// Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
		System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
		logger.log(LogStatus.PASS,"Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		
		//From Here we are Calling TravelInsuranceDropdown for Explore product
		TravelInsuranceDropDown.ExploreTravelPolicy();
		
		//<----------------------Quotation Page Journey Started From Here----------------->

		//Selecting Value of Travelling to from Excel on Quotation Page
		//Fluentwait(By.xpath(TravellingAllDropdowns_xpath));
		Thread.sleep(10000);
		clickElement(By.xpath(TravellingAllDropdowns_xpath));
		//*[@class='toolbar_plan_name_input']/ui-dropdown/div[1]/div/a[contains(text(),'Asia')]
		String TravellingTo = TestCaseData[n][1].toString().trim();
		System.out.println(TravellingTo);
		//clickElement(By.xpath("//div[2]/form/div/div/div/div[1]/div/div/div/ui-dropdown/div/div/ul//a[contains(text(),"+"'"+TravellingTo+"'"+")]"));
		clickElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div[1]/div/ui-dropdown/div/div/ul/li//a[contains(text(),"+"'"+TravellingTo+"'"+")]"));
		System.out.println("User is Travelling To : "+ TravellingTo);	
		
		//Selecting Trip Type on The Basis of Travelling To
		try{
		if(TravellingTo.contains("WW Excl US & Canada") || TravellingTo.contains("Worldwide"))
		{
			String TripType = TestCaseData[n][2].toString().trim();
			if(TripType.contains("Single"))
			{
			clickElement(By.xpath(TripType_xpath));
			clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+TripType+"'"+")]"));
			System.out.println("User has Selected Trip Type as : "+TripType);
			}else if (TripType.contains("Multi"))
			{
				clickElement(By.xpath(TripType_xpath));
				clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+TripType+"'"+")]"));
				System.out.println("User has Selected Trip Type as : "+TripType);
				int MaxTripDuration = Integer.parseInt(TestCaseData[n][3].toString().trim());
				clickElement(By.xpath(MaxtripDuration_xpath));
				clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+MaxTripDuration+"'"+")]"));
				System.out.println("Selceted Max Trip Duration is : "+MaxTripDuration);
			}
			
		}else
		{
			System.out.println("No Need to Select Trip Type because user is Travelling To "+TravellingTo);
		}
		}catch(Exception e){
			System.out.println("No Need to Select Trip Type because user is Travelling To "+TravellingTo);
		}
		
		//Select Start Date from Excel and Write it on UI
		Thread.sleep(5000);
		String StartDate = TestCaseData[n][4].toString().trim();		
		try{
			Thread.sleep(4000);
				clickElement(By.xpath(StartDateCalander_Id));
				Thread.sleep(2000);
				String spilitter[]=StartDate.split(",");			
				String eday = spilitter[0];
				String emonth = spilitter[1];
				String eYear = spilitter[2];
				String oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next Button
				while(!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth))))
				  {
					Thread.sleep(2000);
				    Next.click();
				    oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				    oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				    Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				  }
				driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+eday+"'"+")]")).click();
				System.out.println("Entered Start_Date on Faveo UI is :"+StartDate);	
				}
				catch(Exception e)
				{
					System.out.println("Unable to Enter Start Date.");
				}
		
		//Select End Date from Excel and Write it on UI
		clickElement(By.id(EndDateCalender_Id));
		String EndDate = TestCaseData[n][5].toString().trim();
		String spilitter1[]=EndDate.split(",");			
		String eday1 = spilitter1[0];
		String emonth1 = spilitter1[1];
		String eYear1 = spilitter1[2];
		String oMonth1=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
		String oYear1=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
		WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next Button
		while(!((eYear1.contentEquals(oYear1)) && (emonth1.contentEquals(oMonth1))))
		  {
			Thread.sleep(2000);
		    Next1.click();
		    oMonth1=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
		    oYear1=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
		    Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
		  }
		driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+eday1+"'"+")]")).click();
		System.out.println("Entered End_Date on Faveo UI is :"+EndDate);	
		
		
		
		//Select PED and Non Ped from Excel and Select it on UI
		String PED = TestCaseData[n][7].toString().trim();
		clickElement(By.xpath(Peddropdown_xpath));
		clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+PED+"'"+")]"));
		System.out.println("User has Selected PED as : "+PED);
		
		
		
		//Select Travellers from Excel 
		List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		try{
		for (WebElement DropDownName : dropdown) {
			if (DropDownName.getText().equals("1")) {
				DropDownName.click();
			int Travellers = Integer.parseInt(TestCaseData[n][8].toString().trim());
			Thread.sleep(2000);
			clickElement(By.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
			//clickElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Travellers+"'"+")]"));
				System.out.println("Total Number of Travellers Selected : "+Travellers);
				break;
			}
		}
		}
		catch(Exception e)
		{
			System.out.println("Unable to Select Member.");
		}
		
		//Select Age of Members from Excel 
		// again call the dropdown
		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[n][8].toString().trim());
		System.out.println("Total Number of Member in Excel : "+membersSize);
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		
		outer:

			for (WebElement DropDownName : dropdown) 
			{
				if (membersSize == 1) 
				{
					List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
					
					
					
					// reading members from test cases sheet memberlist
					String Membersdetails = TestCaseData[n][9];
					System.out.println("Members details is : "+Membersdetails);
					
					if (Membersdetails.contains("")) {

						BaseClass.membres = Membersdetails.split(",");
						//System.out.println("Base Class Member : "+BaseClass.membres);
						//System.out.println("Base Class Member : "+BaseClass.membres.length);
						
						member:
						for (int i = 0; i <= BaseClass.membres.length; i++) {
						
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							System.out.println("value of Mcount Index : "+mcount);
							mcountindex = mcountindex + 1;
							
							
	driver.findElement(By.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'up to 40 Years')]")).click();
	
	
				// List Age of members dropdown
							List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));
							
							String text = FamilyData[mcount][0].toString().trim();
							
							for (WebElement ListData : List) 
							{

								if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) 
								{

									Thread.sleep(1000);
									ListData.click();

									if (count == membersSize) 
									{
										break outer;
									} 
									else 
									{
										count = count + 1;
										break member;
										//break outer;
									}

								}

							}
						}
					}

				}  
				
				else
				
				{
						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().equals("up to 40 Years")) 
							{
								
								String Membersdetails = TestCaseData[n][9];
								if (Membersdetails.contains(",")) 
								{

									BaseClass.membres = Membersdetails.split(",");
								} else {
									//BaseClass.membres = Membersdetails.split(" ");
									System.out.println("Hello");
								}
								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
							
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									
									// List Age of members dropdown
									
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));

									for (WebElement ListData : List) {
							
										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
											Thread.sleep(1000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
											}

										}

									}
								}
								
							}
						}
					}
				} 
		
		
		//Reading Mobile Number from Excel and Sending it on Faveo UI
		Thread.sleep(2000);
		String MobileNumber = TestCaseData[n][10].toString().trim();
		try{
		int size = MobileNumber.length();
		System.out.println("Entered Mobile number is: " + MobileNumber);
		if (isValid(MobileNumber)) {
			System.out.println("Entered Number is a valid number");
			enterText(By.xpath(MobileNumber_xpath), String.valueOf(MobileNumber));
		} else {
			System.out.println("Entered Number is Not a valid Number");
		}
		}
		catch(Exception e)
		{
			System.out.println("Unable to Enter Mobile Number.");
		}
		
		
		//Reading Email from Excel and Sending it on Faveo UI
		String Email = TestCaseData[n][11].toString().trim();
		System.out.println("Entered Email-id is  :" + Email);
		enterText(By.xpath(Emailid_xpath), Email);
		
		//Reading SumInsured from Excel and Sending it on Faveo UI
		int SumInsured = Integer.parseInt(TestCaseData[n][12].toString().trim());
		try{
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		System.out.println("Entered SumInsured is : " +SumInsured);
		}catch(Exception e)
		{
			System.out.println("Unable to Select SumInsured.");
		}
		ReadDataFromEmail_TravelInsurance.readDates();
		ReadDataFromEmail_TravelInsurance.readAgeGroupTravel();
		AddonsforProducts.readPremiumFromFirstPage_Travel();
		
		DataVerificationShareQuotationPage.clickOnShareQuotationButton();
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox_TravelInsurance(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_Explore(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();  
        
        // Step 3 - Open Email and Verify Data in Email Body
     	System.out.println("Step 3 - Open Email and Verify Data in Email Body");
     	logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
     	LaunchBrowser();
     	DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForTravelInsurance_Explore(n);
     	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
     	
     // Step 4 - Click on Buy Now Button from Email Body and punch
    	// the Policy
    	System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
    	logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
    	ReadDataFromEmail_TravelInsurance.ClickOnBuyNowButtonFromEmail();
    	
		// Rading Nationality from Excel and Selcting it on UI
		Thread.sleep(6000);
		String Nationality = TestCaseData[n][14];
		clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
		clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text()," + "'"
						+ Nationality + "'" + ")]"));
		System.out.println("Entered Nationality is : " + Nationality);

		// Reading Passport Number from Excel and Pass it on Faveo UI
		String Passport = TestCaseData[n][15];
		clearTextfield(By.xpath(PassportNumber_xpath));
		ExplicitWait1(By.xpath(PassportNumber_xpath));
		enterText(By.xpath(PassportNumber_xpath), Passport);
		System.out.println("Entered Passport Number : " + Passport);
		NextTab(By.xpath(PassportNumber_xpath));

		// Reading Title from Excel and Pass it on Faveo UI
		String Title = TestCaseData[n][16].toString().trim();
		try {
		Thread.sleep(5000);
		clickElement(By.xpath(Title_xpath));
		Thread.sleep(1000);
					clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"
							+ Title + "'" + ")]"));
					System.out.println("Entered Title is : " + Title);
				} catch (Exception e) {
					System.out.println("Unable to Selected Title for Proposer.");
					// logger.log(LogStatus.FAIL, "Test Case is Failed because
					// Unable to Selected Title for Proposer.");
				}
		//Reading FirstName and LastName from Excel and Pass it on Faveo UI
				String FirstName = TestCaseData[n][17];
				clearTextfield(By.xpath(FirstName_xpath));
				ExplicitWait1(By.xpath(FirstName_xpath));
				enterText(By.xpath(FirstName_xpath), FirstName);
				System.out.println("Entered First Name is : " +FirstName);
				String LastName = TestCaseData[n][18];
				clearTextfield(By.xpath(LastName_xpath));
				ExplicitWait1(By.xpath(LastName_xpath));
				enterText(By.xpath(LastName_xpath), LastName);
				System.out.println("Entered Last Name is : "+LastName);
				
				
				//Reading DOB from Excel and Pass it on UI
				String DOB = TestCaseData[n][19].toString().trim();
				try{
				clickElement(By.xpath(DOBCalender_xpath));
				clearTextfield(By.xpath(DOBCalender_xpath));
				ExplicitWait1(By.xpath(DOBCalender_xpath));
				enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
				System.out.println("Entered Proposer DOB is : "+ DOB);
				}
				catch(Exception e)
				{
					System.out.println("Unable to Enter Date of Birth Date.");
				}
				
				//Reading AddressLine from Excel and Pass it on Faveo UI
				String AddressLine1 = TestCaseData[n][20].toString().trim();
				clearTextfield(By.xpath(AddressLine1_xpath));
				ExplicitWait1(By.xpath(AddressLine1_xpath));
				clickElement(By.xpath(AddressLine1_xpath));
				enterText(By.xpath(AddressLine1_xpath), AddressLine1);
				System.out.println("Entered AddressLine1 is : "+AddressLine1);
				String AddressLine2 = TestCaseData[n][21].toString().trim();
				clearTextfield(By.xpath(AddressLine2_xpath));
				ExplicitWait1(By.xpath(AddressLine2_xpath));
				clickElement(By.xpath(AddressLine2_xpath));
				enterText(By.xpath(AddressLine2_xpath), AddressLine2);
				System.out.println("Entered AddressLine1 is : "+AddressLine2);
				
				
				//Reading Pincode from Excel and Pass it on Faveo UI
				int Pincode = Integer.parseInt(TestCaseData[n][22].toString().trim());
				clearTextfield(By.xpath(Pincode_xpath));
				ExplicitWait1(By.xpath(Pincode_xpath));
				enterText(By.xpath(Pincode_xpath), String.valueOf(Pincode));
				System.out.println("Entered Pincode is : "+Pincode);
				
				
				//Reading Nominee Name from Excel and Pass it on Faveo UI
				String NomineeName = TestCaseData[n][23];
				clearTextfield(By.xpath(Nomineename_xpath));
				ExplicitWait1(By.xpath(Nomineename_xpath));
				enterText(By.xpath(Nomineename_xpath), NomineeName);
				System.out.println("Entered Nominee Name is : "+NomineeName);

				
				//Reading Purpose Of Visit from Excel and Pass it on Faveo UI
				String PurposeofVisit = TestCaseData[n][24];
				clickElement(By.xpath(PurposeofVisit_xpath));
				clickElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"+"'"+PurposeofVisit+"'"+")]"));
				System.out.println("Selected Purpose of Visit is : "+PurposeofVisit);
				
				String Pancardnumber = TestCaseData[n][30].toString().trim();
				try{
					clickElement(By.xpath(PanCard_xpath));
					enterText(By.xpath(PanCard_xpath), Pancardnumber);
				}catch(Exception e)
				{
					System.out.println("Pan card Field is not Present on UI.");
				}
				
//				<----------------------Insured details Page Journey Started From Here----------------->
				
				System.out.println("<----------------------Insured details Page Journey Started From Here----------------->");

				for(int i=0;i<=BaseClass.membres.length-1;i++)
				{
					mcount = Integer.parseInt(BaseClass.membres[i].toString());
					
					String Relation = FamilyData[mcount][1].toString().trim();
					String InsuredNationality = FamilyData[mcount][2].toString().trim();
					String InsuredPassport = FamilyData[mcount][3].toString().trim();
					String InsuredTitle = FamilyData[mcount][4].toString().trim();
					System.out.println("Value of Mcount : "+mcount);
					System.out.println("Insured Title is : "+InsuredTitle);
					String  InsuredFirstName = FamilyData[mcount][5].toString().trim();
					String  InsuredLastName = FamilyData[mcount][6].toString().trim();
					String InsuredDOB = FamilyData[mcount][7].toString().trim();
					
					
					if(Relation.contains("Self-primary"))
					{
						clickElement(By.xpath("//select[@name='ValidRelation"+i+"']"));
						clickElement(By.xpath("//select[@name='ValidRelation0']//option[@value='SELF']"));
					}else{
					
					BaseClass.selecttext("ValidRelation" + i, Relation);
					System.out.println("Selected Relation is : "+Relation);
					
					
					BaseClass.selecttext("rel_nationality" + i, InsuredNationality);
					System.out.println("Selected Nationality is : "+InsuredNationality);
					
					
					enterText(By.name("rel_passport" + i), InsuredPassport);
					System.out.println("Entered Passport is : "+InsuredPassport);
					
					
					try{
					System.out.println("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+InsuredTitle+"'"+"]");
					clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']"));
					Thread.sleep(2000);
					clickElement(By.xpath("//select[@id='ValidRelTitle"+i+"']//option[@value="+"'"+InsuredTitle+"'"+"]"));
					System.out.println("Selcted Title is : "+InsuredTitle);
					}catch(Exception e)
					{
						System.out.println("Unable to Select Title.");
					}
					
					
					if(Relation.contains("Self-primary"))
					{
					enterText(By.name("RelFName" + i), InsuredFirstName);
					enterText(By.name("RelLName" + i), InsuredLastName);
					System.out.println("Entered Name is : "+InsuredFirstName +" " + InsuredLastName);
					}
					else{
						clearTextfield(By.name("RelFName" + i));
						enterText(By.name("RelFName" + i), InsuredFirstName);
						clearTextfield(By.name("RelLName" + i));
						enterText(By.name("RelLName" + i), InsuredLastName);
						System.out.println("Entered Name is : "+InsuredFirstName +" " + InsuredLastName);
					}
					
					if(Relation.contains("Self-primary"))
					{
						try{
							clickElement(By.xpath("//input[@name='rel_dob"+i+"']"));
							enterText(By.xpath("//input[@name='rel_dob"+i+"']"), String.valueOf(InsuredDOB));
							System.out.println("Entered DateofBirth is : "+InsuredDOB);
							}
							catch(Exception e)
							{
								System.out.println("Test Case is failed Beacuse Unable to click on DOB.");
							
							}
					}
					else
					{
					try{
					clickElement(By.xpath("//input[@name='rel_dob"+i+"']"));
					clearTextfield(By.xpath("//input[@name='rel_dob"+i+"']"));
					Thread.sleep(2000);
					enterText(By.xpath("//input[@name='rel_dob"+i+"']"), String.valueOf(InsuredDOB));
					System.out.println("Entered DateofBirth is : "+InsuredDOB);
					}
					catch(Exception e)
					{
						System.out.println("Test Case is failed Beacuse Unable to click on DOB.");
					
					}
					}
			}
				}
				
				
				//<----------------------PED Questions details Started From Here----------------->
				
				
				//Health Questionnarire Elements 
				BaseClass.scrolldown();
				String preExistingdeases = TestCaseData[n][25].toString().trim();
				int Travellers = Integer.parseInt(TestCaseData[n][8].toString().trim());
				Thread.sleep(2000);
				System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
				try{
					 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes"))
					 {
						
						 waitForElements(By.xpath(PEDYesButton_xpath));
						 clickElement(By.xpath(PEDYesButton_xpath));
					
							String[] ChckData = null;
							int datacheck1 = 0;
								String HealthQuestion = QuestionSetData[n][1].toString().trim();
								System.out.println("Member Having PED : "+HealthQuestion);
								for(int i=0;i<Travellers;i++)
								{
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
									clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='NO']"));
								}	
								
								if (HealthQuestion.contains(",")) 
									{
						
										ChckData = HealthQuestion.split(",");
										for (String Chdata : ChckData) 
										{
											datacheck1 = Integer.parseInt(Chdata);
											datacheck1 = datacheck1 - 1;
											
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
											clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
											
										}
									}
									else if (HealthQuestion.contains(""))
									{
										datacheck1 = Integer.parseInt(HealthQuestion);
										datacheck1 = datacheck1-1;
										
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']"));
										clickElement(By.xpath("//select[@id='qs_pedYesNo_"+datacheck1+"']//option[@value='YES']"));
									}
								
										String[] ChckData1 = null;
										int detailsnumber = 0;
										
										for(int qlist=2;qlist<=6;qlist++) 
										{
											
											String Details =QuestionSetData[n][qlist].toString().trim();
											int q1list = qlist+1;
											
											 if(Details.equals("")) 
											 {
												 System.out.println("Please Enter Some value in Excel to Select PED.");
											 }
											 else if(Details.contains(",")) 
											 {
												 
												 ChckData1 = Details.split(",");
													
												 for (String Chdata1 : ChckData1) 
												{
													 
												 detailsnumber = Integer.parseInt(Chdata1);
												 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
												
												}
											 }

												else if (Details.contains(""))
												{
													detailsnumber = Integer.parseInt(Details);
													 try{
													 driver.findElement(By.xpath("//html//div["+qlist+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']")).click();
													 }catch(Exception e)
													 {
													System.out.println("Not able to click on Sub Questions Values of Question 1.");	 
													 }
													 
													 }

										}
										
										
										String[] AlimnetsQuestionData = null;
										for(int qlist=7;qlist<=7;qlist++) 
										{
										String Details =QuestionSetData[n][qlist].toString().trim();
										String AlimnetsQuestion =QuestionSetData[n][7].toString().trim();
										String OtherQuestionDetails =QuestionSetData[n][8].toString().trim();

										if(AlimnetsQuestion.equals("")) 
										{
											System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
											
										 }else if(AlimnetsQuestion.contains(","))
										 {
											 AlimnetsQuestionData = AlimnetsQuestion.split(",");
											 for(String AlimntQuesnData : AlimnetsQuestionData)
											{
											 detailsnumber = Integer.parseInt(AlimntQuesnData);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
											}
											 }
										 else if(AlimnetsQuestion.contains(""))
										 {
											 detailsnumber = Integer.parseInt(AlimnetsQuestion);
											 int Num = detailsnumber-1;
											 int q1list=qlist+1;
											 clickElement(By.xpath("//html//div["+q1list+"]/div[2]/table[1]/tbody[1]/tr[1]/td["+detailsnumber+"]/div[1]//input[@type='checkbox']"));
											 enterText(By.xpath("//textarea[@id='otherDiseasesDescription_"+Num+"']"), OtherQuestionDetails);
										 }
					}									
					 }
				else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) 
				{
							 	clickElement(By.xpath(PEDNoButton_xpath));
				}
				}
				catch(Exception e)
						{
						System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");
							//logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
						}
				
				
				//2nd Question injury during the last 48 months?
				String QuestionNumber2 = TestCaseData[n][26].toString().trim();
				String QuestionNumber2Details = TestCaseData[n][27].toString().trim();
				String[] CheckData = null;
				int DataCheck=0;
				try{
					if(QuestionNumber2.contains("NO") || QuestionNumber2.contains("No") || QuestionNumber2.contains("no"))
					{
						System.out.println("Q.2 Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : No");
						clickElement(By.xpath(PED_2_NoButton_xpath));
					}else
					{
						System.out.println("Q2. Has anyone been diagnosed / hospitalized / or under any treatment for any illness / injury during the last 48 months? : Yes");				
						System.out.println("Member Who have injured During last 48 months : "+QuestionNumber2);
						clickElement(By.xpath(PED_2_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'No')]")).click();
						}
						
						if (QuestionNumber2.contains(",")) 
						{
							CheckData = QuestionNumber2.split(",");
							for (String Chdata : CheckData) 
							{
								
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
								System.out.println("Reason of Injury : "+QuestionNumber2Details);
									
						}
						} 
						else if (QuestionNumber2.contains(""))
						{
							DataCheck = Integer.parseInt(QuestionNumber2);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T001_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T001Desc_"+DataCheck+"']"), QuestionNumber2Details);
							System.out.println("Reason of Injury : "+QuestionNumber2Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 2.");
				}
				

				//3rd Question injury during the last 48 months?
				String QuestionNumber3 = TestCaseData[n][28].toString().trim();
				String QuestionNumber3Details = TestCaseData[n][29].toString().trim();
				try{
					if(QuestionNumber3.contains("NO") || QuestionNumber3.contains("No") || QuestionNumber3.contains("no"))
					{
						System.out.println("Q.3 Have you ever claimed under any travel policy? : No");
						clickElement(By.xpath(PED_3_NoButton_xpath));
					}else
					{
						System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
						System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
						clickElement(By.xpath(PED_3_YesButton_xpath));
						
						for(int i=0;i<Travellers;i++)
						{
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+i+"']//option[contains(text(),'No')]")).click();
						}
						if (QuestionNumber3.contains(",")) 
						{
							CheckData = QuestionNumber3.split(",");
							for (String Chdata : CheckData) 
							{
								DataCheck = Integer.parseInt(Chdata);
								DataCheck = DataCheck - 1;
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
								driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
								enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
						}
						} 
						else if (QuestionNumber3.contains(""))
						{
							DataCheck = Integer.parseInt(QuestionNumber3);
							DataCheck = DataCheck - 1;
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']")).click();
							driver.findElement(By.xpath("//select[@id='qs_T002_"+DataCheck+"']//option[@value='YES']")).click();
							enterText(By.xpath("//textarea[@id='qs_T002Desc_"+DataCheck+"']"), QuestionNumber3Details);
							System.out.println("Reason of Injury : "+QuestionNumber3Details);
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("Unable to Seclect Question 3.");
				}
				clickElement(By.xpath("//input[@id='termsCheckbox1']"));
				clickElement(By.xpath("//input[@id='tripStart']"));
				clickElement(By.xpath("//input[@id='termsCheckbox2']"));
				
				clickElement(By.xpath("//button[@class='btn btn-success']"));
				
				try{
					Fluentwait1(By.xpath("//div[@class='alert alert-danger ng-binding']"));
					String ErrorFill = driver.findElement(By.xpath("//div[@class='alert alert-danger ng-binding']")).getText();
					System.out.println("Test Case is failed Because Getting Error : "+ErrorFill);
					logger.log(LogStatus.FAIL, "Test Case is failed Because Getting Error : "+ErrorFill);
				}catch(Exception e){
					System.out.println("Everything is Filled on Quotation Page.");
				}
				
				BaseClass.ErroronHelathquestionnaire();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				driver.quit();
				
				// Step 5 - Again Login the application and verify after punch
				// the policy data should be not available in quotation Tracker
				System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage
						.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_Explore(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 6- Again Open the Email and Verify GUID Should be
				// Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail_TravelInsurance.openEmailAndClickOnBuyNowButtonFromEmail();

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
			     driver.quit();   

		
	  }
	 catch(AssertionError e){
		 
	 }
	catch (Exception e) {
				//WriteExcel.setCellData("Test_Cases_Care", "Fail", n, 3);
				System.out.println(e);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				//driver.quit();
			}
			continue;
		

	}
	}

}

	
