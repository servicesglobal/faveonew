package com.org.faveo.travelInsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.Base.ReadDataFromEmail_TravelInsurance;
import com.org.faveo.PolicyJourney.GroupStudentExplorePageJourmey;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class GroupStudentExplore extends BaseClass implements AccountnSettingsInterface{
	
	public static Integer n;
	@Test(priority=1, enabled=false)
	public static void GroupStudentExploreTravel() throws Exception {
		
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
	int rowCount = fis.getRowCount("GroupStudentExplore_Testcase");
	System.out.println("Total Number of Row in Sheet : "+rowCount);

	String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
	String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
	
	for(n=1;n<=rowCount;n++) {
		try {
			
		
	BaseClass.LaunchBrowser();
	
	Thread.sleep(5000);
	String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
	logger = extent.startTest("Group Student Explore Travel " + TestCaseName);
	System.out.println("Group Student Explore Travel " + TestCaseName);
	
	//From Here we are Calling Login Class Method to Login in Faveo 
	LoginCase.LoginwithValidCredendial();
	
	System.out.println("++++++++++++++Group Student Expolre starts here+++++++++++++++");
	
	HealthInsuranceDropDown.Groupstudentexploreselect();
	GroupStudentExplorePageJourmey.groupexplorestudentquotation();
	GroupStudentExplorePageJourmey.suminsured();
	GroupStudentExplorePageJourmey.premiumAssertion();
	GroupStudentExplorePageJourmey.GroupStudentExploreproposerDetails();
	GroupStudentExplorePageJourmey.GroupStudentExploreInsuredDetails();
	GroupStudentExplorePageJourmey.GroupstudentExploreEducationDetails();
	GroupStudentExplorePageJourmey.GroupStudentExploreQuestionset();
	GroupStudentExplorePageJourmey.GroupStudentExplorePaymentAndDataValidations();
	
	WriteExcel.setCellData("GroupStudentExplore_Testcase", "PASS", n, 3);

	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
	
	}catch(Exception e){
		WriteExcel.setCellData("GroupStudentExplore_Testcase", "Fail", n, 3);
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
		logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
		logger.log(LogStatus.FAIL, "Test Case is Failed.");
		Thread.sleep(4000);
		 driver.quit();
	}
	continue;
	}
	}
	
	//**************** Share Quote Test case *************************************
	@Test(priority=2, enabled=false)
	public static void GroupStudentExploreTravel_ShareQuote() throws Exception {
	System.out.println("++++++++++++++Group Student Expolre starts here+++++++++++++++");
	ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
	int rowCount = fis.getRowCount("ShareQuotation");
	System.out.println("Total Number of Row in Sheet : "+rowCount);

	String[][] TestCase=BaseClass.excel_Files("ShareQuotation");
	String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
	
	for(n=1;n<=6;n++) {
	try {
	String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
	logger = extent.startTest("Group Student Explore Travel " + TestCaseName);
	System.out.println("Group Student Explore Travel " + TestCaseName);
	
	// Step 1 - Open Email and Delete any Old Email
	System.out.println("Step 1 - Open Email and Delete Old Email");
	logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
	LaunchBrowser();
	ReadDataFromEmail.openAndDeleteOldEmail();
	driver.quit();
	
	// Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
	System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
	logger.log(LogStatus.PASS,"Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");
	
	//From Here we are Calling Login Class Method to Login in Faveo 
	LaunchBrowser();
	Thread.sleep(5000);
	LoginCase.LoginwithValidCredendial();
	Thread.sleep(5000);
	HealthInsuranceDropDown.Groupstudentexploreselect();
	GroupStudentExplorePageJourmey.groupexplorestudentquotation();
	GroupStudentExplorePageJourmey.suminsured();
	
	//ReadDataFromEmail_TravelInsurance.readDates();
	//ReadDataFromEmail_TravelInsurance.readAgeGroupTravel();
	AddonsforProducts.readPremiumFromFirstPage_Travel();
	
	DataVerificationShareQuotationPage.clickOnShareQuotationButton();
	DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox_TravelInsurance(n);
	DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForTravelInsurance_GroupStudentExplore(n);
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
    driver.quit(); 
    
 // Step 3 - Open Email and Verify Data in Email Body
 	System.out.println("Step 3 - Open Email and Verify Data in Email Body");
 	logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
 	LaunchBrowser();
 	DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForTravelInsurance_GroupStudentExplore(n);
 	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
 	
 // Step 4 - Click on Buy Now Button from Email Body and punch the Policy
	System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
	logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
	ReadDataFromEmail_TravelInsurance.ClickOnBuyNowButtonFromEmail();
	
	Thread.sleep(5000);
	GroupStudentExplorePageJourmey.GroupStudentExploreproposerDetails();
	GroupStudentExplorePageJourmey.GroupStudentExploreInsuredDetails();
	GroupStudentExplorePageJourmey.GroupstudentExploreEducationDetails();
	GroupStudentExplorePageJourmey.GroupStudentExploreQuestionset();
	//GroupStudentExplorePageJourmey.GroupStudentExplorePaymentAndDataValidations();
	
	Thread.sleep(3000);
	JavascriptExecutor js1 = (JavascriptExecutor) driver;
	js1.executeScript("window.scrollBy(0,400)");
	driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
	driver.findElement(By.xpath("//input[@id='tripStart']")).click();

	//  js.executeScript("window.scrollBy(0,400)");
	driver.findElement(By.name("optCovers")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
	//waitForElement(By.xpath(pay_online_xpath));
	//clickElement(By.xpath(pay_online_xpath));
	
	System.out.println("Before Pay U");
	(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
	System.out.println("After Pay U");
	
	PayuPage_Credentials();
	driver.quit();
	
	// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
	System.out.println(
			"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
	logger.log(LogStatus.PASS,
			"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
	LaunchBrowser();
	// Login with the help of Login Case class
	LoginCase.LoginwithValidCredendial();
	DataVerificationShareQuotationPage
			.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForTravelInsurance_GroupStudentExplore(n);
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	driver.quit();

	// Step 6- Again Open the Email and Verify GUID Should be
	// Reusable
	System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
	logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

	LaunchBrowser();
	ReadDataFromEmail_TravelInsurance.openEmailAndClickOnBuyNowButtonFromEmail();

	// Verify the Page Title
	DataVerificationShareProposalPage.verifyPageTitle();

	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	logger.log(LogStatus.PASS, "Test Case Passed");
     driver.quit();   
	
	//WriteExcel.setCellData("GroupStudentExplore_Testcase", "PASS", n, 3);

	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
	
	}catch(Exception e){
		//WriteExcel.setCellData("GroupStudentExplore_Testcase", "Fail", n, 3);
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
		logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
		logger.log(LogStatus.FAIL, "Test Case is Failed.");
		Thread.sleep(4000);
		driver.quit();
	}
	continue;
	}
	
}
	
	
	//**************** Share Proposal Test case *************************************
		@Test(priority=3, enabled=true)
		public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		System.out.println("++++++++++++++Group Student Expolre starts here+++++++++++++++");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		String[][] TestCase=BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		
		for(n=1;n<=1;n++) {
		try {
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Group Student Explore Travel " + TestCaseName);
		System.out.println("Group Student Explore Travel " + TestCaseName);
		
		// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
		System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
		logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
		
		//From Here we are Calling Login Class Method to Login in Faveo 
		LaunchBrowser();
		Thread.sleep(5000);
		LoginCase.LoginwithValidCredendial();
		Thread.sleep(5000);
		HealthInsuranceDropDown.Groupstudentexploreselect();
		GroupStudentExplorePageJourmey.groupexplorestudentquotation();
		GroupStudentExplorePageJourmey.suminsured();
		
		waitForElement(By.xpath(posStudent_buynow_xpath));
		clickElement(By.xpath(posStudent_buynow_xpath));
		
		Thread.sleep(5000);
		GroupStudentExplorePageJourmey.GroupStudentExploreproposerDetails();
		GroupStudentExplorePageJourmey.GroupStudentExploreInsuredDetails();
		GroupStudentExplorePageJourmey.GroupstudentExploreEducationDetails();
		
		//Set Details of Share Proposal Popup Box and Verify Data in Draft History
		DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox_TravelInsurance(n);
		DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForTravelGroupStudentExplore(n);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		
		//Click on Resume Policy
		DataVerificationShareProposalPage.setDetailsAfterResumePolicyTravel();
		
		//********* Question Set *********************************************
		GroupStudentExplorePageJourmey.GroupStudentExploreQuestionset();
		Thread.sleep(3000);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//input[@id='tripStart']")).click();

		//  js.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.name("optCovers")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
		//waitForElement(By.xpath(pay_online_xpath));
		//clickElement(By.xpath(pay_online_xpath));
		
		System.out.println("Before Pay U");
		(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		System.out.println("After Pay U");
		
		PayuPage_Credentials();
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
		driver.quit();
		
		//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
		System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");

		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelGroupStudentExplore(n);

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");	
		driver.quit();
		//WriteExcel.setCellData("GroupStudentExplore_Testcase", "PASS", n, 3);
		
	
		
		}
		catch(AssertionError e){
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			Thread.sleep(4000);
			//driver.quit();
		}
		
		catch(Exception e){
			//WriteExcel.setCellData("GroupStudentExplore_Testcase", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			Thread.sleep(4000);
			//driver.quit();
		}
		continue;
		}
		
	}
	//********** Share Proposal Test Case 2 ***************************************************
		@Test(priority=4, enabled=false)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
		System.out.println("++++++++++++++Group Student Expolre starts here+++++++++++++++");
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + ".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareProposal");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		String[][] TestCase=BaseClass.excel_Files("ShareProposal");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		
		for(n=3;n<=3;n++) {
		try {
		String TestCaseName = (TestCase[n+6][0].toString().trim() +" - " +TestCase[n+6][1].toString().trim());
		logger = extent.startTest("Group Student Explore Travel " + TestCaseName);
		System.out.println("Group Student Explore Travel " + TestCaseName);
		
		 //Step 1 - Open Email and Delete Old Email
		System.out.println("Step 1 - Open Email and Delete Old Email");
		logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
		
		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit(); 
		
		//Step 2 - Go to Application and share a proposal
		System.out.println("Step 2 - Go to Application and share a proposal");
		logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
		
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		
		Thread.sleep(5000);
		HealthInsuranceDropDown.Groupstudentexploreselect();
		GroupStudentExplorePageJourmey.groupexplorestudentquotation();
		GroupStudentExplorePageJourmey.suminsured();
		
		AddonsforProducts.readPremiumFromFirstPage_Travel();
		
		waitForElement(By.xpath(posStudent_buynow_xpath));
		clickElement(By.xpath(posStudent_buynow_xpath));
		
		//*** Proposal Page ****************
		Thread.sleep(5000);
		GroupStudentExplorePageJourmey.GroupStudentExploreproposerDetails();
		GroupStudentExplorePageJourmey.GroupStudentExploreInsuredDetails();
		GroupStudentExplorePageJourmey.GroupstudentExploreEducationDetails();
		
		//Set Details of Share Proposal Popup Box and Verify Data in Draft History
		DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox_TravelInsurance(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		
		driver.quit();
		
		 //Step 3- Again Open the email and verify the data of Share Proposal in Email Body
		System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
		logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
		
	    LaunchBrowser();
	    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForTravelStudentGroupExplore(n);
	    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		
		//Step 4 - Click on Buy Now button from Email Body and punch the policy
		System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
		logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
		
		ReadDataFromEmail_TravelInsurance.ClickOnBuyNowButtonFromEmail();
		
		//********* Question Set *********************************************
		GroupStudentExplorePageJourmey.GroupStudentExploreQuestionset();
		Thread.sleep(5000);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//input[@id='tripStart']")).click();

		//  js.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.name("optCovers")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
		//waitForElement(By.xpath(pay_online_xpath));
		//clickElement(By.xpath(pay_online_xpath));
		
		System.out.println("Before Pay U");
		(new WebDriverWait(driver, 120)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		System.out.println("After Pay U");
		
		PayuPage_Credentials();
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "passed")));
		driver.quit();
		
		//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
		System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
		logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");

		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForTravelGroupStudentExplore(n);

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");	
		driver.quit();
		
		//Step 6- Again Open the Email and Verify GUID Should be Reusable
		System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
		logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

		LaunchBrowser();
		ReadDataFromEmail_TravelInsurance.openEmailAndClickOnBuyNowButtonFromEmail();

		// Verify the Page Title
		DataVerificationShareProposalPage.verifyPageTitle();   

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		driver.quit();  
		
		//WriteExcel.setCellData("GroupStudentExplore_Testcase", "PASS", n, 3);
		
		}
		
		catch(AssertionError e){
			//WriteExcel.setCellData("GroupStudentExplore_Testcase", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			Thread.sleep(4000);
			//driver.quit();
		}
		catch(Exception e){
			//WriteExcel.setCellData("GroupStudentExplore_Testcase", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			Thread.sleep(4000);
			//driver.quit();
		}
		continue;
		}
		
	}
	
}

