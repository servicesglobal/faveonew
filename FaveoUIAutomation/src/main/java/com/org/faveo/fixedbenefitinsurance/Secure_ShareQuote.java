package com.org.faveo.fixedbenefitinsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Secure_ShareQuote extends BaseClass implements AccountnSettingsInterface {
	public static Integer n;
	public static String ProposalPremimPage_Value;
	@Test
	public static void SecureTestCases() throws Exception{
	String[][] TestCase=BaseClass.excel_Files("TestCases_Secure");
	String[][] TestCaseData=BaseClass.excel_Files("Secure_Quotation");
	String[][] FamilyData=BaseClass.excel_Files("Secure_InsuredDetails");
	String[][] QuestionSetData=BaseClass.excel_Files("Secure_QuestionSet");
	
	ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
	int rowCount = fis.getRowCount("TestCases_Secure");
	System.out.println("Total Number of Row in Sheet : " + rowCount);
	
	for ( n = 1; n <=1; n++) {
		
		try {
	
	//Reading Test Case Name From Excel and Sheet Name is Assure_Test_Case
	String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
	logger = extent.startTest("Secure - " + TestCaseName);
	System.out.println("Secure - " + TestCaseName);
	
	//Step 1 - Open Email and Delete any Old Email 
	System.out.println("Step 1 - Open Email and Delete Old Email");
	logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
    LaunchBrowser();
	ReadDataFromEmail.openAndDeleteOldEmail();
	driver.quit(); 
	
	//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
	System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
	logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
	LaunchBrowser();
	LoginCase.LoginwithValidCredendial();
	
	//Click on Fixed Benefit Health Insurance
	HealthInsuranceDropDown.SecureDropDown();

	//Reading Name from Excel Sheet - 
	String Name = TestCaseData[n][2].toString().trim();
	enterText(By.xpath(Textbox_Name_xpath), Name);
	System.out.println("Entered Proposer Name on Quotation Page is : " + Name);
			
	//Reading Emailid from Excel Sheet
	String Email = TestCaseData[n][3].toString().trim();
	enterText(By.xpath(Textbox_Email_xpath), Email);
	System.out.println("Entered Email-id on Quotation Page is :" + Email);
		
		
	//Reading Mobile Number From Excel Sheet
	waitForElement(By.xpath(Textbox_Mobile_xpath));
	String MobileNumber = TestCaseData[n][4].toString().trim();
	System.out.println("Entered Mobile Number on Quotation Page is : " + MobileNumber);
	if (isValid(MobileNumber)) 
	{
	System.out.println("Is a valid number");
	enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
	} else 
	{
			System.out.println("Not a valid Number");
	}
	
		//Selecting Age Band From Excel
		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(AllDropdown_xpath));
		int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		try{
		System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3
		outer:
		for (WebElement DropDownName : dropdown) {
			DropDownName.click();
			
			if (DropDownName.getText().equals("18-29")) {
				String Membersdetails = TestCaseData[n][6];
				System.out.println("Total Number of Member in Excel :" + Membersdetails);
				if (Membersdetails.contains("") ||Membersdetails.contains(" ")) {

					// data taking form test case sheet which is
					BaseClass.membres = Membersdetails.split(" ");
					System.out.println(BaseClass.membres = Membersdetails.split(" "));

					member:
					// total number of members
					for (int i = 0; i <= BaseClass.membres.length; i++) {
						
						
						
						// one by one will take from 83 line
						mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
						mcountindex = mcountindex + 1;
		

						// List Age of members dropdown
						Fluentwait(By.xpath("//a[@ng-click='selectVal(item)']"));
						List<WebElement> List = driver.findElements(By.xpath("//a[@ng-click='selectVal(item)']"));
						System.out.println("List Data is : " + List);

						for (WebElement ListData : List) {

							if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
								System.out.println("AgeBand of Member is :" + ListData.getText());

								ListData.click();

								if (count == membersSize) {
									break outer;
								} else {
									count = count + 1;
									break member;
								}

							}

						}
					}
				}
			}
		}
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		
		//Enter Annual Income from Excel
		String AnnualIncome = TestCaseData[n][7].toString().trim();
		System.out.println("Entered Annual Income is : "+AnnualIncome);
		enterText(By.xpath(AnnualIncome_xpath), AnnualIncome);
		
		//Select JobType from Excel
		String JobType = TestCaseData[n][8].toString().trim();
		System.out.println("Selected Job Type is : "+JobType);
		Thread.sleep(3000);
		if(JobType.contains("Salaried")){
			clickElement(By.xpath(Salaried_xpath));
		}else if (JobType.contains("Self Employed")){
			clickElement(By.xpath(SelfEmployed_xpath));
		}
		
		//Reading Sum Insured from Excel 
		Thread.sleep(3000);
		int SumInsured = Integer.parseInt(TestCaseData[n][9].toString().trim());
		System.out.println("Entered Sum Insured is :" +SumInsured);
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));

		//Reading Tenure from Excel
		int Tenure = Integer.parseInt(TestCaseData[n][10].toString().trim());
		System.out.println(Tenure);
		if(Tenure==1)
		{
		clickbyHover(By.xpath(Radio_Tenure1_xpath));
		}
		else if(Tenure==2)
		{
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
		}
		else if(Tenure==3)
		{
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
		}		
		
		//Reading Addons from Excel
		String Addons = TestCaseData[n][11].toString().trim();
		System.out.println("Addons values is : "+Addons);
		if(Addons.contains("Accidental Hospitalization Expenses")){
			clickbyHover(By.xpath(Secure_Addon_xpath));
		}else{
			System.out.println("No Addons Selected in Secure.");
		}
		
		AddonsforProducts.readPremiumFromFirstPage();
       // ReadDataFromEmail.readAgeGroup(); 

        DataVerificationShareQuotationPage.clickOnShareQuotationButton();
	    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForSecure(n);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();
	
	 // Step 3 - Open Email and Verify Data in Email Body
	System.out.println("Step 3 -  Open Email and Verify Data in Email Body");
	logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
	LaunchBrowser();
	DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSecure(n);
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	
	// Step 4 - Click on Buy Now Button from Email Body and punch
	// the Policy
	System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
	logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
	ReadDataFromEmail.ClickOnBuyNowButtonFromEmail2();
		
		// Capture The Premium value
		
		// Reading Proposer Title from Excel
		Thread.sleep(5000);
		String Title = TestCaseData[n][1].toString().trim();
		waitForElement(By.xpath(click_title_xpath));
		clickElement(By.xpath(click_title_xpath));
		clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"+ Title + "'" + ")]"));
		System.out.println("Entered Titel is:" + Title);
		
		// Entering DOB from Excel into dob field
		String DOB = TestCaseData[n][12].toString().trim();
		clickElement(By.id(Dob_Proposer_id));
		enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));
		System.out.println("Entered Date of Birth is :" + DOB);
		
		
		// Reading AddressLine 1 from Excel
		String address1 = TestCaseData[n][13].toString().trim();
		enterText(By.xpath(Textbox_AddressLine1_xpath), address1);
		System.out.println("Entered AdressLine1 is :" + address1);
		
		
		// Reading AddressLine 2 from Excel
		String address2 = TestCaseData[n][14].toString().trim();
		System.out.println("Entered AdressLine2 is :" + address2);
		enterText(By.xpath(Textbox_AddressLine2_xpath), address2);

		// Reading Pincode from Excel
		int Pincode = Integer.parseInt(TestCaseData[n][15].toString().trim());
		enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
		System.out.println("Entered PinCode is :" +Pincode);
		
		
		// Reading Proposer Height in Feet from Excel
		String HeightProposer_Feet = TestCaseData[n][16].toString().trim();
		clickElement(By.xpath(DropDown_HeightFeet_xpath));
		clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text(),"+ HeightProposer_Feet + ")]"));
		System.out.println("Entered Height value in Feet is :" + HeightProposer_Feet);
		
		
		// Reading Proposer Height in Inch from Excel
		String HeightProposer_Inch = TestCaseData[n][17].toString().trim();
		clickElement(By.xpath(DropDown_HeightInch_xpath));
		clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text(),"+ HeightProposer_Inch + ")]"));
		System.out.println("Entered Height value in Inch is :" + HeightProposer_Inch);
		

		// Reading Weight of Proposer from Excel
		String Weight = TestCaseData[n][18].toString().trim();
		enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));
		System.out.println("Entered Weight in Kg is :" + Weight);
		
		
		// Reading Nominee Name from Excel
		String NomineeName = TestCaseData[n][19].toString().trim();
		enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);
		System.out.println("Entered Nominee name is:" + NomineeName);
		
		
		// Nominee Relation
		String NomineeRelation = TestCaseData[n][20].toString().trim();
		clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
		clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"+ NomineeRelation + "'" + ")]"));
		System.out.println("Entered Nominee  relation is:" + NomineeRelation);
		
		
		// Pan Card
		String PanCard = TestCaseData[n][21].toString().trim();
		System.out.println("Entered Pancard number is :" + PanCard);
		Boolean PanCardNumberPresence = driver.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input")).size() > 0;
		if (PanCardNumberPresence == true) {
			enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

		} else {
			System.out.println("PAN Card Field is not Present");
		}

		// Click on Next button
		try{
			clickElement(By.id(Button_NextProposer_id));
			System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
			}
			catch(Exception e)
			{
				logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
				System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
			}

		for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
			mcount = Integer.parseInt(BaseClass.membres[i].toString());
			if (i == 0) {
				clickElement(By.xpath(title1_xpath));
				// Select Self Primary
				BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
				String OccupationClass = FamilyData[mcount][9].toString().trim();
				System.out.println("Selected Occupation Class is : "+OccupationClass);
				if(OccupationClass.contains("Others"))
				{
				clickElement(By.xpath(Occupationid_xpath));
				clickElement(By.xpath("//option[contains(text()," + "'"+ OccupationClass + "'" + ")]"));
				String OccupationDescription = FamilyData[mcount][10].toString().trim();
				clickElement(By.xpath(DescriptionOthers_xpath));
				enterText(By.xpath(DescriptionOthers_xpath), OccupationDescription);
				}
				else{
					clickElement(By.xpath(Occupationid_xpath));
					clickElement(By.xpath("//option[contains(text()," + "'"+ OccupationClass + "'" + ")]"));
					
				}
			} else {
			String Date = FamilyData[mcount][5].toString().trim();
			BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
			BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
			enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
			enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
			clickElement(By.name("rel_dob" + i));
			enterText(By.name("rel_dob" + i), String.valueOf(Date));
			BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
			BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
			enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
			String OccupationClass = FamilyData[mcount][9].toString().trim();
			System.out.println("Selected Occupation Class is : "+OccupationClass);
			if(OccupationClass.contains("Others"))
			{
			clickElement(By.xpath(Occupationid_xpath));
			clickElement(By.xpath("//option[contains(text()," + "'"+ OccupationClass + "'" + ")]"));
			String OccupationDescription = FamilyData[mcount][10].toString().trim();
			clickElement(By.xpath(DescriptionOthers_xpath));
			enterText(By.xpath(DescriptionOthers_xpath), OccupationDescription);
			}
			else{
				clickElement(By.xpath(Occupationid_xpath));
				clickElement(By.xpath("//option[contains(text()," + "'"+ OccupationClass + "'" + ")]"));
				
			}
			}
		}
		

		driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
		System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
		
		
		//Health Questionnaire Elements
		String preExistingdeases=TestCaseData[n][22].toString().trim();
		Thread.sleep(3000);
		System.out.println("Do you have an existing personal Accident/Health Insurance policy with Religare or any other Insurer? :" +preExistingdeases);
		BaseClass.scrollup();
		if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
			  
			waitForElements(By.xpath(secureYes_question_xpath));
			  clickElement(By.xpath(secureYes_question_xpath));		
			  
			  String InsurerDetails=QuestionSetData[n][1];
			  enterText(By.xpath(Insurer_Details_xpath),InsurerDetails);
			  System.out.println("Entered Insurer Name is : "+InsurerDetails);
			  
			  String PolicyNum=QuestionSetData[n][2];
			  enterText(By.xpath(Policynumber_xpath),PolicyNum);
			  System.out.println("Entered Policy Number is : "+PolicyNum);
			  
			  String PlanName=QuestionSetData[n][3];
			  enterText(By.xpath(PlanName_xpath),PlanName);
			  System.out.println("Entered Plan Name is : "+PlanName);
			  
			  String SumInsuredDetails=QuestionSetData[n][4];
			  enterText(By.xpath(Sum_Insured_xpath),SumInsuredDetails);
			  System.out.println("Entered SumInsured Details is : "+SumInsuredDetails);
			  
		 }	  else if(preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")){
			 clickElement(By.xpath(secureNo_question_xpath));
			  }
		
		String important_political_party_officials = TestCaseData[n][23].toString().trim();
		System.out.println("Have you ever been entrusted with prominent public functions ? : "+important_political_party_officials);
		if(important_political_party_officials.contains("YES") || important_political_party_officials.contains("Yes") || important_political_party_officials.contains("yes"))
		{
			clickElement(By.xpath(political_party_officials_Question_Yes_xpath));
		}else if(important_political_party_officials.contains("No") || important_political_party_officials.contains("NO") || important_political_party_officials.contains("no"))
		{
			clickElement(By.xpath(political_party_officials_Question_No_xpath));
		}
				
		String Hazardous_activity = TestCaseData[n][24].toString().trim();
		System.out.println("Does your job require you to be involved with any hazardous activity ? :" +Hazardous_activity);
		if(Hazardous_activity.contains("YES") || Hazardous_activity.contains("Yes") || Hazardous_activity.contains("yes"))
		{
			clickElement(By.xpath(Hazardous_activity_Yes_xpath));
		}else if(Hazardous_activity.contains("No") || Hazardous_activity.contains("NO") || Hazardous_activity.contains("no"))
		{
			clickElement(By.xpath(Hazardous_activity_No_xpath));
		}
		
		String illness_or_disease = TestCaseData[n][25].toString().trim();
		System.out.println("Have you ever been diagnosed with or are you under treatment for any disability ? :" +illness_or_disease);
		if(illness_or_disease.contains("YES") || illness_or_disease.contains("Yes") || illness_or_disease.contains("yes"))
		{
			clickElement(By.xpath(illness_or_disease_Yes_xpath));
		}else if(illness_or_disease.contains("No") || illness_or_disease.contains("NO") || illness_or_disease.contains("no"))
		{
			clickElement(By.xpath(illness_or_disease_No_xpath));
		}
	
		String Personal_Accident=TestCaseData[n][26].toString().trim();
		System.out.println("Has any company ever declined to issue/renew a Personal Accident policy for any proposed? If yes, please provide details? : "+Personal_Accident);
		if(Personal_Accident.contains("YES") || Personal_Accident.contains("Yes") || Personal_Accident.contains("yes"))
		{
			clickElement(By.xpath(Personal_Accident_Yes_xpath));
		}else if(Personal_Accident.contains("No") || Personal_Accident.contains("NO") || Personal_Accident.contains("no"))
		{
			clickElement(By.xpath(Personal_Accident_No_xpath));
		}
		
		String extreme_sports=TestCaseData[n][27].toString().trim();
		System.out.println("Do you participate in Adventure/ extreme sports? : "+extreme_sports);
		if(extreme_sports.contains("YES") || extreme_sports.contains("Yes") || extreme_sports.contains("yes"))
		{
			clickElement(By.xpath(extreme_sports_Yes_xpath));
		}else if(extreme_sports.contains("No") || extreme_sports.contains("NO") || extreme_sports.contains("no"))
		{
			clickElement(By.xpath(extreme_sports_No_xpath));
		}
		
		
		//Check Box on Health Questionnaire
		BaseClass.scrolldown();
		BaseClass.HelathQuestionnairecheckbox();
		
		try{
			clickElement(By.xpath(proceed_to_pay_xpath));
			System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
			}
			catch(Exception e)
			{
				logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
				System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
			}

		BaseClass.ErroronHelathquestionnaire();

		System.out.println("Before Pay U");
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		System.out.println("After Pay U");
		
		PayuPage_Credentials();
		driver.quit();
		
		// Step 5 - Again Login the application and verify after punch
		// the policy data should be not available in quotation Tracker
		System.out.println(
				"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		logger.log(LogStatus.PASS,
				"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
		LaunchBrowser();
		// Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareQuotationPage
				.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSecure(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();

		// Step 6- Again Open the Email and Verify GUID Should be
		// Reusable
		System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
		logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton2(n);

		// Verify the Page Title
		DataVerificationShareProposalPage.verifyPageTitle();

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
	     driver.quit();   

		//WriteExcel.setCellData1("TestCases_Secure", TestResult, ThankyoupageProposal_Pol_num, n, 2, 3);

	

	} catch (Exception e) {
		//WriteExcel.setCellData("TestCases_Secure", "Fail", n, 3);
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
		logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
		logger.log(LogStatus.FAIL, "Test Case is Failed.");
		//driver.quit();
		
	}
	 continue; 

}
}
}
