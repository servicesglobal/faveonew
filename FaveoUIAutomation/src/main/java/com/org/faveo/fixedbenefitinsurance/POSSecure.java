package com.org.faveo.fixedbenefitinsurance;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.healthinsurance.POSSecureJourney;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class POSSecure extends BaseClass implements AccountnSettingsInterface {
	public static Integer n;

	@Test(priority=1, enabled=false)
	public static void POSSecurePolicyTestcase() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("POSSecureTestCase");
		String[][] TestCaseData = BaseClass.excel_Files("POSSecQuotation");
		String[][] FamilyData = BaseClass.excel_Files("POSSecureFamily");
		String[][] QuestionSetData = BaseClass.excel_Files("POSSecureQuestion");

		ReadExcel fis = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("POSSecureTestCase");
		System.out.println("Total Number of Row in Sheet : " + rowCount);
		for (n = 1; n < 2; n++) {
			try {
				BaseClass.LaunchBrowser();
				Thread.sleep(7000);
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" + TestCaseName);

				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				HealthInsuranceDropDown.POSSecureDropDown();
				POSSecureJourney.possecurequotationpagejourney();
				POSSecureJourney.possecurejourneydropdown();
				POSSecureJourney.POSSecuresuminsurejtypeTenure();
				POSSecureJourney.POSSecurePremiumAssertion();
				POSSecureJourney.POSSecureProposalDetails();
				POSSecureJourney.POSSecureInsuredDetails();
				POSSecureJourney.POSSecureQuestionariesDetails();
				POSSecureJourney.proposalsummaryDetails();
				POSSecureJourney.ThankyouPageDetailspageVerification();

			} catch (Exception e) {
				//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				// driver.quit();

			}

		}
	}
	
	//************ Share Quote Test Case ***********************************************
	@Test(priority=2, enabled=true)
	public static void verifyDataOfShareQuoteUisngEmailAndQuotesTracker() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("POSSecureTestCase");
		String[][] TestCaseData = BaseClass.excel_Files("POSSecQuotation");
		String[][] FamilyData = BaseClass.excel_Files("POSSecureFamily");
		String[][] QuestionSetData = BaseClass.excel_Files("POSSecureQuestion");

		ReadExcel fis = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : " + rowCount);
		for (n = 6; n <=6; n++) {
			try {
				//BaseClass.LaunchBrowser();
				Thread.sleep(7000);
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" + TestCaseName);
				
				//Step 1 - Open Email and Delete any Old Email 
				System.out.println("Step 1 - Open Email and Delete Old Email");
				logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
			    LaunchBrowser();
				ReadDataFromEmail.openAndDeleteOldEmail();
				driver.quit(); 
				
				//Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
				System.out.println("Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker");
				logger.log(LogStatus.PASS, "Step 2 -Go to Application and share a Quote and Verify Data in Quotation Tracker");
				LaunchBrowser();
				
				LoginCase.LoginwithValidCredendial();

				// Login with the help of Login Case class
				//LoginCase.LoginwithValidCredendial();
				HealthInsuranceDropDown.POSSecureDropDown();
				POSSecureJourney.possecurequotationpagejourney();
				POSSecureJourney.possecurejourneydropdown();
				POSSecureJourney.POSSecuresuminsurejtypeTenure();
				
				AddonsforProducts.readPremiumFromFirstPage();
			       // ReadDataFromEmail.readAgeGroup(); 

			        DataVerificationShareQuotationPage.clickOnShareQuotationButton();
				    DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
					DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForPOSSecure(n);
					
					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			        driver.quit();
				
				 // Step 3 - Open Email and Verify Data in Email Body
				System.out.println("Step 3 -  Open Email and Verify Data in Email Body");
				logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
				LaunchBrowser();
				DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForPOSSecure(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				// Step 4 - Click on Buy Now Button from Email Body and punch
				// the Policy
				System.out.println("Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				logger.log(LogStatus.PASS, "Step 4 - Click on Buy Now Button from Email Body and punch the Policy");
				ReadDataFromEmail.ClickOnBuyNowButtonFromEmail2();
				
				//POSSecureJourney.POSSecurePremiumAssertion();
				POSSecureJourney.POSSecureProposalDetails();
				POSSecureJourney.POSSecureInsuredDetails();
				POSSecureJourney.POSSecureQuestionariesDetails();
				
				BaseClass.HelathQuestionnairecheckbox();
				Thread.sleep(2000);
				clickElement(By.xpath(proceed_to_pay_xpath));
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				driver.quit();
				
				// Step 5 - Again Login the application and verify after punch
				// the policy data should be not available in quotation Tracker
				System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
				logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
				LaunchBrowser();
				// Login with the help of Login Case class
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage
						.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForPOSSecure(n);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				driver.quit();

				// Step 6- Again Open the Email and Verify GUID Should be
				// Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton2(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
			     driver.quit();   
				
				//POSSecureJourney.proposalsummaryDetails();
				//POSSecureJourney.ThankyouPageDetailspageVerification();

			} catch (AssertionError e) {
				//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				// driver.quit();

			}
			
			catch (Exception e) {
				//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				 //driver.quit();

			}
             continue;
		}
	}
	
	//********* Share Proposal Test Case - 1 *********************************
	@Test(priority=2, enabled=false)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("POSSecureTestCase");
		String[][] TestCaseData = BaseClass.excel_Files("POSSecQuotation");
		String[][] FamilyData = BaseClass.excel_Files("POSSecureFamily");
		String[][] QuestionSetData = BaseClass.excel_Files("POSSecureQuestion");

		ReadExcel fis = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("POSSecureTestCase");
		System.out.println("Total Number of Row in Sheet : " + rowCount);
		for (n = 1; n <=6; n++) {
			try {
				
				Thread.sleep(7000);
				String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
				logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
				System.out.println("Care Senior -" + TestCaseName);

				// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
				System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
				logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				
				HealthInsuranceDropDown.POSSecureDropDown();
				POSSecureJourney.possecurequotationpagejourney();
				POSSecureJourney.possecurejourneydropdown();
				POSSecureJourney.POSSecuresuminsurejtypeTenure();
				POSSecureJourney.POSSecurePremiumAssertion();
				POSSecureJourney.POSSecureProposalDetails();
				POSSecureJourney.POSSecureInsuredDetails();
				
				//Set Details of Share Proposal Popup Box and Verify Data in Draft History
				DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
				DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForPOSSecure(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				
				//Click on Resume Policy
				DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
				
				POSSecureJourney.POSSecureQuestionariesDetails();
				POSSecureJourney.proposalsummaryDetails();
				
				System.out.println("Before Pay U");
				(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
				System.out.println("After Pay U");
				
				PayuPage_Credentials();
				//setDetailsofAxisSimulator();
				driver.quit();
				
				//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");

				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSSecure(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				driver.quit();
				
				//POSSecureJourney.ThankyouPageDetailspageVerification();

			} 
			catch (AssertionError e) {
				//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				driver.quit();

			}
			catch (Exception e) {
				//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed.");
				Thread.sleep(4000);
				driver.quit();

			}
            continue;
		}
	}
	
	//********* Share Proposal Test Case - 2 *********************************
		@Test(priority=3, enabled=false)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {

			String[][] TestCase = BaseClass.excel_Files("POSSecureTestCase");
			String[][] TestCaseData = BaseClass.excel_Files("POSSecQuotation");
			String[][] FamilyData = BaseClass.excel_Files("POSSecureFamily");
			String[][] QuestionSetData = BaseClass.excel_Files("POSSecureQuestion");

			ReadExcel fis = new ReadExcel(".\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("POSSecureTestCase");
			System.out.println("Total Number of Row in Sheet : " + rowCount);
			for (n = 3; n <=3; n++) {
				try {
					
					
					String TestCaseName = (TestCase[n+6][0].toString().trim() + " - " + TestCase[n+6][1].toString().trim());
					logger = extent.startTest("Care Senior test case name is  -" + TestCaseName);
					System.out.println("Care Senior -" + TestCaseName);

					/*//Step 1 - Open Email and Delete Old Email
					System.out.println("Step 1 - Open Email and Delete Old Email");
					logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
					
					LaunchBrowser();
					ReadDataFromEmail.openAndDeleteOldEmail();
					driver.quit(); */
					
					//Step 2 - Go to Application and share a proposal
					System.out.println("Step 2 - Go to Application and share a proposal");
					logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
					
					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					Thread.sleep(4000);
					HealthInsuranceDropDown.POSSecureDropDown();
					Thread.sleep(10000);
					POSSecureJourney.possecurequotationpagejourney();
					POSSecureJourney.possecurejourneydropdown();
					POSSecureJourney.POSSecuresuminsurejtypeTenure();
					
					AddonsforProducts.readPremiumFromFirstPage();
					
					POSSecureJourney.POSSecurePremiumAssertion();
					POSSecureJourney.POSSecureProposalDetails();
					POSSecureJourney.POSSecureInsuredDetails();
					
					//Set Details of Share Proposal Popup Box and Verify Data in Draft History
					DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
					//DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForPOSSecure(n);
					
					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					driver.quit();
					
					//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
					System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
					logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
					
				    LaunchBrowser();
				    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForPOSSecure(n);
				    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					
					//Step 4 - Click on Buy Now button from Email Body and punch the policy
					System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
					logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
					
					ReadDataFromEmail.ClickOnBuyNowButtonFromEmail2();
					
					DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
					DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
					
					POSSecureJourney.POSSecureQuestionariesDetails();
					POSSecureJourney.proposalsummaryDetails();
					
					System.out.println("Before Pay U");
					(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
					System.out.println("After Pay U");
					
					PayuPage_Credentials();
					//setDetailsofAxisSimulator();
					driver.quit();
					
					//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
					System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
					logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");

					LaunchBrowser();
					LoginCase.LoginwithValidCredendial();
					DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForPOSSecure(n);

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");	
					
					driver.quit();
					
					// Step 6- Again Open the Email and Verify GUID Should be Reusable
					System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
					logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

					LaunchBrowser();
					ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton2(n);

					// Verify the Page Title
					DataVerificationShareProposalPage.verifyPageTitle();   

					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
					logger.log(LogStatus.PASS, "Test Case Passed");
					 driver.quit();  
					
					//POSSecureJourney.ThankyouPageDetailspageVerification();

				} 
				
				catch (AssertionError e) {
					//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);
					driver.quit();

				}
				
				catch (Exception e) {
					//WriteExcel.setCellData("POSSecureTestCase", "FAIL", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
					logger.log(LogStatus.FAIL, "Test Case is Failed.");
					Thread.sleep(4000);
					driver.quit();

				}
	            continue;
			}
		}
}
