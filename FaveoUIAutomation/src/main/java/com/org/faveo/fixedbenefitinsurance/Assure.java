package com.org.faveo.fixedbenefitinsurance;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.logging.Logger;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.DbManager;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Assure extends BaseClass implements AccountnSettingsInterface
{

	@Test
	public static void AssureTestCases() throws Exception{
	String[][] TestCase=BaseClass.excel_Files("TestCases_Assure");
	String[][] TestCaseData=BaseClass.excel_Files("Assure_Quotation");
	String[][] FamilyData=BaseClass.excel_Files("Assure_InsuredDetails");
	String[][] QuestionSetData=BaseClass.excel_Files("Assure_QuestionSet");
	
	ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
	int rowCount = fis.getRowCount("TestCases_Assure");
	System.out.println("Total Number of Row in Sheet : " + rowCount);
	
	
	for (int n = 1; n < rowCount; n++) 
	{
			
		try 
				{
				
	//Launching Browser and URL 
	BaseClass.LaunchBrowser();
	
	
	//Reading Test Case Name From Excel and Sheet Name is Assure_Test_Case
	String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
	logger = extent.startTest("Assure - " + TestCaseName);
	System.out.println("Assure - " + TestCaseName);
	
	//Login with the help of Login Case class
	LoginCase.LoginwithValidCredendial();
	
	//Click on Fixed Benefit Health Insurance
	HealthInsuranceDropDown.AssureDropDown();
	
	//Reading Name from Excel Sheet - 
	String Name = TestCaseData[n][2].toString().trim();
	enterText(By.xpath(Textbox_Name_xpath), Name);
	System.out.println("Entered Proposer Name on Quotation Page is : " + Name);
		
	//Reading Emailid from Excel Sheet
	String Email = TestCaseData[n][3].toString().trim();
	enterText(By.xpath(Textbox_Email_xpath), Email);
	System.out.println("Entered Email-id on Quotation Page is :" + Email);
	
	
	//Reading Mobile Number From Excel Sheet
	waitForElement(By.xpath(Textbox_Mobile_xpath));
	String MobileNumber = TestCaseData[n][4].toString().trim();
	System.out.println("Entered Mobile Number on Quotation Page is : " + MobileNumber);
	if (isValid(MobileNumber)) {
		System.out.println("Entered MobileNumber Is a valid number");
		enterText(By.xpath(Textbox_Mobile_xpath), String.valueOf(MobileNumber));
	} else {
		System.out.println("Not a valid Number");
	}
	
	

	// Enter The Value of Total members present in policy
	Thread.sleep(10000);
	List<WebElement> dropdown = driver.findElements(By.xpath(AllDropdown_xpath));
	int membersSize = Integer.parseInt(TestCaseData[n][5].toString().trim());
	int count = 1;
	int mcount;
	int mcountindex = 0;
	try{
	outer:
	for (WebElement DropDownName : dropdown) {
		DropDownName.click();
		
		if (DropDownName.getText().equals("18 years - 25 years")) {
			String Membersdetails = TestCaseData[n][6];
			if (Membersdetails.contains("") ||Membersdetails.contains(" ")) {

				// data taking form test case sheet which is
				BaseClass.membres = Membersdetails.split(" ");
				member:
				// total number of members
				for (int i = 0; i <= BaseClass.membres.length; i++) {
					
					// one by one will take from 83 line
					mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
					mcountindex = mcountindex + 1;


					// List Age of members dropdown
					Fluentwait(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
					List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

					for (WebElement ListData : List) {

						if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
							System.out.println("Age of Member is :" + ListData.getText());

							ListData.click();

							if (count == membersSize) {
								break outer;
							} else {
								count = count + 1;
								break member;
							}

						}

					}
				}
			}
		}
	}
	}
	catch(Exception e){
		logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
		BaseClass.AbacusURL();
	}
	
	//Reading Sum Insured from Excel 
	int SumInsured = Integer.parseInt(TestCaseData[n][7].toString().trim());
	System.out.println("Entered Sum Insured is :" +SumInsured);
	clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
	clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
	//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));

	//Reading Tenure from Excel
	int Tenure = Integer.parseInt(TestCaseData[n][8].toString().trim());
	if(Tenure==1)
	{
	clickbyHover(By.xpath(Radio_Tenure1_xpath));
	//clickElement(By.xpath(Radio_Tenure1_xpath));
	System.out.println("Entered Tenure is : "+Tenure);
	}
	else if(Tenure==2)
	{
	clickbyHover(By.xpath(Radio_Tenure2_xpath));
	//clickElement(By.xpath(Radio_Tenure2_xpath));
	System.out.println("Entered Tenure is : "+Tenure);
	}
	else if(Tenure==3)
	{
	clickbyHover(By.xpath(Radio_Tenure3_xpath));
	//clickElement(By.xpath(Radio_Tenure3_xpath));
	System.out.println("Entered Tenure is : "+Tenure);
	}		
	
	
	// Reading Addons Assure Premium from Excel
	Thread.sleep(10000);
	ExplicitWait(By.xpath(Assure_Addon1_xpath));
	String Addons = TestCaseData[n][9].toString().trim();
	System.out.println("Selceted Addon is : "+Addons);
	if (Addons.contains("Assure Addon")) {
		clickbyHover(By.xpath(Assure_Addon1_xpath));
		//clickElement(By.xpath(Assure_Addon1_xpath));
		System.out.println("Selceted Addon is : "+Addons);
	} else {
		clickbyHover(By.xpath(Assure_Addon2_xpath));
		//clickElement(By.xpath(Assure_Addon2_xpath));
		System.out.println("Selceted Addon is : "+Addons);
	}
	 
	
	
	// Capture The Premium value
	Thread.sleep(5000);
	String Quotationpremium_value = driver.findElement(By.xpath(QuotationPage_premium_xpath)).getText();
	System.out.println("Total Premium Value on Quotation Page is :" + " - "+ Quotationpremium_value.substring(1, Quotationpremium_value.length()));
	
	
	//Premium Verification with Addons
	Thread.sleep(5000);
	if(Addons.contains("Assure Addon"))
	{
		String Addon1premium= driver.findElement(By.xpath(Assure_Addon1_premium_xpath)).getText();
		try{
			Assert.assertEquals(Addon1premium, Quotationpremium_value);
			System.out.println("Total Premium and Addon Premium is Verified and both are same : " +Addon1premium.substring(1, Addon1premium.length()));
			logger.log(LogStatus.INFO, "Total Premium and Addon Premium is Verified and both are same : " +Addon1premium.substring(1, Addon1premium.length()));
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Total Premium and Addon Premium are not Same");
			System.out.println("Total Premium and Addon Premium are not Same");
		}
	}else{
		String Addon2premium= driver.findElement(By.xpath(Assure_Addon2_premium_xpath)).getText();
		try{
			Assert.assertEquals(Addon2premium, Quotationpremium_value);
			logger.log(LogStatus.INFO, "Total Premium and Addon Premium is Verified and both are same : " +Addon2premium.substring(1, Addon2premium.length()));
			System.out.println("Total Premium and Addon Premium is Verified and both are same : " +Addon2premium.substring(1, Addon2premium.length()));
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Total Premium and Addon Premium are not Same");
			System.out.println("Total Premium and Addon Premium are not Same");
		}
	}
	


	// Click on BuyNow Button
	try{
	Thread.sleep(3000);
	clickElement(By.xpath(Button_Buynow_xpath));
	System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
			}
			catch(Exception e){
				System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
			
			}
	
	// Premium verification on Proposal Page with Quotation Page
	Thread.sleep(10000);
	String Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
	String ProposalPremimPage_Value = Proposalpremium_value.substring(1, Proposalpremium_value.length());
	System.out.println("Total Premium Value on Proposal Page is : " + " - "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
	try {
		Assert.assertEquals(Quotationpremium_value, Proposalpremium_value);
		logger.log(LogStatus.INFO,"Quotaion Premium and Proposal Premium is Verified and Both are Same : "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
	} catch (AssertionError e) {
		System.out.println(Quotationpremium_value + " - failed");
		logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");
		
	}

	
	// Reading Proposer Title from Excel
	String Title = TestCaseData[n][1].toString().trim();
	clickElement(By.xpath(click_title_xpath));
	clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text()," + "'"+ Title + "'" + ")]"));
	System.out.println("Entered Titel is:" + Title);
	
	// Entering DOB from Excel into dob field
	String DOB = TestCaseData[n][10].toString().trim();
	clickElement(By.id(Dob_Proposer_id));
	enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));
	System.out.println("Entered Date of Birth is :" + DOB);
	
	
	// Reading AddressLine 1 from Excel
	String address1 = TestCaseData[n][11].toString().trim();
	enterText(By.xpath(Textbox_AddressLine1_xpath), address1);
	System.out.println("Entered AdressLine1 is :" + address1);
	
	
	// Reading AddressLine 2 from Excel
	String address2 = TestCaseData[n][12].toString().trim();
	System.out.println("Entered AdressLine2 is :" + address2);
	enterText(By.xpath(Textbox_AddressLine2_xpath), address2);

	// Reading Pincode from Excel
	int Pincode = Integer.parseInt(TestCaseData[n][13].toString().trim());
	enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
	System.out.println("Entered PinCode is :" +Pincode);
	
	
	// Reading Proposer Height in Feet from Excel
	String HeightProposer_Feet = TestCaseData[n][14].toString().trim();
	clickElement(By.xpath(DropDown_HeightFeet_xpath));
	clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_FEET'][contains(text(),"+ HeightProposer_Feet + ")]"));
	System.out.println("Entered Height value in Feet is :" + HeightProposer_Feet);
	
	
	// Reading Proposer Height in Inch from Excel
	String HeightProposer_Inch = TestCaseData[n][15].toString().trim();
	clickElement(By.xpath(DropDown_HeightInch_xpath));
	clickElement(By.xpath("//option[@ng-repeat='data in HEIGHT_INCHES'][contains(text(),"+ HeightProposer_Inch + ")]"));
	System.out.println("Entered Height value in Inch is :" + HeightProposer_Inch);
	

	// Reading Weight of Proposer from Excel
	String Weight = TestCaseData[n][16].toString().trim();
	enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));
	System.out.println("Entered Weight in Kg is :" + Weight);
	
	
	// Reading Nominee Name from Excel
	String NomineeName = TestCaseData[n][17].toString().trim();
	enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);
	System.out.println("Entered Nominee name is:" + NomineeName);
	
	
	// Nominee Relation
	String NomineeRelation = TestCaseData[n][18].toString().trim();
	clickElement(By.xpath(Dropdown_Nomineerelation_xpath));
	clickElement(By.xpath("//option[@ng-repeat='relData in nomineeRelationship'][contains(text()," + "'"+ NomineeRelation + "'" + ")]"));
	System.out.println("Entered Nominee  relation is:" + NomineeRelation);
	
	
	// Pan Card
	String PanCard = TestCaseData[n][19].toString().trim();
	System.out.println("Entered Pancard number is :" + PanCard);
	Boolean PanCardNumberPresence = driver.findElements(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input")).size() > 0;
	if (PanCardNumberPresence == true) {
		enterText(By.xpath("//*[@id='msform']/div[2]/fieldset[1]/div[5]/div[2]/div/input"), PanCard);

	} else {
		System.out.println("PAN Card Field is not Present");
	}

	// Click on Next button
	try{
		clickElement(By.id(Button_NextProposer_id));
		System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Unable to Click on Next Button from Proposer Detail Page.");
			System.out.println("Unable to Click on Next Button from Proposer Detail Page.");
		}

	
	
	for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
		mcount = Integer.parseInt(BaseClass.membres[i].toString());

		String Date = FamilyData[mcount][5].toString().trim();
		BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
		BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
		enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
		enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
		clickElement(By.name("rel_dob" + i));
		enterText(By.name("rel_dob" + i), String.valueOf(Date));
		BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
		BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
		enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
	}

	
	driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
	System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");
	
	String ChecksData = null;
	try{
	for (int morechecks = 1; morechecks <= 3; morechecks++) {
		int mch = morechecks;
		ChecksData = TestCaseData[n][19 + morechecks].toString().trim();
		clickElement(By.xpath("//label[@for='question_"+mch+"_"+ChecksData+"']"));
	
	}
			}
			catch(Exception e)
			{
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
			}
	
	String preExistingdeases = TestCaseData[n][23].toString().trim();
	Thread.sleep(1000);
	System.out.println("Are you suffering from any Pre-existing Disease? :" + preExistingdeases);
	if (preExistingdeases.contains("yes")) {
		
		clickElement(By.xpath("//label[@for='question_4_yes']"));
		String Details = null;
		for (int qlist = 1; qlist <= 9; qlist++) {
			Details = QuestionSetData[n][qlist].toString().trim();
			if (Details.equals("")) {
				// break;
			} else {
				int detailsnumber = Integer.parseInt(Details);

				detailsnumber = detailsnumber + 1;
				System.out.println("Details is :" + Details);
				clickElement(By.xpath("//*[@id='accordion_q']/div[1]/div[2]/div/table/tbody/tr["+qlist+"]/td[2]/div/input"));
				Thread.sleep(1000);
			}

		}
	} else if (preExistingdeases.contains("no")) {
		clickElement(By.xpath("//label[@for='question_4_no']"));
	}
	



	for (int morechecks = 1; morechecks <= 7; morechecks++) {
		int mch = morechecks+4;
		ChecksData = TestCaseData[n][23 + morechecks].toString().trim();
		clickElement(By.xpath("//label[@for='question_"+mch+"_"+ChecksData+"']"));
	
	}
	
	// Scroll down the page
	BaseClass.scrolldown();
		
	BaseClass.HelathQuestionnairecheckbox();

	try{
		clickElement(By.xpath(proceed_to_pay_xpath));
		System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
			System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
		}

	BaseClass.ErroronHelathquestionnaire();

	String proposalSummarypremium_value = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='premium_amount ng-binding']"))).getText();
	System.out.println("Total premium value is:" + proposalSummarypremium_value);

	BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage(ProposalPremimPage_Value,proposalSummarypremium_value);

	System.out.println("Before Pay U");
	(new WebDriverWait(driver, 20)).until(ExpectedConditions
			.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
			.click();
	System.out.println("After Pay U");

	waitForElement(By.xpath(payu_proposalnum_xpath));
	String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
	System.out.println(PayuProposalNum);

	BaseClass.PayuPage_Credentials();

	Thread.sleep(2000);
	// Scroll down the page
	BaseClass.scrolldown();

	Thread.sleep(10000);
	try {
		String PayuTimeout = driver.findElement(By.xpath("/html/body/h1")).getText();
		logger.log(LogStatus.FAIL, "Test Case is Fail because  Payu is downn and getting : " + PayuTimeout);

	} catch (Exception e) {
		System.out.println("Test Case Conti...");
	}

	// Thankyou Page Message Verfictaion
	//BaseClass.Thankyoupageverification();
	String expectedTitle = "Your payment transaction is successful !";
	Fluentwait(By.xpath(ExpectedMessage_xpath));
     String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
		try{
			Assert.assertEquals(expectedTitle,actualTitle);
	          logger.log(LogStatus.INFO, actualTitle);
	     }catch(AssertionError e){
	          System.out.println("Payment Failed");
	          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
	          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
	          
	     }
	
	
	
	String Thankyoupagepremium_value = driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
	System.out.println("Total premium value is:" + Thankyoupagepremium_value);

	try {
		Assert.assertEquals(proposalSummarypremium_value, Thankyoupagepremium_value);
		logger.log(LogStatus.INFO,"Proposal Summuary Premium and Thankyou page Premium is Verified and Both are Same i.e : "+ Thankyoupagepremium_value);
	} catch (AssertionError e) {
		System.out.println(proposalSummarypremium_value + " - failed");
		logger.log(LogStatus.FAIL, "Proposal Summuary Premium and Thankyou page Premium are not Same");
		throw e;
	}

	DbManager.setDbConnection();
	List<String> DBValues = DbManager.getSqlQuery("Select policynum,proposalnum,PROCESSSTATUSCD from policy Where proposalnum ='"+ PayuProposalNum + "'");
	System.out.println(DBValues);

	logger.log(LogStatus.INFO,"Proposal Number/Policy Num and Premium is Verified in DB : " + DBValues);

	waitForElement(By.xpath(PolProp_xpath));
	String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
	String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
	System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
	if (ProposalSummary.contains("Policy No.")) 
	{	
		Thread.sleep(15000);
		clickElement(By.xpath(Downloadpdf_Thankyou_xpath));
		Thread.sleep(25000);
		try {
			try{
			String UserName = System.getProperty("user.name");
			System.out.println(UserName);

			File file = new File("C:\\Users\\" + UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");

			FileInputStream input = new FileInputStream(file);

			PDFParser parser = new PDFParser(input);

			parser.parse();

			COSDocument cosDoc = parser.getDocument();

			PDDocument pdDoc = new PDDocument(cosDoc);

			PDFTextStripper strip = new PDFTextStripper();
			
			strip.setStartPage(1);
			strip.setEndPage(2);

			String data = strip.getText(pdDoc);

		//	System.out.println(data);

			Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
			
			
			cosDoc.close();
			pdDoc.close();
			System.out.println("Text Found on the pdf File...");
			logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
			TestResult = "Pass";
			}
			catch(Exception e)
			{
				logger.log(LogStatus.FAIL, "Test Case is Failed Beacuse Premium Amount is different.");
			}


		} catch (Exception e) {

			String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
			System.out.println(ErrorMessage);
			logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
			TestResult = "Fail";
		}


	} else if (ProposalSummary.contains("Application No.")) {
		System.out.println(ThankyoupageProposal_Pol_num);
		try {
			Assert.assertEquals(PayuProposalNum, ThankyoupageProposal_Pol_num);
			TestResult = "Pass";
			logger.log(LogStatus.INFO,"Proposal number on Payu page and Thankyour Page is Verified and Both are Same");
		} catch (AssertionError e) {
			System.out.println(proposalSummarypremium_value + " - failed");
			TestResult = "Fail";
			logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same");
			throw e;
		}
	}

	// logger.log(LogStatus.PASS, "Test Step is Verified");

	WriteExcel.setCellData1("TestCases_Assure", TestResult, ThankyoupageProposal_Pol_num, n, 2, 3);

	driver.close();

} catch (Exception e) {
	WriteExcel.setCellData("TestCases_Assure", "Fail", n, 3);
	System.out.println(e.getMessage());
	logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
	logger.log(LogStatus.FAIL, "Test Case is fail beacuse getting :" +e.getMessage());
	//logger.log(LogStatus.FAIL, "Test Case is Failed.");
	driver.close();
	
}
 
continue; 
}
}
}
