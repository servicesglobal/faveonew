package com.org.faveo.proposal;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Proposal extends BaseClass implements AccountnSettingsInterface
{

	public static String TestResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	
	@Test
	public static void ExploreTravel() throws Exception
	{
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Explore_Test_Cases");
		System.out.println("Total Number of Row in Sheet : "+rowCount);

		for(int n=1; n<rowCount; n++)
		{
			
			try{
		String[][] TestCase=BaseClass.excel_Files("Explore_Test_Cases");
		String[][] TestCaseData=BaseClass.excel_Files("Explore_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Explore_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Explore_Question_Set");

		//From Here we are Calling Base Class Method to Open the Browser
		BaseClass.LaunchBrowser();

		//We are Taking Test Case name from Excel and Printing it in our Report
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("Explore Travel " + TestCaseName);
		
		//From Here we are Calling Login Class Method to Login in Faveo 
		LoginCase.LoginwithValidCredendial();
		
		//click on View Proposal
		clickElement(By.xpath(ViewProposal_xpath));
		
		
			
			
		}catch (Exception e) {
				WriteExcel.setCellData("Explore_Test_Cases", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed");
				driver.close();
			}
			continue;
		}

	}


}

	