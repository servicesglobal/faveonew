package com.org.faveo.model;

import org.openqa.selenium.By;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class HealthQuestions extends BaseClass implements AccountnSettingsInterface
{

	public static void CareNcbHealthQuestions(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		Thread.sleep(2000);
		String[][] QuestionSetData = BaseClass.excel_Files(QuestionSheetName);
		String[][] TestCaseData = BaseClass.excel_Files(TestCaseSheetName);
		String[] AlimentsQuestionData=null;
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
				
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=13;qlist++) {
					 Details =QuestionSetData[Rownum][qlist+(qlist-1)].toString().trim();
					 years=QuestionSetData[Rownum][qlist+qlist].toString().trim();
					 if(Details.equals(""))
						{
							//System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
						}
					 else if(Details.contains(",")) 
					 {
						 AlimentsQuestionData = Details.split(",");
						 String spilitter[]=years.split(",");
						 for(String AlimntQuesnData : AlimentsQuestionData)
						{
							 int detailsnumber = Integer.parseInt(AlimntQuesnData);
							 detailsnumber=detailsnumber+1;
							 System.out.println("Details and years are :"+Details+"----"+years);
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
							 String YearDetail = spilitter[0];
							 try {
								 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
								 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),YearDetail);
							 }catch(Exception e) {
								 	System.out.println("Unable to Select Health Questions.");
							 }
						}
						

					 }else if(Details.contains(""))
					 {
						 int detailsnumber = Integer.parseInt(Details);
						 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details+"----"+years);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
			 try {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
			 }catch(Exception e) {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
			 }
		 }
			
	}	
				 
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		
		//BaseClass.scrolldown();
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][Colnum + morechecks].toString().trim();
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		// Pa Question
		String JobRisk = TestCaseData[Rownum][Colnum+4].toString().trim();
		System.out.println("PA Question Value : "+JobRisk);
		try{
			Fluentwait(By.xpath(PAHealthQuestion_No_xpath), 5, "");
			/*clickElement(By.xpath(PAHealthQuestion_No_xpath));
			*/if(JobRisk.contains("YES") || JobRisk.contains("Yes") || JobRisk.contains("yes")) {
				clickElement(By.xpath(PAHealthQuestion_Yes_xpath));
			} else if (JobRisk.contains("NO") || JobRisk.contains("No") || JobRisk.contains("no")) {
				clickElement(By.xpath(PAHealthQuestion_No_xpath));
			}

		}catch(Exception e)
		{
			System.out.println("No Need to Select PA Addon.");
		}


		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));
		
		
	}
	
	public static void CareSuperSaverHealthQuestions(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] QuestionSetData = BaseClass.excel_Files(QuestionSheetName);
		String[][] TestCaseData = BaseClass.excel_Files(TestCaseSheetName);
		String[] AlimentsQuestionData=null;
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("yes") || preExistingdeases.contains("Yes")) {
				
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=13;qlist++) {
					 Details =QuestionSetData[Rownum][qlist+(qlist-1)].toString().trim();
					 years=QuestionSetData[Rownum][qlist+qlist].toString().trim();
					 if(Details.equals(""))
						{
							//System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
						}
					 else if(Details.contains(",")) 
					 {
						 AlimentsQuestionData = Details.split(",");
						 String spilitter[]=years.split(",");
						 for(String AlimntQuesnData : AlimentsQuestionData)
						{
							 int detailsnumber = Integer.parseInt(AlimntQuesnData);
							 detailsnumber=detailsnumber+1;
							 System.out.println("Details and years are :"+Details+"----"+years);
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
							 String YearDetail = spilitter[0];
							 try {
								 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
								 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),YearDetail);
							 }catch(Exception e) {
								 	System.out.println("Unable to Select Health Questions.");
							 }
						}
						

					 }else if(Details.contains(""))
					 {
						 int detailsnumber = Integer.parseInt(Details);
						 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details+"----"+years);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
			 try {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
			 }catch(Exception e) {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
			 }
		 }
			
	}	
				 
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		
		//BaseClass.scrolldown();
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][Colnum + morechecks].toString().trim();
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("no") || ChecksData.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else 
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));
		
	}
	
	public static void CareFreedomHealthQuestions(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] QuestionSetData = BaseClass.excel_Files(QuestionSheetName);
		String[][] TestCaseData = BaseClass.excel_Files(TestCaseSheetName);
		String[] AlimentsQuestionData=null;
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
				
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String Details=null;		
				 for(int qlist=1;qlist<=12;qlist++) {
					 Details =QuestionSetData[Rownum][qlist].toString().trim();
					 if(Details.equals(""))
						{
							//System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
						}
					 else if(Details.contains(",")) 
					 {
						 AlimentsQuestionData = Details.split(",");
						 
						 for(String AlimntQuesnData : AlimentsQuestionData)
						{
							 int detailsnumber = Integer.parseInt(AlimntQuesnData);
							 detailsnumber=detailsnumber+1;
							 System.out.println("Details and :"+Details);
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
							
						}
						

					 }else if(Details.contains(""))
					 {
						 int detailsnumber = Integer.parseInt(Details);
						 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
		 }
			
	}	
				 
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		
		//BaseClass.scrolldown();
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 2; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][Colnum + morechecks].toString().trim();
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		Thread.sleep(5000);
		clickElement(By.xpath(proceed_to_pay_xpath));


	}

	public static void CareGlobalHealthQuestions(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] QuestionSetData = BaseClass.excel_Files(QuestionSheetName);
		String[][] TestCaseData = BaseClass.excel_Files(TestCaseSheetName);
		String[] AlimentsQuestionData=null;
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
				
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=13;qlist++) {
					 Details =QuestionSetData[Rownum][qlist+(qlist-1)].toString().trim();
					 years=QuestionSetData[Rownum][qlist+qlist].toString().trim();
					 if(Details.equals(""))
						{
							//System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
						}
					 else if(Details.contains(",")) 
					 {
						 AlimentsQuestionData = Details.split(",");
						 String spilitter[]=years.split(",");
						 for(String AlimntQuesnData : AlimentsQuestionData)
						{
							 int detailsnumber = Integer.parseInt(AlimntQuesnData);
							 detailsnumber=detailsnumber+1;
							 System.out.println("Details and years are :"+Details+"----"+years);
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
							 String YearDetail = spilitter[0];
							 try {
								 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
								 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),YearDetail);
							 }catch(Exception e) {
								 	System.out.println("Unable to Select Health Questions.");
							 }
						}
						

					 }else if(Details.contains(""))
					 {
						 int detailsnumber = Integer.parseInt(Details);
						 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details+"----"+years);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
			 try {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
			 }catch(Exception e) {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
			 }
		 }
			
	}	
				 
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		
		//BaseClass.scrolldown();
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][Colnum + morechecks].toString().trim();
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("no") || ChecksData.contains("No")) 
			{
				System.out.println("Quatation set to NO");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));


	}
	
	public static void CareHNIQuestions(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] QuestionSetData = BaseClass.excel_Files(QuestionSheetName);
		String[][] TestCaseData = BaseClass.excel_Files(TestCaseSheetName);
		String[] AlimentsQuestionData=null;
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
				
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=13;qlist++) {
					 Details =QuestionSetData[Rownum][qlist+(qlist-1)].toString().trim();
					 years=QuestionSetData[Rownum][qlist+qlist].toString().trim();
					 if(Details.equals(""))
						{
							//System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
						}
					 else if(Details.contains(",")) 
					 {
						 AlimentsQuestionData = Details.split(",");
						 String spilitter[]=years.split(",");
						 for(String AlimntQuesnData : AlimentsQuestionData)
						{
							 int detailsnumber = Integer.parseInt(AlimntQuesnData);
							 detailsnumber=detailsnumber+1;
							 System.out.println("Details and years are :"+Details+"----"+years);
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
							 String YearDetail = spilitter[0];
							 try {
								 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
								 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),YearDetail);
							 }catch(Exception e) {
								 	System.out.println("Unable to Select Health Questions.");
							 }
						}
						

					 }else if(Details.contains(""))
					 {
						 int detailsnumber = Integer.parseInt(Details);
						 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details+"----"+years);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
			 try {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
			 }catch(Exception e) {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
			 }
		 }
			
	}	
				 
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		
		//BaseClass.scrolldown();
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][Colnum + morechecks].toString().trim();
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else 
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));

		
	}

	public static void Enhnace(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		
		String[][] TestCaseData=BaseClass.excel_Files(TestCaseSheetName);
		String[][] QuestionSetData=BaseClass.excel_Files(QuestionSheetName);
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) 
			 {
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=10;qlist++) {
				 Details =QuestionSetData[Rownum][qlist+(qlist-1)].toString().trim();
				 years=QuestionSetData[Rownum][qlist+qlist].toString().trim();
				 if(Details.equals("")) 
				 {
						 //break;
					 }else 
					 {
						 int detailsnumber = Integer.parseInt(Details);

			 //Will click on check box and select the month & year u
			 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details+"----"+years);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
			 Thread.sleep(1000);
			 try {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
			 }catch(Exception e) {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
			 }
		 }
	}	
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		//Question Number 2
		String QuestionNumber2 = TestCaseData[Rownum][Colnum+1].toString().trim();
		String[] ChckData = null;
		int datacheck = 0;
		try{
			
			if (QuestionNumber2.contains("NO") || QuestionNumber2.contains("No") || QuestionNumber2.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				System.out.println("//label[@for='question_6_no']");
				clickElement(By.xpath("//label[@for='question_6_no']"));
			} 
			else
			{
				driver.findElement(By.xpath("//label[@for='question_6_yes']")).click();
				if (QuestionNumber2.contains(",")) 
				{
					ChckData = QuestionNumber2.split(",");
					for (String Chdata : ChckData) 
					{
						
						datacheck = Integer.parseInt(Chdata);
						int mch=datacheck;
						datacheck = datacheck - 1;
						System.out.println("//input[@name='qs_H00"+mch+"_"+datacheck+"']");
						driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
						
					}
				} else if (QuestionNumber2.contains(""))
				{
					datacheck = Integer.parseInt(QuestionNumber2);
					int mch=datacheck;
					datacheck = datacheck - 1;
					System.out.println("//input[@name='qs_H00"+mch+"_"+datacheck+"']");
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}
		
		
		BaseClass.scrolldown();
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][(Colnum+1) + morechecks].toString().trim();
			System.out.println("Check data is : "+ChecksData);
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				System.out.println("//label[@for='question_"+mch+"_no']");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));

		
	}
	
	public static void AssureQuestion(String SheetTestData, String QuestiondataSheet, int Rownum, int prevColnum) throws Exception
	{
		String ChecksData = null;
		String[][] TestCaseData=BaseClass.excel_Files(SheetTestData);
		String[][] QuestionSetData=BaseClass.excel_Files(QuestiondataSheet);
		try{
		for (int morechecks = 1; morechecks <= 3; morechecks++) {
			int mch = morechecks;
			ChecksData = TestCaseData[Rownum][prevColnum + morechecks].toString().trim();
			clickElement(By.xpath("//label[@for='question_"+mch+"_"+ChecksData+"']"));
		
		}
				}
				catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}
		
		String preExistingdeases = TestCaseData[Rownum][prevColnum+4].toString().trim();
		ExplicitWait(By.xpath("//label[@for='question_4_yes']"), 2);
		System.out.println("Are you suffering from any Pre-existing Disease? :" + preExistingdeases);
		if (preExistingdeases.contains("yes") || preExistingdeases.contains("YES") || preExistingdeases.contains("Yes")) {
			
			clickElement(By.xpath("//label[@for='question_4_yes']"));
			String Details = null;
			for (int qlist = 1; qlist <= 9; qlist++) {
				Details = QuestionSetData[Rownum][qlist].toString().trim();
				if (Details.equals("")) {
					// break;
				} else {
					int detailsnumber = Integer.parseInt(Details);

					detailsnumber = detailsnumber + 1;
					System.out.println("Details is :" + Details);
					clickElement(By.xpath("//*[@id='accordion_q']/div[1]/div[2]/div/table/tbody/tr["+qlist+"]/td[2]/div/input"));
				}

			}
		} else if (preExistingdeases.contains("no") || preExistingdeases.contains("No") || preExistingdeases.contains("NO")) {
			clickElement(By.xpath("//label[@for='question_4_no']"));
		}
		

		for (int morechecks = 1; morechecks <= 7; morechecks++) {
			int mch = morechecks+4;
			ChecksData = TestCaseData[Rownum][(prevColnum+4) + morechecks].toString().trim();
			clickElement(By.xpath("//label[@for='question_"+mch+"_"+ChecksData+"']"));
		
		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));

	}

	public static void SecureQuestionSet(String TestDataSheet, String QuestionDataSheet, int Rownum,int Colnum) throws Exception
	{
		String[][] TestCaseData=BaseClass.excel_Files(TestDataSheet);
		String[][] QuestionSetData=BaseClass.excel_Files(QuestionDataSheet);
		String preExistingdeases=TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(secureYes_question_xpath),2);
		System.out.println("Do you have an existing personal Accident/Health Insurance policy with Religare or any other Insurer? :" +preExistingdeases);
		BaseClass.scrollup();
		if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
			  
			waitForElements(By.xpath(secureYes_question_xpath));
			  clickElement(By.xpath(secureYes_question_xpath));		
			  
			  String InsurerDetails=QuestionSetData[Rownum][1];
			  enterText(By.xpath(Insurer_Details_xpath),InsurerDetails);
			  System.out.println("Entered Insurer Name is : "+InsurerDetails);
			  
			  String PolicyNum=QuestionSetData[Rownum][2];
			  enterText(By.xpath(Policynumber_xpath),PolicyNum);
			  System.out.println("Entered Policy Number is : "+PolicyNum);
			  
			  String PlanName=QuestionSetData[Rownum][3];
			  enterText(By.xpath(PlanName_xpath),PlanName);
			  System.out.println("Entered Plan Name is : "+PlanName);
			  
			  String SumInsuredDetails=QuestionSetData[Rownum][4];
			  enterText(By.xpath(Sum_Insured_xpath),SumInsuredDetails);
			  System.out.println("Entered SumInsured Details is : "+SumInsuredDetails);
			  
		 }	  else if(preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")){
			 clickElement(By.xpath(secureNo_question_xpath));
			  }
		
		String important_political_party_officials = TestCaseData[Rownum][Colnum+1].toString().trim();
		System.out.println("Have you ever been entrusted with prominent public functions ? : "+important_political_party_officials);
		if(important_political_party_officials.contains("YES") || important_political_party_officials.contains("Yes") || important_political_party_officials.contains("yes"))
		{
			clickElement(By.xpath(political_party_officials_Question_Yes_xpath));
		}else if(important_political_party_officials.contains("No") || important_political_party_officials.contains("NO") || important_political_party_officials.contains("no"))
		{
			clickElement(By.xpath(political_party_officials_Question_No_xpath));
		}
				
		String Hazardous_activity = TestCaseData[Rownum][Colnum+2].toString().trim();
		System.out.println("Does your job require you to be involved with any hazardous activity ? :" +Hazardous_activity);
		if(Hazardous_activity.contains("YES") || Hazardous_activity.contains("Yes") || Hazardous_activity.contains("yes"))
		{
			clickElement(By.xpath(Hazardous_activity_Yes_xpath));
		}else if(Hazardous_activity.contains("No") || Hazardous_activity.contains("NO") || Hazardous_activity.contains("no"))
		{
			clickElement(By.xpath(Hazardous_activity_No_xpath));
		}
		
		String illness_or_disease = TestCaseData[Rownum][Colnum+3].toString().trim();
		System.out.println("Have you ever been diagnosed with or are you under treatment for any disability ? :" +illness_or_disease);
		if(illness_or_disease.contains("YES") || illness_or_disease.contains("Yes") || illness_or_disease.contains("yes"))
		{
			clickElement(By.xpath(illness_or_disease_Yes_xpath));
		}else if(illness_or_disease.contains("No") || illness_or_disease.contains("NO") || illness_or_disease.contains("no"))
		{
			clickElement(By.xpath(illness_or_disease_No_xpath));
		}
	
		String Personal_Accident=TestCaseData[Rownum][Colnum+4].toString().trim();
		System.out.println("Has any company ever declined to issue/renew a Personal Accident policy for any proposed? If yes, please provide details? : "+Personal_Accident);
		if(Personal_Accident.contains("YES") || Personal_Accident.contains("Yes") || Personal_Accident.contains("yes"))
		{
			clickElement(By.xpath(Personal_Accident_Yes_xpath));
		}else if(Personal_Accident.contains("No") || Personal_Accident.contains("NO") || Personal_Accident.contains("no"))
		{
			clickElement(By.xpath(Personal_Accident_No_xpath));
		}
		
		String extreme_sports=TestCaseData[Rownum][Colnum+5].toString().trim();
		System.out.println("Do you participate in Adventure/ extreme sports? : "+extreme_sports);
		if(extreme_sports.contains("YES") || extreme_sports.contains("Yes") || extreme_sports.contains("yes"))
		{
			clickElement(By.xpath(extreme_sports_Yes_xpath));
		}else if(extreme_sports.contains("No") || extreme_sports.contains("NO") || extreme_sports.contains("no"))
		{
			clickElement(By.xpath(extreme_sports_No_xpath));
		}
		
		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));


	}

	public static void CareSeniorQuestions(String TestCaseSheetName,String QuestionSheetName,int Rownum,int Colnum) throws Exception
	{
		Thread.sleep(2000);
		String[][] QuestionSetData = BaseClass.excel_Files(QuestionSheetName);
		String[][] TestCaseData = BaseClass.excel_Files(TestCaseSheetName);
		String[] AlimentsQuestionData=null;
		String preExistingdeases = TestCaseData[Rownum][Colnum].toString().trim();
		ExplicitWait(By.xpath(YesButton_xpath), 2);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		BaseClass.scrollup();
		try{
			 if(preExistingdeases.contains("YES") || preExistingdeases.contains("Yes") || preExistingdeases.contains("yes")) {
				
				 waitForElements(By.xpath(YesButton_xpath));
				 clickElement(By.xpath(YesButton_xpath));
				 String years=null;
				 String Details=null;		
				 for(int qlist=1;qlist<=13;qlist++) {
					 Details =QuestionSetData[Rownum][qlist+(qlist-1)].toString().trim();
					 years=QuestionSetData[Rownum][qlist+qlist].toString().trim();
					 if(Details.equals(""))
						{
							//System.out.println("No Need to Select question : Any other diseases or ailments not mentioned above."); 
						}
					 else if(Details.contains(",")) 
					 {
						 AlimentsQuestionData = Details.split(",");
						 String spilitter[]=years.split(",");
						 for(String AlimntQuesnData : AlimentsQuestionData)
						{
							 int detailsnumber = Integer.parseInt(AlimntQuesnData);
							 detailsnumber=detailsnumber+1;
							 System.out.println("Details and years are :"+Details+"----"+years);
							 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
							 String YearDetail = spilitter[0];
							 try {
								 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
								 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),YearDetail);
							 }catch(Exception e) {
								 	System.out.println("Unable to Select Health Questions.");
							 }
						}
						

					 }else if(Details.contains(""))
					 {
						 int detailsnumber = Integer.parseInt(Details);
						 detailsnumber=detailsnumber+1;
			 System.out.println("Details and years are :"+Details+"----"+years);
			 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
			 try {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
			 }catch(Exception e) {
				 clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
				 enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
			 }
		 }
			
	}	
				 
	} else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) {
		clickElement(By.xpath(NoButton_xpath));
		}
		}
		catch(Exception e)
				{
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
				}

		
		//BaseClass.scrolldown();
		String[] ChckData = null;
		int datacheck = 0;
		for (int morechecks = 1; morechecks <= 3; morechecks++) 
		{
			int mch = morechecks + 1;
			String ChecksData = TestCaseData[Rownum][Colnum + morechecks].toString().trim();
			try{
			if (ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
			{
				System.out.println("Quatation set to NO");
				clickElement(By.xpath("//label[@for='question_"+mch+"_no']"));
			} 
			else
			{
				driver.findElement(By.xpath("//label[@for='question_"+mch+"_yes']")).click();
				if (ChecksData.contains(",")) 
				{
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) 
					{
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(
						By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
					}
				} else if (ChecksData.contains(""))
				{
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00"+mch+"_"+datacheck+"']")).click();
				}
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
			}

		}

		BaseClass.scrolldown();
		
		BaseClass.HelathQuestionnairecheckbox();
		
		clickElement(By.xpath(proceed_to_pay_xpath));	
	}
	
	

}
