package com.org.faveo.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class AddonsforProducts extends BaseClass implements AccountnSettingsInterface {
	
	public static String ExpectedpremiumAmount=null;

	public static void CareWithNCBAddons(String SheetName, int Rownum, int Colnum) throws Exception 
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String[] AddonsData = null;
		String Addon = TestCaseData[Rownum][Colnum].toString().trim();
		try{
		if (Addon.contains(",")) 
		{
			AddonsData = Addon.split(",");
			for (String AddonData : AddonsData) 
			{
				System.out.println("Selected Addons is : " + AddonData);
				Thread.sleep(3000);
				/*ExplicitWait(By.xpath("//b[contains(text()," + "'" + AddonData + "'" + ")]"), 4);*/
				clickElement(By.xpath("//b[contains(text()," + "'" + AddonData + "'" + ")]"));
			}
		} else if (Addon.contains("")) {
			System.out.println("Selected Addons is : " + Addon);
			ExplicitWait(By.xpath("//b[contains(text()," + "'" + Addon + "'" + ")]"), 2);
			clickElement(By.xpath("//b[contains(text()," + "'" + Addon + "'" + ")]"));
		}
		if (Addon.equals("")) {
			System.out.println("No Need to Select any Addon Beacuse there is no data in Excel with respect to Addon.");
		}}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}
	
	public static void CareSeniorAddons(String SheetName, int Rownum, int Colnum) throws Exception 
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String[] AddonsData = null;
		String Addon = TestCaseData[Rownum][Colnum].toString().trim();
		try{
		if (Addon.contains(",")) 
		{
			AddonsData = Addon.split(",");
			for (String AddonData : AddonsData) 
			{
				System.out.println("Selected Addons is : " + AddonData);
				Thread.sleep(3000);
				/*ExplicitWait(By.xpath("//b[contains(text()," + "'" + AddonData + "'" + ")]"), 4);*/
				clickElement(By.xpath("//b[contains(text()," + "'" + AddonData + "'" + ")]"));
			}
		} else if (Addon.contains("")) {
			System.out.println("Selected Addons is : " + Addon);
			ExplicitWait(By.xpath("//b[contains(text()," + "'" + Addon + "'" + ")]"), 2);
			clickElement(By.xpath("//b[contains(text()," + "'" + Addon + "'" + ")]"));
		}
		if (Addon.equals("")) {
			System.out.println("No Need to Select any Addon Beacuse there is no data in Excel with respect to Addon.");
		}}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	public static void CareSuperSaverAddons(String SheetName,int Rownum,int Colnum) throws Exception
	{

		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String NCBSuper = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Addon is : "+NCBSuper);
		ExplicitWait(By.xpath("//b[contains(text(),"+"'"+NCBSuper+"'"+")]"), 4);
		clickbyHover(By.xpath("//b[contains(text()," + "'" + NCBSuper + "'" + ")]"));
	}
	
	public static void CareFreedomAddons(String SheetName, int Rownum,int Colnum) throws Exception
	{

		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Addon = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Addon is : "+Addon);
		int n = 0;
		switch(Addon)
		{
		case "For Care Freedom" : n=0;
					break;
		case "With Home Care" : n=1;
					break;
		case "With Health Check Plus" : n=2;
						break;
		case "With Home Care, Health Check Plus" : n=3;
		break;

		default : System.out.println("Invalid Addon.");
		}
		try{
			
		Thread.sleep(2000);
		clickElement(By.xpath("//label[@for='premiumRadio"+n+"q']//img[@src='assets/img/correct_signal.png']"));
		}
		catch(Exception e)
		{
			System.out.println("Unable to Select Addon.");
		}
	}

	public static void CareGlobal(String SheetName,int Rownum, int Colnum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Addons = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Care Global Addon is : " +Addons);
		int n=0;
		switch(Addons)
		{
			case "Excluding USA with ISO and AAC" : n=0;
					break;
			case "Including USA with ISO and AAC" : n=1;
			break;
			default : System.out.println("Invalid Addon.");		
		}
		
		try{
		clickbyHover(By.xpath("//label[@for='premiumRadio"+n+"q']//img[@src='assets/img/correct_signal.png']"));
		logger.log(LogStatus.PASS, "User has Selected : " + Addons + " Addon");
		}
		catch(Exception e)
		{
			System.out.println("Unable to Select Addon.");
		}
	}
	
	public static void AssureAddons(String SheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Addons = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selceted Addon in Assure is : "+Addons);
		int n=0;
		switch(Addons)
		{
			case "Assure Addon" : n=0;
					break;
			default : n=1;		
		}
		clickElement(By.xpath("//label[@for='premiumRadio"+n+"q']//img[@src='assets/img/correct_signal.png']"));
		
	}
	
	public static void SecureAddon(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Addons = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Addons values is : "+Addons);
		if(Addons.contains("Accidental Hospitalization Expenses")){
			clickbyHover(By.xpath(Secure_Addon_xpath));
		}else{
			System.out.println("No Addons Selected in Secure.");
		}

	}
	
	public static void EnhanceAddon(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String 	Addons = TestCaseData[Rownum][Colnum].toString().trim();
		int n=0;
		switch(Addons)
		{
		case "with Add on Benefit" : n=1;
									break;
		default : n=0;
		}
		clickElement(By.xpath("//label[@for='premiumRadio"+n+"q']//img[@src='assets/img/correct_signal.png']"));
		
	}
	
	public static void TravelAddon(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String 	Addons = TestCaseData[Rownum][Colnum].toString().trim();
		int n=0;
		switch(Addons)
		{
		case "No Addon" : n=0;
						break;
		case "for gold plan" : n=0;
						break;
		case "with platinum plan" : n=1;
						break;				
		default : System.out.println("Please Select Valid Addon.");
		}
		clickElement(By.xpath("//label[@for='premiumRadio"+n+"q']//img[@src='assets/img/correct_signal.png']"));
		
	}
	
	//---------- Super Mediclaim Add On Functions------------------------------------------------

		public static void setPaymentFrequency(String SheetName, int Rownum, int Colnum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String 	tenure = TestCaseData[Rownum][8].toString().trim();
			String 	paymentFrequency = TestCaseData[Rownum][Colnum].toString().trim();
			Thread.sleep(2000);
			if(tenure.equalsIgnoreCase("1 Year")){
				clickElement(By.xpath(superMediclaim_BuyNowButton_Xpath));
			}
			else{
				
				switch(paymentFrequency){
				case "One Time":
				break;
				case "Monthly":
					clickElement(By.xpath(superMediclaim_PaymentFrequency_Monthly_Xpath));
				break;
				case "Quarterly":
					clickElement(By.xpath(superMediclaim_PaymentFrequency_Quarterly_Xpath));
				break;
				default:
					System.out.println("Not Clicked on Buy Now button");
				}
				readPremiumFromFirstPage();
				
			}
			
			logger.log(LogStatus.PASS, "Data Entered for Payment Frequency: " + paymentFrequency);
		}
		
		public static String premium1;
		//Read Premium Amount on - First Page
		public static void readPremiumFromFirstPage() throws Exception{
			Thread.sleep(5000);
			WebElement e1=driver.findElement(By.xpath("//p[@class='get_quot_total_premium']/span"));
			waitTillElementVisible(By.xpath("//p[@class='get_quot_total_premium']/span"));
			scrollup();
			 premium1=e1.getText();
			System.out.println(premium1);
			ExpectedpremiumAmount=premium1.replaceAll("\\W", "");
			System.out.println("Premium Amount in Quotation Page: "+  ExpectedpremiumAmount);
			logger.log(LogStatus.PASS, "Premium Amount in Quotation Page: "+  ExpectedpremiumAmount);
			
		}
		
		//public static String ExpectedpremiumAmountTravel;
		//Read Premium Amount on - First Page
		public static void readPremiumFromFirstPage_Travel() throws Exception{
			Thread.sleep(5000);
			WebElement e1=driver.findElement(By.xpath("//label[@for='premiumRadio0q']/b[1]"));
			waitTillElementVisible(By.xpath("//label[@for='premiumRadio0q']/b[1]"));
			//scrollup();
			ExpectedpremiumAmount=e1.getText();
			System.out.println(ExpectedpremiumAmount);
			ExpectedpremiumAmount=ExpectedpremiumAmount.replaceAll("\\W", "");
			System.out.println("Premium Amount in Quotation Page: "+  ExpectedpremiumAmount);
			logger.log(LogStatus.PASS, "Premium Amount in Quotation Page: "+  ExpectedpremiumAmount);
			
		}
		
	
}
