package com.org.faveo.model;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.utility.ReadExcel;
import com.relevantcodes.extentreports.LogStatus;

public class QuotationPage extends BaseClass implements AccountnSettingsInterface {

		
	public static void ProposerName(String SheetName, int Rownum, int colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Name =TestCaseData[Rownum][2].toString().trim();
		System.out.println("Entered Proposer Name is : " + Name);
		if (Name.equals("")) {
			System.out.println("Please enter Name on Quotation Page.");
			logger.log(LogStatus.WARNING, "Please enter Name on Quotation Page.");
		} else {
			Fluentwait(By.xpath(Textbox_Name_xpath));
			enterText(By.xpath(Textbox_Name_xpath), Name);
			logger.log(LogStatus.PASS, "Entered User Proposer Name: " + Name);
		}
	}

	public static void ProposerEmail(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Email = (TestCaseData[Rownum][Colnum].toString().trim());
		System.out.println("Entered EmailId is : " + Email);
		logger.log(LogStatus.PASS, "Email entered: " + Email);
		if (Email.equals("")) {
			System.out.println("Please enter Email on Quotation Page.");
			logger.log(LogStatus.WARNING, "Please enter Email on Quotation Page.");
		} else {
			if (Email.contains("@")) {
				Fluentwait(By.xpath(Textbox_Email_xpath));
				enterText(By.xpath(Textbox_Email_xpath), Email);
			} else {
				
				System.out.println("Please enter Email on Quotation Page.");
				logger.log(LogStatus.WARNING, "Please enter valid Email Id on Quotation Page.");
			}
		}
	}

	public static void ProposerMobile(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Mobile = (TestCaseData[Rownum][Colnum].toString().trim());
		int Size = Mobile.length();
		System.out.println("Entered Mobile Number is : " + Mobile);
		logger.log(LogStatus.PASS, "Mobile Number entered: " + Mobile);
		if (Mobile.equals("")) {
			System.out.println("Please enter Mobile Number on Quotation Page.");
			logger.log(LogStatus.WARNING, "Please enter Mobile Number on Quotation Page.");
		} else {
			if (Size == 10) {
				Fluentwait(By.xpath(Textbox_Mobile_xpath));
				enterText(By.xpath(Textbox_Mobile_xpath), Mobile);
			} else {
				System.out.println("Please enter mobile number Minimum of 10 digits required.");
				logger.log(LogStatus.WARNING, "Please enter mobile number Minimum of 10 digits required.");
			}
		}
	}

	public static void SelectMembers(String SheetName, int Rownum, int Colnum) throws Exception {
		/*Thread.sleep(6000);*/
		Fluentwait(By.xpath(TotalMember_Xpath), 60, "Response is coming late from Abacuse so couldn't find Total member.");
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) 
				{
					Thread.sleep(2000);
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li[" + TotalMember + "]"))
							.click();
					System.out.println("Total Number of Member Selected : " + TotalMember);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		logger.log(LogStatus.PASS, "Total Number of Member Selected: " + TotalMember);
	}
	
	public static void SelectMembersCareSenior(String SheetName, int Rownum, int Colnum) throws Exception {
		/*Thread.sleep(6000);*/
		Fluentwait(By.xpath("//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding'][contains(text(),'2')]"), 60, "Response is coming late from Abacuse so couldn't find Total member.");
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = (TestCaseData[Rownum][Colnum].toString().trim());
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("2")) 
				{
					Thread.sleep(2000);
					/*clickElement(By.xpath("//div[@class='dropdown year_drop_slect master open']//a[@class='ng-binding'][contains(text(),"+"'"+TotalMember+"'"+")]"));*/
					driver.findElement(By.xpath("//div[@class='dropdown year_drop_slect master open']//a[@class='ng-binding'][contains(text(),"+"'"+TotalMember+"'"+")]")).click();
					System.out.println("Total Number of Member Selected : " + TotalMember);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

	}

	public static void SelectMemberHNI(String SheetName, int Rownum, int Colnum) throws Exception
	{
		Fluentwait(By.xpath(CareHni_Member_Xpath), 60, "Response is coming late from Abacuse so couldn't find Total member.");
		WebElement policy_name = driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[1]/div/p/span"));
		String name = policy_name.getText().toString().trim();
		if (name.equals("Care For HNI")) {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			int TotalMember = Integer.parseInt(TestCaseData[Rownum][Colnum].toString().trim());
			System.out.println("if loop started");
			List<WebElement> dropdown = driver.findElements(By.xpath(AllDropdown_Xpath));
			for (WebElement DropDownName : dropdown) {
				Fluentwait(By.xpath(AllDropdown_Xpath));
				DropDownName.click();
				System.out.println("Total Number of member in Excel Sheet : " + TotalMember);
				int ActualCount = TotalMember - 1;
				driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + ActualCount + "]")).click();
				break;
			}
		}
	}
	
	public static void SelectAgeofMember(String SheetName, String SheetNameInsured, int Rownum, int MembernumColnum,
		int MemDetailCol, int ChildColnum, int CovertypeColnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String[][] FamilyData = BaseClass.excel_Files(SheetNameInsured);
		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		
		int membersSize = Integer.parseInt(TestCaseData[Rownum][MembernumColnum].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3

		outer:

		for (WebElement DropDownName : dropdown) {
			if (membersSize == 1) {

				// reading members from test cases sheet memberlist
				String Membersdetails = TestCaseData[Rownum][MemDetailCol];
				System.out.println("Total Number of Member in Excel :" + Membersdetails);
				if (Membersdetails.contains("")) {

			
					BaseClass.membres = Membersdetails.split("");

					member:
					// total number of members
					for (int i = 0; i <= BaseClass.membres.length; i++) {
						// System.out.println("Mdeatils is :
						// "+membres);

						// one by one will take from 83 line
						mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
						mcountindex = mcountindex + 1;
						System.out.println("Mcount Index : " + mcountindex);

						driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();
						// List Age of members dropdown
						List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
						
						for (WebElement ListData : List) {

							if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
								System.out.println("List Data is :" + ListData.getText());

								ListData.click();

								if (count == membersSize) {
									break outer;
								} else {
									count = count + 1;
									break member;
								}

							}

						}
					}
				}

			} else if (DropDownName.getText().contains("Individual")) {
				System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

				if (TestCaseData[Rownum][CovertypeColnum].toString().trim().equals("Individual")) {
					covertype = 1;
				} else {
					covertype = 2;
				}
				DropDownName.click();
				driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ covertype + "]")).click();
				// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
				Thread.sleep(2000);
				if (covertype == 2) 
				{
					List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
					for (WebElement DropDowns : dropdowns) 
					{
						if (DropDowns.getText().contains("Floater")) 
						{
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals(TestCaseData[Rownum][MembernumColnum].toString().trim())) {
							System.out.println("DropDownName is  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("2")) {
							System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
						} else if (DropDowns.getText().equals("18 - 24 years")) {
							// reading members from test cases sheet
							// memberlist
				int Children = Integer.parseInt(TestCaseData[Rownum][ChildColnum].toString().trim());
				clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
				clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
				System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
							String Membersdetails = TestCaseData[Rownum][MemDetailCol];
							if (Membersdetails.contains(",")) {

								BaseClass.membres = Membersdetails.split(",");
							} else {
								//BaseClass.membres = Membersdetails.split(" ");
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
								// System.out.println("Mdeatils is :
								// "+membres);

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								
								List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								for (WebElement ListData : List) {
									
									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										Thread.sleep(2000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											break outer;
										}

									}

								}
							}
							
						}
					}
				}
			} 
			else{

				List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
				for (WebElement DropDowns : dropdowns) 
				{
					if (DropDowns.getText().contains("Floater")) 
					{
						System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
					} else if (DropDowns.getText().equals("5 - 24 years")) {
		
						String Membersdetails = TestCaseData[Rownum][MemDetailCol];
						if (Membersdetails.contains(",")) {
							BaseClass.membres = Membersdetails.split(",");
						} else 
						{
							System.out.println("Hello");
						}

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {
						
							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							DropDowns.click();
							// List Age of members dropdown
							
							List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (WebElement ListData : List) {
								
								if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
									Thread.sleep(2000);
									ListData.click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break member;
									}

								}

							}
						}
						
					}
				}
			}
		}
	}

	
	public static void SumInsured(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		int SumInsured = Integer.parseInt(TestCaseData[Rownum][Colnum].toString().trim());
		try{
	clickElement(By.xpath(slider_xpath));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), "+SumInsured+")]"));
			/*Thread.sleep(5000);
			WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/div/div"));
			Thread.sleep(5000);
			List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
			//String SliderValue=null;

			for(WebElement Slider:dragable) {
				
				Thread.sleep(5000);
				if(	Slider.getText().equals(SumInsured)) {
					Slider.click();
					break;
				}
			}*/
		System.out.println("Entered Sum Insured is : "+SumInsured+" Lakhs");	
		logger.log(LogStatus.PASS, "Data entered for Sum Insured: " + SumInsured + "Lakhs");
		}
		catch(Exception e)
		{
			System.out.println(e);
			logger.log(LogStatus.FAIL, e);
		}
	}


	public static void Tenure(String SheetName, int Rownum, int Colnum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		int Tenure = Integer.parseInt(TestCaseData[Rownum][Colnum].toString().trim());
		int Tenure1=Tenure-1;
		System.out.println("Selected Tenure is : "+Tenure+" Year");
		scrolldown();
		Thread.sleep(2000);
		clickElement(By.xpath("//label[@for='Radio"+Tenure1+"q']//img[@src='assets/img/correct_signal.png']"));
scrollup();
	}
	
	public static void GetQuotationPremium()
	{
		// Capture The Premium value
		ExplicitWait(By.xpath(Quotationpage_premium_xpath), 3);
		String Quotationpremium_value = driver.findElement(By.xpath(Quotationpage_premium_xpath)).getText();
		System.out.println("Total Premium Value on Quotation Page is :" + " - "+ Quotationpremium_value.substring(1, Quotationpremium_value.length()));

	}
	
	public static void SelectAgeofMemberCareFreedom(String SheetName, String SheetNameInsured, int Rownum, int MembernumColnum,
			int MemDetailCol, int ChildColnum, int CovertypeColnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String[][] FamilyData = BaseClass.excel_Files(SheetNameInsured);
		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[Rownum][MembernumColnum].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		try {
			outer:

				for (WebElement DropDownName : dropdown) {
					if (membersSize == 1) {
						// reading members from test cases sheet memberlist
						String Membersdetails = TestCaseData[Rownum][MemDetailCol];
						if (Membersdetails.contains("")) {
							BaseClass.membres = Membersdetails.split("");

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {
								
								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;
			
								driver.findElement(By
										.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
										.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));
			
								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					}  else if (DropDownName.getText().contains("Individual")) 
					{
							System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());							

						if (TestCaseData[Rownum][CovertypeColnum].toString().trim().equals("Individual")) {
							covertype = 1;
						} 
						else 
						{
							covertype = 2;
						}
						
						Thread.sleep(3000);
						DropDownName.click();
						Thread.sleep(4000);
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["+ covertype + "]")).click();
						ImplicitWait(10);
						Thread.sleep(5000);
						if (covertype == 2) 
						{
							List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) 
							{
								if (DropDowns.getText().contains("Floater")) 
								{
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[Rownum][MembernumColnum].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("18 to 35 Yrs")||DropDowns.getText().equals("36 to 40 Yrs")||DropDowns.getText().equals("41 to 45 Yrs")||
										DropDowns.getText().equals("46 - 50 Yrs")||DropDowns.getText().equals("51 - 55 Yrs")||DropDowns.getText().equals("56 - 60 Yrs")||
										DropDowns.getText().equals("61 - 65 Yrs")||DropDowns.getText().equals("66 - 70 Yrs")||DropDowns.getText().equals("71 - 75 Yrs")||
										DropDowns.getText().equals("76 - 80 Yrs")||DropDowns.getText().equals("Above 80 Yrs")||DropDowns.getText().equals("90 Days to 5 Yrs")
										||DropDowns.getText().equals("6 Yrs to 17 Yrs")) {
									// reading members from test cases sheet
									// memberlist
						int Children = Integer.parseInt(TestCaseData[Rownum][ChildColnum].toString().trim());
						clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
						clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
						System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[Rownum][MemDetailCol];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										
										System.out.println("Hello");
									}

									member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {
										// System.out.println("Mdeatils is :
										// "+membres);

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

										for (WebElement ListData : List) {
											
											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												ImplicitWait(5);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}
									
								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) 
						{
							System.out.println("List Value: "+ DropDowns.getText());
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("90 Days - 5 Yrs")||DropDowns.getText().equals("6 - 17 Yrs")||DropDowns.getText().equals("18 - 35 Yrs")
									|| DropDowns.getText().equals("36 - 40 Yrs")||DropDowns.getText().equals("41 - 45 Yrs")||DropDowns.getText().equals("46 - 50 Yrs")
									||DropDowns.getText().equals("51 - 55 Yrs")||DropDowns.getText().equals("56 - 60 Yrs")||DropDowns.getText().equals("61 - 65 Yrs")
									||DropDowns.getText().equals("66 - 70 Yrs")||DropDowns.getText().equals("71 - 75 Yrs")||DropDowns.getText().equals("76 - 80 Yrs")||DropDowns.getText().equals("Above 80 Yrs")) {
				
								String Membersdetails = TestCaseData[Rownum][MemDetailCol];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else 
								{
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
								
									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown
									
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

									for (WebElement ListData : List) {
										System.out.println("Age Group: "+ ListData.getText());
										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is :" + ListData.getText());
											ImplicitWait(5);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}


		}

	public static void SelectAgeofMemberCareGlobal(String SheetName, String SheetNameInsured, int Rownum, int MembernumColnum,
			int MemDetailCol, int ChildColnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String[][] FamilyData = BaseClass.excel_Files(SheetNameInsured);
			Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
			dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			int membersSize = Integer.parseInt(TestCaseData[Rownum][MembernumColnum].toString().trim());
			int count = 1;
			int mcount;
			int mcountindex = 0;
			int covertype;

			try {
				outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[Rownum][MemDetailCol];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

		driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();

		List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

		for (WebElement ListData : List) 
		{

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Selcted Age Of Member :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else {
						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) 
						{
							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} 
							else if (DropDowns.getText().equals("18 - 24 years")) 
							{
								
					int Children = Integer.parseInt(TestCaseData[Rownum][ChildColnum].toString().trim());
					clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/a"));
					clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
					
								String Membersdetails = TestCaseData[Rownum][MemDetailCol];
								if (Membersdetails.contains(",")) 
								{

									BaseClass.membres = Membersdetails.split(",");
								} else {
									//BaseClass.membres = Membersdetails.split(" ");
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {
									// System.out.println("Mdeatils is :
									// "+membres);

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									
									// List Age of members dropdown
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {
							
										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("List Data is :" + ListData.getText());
									Thread.sleep(2000);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
												}

											}

										}
									}

								}
							}
						}
					} 			
			} catch (Exception e) {
				System.out.println("Unable to Select Total Members.");
				logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
			}

		}

	public static void SelectAgeofMemberCareHNI(String SheetName, String SheetNameInsured, int Rownum, int MembernumColnum,
			int MemDetailCol, int ChildColnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String[][] FamilyData = BaseClass.excel_Files(SheetNameInsured);
		WebElement policy_name = driver.findElement(By.xpath("//*[@id=\"getquote\"]/form/div[2]/div[1]/div/p/span"));
		String name = policy_name.getText().toString().trim();
		System.out.println("Name is:" + name);
		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[Rownum][MembernumColnum].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		System.out.println("dropdown size is " + dropdown.size());
		try{
		outer: for (WebElement DropDownName : dropdown) {
			System.out.println("DropDownName is  " + DropDownName.getText());
			if (DropDownName.getText().equals(TestCaseData[Rownum][MembernumColnum].toString().trim())) {
				System.out.println("DropDownName is  " + DropDownName.getText());
			}

			else if (DropDownName.getText().contains("Individual")) {
				System.out.println("DropDownName is  " + DropDownName.getText());
				DropDownName.click();
			} else if (name.equals("Care For HNI") && !DropDownName.getText().contains("18 to 24 Years")) {
				int Test = Integer.parseInt(DropDownName.getText());

				int Children = Integer.parseInt(TestCaseData[Rownum][ChildColnum].toString().trim());
				DropDownName.click();
				System.out.println("Children Dropdown text is :" + Test);
				
				driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ Children + ")]")).click();

			} else {

				// reading members from carewith TestData sheet
				// memberlist
				String Membersdetails = TestCaseData[Rownum][MemDetailCol];
				if (Membersdetails.contains(",")) {

					// data taking form test case sheet which is
					// 7,4,1,8,2,5
					BaseClass.membres = Membersdetails.split(",");
				} else {
					BaseClass.membres = Membersdetails.split("");
				}

				member:
				// total number of members
				for (int i = 0; i <= BaseClass.membres.length; i++) {
					// System.out.println("Mdeatils is : "+membres);

					// one by one will take from 83 line
					mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
					mcountindex = mcountindex + 1;

					DropDownName.click();
					// List Age of members dropdown

					List<WebElement> List = driver.findElements(By.xpath("//*[@class='dropdown year_drop_slect master open']/ul/li/a"));
					int totalsize = List.size();
					System.out.println("Size is:" + totalsize);
					System.out.println("Family Member details :" + FamilyData[mcount][0].toString().trim());
					for (WebElement age : List) {
						// System.out.println("Age details is :
						// "+age.getText());
						if (age.getText().equals(FamilyData[mcount][0].toString().trim())) {
							age.click();
						}
					}

					if (count == membersSize) {
						break outer;
					} else {
						count = count + 1;
						break member;
					}

				}

			}

		}
	}
catch(Exception e){
		logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
		BaseClass.AbacusURL();
	}

		}

	
	public static void SelectAgeofMemberSenior(String SheetName, String SheetNameInsured, int Rownum, int MembernumColnum,
			int MemDetailCol, int ChildColnum, int CovertypeColnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String[][] FamilyData = BaseClass.excel_Files(SheetNameInsured);
			Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
			dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
			int membersSize = Integer.parseInt(TestCaseData[Rownum][MembernumColnum].toString().trim());
			int count = 1;
			int mcount;
			int mcountindex = 0;
			int covertype;

			try {
				outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[Rownum][MemDetailCol];
						if (Membersdetails.contains("")) 
						{

							BaseClass.membres = Membersdetails.split("");

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

		driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();

		List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

		for (WebElement ListData : List) 
		{

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) 
									{
										System.out.println(FamilyData[mcount][0].toString().trim());
										System.out.println("Selcted Age Of Member :" + ListData.getText());

										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											break member;
										}

									}

								}
							}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[Rownum][CovertypeColnum].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
			driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]")).click();
						ImplicitWait(10);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[Rownum][MembernumColnum].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println(
											"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else if (DropDowns.getText().equals("61 - 65 years")) {
									int Children = Integer.parseInt(TestCaseData[Rownum][ChildColnum].toString().trim());
									clickElement(
											By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
									clickElement(By
											.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
									String Membersdetails = TestCaseData[Rownum][MemDetailCol];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: 
										for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Eldest Member is :" + ListData.getText());
												ImplicitWait(5);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("61 - 65 years")) {

								String Membersdetails = TestCaseData[Rownum][MemDetailCol];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown
									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'years')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is :" + ListData.getText());
											ImplicitWait(5);
											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}

							}
						}
					}
				}
			} catch (Exception e) {
				System.out.println("Unable to Select Total Members.");
				logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
			}

		}
	
	//---------------- Functions for Super MediClaim ---------------------------------------
	
	//Function for Select Total Number of Members in Quotation page
	public static void SelectNoOfMembersInSuperMediClaim(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = TestCaseData[Rownum][Colnum].toString().trim();
		
		WebElement noOfMembers=driver.findElement(By.xpath("//p[text()='Total Members']//following-sibling::div[1]"));
		noOfMembers.click();
		
		List<WebElement> dropdown = driver.findElements(By.xpath("//p[text()='Total Members']//following-sibling::div[1]//ul//li//a"));
		
		try {
			for (WebElement DropDownName : dropdown) {
				if (DropDownName.getText().equals(TotalMember)) 
				{
					DropDownName.click();
					break;
				}
			}
			System.out.println("Total Number of Member Selected : " + TotalMember);
			logger.log(LogStatus.PASS, "Total Number of Member Selected : " + TotalMember);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, "Total Number of Member Not Selected");
		}

	}
	//Function for Select Age group of Member 1 in Quotation page
	public static String setAgeForMember1(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		
		String ageOfMemberValue=TestCaseData[rowNum][colNum].toString();
		
		try{
		
		WebElement ageOfMember=driver.findElement(By.xpath("//p[text()='Age of Member 1']//following::div[contains(@class,'year_drop_slect master')]"));
		ageOfMember.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//p[text()='Age of Member 1']//following::div[contains(@class,'year_drop_slect master')]//ul//li//a"));
		
		for(WebElement option:list){
			
			if(option.getText().equalsIgnoreCase(ageOfMemberValue)){
				option.click();
				break;
			}
		}
		System.out.println("Data Entered for Age Group of Memeber 1: " + ageOfMemberValue);
		logger.log(LogStatus.PASS, "Data Entered for Age Group of Memeber 1: " + ageOfMemberValue);
		
		}
		catch(Exception e){
			System.out.println("Invalid Data entered for Age Group of Memeber 1: "+ ageOfMemberValue);
		}
		
		return ageOfMemberValue;
	}
	//Function for Select Age group of Member 2 in Quotation page
	public static void setAgeForMember2(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		
		String ageOfMemberValue=TestCaseData[rowNum][colNum].toString();
		
		try{
		
		WebElement ageOfMember=driver.findElement(By.xpath("//p[text()='Age of Member 2']//following::div[contains(@class,'year_drop_slect master')]"));
		ageOfMember.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//p[text()='Age of Member 2']//following::div[contains(@class,'year_drop_slect master')]//ul//li//a"));
		
		for(WebElement option:list){
			
			if(option.getText().equalsIgnoreCase(ageOfMemberValue)){
				option.click();
				break;
			}
		}
		System.out.println("Data Entered for Age Group of Memeber 2:" + ageOfMemberValue);
		logger.log(LogStatus.PASS, "Data Entered for Age Group of Memeber 2: " + ageOfMemberValue);
		
		}
		catch(Exception e){
		System.out.println("Invalid Data entered for Age Group of Memeber 2: "+ ageOfMemberValue);
		}
	}
	//Function for Select Age group of Member 3 in Quotation page
	public static void setAgeForMember3(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		
		String ageOfMemberValue=TestCaseData[rowNum][colNum].toString();
		
		try{
		
		WebElement ageOfMember=driver.findElement(By.xpath("//p[text()='Age of Member 3']//following::div[contains(@class,'year_drop_slect master')]"));
		ageOfMember.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//p[text()='Age of Member 3']//following::div[contains(@class,'year_drop_slect master')]//ul//li//a"));
		
		for(WebElement option:list){
			
			if(option.getText().equalsIgnoreCase(ageOfMemberValue)){
				option.click();
				break;
			}
		}
		System.out.println("Data Entered for Age Group of Memeber 3:" + ageOfMemberValue);
		logger.log(LogStatus.PASS, "Data Entered for Age Group of Memeber 3: " + ageOfMemberValue);
		
		}
		catch(Exception e){
			System.out.println("Invalid Data entered for Age Group of Memeber 3: "+ ageOfMemberValue);
		}
	}
	//Function for Select Age group of Member 4 in Quotation page
	public static void setAgeForMember4(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		
		String ageOfMemberValue=TestCaseData[rowNum][colNum].toString();
		
		try{
		
		WebElement ageOfMember=driver.findElement(By.xpath("//p[text()='Age of Member 4']//following::div[contains(@class,'year_drop_slect master')]"));
		ageOfMember.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//p[text()='Age of Member 4']//following::div[contains(@class,'year_drop_slect master')]//ul//li//a"));
		
		for(WebElement option:list){
			
			if(option.getText().equalsIgnoreCase(ageOfMemberValue)){
				option.click();
				break;
			}
		}
		System.out.println("Data Entered for Age Group of Memeber 4:" + ageOfMemberValue);
		logger.log(LogStatus.PASS, "Data Entered for Age Group of Memeber 4: " + ageOfMemberValue);
		
		}
		catch(Exception e){
		System.out.println("Invalid Data entered for Age Group of Memeber 4: "+ ageOfMemberValue);
		}
	}
	//Function for Select Age group of Member 5 in Quotation page
	public static void setAgeForMember5(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		
		String ageOfMemberValue=TestCaseData[rowNum][colNum].toString();
		
		try{
		
		WebElement ageOfMember=driver.findElement(By.xpath("//p[text()='Age of Member 5']//following::div[contains(@class,'year_drop_slect master')]"));
		ageOfMember.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//p[text()='Age of Member 5']//following::div[contains(@class,'year_drop_slect master')]//ul//li//a"));
		
		for(WebElement option:list){
			
			if(option.getText().equalsIgnoreCase(ageOfMemberValue)){
				option.click();
				break;
			}
		}
		System.out.println("Data Entered for Age Group of Memeber 5:" + ageOfMemberValue);
		logger.log(LogStatus.PASS, "Data Entered for Age Group of Memeber 5: " + ageOfMemberValue);
		
		}
		catch(Exception e){
		System.out.println("Invalid Data entered for Age Group of Memeber 5: "+ ageOfMemberValue);
		}
	}
	//Function for Select Age group of Member 6 in Quotation page
	public static void setAgeForMember6(String SheetName, int rowNum, int colNum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		
		String ageOfMemberValue=TestCaseData[rowNum][colNum].toString();
		
		try{
		
		WebElement ageOfMember=driver.findElement(By.xpath("//p[text()='Age of Member 6']//following::div[contains(@class,'year_drop_slect master')]"));
		ageOfMember.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//p[text()='Age of Member 6']//following::div[contains(@class,'year_drop_slect master')]//ul//li//a"));
		
		for(WebElement option:list){
			
			if(option.getText().equalsIgnoreCase(ageOfMemberValue)){
				option.click();
				break;
			}
		}
		System.out.println("Data Entered for Age Group of Memeber 6:" + ageOfMemberValue);
		logger.log(LogStatus.PASS, "Data Entered for Age Group of Memeber 6: " + ageOfMemberValue);
		
		}
		catch(Exception e){
		System.out.println("Invalid Data entered for Age Group of Memeber 6: "+ ageOfMemberValue);
		}
	}
	
	//Main Function for set Details of age group based on Number of Members selection
	public static void setAgeDeatilsforMemebrsInSuperMediclaim(String SheetName, int rowNum, int ColNoForAgeGroupOfMember1, int ColNoForAgeGroupOfMember2, int ColNoForAgeGroupOfMember3, int ColNoForAgeGroupOfMember4, int ColNoForAgeGroupOfMember5,int ColNoForAgeGroupOfMember6) throws Exception{
		
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String TotalMember = TestCaseData[rowNum][5].toString().trim();
		Thread.sleep(5000);
		
		if(TotalMember.equalsIgnoreCase("1")){
			setAgeForMember1(SheetName, rowNum, ColNoForAgeGroupOfMember1);
		}
		else if(TotalMember.equalsIgnoreCase("2")){
			Thread.sleep(2000);
			setAgeForMember1(SheetName, rowNum, ColNoForAgeGroupOfMember1);
			Thread.sleep(2000);
			setAgeForMember2(SheetName, rowNum, ColNoForAgeGroupOfMember2);
		}
		else if(TotalMember.equalsIgnoreCase("3")){
			Thread.sleep(2000);
			setAgeForMember1(SheetName, rowNum, ColNoForAgeGroupOfMember1);
			Thread.sleep(2000);
			setAgeForMember2(SheetName, rowNum, ColNoForAgeGroupOfMember2);
			Thread.sleep(2000);
			setAgeForMember3(SheetName, rowNum, ColNoForAgeGroupOfMember3);
		}
		else if(TotalMember.equalsIgnoreCase("4")){
			Thread.sleep(2000);
			setAgeForMember1(SheetName, rowNum, ColNoForAgeGroupOfMember1);
			Thread.sleep(2000);
			setAgeForMember2(SheetName, rowNum, ColNoForAgeGroupOfMember2);
			setAgeForMember3(SheetName, rowNum, ColNoForAgeGroupOfMember3);
			Thread.sleep(2000);
			setAgeForMember4(SheetName, rowNum, ColNoForAgeGroupOfMember4);
		}
		else if(TotalMember.equalsIgnoreCase("5")){
			Thread.sleep(2000);
			setAgeForMember1(SheetName, rowNum, ColNoForAgeGroupOfMember1);
			Thread.sleep(2000);
			setAgeForMember2(SheetName, rowNum, ColNoForAgeGroupOfMember2);
			setAgeForMember3(SheetName, rowNum, ColNoForAgeGroupOfMember3);
			Thread.sleep(2000);
			setAgeForMember4(SheetName, rowNum, ColNoForAgeGroupOfMember4);
			Thread.sleep(2000);
			setAgeForMember5(SheetName, rowNum, ColNoForAgeGroupOfMember5);
		}
		else if(TotalMember.equalsIgnoreCase("6")){
			Thread.sleep(2000);
			setAgeForMember1(SheetName, rowNum, ColNoForAgeGroupOfMember1);
			Thread.sleep(2000);
			setAgeForMember2(SheetName, rowNum, ColNoForAgeGroupOfMember2);
			setAgeForMember3(SheetName, rowNum, ColNoForAgeGroupOfMember3);
			Thread.sleep(2000);
			setAgeForMember4(SheetName, rowNum, ColNoForAgeGroupOfMember4);
			Thread.sleep(2000);
			setAgeForMember5(SheetName, rowNum, ColNoForAgeGroupOfMember5);
			setAgeForMember6(SheetName, rowNum, ColNoForAgeGroupOfMember6);
		}
		else{
			System.out.println("Number of Memebrs are Entered wrong value");
		}
	}
		
		//Function for Set Details of Tenure in Quotation Page
		public static void setTenureForSuperMedicliamCancer(String SheetName, int rowNum, int colNum) throws Exception{
            String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String tenure=TestCaseData[rowNum][colNum].toString();
			Thread.sleep(2000);
			try{
			switch(tenure){
			case "1 Year":
				break;
			case "2 Year":
				Fluentwait(By.xpath(superMediClaim_tenure2Year_Xpath));
				clickElement(By.xpath(superMediClaim_tenure2Year_Xpath));
				break;
			case "3 Year":
				clickElement(By.xpath(superMediClaim_tenure3Year_Xpath));
				break;
				
			}
			System.out.println("Data entered for Tenure: " + tenure);
			logger.log(LogStatus.PASS, "Data entered for Tenure: " + tenure);
			
			}
			catch(Exception e){
				Assert.fail("Wrong Data entered for Tenure: "+ tenure);
			}
		}	
		
	// Function for click Buy Now Button
	public static void SetBuyNowButton() {
		clickElement(By.xpath(superMediclaim_BuyNowButton_Xpath));
	}
	
}
