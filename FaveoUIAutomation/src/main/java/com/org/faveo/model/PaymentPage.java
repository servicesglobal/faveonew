package com.org.faveo.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class PaymentPage extends BaseClass implements AccountnSettingsInterface{

	//Click on Online Payamnet
	public static void clickOnOnlinePayment(){
		Fluentwait(By.xpath(payOnlineButton_Xpath));
		clickElement(By.xpath(payOnlineButton_Xpath));
		System.out.println("Clicked on Online Payment button");
	}
	
	//Enter Credit Card Number
	public static void setCardNumber(String SheetName, int RowNum, int colNum ) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String carNumber = TestCaseData[RowNum][colNum].toString().trim();
		waitForElement(By.xpath(cardNumber_Xpath));
		enterText(By.xpath(cardNumber_Xpath),carNumber);
		System.out.println("Card Number Entered: "+ carNumber);
	}
	
	//Enter Name on Card
	public static void setNameOnCard(String SheetName, int RowNum, int colNum ) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String nameOnCard = TestCaseData[RowNum][colNum].toString().trim();
		waitForElement(By.xpath(nameOnCard_Xpath));
		enterText(By.xpath(nameOnCard_Xpath),nameOnCard);
		System.out.println("Name on Card Entered: "+ nameOnCard);
	}
	
	//Enter CVV Number
	public static void setCVVNumber(String SheetName, int RowNum, int colNum ) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String cvvNumber = TestCaseData[RowNum][colNum].toString().trim();
		waitForElement(By.xpath(cvvNumber_Xpath));
		enterText(By.xpath(cvvNumber_Xpath),cvvNumber);
		System.out.println("CVV Number Entered: "+ cvvNumber);
	}
	
	//Enter Expiry Month
	public static void setExpirayMonth(String SheetName, int RowNum, int colNum ) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String expiryMonth = TestCaseData[RowNum][colNum].toString().trim();
		waitForElement(By.id(expirayMonth_Xpath));
		selecttext(expirayMonth_Xpath, expiryMonth);
		System.out.println("Card Number Entered: "+ expiryMonth);
	}
	
	//Enter Expiry Year
	public static void setExpirayYear(String SheetName, int RowNum, int colNum ) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String expiryYear = TestCaseData[RowNum][colNum].toString().trim();
		waitForElement(By.name(expirayYear_Xpath));
		selecttext(expirayYear_Xpath, expiryYear);
		System.out.println("Card Number Entered: "+ expiryYear);
	}
	
	//Click on Pay Now Button
	public static void clickOnPayNowButton_CreditCard(){
		Fluentwait(By.xpath(PayNowbutton_CreditCard_Xpath));
		clickElement(By.xpath(PayNowbutton_CreditCard_Xpath));
		System.out.println("Clicked on Pay Button button");
	}
	
		
	//Set Payment Details
	public static void setPaymentDetails(String SheetName,int rowNum) throws Exception{
		
		//Click on Online Payment
		clickOnOnlinePayment();
		
		WebElement managethisCardElement=driver.findElement(By.xpath(manageThisCardString_Xpath));
		String managethisCardText=managethisCardElement.getText();
		
		if(managethisCardText.equalsIgnoreCase("Manage this card")){
			setCVVNumber(SheetName, rowNum, 2);
			scrolldown();
			Thread.sleep(2000);
			clickOnPayNowButton_CreditCard();
		}
		
		else{
		
		setCardNumber(SheetName, rowNum, 0);
		setNameOnCard(SheetName, rowNum, 1);
		setCVVNumber(SheetName, rowNum, 2);
		setExpirayMonth(SheetName, rowNum, 3);
		setExpirayYear(SheetName, rowNum, 4);
		scrolldown();
		Thread.sleep(2000);
		clickOnPayNowButton_CreditCard();
		}
		
		Thread.sleep(5000);
		verifySuccessfulPaymentTranscationPage();
	}
	
	//Set Payment Details
		public static void setPaymentDetails_CareWithNCB(String SheetName,int rowNum) throws Exception{
			
			//Click on Online Payment
			//clickOnOnlinePayment();
			
			WebElement managethisCardElement=driver.findElement(By.xpath(manageThisCardString_Xpath));
			String managethisCardText=managethisCardElement.getText();
			
			if(managethisCardText.equalsIgnoreCase("Manage this card")){
				setCVVNumber(SheetName, rowNum, 2);
				scrolldown();
				Thread.sleep(2000);
				clickOnPayNowButton_CreditCard();
			}
			
			else{
			
			setCardNumber(SheetName, rowNum, 0);
			setNameOnCard(SheetName, rowNum, 1);
			setCVVNumber(SheetName, rowNum, 2);
			setExpirayMonth(SheetName, rowNum, 3);
			setExpirayYear(SheetName, rowNum, 4);
			scrolldown();
			Thread.sleep(2000);
			clickOnPayNowButton_CreditCard();
			}
			
			Thread.sleep(5000);
			verifySuccessfulPaymentTranscationPage();
		}
	
	//Print Policy number after successful Payment Transcation Page
	public static void verifySuccessfulPaymentTranscationPage() throws Exception{
		Thread.sleep(5000);
		waitForElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
		WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
		String policy_Number=Policy.getText();
		System.out.println("Policy number is  :"   +policy_Number);
		logger.log(LogStatus.PASS, "Policy number is  :"   +policy_Number);
		
		/*clickElement(By.xpath("//a[@ng-click='goToDashboard();']"));
		System.out.println("Moved to Dashboard");
		logger.log(LogStatus.PASS, "Moved to Dashboard");*/
		
		
		/*WebElement tranctNumElement=driver.findElement(By.xpath("//div[contains(@class,'your_transaction')][1]/p[contains(@class,'Transaction_number')]"));
	    String trancaionNumber=tranctNumElement.getText();*/
	    
	    
	}
}
