package com.org.faveo.PolicyJourney;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.org.faveo.Assertions.DataVerificationinDb;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestions;
import com.org.faveo.model.InsuredDetails;
import com.org.faveo.model.PayUPaymentFn;
import com.org.faveo.model.ProposerDetailsPageFn;
import com.org.faveo.model.QuotationPage;
import com.org.faveo.utility.WriteExcel;

public class CareGlobalPageJourney extends BaseClass implements AccountnSettingsInterface 

{

	public static String ProposalPremimPage_Value=null;
	public static String proposalSummarypremium_value=null;
	
	@Test(priority=1)
	public static void QuotationPageJourney(int Rownum) throws Exception
	{
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("CareGlobal_Quotation", Rownum, 2);
					

		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("CareGlobal_Quotation", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("CareGlobal_Quotation", Rownum, 4);
					
		// Enter The Value of Total members present in policy
		QuotationPage.SelectMembers("CareGlobal_Quotation", Rownum, 5);
					
		// again call the dropdown
		QuotationPage.SelectAgeofMemberCareGlobal("CareGlobal_Quotation", "CareGlobal_Insured_Details", Rownum, 5, 6,7);
		
		// Read the Value of Suminsured
		QuotationPage.SumInsured("CareGlobal_Quotation", Rownum, 8);
		
		// Reading the value of Tenure from Excel
		QuotationPage.Tenure("CareGlobal_Quotation", Rownum, 9);
		
		// Scroll Window Up
		BaseClass.scrollup();

		// NCB Super Addon Selection
		AddonsforProducts.CareGlobal("CareGlobal_Quotation", Rownum, 10);
		
		//Assertion 
		QuotationandProposalVerification.PremiumAssertion();

		
	}
	
	@Test(priority=2,dependsOnMethods = { "QuotationPageJourney" })
	public static void ProposalPageJourney(int Rownum) throws Exception
	{
				// Reading Proposer Title from Excel
				ProposerDetailsPageFn.ProposerTitle("CareGlobal_Quotation", Rownum, 1);

				// Entering DOB from Excel into dob field
				ProposerDetailsPageFn.ProposerDOB("CareGlobal_Quotation", Rownum, 11);
				
				
				// Reading AddressLine 1 from Excel
				ProposerDetailsPageFn.ProposerAddressLine1("CareGlobal_Quotation", Rownum, 12);
				ProposerDetailsPageFn.ProposerAddressLine2("CareGlobal_Quotation", Rownum, 13);
				
				
				// Reading Pincode from Excel
				ProposerDetailsPageFn.Pincode("CareGlobal_Quotation", Rownum, 14);
				
				// Reading Proposer Height in Feet from Excel
				ProposerDetailsPageFn.ProposerHeightFeet("CareGlobal_Quotation", Rownum, 15);
				
				// Reading Proposer Height in Inch from Excel
				ProposerDetailsPageFn.ProposerHeightInch("CareGlobal_Quotation", Rownum, 16);
				
				// Reading Weight of Proposer from Excel
				ProposerDetailsPageFn.ProposerWeight("CareGlobal_Quotation", Rownum, 17);
				
				// Reading Nominee Name from Excel
				ProposerDetailsPageFn.ProposerNominee("CareGlobal_Quotation", Rownum, 18);
				

				// Nominee Relation
				ProposerDetailsPageFn.ProposerNomineeRelation("CareGlobal_Quotation", Rownum, 19);

							
				// Pan Card
				ProposerDetailsPageFn.PanCardNumber("CareGlobal_Quotation", Rownum, 20);
			
				// Click on Next button
				clickElement(By.id(Button_NextProposer_id));
				System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				
				InsuredDetails.InsuredDetail("CareGlobal_Insured_Details");
							
				//Health Questionnarire Elements 
				HealthQuestions.CareGlobalHealthQuestions("CareGlobal_Quotation", "CareGlobal_QuestionSet", Rownum, 21);
			
				BaseClass.ErroronHelathquestionnaire();
		
	}

	@Test(priority=3,dependsOnMethods = { "ProposalPageJourney" })
	public static void ProposalSummaryPage(String TestCasenameSheet,int Rownum) throws Exception
	{
		QuotationandProposalVerification.VerifyPremiumIncrease_on_Proposalsummarypage();
		
		try
		{
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);'][contains(text(),'PAY ONLINE')]"))).click();
		}
		catch(Exception e)
		{
		clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);'][contains(text(),'PAY ONLINE')]"));	
		}
		
		waitForElement(By.xpath(payu_proposalnum_xpath));
		String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number : "+PayuProposalNum);
		
		waitForElement(By.xpath(payuAmount_xpath));
		String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
		String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
		System.out.println("Pay U Premium Amount : "+FinalAmount);
		
			PayUPaymentFn.PayuPage_Credentials();
			
			BaseClass.scrolldown();
			
			QuotationandProposalVerification.ThankyouPagemessageVerification();
			
			String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
			System.out.println(ProposerName);
			
			DataVerificationinDb.DBVerification(PayuProposalNum);
			waitForElement(By.xpath(PolProp_xpath));
			
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			
			PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);

			//WriteExcel.setCellData1(TestCasenameSheet, TestResult, ThankyoupageProposal_Pol_num, Rownum, 2, 3);
			WriteExcel.setCellData(TestCasenameSheet, ThankyoupageProposal_Pol_num, Rownum, 2);

			
			
	}
}
