package com.org.faveo.PolicyJourney;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.faveo.edit.Edit;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.healthinsurance.POSCareSmartSelect;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class POSCareSmartSelectJourney extends BaseClass implements AccountnSettingsInterface {
public static String ProposalPremimPage_Value;
public static String proposalSummarypremium_value=null;
public static POSCareSmartSelect pcss=new POSCareSmartSelect();
	
	public static void possmartselectQuotationpage() throws Exception {
	String[][] TestCase=BaseClass.excel_Files("POSSmartSelect_TestCase");
	String[][] TestCaseData=BaseClass.excel_Files("POSSmartSelect_Quotation");

	Thread.sleep(5000);
	
/*	String TestcaseName=(TestCase[ps.n][0].toString().trim() + " - " + TestCase[ps.n][1].toString().trim());
	logger = extent.startTest("CareWithNCB - " + TestcaseName);*/
	//System.out.println("Heart tets case name is  - " + TestcaseName);
	
	driver.findElement(By.name("name")).clear();
	driver.findElement(By.name("name")).sendKeys(TestCaseData[pcss.n][2].toString().trim() + "  " + TestCaseData[pcss.n][3].toString().trim());
	System.out.println("Entered Name is  " + TestCaseData[pcss.n][2].toString().trim() + "  " + TestCaseData[pcss.n][3].toString().trim());
	//logger.log(LogStatus.PASS,"Entered Name is  " + TestCaseData[pcss.n][2].toString().trim() + "  " + TestCaseData[pcss.n][3].toString().trim());
	logger.log(LogStatus.PASS,"Entered Name is  " + TestCaseData[pcss.n][2].toString().trim() + "  " + TestCaseData[pcss.n][3].toString().trim());

	driver.findElement(By.name("ValidEmail")).clear();

	String email = TestCaseData[pcss.n][4].toString().trim();
	System.out.println("Email is :" + email);
	if (email.contains("@")) {
		driver.findElement(By.name("ValidEmail")).sendKeys(email);
	} else {
		System.out.println("Not a valid email");
	}
	logger.log(LogStatus.PASS,"Entered Email id is  "+email );
	Thread.sleep(5000);
	String mnumber = TestCaseData[pcss.n][5].toString().trim();
	int size = mnumber.length();
	logger.log(LogStatus.PASS,"Entered Mobile number is  "+mnumber );

	System.out.println("mobile number is: " + mnumber);
	String format = "^[789]\\d{9}$";
System.out.println("Entered mobile number is : "+mnumber);
	if (mnumber.matches(format) && size == 10) {
		driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
	} else {
		System.out.println(" Not a valid mobile  Number");
	}
	
	}
public static void POScareSmartSelectDropdownselect() throws Exception {



	String[][] TestCase=BaseClass.excel_Files("POSSmartSelect_TestCase");
	String[][] TestCaseData=BaseClass.excel_Files("POSSmartSelect_Quotation");

	String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
	int covertype;
	int membersSize = Integer.parseInt(TestCaseData[pcss.n][6].toString().trim());
	List<WebElement>OPDDrop=driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

	try {
		for (WebElement DropDownName :OPDDrop) {
			DropDownName.click();
			System.out.println(DropDownName.getText());
			if (DropDownName.getText().equals("2")) {
				
				//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'1')]
				
				driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+"'"+membersSize+"'"+")]")).click();
				System.out.println("Total Number of Member Selected : " + TestCaseData[pcss.n][6].toString().trim());
				Thread.sleep(5000);
				break;
			}
		}
	} catch (Exception e) {
		logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
		BaseClass.AbacusURL();
	}
	
	
	
	Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
	List<WebElement> dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
	int count = 1;
	int mcount;
	int mcountindex = 0;

	System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3

	outer:

	for (WebElement DropDownName : dropdown) {
		if (membersSize == 1) {

			// reading members from test cases sheet memberlist
			String Membersdetails = TestCaseData[pcss.n][7];
			System.out.println("Total Number of Member in Excel :" + Membersdetails);
			if (Membersdetails.contains("")) {

		
				BaseClass.membres = Membersdetails.split("");

				member:
				// total number of members
				for (int i = 0; i <= BaseClass.membres.length; i++) {
					// System.out.println("Mdeatils is :
					// "+membres);

					// one by one will take from 83 line
					mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
					mcountindex = mcountindex + 1;
					System.out.println("Mcount Index : " + mcountindex);

					driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();
					// List Age of members dropdown
					List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
					
					for (WebElement ListData : List) {

						if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
							System.out.println("List Data is :" + ListData.getText());

							ListData.click();

							if (count == membersSize) {
								break outer;
							} else {
								count = count + 1;
								break member;
							}

						}

					}
				}
			}

		} else if (DropDownName.getText().contains("Individual")) {
			System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

			if (TestCaseData[pcss.n][8].toString().trim().equals("Individual")) {
				covertype = 1;
			} else {
				covertype = 2;
			}
			DropDownName.click();
			driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["+ covertype + "]")).click();
			// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
			Thread.sleep(2000);
			if (covertype == 2) 
			{
				List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
				for (WebElement DropDowns : dropdowns) 
				{
					if (DropDowns.getText().contains("Floater")) 
					{
						System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
					} else if (DropDowns.getText().equals(TestCaseData[pcss.n][6].toString().trim())) {
						System.out.println("DropDownName is  " + DropDowns.getText());
					} else if (DropDowns.getText().equals("2")) {
						System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
					} else if (DropDowns.getText().equals("18 - 24 years")) {
						// reading members from test cases sheet
						// memberlist
			int Children = Integer.parseInt(TestCaseData[pcss.n][9].toString().trim());
			clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/a"));
			clickElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
			System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
						String Membersdetails = TestCaseData[pcss.n][7];
						if (Membersdetails.contains(",")) {

							BaseClass.membres = Membersdetails.split(",");
						} else {
							//BaseClass.membres = Membersdetails.split(" ");
							System.out.println("Hello");
						}

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {
							// System.out.println("Mdeatils is :
							// "+membres);

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							DropDowns.click();
							// List Age of members dropdown
							
							List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (WebElement ListData : List) {
								
								if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
									Thread.sleep(2000);
									ListData.click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
						
					}
				}
			}
		} 
		else{

			List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
			for (WebElement DropDowns : dropdowns) 
			{
				if (DropDowns.getText().contains("Floater")) 
				{
					System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
				} else if (DropDowns.getText().equals("5 - 24 years")||DropDowns.getText().equals("25 - 35 years")
						||DropDowns.getText().equals("18 - 24 years")
						||DropDowns.getText().equals("36 - 40 years")||DropDowns.getText().equals("41 - 45 years")
						||DropDowns.getText().equals("46 - 50 years")||DropDowns.getText().equals("51 - 55 years")
						||DropDowns.getText().equals("56 - 60 years")||DropDowns.getText().equals("61 - 55 years")
						||DropDowns.getText().equals("66 - 70 years")||DropDowns.getText().equals("71 - 75 years")
						||DropDowns.getText().equals("> 75 years")) {
	
					String Membersdetails = TestCaseData[pcss.n][7];
					if (Membersdetails.contains(",")) {
						BaseClass.membres = Membersdetails.split(",");
					} else 
					{
						System.out.println("Hello");
					}

					member:
					// total number of members
					for (int i = 0; i <= BaseClass.membres.length; i++) {
					
						// one by one will take from 83 line
						mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
						mcountindex = mcountindex + 1;

						DropDowns.click();
						// List Age of members dropdown
						
						List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

						for (WebElement ListData : List) {
							
							if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
								Thread.sleep(2000);
								ListData.click();

								if (count == membersSize) {
									break outer;
								} else {
									count = count + 1;
									// break member;
									break member;
								}

							}

						}
					}
					
				}
			}
		}
	}

   }
public static void possmartselectsuminsuredtenure() throws Exception {


	//String[][] TestCase=BaseClass.excel_Files("POSSmartSelect_TestCase");
	String[][] TestCaseData=BaseClass.excel_Files("POSSmartSelect_Quotation");
	
	int SumInsured = Integer.parseInt(TestCaseData[pcss.n][10].toString().trim());
	System.out.println("Suminsured is  : "+SumInsured);
	int Tenure = Integer.parseInt(TestCaseData[pcss.n][11].toString().trim());
	try{
    clickElement(By.xpath(slider_xpath));
	clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), "+SumInsured+")]"));
		
	System.out.println("Entered Sum Insured is : "+SumInsured+" Lakhs");	
	logger.log(LogStatus.PASS, "Data entered for Sum Insured: " + SumInsured + "Lakhs");
	}
	catch(Exception e)
	{
		System.out.println(e);
		logger.log(LogStatus.FAIL, e);
	}
		
	int Tenure1=Tenure-1;
	System.out.println("Selected Tenure is : "+Tenure+" Year");
	scrolldown();
	Thread.sleep(2000);
	clickElement(By.xpath("//label[@for='Radio"+Tenure1+"q']//img[@src='assets/img/correct_signal.png']"));
	BaseClass.scrollup();



     }
public static void possmartselectAddons() throws Exception {
	String[][] TestCaseData=BaseClass.excel_Files("POSSmartSelect_Quotation");
	// NCB Super Addon Selection
	String NCBSuper = TestCaseData[pcss.n][28].toString().trim();
	Thread.sleep(4000);
	waitForElement(By.xpath(Checkbox_carewithNCBSuper_xpath));
	if (NCBSuper.contains("for care with NCB Super")) {
		clickbyHover(By.xpath(Checkbox_carewithNCBSuper_xpath));
		// clickElement(By.xpath(Checkbox_CareWithNCB_xpath));
		System.out.println("Selected Addon is : " + NCBSuper);
	} else if (NCBSuper.contains("for care")) {
		//clickbyHover(By.xpath(checkbox_forcare_xpath));
		 clickElement(By.xpath(checkbox_forcare_xpath));
	}

	// UAR Addon Selection
	String UAR = TestCaseData[pcss.n][29].toString().trim();
	waitForElement(By.xpath(Checkbox_UAR_xpath));
	if (UAR.contains("Unlimited Recharge")) {
		//clickbyHover(By.xpath(Checkbox_UAR_xpath));
		 clickElement(By.xpath(Checkbox_UAR_xpath));
		System.out.println("Selected Addon is : " + UAR);
	} else if (UAR.contains("No") || UAR.contains("NO") || UAR.contains("no")) {
		System.out.println("UAR Addon is not Selected.");
	}

	// Everyday Care Addon Selection
	String EverydayCare = TestCaseData[pcss.n][30].toString().trim();
	waitForElement(By.xpath(Checkbox_EverydayCare_xpath));
	if (EverydayCare.contains("Everyday Care")) {
		clickbyHover(By.xpath(Checkbox_EverydayCare_xpath));
		// clickElement(By.xpath(Checkbox_EverydayCare_xpath));
		System.out.println("Selected Addon is : " + EverydayCare);
	} else if (EverydayCare.contains("No") || EverydayCare.contains("NO") || EverydayCare.contains("no")) {
		System.out.println("EverydayCare Addon is not Selected.");
	}
}
public static void poscaresmartselectpremiumwithoutEdit() {

	try {
	
	WebElement TotalMember=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a"));
	String TotalMemberPresentonQuotation=TotalMember.getText();
	System.out.println("Total Member in quotation page is : "+TotalMemberPresentonQuotation);
	scrolldown();
	Thread.sleep(5000);
	WebElement quotepremi=driver.findElement(By.xpath("//p[@class='get_quot_total_premium']/span[1]"));
	Quotationpremium_value=quotepremi.getText();
	System.out.println("Quotation premium for pos care smart select is :"+Quotationpremium_value);
	clickElement(By.xpath(Button_Buynow_xpath));
	
	// Premium verification on Proposal Page
	/*Fluentwait(By.xpath("//p[@class='amount ng-binding']"), 60, "Unable to read Premium on Proposal Page.");*/
	Thread.sleep(10000);
	scrolldown();
	String Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
	System.out.println("proposal premium is :"+Proposalpremium_value);
	ProposalPremimPage_Value = Proposalpremium_value.substring(1, Proposalpremium_value.length());
	System.out.println("Total Premium Value on Proposal Page is : " + Proposalpremium_value.substring(1, Proposalpremium_value.length()));
scrollup();
	try {
		Assert.assertEquals(Quotationpremium_value, Proposalpremium_value);
		logger.log(LogStatus.INFO,"Quotation Premium and Proposal Premium is Verified and Both are Same : "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
	} catch (AssertionError e) {
		System.out.println(Quotationpremium_value + " - failed");
		logger.log(LogStatus.FAIL, "Quotation Premium and Proposal Premium are not Same");
		
	}

	
	// Total Number of Member on Quotation and Proposal Page
	String TotalMemberProposal = driver.findElement(By.xpath(TotalMemberProposal_xpath)).getText();
	System.out.println("Total Members on Proposal Page : " + TotalMemberProposal);
	try {
		Assert.assertEquals(TotalMemberPresentonQuotation, TotalMemberProposal);
		logger.log(LogStatus.INFO,
				"Number Of Members Verified on Quotation and ProposalPage Both are Same : "
						+ TotalMemberProposal);
	} catch (AssertionError e) {
		logger.log(LogStatus.INFO, "Number Of Members are diffrent on Quotation and ProposalPage");
	}

	
	
}catch(Exception e)
	{
	System.out.println(e);
	}

}
public static void possmartselectproposerdetails() throws Exception {


	String[][] TestCaseData=BaseClass.excel_Files("CareWithOPD_Quotation");
	scrollup();
	String Title = TestCaseData[pcss.n][1].toString().trim();
	System.out.println("Titel Name is:" + Title);
	Thread.sleep(8000);
	clickElement(By.name("ValidTitle"));
	logger.log(LogStatus.PASS, "Selected Title  is :" + Title);

	BaseClass.selecttext("ValidTitle", Title.toString());

	// Entering DOB from Excel into dob field
	driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
	String DOB = TestCaseData[pcss.n][13].toString().trim();
	System.out.println("date is:" + DOB);
	enterText(By.id("proposer_dob"), String.valueOf(DOB));
	//logger.log(LogStatus.PASS, "Entered DOB is :" + DOB);

	Thread.sleep(3000);
	String address1 = TestCaseData[pcss.n][14].toString().trim();
	String address2 = TestCaseData[pcss.n][15].toString().trim();

	driver.findElement(By.xpath(addressline1_xpath)).sendKeys(address1);
	driver.findElement(By.xpath(addressline2_xpath)).sendKeys(address2);
	driver.findElement(By.xpath(pincode_xpath)).sendKeys(TestCaseData[pcss.n][16]);

	// Height selection
	String Height = TestCaseData[pcss.n][17].toString().trim();
	System.out.println("Height value from excel  is:" + Height);
	clickElement(By.xpath(height_xpath));
	BaseClass.selecttext("heightFeet", Height.toString().trim());
	// Inch Selection
	String Inch = TestCaseData[pcss.n][18].toString().trim();
	System.out.println("Inch value from excel  is:" + Inch);
	clickElement(By.xpath(inch_xpath));
	BaseClass.selecttext("heightInches", Inch.toString().trim());
	String Weight = TestCaseData[pcss.n][19].toString().trim();
	System.out.println("Weight is :" + Weight);
	enterText(By.xpath(weight_xpath), Weight);
	logger.log(LogStatus.PASS, "Entered Height,Inch and Weight is :" + Height + Inch + Weight);

	String NomineeName = TestCaseData[pcss.n][20].toString().trim();
	System.out.println("Nominee name   is:" + NomineeName);
	enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
	logger.log(LogStatus.PASS, "Entered nominee name is :" + NomineeName);

	// Nominee Relation
	String Nrelation = TestCaseData[pcss.n][21].toString().trim();
	System.out.println("Nominee  relation from excel  is:" + Nrelation);
	clickElement(By.xpath(Nominee_relation_xpath));
	BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
	logger.log(LogStatus.PASS, "Entered nominee relation is :" + Nrelation);

	String pancard = TestCaseData[pcss.n][22].toString().trim();

	String pospancard = "KJHYS8977E";
	System.out.println("pancard number is :" + pospancard);
	try {
		driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).sendKeys(pospancard);
	} catch (Exception e) {
		System.out.println("Pan card field not visibled");
	}
	logger.log(LogStatus.PASS, "Entered Pancard number  is :" + pospancard);
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
	Thread.sleep(7000);
	clickElement(By.xpath(submit_xpath));

}
public static void poscaresmartselectinsureddetails() throws Exception {

	String[][] TestCase=BaseClass.excel_Files("POSSmartSelect_TestCase");
	String[][] TestCaseData=BaseClass.excel_Files("POSSmartSelect_Quotation");

	String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
	Thread.sleep(3000);
	scrollup();
int mcount;
	for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
		mcount = Integer.parseInt(BaseClass.membres[i].toString());
		if (i == 0) {
			clickElement(By.xpath(title1_xpath));
			// Select Self Primary
			BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
		} else {

			// String firstName= "fname"+i+"_xpath";
			String Date = FamilyData[mcount][5].toString().trim();

			BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
			// title
			BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
			enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
			enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
			clickElement(By.name("rel_dob" + i));
			enterText(By.name("rel_dob" + i), String.valueOf(Date));

			BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
			BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
			enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

		}
	}

	driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
	System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");

}
public static void poscaresmartselectQuestionset() throws Exception {

	String[][] TestCase=BaseClass.excel_Files("POSSmartSelect_TestCase");
	String[][] TestCaseData=BaseClass.excel_Files("POSSmartSelect_Quotation");

	String[][] FamilyData = BaseClass.excel_Files("POSSmartSelect_FamilyData");
	String[][] QuestionSetData=BaseClass.excel_Files("POScaresmartselectQuestionset");
	
	String preExistingdeases = TestCaseData[pcss.n][23].toString().trim();
	Thread.sleep(3000);
	System.out.println(
			"Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
	// BaseClass.scrollup();
	try {
		if (preExistingdeases.contains("YES")) {

			waitForElements(By.xpath(YesButton_xpath));
			clickElement(By.xpath(YesButton_xpath));
			// Thread.sleep(2000);
			String years = null;
			String Details = null;
			for (int qlist = 1; qlist <= 13; qlist++) {
				Details = QuestionSetData[pcss.n][qlist + (qlist - 1)].toString().trim();
				years = QuestionSetData[pcss.n][qlist + qlist].toString().trim();
				if (Details.equals("")) {
					// break;
				} else {
					int detailsnumber = Integer.parseInt(Details);

					// Will click on check box and select the month & year u
					detailsnumber = detailsnumber + 1;
					System.out.println("Details and years are :" + Details + "----" + years);
					clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
							+ detailsnumber + "]//input[@type='checkbox']"));
					Thread.sleep(1000);
					try {
						clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
								+ detailsnumber + "]//label"));
						enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
								+ detailsnumber + "]//label"), years);
					} catch (Exception e) {
						clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
								+ detailsnumber + "]//label[@class='monthYear']"));
						enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
								+ detailsnumber + "]//label[@class='monthYear']"), years);
					}
				}
			}
		} else if (preExistingdeases.contains("NO")) {
			clickElement(By.xpath(NoButton_xpath));
		}
	} catch (Exception e) {
		//logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
	}

	// BaseClass.scrolldown();
	String[] ChckData = null;
	int datacheck = 0;
	for (int morechecks = 1; morechecks <= 3; morechecks++) {
		int mch = morechecks + 1;
		String ChecksData = TestCaseData[pcss.n][23 + morechecks].toString().trim();

		try {
			if (ChecksData.contains("NO")) {
				System.out.println("Quatation set to NO");

				clickElement(By.xpath("//label[@for='question_" + mch + "_no']"));
			} else {
				driver.findElement(By.xpath("//label[@for='question_" + mch + "_yes']")).click();
				if (ChecksData.contains(",")) {
					ChckData = ChecksData.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
								.click();
					}
				} else if (ChecksData.contains("")) {
					datacheck = Integer.parseInt(ChecksData);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_H00" + mch + "_" + datacheck + "']"))
							.click();
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
		}

	}

	// Check Box on Health Questionnaire
	BaseClass.scrolldown();
	BaseClass.HelathQuestionnairecheckbox();

	try {
		clickElement(By.xpath(proceed_to_pay_xpath));
		System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
	} catch (Exception e) {
		logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
		System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
	}



}
public static void poscaresmartselectproposalsummaryandPayment() throws Exception {

	String[][] TestCase=BaseClass.excel_Files("POSSmartSelect_TestCase");
	
	Thread.sleep(12000);
	String ExecutionStatus=TestCase[pcss.n][4].toString().trim();
	if(ExecutionStatus.equalsIgnoreCase("Normal")) {
	QuotationandProposalVerification.VerifyPremiumIncrease_on_Proposalsummarypagenoeditpossmartselect();
	}else {
		QuotationandProposalVerification.VerifyPremiumIncrease_on_posProposalsummarypagenoedit();
	}
	
	(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
	
	waitForElement(By.xpath(payu_proposalnum_xpath));
	String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
	System.out.println("Pay U Proposal Number : "+PayuProposalNum);
	
	waitForElement(By.xpath(payuAmount_xpath));
	String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
	String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
	System.out.println("Pay U Premium Amount : "+FinalAmount);
	//PayUPaymentFn.PayuPage_Credentials();
	BaseClass.PayuPage_Credentials();

	BaseClass.scrolldown();
	
	QuotationandProposalVerification.ThankyouPagemessageVerification();
	
	String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/p")).getText();
	System.out.println(ProposerName);
	
	//DataVerificationinDb.DBVerification(PayuProposalNum);
	waitForElement(By.xpath(PolProp2_xpath));
	
	String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
	System.out.println("Proposal summary is :"+ProposalSummary);
	String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp2_xpath)).getText();
	System.out.println("Proposal number is : "+ThankyoupageProposal_Pol_num);
	if(ExecutionStatus.equalsIgnoreCase("Normal")) {
	PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);
	}else {
		PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, Edit.afterEdit_Proposalpremium_value);
	}
	WriteExcel.setCellData1("CareWithOPD_Testcase", TestResult, ThankyoupageProposal_Pol_num, pcss.n,2, 3);
	



}
}
