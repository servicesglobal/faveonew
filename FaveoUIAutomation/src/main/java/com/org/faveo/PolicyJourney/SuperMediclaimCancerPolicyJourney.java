package com.org.faveo.PolicyJourney;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.org.faveo.Assertions.DataVerificationinDb;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.InsuredDetails;
import com.org.faveo.model.PayUPaymentFn;
import com.org.faveo.model.PaymentPage;
import com.org.faveo.model.ProposerDetailsPageFn;
import com.org.faveo.model.QuotationPage;
import com.org.faveo.utility.WriteExcel;

import junit.framework.Assert;

public class SuperMediclaimCancerPolicyJourney extends BaseClass implements AccountnSettingsInterface {
	
	public static String ProposalPremimPage_Value=null;
	public static String proposalSummarypremium_value=null;

	public static void QuotationPageJourney(int Rownum) throws Exception
	{
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("SuperMediclaimCancer_TestData", Rownum, 2);
					

		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("SuperMediclaimCancer_TestData", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("SuperMediclaimCancer_TestData", Rownum, 4);
		
		// Enter The Value of Total members present in policy
		QuotationPage.SelectNoOfMembersInSuperMediClaim("SuperMediclaimCancer_TestData", Rownum, 5);
		
		// Select Age of Member 1 from Excel
		QuotationPage.setAgeDeatilsforMemebrsInSuperMediclaim("SuperMediclaimCancer_TestData", Rownum,6,7,8,9,10,11);
		
		// Read the Value of Suminsured
		QuotationPage.SumInsured("SuperMediclaimCancer_TestData", Rownum, 13);
		
		// Reading the value of Tenure from Excel
		QuotationPage.setTenureForSuperMedicliamCancer("SuperMediclaimCancer_TestData", Rownum, 14);
		
		// Scroll Window Up
		BaseClass.scrollup();

		// NCB Super Addon Selection
		AddonsforProducts.setPaymentFrequency("SuperMediclaimCancer_TestData", Rownum, 15);
		ReadDataFromEmail.readAgeGroup();
		
		QuotationPage.SetBuyNowButton();
			
	}
	
	
	public static void ProposalPageJourney(int Rownum) throws Exception {
		Thread.sleep(5000);
		//verifyPremiumAmountOnQuotationPage(); //Verify Premium Amount
		
		// Reading Proposer Title from Excel
		ProposerDetailsPageFn.ProposerTitle("SuperMediclaimCancer_TestData", Rownum, 16);

		// Entering DOB from Excel into dob field
		ProposerDetailsPageFn.ProposerDOB("SuperMediclaimCancer_TestData", Rownum, 17);

		// Reading AddressLine 1 from Excel
		ProposerDetailsPageFn.ProposerAddressLine1("SuperMediclaimCancer_TestData", Rownum, 18);
		ProposerDetailsPageFn.ProposerAddressLine2("SuperMediclaimCancer_TestData", Rownum, 19);

		// Reading Pincode from Excel
		ProposerDetailsPageFn.Pincode("SuperMediclaimCancer_TestData", Rownum, 20);

		// Reading Proposer Height in Feet from Excel
		ProposerDetailsPageFn.ProposerHeightFeet("SuperMediclaimCancer_TestData", Rownum, 21);

		// Reading Proposer Height in Inch from Excel
		ProposerDetailsPageFn.ProposerHeightInch("SuperMediclaimCancer_TestData", Rownum, 22);

		// Reading Weight of Proposer from Excel
		ProposerDetailsPageFn.ProposerWeight("SuperMediclaimCancer_TestData", Rownum, 23);

		// Reading Nominee Name from Excel
		ProposerDetailsPageFn.ProposerNominee("SuperMediclaimCancer_TestData", Rownum, 24);

		// Nominee Relation
		ProposerDetailsPageFn.ProposerNomineeRelation("SuperMediclaimCancer_TestData", Rownum, 25);
		
		ProposerDetailsPageFn.PanCardNumber("SuperMediclaimCancer_TestData", Rownum, 28);
		
		// Click on Next button
		clickElement(By.id(Button_NextProposer_id));
		System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");

	}
	
	//Function for set the Insured details 1 
	public static void setDataOfInsuredForMember1(int RowNum) throws Exception{
		InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
		InsuredDetails.Insured1_FirstName("SuperMediclaimCancerInsuredDeta", RowNum, 9);
		InsuredDetails.Insured1_LastName("SuperMediclaimCancerInsuredDeta", RowNum, 10);
		InsuredDetails.Insured1_DOB("SuperMediclaimCancerInsuredDeta", RowNum);
		InsuredDetails.Insured1_height("SuperMediclaimCancerInsuredDeta", RowNum, 11);
		InsuredDetails.Insured1_inch("SuperMediclaimCancerInsuredDeta", RowNum, 12);
		InsuredDetails.Insured1_weight("SuperMediclaimCancerInsuredDeta", RowNum, 13);
	}
	
	public static void setDataOfInsuredForMember2(int RowNum) throws Exception{
		InsuredDetails.Insured2_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 14);
		InsuredDetails.Insured2_FirstName("SuperMediclaimCancerInsuredDeta", RowNum, 15);
		InsuredDetails.Insured2_LastName("SuperMediclaimCancerInsuredDeta", RowNum, 16);
		InsuredDetails.Insured2_DOB("SuperMediclaimCancerInsuredDeta", RowNum);
		InsuredDetails.Insured2_height("SuperMediclaimCancerInsuredDeta", RowNum, 17);
		InsuredDetails.Insured2_inch("SuperMediclaimCancerInsuredDeta", RowNum, 18);
		InsuredDetails.Insured2_weight("SuperMediclaimCancerInsuredDeta", RowNum, 19);
		
	}
	
	public static void setDataOfInsuredForMember3(int RowNum) throws Exception{
		InsuredDetails.Insured3_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 20);
		InsuredDetails.Insured3_FirstName("SuperMediclaimCancerInsuredDeta", RowNum, 21);
		InsuredDetails.Insured3_LastName("SuperMediclaimCancerInsuredDeta", RowNum, 22);
		InsuredDetails.Insured3_DOB("SuperMediclaimCancerInsuredDeta", RowNum);
		InsuredDetails.Insured3_height("SuperMediclaimCancerInsuredDeta", RowNum, 23);
		InsuredDetails.Insured3_inch("SuperMediclaimCancerInsuredDeta", RowNum, 24);
		InsuredDetails.Insured3_weight("SuperMediclaimCancerInsuredDeta", RowNum, 25);
		
	}
	
	public static void setDataOfInsuredForMember4(int RowNum) throws Exception{
		InsuredDetails.Insured4_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 26);
		InsuredDetails.Insured4_FirstName("SuperMediclaimCancerInsuredDeta", RowNum, 27);
		InsuredDetails.Insured4_LastName("SuperMediclaimCancerInsuredDeta", RowNum, 28);
		InsuredDetails.Insured4_DOB("SuperMediclaimCancerInsuredDeta", RowNum);
		InsuredDetails.Insured4_height("SuperMediclaimCancerInsuredDeta", RowNum, 29);
		InsuredDetails.Insured4_inch("SuperMediclaimCancerInsuredDeta", RowNum, 30);
		InsuredDetails.Insured4_weight("SuperMediclaimCancerInsuredDeta", RowNum, 31);
		
	}
	
	public static void setDataOfInsuredForMember5(int RowNum) throws Exception{
		InsuredDetails.Insured5_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 32);
		InsuredDetails.Insured5_FirstName("SuperMediclaimCancerInsuredDeta", RowNum, 33);
		InsuredDetails.Insured5_LastName("SuperMediclaimCancerInsuredDeta", RowNum, 34);
		InsuredDetails.Insured5_DOB("SuperMediclaimCancerInsuredDeta", RowNum);
		InsuredDetails.Insured5_height("SuperMediclaimCancerInsuredDeta", RowNum, 35);
		InsuredDetails.Insured5_inch("SuperMediclaimCancerInsuredDeta", RowNum, 36);
		InsuredDetails.Insured5_weight("SuperMediclaimCancerInsuredDeta", RowNum, 37);
		
	}
	
	public static void setDataOfInsuredForMember6(int RowNum) throws Exception{
		
		InsuredDetails.Insured6_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 38);
		InsuredDetails.Insured6_FirstName("SuperMediclaimCancerInsuredDeta", RowNum, 39);
		InsuredDetails.Insured6_LastName("SuperMediclaimCancerInsuredDeta", RowNum, 40);
		InsuredDetails.Insured6_DOB("SuperMediclaimCancerInsuredDeta", RowNum);
		InsuredDetails.Insured6_height("SuperMediclaimCancerInsuredDeta", RowNum, 41);
		InsuredDetails.Insured6_inch("SuperMediclaimCancerInsuredDeta", RowNum, 42);
		InsuredDetails.Insured6_weight("SuperMediclaimCancerInsuredDeta", RowNum, 43);
		
	}
	
	
	//Function for set Insured Details 
	public static void SetInsuredDetails(int RowNum) throws Exception{
		
		String[][] TestCaseData = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String TotalMember = TestCaseData[RowNum][5].toString().trim();
		
		String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancerInsuredDeta");
		
		String Insured1_realtion=TestCaseData1[RowNum][8].toString();
		String Insured2_realtion=TestCaseData1[RowNum][14].toString();
		String Insured3_realtion=TestCaseData1[RowNum][20].toString();
		String Insured4_realtion=TestCaseData1[RowNum][26].toString();
		String Insured5_realtion=TestCaseData1[RowNum][32].toString();
		String Insured6_realtion=TestCaseData1[RowNum][38].toString();
		
		switch(TotalMember){
		
		case "1":
			
			if(Insured1_realtion.equalsIgnoreCase("Self-primary")){     
				InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
			}
			else{
				setDataOfInsuredForMember1(RowNum);  //called the function to Set Data for Insured Member 1
			}
			
			break;
		
		 case "2":
			 if(Insured1_realtion.equalsIgnoreCase("Self-primary")){
					InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
				}
			 else if(Insured2_realtion.equalsIgnoreCase("Self-primary")){
				     setDataOfInsuredForMember1(RowNum);  //Called the function to Set Data for Insured Member 2
					
				     InsuredDetails.Insured2_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 14);  
			  }
			 else{
				 setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1

				 setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
			 }
				
			break;
		    
		 case "3":
			
			if(Insured1_realtion.equalsIgnoreCase("Self-primary")) {
				InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
				
				 setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2

				 setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
			}
			else if(Insured2_realtion.equalsIgnoreCase("Self-primary")){
				
				setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
				
				InsuredDetails.Insured2_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 14);
				
				setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
			
			}
			else if(Insured3_realtion.equalsIgnoreCase("Self-primary")){
				setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
				
				setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
				
				InsuredDetails.Insured3_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 20);
			}
			
			else{
                setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
				
				setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
				
				setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
			}
			
			break;
			
		    case "4":
		    	if(Insured1_realtion.equalsIgnoreCase("Self-primary")) {
					InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
					
					 setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2

					 setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					 
					 setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
				}
				else if(Insured2_realtion.equalsIgnoreCase("Self-primary")){
					
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					InsuredDetails.Insured2_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 14);
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
				
				}
				else if(Insured3_realtion.equalsIgnoreCase("Self-primary")){
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					InsuredDetails.Insured3_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 20);
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
				}
				else if(Insured4_realtion.equalsIgnoreCase("Self-primary")){
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
						
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
						
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					InsuredDetails.Insured4_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 26);
				}
				
				else{
	                setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
				}
				
				break;
			
			
		    case "5":
		    	
		    	if(Insured1_realtion.equalsIgnoreCase("Self-primary")) {
					InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
					
					 setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2

					 setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					 
					 setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					 
					 setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
				}
				else if(Insured2_realtion.equalsIgnoreCase("Self-primary")){
					
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					InsuredDetails.Insured2_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 14);
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
				
				}
				else if(Insured3_realtion.equalsIgnoreCase("Self-primary")){
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					InsuredDetails.Insured3_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 20);
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
				}
				else if(Insured4_realtion.equalsIgnoreCase("Self-primary")){
                   setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					InsuredDetails.Insured4_weight("SuperMediclaimCancerInsuredDeta", RowNum, 26);
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
				}
		    	
				else if(Insured5_realtion.equalsIgnoreCase("Self-primary")){
					
                    setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					InsuredDetails.Insured5_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 32);
				}
				
				else{
	                setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
				}
				
				break;
	
			
		    case "6":
		    	
		    	if(Insured1_realtion.equalsIgnoreCase("Self-primary")) {
					InsuredDetails.Insured1_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 8);
					
					 setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2

					 setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					 
					 setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					 
					 setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
					 
					 setDataOfInsuredForMember6(RowNum); //Called the function to Set Data for Insured Member 6
				}
				else if(Insured2_realtion.equalsIgnoreCase("Self-primary")){
					
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					InsuredDetails.Insured2_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 14);
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
					
					setDataOfInsuredForMember6(RowNum); //Called the function to Set Data for Insured Member 6
				
				}
				else if(Insured3_realtion.equalsIgnoreCase("Self-primary")){
					setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					InsuredDetails.Insured3_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 20);
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
					
					setDataOfInsuredForMember6(RowNum); //Called the function to Set Data for Insured Member 6
				}
				else if(Insured4_realtion.equalsIgnoreCase("Self-primary")){
                   setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					InsuredDetails.Insured4_weight("SuperMediclaimCancerInsuredDeta", RowNum, 26);
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
					
					setDataOfInsuredForMember6(RowNum); //Called the function to Set Data for Insured Member 6
				}
		    	
				else if(Insured5_realtion.equalsIgnoreCase("Self-primary")){
					
                    setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					InsuredDetails.Insured5_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 32);
					
					setDataOfInsuredForMember6(RowNum); //Called the function to Set Data for Insured Member 6
				}
		    	
                  else if(Insured6_realtion.equalsIgnoreCase("Self-primary")){
					
                    setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
					
					InsuredDetails.Insured6_Relation("SuperMediclaimCancerInsuredDeta", RowNum, 38);
				}
		    	
				
				else{
	                setDataOfInsuredForMember1(RowNum); //Called the function to Set Data for Insured Member 1
					
					setDataOfInsuredForMember2(RowNum); //Called the function to Set Data for Insured Member 2
					
					setDataOfInsuredForMember3(RowNum); //Called the function to Set Data for Insured Member 3
					
					setDataOfInsuredForMember4(RowNum); //Called the function to Set Data for Insured Member 4
					
					setDataOfInsuredForMember5(RowNum); //Called the function to Set Data for Insured Member 5
					
					setDataOfInsuredForMember6(RowNum); //Called the function to Set Data for Insured Member 6
				}
				
				break;
		       }
				InsuredDetails.clickOnNextButton();
    	   
		}		
//Verify Premium Amount
public static void verifyPremiumAmountOnQuotationPage(){
	WebElement e1=driver.findElement(By.xpath("//p[@class='amount ng-binding']"));
	//pointToElement(e1);
	String premium1=e1.getText();
	//System.out.println(premium1);
	 String AcutalpremiumAmount=premium1.replaceAll("\\W", "");
	 String expectedPremiumAmountFromFirstPage=AddonsforProducts.ExpectedpremiumAmount;
	 System.err.println("Acutal Premium Amount in Quotation Page: "+ AcutalpremiumAmount);
	 System.err.println("Expected Premium Amount in First Page: "+ expectedPremiumAmountFromFirstPage);
	 Assert.assertEquals(expectedPremiumAmountFromFirstPage, AcutalpremiumAmount);
	 System.err.println("Acutal Premium Amount: "+ AcutalpremiumAmount + "Excpcted Premium Amount: "+ expectedPremiumAmountFromFirstPage+ "both are matching and verified");
	
}
	
	
	
	//functions for set Payment Details
	public static void setPaymetDetailsSuperMediClaim(int Rownum) throws Exception{
		PaymentPage.setPaymentDetails("Credentials", Rownum);
	}
	
	
	
}
