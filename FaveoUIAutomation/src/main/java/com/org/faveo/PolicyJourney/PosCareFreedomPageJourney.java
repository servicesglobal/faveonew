package com.org.faveo.PolicyJourney;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Assertions.DataVerificationinDb;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.model.HealthQuestions;
import com.org.faveo.model.InsuredDetails;
import com.org.faveo.model.PayUPaymentFn;
import com.org.faveo.model.ProposerDetailsPageFn;
import com.org.faveo.model.QuotationPage;
import com.org.faveo.utility.WriteExcel;

public class PosCareFreedomPageJourney extends BaseClass implements AccountnSettingsInterface
{

	public static String ProposalPremimPage_Value=null;
	public static String proposalSummarypremium_value=null;
	
	@Test(priority = 1)
	public static void QuotationPageJourney(int Rownum) throws Exception {
		// Reading Proposer Name from Excel
		QuotationPage.ProposerName("PosCarefreedom_Quotation_Data", Rownum, 2);

		// Reading Emailfrom Excel Sheet
		QuotationPage.ProposerEmail("PosCarefreedom_Quotation_Data", Rownum, 3);

		// Reading Mobile Number from Excel
		QuotationPage.ProposerMobile("PosCarefreedom_Quotation_Data", Rownum, 4);

		// Enter The Value of Total members present in policy
		QuotationPage.SelectMembers("PosCarefreedom_Quotation_Data", Rownum, 5);

		// again call the dropdown
		QuotationPage.SelectAgeofMemberCareFreedom("PosCarefreedom_Quotation_Data", "Pos_CareFreedom_Insured_Details",
				Rownum, 5, 6, 7, 8);

		// Read the Value of Suminsured
		QuotationPage.SumInsured("PosCarefreedom_Quotation_Data", Rownum, 9);

		// Reading the value of Tenure from Excel
		QuotationPage.Tenure("PosCarefreedom_Quotation_Data", Rownum, 10);
		Thread.sleep(5000);
		// Scroll Window Up
		// BaseClass.scrollup();
		scrollup();
		// NCB Super Addon Selection
		AddonsforProducts.CareFreedomAddons("PosCarefreedom_Quotation_Data", Rownum, 11);

		// Assertion
		QuotationandProposalVerification.PremiumAssertionPOSFreedom();

		String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");
		String executionstatus = TestCase[Rownum][4].toString().trim();
		if (executionstatus.equals("Edit")) {
			Edit.editposcarefreedom();
		} else {
			System.out.println("Edit not required.............");
		}
		scrollup();

	}
	
	@Test(priority=2,dependsOnMethods = { "QuotationPageJourney" })
	public static void ProposalPageJourney(int Rownum) throws Exception
	{
				// Reading Proposer Title from Excel
		       Thread.sleep(10000);
				ProposerDetailsPageFn.ProposerTitle("PosCarefreedom_Quotation_Data", Rownum, 1);

				// Entering DOB from Excel into dob field
				ProposerDetailsPageFn.ProposerDOB("PosCarefreedom_Quotation_Data", Rownum, 12);
				
				
				// Reading AddressLine 1 from Excel
				ProposerDetailsPageFn.ProposerAddressLine1("PosCarefreedom_Quotation_Data", Rownum, 13);
				ProposerDetailsPageFn.ProposerAddressLine2("PosCarefreedom_Quotation_Data", Rownum, 14);
				
				
				// Reading Pincode from Excel
				ProposerDetailsPageFn.Pincode("PosCarefreedom_Quotation_Data", Rownum, 15);
				
				// Reading Proposer Height in Feet from Excel
				ProposerDetailsPageFn.ProposerHeightFeet("PosCarefreedom_Quotation_Data", Rownum, 16);
				
				// Reading Proposer Height in Inch from Excel
				ProposerDetailsPageFn.ProposerHeightInch("PosCarefreedom_Quotation_Data", Rownum, 17);
				
				// Reading Weight of Proposer from Excel
				ProposerDetailsPageFn.ProposerWeight("PosCarefreedom_Quotation_Data", Rownum, 18);
				
				// Reading Nominee Name from Excel
				ProposerDetailsPageFn.ProposerNominee("PosCarefreedom_Quotation_Data", Rownum, 19);
				

				// Nominee Relation
				ProposerDetailsPageFn.ProposerNomineeRelation("PosCarefreedom_Quotation_Data", Rownum, 20);

							
				// Pan Card
				ProposerDetailsPageFn.PanCardNumber("PosCarefreedom_Quotation_Data", Rownum, 21);
			
				// Click on Next button
				clickElement(By.id(Button_NextProposer_id));
				System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				
				InsuredDetails.InsuredDetail("Pos_CareFreedom_Insured_Details");
							
				//Health Questionnarire Elements 
				HealthQuestions.CareFreedomHealthQuestions("PosCarefreedom_Quotation_Data", "Pos_CareFreedom_QuestionSet", Rownum, 22);
			
				BaseClass.ErroronHelathquestionnaire();
		
	}
	
	public static void ProposalPageJourney2(int Rownum) throws Exception
	{
				// Reading Proposer Title from Excel
		       Thread.sleep(10000);
				ProposerDetailsPageFn.ProposerTitle("PosCarefreedom_Quotation_Data", Rownum, 1);

				// Entering DOB from Excel into dob field
				ProposerDetailsPageFn.ProposerDOB("PosCarefreedom_Quotation_Data", Rownum, 12);
				
				
				// Reading AddressLine 1 from Excel
				ProposerDetailsPageFn.ProposerAddressLine1("PosCarefreedom_Quotation_Data", Rownum, 13);
				ProposerDetailsPageFn.ProposerAddressLine2("PosCarefreedom_Quotation_Data", Rownum, 14);
				
				
				// Reading Pincode from Excel
				ProposerDetailsPageFn.Pincode("PosCarefreedom_Quotation_Data", Rownum, 15);
				
				// Reading Proposer Height in Feet from Excel
				ProposerDetailsPageFn.ProposerHeightFeet("PosCarefreedom_Quotation_Data", Rownum, 16);
				
				// Reading Proposer Height in Inch from Excel
				ProposerDetailsPageFn.ProposerHeightInch("PosCarefreedom_Quotation_Data", Rownum, 17);
				
				// Reading Weight of Proposer from Excel
				ProposerDetailsPageFn.ProposerWeight("PosCarefreedom_Quotation_Data", Rownum, 18);
				
				// Reading Nominee Name from Excel
				ProposerDetailsPageFn.ProposerNominee("PosCarefreedom_Quotation_Data", Rownum, 19);
				

				// Nominee Relation
				ProposerDetailsPageFn.ProposerNomineeRelation("PosCarefreedom_Quotation_Data", Rownum, 20);

							
				// Pan Card
				ProposerDetailsPageFn.PanCardNumber("PosCarefreedom_Quotation_Data", Rownum, 21);
			
				// Click on Next button
				clickElement(By.id(Button_NextProposer_id));
				System.out.println("Sucessfully Clicked on Next Button from Proposer Detail Page.");
				
			
		
	}

	@Test(priority=3,dependsOnMethods = { "ProposalPageJourney" })
	public static void ProposalSummaryPage(String TestCasenameSheet,int Rownum) throws Exception
	{                                                              
		proposalSummarypremium_value=driver.findElement(By.xpath("//*[@class='premium_amount ng-binding']")).getText();
		System.out.println("Premium is :"+proposalSummarypremium_value);
		
		String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");
		String executionstatus = TestCase[Rownum][4].toString().trim();
		if (executionstatus.equals("Edit")) {
			BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage_afterEdit(Edit.afterEdit_Proposalpremium_value,proposalSummarypremium_value);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		} else {
			QuotationandProposalVerification.VerifyPremiumIncrease_on_Proposalsummarypagenoedit();
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		}
		
		
		waitForElement(By.xpath(payu_proposalnum_xpath));
		String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number : "+PayuProposalNum);
		
		waitForElement(By.xpath(payuAmount_xpath));
		String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
		String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
		System.out.println("Pay U Premium Amount : "+FinalAmount);
		
			PayUPaymentFn.PayuPage_Credentials();
			
			BaseClass.scrolldown();
			
			QuotationandProposalVerification.ThankyouPagemessageVerification();
			
			String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
			System.out.println(ProposerName);
			
			/*DataVerificationinDb.DBVerification(PayuProposalNum);*/
			waitForElement(By.xpath(PolProp_xpath));
			
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			
			PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);

			WriteExcel.setCellData1(TestCasenameSheet, TestResult, ThankyoupageProposal_Pol_num, Rownum, 2, 3);
			
			
	}
	
	
	public static void ProposalSummaryPage_ShareQuote(String TestCasenameSheet,int Rownum) throws Exception
	{                                                              
		/*proposalSummarypremium_value=driver.findElement(By.xpath("//*[@class='premium_amount ng-binding']")).getText();
		System.out.println("Premium is :"+proposalSummarypremium_value);
		
		String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");
		String executionstatus = TestCase[Rownum][4].toString().trim();
		if (executionstatus.equals("Edit")) {
			BaseClass.VerifyPremiumIncrease_on_Proposalsummarypage_afterEdit(Edit.afterEdit_Proposalpremium_value,proposalSummarypremium_value);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		} else {
			QuotationandProposalVerification.VerifyPremiumIncrease_on_Proposalsummarypagenoedit();
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		}*/
		
		
		/*waitForElement(By.xpath(payu_proposalnum_xpath));
		String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number : "+PayuProposalNum);
		
		waitForElement(By.xpath(payuAmount_xpath));
		String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
		String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
		System.out.println("Pay U Premium Amount : "+FinalAmount);
		*/
			PayUPaymentFn.PayuPage_Credentials();
			
			/*BaseClass.scrolldown();
			
			QuotationandProposalVerification.ThankyouPagemessageVerification();
			
			String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
			System.out.println(ProposerName);
			
			DataVerificationinDb.DBVerification(PayuProposalNum);
			waitForElement(By.xpath(PolProp_xpath));
			
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			
			PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);

			WriteExcel.setCellData1(TestCasenameSheet, TestResult, ThankyoupageProposal_Pol_num, Rownum, 2, 3);*/
			
			
	}
	
}
