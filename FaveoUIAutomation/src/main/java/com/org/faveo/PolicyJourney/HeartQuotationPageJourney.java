package com.org.faveo.PolicyJourney;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.faveo.edit.Edit;
import com.faveo.org.supermediclaim.Heart;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class HeartQuotationPageJourney extends BaseClass implements AccountnSettingsInterface {

	public static String PolicyType = null;
	
	public static void heartquotaionjourney() throws Exception {
		
		String[][] TestCase=BaseClass.excel_Files("Heart_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("Heart_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("HearInsuredDetails");
		Heart h=new Heart();
		Thread.sleep(5000);
		
		//String TestcaseName=(TestCase[h.n][0].toString().trim() + " - " + TestCase[h.n][1].toString().trim());
		//logger = extent.startTest("CareWithNCB - " + TestcaseName);
		//System.out.println("Heart tets case name is  - " + TestcaseName);
		
		driver.findElement(By.name("name")).clear();
		driver.findElement(By.name("name")).sendKeys(TestCaseData[h.n][2].toString().trim() + "  " + TestCaseData[h.n][3].toString().trim());
		logger.log(LogStatus.PASS,"Entered Name is  " + TestCaseData[h.n][2].toString().trim() + "  " + TestCaseData[h.n][3].toString().trim());
		System.out.println("Entered Name is  " + TestCaseData[h.n][2].toString().trim() + "  " + TestCaseData[h.n][3].toString().trim());
		
		driver.findElement(By.name("ValidEmail")).clear();

		String email = TestCaseData[h.n][6].toString().trim();
		System.out.println("Email is :" + email);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		logger.log(LogStatus.PASS,"Entered Email id is  "+email );
		Thread.sleep(5000);
		String mnumber = TestCaseData[h.n][5].toString().trim();
		int size = mnumber.length();
		logger.log(LogStatus.PASS,"Entered Mobile number is  "+mnumber );

		System.out.println("mobile number is: " + mnumber);
		String format = "^[789]\\d{9}$";
System.out.println("Entered mobile number is : "+mnumber);
		if (mnumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}
	}

	public static void HeartDropdown() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
		Heart h=new Heart();

		Thread.sleep(5000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[h.n][15].toString().trim() + "]")).click();//select 4 based on the excel data
					System.out.println("Total Number of Member Selected : " + TestCaseData[h.n][15].toString().trim());
					Thread.sleep(2000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		//	dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[h.n][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		//int covertype;

		outer:

			for (@SuppressWarnings("unused") WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String Membersdetails = TestCaseData[h.n][16];
					if (Membersdetails.contains("")) {

						BaseClass.membres = Membersdetails.split("");

						member:
							// total number of members
							for (int i = 1; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								driver.findElement(By
										.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
								.click();

								// List Age of members dropdown
								List<WebElement> List = driver	
										.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								for (WebElement ListData : List) {
                                              // System.out.println(ListData.getText());
                                               System.out.println(FamilyData[mcount][0].toString().trim());
									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Selcted Age Of Member :" + ListData.getText());

										ListData.click();
									}

								}

							}
					}
					break;
				} 
				else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
					for (WebElement DropDowns : dropdowns) {
						 if (DropDowns.getText().equals("5 - 17 years")) {

							String Membersdetails = TestCaseData[h.n][16];
							if (Membersdetails.contains(",")) {
								BaseClass.membres = Membersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								for (WebElement ListData : List) {

									if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is :" + ListData.getText());
										Thread.sleep(2000);
										ListData.click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
	}
	
	public static void Heart_dragdrop() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
		
		Heart h=new Heart();
		String suminsure = TestCaseData[h.n][17];


		WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/div/div"));
		List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
		//String SliderValue=null;

		for(WebElement Slider:dragable) {
			System.out.println("Policy type is : "+PolicyType);
			Thread.sleep(2000);
			if(	Slider.getText().equals(suminsure)) {
				Slider.click();
				break;
			}
		}
Thread.sleep(2000);

	}
	
	public static void HeartTenure() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
		Heart h=new Heart();
		
		int Tenure = Integer.parseInt(TestCaseData[h.n][18].toString().trim());

		System.out.println("Selected Tenure is : " +Tenure);
		if(Tenure==1)
		{
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			//clickElement(By.xpath(Radio_Tenure1_xpath));
		}
		else if(Tenure==2)
		{
			clickbyHover(By.xpath(Radio_Tenure2_xpath));	
			//clickElement(By.xpath(Radio_Tenure2_xpath));
		}
		else if(Tenure==3)
		{
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			//clickElement(By.xpath(Radio_Tenure3_xpath));
		}		
		BaseClass.scrollup();
		Thread.sleep(2000);
	}
	public static void Heart_Monthly_frequency() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");		
		Heart h=new Heart();
		int Tenure = Integer.parseInt(TestCaseData[h.n][18].toString().trim());

		String Monthlyfrequency = TestCaseData[h.n][19];
		System.out.println("Monthly Frequency is :  "  +Monthlyfrequency);
		if(Tenure==1)
		{
			System.out.println("Selected monthly frequency is : "+Monthlyfrequency);
		}

		else if(Tenure==2) {
			if(Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='paymentRadio1q']"));

			}else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='paymentRadio2q']"));
			}
			else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='paymentRadio0q']"));
			}
		}else if(Tenure==3) {
			if(Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='paymentRadio1q']"));

			}else if(Monthlyfrequency.contains("Quarterly")) {
				try {
					clickElement(By.xpath("//label[@for='paymentRadio2q']"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//input[@id='paymentRadio0q']"));
			}
		}
		/*clickElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[2]/div[1]/button"));
		Thread.sleep(3000);
		clickElement(By.xpath("//*[@class='col-md-12 col-xs-12 text-center last-child']/button[contains(text(),'Send')]"));*/
Thread.sleep(5000);
readPremiumFromFirstPage();
	}	
	
	public static String premium1;
	public static String ExpectedpremiumAmount;
	
	//Read Premium Amount on - First Page
	public static void readPremiumFromFirstPage(){
		Heart h=new Heart();
		WebElement e1=driver.findElement(By.xpath("//p[@class='get_quot_total_premium']/span"));
		scrollup();
		 premium1=e1.getText();
		System.out.println(premium1);
		ExpectedpremiumAmount=premium1.replaceAll("\\W", "");
		System.out.println("Premium Amount in Quotation Page: "+  ExpectedpremiumAmount);
		logger.log(LogStatus.PASS, "Premium Amount in Quotation Page: "+  ExpectedpremiumAmount);
		
	}

	public static void Heart_verify_premium() throws InterruptedException {
		Heart h=new Heart();
		//	WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]/b[1]"));
		WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@id='getquote']/form/div[2]/div[3]/div/div[1]/b/p[1]/span"));
		Thread.sleep(10000);
		String firstpage_PremiumValue=firstpage_Premium.getText();
		Thread.sleep(10000);
		System.out.println("First page premium value is   :"  +firstpage_PremiumValue);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,150)", "");
		//clickElement(By.xpath(Group_BuyNow_xpath));
		clickElement(By.xpath(critical_xpath));
		Thread.sleep(5000);
		jse.executeScript("window.scrollBy(0,-350)", "");
		//WebElement secondpage_premium=driver.findElement(By.xpath("//div[@class='col-md-12 tr_quotation_heading']/h4/span/span[@class='ng-binding']"));

		WebElement secondpage_premium=driver.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		String secondpage_PremiumValue=secondpage_premium.getText();
		Thread.sleep(10000);
		System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
		Assert.assertEquals(firstpage_PremiumValue, secondpage_PremiumValue);


	}
	public static void Heart_proposerDetails() throws Exception {

		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
		Heart h=new Heart();
		driver.findElement(By.xpath("//*[@id=\"progressbar\"]/li[1]")).click();
		Thread.sleep(20000);
		System.out.println("All the given data is correct");
		String Title = TestCaseData[h.n][1].toString().trim();
		System.out.println("titel Name is:" + Title);
	
		clickElement(By.xpath(click_title_xpath));

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[h.n][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));

		Thread.sleep(3000);
		

		final String address1 = TestCaseData[h.n][7].toString().trim();
		System.out.println("Adress1 name is :" + address1);
		enterText(By.xpath(addressline1_xpath), address1);
		enterText(By.xpath(addressline2_xpath), TestCaseData[h.n][8].toString().trim());
		enterText(By.xpath(pincode_xpath), TestCaseData[h.n][9]);
		// Height selection
		String Height = TestCaseData[h.n][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[h.n][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[h.n][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);

		String NomineeName = TestCaseData[h.n][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		// Nominee Relation
		String Nrelation = TestCaseData[h.n][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
		String pancard=TestCaseData[h.n][20].toString().trim();
System.out.println("pancard number is :"  +pancard);
		String pospancard="KJHYS8977E";
		System.out.println("pancard number is :"+pospancard); try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).
			sendKeys(pospancard); }catch(Exception e) {
				System.out.println("Pan card field not visibled"); }


		clickElement(By.xpath(submit_xpath));
Thread.sleep(5000);

	}
	public static void Heart_insuredDetails() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
	//	String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
		Heart h=new Heart();
		int mcountindex = 0;
		int mcount;
		String Membersdetails = TestCaseData[h.n][16].toString().trim();
		// BaseClass.membres=Membersdetails.split(",");
		// System.out.println("BaseClass.membres :
		// "+BaseClass.membres[mcountindex].length());
		// mcount= Integer.parseInt(BaseClass.membres[mcountindex].toString());
		mcountindex = mcountindex + 1;
		// String Membersdetails=TestCaseData[1][16].toString().trim();
		if (Membersdetails.contains(",")) {

			// data taking form test case sheet which is
			// 7,4,1,8,2,5
			BaseClass.membres = Membersdetails.split(",");
		} else {
			BaseClass.membres = Membersdetails.split(" ");
		}

		System.out.println("BaseClass.membres.length " + BaseClass.membres.length);
		member: 
			for (int i = 0; i <= BaseClass.membres.length-1; i++) {
				//for (int i = 0; i <= BaseClass.membres.length-2; i++) { for number of members=1

				mcount = Integer.parseInt(BaseClass.membres[i].toString());
				if (i == 0) {
					clickElement(By.xpath(title1_xpath));
					// Select Self Primary
					BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
				} else {

					// String firstName= "fname"+i+"_xpath";
					String Date = FamilyData[mcount][5].toString().trim();
					// Date=Date.replaceAll("-", "/");
					// Relation
					BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
					// title
					BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
					enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
					enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
					clickElement(By.name("rel_dob" + i));
					enterText(By.name("rel_dob" + i), String.valueOf(Date));

					BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
					BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
					enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());
					//Thread.sleep(5000);
				}
			}
		clickElement(By.xpath("//*[@id='msform']/div[2]/fieldset[2]/input[2]"));

	}

	public static void Heart_questionSet() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files1("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("HearInsuredDetails");
		String[][] QuestionSetData = BaseClass.excel_Files1("HeartQuestionDetails");
		Heart h=new Heart();

		
		String preExistingdeases = TestCaseData[h.n][21].toString().trim();
		Thread.sleep(5000);
		System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		//BaseClass.scrollup();
		try{
			if(preExistingdeases.contains("YES")) {
				// String testra = TestCaseData[1][21].toString().trim();
				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				//Thread.sleep(2000);
				String years=null;
				String Details=null;		
				for(int qlist=1;qlist<=13;qlist++) {
					Details =QuestionSetData[h.n][qlist+(qlist-1)].toString().trim();
					years=QuestionSetData[h.n][qlist+qlist].toString().trim();
					if(Details.equals("")) {
						//break;
					}else 
					{
						int detailsnumber = Integer.parseInt(Details);

						//Will click on check box and select the month & year u
						detailsnumber=detailsnumber+1;
						System.out.println("Details and years are :"+Details+"----"+years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label"),years);
						}catch(Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr["+qlist+"]/td["+detailsnumber+"]//label[@class='monthYear']"),years);
						}
					}
				}	
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
				Thread.sleep(5000);
			}
		}
		catch(Exception e)
		{
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}
		Thread.sleep(5000);

		//question2 selection
		String Questionsdata = null;
		String ChecksData = null;
		String[] ChckData = null;
		int datacheck = 0;
		//int datacheckq3=0;
		ChecksData = TestCaseData[h.n][22].toString().trim();
		Questionsdata= QuestionSetData[h.n][29].toString().trim();

		if (ChecksData.equalsIgnoreCase("no")) {
			System.out.println("Quatation set to NO");
			driver.findElement(By.xpath("//label[@for='question_3_"+ChecksData+"']")).click();
		}else {
			driver.findElement(By.xpath("//label[@for='question_3_"+ChecksData+"']")).click();
			if (Questionsdata.contains(",")) {
				ChckData = Questionsdata.split(",");
				for (String Chdata : ChckData) {
					datacheck = Integer.parseInt(Chdata);
					datacheck = datacheck - 1;
					driver.findElement(By.xpath("//input[@name='qs_503_"+datacheck+"']")).click();		

				}
			}else {
				driver.findElement(By.xpath("//input[@name='qs_503_"+datacheck+"']")).click();
			}
		}
		//question 3
		String ChecksDatav = TestCaseData[h.n][24].toString().trim();
		//if (ChecksData.equalsIgnoreCase("no")) {
		if (ChecksDatav.equalsIgnoreCase("no")) {
		System.out.println("Quatation set to NO");
			driver.findElement(By.xpath("//label[@for='question_5_"+ChecksDatav+"']")).click();
		}else {
			driver.findElement(By.xpath("//label[@for='question_5_"+ChecksDatav+"']")).click();
			if (Questionsdata.contains(",")) {
				ChckData = Questionsdata.split(",");
				for (String Chdata : ChckData) {
					datacheck = Integer.parseInt(Chdata);
					datacheck = datacheck - 1;           //qs_P010_0
					driver.findElement(By.xpath("//input[@name='qs_P010_"+datacheck+"']")).click();		

				}
			}else {
				driver.findElement(By.xpath("//input[@name='qs_P010_"+datacheck+"']")).click();
			}
		}
		
		
		
		//Question 4 selection
		String question3ChecksData=null;
		String ChecksDataf = TestCaseData[h.n][25].toString().trim();
		//if (ChecksData.equalsIgnoreCase("no")) {
		if (ChecksDataf.equalsIgnoreCase("no")) {
			System.out.println("Quatation set to NO");
			driver.findElement(By.xpath("//label[@for='question_6_"+ChecksDataf+"']")).click();
		}else {
			driver.findElement(By.xpath("//label[@for='question_6_"+ChecksDataf+"']")).click();
			if (Questionsdata.contains(",")) {
				ChckData = Questionsdata.split(",");
				for (String Chdata : ChckData) {
					datacheck = Integer.parseInt(Chdata);
					datacheck = datacheck - 1;           //qs_P010_0
					driver.findElement(By.xpath("//input[@name='qs_232_0']")).click();	
					driver.findElement(By.xpath("//input[@name='qs_129_0']")).click();
					driver.findElement(By.xpath("//input[@name='qs_147_0']")).click();
				}
			}else {
				driver.findElement(By.xpath("//input[@name='qs_P010_"+datacheck+"']")).click();
			}
		}
		String question3Questionsdata=null;
		//int datacheck3=0;
		question3ChecksData = TestCaseData[h.n][23].toString().trim();


		String quantityperDay=null;

		if (question3ChecksData.equalsIgnoreCase("no")) {
			System.out.println("Quatation set to NO");
			driver.findElement(By.xpath("//label[@for='question_4_"+question3ChecksData+"']")).click();
		}else {
			driver.findElement(By.xpath("//label[@for='question_4_"+question3ChecksData+"']")).click();

		
			int queslist;
			int qcount=0;
			for( queslist=1;queslist<=3;queslist++) {
				List<WebElement>Noofmember=driver.findElements(By.xpath("//*[@id='collapse4']/div/table/tbody/tr["+queslist+"]/td/div/input[@type='checkbox']"));
				int total_membe=Noofmember.size();
				System.out.println("Total members are : "+total_membe);
				int loop=30+qcount;
				//int loop1=1;
				question3Questionsdata= QuestionSetData[h.n][loop].toString().trim();
				quantityperDay=QuestionSetData[h.n][loop+1].toString().trim();
				int noofMemb = Integer.parseInt(TestCaseData[h.n][15].toString().trim());
				for(int i=1;i<=noofMemb;i++) {	
					if (question3Questionsdata.contains(",")) {
						ChckData = question3Questionsdata.split(",");
						for(String chekdta:ChckData) {
							if(chekdta.equals(String.valueOf(i))) {
								Noofmember.get(i-1).click();
								int td=i+1;
								driver.findElement(By.xpath("//*[@id='collapse4']/div/table/tbody/tr["+queslist+"]/td["+td+"]//textarea[@required='required']")).sendKeys(quantityperDay);
								break;
							}
						}
					}
					else if(question3Questionsdata.contains("")){
						ChckData = question3Questionsdata.split("");
						for(String chekdta:ChckData) {
							if(chekdta.equals(String.valueOf(i))) {
								Noofmember.get(i-1).click();
								int td=i+1;
								driver.findElement(By.xpath("//*[@id='collapse4']/div/table/tbody/tr["+queslist+"]/td["+td+"]//textarea[@required='required']")).sendKeys(quantityperDay);
								break;
							}
						}
					}
				}
				qcount=qcount+2;
			}
		}
			//question4 selection

			/*String Questions4data = null;
			String Checks4Data = null;
			String[] Chck4Data = null;
			int data4check = 0;
			int datacheckq4=0;*/
			ChecksData = TestCaseData[h.n][24].toString().trim();
			// Questionsdata= QuestionSetData[1][28].toString().trim();

			if (ChecksData.equalsIgnoreCase("no")) {
				System.out.println("Quatation set to NO");
				driver.findElement(By.xpath("//label[@for='question_2_"+ChecksData+"']")).click();
			}else {
				driver.findElement(By.xpath("//label[@for='question_2_"+ChecksData+"']")).click();
				if (Questionsdata.contains(",")) {
					ChckData = Questionsdata.split(",");
					for (String Chdata : ChckData) {
						datacheck = Integer.parseInt(Chdata);
						datacheck = datacheck - 1;
						driver.findElement(By.xpath("//input[@name='qs_H002_"+datacheck+"']")).click();;			

					}
				}else {
					driver.findElement(By.xpath("//input[@name='qs_H002_"+datacheck+"']")).click();;
				}
			}

			driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
			driver.findElement(By.xpath("//*[@id='termsCheckbox3']")).click();
			driver.findElement(By.xpath("//input[@id='alertCheck']")).click();
			// driver.findElement(By.xpath("//input[@id='termsCheckbox3']")).click();
			//driver.findElement(By.xpath("//*[@id=\"alertCheck\"]")).click();
			JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			// driver.findElement(By.xpath("//*[@id='siQues']/label[2]")).click();
			jse1.executeScript("window.scrollBy(0,250)", "");
			Thread.sleep(5000);
			clickElement(By.xpath(proceed_to_pay_xpath));
			Thread.sleep(15000);	

	}
public static void Heart_Payment() throws Exception {


	clickElement(By.xpath(critical_PAYonline_xpath));	
	Thread.sleep(10000);

	String[][] carddetails = BaseClass.excel_Files1("Credentials");
	System.out.println("card number is: " + carddetails[1][0].toString().trim());

	if(driver.findElement(By.xpath("//*[@id='manageCardLink']")).isDisplayed()){  
		Thread.sleep(5000);
		enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
		driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

		Thread.sleep(2000);
		String  title = driver.getTitle();
		System.out.println("Title og Axis emulator page is :  "+title);
		if(title.equals("PayUbiz")||title.equals("Axis Simulator")) {
		enterText(By.xpath("//input[@name='password']"), "123456");
		clickElement(By.xpath("//input[@id='submitBtn']"));
		}else {
			System.out.println("Page is not available");
		}
		String policy_Number;
		Fluentwait(By.xpath("//div[@class='col-md-6 proposal_appliction_no_container']/div[1]/p[2]"));
		try {
			WebElement Policy=driver.findElement(By.xpath("//div[@class='col-md-6 proposal_appliction_no_container']/div[1]/p[2]"));
			policy_Number = Policy.getText();
		} catch (Exception e) {

			WebElement Policy=driver.findElement(By.xpath("//p[@class='your_payment_sucess_p ng-binding']"));
			policy_Number = Policy.getText();

		}

		System.out.println("Policy number is  :"   +policy_Number);

	}else{

		Thread.sleep(5000);
		enterText(By.xpath("//input[@name='ccard_number']"), carddetails[1][0].toString().trim());
		Thread.sleep(5000);
		enterText(By.xpath(card_name_xpath), carddetails[1][1].toString().trim());
		Thread.sleep(5000);
		enterText(By.xpath(cvv_xpath), carddetails[1][2].toString().trim());
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("window.scrollBy(0,250)", "");
		String expmonth = carddetails[1][3].toString().trim();
		String expyear = carddetails[1][4].toString().trim();
		/*
		 * WebElement
		 * elm=driver.findElements(By.xpath("//select[@id='cexpiry_date_month']"
		 * )).click(); Select sel=new Select(elm); sel.selectByValue(month);
		 */

		BaseClass.selecttext("cexpiry_date_month", expmonth.toString());
		BaseClass.selecttext("cexpiry_date_year", expyear.toString());
		driver.findElement(By.xpath("//input[@type='submit' and @name='pay_button']")).click();

		Thread.sleep(2000);
		String  title = driver.getTitle();
		System.out.println("Title og Axis emulator page is :  "+title);
		if(title.equals("PayUbiz")||title.equals("Axis Simulator")) {
		enterText(By.xpath("//input[@name='password']"), "123456");
		clickElement(By.xpath("//input[@id='submitBtn']"));
		}else {
			System.out.println("Page is not available");
		}
		/*String message = driver.findElement(By.xpath("//*[@class='ng-scope']/div[22]/div[1]/div/div[1]/div[1]/div/div[1]/p")).getText();
		System.out.println("Message is :" + message);*/
	}
	
		Thread.sleep(10000);
		WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
		String policy_Number=Policy.getText();
		System.out.println("Policy number is  :"   +policy_Number);
		Thread.sleep(5000);
		WebElement iName=driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/p"));
		String InsuredName=iName.getText();
		System.out.println("Insured Name is :  "  +InsuredName);
		WebElement TDetails=driver.findElement(By.xpath("//div[@class='proposal_app_body']/p[2]"));
		String Tenure=TDetails.getText();
		System.out.println("Tenure is :  "  +Tenure);
		WebElement sumidetails=driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/p[2]"));
		String SumInsured=sumidetails.getText();
		System.out.println("Sum insured is :  "  +SumInsured);
		
		WebElement premiumDetails=driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[3]/p[2]"));
		String Premium=sumidetails.getText();
		System.out.println("Final premium is  :  "  +Premium);
		//driver.close();

		//driver.close();


	
}

}
