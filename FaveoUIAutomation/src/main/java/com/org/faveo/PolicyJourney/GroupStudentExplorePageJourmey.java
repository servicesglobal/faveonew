package com.org.faveo.PolicyJourney;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.travelInsurance.GroupStudentExplore;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class GroupStudentExplorePageJourmey extends BaseClass implements AccountnSettingsInterface {

	public static GroupStudentExplore gse=new GroupStudentExplore();
	
	public static void groupexplorestudentquotation() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		Thread.sleep(10000);
		String email=TestCaseData[gse.n][1].toString().trim();
		String mobilenumber=TestCaseData[gse.n][2].toString().trim();
		System.out.println("Email is :" + email);
		Thread.sleep(4000);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		logger.log(LogStatus.PASS, "Entered Email id: " + email);
		int size = mobilenumber.length();
		System.out.println("mobile number is: " + mobilenumber);
		String format = "^[789]\\d{9}$";

		Thread.sleep(4000);

		if (mobilenumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mobilenumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}
	
	logger.log(LogStatus.PASS, "Entered Mobile nunmber is : " + mobilenumber);
	String GeographicalScope=TestCaseData[gse.n][4].toString().trim();	
	System.out.println("Scope is : "+GeographicalScope);
	logger.log(LogStatus.PASS, "Selected geographical scope is : " + GeographicalScope);
	String PolicyTenure=TestCaseData[gse.n][5].toString().trim();
	System.out.println("Policy Tenure is  : "+ PolicyTenure);
	logger.log(LogStatus.PASS, "Selected tenure is : " + PolicyTenure);
	String planType=TestCaseData[gse.n][6].toString().trim();
	System.out.println(" Plan type is :  "+ planType);	
	logger.log(LogStatus.PASS, "Selected planType is : " + planType);
	String ped=TestCaseData[gse.n][7].toString().trim();
	System.out.println(" Plan type is :  "+ ped);
	logger.log(LogStatus.PASS, "Selected ped is : " + ped);

	String suminsured=TestCaseData[gse.n][8].toString().trim();
	System.out.println("Sum Insured is   :" +suminsured);
	logger.log(LogStatus.PASS, "Selected suminsured is : " + suminsured);
	List<WebElement> ddowns=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div"));
	int dropsize=ddowns.size();
	System.out.println("Total dropdowns are :"  +dropsize);
	int x=0;
	for(WebElement dropdownvalue:ddowns){	
		dropdownvalue.click();
		Thread.sleep(3000);
		String Ageexvalue=TestCaseData[gse.n][3+x].toString().trim();
		System.out.println("Values are : "+Ageexvalue);
		//System.out.println("Age is  :" +Ageexvalue);
		List<WebElement> ddlist=driver.findElements(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div//ul/li"));
		for(int j=0;j<=ddlist.size()-1;j++) {
			System.out.println("Text is : "+ddlist.get(j).getText());
			if(ddlist.get(j).getText().contains(Ageexvalue)) {
				System.out.println("Click value is : "+ddlist.get(j).getText());
				//Thread.sleep(5000);
				ddlist.get(j).click();
				Thread.sleep(5000);
				break;
			}
		}
		x=x+1;

	}
  }
	public static void suminsured() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		String suminsured=TestCaseData[gse.n][8].toString().trim();
		String planType=TestCaseData[gse.n][6].toString().trim();
		WebElement progress= driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[2]/div/div/div"));

		List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
	
		int total_size=Slidnumber.size();			
		System.out.println("Slider numbers are :  " +total_size );
		String SliderValue=null;
		for(WebElement Slider:Slidnumber) {
		SliderValue=suminsured;
        System.out.println(Slider.getText());
			Thread.sleep(3000);
			if(Slider.getText().equals(SliderValue.toString())) {
				Thread.sleep(5000);
			
				Slider.click();
				break;
			}
		}
		scrolldown();
		//Selectecting optional covers here
		if(planType.equals("Explore Start")) {

			System.out.println("Optional cobver is not required for this plan type");
		}else {
		clickElement(By.xpath(Optional_cover_xpath));
	
		String optionalcover1=TestCaseData[gse.n][28].toString().trim();
		
		if(optionalcover1.equals("Yes")) {
			clickElement(By.xpath(optionalcover1_xpath));
		}else {
			System.out.println("Optional cover1  not required");
		}
       String optionalcover2=TestCaseData[gse.n][29].toString().trim();
		
		if(optionalcover2.equals("Yes")) {
			clickElement(By.xpath(optionalcover2_xpath));
		}else {
			System.out.println("Optional cover2  not required");
		}
        String optionalcover3=TestCaseData[gse.n][30].toString().trim();
		
		if(optionalcover3.equals("Yes")) {
			clickElement(By.xpath(optionalcover3_xpath));
		}else {
			System.out.println("Optional cover3  not required");
		}
       String optionalcover4=TestCaseData[gse.n][31].toString().trim();
		
		if(optionalcover4.equals("Yes")) {
			clickElement(By.xpath(optionalcover4_xpath));
		}else {
			System.out.println("Optional cover4  not required");
		}
       String optionalcover5=TestCaseData[gse.n][32].toString().trim();
		
		if(optionalcover5.equals("Yes")) {
			clickElement(By.xpath(optionalcover5_xpath));
		}else {
			System.out.println("Optional cover5  not required");
		}
		String optionalcover6=TestCaseData[gse.n][33].toString().trim();
		
		if(optionalcover6.equals("Yes")) {
			clickElement(By.xpath(optionalcover6_xpath));
		}else {
			System.out.println("Optional cover6  not required");
		}
		}
	}
	public static void premiumAssertion() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		String executionstatus=	TestCase[gse.n][4].toString().trim();	
		JavascriptExecutor jse= (JavascriptExecutor)driver;
		if(executionstatus.contentEquals("Edit")) {
			waitForElement(By.xpath(posStudent_buynow_xpath));
			clickElement(By.xpath(posStudent_buynow_xpath));
			Edit.GroupStudentExploreEdit();
			Thread.sleep(5000);
			WebElement Editfirstpage_Premium=driver.findElement(By.xpath("//*[@class='get_quot_total_premium']/span"));
			String Editfirstpage_PremiumValue=Editfirstpage_Premium.getText();				
			System.out.println("After Edit First page premium value is   :"  +Editfirstpage_PremiumValue);
			JavascriptExecutor jsee = (JavascriptExecutor)driver;
			clickElement(By.xpath(Edit_Update_premium_button_xpath));
			jse.executeScript("window.scrollBy(0,-350)", "");
            scrollup();
            Thread.sleep(4000);
			WebElement secondpage_premium=driver.findElement(By.xpath("//*[@class='tr_premium_val']/span[1]"));
			String secondpage_PremiumValue=secondpage_premium.getText();
			System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
			Assert.assertEquals(Editfirstpage_PremiumValue, secondpage_PremiumValue);
            System.out.println("Second premium value is same as first premium value  : "+secondpage_PremiumValue);
		}else {
			scrolldown();
			Thread.sleep(5000);
			WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]"));
			String BeforeEditfirstpage_PremiumValue=firstpage_Premium.getText();				
			System.out.println("First page premium value is   :"  +BeforeEditfirstpage_PremiumValue);
			jse.executeScript("window.scrollBy(0,150)", "");
			//clickElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[6]/button[2]"));
			waitForElement(By.xpath(posStudent_buynow_xpath));
			clickElement(By.xpath(posStudent_buynow_xpath));
			// Verify premium here
			scrollup();
			System.out.println("Edit is not required");
			
			jse.executeScript("window.scrollBy(0,-350)", "");
            scrollup();
            Thread.sleep(4000);
			WebElement secondpage_premium=driver.findElement(By.xpath("//*[@class='tr_premium_val']/span[1]"));
			String secondpage_PremiumValue=secondpage_premium.getText();
			System.out.println("Second Page Premium value is  : "   +    secondpage_PremiumValue);
			Assert.assertEquals(BeforeEditfirstpage_PremiumValue, secondpage_PremiumValue);
            System.out.println("Second premium value is same as first premium value");
      
            logger.log(LogStatus.INFO,"Quotaion Premium and Proposal Premium is Verified and Both are Same without Edit : "+ secondpage_PremiumValue     +BeforeEditfirstpage_PremiumValue);
      
		}
		
	}
	public static void GroupStudentExploreproposerDetails() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		Thread.sleep(5000);
   		String nationality=TestCaseData[gse.n][12].toString().trim();
   		System.out.println("Nationality is  : "  +nationality);
   		logger.log(LogStatus.PASS, "Selected nationality is : " + nationality);
        scrollup();
   		clickElement(By.xpath("//select[@ng-model='formParams.citizenshipCd']"));
   		clickElement(By.xpath("//option[@ng-repeat='data in nationalityData'][contains(text(),"+"'"+nationality+"'"+")]"));
   		System.out.println("Entered Nationality is : " +nationality);

   		String Passport = TestCaseData[gse.n][13];
   		//clearTextfield(By.xpath(PassportNumber_xpath));
   		ExplicitWait1(By.xpath(PassportNumber_xpath));
   		enterText(By.xpath(PassportNumber_xpath), Passport);
   		Thread.sleep(2000);
   		System.out.println("Entered Passport Number : "+Passport);
   		logger.log(LogStatus.PASS, "Entered Passport Number : "+Passport);
   		String Title = TestCaseData[gse.n][14].toString().trim();
   		clickElement(By.xpath("//select[@name='ValidTitle']"));
   		clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text(),"+"'"+Title+"'"+")]"));
   		Thread.sleep(3000);
   		System.out.println("Entered Title is : " +Title);
   		logger.log(LogStatus.PASS, "slected title is : "+Title);
   		BaseClass.selecttext("ValidTitle", Title.toString());

   		Thread.sleep(3000);
   		String FirstName = TestCaseData[gse.n][15];
   		clearTextfield(By.xpath(FirstName_xpath));
   		enterText(By.xpath(FirstName_xpath), FirstName);
   		System.out.println("Entered First Name is : " +FirstName);
   		logger.log(LogStatus.PASS, "Entered first name is : "+FirstName);
   		String LastName = TestCaseData[gse.n][16];
   		clearTextfield(By.xpath(LastName_xpath));
   		enterText(By.xpath(LastName_xpath), LastName);
   		System.out.println("Entered Last Name is : "+LastName);
   		logger.log(LogStatus.PASS, "Entered last name is  : "+LastName);
   		// Entering DOB from Excel into dob field

   		String DOB = TestCaseData[gse.n][17].toString().trim();
   		System.out.println("date is:" + DOB);

   		try{
   			clickElement(By.xpath(DOBCalender_xpath));
   			clearTextfield(By.xpath(DOBCalender_xpath));
   			enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
   			driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

   			System.out.println("Entered Proposer DOB is : "+ DOB);
   			Thread.sleep(3000);
   		}
   		catch(Exception e)
   		{
   			System.out.println("Unable to Enter Date of Birth Date.");
   		}

   		logger.log(LogStatus.PASS, "Selected date of birth is  : "+DOB);

   		final String address1 = TestCaseData[gse.n][18].toString().trim();
   		System.out.println("Adress1 name is :" + address1);
   		clearTextfield(By.xpath(addressline1_xpath));
   		enterText(By.xpath(addressline1_xpath), address1);
   		clearTextfield(By.xpath(addressline2_xpath));
   		enterText(By.xpath(addressline2_xpath), TestCaseData[gse.n][19].toString().trim());
   		clearTextfield(By.xpath(pincode_xpath));
   		enterText(By.xpath(pincode_xpath), TestCaseData[gse.n][20]);
   		logger.log(LogStatus.PASS, "Entered address one  is  : "+address1);
   		logger.log(LogStatus.PASS, "Entered address two is  : "+TestCaseData[gse.n][20].toString().trim());
   		logger.log(LogStatus.PASS, "Entered pincode is  : "+TestCaseData[gse.n][20]);
   		String NomineeName = TestCaseData[gse.n][21].toString().trim();
   		System.out.println("Nominee name   is:" + NomineeName);
   		driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
   		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
   		logger.log(LogStatus.PASS, "Entered NomineeName is   : "+TestCaseData[gse.n][21]);
   		Actions action = new Actions(driver);
   		action.sendKeys(Keys.ESCAPE);
   		Thread.sleep(2000);
   		
   		String nominiRelation=TestCaseData[gse.n][22];
   		clickElement(By.xpath(nomineRelation_xpath));
   		Thread.sleep(2000);
   		driver.findElement(By.xpath("//select[@name='nomineeRelation']/option[contains(text(),"+"'"+nominiRelation+"'"+")]")).click();
   		// driver.findElement(By.xpath("//*[@id=\"tr_details_full_cont\"]/div[1]/div[2]/div[3]/div[4]/div/select/option"));
   		System.out.println("Selected nominee relation is : "+nominiRelation);
   		logger.log(LogStatus.PASS, "Selected nominee relation is  : "+nominiRelation);
   		String PurposeofVisit = TestCaseData[gse.n][23];
   		clickElement(By.xpath(PurposeofVisit_xpath));
   		Thread.sleep(2000);
   		driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"+"'"+PurposeofVisit+"'"+")]")).click();
   		System.out.println("Selected Purpose of Visit is : "+PurposeofVisit);
   		logger.log(LogStatus.PASS, "Selected Purpose of Visit is : "+PurposeofVisit);
   		
   		
   		// Nominee Relation
   	//Trip start date
   		String TripstartDate=TestCaseData[gse.n][24];
	
		try{
			Thread.sleep(4000);
				clickElement(By.xpath("//*[@name='trip_start_date']"));
				Thread.sleep(2000);
				String spilitter[]=TripstartDate.split(",");			
				String eday = spilitter[0];
				String emonth = spilitter[1];
				String eYear = spilitter[2];
				String oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				String oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				WebElement Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next Button
				while(!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth))))
				  {
					Thread.sleep(2000);
				    Next.click();
				    oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				    oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				    Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
				  }
				driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+eday+"'"+")]")).click();
				System.out.println("Entered Start_Date on Faveo UI is :"+TripstartDate);	
				}
				catch(Exception e)
				{
					System.out.println("Unable to Enter Start Date.");
				}
		Thread.sleep(5000);
		Actions actionpos=new Actions(driver);
		actionpos.sendKeys(Keys.ESCAPE);
		
	}
	public static void GroupStudentExploreInsuredDetails() throws Exception {
		//*******************insured details start here*******************************
		String[][] StudentExploreInsuredDetails = BaseClass.excel_Files("GroupStudentInsuredDetails");

		
		String Relation=StudentExploreInsuredDetails[gse.n][0].toString().trim();

		//String Relation2=StudentExplore_InsuredDetails[1][0].toString().trim();

		String passport=StudentExploreInsuredDetails[gse.n][1].toString().trim();

		String title=StudentExploreInsuredDetails[gse.n][2].toString().trim();

		String Fname=StudentExploreInsuredDetails[gse.n][3].toString().trim();

		String Lname=StudentExploreInsuredDetails[gse.n][4].toString().trim();

		String Dob=StudentExploreInsuredDetails[gse.n][5].toString().trim();
		
		List<WebElement>Rel=driver.findElements(By.xpath("//select[@name='ValidRelation0']/option"));
		int size1=Rel.size();
		System.out.println("List size is : "+size1);
		for(WebElement Relations:Rel) {
			if(Relations.getText().contains("Self-primary")&&Relation.contains("Self-primary")) {
				Relations.click();
				System.out.println("Selected relation is :"  +Relation);
				Actions action1 = new Actions(driver);
				action1.sendKeys(Keys.ESCAPE);
				break;
			}else if ((!Relations.getText().contains("Self-primary")) && Relations.getText().contains(Relation)) {
				Relations.click();
				driver.findElement(By.xpath("//*[@name='rel_passport0']")).sendKeys(passport);
				clickElement(By.xpath("//*[@id='ValidRelTitle0']"));
				Thread.sleep(3000);

				BaseClass.selecttext("ValidRelTitle0", title.toString());
				driver.findElement(By.xpath("//*[@id='RelFName0']")).clear();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='RelFName0']")).sendKeys(Fname);
				driver.findElement(By.xpath("//*[@id='RelLName0']")).clear();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='RelLName0']")).sendKeys(Lname);
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@name='rel_dob0']")).clear();
				driver.findElement(By.xpath("//*[@name='rel_dob0']")).click();
				Thread.sleep(3000);
				enterText(By.name("rel_dob0"), String.valueOf(Dob));

				Thread.sleep(3000);
				break;
			}

		}
	}
	public static void GroupstudentExploreEducationDetails() throws Exception {
		
		String[][] StudentEducationDetails = BaseClass.excel_Files("GroupstudentExploreEducation");
		//String InstitutionName=StudentEducationDetails[gse.n][0].toString().trim();
		String CourseName=StudentEducationDetails[gse.n][1].toString().trim();
		String InstituteAddress=StudentEducationDetails[gse.n][2].toString().trim();
		String Country=StudentEducationDetails[gse.n][3].toString().trim();
		System.out.println("Excel values are :"  +CourseName +"," +InstituteAddress +"," +Country);
		logger.log(LogStatus.PASS, "Selected course is  : "+CourseName);
		logger.log(LogStatus.PASS, "Entered institute is  : "+InstituteAddress);
		logger.log(LogStatus.PASS, "Selecte country is  : "+Country);
		

		/*List<WebElement> Textbox=driver.findElements(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[8]/div[2]/div"));
		int totaltextbox=Textbox.size();
		System.out.println("Text box size is :"+totaltextbox );
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;

		jse1.executeScript("window.scrollBy(0,150)", "");

		int a=0;
		for(WebElement textdata : Textbox ) {
			String InstitutionName=StudentEducationDetails[gse.n][a].toString().trim();
			System.out.println("Selected text data is : "+InstitutionName);
			textdata.click();
			textdata.sendKeys(InstitutionName);
			a++;
		}
		*/
		List<WebElement> Textbox=driver.findElements(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[8]/descendant::div[2]/div//input"));
		int totaltextbox=Textbox.size();
		System.out.println("Text box size is :"+totaltextbox );
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;

		jse1.executeScript("window.scrollBy(0,150)", "");

		int a=0;
		for(WebElement textdata : Textbox ) {
			String InstitutionName=StudentEducationDetails[gse.n][a].toString().trim();
			System.out.println("Selected text data is : "+InstitutionName);
			textdata.sendKeys(InstitutionName);
			a++;
		}

		//******************Sponcer details here*****************************
	
	String SponserName=StudentEducationDetails[gse.n][4].toString().trim();
	String SponserDOB=StudentEducationDetails[gse.n][5].toString().trim();
	String SponserRelation=StudentEducationDetails[gse.n][6].toString().trim();
	logger.log(LogStatus.PASS, "Selected Sponser name is  : "+SponserName);
	logger.log(LogStatus.PASS, "Selected Sponser date of birth  is  : "+SponserDOB);
	logger.log(LogStatus.PASS, "Selecte Sponser Relation is  : "+SponserRelation);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,200)");
	driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[1]/div[8]/descendant::div[8]/div/div//p/input")).sendKeys(SponserName);
	driver.findElement(By.xpath("//*[@name='sponsor_dob']")).click();
	Thread.sleep(3000);
	enterText(By.name("sponsor_dob"), String.valueOf(SponserDOB));
	Thread.sleep(3000);
	
	clickElement(By.xpath("//*[@name='sponsorRelation']"));
	//  Thread.sleep(5000);
	driver.findElement(By.xpath("//select[@name='sponsorRelation']/option[contains(text(),"+"'"+SponserRelation+"'"+")]")).click();
	// driver.findElement(By.xpath("//*[@id=\"tr_details_full_cont\"]/div[1]/div[2]/div[3]/div[4]/div/select/option"));
	System.out.println("Selected nominee relation is : "+SponserRelation);
	Thread.sleep(3000);
	Actions acti = new Actions(driver);
	acti.sendKeys(Keys.ESCAPE);
	}
	public static void GroupStudentExploreQuestionset() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("GroupStudentExplore_Quotation");
		System.out.println("****************Student Explore Question part start here*********************");	
		String[][] StudentExploreQuestions=BaseClass.excel_Files("GroupStudentExploQuestionset");

		WebElement pedtext=driver.findElement(By.xpath("//*[@class='col-md-12 padding0']/div[4]/p/span[2]"));
		System.out.println("PED text is :  "   +pedtext.getText());
		if(pedtext.equals("No")) {

			System.out.println("Not required to select any questions ");


		}else if(pedtext.getText().equals("Yes")){
			String preExistingdeases = TestCaseData[gse.n][26].toString().trim();
			logger.log(LogStatus.PASS, "Selected pre-Existing diseases is  : "+preExistingdeases);
			System.out.println("Q1. Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[@ng-checked='ped.question_1 == true']")).click();
			int i=0;
			clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']"));
			clickElement(By.xpath("//select[@id='qs_pedYesNo_"+i+"']//option[@value='YES']"));

			List<WebElement> checkbox_list=driver.findElements(By.xpath("//input[@type='checkbox' and contains(@id,'qs_')]"));
			int total=checkbox_list.size();

			for(WebElement checkboxclick:checkbox_list) {
				checkboxclick.click();

			}
			String reason =StudentExploreQuestions[gse.n][8].toString();
			logger.log(LogStatus.PASS, "Entered reason  is  : "+reason);
			driver.findElement(By.xpath("//*[@id='otherDiseasesDescription_0']")).sendKeys(reason);

			//Question two selection
			String QuestionNumber3 = StudentExploreQuestions[gse.n][8].toString().trim();
			String QuestionNumber3Details = StudentExploreQuestions[gse.n][9].toString().trim();
			System.out.println("Q3. Have you ever claimed under any travel policy? : Yes");				
			System.out.println("Member claimed under any travel policy? : "+QuestionNumber3);
			driver.findElement(By.xpath("//input[@ng-checked='ped.question_2 == true']")).click();

			//*[@id="qs_T001_0"]/option[2]
			driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']")).click();
			driver.findElement(By.xpath("//select[@id='qs_T001_"+i+"']//option[contains(text(),'Yes')]")).click();

			WebElement reasontextbox=driver.findElement(By.xpath("//*[@name='qs_T001Desc_"+i+"']"));
			reasontextbox.clear();
			//String reason =StudentExploreQuestion[1][8].toString();
			reasontextbox.sendKeys(reason);

		}
		
	}
	public static void GroupStudentExplorePaymentAndDataValidations() throws Exception {
		System.out.println("******************************************Student Explore payment start here*****************************************");
		
		String[][] TestCase=BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] Environmentdetails=BaseClass.excel_Files("Credentials");
		String Envionment=Environmentdetails[3][1].toString().trim();
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("window.scrollBy(0,400)");
		driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
		driver.findElement(By.xpath("//input[@id='tripStart']")).click();

		//  js.executeScript("window.scrollBy(0,400)");
		Thread.sleep(2000);
		driver.findElement(By.name("optCovers")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
		waitForElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));
		//clickElement(By.xpath(pay_online_xpath));
		clickElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));

		
		BaseClass.PayuPage_Credentials();

		BaseClass.scrolldown();
		 String expectedTitle = "Your payment transaction is successful !";
			Fluentwait(By.xpath("//p[@class='your_payment_sucess_p ng-binding']"));
		     //String actualTitle = driver.findElement(By.xpath(ExpectedMessage_xpath)).getText();
			String actualTitle = driver.findElement(By.xpath("//p[@class='your_payment_sucess_p ng-binding']")).getText();
				try{
					Assert.assertEquals(expectedTitle,actualTitle);
			          logger.log(LogStatus.INFO, actualTitle);
			     }catch(AssertionError e){
			          System.out.println("Payment Failed");
			          String FoundError = driver.findElement(By.xpath("/html/body/div[2]/div[21]/div[1]/div/div[1]/div[1]/div/div[2]/p")).getText();
			          logger.log(LogStatus.FAIL, "Test Case is Failed Because getting " + FoundError);
			          TestResult="Fail";
			     }

				
		WebElement Policy=driver.findElement(By.xpath("//*[@class='col-md-12']/div/div[1]/div[1]/p[2]"));
		String policy_Number=Policy.getText();
		System.out.println("Policy number is  :"   +policy_Number);
		
		Thread.sleep(5000);
	
		 String ProposerName=driver.findElement(By.xpath("//div[@class='col-md-5 person_dob_cont ng-scope']/div[2]/div[1]/div/div/p")).getText();
		    System.out.println("Proposer Name : "+ProposerName);
		    
		    
			String Thankyoupagepremium_value=driver.findElement(By.xpath("//p[@class='premium_amount ng-binding']")).getText();
			System.out.println("Total premium value is:"+Thankyoupagepremium_value);
			
	
	
			
		//	BaseClass.DBVerification(policy_Number);
String ThankyoupageProposal_Pol_num;
			String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
			if(Envionment.equals("UAT")) {
		ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp_xpath)).getText();
			System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
			}else {
				ThankyoupageProposal_Pol_num = driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/p[2]")).getText();
				System.out.println("Proposal / Policy Num = " + ThankyoupageProposal_Pol_num);
			}
			
			if (ProposalSummary.contains("Policy No.")) {
				Thread.sleep(1000);
				clickElement(By.xpath(Downloadpdf_Travel_Thankyou_xpath));
				Thread.sleep(15000);
				try {
				
					try{
					String UserName = System.getProperty("user.home");
					System.out.println(UserName);

					File file = new File(UserName + "\\Downloads\\" + ThankyoupageProposal_Pol_num + ".pdf");
					
					FileInputStream input = new FileInputStream(file);

					PDFParser parser = new PDFParser(input);

					parser.parse();

					COSDocument cosDoc = parser.getDocument();

					PDDocument pdDoc = new PDDocument(cosDoc);

					PDFTextStripper strip = new PDFTextStripper();
					
					strip.setStartPage(1);
					strip.setEndPage(3);

					String studentdata = strip.getText(pdDoc);

					//System.out.println(data);
					String data=TestCase[gse.n][0].toString().trim();
					System.out.println(data);
					
					Assert.assertTrue(data.contains(ThankyoupageProposal_Pol_num));
					//Assert.assertTrue(data.contains(FinalAmount));
					
					cosDoc.close();
					pdDoc.close();
					System.out.println("Text Found on the pdf File...");
					logger.log(LogStatus.INFO, "Policy Number, Name, Premium Amount  are Matching in pdf");
					TestResult = "Pass";

					}catch(Exception e)
					{
						String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
						System.out.println(ErrorMessage);
						logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
						TestResult = "Fail";
					}


				} catch (Exception e) {

					String ErrorMessage = driver.findElement(By.xpath("/html/body/div[2]/div")).getText();
					System.out.println(ErrorMessage);
					logger.log(LogStatus.FAIL,"Test Case is Failed beacuse getting Error: Due to some technical error your Policy PDF is not downloaded. Kindly try again.");
					TestResult = "Fail";
				}
				
				


			} else if (ProposalSummary.contains("Application No.")) {
				System.out.println(ThankyoupageProposal_Pol_num);
				try {
					Assert.assertEquals(policy_Number, ThankyoupageProposal_Pol_num);
					TestResult = "Pass";
					logger.log(LogStatus.INFO,"Proposal number on Payu page and Thankyour Page is Verified and Both are Same");
				} catch (AssertionError e) {
					System.out.println(policy_Number + " - failed");
					TestResult = "Fail";
					logger.log(LogStatus.INFO, "Proposal number on Payu page and Thankyour Page are not Same");
					throw e;
				}
			}
			WriteExcel.setCellData1("GroupStudentExplore_Testcase", TestResult, ThankyoupageProposal_Pol_num, gse.n,2, 3);
	}
}
