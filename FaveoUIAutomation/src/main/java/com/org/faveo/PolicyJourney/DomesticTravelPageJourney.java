package com.org.faveo.PolicyJourney;



import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.faveo.edit.Edit;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.TravelInsuranceDropDown;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.travelInsurance.DomesticTravel;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class DomesticTravelPageJourney extends BaseClass implements AccountnSettingsInterface {
	public static String proposalSummarypremium_value=null;
	public static String ProposalPremimPage_Value=null;
	public static DomesticTravel DT=new DomesticTravel();
public static void DomesticTravelQuotationdetails() throws Exception {
	
	String[][] TestCase=BaseClass.excel_Files("DomesticTravel_Testcase");
	String[][] TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");

	String triptype=TestCaseData[DT.n][1].toString().trim();
	String terms=TestCaseData[DT.n][2].toString().trim();
	System.out.println("=========Selecteing Trip Type here=============================");
	
	try {
		clickElement(By.xpath("//*[@class='dropdown year_drop_slect master']/a[contains(text(),'Round Trip')]"));
	} catch (Exception e1) {
		// TODO Auto-generated catch block
	System.out.println("Unable to click on Trip type");
	}
List<WebElement> triptypeele=driver.findElements(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a"));
for(WebElement triptyp:triptypeele) {
	System.out.println("Trip type context is : "+triptyp);
	if(triptyp.getText().equals(triptype)) {
		triptyp.click();
		System.out.println("Selected trip type is :"+triptype);
		break;
	}
  }
System.out.println("+++++++++++++Selecting Terms value here+++++++++++++++++++++++++++");
if(triptype.equals("One Way Trip")||triptype.equals("Round Trip")||triptype.equals("Annual Trip 365 days")) {
	clickElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[1]/div/div/div[2]/div/div/ui-dropdown/div/div/a"));
	List<WebElement>termsvalue=driver.findElements(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a"));
	for(WebElement termvalue:termsvalue) {
		System.out.println("Terms values are : "+termvalue.getText());
		if(termvalue.getText().equals(terms)) {
			termvalue.click();
			break;
		}
	}
	Thread.sleep(5000);
System.out.println("+++++++++++++++++Selecting Start Date here+++++++++++++++++++");
	String StartDate = TestCaseData[DT.n][3].toString().trim();		
	try{
		Thread.sleep(2000);
			clickElement(By.xpath(StartDateCalander_Id));
			Thread.sleep(2000);
			String spilitter[]=StartDate.split(",");			
			String eday = spilitter[0];
			String emonth = spilitter[1];
			String eYear = spilitter[2];
			String oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			String oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			WebElement Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next Button
			while(!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth))))
			  {
				Thread.sleep(2000);
			    Next.click();
			    oMonth=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			    oYear=driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			    Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
			  }
			driver.findElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+eday+"'"+")]")).click();
			System.out.println("Entered Start_Date on Faveo UI is :"+StartDate);	
		
			}
			catch(Exception e)
			{
				System.out.println("Unable to Enter Start Date.");
			}
	Actions action = new Actions(driver);
	action.sendKeys(Keys.ESCAPE);
	System.out.println("+++++++++++++++++++++++Transport Type selecting here++++++++++++++++++++++++++");
	String TransportTypee = TestCaseData[DT.n][4].toString().trim();
if(triptype.equals("One Way Trip")) {
	clickElement(By.xpath("//*[@class='col-md-12 padding0 tr_travl_date_cont ng-scope']/div/div[2]/div"));
	
	List<WebElement>transporttype=driver.findElements(By.xpath("//*[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a"));
	for(WebElement type:transporttype) {
		if(type.getText().equals(TransportTypee)) {
			type.click();
			break;
		}else {
			System.out.println("Trip type is not a one way trip");
		}
	}
	
	}else {
		System.out.println("Trip type is not a one way trip");
	}
	System.out.println("+++++++++++++++++++++++Ped Value selecteing here++++++++++++++++++++++++++");
	String PedValue=TestCaseData[DT.n][5].toString().trim();
	WebElement pedtext=driver.findElement(By.xpath("//*[@class='dropdown year_drop_slect master']/a[contains(text(),'No')]"));
String 	pedtype=pedtext.getText();
System.out.println("Ped type value from dropdown is :"+pedtype);
if(PedValue.equals(pedtype)) {
	System.out.println("No need to select or click on ped dropdown because it is a functionality of Domestic Travel");


}else {
	System.out.println("This functionality may be changed in future");
}
System.out.println("+++++++++++++++++++++++Travellers value selecting here++++++++++++++++++++++++++");
String Travelr=TestCaseData[DT.n][6].toString().trim();
clickElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[3]/div/div[2]/div/ui-dropdown/div/div/a"));
List<WebElement>travel=driver.findElements(By.xpath("//*[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a"));

for(WebElement travelerco:travel) {
	if(travelerco.getText().equals(Travelr)) {
		travelerco.click();
		System.out.println("Selected travellers details is :"+Travelr);
		break;
	}
}

System.out.println("+++++++++++++++++++++++++++++++++++Travellers Age Selecting here+++++++++++++++++++++++++++++++++++++++");
String agelimit=TestCaseData[DT.n][7].toString().trim();
clickElement(By.xpath("//*[@id='travellers_num1']/div/div/li/a"));
List<WebElement>travlage=driver.findElements(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year q_dMenue show']/li/a"));
for(WebElement travellerage:travlage) {
	
	if(travellerage.getText().equals(agelimit)) {
		travellerage.click();
		System.out.println("Selected Age up traveller is : "+agelimit);
	break;
	}
	
}
System.out.println("+++++++++++++++++++++++++++++++++++++++Mobile number and Email+++++++++++++++++++++++++++++++++++++++++++++++++++");

String mobilenumber=TestCaseData[DT.n][8].toString().trim();
String email=TestCaseData[DT.n][9].toString().trim();
System.out.println("Email is :" + email);

logger.log(LogStatus.PASS, "Entered Email id: " + email);
int size = mobilenumber.length();
System.out.println("mobile number is: " + mobilenumber);
String format = "^[789]\\d{9}$";

Thread.sleep(4000);

if (mobilenumber.matches(format) && size == 10) {
	driver.findElement(By.name("mobileNumber")).sendKeys(mobilenumber);
} else {
	System.out.println(" Not a valid mobile  Number");
}

Thread.sleep(4000);
if (email.contains("@")) {
	driver.findElement(By.name("ValidEmail")).sendKeys(email);
} else {
	System.out.println("Not a valid email");
}
System.out.println("===============================SumINsured Selecting here=========================");
Thread.sleep(3000);
WebElement progress= driver.findElement(By.xpath("//*[@id='tr_quotation_full_cont']/div[2]/form/div/div/div/div[6]/div/div/div"));
String suminsured=TestCaseData[DT.n][10].toString().trim();
List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
int total_size=Slidnumber.size();
System.out.println("Slider numbers are :  " +total_size );
String SliderValue=null;
for(WebElement Slider:Slidnumber) {
SliderValue=suminsured;

	Thread.sleep(3000);
	if(Slider.getText().equals(SliderValue.toString())) {
		Thread.sleep(5000);
	
		Slider.click();
		break;
	}
}
	
}
}
public static void DomesticTravelPremium() throws Exception {
	String[][] TestCase=BaseClass.excel_Files("DomesticTravel_Testcase");
	String executionStatus=TestCase[1][4].toString().trim();
	if(!executionStatus.equals("Edit")) {
	scrolldown();
	Thread.sleep(5000);
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	WebElement firstpage_Premium=driver.findElement(By.xpath("//*[@class='input_year your_premium_cont']/div/div/label[2]"));
	String firstpage_PremiumValue=firstpage_Premium.getText();				
	System.out.println("First page premium value is   :"  +firstpage_PremiumValue);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,150)", "");
	try {
	clickElement(By.xpath("//*[@class='col-md-12 padding0 tr_no_btn text-center']/button[contains(text(),'Buy Now')]"));
	}catch(Exception e){
		System.out.println("Unable to click on buy now button");
	}
	jse.executeScript("window.scrollBy(0,-350)", "");
    scrollup();
    Thread.sleep(4000);
	WebElement secondpage_premium=driver.findElement(By.xpath("//*[@class='tr_premium_val']/span[1]"));
	ProposalPremimPage_Value=secondpage_premium.getText();
	System.out.println("Second Page Premium value is  : "   +    ProposalPremimPage_Value);
	Assert.assertEquals(firstpage_PremiumValue, ProposalPremimPage_Value);
    System.out.println("Second page premium value is same as first page premium value");

    logger.log(LogStatus.INFO,"Quotaion Premium and Proposal Premium is Verified and Both are Same : "+ ProposalPremimPage_Value     +firstpage_PremiumValue);
	}else {
		Edit.DomesticTravelEdit();
	}
}
public static void DomesticTravelproposerDetails() throws Exception {
	String[][] TestCase=BaseClass.excel_Files("DomesticTravel_Testcase");
	String[][] TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");
	String[][] proposerdetails=BaseClass.excel_Files("DomesticTravel_proposerInsuredD");
	
	String Title = proposerdetails[DT.n][1].toString().trim();
		clickElement(By.xpath("//select[@name='ValidTitle']"));
		clickElement(By.xpath("//select[@ng-model='formParams.ValidTitle']/option[contains(text(),"+"'"+Title+"'"+")]"));
		Thread.sleep(3000);
		System.out.println("Entered Title is : " +Title);
		logger.log(LogStatus.PASS, "slected title is : "+Title);
		BaseClass.selecttext("ValidTitle", Title.toString());

		String FirstName = proposerdetails[DT.n][2];
		clearTextfield(By.xpath(FirstName_xpath));
		enterText(By.xpath(FirstName_xpath), FirstName);
		Thread.sleep(3000);
		System.out.println("Entered First Name is : " +FirstName);
		logger.log(LogStatus.PASS, "Entered first name is : "+FirstName);
		String LastName = proposerdetails[DT.n][3];
		clearTextfield(By.xpath(LastName_xpath));
		enterText(By.xpath(LastName_xpath), LastName);
		System.out.println("Entered Last Name is : "+LastName);
		logger.log(LogStatus.PASS, "Entered last name is  : "+LastName);
		String DOB = proposerdetails[DT.n][4].toString().trim();
   		System.out.println("date is:" + DOB);

   		try{
   			clickElement(By.xpath(DOBCalender_xpath));
   			clearTextfield(By.xpath(DOBCalender_xpath));
   			enterText(By.xpath(DOBCalender_xpath), String.valueOf(DOB));
   			driver.findElement(By.xpath(DOBCalender_xpath)).sendKeys(Keys.ESCAPE);

   			System.out.println("Entered Proposer DOB is : "+ DOB);
   			Thread.sleep(3000);
   		}
   		catch(Exception e)
   		{
   			System.out.println("Unable to Enter Date of Birth Date.");
   		}

   		logger.log(LogStatus.PASS, "Selected date of birth is  : "+DOB);

   		final String address1 = proposerdetails[DT.n][5].toString().trim();
   		System.out.println("Adress1 name is :" + address1);
   		clearTextfield(By.xpath(addressline1_xpath));
   		enterText(By.xpath(addressline1_xpath), address1);
   		clearTextfield(By.xpath(addressline2_xpath));
   		enterText(By.xpath(addressline2_xpath), proposerdetails[DT.n][6].toString().trim());
   		clearTextfield(By.xpath(pincode_xpath));
   		enterText(By.xpath(pincode_xpath), proposerdetails[DT.n][7].toString().trim());
   		logger.log(LogStatus.PASS, "Entered address one ,two,pincode  is  : "+address1 +proposerdetails[DT.n][6].toString().trim()  +proposerdetails[DT.n][7].toString().trim());

   		String NomineeName = proposerdetails[DT.n][8].toString().trim();
   		System.out.println("Nominee name   is:" + NomineeName);
   		driver.findElement(By.xpath("//input[@placeholder='Nominee Name']")).clear();
   		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
   		logger.log(LogStatus.PASS, "Entered NomineeName is   : "+proposerdetails[DT.n][8]);
   		Actions action = new Actions(driver);
   		action.sendKeys(Keys.ESCAPE);
   		String PurposeofVisit = proposerdetails[DT.n][9];
   		clickElement(By.xpath(PurposeofVisit_xpath));
   		Thread.sleep(2000);
   		driver.findElement(By.xpath("//select[@ng-model='formParams.visitPurposeCd']/option[contains(text(),"+"'"+PurposeofVisit+"'"+")]")).click();
   		System.out.println("Selected Purpose of Visit is : "+PurposeofVisit);
   		logger.log(LogStatus.PASS, "Selected Purpose of Visit is : "+PurposeofVisit);
   		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
   		
}
public static void DomesticTravelInsuredDetails() throws Exception {
	String[][] TestCase=BaseClass.excel_Files("DomesticTravel_Testcase");
	String[][] TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");
	String[][] proposerdetails=BaseClass.excel_Files("DomesticTravel_proposerInsuredD");
	String triptypedetails= TestCaseData[DT.n][1].toString().trim();
	String relation=proposerdetails[DT.n][10].toString().trim();
	List<WebElement>Rel=driver.findElements(By.xpath("//select[@name='ValidRelation0']/option"));
	int size1=Rel.size();
	System.out.println("List size is : "+size1);
	for(WebElement Relations:Rel) {
		if(Relations.getText().contains("Self-primary")&&relation.contains("Self-primary")) {
			Relations.click();
			System.out.println("Selected relation is :"  +relation);
			Actions action1 = new Actions(driver);
			action1.sendKeys(Keys.ESCAPE);
			break;
		}else {
			System.out.println("Unable to select relation");
		}
	
   }
	Thread.sleep(4000);
	String pnr=TestCaseData[1][11].toString().trim();
	String SourceStation=TestCaseData[1][12].toString().trim();
	String DestinationStation=TestCaseData[1][13].toString().trim();
	if(triptypedetails.equals("One Way Trip")) {
		
	enterText(By.xpath("//*[@name='pnrNum']"), pnr);
	
	enterText(By.xpath("//*[@name='journeySorce']"), SourceStation);
	enterText(By.xpath("//*[@name='journeyDestination']"), DestinationStation);
		
		}else {
			System.out.println("Trip type is not a one way trip");
		}
	JavascriptExecutor js1 = (JavascriptExecutor) driver;
	js1.executeScript("window.scrollBy(0,400)");
	driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
	driver.findElement(By.xpath("//input[@id='tripStart']")).click();
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
	/*waitForElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));
	//clickElement(By.xpath(pay_online_xpath));
	clickElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));*/
}


public static void DomesticTravelInsuredDetails2() throws Exception {
	String[][] TestCase=BaseClass.excel_Files("DomesticTravel_Testcase");
	String[][] TestCaseData=BaseClass.excel_Files("DomesticTravel_quotation");
	String[][] proposerdetails=BaseClass.excel_Files("DomesticTravel_proposerInsuredD");
	String triptypedetails= TestCaseData[DT.n][1].toString().trim();
	String relation=proposerdetails[DT.n][10].toString().trim();
	List<WebElement>Rel=driver.findElements(By.xpath("//select[@name='ValidRelation0']/option"));
	int size1=Rel.size();
	System.out.println("List size is : "+size1);
	for(WebElement Relations:Rel) {
		if(Relations.getText().contains("Self-primary")&&relation.contains("Self-primary")) {
			Relations.click();
			System.out.println("Selected relation is :"  +relation);
			Actions action1 = new Actions(driver);
			action1.sendKeys(Keys.ESCAPE);
			break;
		}else {
			System.out.println("Unable to select relation");
		}
	
   }
	Thread.sleep(4000);
	String pnr=TestCaseData[1][11].toString().trim();
	String SourceStation=TestCaseData[1][12].toString().trim();
	String DestinationStation=TestCaseData[1][13].toString().trim();
	if(triptypedetails.equals("One Way Trip")) {
		
	enterText(By.xpath("//*[@name='pnrNum']"), pnr);
	
	enterText(By.xpath("//*[@name='journeySorce']"), SourceStation);
	enterText(By.xpath("//*[@name='journeyDestination']"), DestinationStation);
		
		}else {
			System.out.println("Trip type is not a one way trip");
		}
	JavascriptExecutor js1 = (JavascriptExecutor) driver;
	js1.executeScript("window.scrollBy(0,400)");
	driver.findElement(By.xpath("//input[@id='termsCheckbox1']")).click();
	driver.findElement(By.xpath("//input[@id='tripStart']")).click();
	//logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	//driver.findElement(By.xpath("//*[@id='tr_details_full_cont']/div[3]/button[2]")).click();
	/*waitForElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));
	//clickElement(By.xpath(pay_online_xpath));
	clickElement(By.xpath("//ul[@class='nav nav-tabs text-center nav_nav_tabs_child']/li/a[contains(text(),'PAY ONLINE')]"));*/
}

public static void DomesticTravelProposalSummaryDetailsWithPayment() throws Exception {

	
	Thread.sleep(12000);
	QuotationandProposalVerification.VerifyPremiumIncrease_On_ProposalsumarryPageDomesticTravel();
	(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
	
	waitForElement(By.xpath(payu_proposalnum_xpath));
	String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
	System.out.println("Pay U Proposal Number : "+PayuProposalNum);
	
	waitForElement(By.xpath(payuAmount_xpath));
	String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
	String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
	System.out.println("Pay U Premium Amount : "+FinalAmount);
	//PayUPaymentFn.PayuPage_Credentials();
	BaseClass.PayuPage_Credentials();

	
	
	QuotationandProposalVerification.DomesticTravelThankyouPagemessageVerification();
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
	BaseClass.scrolldown();
	
	String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div/p")).getText();
	System.out.println(ProposerName);
	
	//DataVerificationinDb.DBVerification(PayuProposalNum);
	waitForElement(By.xpath(PolProp2_xpath));
	
	String ProposalSummary = driver.findElement(By.xpath("//p[@class='Transaction_number ng-binding']")).getText();
	String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp2_xpath)).getText();
	System.out.println("Proposal summary details is :"+ProposalSummary);
	System.out.println("Thank you page proposal number is :"+ThankyoupageProposal_Pol_num);
	PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);
	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

	WriteExcel.setCellData1("DomesticTravel_Testcase", TestResult, ThankyoupageProposal_Pol_num, 1, 2, 3);
	
System.out.println("Thank you page proposal number is :"+ThankyoupageProposal_Pol_num);
}
}