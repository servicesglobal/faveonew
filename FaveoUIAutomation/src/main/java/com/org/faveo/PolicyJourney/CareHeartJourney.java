package com.org.faveo.PolicyJourney;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.faveo.edit.Edit;
import com.org.faveo.Assertions.PdfMatch;
import com.org.faveo.Assertions.QuotationandProposalVerification;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.healthinsurance.CareHeart;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CareHeartJourney extends BaseClass implements AccountnSettingsInterface{
	public static CareHeart ch=new CareHeart();
	public static String proposalSummarypremium_value=null;
	public static String ProposalPremimPage_Value;

	public static void careheartquotation() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("Care_Heart_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");

		

		//String TestcaseName=(TestCase[ch.n][0].toString().trim() + " - " + TestCase[ch.n][1].toString().trim());
		//logger = extent.startTest("CareWithNCB - " + TestcaseName);
		//System.out.println("Heart tets case name is  - " + TestcaseName);
		
		Thread.sleep(5000);
		driver.findElement(By.name("name")).clear();
		Thread.sleep(2000);
		driver.findElement(By.name("name")).sendKeys(TestCaseData[ch.n][2].toString().trim() + "  " + TestCaseData[ch.n][3].toString().trim());
		logger.log(LogStatus.PASS,"Entered Name is  " + TestCaseData[ch.n][2].toString().trim() + "  " + TestCaseData[ch.n][3].toString().trim());
		System.out.println("Entered Name is  " + TestCaseData[ch.n][2].toString().trim() + "  " + TestCaseData[ch.n][3].toString().trim());

		driver.findElement(By.name("ValidEmail")).clear();

		String email = TestCaseData[ch.n][6].toString().trim();
		System.out.println("Email is :" + email);
		if (email.contains("@")) {
			driver.findElement(By.name("ValidEmail")).sendKeys(email);
		} else {
			System.out.println("Not a valid email");
		}
		logger.log(LogStatus.PASS,"Entered Email id is  "+email );
		Thread.sleep(5000);
		String mnumber = TestCaseData[ch.n][5].toString().trim();
		int size = mnumber.length();
		logger.log(LogStatus.PASS,"Entered Mobile number is  "+mnumber );

		System.out.println("mobile number is: " + mnumber);
		String format = "^[789]\\d{9}$";
		System.out.println("Entered mobile number is : "+mnumber);
		if (mnumber.matches(format) && size == 10) {
			driver.findElement(By.name("mobileNumber")).sendKeys(mnumber);
		} else {
			System.out.println(" Not a valid mobile  Number");
		}

	}
	public static void careheartdp() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Care_Heart_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files("CareHeart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareHeart_Insured_Details");
		//String[][] QuestionSetData = BaseClass.excel_Files("Care_QuestionSet");
		/*CBMainclass NCB=new NCBMainclass();
		Thread.sleep(1000);*/
		Thread.sleep(3000);
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1")) {
					driver.findElement(
							By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["
									+ TestCaseData[ch.n][15].toString().trim() + "]"))
					.click();
					System.out.println(
							"Total Number of Member Selected : " + TestCaseData[ch.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// again call the dropdown
		Fluentwait(By.xpath(
				"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(By.xpath(
				"//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[ch.n][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;

		try {
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String Membersdetails = TestCaseData[ch.n][16];
						if (Membersdetails.contains("")) {

							BaseClass.membres = Membersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By.xpath(
											"//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

									// List Age of members dropdown
									// DropDownName.click();
									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (WebElement ListData : List) {

										if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Selcted Age Of Member :" + ListData.getText());

											ListData.click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												break member;
											}

										}

									}
								}
						}

					} else if (DropDownName.getText().contains("Individual")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[ch.n][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li["
								+ covertype + "]"))
						.click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(5000);
						if (covertype == 2) {
							List<WebElement> dropdowns = driver
									.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) {
								if (DropDowns.getText().contains("Floater")) {
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[ch.n][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println("Total DropDownName Present on Quotation page are : "
											+ DropDowns.getText());
								} else if (DropDowns.getText().equals("18 - 24 years")) {
									// reading members from test cases sheet
									// memberlist
									System.out.println(TestCaseData[ch.n][50].toString().trim());
									int Children = Integer.parseInt(TestCaseData[ch.n][50].toString().trim());
									clickElement(By.xpath(
											"//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/a"));
									clickElement(By.xpath(
											"//*[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
									System.out.println(
											"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
													+ "'" + Children + "'" + ")]");
									String Membersdetails = TestCaseData[ch.n][16];
									if (Membersdetails.contains(",")) {

										BaseClass.membres = Membersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member: for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown

										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText()
													.contains(FamilyData[mcount][0].toString().trim())) {
												System.out
												.println("Age of Eldest Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break outer;
												}

											}

										}
									}

								}
							}
						}
					} else {

						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("18 - 24 years")) {

								String Membersdetails = TestCaseData[ch.n][16];
								if (Membersdetails.contains(",")) {
									BaseClass.membres = Membersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										List<WebElement> List = driver.findElements(
												By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										for (WebElement ListData : List) {

											if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is :" + ListData.getText());
												Thread.sleep(2000);
												ListData.click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													break member;
												}

											}

										}
									}

							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}

	}
	public static void suminsuredCareHeart() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("CareHeart_Quotation");

		int SumInsured = Integer.parseInt(TestCaseData[ch.n][18].toString().trim());
		System.out.println("Suminsured is  : "+SumInsured);
		int Tenure = Integer.parseInt(TestCaseData[ch.n][19].toString().trim());
		   Thread.sleep(3000);
		try{
			clickElement(By.xpath(slider_xpath));
			clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), "+SumInsured+")]"));
			/*Thread.sleep(5000);
			WebElement drag=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[6]/div/div/div"));
			Thread.sleep(5000);
			List<WebElement> dragable=drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
			//String SliderValue=null;

			for(WebElement Slider:dragable) {

				Thread.sleep(5000);
				if(	Slider.getText().equals(SumInsured)) {
					Slider.click();
					break;
				}
			}*/
			System.out.println("Entered Sum Insured is : "+SumInsured+" Lakhs");	
			logger.log(LogStatus.PASS, "Data entered for Sum Insured: " + SumInsured + "Lakhs");
		}
		catch(Exception e)
		{
			System.out.println(e);
			logger.log(LogStatus.FAIL, e);
		}


		int Tenure1=Tenure-1;
		System.out.println("Selected Tenure is : "+Tenure+" Year");
		scrolldown();
		Thread.sleep(2000);
		clickElement(By.xpath("//label[@for='Radio"+Tenure1+"q']//img[@src='assets/img/correct_signal.png']"));
		BaseClass.scrollup();
	}
	public static void careHeartAddon() throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files("CareHeart_Quotation");

		String addon = TestCaseData[ch.n][21].toString().trim();
		if(addon.equalsIgnoreCase("Premium For Care Heart")) {
			clickElement(By.xpath("//label[@for='premiumRadio0q']/b/span[contains(text(),'Premium For Care Heart')]"));
		}else {
			clickElement(By.xpath("//label[@for='premiumRadio1q']/b/span[contains(text(),'Premium With Home Care')]"));
		}
	}
	public static void careheartProposerdetails() throws Exception {

		String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");
		scrollup();
		String Title = TestCaseData[ch.n][1].toString().trim();
		System.out.println("Titel Name is:" + Title);
		Thread.sleep(5000);
		clickElement(By.name("ValidTitle"));
		logger.log(LogStatus.PASS, "Selected Title  is :" + Title);

		BaseClass.selecttext("ValidTitle", Title.toString());

		// Entering DOB from Excel into dob field
		driver.findElement(By.xpath("//*[@id=\"datetimepicker21\"]")).click();
		String DOB = TestCaseData[ch.n][4].toString().trim();
		System.out.println("date is:" + DOB);
		enterText(By.id("proposer_dob"), String.valueOf(DOB));
		//logger.log(LogStatus.PASS, "Entered DOB is :" + DOB);

		Thread.sleep(3000);
		String address1 = TestCaseData[ch.n][7].toString().trim();
		String address2 = TestCaseData[ch.n][8].toString().trim();

		driver.findElement(By.xpath(addressline1_xpath)).sendKeys(address1);
		driver.findElement(By.xpath(addressline2_xpath)).sendKeys(address2);
		driver.findElement(By.xpath(pincode_xpath)).sendKeys(TestCaseData[ch.n][9]);
		

		// Height selection
		String Height = TestCaseData[ch.n][10].toString().trim();
		System.out.println("Height value from excel  is:" + Height);
		clickElement(By.xpath(height_xpath));
		BaseClass.selecttext("heightFeet", Height.toString().trim());
		// Inch Selection
		String Inch = TestCaseData[ch.n][11].toString().trim();
		System.out.println("Inch value from excel  is:" + Inch);
		clickElement(By.xpath(inch_xpath));
		BaseClass.selecttext("heightInches", Inch.toString().trim());
		String Weight = TestCaseData[ch.n][12].toString().trim();
		System.out.println("Weight is :" + Weight);
		enterText(By.xpath(weight_xpath), Weight);
		logger.log(LogStatus.PASS, "Entered Height,Inch and Weight is :" + Height + Inch + Weight);

		String NomineeName = TestCaseData[ch.n][13].toString().trim();
		System.out.println("Nominee name   is:" + NomineeName);
		enterText(By.xpath(Nominee_Name_xpqth), NomineeName);
		logger.log(LogStatus.PASS, "Entered nominee name is :" + NomineeName);

		// Nominee Relation
		String Nrelation = TestCaseData[ch.n][14].toString().trim();
		System.out.println("Nominee  relation from excel  is:" + Nrelation);
		clickElement(By.xpath(Nominee_relation_xpath));
		BaseClass.selecttext("nomineeRelation", Nrelation.toString().trim());
		logger.log(LogStatus.PASS, "Entered nominee relation is :" + Nrelation);

		String pancard = TestCaseData[ch.n][20].toString().trim();

		String pospancard = "KJHYS8977E";
		System.out.println("pancard number is :" + pospancard);
		try {
			driver.findElement(By.xpath("//input[@placeholder='Pan Card']")).sendKeys(pospancard);
		} catch (Exception e) {
			System.out.println("Pan card field not visibled");
		}
		logger.log(LogStatus.PASS, "Entered Pancard number  is :" + pospancard);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		Thread.sleep(7000);
		clickElement(By.xpath(submit_xpath));



	}
	public static void CareHeartInsuredDetails() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("Care_Heart_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files("CareHeart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareHeart_Insured_Details");
		Thread.sleep(3000);
		scrollup();
		int mcount;
		for (int i = 0; i <= BaseClass.membres.length - 1; i++) {
			mcount = Integer.parseInt(BaseClass.membres[i].toString());
			if (i == 0) {
				clickElement(By.xpath(title1_xpath));
				// Select Self Primary
				BaseClass.selecttext("ValidRelation0", FamilyData[mcount][1].toString().trim());
			} else {

				// String firstName= "fname"+i+"_xpath";
				String Date = FamilyData[mcount][5].toString().trim();

				BaseClass.selecttext("ValidRelation" + i, FamilyData[mcount][1].toString().trim());
				// title
				BaseClass.selecttext("ValidRelTitle" + i, FamilyData[mcount][2].toString().trim());
				enterText(By.name("RelFName" + i), FamilyData[mcount][3].toString().trim());
				enterText(By.name("RelLName" + i), FamilyData[mcount][4].toString().trim());
				clickElement(By.name("rel_dob" + i));
				enterText(By.name("rel_dob" + i), String.valueOf(Date));

				BaseClass.selecttext("relHeightFeet" + i, FamilyData[mcount][6].toString().trim());
				BaseClass.selecttext("relHeightInches" + i, FamilyData[mcount][7].toString().trim());
				enterText(By.name("relWeight" + i), FamilyData[mcount][8].toString().trim());

			}
		}

		driver.findElement(By.xpath("//input[@type='button' and @name='next']")).click();
		System.out.println("Sucessfully Clicked on Next Button from Insurer Page.");


	}
	public static void careHeartQuestionset() throws Exception
	{
		String [] Numbers = {"1","2","3","4","5","6","7","8","9","0"};
		String [] Words = {"One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Zero"};
		String[][] FamilyData = BaseClass.excel_Files("CareHeart_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("CareHeart_QuestionSet");
		String[][] TestCaseData = BaseClass.excel_Files("CareHeart_Quotation");
		//Question mumber 1
		String preExistingdeases = TestCaseData[ch.n][22].toString().trim();
		Thread.sleep(3000);
		System.out.println(
				"Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
		// BaseClass.scrollup();
		try {

			if (preExistingdeases.contains("YES")) {

				waitForElements(By.xpath(YesButton_xpath));
				clickElement(By.xpath(YesButton_xpath));
				// Thread.sleep(2000);
				String years = null;
				String Details = null;
				for (int qlist = 1; qlist <= 11; qlist++) {
					Details = QuestionSetData[ch.n][qlist + (qlist - 1)].toString().trim();
					//System.out.println("Details is : "+Details);
					years = QuestionSetData[ch.n][qlist + qlist].toString().trim();
					if (Details.equals("")) {
						// break;
					} else {
						int detailsnumber = Integer.parseInt(Details);

						// Will click on check box and select the month & year u
						detailsnumber = detailsnumber + 1;
						System.out.println("Details and years are :" + Details + "----" + years);
						clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["+ detailsnumber + "]//input[@type='checkbox']"));
						Thread.sleep(1000);
						try {
							clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
									+ detailsnumber + "]//label"));
							enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
									+ detailsnumber + "]//label"), years);
						} catch (Exception e) {
							clickElement(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
									+ detailsnumber + "]//label[@class='monthYear']"));
							enterText(By.xpath("//*[@class='multyple_body']/tr[" + qlist + "]/td["
									+ detailsnumber + "]//label[@class='monthYear']"), years);
						}
					}
				}
			} else if (preExistingdeases.contains("NO")) {
				clickElement(By.xpath(NoButton_xpath));
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse User is unable to click on Health Question");
		}

		List<WebElement> Qlist =driver.findElements(By.xpath("//label[contains(@for,'_yes')]"));
		//Selecting for question2 
		int colloop=1;
		end:
			for(int question =1;question<Qlist.size();question++) {
				int selectMember = 0;
				int Coll =question+2;
				String Collaps = String.valueOf(Coll);
				int ql = question;
				int  nl = question;
				int  qnl;
				qnl = question;
				
				String noOfMember=TestCaseData[ch.n][15].toString().trim();
				System.out.println("Number of member is--- :"+noOfMember);
				String question2 = null;
				int questioncount;
				for(questioncount =23;questioncount<BaseClass.colCount;questioncount++ ) {
					if(TestCaseData[0][questioncount].equalsIgnoreCase("question"+question)) {
						question2=TestCaseData[ch.n][questioncount].toString().trim();
						selectMember =questioncount+1;
						break;
					}else {
					//	System.out.println("Quesion  Number not matched");
					}
				}
				String Details = null;
				if(question2.equalsIgnoreCase("Yes")) {
					System.out.println("Collapse is-- :"+Collaps);
					try {
						
						clickElement(By.xpath("//label[@for='question_"+Collaps+"_yes']"));
					}catch(Exception e) {
						Qlist.get(ql).click();
						System.out.println("Label text is : "+Qlist.get(ql).getAttribute("value"));

					}
					List<WebElement> Q2 = null ;


					Q2 = driver.findElements(By.xpath("//*[@id=\"collapse"+Collaps+"\"]/div/table/tbody/tr"));

					if(Q2.size()==0) {
						System.out.println("Answer Size is : "+Q2.size()+"Need to check with another path");
						for(int i=0;i<Numbers.length;i++) {
							if(Collaps.equals(Numbers[i])) {
								Collaps=Words[i].toString();
								break;
							}	
						}
						Q2 = driver.findElements(By.xpath("//*[@id=\"collapse"+Collaps+"\"]/div/table/tbody/tr"));
					}else {
						System.out.println("Answer Size is : "+Q2.size());
					}

					String SelectList = TestCaseData[ch.n][selectMember].toString().trim();
					String Data ;
					String[] Slist = SelectList.split(",");
					String[] dataEntry ; 
					int loop;
					ansLoop:
						for(int ansLoop=1;ansLoop<=Q2.size();ansLoop++) {
							Data=TestCaseData[ch.n][selectMember+ansLoop].toString().trim();
							dataEntry = Data.split(",");
							for(int i=0;i<Slist.length;i++) {
								if(Q2.size()<=1) {
									//System.out.println("Test is correct");
									loop= Integer.parseInt(Slist[i].trim());
								}else {


									loop=Integer.parseInt(noOfMember)+i;

								}
								Thread.sleep(5000);
								List<WebElement> checks=driver.findElements(By.xpath("//*[@id='collapse"+Collaps+"']/div/table/tbody/tr["+ansLoop+"]/td")); 
								System.out.println("Checks Size is : "+checks.size());
								if(checks.size()==1) {
									driver.findElement(By.xpath("//*[@id='collapse"+Collaps+"']/div/table/tbody/tr["+ansLoop+"]/td["+loop+"]//*[@type='checkbox']")).click();	
								}else {
									if(Collaps.equals("10")&colloop==1) {
										int testloop=0;
										int heading=11;
										for(int clloop=1;clloop<=checks.size();clloop++) {
											try {
												if(testloop==0) {
													driver.findElement(By.xpath("//*[@aria-labelledby= 'heading"+heading+"']/div/table/tbody/tr["+ansLoop+"]/td["+clloop+"]//*[@type='checkbox']")).click();	
												}else {
													driver.findElement(By.xpath("//*[@aria-labelledby= 'heading"+heading+"']/div/table/tbody/tr["+ansLoop+"]/td["+clloop+"]//*[@type='checkbox']")).click();	
													break end;
												}
											}catch(Exception e) {
												driver.findElement(By.xpath("//*[@id=\"heading11\"]/h4/div[2]/div/label[1]")).click();
												clloop=1;
												heading=10;
												testloop=1;
												driver.findElement(By.xpath("//*[@aria-labelledby= 'heading"+heading+"']/div/table/tbody/tr["+ansLoop+"]/td["+clloop+"]//*[@type='checkbox']")).click();	
												if(clloop==checks.size()/2) {
													break end;
												}
											}
										}
									}else {
										////Check debug here
										driver.findElement(By.xpath("//*[@id='collapse"+Collaps+"']/div/table/tbody/tr["+ansLoop+"]/td["+loop+"]//*[@type='checkbox']")).click();	
										
									}
								}
								try {
									driver.findElement(By.xpath("//*[@id='collapse"+Collaps+"']/div/table/tbody/tr["+ansLoop+"]/td["+loop+"]/div[2]/p/label/textarea")).sendKeys(dataEntry[i]);

								}catch(Exception e) {
									//System.out.println("Quesion  and answer row not matched");
								}


							}
						}
//["+question+"]
				}else if(question2.equalsIgnoreCase("No")) {
					List<WebElement> Nolist =driver.findElements(By.xpath("//label[contains(@for,'_no')]"));
					System.out.println("size"+Nolist.size());
					for(int nList=nl;nList<=Nolist.size();nList++) {
						if(nList<=2) {
							Nolist.get(qnl).click();
							break;
						}else {
						Nolist.get(nList).click();
						break;
						}
						
					}
					//clickElement(By.xpath("//label[contains(@for,'_no')]"));
					//clickElement(By.xpath("//*[@class='switch-field']/label[contains(text(),'No')]"));
				}
			}

String questionstatus=TestCaseData[ch.n][22].toString().trim();
if(questionstatus.equalsIgnoreCase("Yes")) {
	System.out.println("Please upload the file");
}else {
	System.out.println("No need to upload any file");
}
// Check Box on Health Questionnaire
BaseClass.scrolldown();
BaseClass.HelathQuestionnairecheckbox();

try {
	clickElement(By.xpath(proceed_to_pay_xpath));
	System.out.println("Sucessfully Clicked on Procced to Pay Button from Insurer Page.");
} catch (Exception e) {
	logger.log(LogStatus.FAIL, "Unable to Click on Procced to Pay Button from Insurer Page.");
	System.out.println("Unable to Click on Procced to Pay Button from Insurer Page.");
}
	}
	public static void CareHeartwithNoEdit() {


		try {
		
		WebElement TotalMember=driver.findElement(By.xpath("//*[@id='getquote']/form/div[1]/div[4]/div/ui-dropdown/div/div/a"));
		String TotalMemberPresentonQuotation=TotalMember.getText();
		System.out.println("Total Member in quotation page is : "+TotalMemberPresentonQuotation);
		scrolldown();
		Thread.sleep(5000);
		WebElement quotepremi=driver.findElement(By.xpath("//p[@class='get_quot_total_premium']/span[1]"));
		Quotationpremium_value=quotepremi.getText();
		System.out.println("Quotation premium for pos care smart select is :"+Quotationpremium_value);
		clickElement(By.xpath(Button_Buynow_xpath));
		
		// Premium verification on Proposal Page
		/*Fluentwait(By.xpath("//p[@class='amount ng-binding']"), 60, "Unable to read Premium on Proposal Page.");*/
		Thread.sleep(10000);
		scrolldown();
		String Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("proposal premium is :"+Proposalpremium_value);
		ProposalPremimPage_Value = Proposalpremium_value.substring(1, Proposalpremium_value.length());
		System.out.println("Total Premium Value on Proposal Page is : " + Proposalpremium_value.substring(1, Proposalpremium_value.length()));
	scrollup();
		try {
			Assert.assertEquals(Quotationpremium_value, Proposalpremium_value);
			logger.log(LogStatus.INFO,"Quotation Premium and Proposal Premium is Verified and Both are Same : "+ Proposalpremium_value.substring(1, Proposalpremium_value.length()));
		} catch (AssertionError e) {
			System.out.println(Quotationpremium_value + " - failed");
			logger.log(LogStatus.FAIL, "Quotation Premium and Proposal Premium are not Same");
			
		}

		
		// Total Number of Member on Quotation and Proposal Page
		String TotalMemberProposal = driver.findElement(By.xpath(TotalMemberProposal_xpath)).getText();
		System.out.println("Total Members on Proposal Page : " + TotalMemberProposal);
		try {
			Assert.assertEquals(TotalMemberPresentonQuotation, TotalMemberProposal);
			logger.log(LogStatus.INFO,
					"Number Of Members Verified on Quotation and ProposalPage Both are Same : "
							+ TotalMemberProposal);
		} catch (AssertionError e) {
			logger.log(LogStatus.INFO, "Number Of Members are diffrent on Quotation and ProposalPage");
		}

		
		
	}catch(Exception e)
		{
		System.out.println(e);
		}


	}
	public static void careheartproposalsummaryandpayment() throws Exception {
		


		String[][] TestCase=BaseClass.excel_Files("Care_Heart_TestCase");
		
		Thread.sleep(12000);
		String ExecutionStatus=TestCase[ch.n][4].toString().trim();
		if(ExecutionStatus.equalsIgnoreCase("Normal")) {
		QuotationandProposalVerification.VerifyPremiumIncrease_on_ProposalsummarypagenoeditCareHeart();
		}else {
			QuotationandProposalVerification.VerifyPremiumIncrease_on_posProposalsummarypagenoedit();
		}
		
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"))).click();
		
		waitForElement(By.xpath(payu_proposalnum_xpath));
		String PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number : "+PayuProposalNum);
		
		waitForElement(By.xpath(payuAmount_xpath));
		String PayuPremium= driver.findElement(By.xpath(payuAmount_xpath)).getText();
		String FinalAmount = PayuPremium.substring(0, PayuPremium.length()-3);
		System.out.println("Pay U Premium Amount : "+FinalAmount);
		//PayUPaymentFn.PayuPage_Credentials();
		BaseClass.PayuPage_Credentials();

		BaseClass.scrolldown();
		
		QuotationandProposalVerification.ThankyouPagemessageVerification();
		
		String ProposerName=driver.findElement(By.xpath("/html/body/div[2]/div[22]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div/div[1]/p")).getText();
		System.out.println(ProposerName);
		
		//DataVerificationinDb.DBVerification(PayuProposalNum);
		waitForElement(By.xpath(PolProp2_xpath));
		
		String ProposalSummary = driver.findElement(By.xpath(proposalsummary_thaankyou_xpath)).getText();
		System.out.println("Proposal summary is :"+ProposalSummary);
		String ThankyoupageProposal_Pol_num = driver.findElement(By.xpath(PolProp2_xpath)).getText();
		System.out.println("Proposal number is : "+ThankyoupageProposal_Pol_num);
		if(ExecutionStatus.equalsIgnoreCase("Normal")) {
		PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, proposalSummarypremium_value);
		}else {
			PdfMatch.pdfmatchcode(FinalAmount, PayuProposalNum, Edit.afterEdit_Proposalpremium_value);
		}
		WriteExcel.setCellData1("POSSmartSelect_TestCase", TestResult, ThankyoupageProposal_Pol_num, ch.n,2, 3);
		




	}
	
}
