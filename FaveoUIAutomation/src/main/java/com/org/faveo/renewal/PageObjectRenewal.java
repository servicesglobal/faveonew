package com.org.faveo.renewal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class PageObjectRenewal  extends BaseClass implements AccountnSettingsInterface

{
	static int count = 1;
	static int mcount;
	static int mcountindex = 0;
	static int covertype;

	public static String Application_Number_Renewal=null;
	public static String Renewal_Premium=null;	
	public static String PremiumModification_page=null;
	public static String RenewProposal_Num=null;
	public static String PayuProposalNum=null;
	
	
	public static void ClickRenewalTab()
	{
		//Click on Side Menu Bar and Renewal Tab
		Fluentwait(By.xpath(SideMenuBar_xpath));
		clickElement(By.xpath(SideMenuBar_xpath));
		System.out.println("Clicked on Side Menu Bar.");
		
		
		//Click on Renewal Tab
		try{
		clickElement(By.xpath(RenewalTab_Button_xpath));
		System.out.println("Clicked on Renewal Tab.");
		}
		catch(Exception e)
		{
			System.out.println("Unable to click on Renewal tab.");
		}
		
	}
	
	
	public static void RowColoumnCount(int n) throws Exception
	{
		
		String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
		
		//Find the Total Number of Columns and Row
		int Col=0;
		int Row=0;
		Col = driver.findElements(By.xpath("//div[@ng-hide='noDataFoundMsgFlag']//table//thead//tr//th")).size();
		Row = driver.findElements(By.xpath("//div[@ng-hide='noDataFoundMsgFlag']//table//tbody//tr")).size();
		
		
		//Used for loop for number of rows.
		int RowNumber =0;
		String PolicyNumber_Excel = TestCaseData[n][1].toString().trim();
		String Customer_Name;
		for (int i=1; i<=Row; i++)
		 {
			
			//Used for loop for number of columns.
		 	  String PolicyNumber_UI = driver.findElement(By.xpath("//html//tr["+i+"]/td[1]//h5")).getText();
			if(PolicyNumber_Excel.equals(PolicyNumber_UI))
			{
				RowNumber=i;
				System.out.println("Policy Number Found in Row number : "+i);
				//Check The Renewal Status of Policy
				String Renewal_Status = driver.findElement(By.xpath("//html//tr["+RowNumber+"]/td[8]/h5[1]")).getText();
				System.out.println("Renewal Status is : "+Renewal_Status);
				Customer_Name= driver.findElement(By.xpath("//html//tr["+RowNumber+"]/td[2]/h5[1]")).getText();
				System.out.println("Customer Name on Renewal Page is : "+Customer_Name);
				if(Renewal_Status.contains("INVITE SENT"))
				{
				driver.findElement(By.xpath("//html//tr["+RowNumber+"]/td[9]/h5[1]/a[1]")).click();
				System.out.println("Navigating To Renewal Journey Page.");
				}
				else if (Renewal_Status.contains("RENEWAL IN PROGRESS"))
				{
				System.out.println("Test Case is Failed Beacuse : Renewal is already in Progress so couldn't renew.");
				logger.log(LogStatus.FAIL, "Test Case is Failed Beacuse : Renewal is already in Progress so couldn't renew.");
					
				}
				
				break;
			}else
			{
				continue;
			}
			  
		  }
	}
	
	public static void SwitchWindow()
	{
		 String winHandleBefore = driver.getWindowHandle();
	        for(String winHandle : driver.getWindowHandles())
	        {
	            driver.switchTo().window(winHandle);
	        }
	}
	
	public static void RenewalPage1() throws Exception
	{
		
		 //Verifying Premium Amount
			try
			 {
					ExplicitWait2(By.xpath(Error_Message_Renewal), 20);
				 	String Error_Message_Renewal_Page=driver.findElement(By.xpath(Error_Message_Renewal)).getText();
					System.out.println("Test Case is Failed Beacuse Getting Error : "+Error_Message_Renewal_Page);
					logger.log(LogStatus.FAIL, "Test Case is Failed Beacuse Getting Error : "+Error_Message_Renewal_Page);
				 
			}
			 catch(Exception e)
			 {
				 
				 try
			{
					 
				Renewal_Premium = driver.findElement(By.xpath(Renewal_Premium_Summary_xpath)).getText();
				System.out.println("Renewal Premium on Renewal Summary Page : " + Renewal_Premium);
				clickElement(By.xpath(Modify_and_Renew_xpath));
				System.out.println("Clicked on Modify and Renew Buttton.");
				
			}
				 catch(Exception e1)
				 {
					 System.out.println("Unable to click on Modify and Renew Buttton.");
				 }
			}
		
	}
	
	public static void premium1match()
	{
		PremiumModification_page = driver.findElement(By.xpath("//span[@id='ren_prem']")).getText();
		 System.out.println("Renewal Premium is : "+PremiumModification_page);
		
		try {
			Assert.assertEquals(Renewal_Premium, PremiumModification_page);
			System.out.println("Quotaion Premium and Proposal Premium is Verified and Both are Same : "+ PremiumModification_page);
			logger.log(LogStatus.INFO,"Quotaion Premium and Proposal Premium is Verified and Both are Same : "+ PremiumModification_page);
		} catch (AssertionError e) 
		{
			System.out.println("Quotaion Premium and Proposal Premium are not Same :"+Renewal_Premium );
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same.");
		}
	}
	
	
	
	
	
	public static void RenewalWithoutModification(int n) throws Exception
	{
		 //Slide down
		 BaseClass.scrollup();
		 
		 //Go Green Initiative Selection
		 PageObjectRenewal.GoGreenInitiative(n);
		 
		 //NCD Declaration
		 NCDDeclaration();
	
		 //Standing Instruction
		 PageObjectRenewal.StandingInstruction(n);
		 
		 //I Agrre box
		 NCDDeclarationTerms();
		 
		 //Capture Proposal Page Premium
		 ExplicitWait1(By.xpath("//span[@id='ren_prem']"));
		 premium1match();
		 
		try
		{
		 clickbyid(By.xpath(Button_Submit_xpath));
		clickElement(By.xpath(Button_Submit_xpath));
		System.out.println("Clicked on Submit Button.");
		}
		catch(Exception e)
		{
			System.out.println("Unable to Click on Submit Button.");
		}
		
		//Get Proposal Number 
		RenewProposal_Num = driver.findElement(By.xpath(Popup_proposal_xpath)).getText();
		System.out.println("Renewal Proposal Number is : "+RenewProposal_Num);
		
		//Verify Premium Amount on Proposal Page and Proposal Summary Page
		String ProposalSum_Premium = driver.findElement(By.xpath(Popup_Premium_xpath)).getText();
		System.out.println("Proposal Summary Premium is : " + ProposalSum_Premium);
			try {
				Assert.assertEquals(PremiumModification_page, ProposalSum_Premium);
				System.out.println("Proposal page premium and Proposal summary premium is Verified and Both are Same : "+ ProposalSum_Premium);
				logger.log(LogStatus.INFO,"Proposal page premium and Proposal summary premium is Verified and Both are Same : "+ ProposalSum_Premium);
			} 
			catch (AssertionError e) 
			{
				System.out.println("Proposal page premium and Proposal summary premium are not Same :"+PremiumModification_page );
				logger.log(LogStatus.FAIL, "Proposal page premium and Proposal summary premium are not Same.");
				
			}
		
			//Click on Online Payment
			try{
				clickElement(By.xpath(Online_Payment_to_Renew));
				System.out.println("Clicked on Button Online Payment to Renew.");
			}catch(Exception e)
			{
				System.out.println("Unable to click on Button  Online Payment to Renew.");
			}	
			
			
		//Verify Proposal Number on Payu
		ExplicitWait2(By.xpath(payu_proposalnum_xpath), 20);
		PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number is : "+PayuProposalNum);
		try{
			Assert.assertEquals(RenewProposal_Num, PayuProposalNum);
			System.out.println("Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are Same : "+ PayuProposalNum);
			logger.log(LogStatus.INFO,"Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are Same : "+ PayuProposalNum);	
		}catch(Exception e)
		{
			System.out.println("Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are not Same." );
			logger.log(LogStatus.FAIL, "Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are not Same.");
		}
		
		
		
		//Pay U Page
		BaseClass.RenewalPayuPage_Credentials();
		
		//Pdf Match 
		BaseClass.Renewalpdfmatchcode(ProposalSum_Premium, PayuProposalNum, ProposalSum_Premium, n);
		
		
		
	}
	
	
	public static void RenewalWithModification(int n) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
		
		//Change Cover Type in With Modification Case.
		String CoverType = TestCaseData[n][3].toString().trim();
		try
		{
			if(CoverType.contains("INDIVIDUAL") || CoverType.contains("FAMILYFLOATER"))
			{
			Fluentwait(By.xpath(Cover_Type_xpath));
			clickElement(By.xpath(Cover_Type_xpath));
			clickElement(By.xpath("//select[@id='cover_type']//option[contains(text(),"+"'"+CoverType+"'"+")]"));
			System.out.println("Selected Cover Type is : "+CoverType);
			}
			else
			{
			System.out.println("There is No data in Excel so need to Change Cover Type.");	
			}

		}
			catch(Exception e)
		{
				System.out.println("Unable to Select Cover Type.");
		}	
		
		//Select SumInsured according to Excel
		String SumInsured = TestCaseData[n][4].toString().trim();
		try
		{
		if(SumInsured.equals(""))
		{
			System.out.println("There is No data in Excel so need to Change Sum Insured.");	
		}
		else{
		Fluentwait(By.xpath(SumInsured_PolicySum_xpath));
		clickElement(By.xpath(SumInsured_PolicySum_xpath));
		
		clickElement(By.xpath("//select[@id='sum_insured']//option[contains(text(),"+"'"+SumInsured+"'"+")]"));
		System.out.println("Selected SumInsured is : "+SumInsured);
		}
		}
		catch(Exception e)
		{
			System.out.println("Unable to Select Sum Insured.");
		}
		
		//Select Tenure from Excel
		String Tenure = TestCaseData[n][5].toString().trim();
		try{
		if(Tenure.equals("1") ||Tenure.equals("2") || Tenure.equals("3"))
		{

			Fluentwait(By.xpath(tenure_PolicySum_xpath));
			clickElement(By.xpath(tenure_PolicySum_xpath));
			clickElement(By.xpath("//select[@id='tenure']//option[contains(text(),"+"'"+Tenure+"'"+")]"));
			System.out.println("Selected Tenure is : "+Tenure);
		}
		else if(Tenure.equals("")){
		
		System.out.println("There is No data in Excel so need to Change Tenure.");	
		}
		}
		catch(Exception e)
		{
			System.out.println("Unable to Select Tenure.");
		}
		
		//Enter Aadhar Number
		/*String AadharNumber = TestCaseData[n][6].toString().trim();
		try{
			if(AadharNumber.equals(""))
			{
				System.out.println("There is No data in Excel so need to Enter Aadhar Card Number.");	
			}
			else{
			Fluentwait(By.xpath(AadharNumber_xpath));
			clickElement(By.xpath(AadharNumber_xpath));
			enterText(By.xpath(AadharNumber_xpath), AadharNumber);
			System.out.println("Entered Aadhar Number is : "+AadharNumber);
			}
			}
			catch(Exception e)
			{
				System.out.println("Unable to Enter Aadhar Number.");
			}*/
		
		//Addons Selection
		String UAR_addon= TestCaseData[n][7].toString().trim();
		try
		{
			findelement(By.xpath(UAR_Addon_xpath));
			if(UAR_addon.equals(""))
			{
				System.out.println("Please Enter data in Excel for UAR Addon.");
			}
			else if(UAR_addon.contains("Yes") || UAR_addon.contains("YES") || UAR_addon.contains("yes"))
			{
				System.out.println("UAR addon is Selected on UI.");
			}
			else if(UAR_addon.contains("NO") || UAR_addon.contains("No") || UAR_addon.contains("no"))
			{
				Fluentwait1(By.xpath(UAR_Addon_xpath));
				clickElement(By.xpath(UAR_Addon_xpath));
				System.out.println("UAR Addon is Unselected from UI.");
				
			}
		}
		catch(Exception e)
		{
			if(UAR_addon.equals("") || UAR_addon.contains("NO") || UAR_addon.contains("No") || UAR_addon.contains("no"))
			{
				System.out.println("No Need to Select UAR Addon.");
			}
			else if(UAR_addon.contains("Yes") || UAR_addon.contains("YES") || UAR_addon.contains("yes"))
			{
				System.out.println("Selection of UAR addon is not available on UI.");
				logger.log(LogStatus.FAIL, "UAR addon Selection option is not available on UI because in NewBusiness Policy UAR was not available.");
			}
			
		}
		
		
		//Care With NCB Addon
		String CareWithNcb= TestCaseData[n][8].toString().trim();
		try
		{
			findelement(By.xpath(CareWithNcb_xpath));
			if(CareWithNcb.equals(""))
			{
				System.out.println("Please Enter data in Excel for CareWithNcb Addon.");
			}
			else if(CareWithNcb.contains("Yes") || CareWithNcb.contains("YES") || CareWithNcb.contains("yes"))
			{
				System.out.println("CareWithNcb addon is Selected on UI.");
			}
			else if(CareWithNcb.contains("NO") || CareWithNcb.contains("No") || CareWithNcb.contains("no"))
			{
				Fluentwait1(By.xpath(CareWithNcb_xpath));
				clickElement(By.xpath(CareWithNcb_xpath));
				System.out.println("CareWithNcb Addon is Unselected from UI.");
				
			}
		}
		catch(Exception e)
		{
			if(CareWithNcb.equals("") || CareWithNcb.contains("NO") || CareWithNcb.contains("No") || CareWithNcb.contains("no"))
			{
				System.out.println("No Need to Select CareWithNcb Addon.");
			}
			else if(CareWithNcb.contains("Yes") || CareWithNcb.contains("YES") || CareWithNcb.contains("yes"))
			{
				System.out.println("Selection of CareWithNcb addon is not available on UI.");
				logger.log(LogStatus.FAIL, "CareWithNcb addon Selection option is not available on UI because in NewBusiness Policy CareWithNcb was not available.");
			}
			
		}
		
		//Everyday Care Addon
		String EveryDayCare= TestCaseData[n][9].toString().trim();
		try
		{
			findelement(By.xpath(EverydayCare_xpath));
			if(EveryDayCare.equals(""))
			{
				System.out.println("Please Enter data in Excel for EveryDayCare Addon.");
			}
			else if(EveryDayCare.contains("Yes") || EveryDayCare.contains("YES") || EveryDayCare.contains("yes"))
			{
				System.out.println("EveryDayCare addon is Selected on UI.");
			}
			else if(EveryDayCare.contains("NO") || EveryDayCare.contains("No") || EveryDayCare.contains("no"))
			{
				Fluentwait1(By.xpath(EverydayCare_xpath));
				clickElement(By.xpath(EverydayCare_xpath));
				System.out.println("EveryDayCare Addon is Unselected from UI.");
				
			}
		}
		catch(Exception e)
		{
			if(EveryDayCare.equals("") || EveryDayCare.contains("NO") || EveryDayCare.contains("No") || EveryDayCare.contains("no"))
			{
				System.out.println("No Need to Select EveryDayCare Addon.");
			}
			else if(EveryDayCare.contains("Yes") || EveryDayCare.contains("YES") || EveryDayCare.contains("yes"))
			{
				System.out.println("Selection of EveryDayCare addon is not available on UI.");
				logger.log(LogStatus.FAIL, "EveryDayCare addon Selection option is not available on UI because in NewBusiness Policy EveryDayCare was not available.");
			}
			
		}
		
		
		//Select PA Addons
		String PAAddon= TestCaseData[n][10].toString().trim();
		try
		{
			findelement(By.xpath(PA_xpath));
			if(PAAddon.equals(""))
			{
				System.out.println("Please Enter data in Excel for PA Addon.");
			}
			else if(PAAddon.contains("Yes") || PAAddon.contains("YES") || PAAddon.contains("yes"))
			{
				System.out.println("PA addon is Selected on UI.");
			}
			else if(PAAddon.contains("NO") || PAAddon.contains("No") || PAAddon.contains("no"))
			{
				Fluentwait1(By.xpath(PA_xpath));
				clickElement(By.xpath(PA_xpath));
				System.out.println("PA addon is Unselected from UI.");
				
			}
		}
		catch(Exception e)
		{
			if(PAAddon.equals("") || PAAddon.contains("NO") || PAAddon.contains("No") || PAAddon.contains("no"))
			{
				System.out.println("No Need to Select PA Addon.");
			}
			else if(PAAddon.contains("Yes") || PAAddon.contains("YES") || PAAddon.contains("yes"))
			{
				System.out.println("Selection of PA addon is not available on UI.");
				logger.log(LogStatus.FAIL, "PA addon Selection option is not available on UI because in NewBusiness Policy PA was not available.");
			}
			
		}
		
		//OPD Care Addon
		String OPDCare= TestCaseData[n][11].toString().trim();
		try
		{
			findelement(By.xpath(OPDCARE_xpath));
			if(OPDCare.equals(""))
			{
				System.out.println("Please Enter data in Excel for OPDCare Addon.");
			}
			else if(OPDCare.contains("Yes") || OPDCare.contains("YES") || OPDCare.contains("yes"))
			{
				System.out.println("OPDCare addon is Selected on UI.");
			}
			else if(OPDCare.contains("NO") || OPDCare.contains("No") || OPDCare.contains("no"))
			{
				Fluentwait1(By.xpath(OPDCARE_xpath));
				clickElement(By.xpath(OPDCARE_xpath));
				System.out.println("OPDCare addon is Unselected from UI.");
				
			}
		}
		catch(Exception e)
		{
			if(OPDCare.equals("") || OPDCare.contains("NO") || OPDCare.contains("No") || OPDCare.contains("no"))
			{
				System.out.println("No Need to Select OPDCare Addon.");
			}
			else if(OPDCare.contains("Yes") || OPDCare.contains("YES") || OPDCare.contains("yes"))
			{
				System.out.println("Selection of OPDCare addon is not available on UI.");
				logger.log(LogStatus.FAIL, "OPDCare addon Selection option is not available on UI because in NewBusiness Policy OPDCare was not available.");
			}
			
		}
		
		//Go Green Initiative
		PageObjectRenewal.GoGreenInitiative(n);
		
		//Add Member 
		PageObjectRenewal.addMember(n);
		
		//Health Questionnaire
		PageObjectRenewal.HealthQuestionnaire(n);
		
		 //NCD Declaration
		 NCDDeclaration();
		
		//Standing Instruction
		StandingInstruction(n);
		
		NCDDeclarationTerms();
		
		 //Capture Proposal Page Premium
		 ExplicitWait(By.xpath("//span[@id='ren_prem']"));
		 PremiumModification_page = driver.findElement(By.xpath("//span[@id='ren_prem']")).getText();
		 System.out.println("Renewal Premium is : "+PremiumModification_page);
		 
		try
		{
		clickbyid(By.xpath(Button_Submit_xpath));
		clickElement(By.xpath(Button_Submit_xpath));
		System.out.println("Clicked on Submit Button.");
		}
		catch(Exception e)
		{
			System.out.println("Unable to Click on Submit Button.");
		}
		
		//GHD Question
		GHDQuestion(n);
		
		//Verify Error Message
		PageObjectRenewal.ErrorMessageSubmit();
	
		
		//Get Proposal Number 
		RenewProposal_Num = driver.findElement(By.xpath(Popup_proposal_xpath)).getText();
		System.out.println("Renewal Proposal Number is : "+RenewProposal_Num);
		
		//Verify Premium Amount on Proposal Page and Proposal Summary Page
		String ProposalSum_Premium = driver.findElement(By.xpath(Popup_Premium_xpath)).getText();
		System.out.println("Proposal Summary Premium is : " + ProposalSum_Premium);
			try {
				Assert.assertEquals(PremiumModification_page, ProposalSum_Premium);
				System.out.println("Proposal page premium and Proposal summary premium is Verified and Both are Same : "+ ProposalSum_Premium);
				logger.log(LogStatus.INFO,"Proposal page premium and Proposal summary premium is Verified and Both are Same : "+ ProposalSum_Premium);
			} 
			catch (AssertionError e) 
			{
				System.out.println("Proposal page premium and Proposal summary premium are not Same :"+PremiumModification_page );
				logger.log(LogStatus.FAIL, "Proposal page premium and Proposal summary premium are not Same.");
				
			}
		
			//Click on Online Payment
			try{
				clickElement(By.xpath(Online_Payment_to_Renew));
				System.out.println("Clicked on Button Online Payment to Renew.");
			}catch(Exception e)
			{
				System.out.println("Unable to click on Button  Online Payment to Renew.");
			}	
			
		//Verify Proposal Number on Payu
		ExplicitWait2(By.xpath(payu_proposalnum_xpath), 20);
		PayuProposalNum = driver.findElement(By.xpath(payu_proposalnum_xpath)).getText();
		System.out.println("Pay U Proposal Number is : "+PayuProposalNum);
		try{
			Assert.assertEquals(RenewProposal_Num, PayuProposalNum);
			System.out.println("Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are Same : "+ PayuProposalNum);
			logger.log(LogStatus.INFO,"Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are Same : "+ PayuProposalNum);	
		}catch(Exception e)
		{
			System.out.println("Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are not Same." );
			logger.log(LogStatus.FAIL, "Proposal Summary Page Proposal Number and Pay u page Proposal Number is Verified and Both are not Same.");
		}
		
		//Pay U Page
		BaseClass.RenewalPayuPage_Credentials();
		
		
		//Final Premium on Thankyou Page
		String FinalAmount;
		try{
		FinalAmount=driver.findElement(By.xpath("//*[@id='succ_case2']/div[1]/div[2]/div[1]/div[3]/div/div[2]/strong")).getText();
		System.out.println("Premium on Thankyou Page : "+FinalAmount);
		}
		catch(Exception e)
		{
			FinalAmount=driver.findElement(By.xpath("//*[@id='succ_case1']/div[1]/div[2]/div[1]/div[3]/div/div[2]/strong")).getText();
			System.out.println("Premium on Thankyou Page : "+FinalAmount);	
		}
		//Pdf Match 
		BaseClass.Renewalpdfmatchcode(FinalAmount, PayuProposalNum, ProposalSum_Premium, n);
	
		
		
	}
	

	public static void GoGreenInitiative(int n) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
		//Go Green Initiative 
		String GoGreenInitiative = TestCaseData[n][12].toString().trim();
		try{
			if(GoGreenInitiative.equals(""))
			{
				System.out.println("There is No data in Excel so need to Change in Go Green Initiative.");	
			}
			else if(GoGreenInitiative.contains("Yes") || GoGreenInitiative.contains("YES") || GoGreenInitiative.contains("yes"))
			{
				System.out.println("Go Green Initiative is already selected.");
				logger.log(LogStatus.INFO, "User has Selected renewal communication electronically on their registered email address.");
			}
			else if(GoGreenInitiative.contains("NO") || GoGreenInitiative.contains("No") || GoGreenInitiative.contains("no"))
			{
				Fluentwait(By.xpath(GoGreen_Initiative_xpath));
				clickElement(By.xpath(GoGreen_Initiative_xpath));
			System.out.println("User don't wants renewal communication electronically.");
			logger.log(LogStatus.INFO, "User don't wants renewal communication electronically. - Go Green Initiative.");
			}
		}
			catch(Exception e)
			{
				System.out.println("Unable to Select Go Green Initiative.");
			}

	}
	
	public static void addMember(int n) throws Exception
	{
		//clickElement(By.xpath(AddMember_Renewal_xpath));
		
		
		String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
		String[][] FamilyData = BaseClass.excel_File("Renewal_Insured_Details");
		String AddMember = TestCaseData[n][13].toString().trim();
		String Membersdetails = TestCaseData[n][14].toString().trim();
		System.out.println("Member Details : "+Membersdetails);
		int Addmem = Integer.parseInt(AddMember);
		int Add = (Addmem+2);
	int count = 2;
		
		if(AddMember.equals(""))
		{
			System.out.println("There is No Data in Excel to add Member.");
		}
		else{
		for (int i = 2; i <Add; i++) 
		{
			BaseClass.membres = Membersdetails.split(",");
			mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
			mcountindex = mcountindex + 1;
			clickElement(By.xpath(AddMember_Renewal_xpath));
			
			String Relation = FamilyData[mcount][0].toString().trim();
			System.out.println("Need to Select Relation : "+Relation);
			
			String Title = FamilyData[mcount][1].toString().trim();
			System.out.println("Need to Select Title : "+Title);
			
			String Firstname = FamilyData[mcount][2].toString().trim();
			System.out.println("Entered First Name is : "+Firstname);
			
			String Lastname = FamilyData[mcount][3].toString().trim();
			System.out.println("Entered Last Name is : "+Lastname);
			
			String DateOfBirth = FamilyData[mcount][4].toString().trim();
			System.out.println("Date of Birth for Insured Person : "+DateOfBirth);
			
			
			ExplicitWait2(By.name("relationCd" + i), 2);
			clickElement(By.name("relationCd" + i));
			BaseClass.selecttext("relationCd" + i, Relation);
			
			BaseClass.selecttext("titleCd" + i, Title);
			
			enterText(By.name("fname" + i), Firstname);
			
			
			enterText(By.name("lname" + i), Lastname);
			
			
			//Date of Birth
			String spilitter[]=DateOfBirth.split(",");			
			String eday = spilitter[0];
			String emonth = spilitter[1];
			String eYear = spilitter[2];
			clickElement(By.name("dob" + i));
			clickElement(By.xpath("//select[@class='ui-datepicker-month']"));
			clickElement(By.xpath("//select[@class='ui-datepicker-month']//option[contains(text(),"+"'"+emonth+"'"+")]"));
			clickElement(By.xpath("//select[@class='ui-datepicker-year']"));
			clickElement(By.xpath("//select[@class='ui-datepicker-year']//option[contains(text(),"+"'"+eYear+"'"+")]"));
			clickElement(By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"+"'"+eday+"'"+")]"));
			
			
		}
		}

			
	}
	
	public static void StandingInstruction(int n) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
		String StandingInstruction = TestCaseData[n][15].toString().trim();
		System.out.println("Standing Instruction is Selected as : "+StandingInstruction);
		if(StandingInstruction.contains("Yes") || StandingInstruction.contains("YES") || StandingInstruction.contains("yes"))
		{
			//Click as Yes on Standing Instruction
			clickElement(By.xpath(Standing_Instruction_xpath));
		}
		else if(StandingInstruction.contains("No") || StandingInstruction.contains("NO") || StandingInstruction.contains("no"))
		{
			//Click On Submit Button
			 clickbyid(By.xpath(Button_Submit_xpath));
			 clickElement(By.xpath("//div[@class='main-loading']"));
			 clickElement(By.xpath("//button[@id='alertCheckNO']"));
		}
		else if(StandingInstruction.equals(""))
		{
			System.out.println("Please Enter Some Data in Standing Instruction Filed in Test Data Excel.");
			logger.log(LogStatus.FAIL, "Please Enter Some Data in Standing Instruction Filed in Test Data Excel.");
		}
	}
	
	
	
public static void HealthQuestionnaire(int n) throws Exception
{
	//Health Questionnarire Elements
	String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
			String[][] QuestionSetData = BaseClass.excel_File("Health_Question");
			String preExistingdeases = TestCaseData[n][16].toString().trim();
			ExplicitWait2(By.xpath(PED_Question1_Yes_xpath), 3);
			System.out.println("Does any person(s) to be insured has any pre-exsiting diseases? :" + preExistingdeases);
			
			BaseClass.scrolldown();
			try{
				 if(preExistingdeases.contains("Yes") || preExistingdeases.contains("YES") || preExistingdeases.contains("yes")) {
					
					 waitForElements(By.xpath(PED_Question1_Yes_xpath));
					 clickElement(By.xpath(PED_Question1_Yes_xpath));
					
					 String years=null;
					 String Details=null;		
					 String Year=null;
					 String Month=null;
					 for(int qlist=1;qlist<=14;qlist++) 
					 {
						Details =QuestionSetData[n][qlist+(qlist-1)].toString().trim();
						 years=QuestionSetData[n][qlist+qlist].toString().trim();
						 if(years.equals(""))
						 {
				
						 }else {
						 String spilitter[]=years.split(",");			
							Year = spilitter[0];
							Month = spilitter[1];							
						 }
						 
						 if(Details.equals("")) 
						 {
							continue;
						 }
						 else 
						 {
				 int detailsnumber = qlist+1;
				 clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[2]//div["+Details+"]//label"));
				 Thread.sleep(1000);
				 try {
					 //clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[3]//div[2]//div["+Details+"]//div[1]//select"));
					 
					 clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[3]//div[2]//div["+Details+"]//div[1]//select//option[contains(text(),"+ "'" + Year + "'" + ")]"));
					 //clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[3]//div[2]//div["+Details+"]//div[2]//select"));
					 clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[3]//div[2]//div["+Details+"]//div[2]//select//option[contains(text(),"+ "'" + Month + "'" + ")]"));
				 }catch(Exception e) {
					 clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[3]//div[2]//div["+Details+"]//div[1]//select//option[contains(text(),"+ "'" + Year + "'" + ")]"));
					 clickElement(By.xpath("//div[@class='medicalHistoryQuestionMid']//div["+detailsnumber+"]//div//div[3]//div[2]//div["+Details+"]//div[2]//select//option[contains(text(),"+ "'" + Month + "'" + ")]"));
			}
		}	
		}
	}	else if (preExistingdeases.contains("NO") || preExistingdeases.contains("No") || preExistingdeases.contains("no")) 
		{
			clickElement(By.xpath(NoButton_xpath));
		}
	else if (preExistingdeases.equals("")) 
	{
		System.out.println("There is No data in Excel so no Need to Select PED.");
	}
	}
			catch(Exception e)
					{
				System.out.println("Test Case is Fail Beacuse User is unable to click on Health Question");

					}
			
			
	//Question Number 2
			BaseClass.scrolldown();

			String[] ChckData = null;
			int datacheck = 0;
			int AddMember = Integer.parseInt(TestCaseData[n][13].toString().trim());
			for (int morechecks = 1; morechecks <= 3; morechecks++) 
			{
				int mch = morechecks + 1;
				String ChecksData = TestCaseData[n][16 + morechecks].toString().trim();
				try{
				if (ChecksData.equals("")) 
				{
					System.out.println("There is No Data ib Excel Please fill Some data in Excel.");
				} 
				else 
				{
					if(ChecksData.contains("NO") || ChecksData.contains("No") || ChecksData.contains("no")) 
					{
						System.out.println("Quatation set to NO");
						clickElement(By.xpath("//label[@id='label-id"+mch+"-0']"));
					}
					else{
					BaseClass.scrolldown();
					clickElement(By.xpath("//label[@id='label-id"+mch+"-1']"));
					for(int i=1;i<=AddMember;i++)
					{
					clickElement(By.xpath("//div[@id='id2']//div[2]//div[2]//div[2]//div[@class='selectinsuredParent']//select"));
					clickElement(By.xpath("//div[@id='id"+mch+"']//div[2]//div[2]//div["+i+"]//div[@class='selectinsuredParent']//select//option[contains(text(),'No')]"));
					}
					
					if (ChecksData.contains(",")) 
					{
						
						ChckData = ChecksData.split(",");
						for (String Chdata : ChckData) 
						{
							
							datacheck = Integer.parseInt(Chdata);
							clickElement(By.xpath("//div[@id='id2']//div[2]//div[2]//div[2]//div[@class='selectinsuredParent']//select"));
							clickElement(By.xpath("//div[@id='id"+mch+"']//div[2]//div[2]//div["+datacheck+"]//div[@class='selectinsuredParent']//select//option[contains(text(),'Yes')]"));
						}
					} 
					else if (ChecksData.contains(""))
					{
						datacheck = Integer.parseInt(ChecksData);	
						clickElement(By.xpath("//div[@id='id2']//div[2]//div[2]//div[2]//div[@class='selectinsuredParent']//select"));
						clickElement(By.xpath("//div[@id='id"+mch+"']//div[2]//div[2]//div["+datacheck+"]//div[@class='selectinsuredParent']//select//option[contains(text(),'Yes')]"));
					}
				}
				}
				}
				catch(Exception e){
					logger.log(LogStatus.FAIL, "Test Case is Fail Beacuse Unable to click on Health Question");
				}

			}
			

}
	public static void ErrorMessageSubmit()
	{
		try{
			ExplicitWait(By.xpath(Error_Submit));
			String ErrorMesaage = driver.findElement(By.xpath(Error_Submit)).getText();
			System.out.println("Test Case is Failed Beacuse getting : "+ErrorMesaage);
			logger.log(LogStatus.FAIL, "Test Case is Failed Beacuse getting : "+ErrorMesaage);
		}catch(Exception e)
		{
			System.out.println("EveryThing is Filled on renewal Page.");
		}
	}

	public static void ExplicitWait(By by)
	{
		WebDriverWait wait = new WebDriverWait(driver, 15);
		 
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));

	}
	
	public static void ExplicitWait1(By by)
	{
		WebDriverWait wait = new WebDriverWait(driver, 5);
		 
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));

	}
	
	public static void TestCaseName(int n) throws Exception
	{
		
		String[][] TestCase = BaseClass.excel_File("Test_Cases_Renewal");
		//Picking Test Case Name from Excel 
		String TestCaseName = (TestCase[n][0].toString().trim() + " - " + TestCase[n][1].toString().trim());
		logger = extent.startTest("RenewalJourney - " + TestCaseName);
		System.out.println("RenewalJourney - " +TestCaseName);

	}
	
	public static void ExplicitWait2(By by, int time)
	{
		WebDriverWait wait = new WebDriverWait(driver, time);
		 
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));

	}
	
	public static void NCDDeclaration() throws Exception
	{
		if(driver.findElement(By.xpath(NCD_Declaration_xpath)).isDisplayed())
		{
			clickElement(By.xpath(NCD_Declaration_xpath));
			
		}
	else
		{
			System.out.println("No Need to Select NCD Declaration.");
		}
	}
	
	public static void NCDDeclarationTerms()
	{
		if(driver.findElement(By.xpath(Button_IAgree)).isDisplayed())
		{
			clickElement(By.xpath(Button_IAgree));
		}
		else
		{
		System.out.println("");
		}
	}
	
	public static void GHDQuestion(int n) throws Exception
	{
			String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");
				String GHDQuestion= TestCaseData[n][20].toString().trim();
				String[] ChckData = null;
				int datacheck = 0;
				try
				{
					clickElement(By.xpath(GHD_NO_xpath));
					
					if(GHDQuestion.equals(""))
					{
						System.out.println("Please Enter data in Excel for UAR Addon.");
						logger.log(LogStatus.FAIL, "Please Enter data in Excel for UAR Addon.");
					}
					else if(GHDQuestion.contains("Yes") || GHDQuestion.contains("YES") || GHDQuestion.contains("yes"))
					{
						clickElement(By.xpath(GHD_Yes_xpath));
						System.out.println("GHD Question is Set as Yes.");
						
						List<WebElement> option = driver.findElements(By.xpath("//div[@class='selectinsuredParent']//Select[@ng-model='ghd_qs_set_[$index]']"));
						int optionCount = option.size();
						System.out.println("Number of Dropdown on GHD Question.");

						
						for(int i=1;i<=optionCount;i++)
						{
						clickElement(By.xpath("//select[@id='ghd_qs_set"+optionCount+"']"));
						clickElement(By.xpath("//select[@id='ghd_qs_set"+optionCount+"']//Option[contains(text(),'No')]"));
						}
						
						String GHDQuestionMember= TestCaseData[n][21].toString().trim();

						if (GHDQuestionMember.contains(",")) 
						{
							
							ChckData = GHDQuestionMember.split(",");
							for (String Chdata : ChckData) 
							{
								
								datacheck = Integer.parseInt(Chdata);
								clickElement(By.xpath("//select[@id='ghd_qs_set"+datacheck+"']"));
								clickElement(By.xpath("//select[@id='ghd_qs_set"+datacheck+"']//Option[contains(text(),'Yes')]"));
							}
						} 
						else if (GHDQuestionMember.contains(""))
						{
							datacheck = Integer.parseInt(GHDQuestionMember);	
							clickElement(By.xpath("//select[@id='ghd_qs_set"+datacheck+"']"));
							clickElement(By.xpath("//select[@id='ghd_qs_set"+datacheck+"']//Option[contains(text(),'Yes')]"));	
							
						}
						
						clickElement(By.xpath(Submit_GHD_xpath));

						logger.log(LogStatus.INFO, "GHD Question is Set as Yes.");
						
						clickbyid(By.xpath(Button_Submit_xpath));
						clickElement(By.xpath(Button_Submit_xpath));
						
					}
					else if(GHDQuestion.contains("NO") || GHDQuestion.contains("No") || GHDQuestion.contains("no"))
					{
						clickElement(By.xpath(Submit_GHD_xpath));
							System.out.println("GHD Question is Set as No.");
							logger.log(LogStatus.INFO, "GHD Question is Set as No.");
							clickbyid(By.xpath(Button_Submit_xpath));
							clickElement(By.xpath(Button_Submit_xpath));
								
					}
					
				}
				catch(Exception e)
				{
					System.out.println("No Need to Select GHD Question.");
				}
				
	}
		
}

	
	
	
