package com.org.faveo.renewal;

import org.openqa.selenium.By;
import org.testng.annotations.Test;


import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Renewal extends BaseClass implements AccountnSettingsInterface
{

	@Test
	public void RenewalJourney() throws Exception
	{
		
		String[][] TestCase = BaseClass.excel_File("Test_Cases_Renewal");
		String[][] TestCaseData = BaseClass.excel_File("Renewal_Data");

		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Renewal_Data.xlsx");
		int rowCount = fis.getRowCount("Test_Cases_Renewal");
		System.out.println("Total Number of Row in Sheet : " + rowCount);

		for (int n = 2; n <3; n++) 
		{
		try 
		{
			//Picking Test Case Name from Excel 
			PageObjectRenewal.TestCaseName(n);
			
			
			// Launching Browser using Method of Base Class
			BaseClass.LaunchBrowser();

			// Login with the help of Login Case class
			LoginCase.LoginwithValidCredendial();
			
			//Click on Side Menu Bar and Renewal Tab
			PageObjectRenewal.ClickRenewalTab();

			//Find the Total Number of Columns and Row
			PageObjectRenewal.RowColoumnCount(n);
			
			//Switching to New Window
			PageObjectRenewal.SwitchWindow();
		        
			//Get Premium Amount from Policy Summary Page
			PageObjectRenewal.RenewalPage1();
			
			//With and Without Modification
			String ModificationType = TestCaseData[n][2].toString().trim();
			if(ModificationType.contains("Without Modification"))
			{
				PageObjectRenewal.RenewalWithoutModification(n);
				
			}else if(ModificationType.contains("With Modification"))
			{
				PageObjectRenewal.RenewalWithModification(n);			
			}

			//driver.quit();

		} catch (Exception e) 
		{
			TestResult="Fail";
			WriteExcel.setCellData2("Test_Cases_Renewal", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case : "+e);
			logger.log(LogStatus.FAIL, "Test Case is Failed.");
			/*driver.quit();*/
			
		}
		 continue; 
	
}
}
}
