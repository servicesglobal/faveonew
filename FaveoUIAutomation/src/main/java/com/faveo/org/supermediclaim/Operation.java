package com.faveo.org.supermediclaim;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.OperationQuotationPageJourney;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.login.LoginFn;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Operation extends BaseClass implements AccountnSettingsInterface  {
public static Integer n;
	
@Test(enabled=false)
	public static void Operation() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("Operation_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("Operation_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Operation_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Operation_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Operation_TestCase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		for(n=1;n<=1;n++) {
		try {
		BaseClass.LaunchBrowser();
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
		System.out.println("SuperMediclaim Heart -" +TestCaseName);
		
		//Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		HealthInsuranceDropDown.OperationDropdown();
		OperationQuotationPageJourney.Operationquotationjourney();
		OperationQuotationPageJourney.operationdropDown();
		OperationQuotationPageJourney.operation_dragdrop();
		OperationQuotationPageJourney.OperationTenure();
		OperationQuotationPageJourney.Operation_Monthly_frequency();
		String Executionstatus=TestCase[1][4];
		
		if(Executionstatus.equalsIgnoreCase("Edit")) {
			clickElement(By.xpath(critical_xpath));
			Edit.EditOperation();
		}else {
			OperationQuotationPageJourney.Operation_verify_premium();
		}
		OperationQuotationPageJourney.Operation_proposerDetails();
		OperationQuotationPageJourney.Operation_insuredDetails();
		OperationQuotationPageJourney.Operation_questionSet();
		OperationQuotationPageJourney.Operation_Payment();
		
		//WriteExcel.setCellData1("Operation_TestCase", TestResult,OperationQuotationPageJourney.ThankyoupageProposal_Pol_num, n, 2, 3);

		 //driver.quit();
		
			}catch(Exception e) {
				//WriteExcel.setCellData("Operation_TestCase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				//driver.close();
			}
			
	}
	}
	//***************** Share Quote Test case **********************************************
	@Test(enabled=false)
	public static void OperationShareQuote() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("ShareQuotation");
		String[][] TestCaseData=BaseClass.excel_Files("Operation_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Operation_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Operation_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Operation_TestCase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		for(n=4;n<=4;n++) {
			try {
		//BaseClass.LaunchBrowser();
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
		System.out.println("SuperMediclaim Heart -" +TestCaseName);
		
		//Step 1 - Open Email and Delete any Old Email
		System.out.println("Step 1 - Open Email and Delete Old Email");
		logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");

		LaunchBrowser();
		ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit();
		
		// Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
		System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
		logger.log(LogStatus.PASS,"Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		HealthInsuranceDropDown.OperationDropdown();
		OperationQuotationPageJourney.Operationquotationjourney();
		OperationQuotationPageJourney.operationdropDown();
		OperationQuotationPageJourney.operation_dragdrop();
		OperationQuotationPageJourney.OperationTenure();
		OperationQuotationPageJourney.Operation_Monthly_frequency();
		
		AddonsforProducts.readPremiumFromFirstPage();

		ReadDataFromEmail.readAgeGroup();

		DataVerificationShareQuotationPage.clickOnShareQuotationButton();
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimOperation(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();
        
		// Step 3 - Open Email and Verify Data in Email Body
		System.out.println("Step 3 - Open Email and Verify Data in Email Body");
		logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
		LaunchBrowser();
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimOperation(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

	    // Step 4 - Click on Buy Now Button from Email Body and punch the Policy
		System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
		logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");

		ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
		
		OperationQuotationPageJourney.Operation_proposerDetails();
		OperationQuotationPageJourney.Operation_insuredDetails();
		OperationQuotationPageJourney.Operation_questionSet();
		try {
			(new WebDriverWait(driver, 40)).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
					.click();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("unable to load psoposal summary payment method page :");
		}

		PayuPage_Credentials();

		//OperationQuotationPageJourney.Operation_Payment();
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();

		// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
		System.out.println(
				"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		logger.log(LogStatus.PASS,
				"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareQuotationPage
				.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimOperation(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();

		// Step 6- Again Open the Email and Verify GUID Should be Reusable
		System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
		logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

		// Verify the Page Title
		DataVerificationShareProposalPage.verifyPageTitle();

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");

		 driver.quit();
		
		//WriteExcel.setCellData1("Operation_TestCase", TestResult,OperationQuotationPageJourney.ThankyoupageProposal_Pol_num, n, 2, 3);

		// driver.quit();
		
			}catch(Exception e) {
				//WriteExcel.setCellData("Operation_TestCase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				driver.quit();
			}
			
	}
	}
	//**************** Share Proposal Test Case ***************************************
	@Test(enabled=false)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("Operation_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("Operation_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Operation_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Operation_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Operation_TestCase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		for(n=2;n<=6;n++) {
			try {
		//BaseClass.LaunchBrowser();
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
		System.out.println("SuperMediclaim Heart -" +TestCaseName);
		
		
		// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
		System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
		logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
		LaunchBrowser();
		
		//Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		HealthInsuranceDropDown.OperationDropdown();
		OperationQuotationPageJourney.Operationquotationjourney();
		OperationQuotationPageJourney.operationdropDown();
		OperationQuotationPageJourney.operation_dragdrop();
		OperationQuotationPageJourney.OperationTenure();
		OperationQuotationPageJourney.Operation_Monthly_frequency();
		
		
		//clickElement(By.xpath(superMediclaim_BuyNowButton_Xpath));
		
		try {
			Thread.sleep(3000);
			clickElement(By.xpath(Button_Buynow_xpath));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		
		OperationQuotationPageJourney.Operation_proposerDetails();
		OperationQuotationPageJourney.Operation_insuredDetails();
		
		//Set Details of Share Proposal Popup Box and Verify Data in Draft History
		DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
		DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamOperation(n);
				
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		
		//Click on Resume Policy
		DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
		
		OperationQuotationPageJourney.Operation_questionSet();
		
		waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
	    clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
			
		BaseClass.PayuPage_Credentials();
		driver.quit();
		
		//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
				System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
				logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		        driver.quit();
		        
				LaunchBrowser();
				LoginCase.LoginwithValidCredendial();
				DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamOperation(n);

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");	
				
				 driver.quit();
		
		//OperationQuotationPageJourney.Operation_Payment();
		
		//WriteExcel.setCellData1("Operation_TestCase", TestResult,OperationQuotationPageJourney.ThankyoupageProposal_Pol_num, n, 2, 3);

		 //driver.quit();
		
			}
			catch(AssertionError e) {
				//WriteExcel.setCellData("Operation_TestCase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				//driver.close();
			}
			catch(Exception e) {
				//WriteExcel.setCellData("Operation_TestCase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				//driver.close();
			}
			
	}
	}
	
	
	//**************** Share Proposal Test Case ***************************************
		@Test(enabled=true)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
			String[][] TestCase=BaseClass.excel_Files("Operation_TestCase");
			String[][] TestCaseData=BaseClass.excel_Files("Operation_Quotation");
			String[][] FamilyData=BaseClass.excel_Files("Operation_Insured_Details");
			String[][] QuestionSetData=BaseClass.excel_Files("Operation_QuestionSet");
			
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("Operation_TestCase");
			System.out.println("Total Number of Row in Sheet : "+rowCount);
			for(n=1;n<=1;n++) {
				try {
			//BaseClass.LaunchBrowser();
			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
			System.out.println("SuperMediclaim Heart -" +TestCaseName);
			
			
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Step 2 - Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
			LaunchBrowser();
			
			//Login with the help of Login Case class
			LoginCase.LoginwithValidCredendial();
			HealthInsuranceDropDown.OperationDropdown();
			OperationQuotationPageJourney.Operationquotationjourney();
			OperationQuotationPageJourney.operationdropDown();
			OperationQuotationPageJourney.operation_dragdrop();
			OperationQuotationPageJourney.OperationTenure();
			OperationQuotationPageJourney.Operation_Monthly_frequency();
			
			
			//clickElement(By.xpath(superMediclaim_BuyNowButton_Xpath));
			
			Thread.sleep(5000);
			AddonsforProducts.readPremiumFromFirstPage();
            ReadDataFromEmail.readAgeGroup(); 
			
			try {
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
			} catch (Exception e) {
				System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
			}
			
			OperationQuotationPageJourney.Operation_proposerDetails();
			OperationQuotationPageJourney.Operation_insuredDetails();
			
			//Set Details of Share Proposal Popup Box and Verify Data in Draft History
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			//logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();
			
			//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
			System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			
		    LaunchBrowser();
		    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForSuperMedicliamOperation(n);
		    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			//logger.log(LogStatus.PASS, "Test Case Passed");
			
			//Step 4 - Click on Buy Now button from Email Body and punch the policy
			System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
			
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
			
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			
			OperationQuotationPageJourney.Operation_questionSet();
			
			waitForElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
		    clickElement(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']"));
				
			BaseClass.PayuPage_Credentials();
			driver.quit();
			
			//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
			System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
			logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
			 driver.quit();
			        
		    LaunchBrowser();
			LoginCase.LoginwithValidCredendial();
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamOperation(n);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");	
					
			driver.quit();
			
			// Step 6- Again Open the Email and Verify GUID Should be Reusable
				System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
				logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

				LaunchBrowser();
				ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

				// Verify the Page Title
				DataVerificationShareProposalPage.verifyPageTitle();

				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				logger.log(LogStatus.PASS, "Test Case Passed");
				driver.quit();
			
			//OperationQuotationPageJourney.Operation_Payment();
			
			//WriteExcel.setCellData1("Operation_TestCase", TestResult,OperationQuotationPageJourney.ThankyoupageProposal_Pol_num, n, 2, 3);

			 //driver.quit();
			
				}
				catch(AssertionError e) {
					//WriteExcel.setCellData("Operation_TestCase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
					//driver.close();
				}
				
				catch(Exception e) {
					//WriteExcel.setCellData("Operation_TestCase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
					//driver.close();
				}
				
		}
		}
}
