package com.faveo.org.supermediclaim;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.faveo.edit.Edit;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.Base.DataVerificationShareProposalPage;
import com.org.faveo.Base.DataVerificationShareQuotationPage;
import com.org.faveo.Base.HealthInsuranceDropDown;
import com.org.faveo.Base.ReadDataFromEmail;
import com.org.faveo.PolicyJourney.HeartQuotationPageJourney;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.login.LoginCase;
import com.org.faveo.model.AddonsforProducts;
import com.org.faveo.utility.ReadExcel;
import com.org.faveo.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class Heart extends BaseClass implements AccountnSettingsInterface {
public static Integer n;
	
	@Test(priority=1, enabled=false)
	public static void HeartMediclaim() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("Heart_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("Heart_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Heart_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Care_Freedom_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Heart_Testcase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for(n=2;n<=rowCount;n++) {
			
			try {
		BaseClass.LaunchBrowser();
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
		System.out.println("SuperMediclaim Heart -" +TestCaseName);
		
		//Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		
		HealthInsuranceDropDown.Heart();
		HeartQuotationPageJourney.heartquotaionjourney();
		HeartQuotationPageJourney.HeartDropdown();
		HeartQuotationPageJourney.Heart_dragdrop();
		HeartQuotationPageJourney.HeartTenure();
		HeartQuotationPageJourney.Heart_Monthly_frequency();
		//HeartQuotationPageJourney.readPremiumFromFirstPage();
		String Executionstatus=TestCase[n][4].toString().trim();
		if(Executionstatus.equalsIgnoreCase("Edit")) {
			clickElement(By.xpath(critical_xpath));
			Edit.EditHeart();
		}else {
		HeartQuotationPageJourney.Heart_verify_premium();
		}
		HeartQuotationPageJourney.Heart_proposerDetails();
		HeartQuotationPageJourney.Heart_insuredDetails();
		HeartQuotationPageJourney.Heart_questionSet();
		HeartQuotationPageJourney.Heart_Payment();
		
		System.out.println("Completed test case name :"+TestCaseName);
		
			}catch(Exception e) {
				//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				//driver.close();
			}
	continue;	
		}
	}
	
	//********************** Share Quote *************************************
	@Test(priority=2, enabled=false)
	public static void ShareQuote_HeartMediclaim() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("ShareQuotation");
		String[][] TestCaseData=BaseClass.excel_Files("Heart_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Heart_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Care_Freedom_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("ShareQuotation");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for(n=1;n<=6;n++) {
			
	   try {
		//BaseClass.LaunchBrowser();
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
		System.out.println("SuperMediclaim Heart -" +TestCaseName);
		
		//Step 1 - Open Email and Delete any Old Email
		System.out.println("Step 1 - Open Email and Delete Old Email");
		logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");

		LaunchBrowser();
	    ReadDataFromEmail.openAndDeleteOldEmail();
		driver.quit();
		
		// Step 2 - Go to Application and share a Quote and Verify Data in Quotation Tracker
		System.out.println("Step 2- Go to Application and share a Quote and Verify Data in Quotation Tracker");
		logger.log(LogStatus.PASS,"Step 2 - Go to Application and Quote a proposal and Verify Data in Quotation Tracker");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		
		HealthInsuranceDropDown.Heart();
		HeartQuotationPageJourney.heartquotaionjourney();
		HeartQuotationPageJourney.HeartDropdown();
		HeartQuotationPageJourney.Heart_dragdrop();
		HeartQuotationPageJourney.HeartTenure();
		HeartQuotationPageJourney.Heart_Monthly_frequency();
		
		AddonsforProducts.readPremiumFromFirstPage();

		ReadDataFromEmail.readAgeGroup();

		DataVerificationShareQuotationPage.clickOnShareQuotationButton();
		DataVerificationShareQuotationPage.setDataOfSharequotationPopupBox(n);
		DataVerificationShareQuotationPage.verifyDataOfShareQuotationInQuotationTracker_ForSuperMediclaimHeart(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
        driver.quit();
        
     // Step 3 - Open Email and Verify Data in Email Body
     	System.out.println("Step 3 - Open Email and Verify Data in Email Body");
     	logger.log(LogStatus.PASS, "Step 3 - Open Email and Verify Data in Email Body");
     	LaunchBrowser();
     	DataVerificationShareQuotationPage.verifyDataOfShareQuotationInMail_ForSuperMediclaimHeart(n);
     	logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));

     	// Step 4 - Click on Buy Now Button from Email Body and punch the Policy
        System.out.println("Click on Buy Now Button from Email Body and punch the Policy");
     	logger.log(LogStatus.PASS, "Click on Buy Now Button from Email Body and punch the Policy");

     	ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
     	
		HeartQuotationPageJourney.Heart_proposerDetails();
		HeartQuotationPageJourney.Heart_insuredDetails();
		HeartQuotationPageJourney.Heart_questionSet();
		try {
			(new WebDriverWait(driver, 40)).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//a[@ng-click='payOnlinePayment(summaryObj);']")))
					.click();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("unable to load psoposal summary payment method page :");
		}

		PayuPage_Credentials();
		
		//OperationQuotationPageJourney.Operation_Payment();
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();

		// Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker
		System.out.println(
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Tracker");
		logger.log(LogStatus.PASS,
						"Step 5 - Again Login the application and verify after punch the policy data should be not available in quotation Trackery");
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
				DataVerificationShareQuotationPage
						.verifyDataShouldbeNotAvaialbleInOfShareQuotationInQuotationTracker_ForSuperMediclaimHeart(n);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		driver.quit();

		// Step 6- Again Open the Email and Verify GUID Should be Reusable
		System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
		logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

		LaunchBrowser();
		ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

		// Verify the Page Title
		DataVerificationShareProposalPage.verifyPageTitle();

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");

		driver.quit();
		
	    }catch(Exception e) {
		//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
		System.out.println(e.getMessage());
	    logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
		logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
		driver.quit();
			}
	   
	   catch(AssertionError e) {
			//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
			System.out.println(e.getMessage());
		    logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
			driver.quit();
				}
	continue;	
		}
	}
	
	//**********   Share Proposal *******************************************************************
	@Test(priority=3, enabled=false)
	public static void verifyDataOfShareProposalInDraftHistory() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("Heart_Testcase");
		String[][] TestCaseData=BaseClass.excel_Files("Heart_Quotation");
		String[][] FamilyData=BaseClass.excel_Files("Heart_Insured_Details");
		String[][] QuestionSetData=BaseClass.excel_Files("Care_Freedom_QuestionSet");
		
		ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
		int rowCount = fis.getRowCount("Heart_Testcase");
		System.out.println("Total Number of Row in Sheet : "+rowCount);
		
		for(n=3;n<=3;n++) {
			
		try {
		
		String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
		logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
		System.out.println("SuperMediclaim Heart -" +TestCaseName);
		
		
		// Step 1 - Open the application And Share a Proposal and VerifyData in Draft History
		System.out.println("Step 1- Open the application And Share a Proposal and VerifyData in Draft History");
		logger.log(LogStatus.PASS, "Stap 1- Open the application And Share a Proposal and VerifyData in Draft History");
		LaunchBrowser();
		
		//BaseClass.LaunchBrowser();
		//Login with the help of Login Case class
		LoginCase.LoginwithValidCredendial();
		
		HealthInsuranceDropDown.Heart();
		HeartQuotationPageJourney.heartquotaionjourney();
		HeartQuotationPageJourney.HeartDropdown();
		HeartQuotationPageJourney.Heart_dragdrop();
		HeartQuotationPageJourney.HeartTenure();
		HeartQuotationPageJourney.Heart_Monthly_frequency();
		/*//HeartQuotationPageJourney.readPremiumFromFirstPage();
		String Executionstatus=TestCase[n][4].toString().trim();
		if(Executionstatus.equalsIgnoreCase("Edit")) {
			clickElement(By.xpath(critical_xpath));
			Edit.EditHeart();
		}else {
		HeartQuotationPageJourney.Heart_verify_premium();
		}*/
		
		// Click on BuyNow Button
		try {
			Thread.sleep(3000);
			clickElement(By.xpath(Button_Buynow_xpath));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		
		HeartQuotationPageJourney.Heart_proposerDetails();
		HeartQuotationPageJourney.Heart_insuredDetails();
		
		//Set Details of Share Proposal Popup Box and Verify Data in Draft History
		DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
		DataVerificationShareProposalPage.verifyDataOfShareProposalInDraftHistory_ForSuperMedicliamHeart(n);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		
		//Click on Resume Policy
		DataVerificationShareProposalPage.setDetailsAfterResumePolicy();
		
		HeartQuotationPageJourney.Heart_questionSet();
		HeartQuotationPageJourney.Heart_Payment();
		
		//Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History
		System.out.println("Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
		logger.log(LogStatus.PASS, "Step 2 - Again Login the application and verify shared proposal data should not be available in Draft History");
        driver.quit();
        
		LaunchBrowser();
		LoginCase.LoginwithValidCredendial();
		DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForSuperMedicliamHeart(n);

		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");	
		
		 driver.quit();
		
		//System.out.println("Completed test case name :"+TestCaseName);
		
			}
		
		catch(AssertionError e) {
			//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
			driver.quit();
		}
		catch(Exception e) {
				//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				driver.quit();
			}
	continue;	
		}
	}
	
	//**********   Share Proposal *******************************************************************
		@Test(priority=4, enabled=true)
		public static void verifyDataOfShareProposalInDraftHistoryAndEmail() throws Exception {
			String[][] TestCase=BaseClass.excel_Files("Heart_Testcase");
			String[][] TestCaseData=BaseClass.excel_Files("Heart_Quotation");
			String[][] FamilyData=BaseClass.excel_Files("Heart_Insured_Details");
			String[][] QuestionSetData=BaseClass.excel_Files("Care_Freedom_QuestionSet");
			
			ReadExcel fis = new ReadExcel(System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx");
			int rowCount = fis.getRowCount("Heart_Testcase");
			System.out.println("Total Number of Row in Sheet : "+rowCount);
			
			for(n=4;n<=6;n++) {
				
			try {
			
			String TestCaseName = (TestCase[n][0].toString().trim() +" - " +TestCase[n][1].toString().trim());
			logger = extent.startTest("SuperMediclaim Heart -" + TestCaseName);
			System.out.println("SuperMediclaim Heart -" +TestCaseName);
			
			
			//Step 1 - Open Email and Delete Old Email
			System.out.println("Step 1 - Open Email and Delete Old Email");
			logger.log(LogStatus.PASS, "Step 1 - Open Email and Delete Old Email");
			
			LaunchBrowser();
			ReadDataFromEmail.openAndDeleteOldEmail();
			driver.quit(); 
			
			//Step 2 - Go to Application and share a proposal
			System.out.println("Step 2 - Go to Application and share a proposal");
			logger.log(LogStatus.PASS, "Step 2 - Go to Application and share a proposal");
			LaunchBrowser();
			
			//BaseClass.LaunchBrowser();
			//Login with the help of Login Case class
			LoginCase.LoginwithValidCredendial();
			
			HealthInsuranceDropDown.Heart();
			HeartQuotationPageJourney.heartquotaionjourney();
			HeartQuotationPageJourney.HeartDropdown();
			HeartQuotationPageJourney.Heart_dragdrop();
			HeartQuotationPageJourney.HeartTenure();
			HeartQuotationPageJourney.Heart_Monthly_frequency();
			
			// Verify TotalMembers from Excel and Quotation Page
			Thread.sleep(5000);
			AddonsforProducts.readPremiumFromFirstPage();
            ReadDataFromEmail.readAgeGroup(); 
			
			try {
				Thread.sleep(3000);
				clickElement(By.xpath(Button_Buynow_xpath));
				System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
			} catch (Exception e) {
				System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
			}
			
			HeartQuotationPageJourney.Heart_proposerDetails();
			HeartQuotationPageJourney.Heart_insuredDetails();
			
			//Set Details of Share Proposal Popup Box and Verify Data in Draft History
			DataVerificationShareProposalPage.setDetailsOfShareProposalPoupBox(n);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			//logger.log(LogStatus.PASS, "Test Case Passed");
			driver.quit();
			
			//Step 3- Again Open the email and verify the data of Share Proposal in Email Body
			System.out.println("Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			logger.log(LogStatus.PASS, "Step 3 - Again Open the email and verify the data of Share Proposal in Email Body");
			
		    LaunchBrowser();
		    DataVerificationShareProposalPage.verifyDataOfShareProposalInMail_ForSuperMedicliamHeart(n);
		    logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			//logger.log(LogStatus.PASS, "Test Case Passed");
			
			//Step 4 - Click on Buy Now button from Email Body and punch the policy
			System.out.println("Step 4 - Open Email and Click on Buy Now button and punch the policy");
			logger.log(LogStatus.PASS, "Step 4 - Open Email and Click on Buy Now button and punch the policy");
			
			ReadDataFromEmail.ClickOnBuyNowButtonFromEmail();
			
			DataVerificationShareProposalPage.clickOnNextButtonFromProposarDetailsPage();
			DataVerificationShareProposalPage.clickOnNextButtonFromInsuredDetailsPage();
			
			HeartQuotationPageJourney.Heart_questionSet();
			HeartQuotationPageJourney.Heart_Payment();
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			driver.quit();
			
			//Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History
			System.out.println("Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");
			logger.log(LogStatus.PASS, "Step 5 - Again Login the application and verify shared proposal data should not be available in Draft History");

			LaunchBrowser();
			LoginCase.LoginwithValidCredendial();
			DataVerificationShareProposalPage.verifyDataInOfShareProposalShouldNotBeAvailableInDraftHistory_ForCareWithNCB(n);

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			//logger.log(LogStatus.PASS, "Test Case Passed");	
			driver.quit();
			
			// Step 6- Again Open the Email and Verify GUID Should be Reusable
			System.out.println("Step 6 - Again Open the Email and Verify GUID Should be Reusable");
			logger.log(LogStatus.PASS, "Step 6 - Again Open the Email and Verify GUID Should be Reusable");

			LaunchBrowser();
			ReadDataFromEmail.OpenEmailAndClickOnBuyNowButton(n);

			// Verify the Page Title
			DataVerificationShareProposalPage.verifyPageTitle();   

			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
			logger.log(LogStatus.PASS, "Test Case Passed");
			 driver.quit();  
			
			//System.out.println("Completed test case name :"+TestCaseName);
			
		}
			catch(AssertionError e) {
				//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
				System.out.println(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
				driver.quit();
			}
			catch(Exception e) {
					//WriteExcel.setCellData("Heart_Testcase", "Fail", n, 3);
					System.out.println(e.getMessage());
					logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
					logger.log(LogStatus.FAIL, "Test Case is Failed Because getting : "+e.getMessage());
					driver.quit();
				}
		continue;	
			}
		}

}
