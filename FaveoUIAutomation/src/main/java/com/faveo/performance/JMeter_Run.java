package com.faveo.performance;	
import java.io.BufferedReader;
	import java.io.File;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.text.DateFormat;
	import java.text.SimpleDateFormat;
	import java.util.Date;

	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;


	public class JMeter_Run {


		//public static String Test1="C:\\Users\\bijaya\\Desktop\\NCB_QC\\NCB_Floater.jmx";
		@BeforeTest
		public static void setUp() {
			System.out.println("*************************Jmeter Test Started*****************************");
		}
		@Test(priority=1)
		public static void Demo_Test_ready_10() throws Exception {
			runJmx("NCB_Floater_TestPlan_10",10);
		}
	/*	public static void NCBFloatermessage() {
			System.out.println("*****************************************NCB Floater Started********************************");
		}
		@Test(priority=1)
		public static void Execution_NCB_Floter_50() throws Exception {
		
			runJmx("NCB_Floater",50);

		}
		
		@Test(priority=2)
		public static void Execution_NCB_Floter_100() throws Exception {
			runJmx("NCB_Floater",100);
			

		}
		@Test(priority=3)
		public static void NCB_Floter_TestPlan_200() throws Exception {
			runJmx("NCB_Floater",200);
		}
		
		
		public static void NCBmessage() {
			System.out.println("*****************************************NCB Individual Started********************************");
		}
		
		
		@Test(priority=4)
		public static void NCB_Individual_TestPlan_50() throws Exception {
			runJmx("NCB_Individual",50);
		}	
		@Test(priority=5)
		public static void NCB_Individual_TestPlan_100() throws Exception {
			runJmx("NCB_Individual",100);
		}
		
		@Test(priority=6)
		public static void NCB_Individual_TestPlan_200() throws Exception {
			runJmx("NCB_Individual",200);
		}
		
		
		public static void message_() {
			System.out.println("*****************************************Supersaver floater Started********************************");
		}
		
		
		@Test(priority=7)
		public static void SuperSaver_Floater_TestPlan_50() throws Exception {
			runJmx("SuperSaver_Floater",50);
		}	
		@Test(priority=8)
		public static void SuperSaver_Floater_TestPlan_100() throws Exception {
			runJmx("SuperSaver_Floater",100);
		}*/
		
	/*	@Test(priority=9)
		public static void SuperSaver_Floater_TestPlan_200() throws Exception {
			runJmx("SuperSaver_Floater",200);
		
		}
		
		
		public static void message() {
			System.out.println("*****************************************Supersaver Individual Started********************************");
		}
		
		
		@Test(priority=10)
		public static void SuperSaver_Individual_TestPlan_50() throws Exception {
			runJmx("SuperSaver_Individual",50);
		}	
		@Test(priority=11)
		public static void SuperSaver_Individual_TestPlan_100() throws Exception {
			runJmx("SuperSaver_Individual",100);
		}
		
		@Test(priority=12)
		public static void SuperSaver_Individual_TestPlan_200() throws Exception {
			runJmx("SuperSaver_Individual",200);
		
		}
		
		public static void FreeDomMessage() {
			System.out.println("***************************************Freedom Floater Started********************************************************");
		}
		
		@Test(priority=13)
		public static void freedom_floater_TestPlan_50() throws Exception {
			runJmx("freedom_floater",50);
		}	
		@Test(priority=14)
		public static void freedom_floater_TestPlan_100() throws Exception {
			runJmx("freedom_floater",100);
		}
		
		@Test(priority=15)
		public static void freedom_floater_TestPlan_200() throws Exception {
			runJmx("freedom_floater",200);
		
		}
		
		public static void FreedomMessage() {
			System.out.println("***************************************Freedom Individual Started********************************************************");
		}
		
		@Test(priority=16)
		public static void freedom_Individual_TestPlan_50() throws Exception {
			runJmx("freedom_Individual",50);
		}*/	
/*		@Test(priority=17)
		public static void freedom_Individual_TestPlan_100() throws Exception {
			runJmx("freedom_Individual",100);
		}
		
		@Test(priority=18)
		public static void freedom_Individual_TestPlan_200() throws Exception {
			runJmx("freedom_Individual",200);
		
		}
		
		public static void GlobalMessage() {
			System.out.println("***************************************Global Started********************************************************");
		}
		
		@Test(priority=19)
		public static void Global_Individual_TestPlan_50() throws Exception {
			runJmx("Global_Individual",50);
		}	
		@Test(priority=20)
		public static void Global_Individual_TestPlan_100() throws Exception {
			runJmx("Global_Individual",100);
		}
		
		@Test(priority=21)
		public static void Global_Individual_TestPlan_200() throws Exception {
			runJmx("Global_Individual",200);
		
		}
		
		public static void HNIMessage() {
			System.out.println("***************************************HNI Started********************************************************");
		}
		
		
		@Test(priority=22)
		public static void HNI_TestPlan_50() throws Exception {
			runJmx("HNI",50);
		}	
		@Test(priority=23)
		public static void HNI_TestPlan_100() throws Exception {
			runJmx("HNI",100);
		}
		
		@Test(priority=24)
		public static void HNI_TestPlan_200() throws Exception {
			runJmx("HNI",200);
		
		}
		
		public static void SuperSelectMessage() {
			System.out.println("***************************************Super Select Floater Started********************************************************");
		}
		
		@Test(priority=25)
		public static void SuperSelect_Floater_TestPlan_50() throws Exception {
			runJmx("SuperSelect_Floater",50);
		}	
		@Test(priority=26)
		public static void SuperSelect_Floater_TestPlan_100() throws Exception {
			runJmx("SuperSelect_Floater",100);
		}
		
		@Test(priority=27)
		public static void SuperSelect_Floater_TestPlan_200() throws Exception {
			runJmx("SuperSelect_Floater",200);
		
		}
		
		public static void SuperSelectMessage2() {
			System.out.println("***************************************Super Select Individual Started********************************************************");
		}

		@Test(priority=28)
		public static void SuperSelect_Individual_TestPlan_50() throws Exception {
			runJmx("SuperSelect_Individual",50);
		}	
		@Test(priority=29)
		public static void SuperSelect_Individual_TestPlan_100() throws Exception {
			runJmx("SuperSelect_Individual",100);
		}
		
		@Test(priority=30)
		public static void SuperSelect_Individual_TestPlan_200() throws Exception {
			runJmx("SuperSelect_Individual",200);
		
		}
		
		public static void Assure() {
			System.out.println("***************************************Assure Started********************************************************");
		}
		@Test(priority=31)
		public static void Assure_TestPlan_50() throws Exception {
			runJmx("Assure",50);
		}	
		@Test(priority=32)
		public static void Assure_TestPlan_100() throws Exception {
			runJmx("Assure",100);
		}
		
		@Test(priority=33)
		public static void Assure_TestPlan_200() throws Exception {
			runJmx("Assure",200);
		
		}*/
		
		//Run multiple products jmx
		public  static void runJmx(String productname, int runcount ) throws Exception {
			
			System.out.println("*************************Jmeter Test Started for"+productname+"_"+runcount+" *****************************");
			DateFormat df = new SimpleDateFormat("MM_dd_yy HH_mm_ss"); 
			Date dateobj = new Date(); 
			String dt=df.format(dateobj);
			System.out.println(dt);
			String JmeterPath = "D:\\Bijaya\\bij\\downloads\\apache-jmeter-4.0\\bin\\";
			                
			String jmxFile = "C:\\Users\\bijaya\\Desktop\\NCB_Floater_TestPlan_10.jmx";
			
			//C:\Users\bijaya\Desktop
			String csvfile = productname+"_TestPlan_"+runcount+"_"+df.format(dateobj)+".csv";
			String outputfile = productname+"_TestPlan_"+runcount+"_" +df.format(dateobj)+".html";
			System.out.println("CSV file name is  : "+csvfile+"-----"+"output File is : "+outputfile);
			String[] command = {"jmeter.bat", "-n", "-t", jmxFile, "-l" , csvfile, "-e" , "-o" ,"F:\\JMeter_OutPut\\" +outputfile};
			//String[] command = {"ipconfig"};
			ProcessBuilder pb = new ProcessBuilder("jmeter.bat", "-n", "-t", jmxFile, "-l" , csvfile, "-e" , "-o" ,"F:\\JMeter_OutPut\\" +outputfile);

				pb.redirectOutput(new File("D:\\Bijaya\\bij\\downloads\\apache-jmeter-4.0\\bin\\"));
			/*ProcessBuilder builder1 = new ProcessBuilder(command);
			builder1 = builder1.directory(new File(JmeterPath));*/
			try {
				Process p1 = pb.start();
				System.out.println("ran successfully");
				BufferedReader input = new BufferedReader(new InputStreamReader(p1.getInputStream()));

				String line=null;

				while((line=input.readLine()) != null) {
					System.out.println(line);
				}

				int exitVal = p1.waitFor();
				System.out.println("Exited with error code "+exitVal);
			} catch (IOException e) {
				
				e.printStackTrace();
			}

		}
		
	}


