package com.faveo.edit;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.faveo.org.supermediclaim.Heart;
import com.faveo.org.supermediclaim.Operation;
import com.org.faveo.Base.BaseClass;
import com.org.faveo.PolicyJourney.DomesticTravelPageJourney;
import com.org.faveo.fixedbenefitinsurance.Secure;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.org.faveo.healthinsurance.CareFreedomPolicy;
import com.org.faveo.healthinsurance.CareGlobalPolicy;
import com.org.faveo.healthinsurance.CareHNIPolicy;
import com.org.faveo.healthinsurance.CareHeart;
import com.org.faveo.healthinsurance.CareSmartSelectPolicy;
import com.org.faveo.healthinsurance.CareWithNCB;
import com.org.faveo.healthinsurance.CareWithOPD;
import com.org.faveo.healthinsurance.POSCareSenior;
import com.org.faveo.healthinsurance.POSCareSuperSaver;
import com.org.faveo.healthinsurance.POSCareWithNCB;
import com.org.faveo.healthinsurance.PosCareFreedomPolicy;
import com.org.faveo.healthinsurance.SuperSaverPolicy;
import com.org.faveo.travelInsurance.DomesticTravel;
import com.org.faveo.travelInsurance.Explore;
import com.org.faveo.travelInsurance.GroupStudentExplore;
import com.org.faveo.travelInsurance.POSExplore;
import com.relevantcodes.extentreports.LogStatus;

public class Edit extends BaseClass implements AccountnSettingsInterface {
	// CareWithNCB care=new CareWithNCB();
	public static String afterEdit_Proposalpremium_value;
	public static String afterEdit_Proposalpremium_value_super;
	public static String Quotationpremium_value_Edit;
	public static String Premium = null;
	public static String Premium1 = null;
	public static String Premium2 = null;
	public static String PolicyType = null;

	public static void careEdit() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("Test_Cases");
		String[][] TestCaseData = BaseClass.excel_Files("Care_Quotation_Data");
		String[][] editCareNCB = BaseClass.excel_Files("CareWithNCB_Edit");
		String[][] FamilyData = BaseClass.excel_Files("Insured_Details");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(5000);
		CareWithNCB cb = new CareWithNCB();

		// Total member and covertype and member details scripting starts here

		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();

				if (DropDownName.getText().equals(TestCaseData[cb.n][15].toString().trim())) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By
							.xpath("//*[@class=\"dropdown year_drop_slect master open\"]/ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
									+ TestCaseData[cb.n][15].toString().trim() + "]"))
							.click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[cb.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[cb.n][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = editCareNCB[cb.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							/*
							 * for (WebElement ListData : List) {
							 * 
							 * if
							 * (ListData.getText().contains(FamilyData[mcount][0
							 * ].toString().trim())) {
							 * System.out.println("Selcted Age Of Member :" +
							 * ListData.getText());
							 * 
							 * ListData.click();
							 * 
							 * if (count == membersSize) { break outer; } else {
							 * count = count + 1; break member; }
							 * 
							 * }
							 * 
							 * }
							 */
							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[cb.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[cb.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[cb.n][31].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = editCareNCB[cb.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = editCareNCB[cb.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		String SumInsured = editCareNCB[cb.n][2].toString().trim();
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(editCareNCB[cb.n][3].toString().trim());
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}

		String TotalMemberPresentonQuotation = driver
				.findElement(
						By.xpath("//*[@class='col-md-12 quotqtion_client_white ng-scope']/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		scrolldown();
		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[3]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();

			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}

	}

	public static void EditCareSuperSaver() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Test_Cases_SuperSaver");
		String[][] TestCaseData = BaseClass.excel_Files("SuperSaver_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("SuperSaver_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("SuperSaver_QuestionSet");
		String[][] editcaresupersaver = BaseClass.excel_Files("CareSupersaver_Edit");
		scrollup();
		clickElement(By.xpath(Edit_CareSuperSaver_xpath));
		Thread.sleep(5000);
		SuperSaverPolicy css = new SuperSaverPolicy();

		// Total member and covertype and member details scripting starts here
		List<WebElement> dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		// List<WebElement> dropdown =
		// driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();
				String memberexcel = TestCaseData[css.n][15].toString().trim();
				if (DropDownName.getText().equals(memberexcel)) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+ memberexcel+
					// "]/a")).click();
					driver.findElement(By
							.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/ul/li["
									+ memberexcel + "]/a"))
							.click();
					// *[@id="editGetQuot"]/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/ul/li[2]/a
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[css.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		// Thread.sleep(3000);

		// again call the dropdown
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size());
		int membersSize = Integer.parseInt(TestCaseData[css.n][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = editcaresupersaver[css.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;
							// driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown
							// year_drop_slect
							// master']//a[@class='dropdown-toggle dropdown11
							// dropdown_focus dropdown_month_tab care
							// ng-binding']")).click();
							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
							System.out.println("List Data is : " + List);
							/*
							 * for (WebElement ListData : List) {
							 * 
							 * if
							 * (ListData.getText().contains(FamilyData[mcount][0
							 * ].toString().trim())) {
							 * System.out.println("Selcted Age Of Member :" +
							 * ListData.getText());
							 * 
							 * ListData.click();
							 * 
							 * if (count == membersSize) { break outer; } else {
							 * count = count + 1; break member; }
							 * 
							 * }
							 * 
							 * }
							 */
							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[css.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[css.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[css.n][26].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = editcaresupersaver[css.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = editcaresupersaver[css.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		String SumInsured = editcaresupersaver[css.n][2].toString().trim();
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(editcaresupersaver[css.n][3].toString().trim());
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}

		/*
		 * String TotalMemberPresentonQuotation = driver.findElement(By.xpath(
		 * "//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/a"
		 * )).getText();
		 * System.out.println("Total Number of Member on Quotation Page : " +
		 * TotalMemberPresentonQuotation);
		 */

		String afterEdit_Quotationpremium_value_super = driver
				.findElement(
						By.xpath("//*[@class='col-md-12 quotqtion_client_white ng-scope']/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value_super);
		logger.log(LogStatus.PASS,
				"After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value_super);

		try {
			Thread.sleep(3000);
			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on update premium Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println(
					"Test Case is Failed Because Unable to click on update premium Button from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value_super = driver.findElement(By.xpath("//p[@class='amount ng-binding']"))
				.getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value_super);
		logger.log(LogStatus.PASS,
				"After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value_super);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value_super, afterEdit_Proposalpremium_value_super);
			logger.log(LogStatus.INFO,
					"Quotaion Premium and Proposal Premium is Verified and Both are Same : "
							+ afterEdit_Proposalpremium_value_super.substring(1,
									afterEdit_Proposalpremium_value_super.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value_super);
			logger.log(LogStatus.PASS, "Quotaion Premium and Proposal Premium are  Same");

		}

	}

	public static void EditCareFreedom() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Test_Cases_CareFreedom");
		String[][] TestCaseData = BaseClass.excel_Files("Care_Freedom_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("Care_Freedom_Insured_Deatils");
		String[][] QuestionSetData = BaseClass.excel_Files("Care_Freedom_QuestionSet");
		String[][] editcarefredom = BaseClass.excel_Files("CareFredom_Edit");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(5000);
		CareFreedomPolicy cf = new CareFreedomPolicy();

		// Total member and covertype and member details scripting starts here

		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();
				String membersizefreedom = TestCaseData[cf.n][15].toString().trim();
				if (DropDownName.getText().equals(membersizefreedom)) {

					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By
							.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/ul/li["
									+ membersizefreedom + "]/a"))
							.click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[cf.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}

			}
		}

		catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[cf.n][15].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = editcarefredom[cf.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));
							System.out.println("List Size is : " + List.size());
							/*
							 * for (WebElement ListData : List) {
							 * 
							 * if
							 * (ListData.getText().contains(FamilyData[mcount][0
							 * ].toString().trim())) {
							 * System.out.println("Selcted Age Of Member :" +
							 * ListData.getText());
							 * 
							 * ListData.click();
							 * 
							 * if (count == membersSize) { break outer; } else {
							 * count = count + 1; break member; }
							 * 
							 * }
							 * 
							 * }
							 */
							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[cf.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[cf.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("90 Days - 5 Yrs")
									|| DropDowns.getText().equals("6 - 17 Yrs")
									|| DropDowns.getText().equals("18 - 35 Yrs")
									|| DropDowns.getText().equals("36 - 40 Yrs")
									|| DropDowns.getText().equals("41 - 45 Yrs")
									|| DropDowns.getText().equals("46 - 50 Yrs")
									|| DropDowns.getText().equals("51 - 55 Yrs")
									|| DropDowns.getText().equals("56 - 60 Yrs")
									|| DropDowns.getText().equals("61 - 65 Yrs")
									|| DropDowns.getText().equals("66 - 70 Yrs")
									|| DropDowns.getText().equals("71 - 75 Yrs")
									|| DropDowns.getText().equals("76 - 80 Yrs")
									|| DropDowns.getText().equals("Above 80 Yrs")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[cf.n][25].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = editcarefredom[cf.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("90 Days - 5 Yrs")
								|| DropDowns.getText().equals("6 - 17 Yrs") || DropDowns.getText().equals("18 - 35 Yrs")
								|| DropDowns.getText().equals("36 - 40 Yrs")
								|| DropDowns.getText().equals("41 - 45 Yrs")
								|| DropDowns.getText().equals("46 - 50 Yrs")
								|| DropDowns.getText().equals("51 - 55 Yrs")
								|| DropDowns.getText().equals("56 - 60 Yrs")
								|| DropDowns.getText().equals("61 - 65 Yrs")
								|| DropDowns.getText().equals("66 - 70 Yrs")
								|| DropDowns.getText().equals("71 - 75 Yrs")
								|| DropDowns.getText().equals("76 - 80 Yrs")
								|| DropDowns.getText().equals("Above 80 Yrs")) {

							String EditMembersdetails = editcarefredom[cf.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver
										.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		String SumInsured = editcarefredom[cf.n][2].toString().trim();
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(editcarefredom[cf.n][3].toString().trim());
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}

		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);

		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[5]/p[1]")).getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}

	}

	public static void EditCareGlobal() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Test_Cases_CareGlobal");
		String[][] TestCaseData = BaseClass.excel_Files("CareGlobal_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareGlobal_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("CareGlobal_QuestionSet");
		String[][] editcareglobal = BaseClass.excel_Files("CareGlobal_Edit");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(5000);
		CareGlobalPolicy cgp = new CareGlobalPolicy();

		// Total member and covertype and member details scripting starts here

		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			/*
			 * for (WebElement DropDownName : dropdown) {
			 * System.out.println("Dropdown text is : "+DropDownName.getText());
			 * DropDownName.click();
			 */
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();
				String membersizecareglobal = TestCaseData[cgp.n][5].toString().trim();
				System.out.println("Member size is :" + membersizecareglobal);
				if (DropDownName.getText().equals(membersizecareglobal)) {

					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ membersizecareglobal + "]")).click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[cgp.n][4].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		}

		catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[cgp.n][5].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = editcareglobal[cgp.n][1].toString().trim();
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));
							/*
							 * for (WebElement ListData : List) {
							 * 
							 * if
							 * (ListData.getText().contains(FamilyData[mcount][0
							 * ].toString().trim())) {
							 * System.out.println("Selcted Age Of Member :" +
							 * ListData.getText());
							 * 
							 * ListData.click();
							 * 
							 * if (count == membersSize) { break outer; } else {
							 * count = count + 1; break member; }
							 * 
							 * }
							 * 
							 * }
							 */
							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[cgp.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[cgp.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[cgp.n][7].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = editcareglobal[cgp.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = editcareglobal[cgp.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		String SumInsured = editcareglobal[cgp.n][2].toString().trim();
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(editcareglobal[cgp.n][3].toString().trim());
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}

		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[1]/div/div/div/div[2]/label[2]/span"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);

		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[5]/p[1]")).getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}

	}

	public static void EditcareHNI() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("TestCase_HNI");
		String[][] TestCaseData = BaseClass.excel_Files("CareHNI_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("CareHNI_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("CareHNI_QuestionSet");
		String[][] editcareHNI = BaseClass.excel_Files("CareHNI_Edit");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		System.out.println("Edit start here for care HNI");
		Thread.sleep(5000);
		CareHNIPolicy chni = new CareHNIPolicy();
		// Total member and covertype and member details scripting starts here

		List<WebElement> dropdown = driver.findElements(By.xpath(AllDropdown_Xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				Fluentwait(By.xpath(AllDropdown_Xpath));
				DropDownName.click();
				int TotalMemberQuotation = Integer.parseInt(TestCaseData[chni.n][5].toString().trim());
				System.out.println("Total Number of member in Excel Sheet : " + TotalMemberQuotation);
				if (DropDownName.getText().equals(TotalMemberQuotation)) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ TotalMemberQuotation + "]")).click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[chni.n][5].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		}

		catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[chni.n][5].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = editcareHNI[chni.n][1].toString().trim();
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[chni.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[chni.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("25 to 35 Years")
									|| DropDowns.getText().equals("36 to 40 Years")
									|| DropDowns.getText().equals("41 to 45 Years")
									|| DropDowns.getText().equals("46 to 50 Years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("> 75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[chni.n][7].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = editcareHNI[chni.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}
							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("18 to 24 Years")
								|| DropDowns.getText().equals("25 to 35 Years")
								|| DropDowns.getText().equals("36 to 40 Years")
								|| DropDowns.getText().equals("41 to 45 Years")
								|| DropDowns.getText().equals("46 to 50 Years")) {

							String EditMembersdetails = editcareHNI[chni.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		String SumInsured = editcareHNI[chni.n][2].toString().trim();
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(editcareHNI[chni.n][3].toString().trim());
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}

		// Scroll Window Up
		BaseClass.scrolldown();

		// Verify TotalMembers from Excel and Quotation Page
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		/*
		 * String TotalMemberPresentonQuotation = driver.findElement(By.xpath(
		 * "//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a/text()"
		 * )).getText();
		 * System.out.println("Total Number of Member on Quotation Page : " +
		 * TotalMemberPresentonQuotation);
		 */

		// Capture The Premium value
		Thread.sleep(7000);
		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[2]/b/p/span"))
				.getText();
		System.out.println("Total Premium Value on Quotation Page is :" + " - "
				+ afterEdit_Quotationpremium_value.substring(1, afterEdit_Quotationpremium_value.length()));
		scrolldown();
		// Click on BuyNow Button
		try {
			Thread.sleep(3000);
			clickElement(By.xpath("//*[@class='share_qution_btn padding-top-15']/button"));
			System.out.println("Sucessfully Clicked on Buy Now Button from Quotation Page.");
		} catch (Exception e) {

			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");

		}

	}

	public static void Editcaresmartselect() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("Test_Cases_SmartSelect");
		String[][] TestCaseData = BaseClass.excel_Files("SmartSelect_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("SmartSelect_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("SmartSelect_QuestionSet");
		String[][] Editsmartselect = BaseClass.excel_Files("Editcaresmartselect");
		CareSmartSelectPolicy cssp = new CareSmartSelectPolicy();

		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for care Smart select");

		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();

				if (DropDownName.getText().equals(TestCaseData[cssp.n][5].toString().trim())) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By
							.xpath("//*[@class=\"dropdown year_drop_slect master open\"]/ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
									+ TestCaseData[cssp.n][5].toString().trim() + "]"))
							.click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[cssp.n][5].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[cssp.n][5].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = Editsmartselect[cssp.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							/*
							 * for (WebElement ListData : List) {
							 * 
							 * if
							 * (ListData.getText().contains(FamilyData[mcount][0
							 * ].toString().trim())) {
							 * System.out.println("Selcted Age Of Member :" +
							 * ListData.getText());
							 * 
							 * ListData.click();
							 * 
							 * if (count == membersSize) { break outer; } else {
							 * count = count + 1; break member; }
							 * 
							 * }
							 * 
							 * }
							 */
							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[cssp.n][8].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[cssp.n][5].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[cssp.n][7].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = Editsmartselect[cssp.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = Editsmartselect[cssp.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		Thread.sleep(3000);
		int SumInsured = Integer.parseInt(Editsmartselect[cssp.n][2].toString().trim());
		System.out.println(SumInsured);
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(Editsmartselect[cssp.n][3].toString().trim());
		System.out.println(Tenure);
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
		}
		String membersSizeno = TestCaseData[cssp.n][5].toString().trim();

		if (membersSizeno.equals(TestCaseData[cssp.n][5].toString().trim())) {
			String TotalMemberPresentonQuotation = driver
					.findElement(By
							.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a"))
					.getText();
			System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		} else {
			String TotalMemberPresentonQuotation = driver
					.findElement(By
							.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/a"))
					.getText();
			System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		}
		scrolldown();
		Thread.sleep(5000);
		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[3]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}
	}

	public static void EditPoscarewithNCB() throws Exception {
		String[][] Testcase = BaseClass.excel_Files("POSCare_With_NCB_TestCase");
		String[][] FamilyData = BaseClass.excel_Files("POSCareWIthNCB_FamilyData");
		String[][] TestCaseData = BaseClass.excel_Files("POSCare_With_NCB_TestCaseData");
		String[][] QuestionSetData = BaseClass.excel_Files("POSCareWithNCB_Question");
		String[][] Editposncb = BaseClass.excel_Files("EditPoscareNCB");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for POS care With NCB");
		POSCareWithNCB posncb = new POSCareWithNCB();
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();

				if (DropDownName.getText().equals(TestCaseData[posncb.n][15].toString().trim())) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By
							.xpath("//*[@class=\"dropdown year_drop_slect master open\"]/ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
									+ TestCaseData[posncb.n][15].toString().trim() + "]"))
							.click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[posncb.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[posncb.n][15].toString().trim());
		logger.log(LogStatus.PASS, "selected no of member is  " + membersSize);

		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = Editposncb[posncb.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[posncb.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[posncb.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[posncb.n][31].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = Editposncb[posncb.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = Editposncb[posncb.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Done ");
		}

		int SumInsured = Integer.parseInt(Editposncb[posncb.n][2].toString().trim());
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");
		logger.log(LogStatus.PASS, "selected sum insured is  " + SumInsured);
		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(Editposncb[posncb.n][3].toString().trim());
		logger.log(LogStatus.PASS, "selected tenure is  " + Tenure);
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}
		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);

		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[2]/div[3]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();
			scrolldown();
			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}
	}

	public static void EditPOSCaresupersaver() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("TestCasePOSSuperSaverData");
		String[][] FamilyData = BaseClass.excel_Files("FamilyMembersPOSSuperSaver");
		String[][] TestCaseData = BaseClass.excel_Files("TestCasesPOSSuperSaver");
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSetPOSSuperSaver");
		String[][] EditPOSsupersaver = BaseClass.excel_Files("Editpossupersaver");

		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for POS care With NCB");
		POSCareSuperSaver possupersaver = new POSCareSuperSaver();
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();

				if (DropDownName.getText().equals(TestCaseData[possupersaver.n][15].toString().trim())) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By
							.xpath("//*[@class=\"dropdown year_drop_slect master open\"]/ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
									+ TestCaseData[possupersaver.n][15].toString().trim() + "]"))
							.click();
					System.out.println("---Total Number of Member Selected : "
							+ TestCaseData[possupersaver.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[possupersaver.n][15].toString().trim());
		logger.log(LogStatus.PASS, "selected no of member is  " + membersSize);

		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = EditPOSsupersaver[possupersaver.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[possupersaver.n][17].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText()
									.equals(TestCaseData[possupersaver.n][15].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[possupersaver.n][26].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = EditPOSsupersaver[possupersaver.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = EditPOSsupersaver[possupersaver.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Done ");
		}
		int SumInsured = Integer.parseInt(EditPOSsupersaver[possupersaver.n][2].toString().trim());
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");
		logger.log(LogStatus.PASS, "selected sum insured is  " + SumInsured);
		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(EditPOSsupersaver[possupersaver.n][3].toString().trim());
		logger.log(LogStatus.PASS, "selected tenure is  " + Tenure);
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}
		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		scrolldown();
		scrolldown();
		scrolldown();
		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[2]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {

			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}
	}

	public static void editposcarefreedom() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Test_Case_PosFreedom");
		String[][] FamilyData = BaseClass.excel_Files("Pos_CareFreedom_Insured_Details");
		String[][] TestCaseData = BaseClass.excel_Files("PosCarefreedom_Quotation_Data");
		String[][] QuestionSetData = BaseClass.excel_Files("Pos_CareFreedom_QuestionSet");
		String[][] EditposCareFreedom = BaseClass.excel_Files("EditPOScarefreedom");

		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for POS care Freedom");
		PosCareFreedomPolicy posfreedom = new PosCareFreedomPolicy();
		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());
				DropDownName.click();

				if (DropDownName.getText().equals(TestCaseData[posfreedom.n][5].toString().trim())) {
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By
							.xpath("//*[@class=\"dropdown year_drop_slect master open\"]/ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
									+ TestCaseData[posfreedom.n][5].toString().trim() + "]"))
							.click();
					System.out.println(
							"---Total Number of Member Selected : " + TestCaseData[posfreedom.n][5].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[posfreedom.n][5].toString().trim());
		logger.log(LogStatus.PASS, "selected no of member is  " + membersSize);

		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = EditposCareFreedom[posfreedom.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[posfreedom.n][8].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class='toolbar_plan_name_input']"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[posfreedom.n][5].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("18 to 35 Yrs")
									|| DropDowns.getText().equals("36 to 40 Yrs")
									|| DropDowns.getText().equals("41 to 45 Yrs")
									|| DropDowns.getText().equals("46 - 50 Yrs")
									|| DropDowns.getText().equals("51 - 55 Yrs")
									|| DropDowns.getText().equals("56 - 60 Yrs")
									|| DropDowns.getText().equals("61 - 65 Yrs")
									|| DropDowns.getText().equals("66 - 70 Yrs")
									|| DropDowns.getText().equals("71 - 75 Yrs")
									|| DropDowns.getText().equals("76 - 80 Yrs")
									|| DropDowns.getText().equals("Above 80 Yrs")
									|| DropDowns.getText().equals("90 Days to 5 Yrs")
									|| DropDowns.getText().equals("6 Yrs to 17 Yrs")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[posfreedom.n][7].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = EditposCareFreedom[posfreedom.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("18 to 35 Yrs")
								|| DropDowns.getText().equals("36 to 40 Yrs")
								|| DropDowns.getText().equals("41 to 45 Yrs")
								|| DropDowns.getText().equals("46 - 50 Yrs")
								|| DropDowns.getText().equals("51 - 55 Yrs")
								|| DropDowns.getText().equals("56 - 60 Yrs")
								|| DropDowns.getText().equals("61 - 65 Yrs")
								|| DropDowns.getText().equals("66 - 70 Yrs")
								|| DropDowns.getText().equals("71 - 75 Yrs")
								|| DropDowns.getText().equals("76 - 80 Yrs")
								|| DropDowns.getText().equals("Above 80 Yrs")
								|| DropDowns.getText().equals("90 Days to 5 Yrs")
								|| DropDowns.getText().equals("6 Yrs to 17 Yrs")) {

							String EditMembersdetails = EditposCareFreedom[posfreedom.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver
										.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Yrs')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Done ");
		}
		int SumInsured = Integer.parseInt(EditposCareFreedom[posfreedom.n][2].toString().trim());
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");
		logger.log(LogStatus.PASS, "selected sum insured is  " + SumInsured);
		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(EditposCareFreedom[posfreedom.n][3].toString().trim());
		logger.log(LogStatus.PASS, "selected tenure is  " + Tenure);
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
			// clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
			// clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			// clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : " + Tenure + " Year");
		}
		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);

		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[2]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();
			scrolldown();
			scrolldown();
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}

	}

	public static void EditSecure() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("TestCases_Secure");
		String[][] TestCaseData = BaseClass.excel_Files("Secure_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("Secure_InsuredDetails");
		String[][] QuestionSetData = BaseClass.excel_Files("Secure_QuestionSet");
		String[][] editsecuredata = BaseClass.excel_Files("Secure_Edit_Data");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for Secure");
		Secure editsecure = new Secure();

		Thread.sleep(10000);
		List<WebElement> dropdown = driver.findElements(By.xpath(AllDropdown_xpath));
		int membersSize = Integer.parseInt(TestCaseData[editsecure.n][5].toString().trim());
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int loopcount = membersSize + 1;
		try {
			System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3
			outer: for (WebElement DropDownName : dropdown) {
				DropDownName.click();

				if (DropDownName.getText().equals("18-29") || DropDownName.getText().equals("30-39")
						|| DropDownName.getText().equals("40-44") || DropDownName.getText().equals("45-49")
						|| DropDownName.getText().equals("50-54") || DropDownName.getText().equals("55-59")
						|| DropDownName.getText().equals("60-70")) {
					String Membersdetails = editsecuredata[editsecure.n][1];
					System.out.println("Total Number of Member in Excel :" + Membersdetails);
					if (Membersdetails.contains("") || Membersdetails.contains(" ")) {

						// data taking form test case sheet which is
						BaseClass.membres = Membersdetails.split(" ");
						System.out.println(BaseClass.membres = Membersdetails.split(" "));

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							// List Age of members dropdown
							Fluentwait(By.xpath("//a[@ng-click='selectVal(item)']"));
							List<WebElement> List = driver.findElements(By.xpath("//a[@ng-click='selectVal(item)']"));
							System.out.println("List Data is : " + List);

							for (WebElement ListData : List) {

								if (ListData.getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("AgeBand of Member is :" + ListData.getText());

									ListData.click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										break member;
									}

								}

							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		// Enter Annual Income from Excel
		String AnnualIncome = editsecuredata[editsecure.n][2].toString().trim();
		System.out.println("Entered Annual Income is : " + AnnualIncome);
		driver.findElement(By.xpath("//input[@name='annualIncome']")).clear();
		enterText(By.xpath(AnnualIncome_xpath), AnnualIncome);
		Thread.sleep(5000);
		// Select JobType from Excel
		WebElement jobtypeelem = driver.findElement(By.xpath("//label[@for='RadioJ0q']"));
		if (!jobtypeelem.isDisplayed()) {
			clickElement(By.xpath(Edit_careWithNCB_xpath));
		} else {
			String JobType = editsecuredata[editsecure.n][3].toString().trim();
			System.out.println("Selected Job Type is : " + JobType);
			Thread.sleep(3000);
			if (JobType.contains("Salaried")) {
				// clickElement(By.xpath(Salaried_xpath));
				driver.findElement(By.xpath("//label[@for='RadioJ0q']")).click();
				System.out.println("Job type selected");
			} else if (JobType.contains("Self Employed")) {
				// clickElement(By.xpath(SelfEmployed_xpath));
				driver.findElement(By.xpath("//label[@for='RadioJ1q']")).click();
			}
		}
		// Reading Sum Insured from Excel
		Thread.sleep(3000);
		int SumInsured = Integer.parseInt(editsecuredata[editsecure.n][4].toString().trim());
		System.out.println("Entered Sum Insured is :" + SumInsured);
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		// clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(),
		// " + SumInsured + ")]"));

		// Reading Tenure from Excel
		int Tenure = Integer.parseInt(editsecuredata[editsecure.n][5].toString().trim());
		System.out.println(Tenure);
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
		}

		// Capture The Premium value
		Thread.sleep(5000);
		Quotationpremium_value_Edit = driver.findElement(By.xpath(Edit_QuotationPage_premium_xpath)).getText();
		System.out.println("Total Premium Value on Quotation Page is :" + " - "
				+ Quotationpremium_value_Edit.substring(1, Quotationpremium_value_Edit.length()));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,350)", "");

		try {
			Thread.sleep(3000);
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Sucessfully Clicked on Buy Now Button from Quotation Page.");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");

		}

	}

	public static void ExploreEditStart() throws Exception {
		String[][] TestCase = BaseClass.excel_Files1("Explore_Test_Cases");
		String[][] TestCaseData = BaseClass.excel_Files1("Explore_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("Explore_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files1("Explore_Question_Set");
		String[][] Edit_ExploreData = BaseClass.excel_Files1("Edit_Explore");
		String[][] TestcaseEn = BaseClass.excel_Files("Test_Cases_Care");
		String[][] loginData = BaseClass.excel_Files("Credentials");

		scrollup();
		clickElement(By.xpath(Exploreedit_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for Explore Travel");
		Explore editexplore = new Explore();

		clickElement(By.xpath(Edit_TravellingAllDropdowns_xpath));
		String TravellingTo = Edit_ExploreData[editexplore.n][1].toString().trim();
		System.out.println(TravellingTo);
		clickElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a[contains(text()," + "'"
				+ TravellingTo + "'" + ")]"));
		System.out.println("User is Travelling To : " + TravellingTo);

		//Thread.sleep(5000);
		String StartDate = Edit_ExploreData[editexplore.n][3].toString().trim();
		try {
			Thread.sleep(4000);
			clickElement(By.xpath(StartDateCalander_Id));
			Thread.sleep(2000);
			String spilitter[] = StartDate.split(",");
			String eday = spilitter[0];
			String emonth = spilitter[1];
			String eYear = spilitter[2];
			String oMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			String oYear = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			WebElement Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																								// Button
			while (!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth)))) {
				Thread.sleep(2000);
				Next.click();
				oMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				oYear = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				Next = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
			}
			driver.findElement(
					By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
							+ "'" + eday + "'" + ")]"))
					.click();
			System.out.println("Entered Start_Date on Faveo UI is :" + StartDate);
		} catch (Exception e) {
			System.out.println("Unable to Enter Start Date.");
		}

		// Select End Date from Excel and Write it on UI
		clickElement(By.id(EndDateCalender_Id));
		String EndDate = Edit_ExploreData[editexplore.n][4].toString().trim();
		String spilitter1[] = EndDate.split(",");
		String eday1 = spilitter1[0];
		String emonth1 = spilitter1[1];
		String eYear1 = spilitter1[2];
		String oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
		String oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
		WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																							// Button
		while (!((eYear1.contentEquals(oYear1)) && (emonth1.contentEquals(oMonth1)))) {
			Thread.sleep(2000);
			Next1.click();
			oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
		}
		driver.findElement(
				By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text()," + "'"
						+ eday1 + "'" + ")]"))
				.click();
		Thread.sleep(2000);
		System.out.println("Entered End_Date on Faveo UI is :" + EndDate);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE);

		List<WebElement> dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		try {

			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : " + DropDownName.getText());

				// int Travellers =
				// Integer.parseInt(TestCaseData[editexplore.n][8].toString().trim());
				String Travellers = TestCaseData[editexplore.n][8].toString().trim();
				System.out.println("Dropdown text is : " + DropDownName.getText());
				if (DropDownName.getText().equals(Travellers)) {
					DropDownName.click();
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					clickElement(By
							.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"
									+ "'" + Travellers + "'" + ")]"));
					System.out.println("---Total Number of Member Selected : " + Travellers);
					Thread.sleep(5000);
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("Unable to Select Member.");
		}
		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(Edit_ExploreData[editexplore.n][5].toString().trim());
		System.out.println("Total Number of Member in Excel : " + membersSize);
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int covertype;
		int loopcount = membersSize + 1;

		outer:

		for (WebElement DropDownName : dropdown) {
			if (membersSize == 1) {

				Thread.sleep(5000);
				List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']//a"));

				// reading members from test cases sheet memberlist
				String Membersdetails = Edit_ExploreData[editexplore.n][6];
				System.out.println("Members details is : " + Membersdetails);

				if (Membersdetails.contains("")) {

					BaseClass.membres = Membersdetails.split(",");

					member: for (int i = 0; i <BaseClass.membres.length; i++) {

						mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
						System.out.println("value of Mcount Index : " + mcount);
						mcountindex = mcountindex + 1;

						driver.findElement(By
								.xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'up to 40 Years')]"))
								.click();

						// List Age of members dropdown
						List<WebElement> List = driver
								.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));
						String text = FamilyData[mcount][0].toString().trim();
						/*
						 * for (WebElement ListData : List) { if
						 * (ListData.getText().contains(FamilyData[mcount][0].
						 * toString().trim())) { Thread.sleep(1000);
						 * ListData.click();
						 * 
						 * if (count == membersSize) { break outer; } else {
						 * count = count + 1; break member; //break outer; } }
						 * 
						 * }
						 */
						for (int lData = 0; lData < List.size(); lData++) {
							System.out.println("Age of Member is -- :" + List.get(lData).getText());
							if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								Thread.sleep(2000);

								List.get(lData).click();

								if (count == membersSize) {
									break outer;
								} else {
									count = count + 1;
									// break member;
									break outer;
								}

							}

						}
					}
				}

			}

			else

			{
				Thread.sleep(5000);
				List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']//a"));
				for (WebElement DropDowns : dropdowns) {
					if (DropDowns.getText().equals("up to 40 Years")
							|| DropDowns.getText().equals("41 Years - 60 Years")
							|| DropDowns.getText().equals("61  Years - 70 Years")
							|| DropDowns.getText().equals("71 Years - 80 Years")
							|| DropDowns.getText().equals("> 80 Years")) {

						String Membersdetails = Edit_ExploreData[editexplore.n][6];
						if (Membersdetails.contains(",")) {

							BaseClass.membres = Membersdetails.split(",");
						} else {
							// BaseClass.membres = Membersdetails.split(" ");
							System.out.println("Hello");
						}

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							DropDowns.click();

							// List Age of members dropdown

							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}

					}
				}
			}
		}
		scrolldown();
		int SumInsured = Integer.parseInt(Edit_ExploreData[editexplore.n][8].toString().trim());
		try {
			clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
			clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
			clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
			System.out.println("Entered SumInsured is : " + SumInsured);
		} catch (Exception e) {
			System.out.println("Unable to Select SumInsured.");
		}
		// *[@class='col-md-12 total_premium_text']/b/p/span
		String Addon = TestCaseData[editexplore.n][13];

		if (Addon.contains("No Addon")) {
			Thread.sleep(2000);
			clickElement(By.xpath(noAddon_edit_xpath));
			Thread.sleep(2000);
			Premium = driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text']/b/p/span")).getText();
			System.out.println(
					"Total Premium Value on Quotation Page is : " + " - " + Premium.substring(0, Premium.length()));

		} else if (Addon.contains("for gold plan")) {
			Thread.sleep(2000);
			clickElement(By.xpath(GoldPlanAddon_xpath));
			Thread.sleep(2000);
			Premium1 = driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text']/b/p/span")).getText();
			System.out.println(
					"Total Premium Value on Quotation Page is : " + " - " + Premium1.substring(0, Premium1.length()));
		} else if (Addon.contains("with platinum plan")) {
			Thread.sleep(2000);
			clickElement(By.xpath(PlatinumAddon_xpath));
			Thread.sleep(2000);
			Premium2 = driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text']/b/p/span")).getText();
			System.out.println(
					"Total Premium Value on Quotation Page is : " + " - " + Premium2.substring(0, Premium2.length()));
		}
		System.out.println("Selected Addon is : " + Addon);
		scrolldown();

		// Click on Buy Now Button
		Thread.sleep(5000);
		try {
			System.out.println("Before Click on BuyNow Button.");
			clickElement(By.xpath("//button[@data-dismiss='modal'][contains(text(),'Update Premium')]"));
			System.out.println("After Click on update premium");
		} catch (Exception e) {
			System.out.println("Unable to click on update premium.");
		}

	}

	public static void POSExploreEdit() throws Exception {

		String[][] TestCase = BaseClass.excel_Files1("POSExplore_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files1("POSExplore_Quotation");
		String[][] FamilyData = BaseClass.excel_Files1("POSExplore_InsuredDetails");
		String[][] EditPOSExplore = BaseClass.excel_Files1("EditPosExplore");

		scrollup();
		clickElement(By.xpath(POSExploreedit_xpath));
		Thread.sleep(3000);
		System.out.println("Edit start here for POSExplore Travel");
		POSExplore posexp = new POSExplore();
		clickElement(By.xpath(Edit_TravellingAllDropdowns_xpath));
		Thread.sleep(5000);
		String TravellingTo = EditPOSExplore[posexp.n][1].toString().trim();
		;
		System.out.println(TravellingTo);
		clickElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li/a[contains(text()," + "'"
				+ TravellingTo + "'" + ")]"));
		System.out.println("User is Travelling To : " + TravellingTo);

		Thread.sleep(5000);
		String StartDate = EditPOSExplore[posexp.n][2].toString().trim();
		try {
			Thread.sleep(4000);
			clickElement(By.xpath(StartDateCalander_Id));
			Thread.sleep(2000);
			String spilitter[] = StartDate.split(",");
			String eday = spilitter[0];
			String emonth = spilitter[1];
			String eYear = spilitter[2];
			String oMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			String oYear = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			// WebElement Next =
			// driver.findElement(By.xpath("//span[contains(text(),'Next')]"));//
			// Next Button
			while (!((eYear.contentEquals(oYear)) && (emonth.contentEquals(oMonth)))) {
				Thread.sleep(2000);
				// Next.click();
				oMonth = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
				oYear = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
				// Next =
				// driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
			}
			driver.findElement(
					By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text(),"
							+ "'" + eday + "'" + ")]"))
					.click();
			System.out.println("Entered Start_Date on Faveo UI is :" + StartDate);
		} catch (Exception e) {
			System.out.println("Unable to Enter Start Date.");
		}

		// Select End Date from Excel and Write it on UI
		clickElement(By.id(EndDateCalender_Id));
		String EndDate = EditPOSExplore[posexp.n][3].toString().trim();
		String spilitter1[] = EndDate.split(",");
		String eday1 = spilitter1[0];
		String emonth1 = spilitter1[1];
		String eYear1 = spilitter1[2];
		String oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
		String oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
		WebElement Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));// Next
																							// Button
		while (!((eYear1.contentEquals(oYear1)) && (emonth1.contentEquals(oMonth1)))) {
			Thread.sleep(2000);
			Next1.click();
			oMonth1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[1]")).getText();
			oYear1 = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/div/span[2]")).getText();
			Next1 = driver.findElement(By.xpath("//span[contains(text(),'Next')]"));
		}
		driver.findElement(
				By.xpath("//html//div[@id='ui-datepicker-div']//td[@data-handler='selectDay']/a[contains(text()," + "'"
						+ eday1 + "'" + ")]"))
				.click();
		Thread.sleep(2000);
		System.out.println("Entered End_Date on Faveo UI is :" + EndDate);
		Actions actn = new Actions(driver);
		actn.sendKeys(Keys.ESCAPE);
		List<WebElement> dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		try {

			for (WebElement DropDownName : dropdown) {
				// System.out.println("Dropdown text is :
				// "+DropDownName.getText());

				// int Travellers =
				// Integer.parseInt(TestCaseData[editexplore.n][8].toString().trim());
				String Travellers = EditPOSExplore[posexp.n][4].toString().trim();
				// System.out.println("Dropdown text is :
				// "+DropDownName.getText());
				if (DropDownName.getText().equals(Travellers)) {
					DropDownName.click();
					// driver.findElement(By.xpath("//ul[@class='dropdown-menu
					// dropdown_menu_focus month_year']/li["+
					// TestCaseData[cb.n][15].toString().trim() + "]")).click();
					clickElement(By
							.xpath("//ui-dropdown[@ng-model='quoteParams.postedField.field_17']//div//a[@ng-click='selectVal(item)'][contains(text(),"
									+ "'" + Travellers + "'" + ")]"));
					System.out.println("---Total Number of Member Selected : " + Travellers);
					Thread.sleep(5000);
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("Unable to Select Member.");
		}
		/*
		 * Fluentwait(By.
		 * xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"
		 * )); int membersSize =
		 * Integer.parseInt(EditPOSExplore[posexp.n][4].toString().trim());
		 * System.out.println("Total Number of Member in Excel : "+membersSize);
		 * int count = 1; int mcount; int mcountindex = 0; int covertype; int
		 * loopcount=membersSize+1;
		 * 
		 * outer:
		 * 
		 * for (WebElement DropDownName : dropdown) { if (membersSize == 1) {
		 * List<WebElement> dropdowns =
		 * driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']")
		 * );
		 * 
		 * 
		 * 
		 * // reading members from test cases sheet memberlist String
		 * Membersdetails =EditPOSExplore[posexp.n][5];
		 * System.out.println("Members details is : "+Membersdetails);
		 * 
		 * if (Membersdetails.contains("")) {
		 * 
		 * BaseClass.membres = Membersdetails.split(",");
		 * 
		 * member: for (int i = 0; i <= BaseClass.membres.length; i++) {
		 * 
		 * mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
		 * System.out.println("value of Mcount Index : "+mcount); mcountindex =
		 * mcountindex + 1;
		 * 
		 * driver.findElement(By.
		 * xpath("//*[@class='toolbar_plan_name_input']//a[@role='button'][contains(text(),'up to 40 Years')]"
		 * )).click();
		 * 
		 * //clickElement(By.
		 * xpath("//li[@class='dropdown year_drop_slect master']")); // List Age
		 * of members dropdown List<WebElement> List = driver.findElements(By.
		 * xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));
		 * String text = FamilyData[mcount][0].toString().trim();
		 * 
		 * for(int lData=1;lData<List.size();lData++) {
		 * System.out.println("Age of Member is -- :" +
		 * List.get(lData).getText()); if
		 * (List.get(lData).getText().contains(FamilyData[mcount][0].toString().
		 * trim())) { System.out.println("Age of Member is -- :" +
		 * List.get(lData).getText()); Thread.sleep(2000);
		 * 
		 * List.get(lData).click();
		 * 
		 * if (count == membersSize) { break outer; } else { count = count + 1;
		 * // break member; break outer; }
		 * 
		 * }
		 * 
		 * } } }
		 * 
		 * }
		 * 
		 * else
		 * 
		 * { List<WebElement> dropdowns =
		 * driver.findElements(By.xpath("//*[@class='toolbar_plan_name_input']")
		 * ); for (WebElement DropDowns : dropdowns)
		 * 
		 * { //System.out.println("Text is : "+ DropDowns.getText()); if
		 * (DropDowns.getText().equals("up to 40 Years")||DropDowns.getText().
		 * equals("41 Years - 60 Years")
		 * ||DropDowns.getText().equals("61  Years - 70 Years")||DropDowns.
		 * getText().equals("71 Years - 80 Years")
		 * ||DropDowns.getText().equals("> 80 Years")) {
		 * 
		 * 
		 * String Membersdetails = EditPOSExplore[posexp.n][5]; if
		 * (Membersdetails.contains(",")) {
		 * 
		 * BaseClass.membres = Membersdetails.split(","); } else {
		 * //BaseClass.membres = Membersdetails.split(" ");
		 * System.out.println("Hello"); }
		 * 
		 * member: // total number of members for (int i = 0; i <=
		 * BaseClass.membres.length; i++) {
		 * 
		 * mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
		 * mcountindex = mcountindex + 1;
		 * 
		 * DropDowns.click();
		 * 
		 * // List Age of members dropdown
		 * 
		 * List<WebElement> List = driver.findElements(By.
		 * xpath("//*[@class='ng-binding' and contains(text(), 'Years')]"));
		 * 
		 * for(int lData=1;lData<List.size();lData++) {
		 * System.out.println("Age of Member is -- :" +
		 * List.get(lData).getText()); if
		 * (List.get(lData).getText().contains(FamilyData[mcount][0].toString().
		 * trim())) { System.out.println("Age of Member is -- :" +
		 * List.get(lData).getText()); Thread.sleep(2000);
		 * 
		 * List.get(lData).click();
		 * 
		 * if (count == membersSize) { break outer; } else { count = count + 1;
		 * // break member; //break outer; }
		 * 
		 * }
		 * 
		 * } }
		 * 
		 * } } } }
		 */
		// int SumInsured =
		// Integer.parseInt(EditPOSExplore[posexp.n][6].toString().trim());
		String SumInsured = EditPOSExplore[posexp.n][6].toString().trim();
		try {
			clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
			clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
			clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
			System.out.println("Entered SumInsured is : " + SumInsured);
		} catch (Exception e) {
			System.out.println("Unable to Select SumInsured.");
		}
		System.out.println("Successfully selected suminsured");

		String Addon = EditPOSExplore[posexp.n][7];

		if (Addon.contains("No Addon")) {
			Thread.sleep(2000);
			clickElement(By.xpath(noAddon_edit_xpath));
			Thread.sleep(2000);
			Premium = driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text']/b/p/span")).getText();
			System.out.println(
					"Total Premium Value on Quotation Page is : " + " - " + Premium.substring(0, Premium.length()));

		} else if (Addon.contains("for gold plan")) {
			Thread.sleep(2000);
			clickElement(By.xpath(GoldPlanAddon_xpath));
			Thread.sleep(2000);
			Premium1 = driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text']/b/p/span")).getText();
			System.out.println(
					"Total Premium Value on Quotation Page is : " + " - " + Premium1.substring(0, Premium1.length()));
		} else if (Addon.contains("with platinum plan")) {
			Thread.sleep(2000);
			clickElement(By.xpath(PlatinumAddon_xpath));
			Thread.sleep(2000);
			Premium2 = driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text']/b/p/span")).getText();
			System.out.println(
					"Total Premium Value on Quotation Page is : " + " - " + Premium2.substring(0, Premium2.length()));
		}
		System.out.println("Selected Addon is : " + Addon);

		// Click on Buy Now Button
		Thread.sleep(5000);
		try {
			System.out.println("Before Click on BuyNow Button.");
			clickElement(By.xpath("//button[@data-dismiss='modal'][contains(text(),'Update Premium')]"));
			System.out.println("After Click on update premium");
		} catch (Exception e) {
			System.out.println("Unable to click on update premium.");
		}
	}

	public static void EditHeart() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Heart_Testcase");
		String[][] TestCaseData = BaseClass.excel_Files("Heart_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("Heart_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("Care_Freedom_QuestionSet");
		Heart h = new Heart();

		clickElement(By.xpath(critical_edit_xpath));
		Thread.sleep(10000);

		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1") || DropDownName.getText().equals("2")
						|| DropDownName.getText().equals("3") || DropDownName.getText().equals("4")
						|| DropDownName.getText().equals("1") || DropDownName.getText().equals("5")
						|| DropDownName.getText().equals("6")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ TestCaseData[h.n][30].toString().trim() + "]")).click();// select
																						// 4
																						// based
																						// on
																						// the
																						// excel
																						// data
					System.out.println("Total Number of Member Selected : " + TestCaseData[h.n][30].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		
		String suminsure = TestCaseData[h.n][27];

		WebElement drag = driver.findElement(
				By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/div/div"));
		List<WebElement> dragable = drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
		// String SliderValue=null;

		for (WebElement Slider : dragable) {
			System.out.println("Policy type is : " + PolicyType);
			Thread.sleep(5000);
			if (Slider.getText().equals(suminsure)) {
				Slider.click();
				break;
			}
		}

		String Monthlyfrequency = TestCaseData[h.n][29];
		System.out.println("Monthly Frequency is :  " + Monthlyfrequency);
		int Tenure = Integer.parseInt(TestCaseData[h.n][28].toString().trim());
		if (Tenure == 1) {
			System.out.println("Selected monthly frequency is : " + Monthlyfrequency);
		}

		else if (Tenure == 2) {
			if (Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='Radio2q']"));

			} else if (Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='Radio1q']"));
			} else if (Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='Radio0q']"));
			}
		} else if (Tenure == 3) {
			if (Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='Radio1q']"));

			} else if (Monthlyfrequency.contains("Quarterly")) {
				try {
					clickElement(By.xpath("//label[@for='Radio2q']"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//input[@id='Radio0q']"));
			}
		}

		// Here premium verify
		WebElement update_firstpage_Premium = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[4]//span"));
		Thread.sleep(10000);
		String update_firstpage_PremiumValue = update_firstpage_Premium.getText();
		Thread.sleep(1000);
		System.out.println("First page premium value is   :" + update_firstpage_PremiumValue);
		// JavascriptExecutor jse = (JavascriptExecutor)driver;
		clickElement(By.xpath(critical_updatepremium_xpath));
		// jse.executeScript("window.scrollBy(0,150)", "");
		// clickElement(By.xpath(Group_BuyNow_xpath));
		/*
		 * clickElement(By.xpath(critical_xpath)); Thread.sleep(5000);
		 * jse.executeScript("window.scrollBy(0,-350)", "");
		 */
		// WebElement
		// secondpage_premium=driver.findElement(By.xpath("//div[@class='col-md-12
		// tr_quotation_heading']/h4/span/span[@class='ng-binding']"));

		WebElement secondpage_premium = driver
				.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		String secondpage_PremiumValue = secondpage_premium.getText();
		Thread.sleep(10000);
		System.out.println("Second Page Premium value is  : " + secondpage_PremiumValue);
		Assert.assertEquals(update_firstpage_PremiumValue, secondpage_PremiumValue);
		Thread.sleep(5000);

	}
	public static void EditOperation() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("Operation_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files("Operation_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("Operation_Insured_Details");
		String[][] QuestionSetData = BaseClass.excel_Files("Operation_QuestionSet");
		Operation O = new Operation();

		Thread.sleep(5000);
		clickElement(By.xpath(critical_edit_xpath));
		Thread.sleep(2000);

		List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try {
			for (WebElement DropDownName : dropdown) {
				DropDownName.click();
				if (DropDownName.getText().equals("1") || DropDownName.getText().equals("2")
						|| DropDownName.getText().equals("3") || DropDownName.getText().equals("4")
						|| DropDownName.getText().equals("1") || DropDownName.getText().equals("5")
						|| DropDownName.getText().equals("6")) {
					driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["
							+ TestCaseData[O.n][28].toString().trim() + "]")).click();// select
																						// 4
																						// based
																						// on
																						// the
																						// excel
																						// data
					System.out.println("Total Number of Member Selected : " + TestCaseData[O.n][28].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}
		String suminsure = TestCaseData[O.n][25];

		WebElement drag = driver.findElement(
				By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/div/div"));
		List<WebElement> dragable = drag.findElements(By.xpath("//span[@class='ui-slider-number']"));
		// String SliderValue=null;

		for (WebElement Slider : dragable) {
			// System.out.println("Policy type is : "+PolicyType);
			Thread.sleep(1000);
			if (Slider.getText().equals(suminsure)) {
				Slider.click();
				break;
			}
		}

		String Monthlyfrequency = TestCaseData[O.n][27];
		System.out.println("Monthly Frequency is :  " + Monthlyfrequency);
		int Tenure = Integer.parseInt(TestCaseData[O.n][26].toString().trim());
		if (Tenure == 1) {
			System.out.println("Selected monthly frequency is : " + Monthlyfrequency);
		}

		else if (Tenure == 2) {
			if (Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='Radio2q']"));

			} else if (Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='Radio1q']"));
			} else if (Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//label[@for='Radio0q']"));
			}
		} else if (Tenure == 3) {
			if (Monthlyfrequency.contains("Monthly")) {
				clickElement(By.xpath("//label[@for='Radio1q']"));

			} else if (Monthlyfrequency.contains("Quarterly")) {
				try {
					clickElement(By.xpath("//label[@for='Radio2q']"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (Monthlyfrequency.contains("Quarterly")) {
				clickElement(By.xpath("//input[@id='Radio0q']"));
			}
		}

		// Here premium verify
		WebElement update_firstpage_Premium = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[4]/b/p/span"));
		Thread.sleep(5000);
		String update_firstpage_PremiumValue = update_firstpage_Premium.getText();
		Thread.sleep(1000);
		System.out.println("First page premium value is   :" + update_firstpage_PremiumValue);
		clickElement(By.xpath(critical_updatepremium_xpath));

		WebElement secondpage_premium = driver
				.findElement(By.xpath("//*[@id='msform']/div[1]/div/div/div/div[4]/p[1]"));
		afterEdit_Proposalpremium_value = secondpage_premium.getText();
		Thread.sleep(5000);
		System.out.println("After Edit Premium value is  : " + afterEdit_Proposalpremium_value);
		Assert.assertEquals(update_firstpage_PremiumValue, afterEdit_Proposalpremium_value);
		// Thread.sleep(5000);

	}

	public static void posstudentexploreedit() throws Exception {

		String[][] TestCase = BaseClass.excel_Files("POS_StudentExplore_TestCase");
		String[][] TestCaseData = BaseClass.excel_Files("POS_StudentExplore_Quotation");

		scrollup();

		clickElement(By.xpath("//button[@class='quot_edit_btn']"));
		Thread.sleep(4000);
		String travelingtoEdit = TestCaseData[1][34].toString().trim();
		Thread.sleep(5000);
		try {
			clickElement(By.xpath(travelingto_xpath));
			clickElement(By
					.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'"
							+ travelingtoEdit + "')]"));
		} catch (Exception e) {
			System.out.println("Unable to select travelling to");
		}

		String plantype = TestCaseData[1][35].toString().trim();
		try {
			clickElement(By.xpath(plantypeEdit_xpath));
			clickElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li/a[contains(text(),'"
					+ plantype + "')]"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("unable to select plan type");
		}

		String policytenure = TestCaseData[1][36].toString().trim();
		try {
			clickElement(By.xpath(policy_tenure_xpath));
			clickElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li/a[contains(text(),'"
					+ policytenure + "')]"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to select policy tenure");
		}
		String suminsured = TestCaseData[1][37].toString().trim();
		WebElement progress = driver.findElement(
				By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[2]/div/div/div"));

		List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
		int total_size = Slidnumber.size();
		System.out.println("Slider numbers are :  " + total_size);
		String SliderValue = null;
		for (WebElement Slider : Slidnumber) {
			SliderValue = suminsured;

			Thread.sleep(3000);
			if (Slider.getText().equals(SliderValue.toString())) {
				Thread.sleep(5000);

				Slider.click();
				break;
			}
		}

	}

	public static void GroupStudentExploreEdit() throws Exception {
		GroupStudentExplore gse = new GroupStudentExplore();
		String[][] TestCase = BaseClass.excel_Files("GroupStudentExplore_Testcase");
		String[][] TestCaseData = BaseClass.excel_Files("GroupStudentExplore_Quotation");

		scrollup();

		clickElement(By.xpath("//button[@class='quot_edit_btn']"));
		Thread.sleep(4000);
		String travelingtoEdit = TestCaseData[gse.n][34].toString().trim();
		Thread.sleep(5000);
		try {
			clickElement(By.xpath(travelingto_xpath));
			clickElement(
					By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a[contains(text(),'"
							+ travelingtoEdit + "')]"));
		} catch (Exception e) {
			System.out.println("Unable to select travelling to");
		}

		String plantype = TestCaseData[gse.n][35].toString().trim();
		try {
			clickElement(By.xpath(plantypeEdit_xpath));
			clickElement(
					By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a[contains(text(),'"
							+ plantype + "')]"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("unable to select plan type");
		}
		String policytenure = TestCaseData[gse.n][36].toString().trim();
		try {
			clickElement(By.xpath(policy_tenure_xpath));
			clickElement(
					By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a[contains(text(),'"
							+ policytenure + "')]"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to select policy tenure");
		}

		String suminsured = TestCaseData[gse.n][37].toString().trim();
		WebElement progress = driver.findElement(
				By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[2]/div/div/div"));

		List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
		int total_size = Slidnumber.size();
		System.out.println("Slider numbers are :  " + total_size);
		Thread.sleep(5000);
		String SliderValue = null;
		for (WebElement Slider : Slidnumber) {
			SliderValue = suminsured;

			Thread.sleep(3000);
			if (Slider.getText().equals(SliderValue.toString())) {
				Thread.sleep(3000);

				Slider.click();
				break;
			}
		}

	}

	// **************************************************************************************************

	public static void CareOPDEdit() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("CareWithOPD_Testcase");
		String[][] TestCaseData = BaseClass.excel_Files("CareWithOPD_Quotation");

		String[][] FamilyData = BaseClass.excel_Files("CareWithOPD_insuredDetails");
		String[][] posseniorEdit = BaseClass.excel_Files("CareWithOPDEdit");
		System.out.println("Click here buy now button for pos care senior");
		clickElement(By.xpath(Button_Buynow_xpath));

		Thread.sleep(5000);
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(5000);
		CareWithOPD cwo = new CareWithOPD();

		int covertype;
		int membersSize = Integer.parseInt(TestCaseData[cwo.n][6].toString().trim());
		List<WebElement> opdDrop = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

		try {
			for (WebElement DropDownName : opdDrop) {
				DropDownName.click();
				System.out.println(DropDownName.getText());
				if (DropDownName.getText().equals(TestCaseData[cwo.n][6].toString().trim())) {

					driver.findElement(By
							.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
									+ "'" + membersSize + "'" + ")]"))
							.click();
					System.out.println("Total Number of Member Selected : " + TestCaseData[cwo.n][6].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		List<WebElement> dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int loopcount = membersSize + 1;
		System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = posseniorEdit[cwo.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[cwo.n][8].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By.xpath(
							"//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[cwo.n][6].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")
									|| DropDowns.getText().equals("46 - 50 years")
									|| DropDowns.getText().equals("41 - 45 years")
									|| DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("36 - 40 years")
									|| DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals("51 - 55 years")
									|| DropDowns.getText().equals(">75 years")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[cwo.n][9].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = posseniorEdit[cwo.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("5 - 24 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("56 - 60 years")
								|| DropDowns.getText().equals("46 - 50 years")
								|| DropDowns.getText().equals("41 - 45 years")
								|| DropDowns.getText().equals("25 - 35 years")
								|| DropDowns.getText().equals("36 - 40 years")
								|| DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years")
								|| DropDowns.getText().equals("51 - 55 years")
								|| DropDowns.getText().equals(">75 years")) {

							String EditMembersdetails = posseniorEdit[cwo.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		}

		catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");

		}
		int SumInsured = Integer.parseInt(posseniorEdit[cwo.n][2].toString().trim());
		System.out.println("Suminsured is  : " + SumInsured);
		int Tenure = Integer.parseInt(posseniorEdit[cwo.n][3].toString().trim());
		try {
			clickElement(By.xpath(slider_xpath));
			clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));

			System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");
			logger.log(LogStatus.PASS, "Data entered for Sum Insured: " + SumInsured + "Lakhs");
		} catch (Exception e) {
			System.out.println(e);
			logger.log(LogStatus.FAIL, e);
		}

		int Tenure1 = Tenure - 1;
		System.out.println("Selected Tenure is : " + Tenure + " Year");
		scrolldown();
		Thread.sleep(2000);
		clickElement(By.xpath("//label[@for='Radio" + Tenure1 + "q']//img[@src='assets/img/correct_signal.png']"));
		BaseClass.scrollup();

		// premium Details
		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		scrolldown();
		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[3]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();

			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}
	}

	public static void DomesticTravelEdit() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("DomesticTravel_Testcase");
		String[][] TestCaseData = BaseClass.excel_Files("DomesticTravel_quotation");
		String TripType = TestCaseData[1][14].toString().trim();
		DomesticTravel dt = new DomesticTravel();

		try {
			clickElement(By.xpath(
					"//*[@class='col-md-12 padding0 tr_no_btn text-center']/button[contains(text(),'Buy Now')]"));
			Thread.sleep(5000);
			scrollup();
		} catch (Exception e) {
			System.out.println("Unable to click on buy now button");
		}
		clickElement(By.xpath("//button[@class='quot_edit_btn']"));
		Thread.sleep(4000);

		System.out.println("=========Selecteing Trip Type here=============================");

		try {
			clickElement(
					By.xpath("//*[@class='dropdown year_drop_slect master']/a[contains(text(),'" + TripType + "')]"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("Unable to click on Trip type");
		}
		List<WebElement> triptypeele = driver
				.findElements(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year show']/li/a"));
		for (WebElement triptyp : triptypeele) {
			System.out.println("Trip type context is : " + triptyp);
			if (triptyp.getText().equals(TripType)) {
				triptyp.click();
				System.out.println("Selected trip type is :" + TripType);
				break;
			}
		}
		System.out.println("===============================SumINsured Selecting here=========================");
		Thread.sleep(3000);
		WebElement progress = driver.findElement(By.xpath("//*[@class='wrapper slider2 travel_slider']/div"));
		String suminsured = TestCaseData[dt.n][15].toString().trim();
		List<WebElement> Slidnumber = progress.findElements(By.xpath("//*[@class='ui-slider-number']"));
		int total_size = Slidnumber.size();
		System.out.println("Slider numbers are :  " + total_size);
		String SliderValue = null;
		for (WebElement Slider : Slidnumber) {
			SliderValue = suminsured;

			Thread.sleep(3000);
			if (Slider.getText().equals(SliderValue.toString())) {
				Thread.sleep(5000);

				Slider.click();
				break;
			}
		}
		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/div/div/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		Thread.sleep(4000);
		WebElement firstpage_Premium = driver.findElement(By.xpath(
				"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[3]/div[1]/div/div/div/div/label[2]/span/span/b"));
		String firstpage_PremiumValue = firstpage_Premium.getText();
		System.out.println("First page premium value is   :" + firstpage_PremiumValue);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,150)", "");
		try {
			Thread.sleep(3000);
			scrolldown();

			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		jse.executeScript("window.scrollBy(0,-350)", "");
		scrollup();
		Thread.sleep(4000);
		WebElement secondpage_premium = driver.findElement(By.xpath("//*[@class='tr_premium_val']/span[1]"));
		DomesticTravelPageJourney.ProposalPremimPage_Value = secondpage_premium.getText();
		System.out.println("Second Page Premium value is  : " + DomesticTravelPageJourney.ProposalPremimPage_Value);
		Assert.assertEquals(firstpage_PremiumValue, DomesticTravelPageJourney.ProposalPremimPage_Value);
		System.out.println("Second page premium value is same as first page premium value");

		logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
				+ DomesticTravelPageJourney.ProposalPremimPage_Value + firstpage_PremiumValue);
	}

	public static void POScareseniorEdit() throws Exception {
		String[][] TestCase = BaseClass.excel_Files("POSCareSenior_Testcase");
		String[][] TestCaseData = BaseClass.excel_Files("POScaresenior_Quotation");
		String[][] FamilyData = BaseClass.excel_Files("POSSeniorFamilymember");
		String[][] QuestionSetData = BaseClass.excel_Files("QuestionSetSenior");
		String[][] posseniorEdit = BaseClass.excel_Files("POS_CareseniorEdit");
		System.out.println("Click here buy now button for pos care senior");
		clickElement(By.xpath(Button_Buynow_xpath));

		Thread.sleep(5000);
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(5000);
		POSCareSenior pcs = new POSCareSenior();

		int covertype;
		int membersSize = Integer.parseInt(TestCaseData[pcs.n][6].toString().trim());
		List<WebElement> seniorDrop = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));

		try {
			for (WebElement DropDownName : seniorDrop) {
				DropDownName.click();
				System.out.println(DropDownName.getText());
				if (DropDownName.getText().equals("1")) {

					// *[@id='getquote']/form/div[1]/div[5]/div/ui-dropdown/div/div/ul/li/a[contains(text(),'1')]

					driver.findElement(By
							.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/ul/li[1]/a[contains(text(),"
									+ "'" + membersSize + "'" + ")]"))
							.click();
					System.out.println("Total Number of Member Selected : " + TestCaseData[pcs.n][6].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}

		Fluentwait(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		List<WebElement> dropdown = driver.findElements(
				By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int count = 1;
		int mcount;
		int mcountindex = 0;
		int loopcount = membersSize + 1;
		System.out.println("Total Number of dropdown on Quotation Page is " + dropdown.size()); // 3
		try {
			outer:

			for (WebElement DropDownName : dropdown) {

				if (membersSize == 1) {

					String EditMembersdetails = posseniorEdit[pcs.n][1];
					if (EditMembersdetails.contains("")) {

						BaseClass.membres = EditMembersdetails.split("");

						member:
						// total number of members
						for (int i = 0; i <= BaseClass.membres.length; i++) {

							// one by one will take from 83 line
							mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
							mcountindex = mcountindex + 1;

							driver.findElement(By
									.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"))
									.click();

							// List Age of members dropdown
							// DropDownName.click();
							List<WebElement> List = driver
									.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

							for (int lData = 1; lData < List.size(); lData++) {
								System.out.println("Age of Member is -- :" + List.get(lData).getText());
								if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
									System.out.println("Age of Member is -- :" + List.get(lData).getText());
									Thread.sleep(2000);

									List.get(lData).click();

									if (count == membersSize) {
										break outer;
									} else {
										count = count + 1;
										// break member;
										break outer;
									}

								}

							}
						}
					}

				} else if (DropDownName.getText().contains("Individual")
						|| DropDownName.getText().contains("Floater")) {
					System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

					if (TestCaseData[pcs.n][8].toString().trim().equals("Individual")) {
						covertype = 1;
					} else {
						covertype = 2;
					}
					DropDownName.click();
					driver.findElement(By
							.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li[" + covertype + "]"))
							.click();
					// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
					Thread.sleep(10000);
					if (covertype == 2) {
						List<WebElement> dropdowns = driver
								.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						for (WebElement DropDowns : dropdowns) {
							if (DropDowns.getText().contains("Floater")) {
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals(TestCaseData[pcs.n][6].toString().trim())) {
								System.out.println("DropDownName is  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("2")) {
								System.out.println(
										"Total DropDownName Present on Quotation page are : " + DropDowns.getText());
							} else if (DropDowns.getText().equals("61 - 65 years")
									|| DropDowns.getText().equals("66 - 70 years")
									|| DropDowns.getText().equals("71 - 75 years")
									|| DropDowns.getText().equals(">75")) {
								// reading members from test cases sheet
								// memberlist
								int Children = Integer.parseInt(TestCaseData[pcs.n][9].toString().trim());
								clickElement(By.xpath(
										"//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
								clickElement(By
										.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]"));
								System.out.println(
										"//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"
												+ "'" + Children + "'" + ")]");
								String EditMembersdetails = posseniorEdit[pcs.n][1];
								if (EditMembersdetails.contains(",")) {

									BaseClass.membres = EditMembersdetails.split(",");
								} else {
									System.out.println("Hello");
								}

								member: for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									DropDowns.click();
									// List Age of members dropdown

									List<WebElement> List = driver.findElements(
											By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for (int lData = 1; lData < List.size(); lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										if (List.get(lData).getText()
												.contains(FamilyData[mcount][0].toString().trim())) {
											System.out.println("Age of Member is -- :" + List.get(lData).getText());
											Thread.sleep(2000);

											List.get(lData).click();

											if (count == membersSize) {
												break outer;
											} else {
												count = count + 1;
												// break member;
												break outer;
											}

										}

									}
								}

							}
						}
					}
				} else {

					List<WebElement> dropdowns = driver
							.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));

					for (WebElement DropDowns : dropdowns) {

						if (DropDowns.getText().contains("Floater")) {
							System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
						} else if (DropDowns.getText().equals("61 - 65 years")
								|| DropDowns.getText().equals("66 - 70 years")
								|| DropDowns.getText().equals("71 - 75 years") || DropDowns.getText().equals(">75")) {

							String EditMembersdetails = posseniorEdit[pcs.n][1];
							if (EditMembersdetails.contains(",")) {
								BaseClass.membres = EditMembersdetails.split(",");
							} else {
								System.out.println("Hello");
							}

							member:
							// total number of members
							for (int i = 0; i <= BaseClass.membres.length; i++) {

								// one by one will take from 83 line
								mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
								mcountindex = mcountindex + 1;

								DropDowns.click();
								// List Age of members dropdown
								List<WebElement> List = driver.findElements(
										By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

								// for (WebElement ListData : List) {
								for (int lData = loopcount; lData < List.size(); lData++) {

									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											// loopcount=loopcount+1;
											break member;
										}

									}

								}
							}

						}
					}
				}
			}
		}

		catch (Exception e) {
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");

		}
		int SumInsured = Integer.parseInt(posseniorEdit[pcs.n][2].toString().trim());
		System.out.println("Suminsured is  : " + SumInsured);
		int Tenure = Integer.parseInt(posseniorEdit[pcs.n][3].toString().trim());
		try {
			clickElement(By.xpath(slider_xpath));
			clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));

			System.out.println("Entered Sum Insured is : " + SumInsured + " Lakhs");
			logger.log(LogStatus.PASS, "Data entered for Sum Insured: " + SumInsured + "Lakhs");
		} catch (Exception e) {
			System.out.println(e);
			logger.log(LogStatus.FAIL, e);
		}

		int Tenure1 = Tenure - 1;
		System.out.println("Selected Tenure is : " + Tenure + " Year");
		scrolldown();
		Thread.sleep(2000);
		clickElement(By.xpath("//label[@for='Radio" + Tenure1 + "q']//img[@src='assets/img/correct_signal.png']"));
		BaseClass.scrollup();

		// premium Details
		String TotalMemberPresentonQuotation = driver
				.findElement(By
						.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[1]/div/ui-dropdown/div/div/a"))
				.getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
		scrolldown();
		String afterEdit_Quotationpremium_value = driver
				.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[2]/div[4]/b/p/span"))
				.getText();
		System.out.println("After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :" + afterEdit_Quotationpremium_value);

		try {
			Thread.sleep(3000);
			scrolldown();

			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		} catch (Exception e) {
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :" + afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO, "Quotaion Premium and Proposal Premium is Verified and Both are Same : "
					+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value);
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}

	}
	
	
	public static void careheartEdit() throws Exception {
		String[][] TestCase=BaseClass.excel_Files("Care_Heart_TestCase");
		String[][] TestCaseData=BaseClass.excel_Files("CareHeart_Quotation");
		String[][]  editCareheart =BaseClass.excel_Files("careHeart_Edit");
		String[][] FamilyData = BaseClass.excel_Files("CareHeart_Insured_Details");
		scrollup();
		clickElement(By.xpath(Edit_careWithNCB_xpath));
		Thread.sleep(5000);
	 CareHeart ch=new CareHeart();
	 
	 List<WebElement> dropdown = driver.findElements(By.xpath(DropDown_Value_xpath));
		try{
			for (WebElement DropDownName : dropdown) {
				System.out.println("Dropdown text is : "+DropDownName.getText());
				DropDownName.click();

				if (DropDownName.getText().equals(TestCaseData[ch.n][15].toString().trim())) {
					//driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[cb.n][15].toString().trim() + "]")).click();
					driver.findElement(By.xpath("//*[@class=\"dropdown year_drop_slect master open\"]/ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ TestCaseData[ch.n][15].toString().trim() + "]")).click();
					System.out.println("---Total Number of Member Selected : " + TestCaseData[ch.n][15].toString().trim());
					Thread.sleep(5000);
					break;
				}
			}
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, "Test Case is Failed Because Abacus is Down");
			BaseClass.AbacusURL();
		}


		Fluentwait(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		dropdown = driver.findElements(By.xpath("//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']"));
		int membersSize = Integer.parseInt(TestCaseData[ch.n][15].toString().trim());
		int count = 1;
		int mcount ;
		int mcountindex = 0;
		int covertype;
		int loopcount=membersSize+1;
		try{
			outer:

				for (WebElement DropDownName : dropdown) {

					if (membersSize == 1) {

						String EditMembersdetails = editCareheart[ch.n][1];
						if (EditMembersdetails.contains("")) 
						{

							BaseClass.membres = EditMembersdetails.split("");

							member:
								// total number of members
								for (int i = 0; i <= BaseClass.membres.length; i++) {

									// one by one will take from 83 line
									mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
									mcountindex = mcountindex + 1;

									driver.findElement(By.xpath("//div[@class='form-group']//div[@class='dropdown year_drop_slect master']//a[@class='dropdown-toggle dropdown11 dropdown_focus dropdown_month_tab care ng-binding']")).click();

									// List Age of members dropdown
									//DropDownName.click();
									List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

									for(int lData=1;lData<List.size();lData++) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
									if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
										System.out.println("Age of Member is -- :" + List.get(lData).getText());
										Thread.sleep(2000);

										List.get(lData).click();

										if (count == membersSize) {
											break outer;
										} else {
											count = count + 1;
											// break member;
											break outer;
										}

									}

								}
								}
						}

					} else if (DropDownName.getText().contains("Individual")||DropDownName.getText().contains("Floater")) {
						System.out.println("Present CovertType DropDownName is  " + DropDownName.getText());

						if (TestCaseData[ch.n][17].toString().trim().equals("Individual")) {
							covertype = 1;
						} else {
							covertype = 2;
						}
						DropDownName.click();
						driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown_menu_focus month_year']/li["+ covertype + "]")).click();
						// driver.findElement(By.xpath("//a[@ng-click='selectVal(item)'][contains(text(),"+"'"+Covertype+"'"+")]")).click();
						Thread.sleep(10000);
						if (covertype == 2) 
						{
							List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
							for (WebElement DropDowns : dropdowns) 
							{
								if (DropDowns.getText().contains("Floater")) 
								{
									System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
								} else if (DropDowns.getText().equals(TestCaseData[ch.n][15].toString().trim())) {
									System.out.println("DropDownName is  " + DropDowns.getText());
								} else if (DropDowns.getText().equals("2")) {
									System.out.println("Total DropDownName Present on Quotation page are : " + DropDowns.getText());
								} else  if (DropDowns.getText().equals("5 - 24 years")||DropDowns.getText().equals("36 - 40 years")||DropDowns.getText().equals("25 - 35 years")
										|| DropDowns.getText().equals("56 - 60 years")||DropDowns.getText().equals("46 - 50 years")||DropDowns.getText().equals("41 - 45 years")
										||DropDowns.getText().equals("25 - 35 years")||DropDowns.getText().equals("36 - 40 years")||DropDowns.getText().equals("61 - 65 years")
										||DropDowns.getText().equals("66 - 70 years")||DropDowns.getText().equals("71 - 75 years")||DropDowns.getText().equals("51 - 55 years")||DropDowns.getText().equals(">75 years")) {
									// reading members from test cases sheet
									// memberlist
									int Children = Integer.parseInt(TestCaseData[ch.n][31].toString().trim());
									clickElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/a"));
									clickElement(By.xpath("//*[@id=\"editGetQuot\"]/div/div/div/div[3]/form/div[1]/div[1]/div[3]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]"));
									System.out.println("//*[@id='getquote']/form/div[1]/div[6]/div/ui-dropdown/div/div/ul/li/a[contains(text(),"+ "'" + Children + "'" + ")]");
									String EditMembersdetails = editCareheart[ch.n][1];
									if (EditMembersdetails.contains(",")) {


										BaseClass.membres = EditMembersdetails.split(",");
									} else {
										System.out.println("Hello");
									}

									member:
										for (int i = 0; i <= BaseClass.membres.length; i++) {

											// one by one will take from 83 line
											mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
											mcountindex = mcountindex + 1;

											DropDowns.click();
											// List Age of members dropdown

											List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

											for(int lData=1;lData<List.size();lData++) {
													System.out.println("Age of Member is -- :" + List.get(lData).getText());
												if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
													System.out.println("Age of Member is -- :" + List.get(lData).getText());
													Thread.sleep(2000);

													List.get(lData).click();

													if (count == membersSize) {
														break outer;
													} else {
														count = count + 1;
														// break member;
														break outer;
													}

												}

											}
										}

								}
							}
						}
					} 
					else{

						List<WebElement> dropdowns = driver.findElements(By.xpath("//*[@class=\"toolbar_plan_name_input\"]"));
						
						for (WebElement DropDowns : dropdowns) 
						{

							if (DropDowns.getText().contains("Floater")) 
							{
								System.out.println("Cover Type in Excel is :  " + DropDowns.getText());
							} else if (DropDowns.getText().equals("5 - 24 years")||DropDowns.getText().equals("36 - 40 years")||DropDowns.getText().equals("25 - 35 years")
									|| DropDowns.getText().equals("56 - 60 years")||DropDowns.getText().equals("46 - 50 years")||DropDowns.getText().equals("41 - 45 years")
									||DropDowns.getText().equals("25 - 35 years")||DropDowns.getText().equals("36 - 40 years")||DropDowns.getText().equals("61 - 65 years")
									||DropDowns.getText().equals("66 - 70 years")||DropDowns.getText().equals("71 - 75 years")||DropDowns.getText().equals("51 - 55 years")||DropDowns.getText().equals(">75 years")) {

								String EditMembersdetails = editCareheart[ch.n][1];
								if (EditMembersdetails.contains(",")) {
									BaseClass.membres = EditMembersdetails.split(",");
								} else 
								{
									System.out.println("Hello");
								}

								member:
									// total number of members
									for (int i = 0; i <= BaseClass.membres.length; i++) {

										// one by one will take from 83 line
										mcount = Integer.parseInt(BaseClass.membres[mcountindex].toString());
										mcountindex = mcountindex + 1;

										DropDowns.click();
										// List Age of members dropdown
										List<WebElement> List = driver.findElements(By.xpath("//*[@class='ng-binding' and contains(text(), 'year')]"));

										//for (WebElement ListData : List) {
										for(int lData=loopcount;lData<List.size();lData++) {

											if (List.get(lData).getText().contains(FamilyData[mcount][0].toString().trim())) {
												System.out.println("Age of Member is -- :" + List.get(lData).getText());
												Thread.sleep(2000);

												List.get(lData).click();

												if (count == membersSize) {
													break outer;
												} else {
													count = count + 1;
													// break member;
													//loopcount=loopcount+1;
													break member;
												}

											}

										}
									}

							}
						}
					}
				}
		}
		catch(Exception e){
			System.out.println("Unable to Select Total Members.");
			logger.log(LogStatus.PASS, "Test Case is failes because : Unable to Select Total Members.");
		}
		String SumInsured =editCareheart[ch.n][2].toString().trim();
		clickElement(By.xpath("//p[@class='sum_insured_heding ng-binding']"));
		clickbyHover(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		//clickElement(By.xpath("//span[@class='ui-slider-number'][contains(text(), " + SumInsured + ")]"));
		System.out.println("Entered Sum Insured is : "+SumInsured+" Lakhs");			

		// Reading the value of Tenure from Excel
		int Tenure = Integer.parseInt(editCareheart[ch.n][3].toString().trim());
		if (Tenure == 1) {
			clickbyHover(By.xpath(Radio_Tenure1_xpath));	
			//clickElement(By.xpath(Radio_Tenure1_xpath));
			System.out.println("Selected Tenure is : "+Tenure+" Year");
		} else if (Tenure == 2) {
			clickbyHover(By.xpath(Radio_Tenure2_xpath));	
			//clickElement(By.xpath(Radio_Tenure2_xpath));
			System.out.println("Selected Tenure is : "+Tenure+" Year");
		} else if (Tenure == 3) {
			clickbyHover(By.xpath(Radio_Tenure3_xpath));
			//clickElement(By.xpath(Radio_Tenure3_xpath));
			System.out.println("Selected Tenure is : "+Tenure+" Year");
		}
		String TotalMemberPresentonQuotation = driver.findElement(By.xpath("//*[@id='editGetQuot']/div/div/div/div[3]/form/div[1]/div[1]/div[2]/div/ui-dropdown/div/div/a")).getText();
		System.out.println("Total Number of Member on Quotation Page : " + TotalMemberPresentonQuotation);
scrolldown();
		String afterEdit_Quotationpremium_value=driver.findElement(By.xpath("//*[@class='col-md-12 total_premium_text text-center']/b/p/span")).getText();
		System.out.println("After Edit quotation page premium value is :"  +afterEdit_Quotationpremium_value);
		logger.log(LogStatus.PASS, "After Edit quotation page premium value is :"  +afterEdit_Quotationpremium_value);

		try{
			Thread.sleep(3000);
			scrolldown();
			
			clickElement(By.xpath("//button[@data-dismiss='modal']"));
			System.out.println("Successfully clicked on BuyNow Button from Quotation Page. ");
		}
		catch(Exception e){
			System.out.println("Test Case is Failed Because Unable to click on BuyNow from Quotation Page.");
		}
		Thread.sleep(10000);
		afterEdit_Proposalpremium_value = driver.findElement(By.xpath("//p[@class='amount ng-binding']")).getText();
		System.out.println("After Edit proposal page premium value is :"  +afterEdit_Proposalpremium_value);
		logger.log(LogStatus.PASS, "After Edit proposal page premium value is :"  +afterEdit_Proposalpremium_value);

		try {
			Assert.assertEquals(afterEdit_Quotationpremium_value, afterEdit_Proposalpremium_value);
			logger.log(LogStatus.INFO,"Quotaion Premium and Proposal Premium is Verified and Both are Same : "+ afterEdit_Proposalpremium_value.substring(1, afterEdit_Proposalpremium_value.length()));

		} catch (AssertionError e) {
			System.out.println(afterEdit_Proposalpremium_value );
			logger.log(LogStatus.FAIL, "Quotaion Premium and Proposal Premium are not Same");

		}
	}
	

}
