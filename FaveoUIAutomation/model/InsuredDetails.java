package com.org.faveo.model;

import org.openqa.selenium.By;

import com.org.faveo.Base.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class InsuredDetails extends BaseClass{
	
	public static void InsuredDetail(String SheetName) throws Exception
	{
		int mcount;
		String[][] FamilyData = BaseClass.excel_Files(SheetName);
		
		Fluentwait(By.name("ValidRelation0"), 20, "Unable to Read Realtion from Insured Details.");
		
		for (int i = 0; i < BaseClass.membres.length; i++) 
		{
			mcount = Integer.parseInt(BaseClass.membres[i].toString());
			
			String Relation = FamilyData[mcount][1].toString().trim();
			System.out.println("Selected Relation is : "+Relation);
			
			String Title = FamilyData[mcount][2].toString().trim();
			System.out.println("Selected Title is : "+Title);
			
			String FirstName = FamilyData[mcount][3].toString().trim();
			System.out.println("Entered First Name is : "+FirstName);
			
			String LastName = FamilyData[mcount][4].toString().trim();
			System.out.println("Eneterd Last Name is : "+LastName);
			
			String Date = FamilyData[mcount][5].toString().trim();
			System.out.println("Entered Dob is : "+Date);
			
			String HeightFeet = FamilyData[mcount][6].toString().trim();
			System.out.println("Entered Height in Feet is : "+HeightFeet);
			
			String HeightInch = FamilyData[mcount][7].toString().trim();
			System.out.println("Entered Height in Inch is : "+HeightInch);
			
			String Weight = FamilyData[mcount][8].toString().trim();
			System.out.println("Entered Weight is : "+Weight );
			
			if(Relation.equalsIgnoreCase("Self-primary"))
			{
			BaseClass.selecttext("ValidRelation" + i, Relation);
			}
			else 
			{
			BaseClass.selecttext("ValidRelation" + i, Relation);	
			BaseClass.selecttext("ValidRelTitle" + i, Title);
			
			enterText(By.name("RelFName" + i), FirstName);
			
			enterText(By.name("RelLName" + i), LastName);
			
			clickElement(By.name("rel_dob" + i));
			
			enterText(By.name("rel_dob" + i), String.valueOf(Date));
			
			BaseClass.selecttext("relHeightFeet" + i, HeightFeet);
			
			BaseClass.selecttext("relHeightInches" + i, HeightInch);
			
			enterText(By.name("relWeight" + i), Weight);
			}
		}

		clickElement(By.xpath(InsuredNextbtn_xpath));
	}
	
	//----------- function Set Details of Insured For Super Mediclaim -----------------------------------------------------------------
	
	//Function for set Details of Insured 1 Relation
	public static void Insured1_Relation(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_1_Relation_Xpath), 60, "Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_1_Relation_Xpath, relation);
		System.out.println("Data Selected for Insured 1 Relation is:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured Relation 1: " + relation);
	}
	//Function for set Details of Insured 1 First Name
	public static void Insured1_FirstName(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String firstName = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_1_FName_Xpath), firstName);
		System.out.println("Data Entered for Insured 1 First Name is :" + firstName);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 1 First Name : " + firstName);
	}
	//Function for set Details of Insured 1 Last Name
	public static void Insured1_LastName(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String lastName = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_1_LName_Xpath), lastName);
		System.out.println("Data Entered for Isnured 1 Last Name is :" + lastName);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 1 Last Name : " + lastName);
		
	}
	//Function for set Details of Insured 1 DOB Based on Specific Age Group
	public static void Insured1_DOB(String SheetName,int Rownum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		//String DOB = TestCaseData[Rownum][Colnum].toString().trim();
		
		String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String AgeGroup = TestCaseData1[Rownum][6].toString().trim();
		
		String InsuredDOB_OfAgeGroup1 = TestCaseData[Rownum][1].toString().trim();
		String InsuredDOB_OfAgeGroup2 = TestCaseData[Rownum][2].toString().trim();
		String InsuredDOB_OfAgeGroup3 = TestCaseData[Rownum][3].toString().trim();
		String InsuredDOB_OfAgeGroup4 = TestCaseData[Rownum][4].toString().trim();
		String InsuredDOB_OfAgeGroup5 = TestCaseData[Rownum][5].toString().trim();
		String InsuredDOB_OfAgeGroup6 = TestCaseData[Rownum][6].toString().trim();
		String InsuredDOB_OfAgeGroup7 = TestCaseData[Rownum][7].toString().trim();
		
		clickElement(By.xpath(superMediclaim_Insured_1_DOB_Xpath));
		//enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(DOB));
		if(AgeGroup.equalsIgnoreCase("5 - 17 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup1));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB : " + InsuredDOB_OfAgeGroup1);
			
		}
		else if(AgeGroup.equalsIgnoreCase("18 - 25 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup2));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup2);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB : " + InsuredDOB_OfAgeGroup2);
		}
		
		else if(AgeGroup.equalsIgnoreCase("26 - 30 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup3));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup3);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB: " + InsuredDOB_OfAgeGroup2);
		}
		else if(AgeGroup.equalsIgnoreCase("31 - 35 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup4));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup4);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB: " + InsuredDOB_OfAgeGroup4);
		}
		else if(AgeGroup.equalsIgnoreCase("36 - 40 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup5));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup5);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB: " + InsuredDOB_OfAgeGroup5);
		}
		else if(AgeGroup.equalsIgnoreCase("41 - 45 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup6));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup6);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB: " + InsuredDOB_OfAgeGroup6);
		}
		else if(AgeGroup.equalsIgnoreCase("46 - 50 Years")){
			enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup7));
			System.out.println("Data Entered for Insured 1 Date of Birth is:" + InsuredDOB_OfAgeGroup7);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 1 DOB: " + InsuredDOB_OfAgeGroup7);
		}
		
		
	}
	//Function for set Details of Insured 1 Height
	public static void Insured1_height(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_1_Height_Xpath), 60, "Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_1_Height_Xpath, relation);
		System.out.println("Data Selected for Insured 1 Height is:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 1 Height: " + relation);
	}
	//Function for set Details of Insured 1 Inch
	public static void Insured1_inch(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_1_Inch_Xpath), 60, "Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_1_Inch_Xpath, relation);
		System.out.println("Data Selected for Insured 1 Inch is:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 1 Inch: " + relation);
	}
	//Function for set Details of Insured 1 Weight
	public static void Insured1_weight(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String weight = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_1_Weight_Xpath), weight);
		System.out.println("Data Entered for Insured 1 Weight is :" + weight);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 1 Weight: " + weight);
		
	}
	
	//******* Function for set Details of Insured 2 Relation
		public static void Insured2_Relation(String SheetName, int Rownum, int Colnum) throws Exception
		{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String relation = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_2_Relation_Xpath), 60, "Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_2_Relation_Xpath, relation);
			System.out.println("Data Selected for Insured 2 Relation:" + relation);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 Relation: " + relation);
		}
		//Function for set Details of Insured 2 First Name
		public static void Insured2_FirstName(String SheetName,int Rownum, int Colnum) throws Exception
		{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String firstName = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_2_FName_Xpath), firstName);
			System.out.println("Data Eneterd for Insured 2 First Name is :" + firstName);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 First Name: " + firstName);
			
		}
		//Function for set Details of Insured 2 Last Name
		public static void Insured2_LastName(String SheetName,int Rownum, int Colnum) throws Exception
		{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String lastName = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_2_LName_Xpath), lastName);
			System.out.println("Data Entered for Insured 2 Last Name is :" + lastName);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 Last Name: " + lastName);
			
		}
		/*//Function for set Details of Insured 2 DOB
		public static void Insured2_DOB(String SheetName,int Rownum,int Colnum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String DOB = TestCaseData[Rownum][Colnum].toString().trim();
			clickElement(By.xpath(superMediclaim_Insured_2_DOB_Xpath));
			enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(DOB));
			System.out.println("Data Entered for Insured 2 Date of Birth is:" + DOB);
		}*/
		
		//Function for set Details of Insured 1 DOB Based on Specific Age Group
		public static void Insured2_DOB(String SheetName,int Rownum) throws Exception{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			//String DOB = TestCaseData[Rownum][Colnum].toString().trim();
			String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
			String AgeGroup = TestCaseData1[Rownum][7].toString().trim();

			String InsuredDOB_OfAgeGroup1 = TestCaseData[Rownum][1].toString().trim();
			String InsuredDOB_OfAgeGroup2 = TestCaseData[Rownum][2].toString().trim();
			String InsuredDOB_OfAgeGroup3 = TestCaseData[Rownum][3].toString().trim();
			String InsuredDOB_OfAgeGroup4 = TestCaseData[Rownum][4].toString().trim();
			String InsuredDOB_OfAgeGroup5 = TestCaseData[Rownum][5].toString().trim();
			String InsuredDOB_OfAgeGroup6 = TestCaseData[Rownum][6].toString().trim();
			String InsuredDOB_OfAgeGroup7 = TestCaseData[Rownum][7].toString().trim();
			
			clickElement(By.xpath(superMediclaim_Insured_2_DOB_Xpath));
			//enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath), String.valueOf(DOB));
			if(AgeGroup.equalsIgnoreCase("5 - 17 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup1));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup1);
			}
			else if(AgeGroup.equalsIgnoreCase("18 - 25 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup2));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup2);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup2);
			}
			
			else if(AgeGroup.equalsIgnoreCase("26 - 30 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup3));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup3);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup3);
			}
			else if(AgeGroup.equalsIgnoreCase("31 - 35 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup4));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup4);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup4);
			}
			else if(AgeGroup.equalsIgnoreCase("36 - 40 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup5));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup5);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup5);
			}
			else if(AgeGroup.equalsIgnoreCase("41 - 45 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup6));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup6);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup6);
				
			}
			else if(AgeGroup.equalsIgnoreCase("46 - 50 Years")){
				enterText(By.xpath(superMediclaim_Insured_2_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup7));
				System.out.println("Data Entered for Insured 2 Date of Birth is:" + InsuredDOB_OfAgeGroup7);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 2 DOB: " + InsuredDOB_OfAgeGroup7);
			}
			
			
		}
		
		
		//Function for set Details of Insured 2 Height
		public static void Insured2_height(String SheetName, int Rownum, int Colnum) throws Exception
		{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String relation = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_2_Height_Xpath), 60, "Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_2_Height_Xpath, relation);
			System.out.println("Data Selected for Insured 2 Height is:" + relation);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 Height: " + relation);
		}
		//Function for set Details of Insured 2 Inch
		public static void Insured2_inch(String SheetName, int Rownum, int Colnum) throws Exception
		{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String inch = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_2_Inch_Xpath), 60, "Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_2_Inch_Xpath, inch);
			System.out.println("Data Selected for Insured 2 Inch is:" + inch);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 Inch: " + inch);
		}
		//Function for set Details of Insured 2 Weight
		public static void Insured2_weight(String SheetName,int Rownum, int Colnum) throws Exception
		{
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String weight = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_2_Weight_Xpath), weight);
			System.out.println("Data Entered for Insured 2 Weight is :" + weight);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 2 Weight: " + weight);
			
		}
		
	//******** Function for set Details of Insured 3 Relation
	public static void Insured3_Relation(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_3_Relation_Xpath), 60,
				"Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_3_Relation_Xpath, relation);
		System.out.println("Data Selected for Insured 3 Relation:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 3 Relation: " + relation);
	}

	// Function for set Details of Insured 3 First Name
	public static void Insured3_FirstName(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String firstName = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_3_FName_Xpath), firstName);
		System.out.println("Data Eneterd for Insured 3 First Name is :" + firstName);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 3 First Name: " + firstName);

	}

	// Function for set Details of Insured 3 Last Name
	public static void Insured3_LastName(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String lastName = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_3_LName_Xpath), lastName);
		System.out.println("Data Entered for Insured 3 Last Name is :" + lastName);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 3 Last Name: " + lastName);

	}

	/*// Function for set Details of Insured 3 DOB
	public static void Insured3_DOB(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String DOB = TestCaseData[Rownum][Colnum].toString().trim();
		clickElement(By.xpath(superMediclaim_Insured_3_DOB_Xpath));
		enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(DOB));
		System.out.println("Data Entered for Insured 3 Date of Birth is:" + DOB);
	}*/
	
	// Function for set Details of Insured 1 DOB Based on Specific Age Group
	public static void Insured3_DOB(String SheetName, int Rownum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		// String DOB = TestCaseData[Rownum][Colnum].toString().trim();
		
		String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String AgeGroup = TestCaseData1[Rownum][8].toString().trim();
		

		String InsuredDOB_OfAgeGroup1 = TestCaseData[Rownum][1].toString().trim();
		String InsuredDOB_OfAgeGroup2 = TestCaseData[Rownum][2].toString().trim();
		String InsuredDOB_OfAgeGroup3 = TestCaseData[Rownum][3].toString().trim();
		String InsuredDOB_OfAgeGroup4 = TestCaseData[Rownum][4].toString().trim();
		String InsuredDOB_OfAgeGroup5 = TestCaseData[Rownum][5].toString().trim();
		String InsuredDOB_OfAgeGroup6 = TestCaseData[Rownum][6].toString().trim();
		String InsuredDOB_OfAgeGroup7 = TestCaseData[Rownum][7].toString().trim();

		clickElement(By.xpath(superMediclaim_Insured_3_DOB_Xpath));
		// enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath),
		// String.valueOf(DOB));
		if (AgeGroup.equalsIgnoreCase("5 - 17 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup1));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup1);
		} else if (AgeGroup.equalsIgnoreCase("18 - 25 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup2));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup2);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup2);
		}

		else if (AgeGroup.equalsIgnoreCase("26 - 30 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup3));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup3);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup3);
		} else if (AgeGroup.equalsIgnoreCase("31 - 35 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup4));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup4);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup4);
		} else if (AgeGroup.equalsIgnoreCase("36 - 40 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup5));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup5);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup5);
		} else if (AgeGroup.equalsIgnoreCase("41 - 45 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup6));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup6);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup6);
		} else if (AgeGroup.equalsIgnoreCase("46 - 50 Years")) {
			enterText(By.xpath(superMediclaim_Insured_3_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup7));
			System.out.println("Data Entered for Insured 3 Date of Birth is:" + InsuredDOB_OfAgeGroup7);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 3 DOB: " + InsuredDOB_OfAgeGroup7);
		}

	}
	

	// Function for set Details of Insured 3 Height
	public static void Insured3_height(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_3_Height_Xpath), 60,
				"Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_3_Height_Xpath, relation);
		System.out.println("Data Selected for Insured 3 Height is:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 3 Height: " + relation);
	}

	// Function for set Details of Insured 3 Inch
	public static void Insured3_inch(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String inch = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_3_Inch_Xpath), 60,
				"Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_3_Inch_Xpath, inch);
		System.out.println("Data Selected for Insured 3 Inch is:" + inch);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 3 Inch: " + inch);
		
	}

	// Function for set Details of Insured 3 Weight
	public static void Insured3_weight(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String weight = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_3_Weight_Xpath), weight);
		System.out.println("Data Entered for Insured 3 Weight is :" + weight);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 3 Weight: " + weight);

	}
	
	//******** Function for set Details of Insured 4 Relation
		public static void Insured4_Relation(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String relation = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_4_Relation_Xpath), 60,
					"Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_4_Relation_Xpath, relation);
			System.out.println("Data Selected for Insured 3 Relation:" + relation);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 4 Relation: " + relation);
		}

		// Function for set Details of Insured 4 First Name
		public static void Insured4_FirstName(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String firstName = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_4_FName_Xpath), firstName);
			System.out.println("Data Eneterd for Insured 4 First Name is :" + firstName);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 4 First Name: " + firstName);

		}

		// Function for set Details of Insured 4 Last Name
		public static void Insured4_LastName(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String lastName = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_4_LName_Xpath), lastName);
			System.out.println("Data Entered for Insured 4 Last Name is :" + lastName);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 4 Last Name: " + lastName);

		}

		/*// Function for set Details of Insured 4 DOB
		public static void Insured4_DOB(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String DOB = TestCaseData[Rownum][Colnum].toString().trim();
			clickElement(By.xpath(superMediclaim_Insured_4_DOB_Xpath));
			enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(DOB));
			System.out.println("Data Entered for Insured 4 Date of Birth is:" + DOB);
		}*/
		
		// Function for set Details of Insured 1 DOB Based on Specific Age Group
		public static void Insured4_DOB(String SheetName, int Rownum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			// String DOB = TestCaseData[Rownum][Colnum].toString().trim();
			String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
			String AgeGroup = TestCaseData1[Rownum][9].toString().trim();

			String InsuredDOB_OfAgeGroup1 = TestCaseData[Rownum][1].toString().trim();
			String InsuredDOB_OfAgeGroup2 = TestCaseData[Rownum][2].toString().trim();
			String InsuredDOB_OfAgeGroup3 = TestCaseData[Rownum][3].toString().trim();
			String InsuredDOB_OfAgeGroup4 = TestCaseData[Rownum][4].toString().trim();
			String InsuredDOB_OfAgeGroup5 = TestCaseData[Rownum][5].toString().trim();
			String InsuredDOB_OfAgeGroup6 = TestCaseData[Rownum][6].toString().trim();
			String InsuredDOB_OfAgeGroup7 = TestCaseData[Rownum][7].toString().trim();

			clickElement(By.xpath(superMediclaim_Insured_4_DOB_Xpath));
			// enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath),
			// String.valueOf(DOB));
			if (AgeGroup.equalsIgnoreCase("5 - 17 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup1));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup1);
			} else if (AgeGroup.equalsIgnoreCase("18 - 25 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup2));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup2);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup2);
			}

			else if (AgeGroup.equalsIgnoreCase("26 - 30 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup3));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup3);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup3);
			} else if (AgeGroup.equalsIgnoreCase("31 - 35 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup4));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup4);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup4);
			} else if (AgeGroup.equalsIgnoreCase("36 - 40 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup5));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup5);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup5);
			} else if (AgeGroup.equalsIgnoreCase("41 - 45 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup6));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup6);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup6);
			} else if (AgeGroup.equalsIgnoreCase("46 - 50 Years")) {
				enterText(By.xpath(superMediclaim_Insured_4_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup7));
				System.out.println("Data Entered for Insured 4 Date of Birth is:" + InsuredDOB_OfAgeGroup7);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 4 DOB: " + InsuredDOB_OfAgeGroup7);
			}

		}

		// Function for set Details of Insured 4 Height
		public static void Insured4_height(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String relation = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_4_Height_Xpath), 60,
					"Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_4_Height_Xpath, relation);
			System.out.println("Data Selected for Insured 4 Height is:" + relation);
			logger.log(LogStatus.PASS, "Data Entered for Isnured Height: " + relation);
		}

		// Function for set Details of Insured 4 Inch
		public static void Insured4_inch(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String inch = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_4_Inch_Xpath), 60,
					"Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_4_Inch_Xpath, inch);
			System.out.println("Data Selected for Insured 4 Inch is:" + inch);
			logger.log(LogStatus.PASS, "Data Entered for Isnured Inch: " + inch);
		}

		// Function for set Details of Insured 4 Weight
		public static void Insured4_weight(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String weight = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_4_Weight_Xpath), weight);
			System.out.println("Data Entered for Insured 4 Weight is :" + weight);
			logger.log(LogStatus.PASS, "Data Entered for Isnured Weight: " + weight);

		}
		
	// ******** Function for set Details of Insured 5 Relation
	public static void Insured5_Relation(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_5_Relation_Xpath), 60,
				"Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_5_Relation_Xpath, relation);
		System.out.println("Data Selected for Insured 5 Relation:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 5 Relation: " + relation);
	}

	// Function for set Details of Insured 5 First Name
	public static void Insured5_FirstName(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String firstName = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_5_FName_Xpath), firstName);
		System.out.println("Data Eneterd for Insured 5 First Name is :" + firstName);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 5 First Name: " + firstName);

	}

	// Function for set Details of Insured 5 Last Name
	public static void Insured5_LastName(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String lastName = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_5_LName_Xpath), lastName);
		System.out.println("Data Entered for Insured 5 Last Name is :" + lastName);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 5 Last Name: " + lastName);

	}

	/*// Function for set Details of Insured 5 DOB
	public static void Insured5_DOB(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String DOB = TestCaseData[Rownum][Colnum].toString().trim();
		clickElement(By.xpath(superMediclaim_Insured_5_DOB_Xpath));
		enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(DOB));
		System.out.println("Data Entered for Insured 5 Date of Birth is:" + DOB);
	}*/
	
	// Function for set Details of Insured 5 DOB Based on Specific Age Group
	public static void Insured5_DOB(String SheetName, int Rownum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		// String DOB = TestCaseData[Rownum][Colnum].toString().trim();
		
		String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
		String AgeGroup = TestCaseData1[Rownum][10].toString().trim();

		String InsuredDOB_OfAgeGroup1 = TestCaseData[Rownum][1].toString().trim();
		String InsuredDOB_OfAgeGroup2 = TestCaseData[Rownum][2].toString().trim();
		String InsuredDOB_OfAgeGroup3 = TestCaseData[Rownum][3].toString().trim();
		String InsuredDOB_OfAgeGroup4 = TestCaseData[Rownum][4].toString().trim();
		String InsuredDOB_OfAgeGroup5 = TestCaseData[Rownum][5].toString().trim();
		String InsuredDOB_OfAgeGroup6 = TestCaseData[Rownum][6].toString().trim();
		String InsuredDOB_OfAgeGroup7 = TestCaseData[Rownum][7].toString().trim();

		clickElement(By.xpath(superMediclaim_Insured_5_DOB_Xpath));
		// enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath),
		// String.valueOf(DOB));
		if (AgeGroup.equalsIgnoreCase("5 - 17 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup1));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup1);
		} else if (AgeGroup.equalsIgnoreCase("18 - 25 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup2));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup2);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup2);
		}

		else if (AgeGroup.equalsIgnoreCase("26 - 30 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup3));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup3);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup3);
		} else if (AgeGroup.equalsIgnoreCase("31 - 35 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup4));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup4);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup4);
		} else if (AgeGroup.equalsIgnoreCase("36 - 40 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup5));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup5);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup5);
		} else if (AgeGroup.equalsIgnoreCase("41 - 45 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup6));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup6);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup6);
		} else if (AgeGroup.equalsIgnoreCase("46 - 50 Years")) {
			enterText(By.xpath(superMediclaim_Insured_5_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup7));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + InsuredDOB_OfAgeGroup7);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 5 DOB: " + InsuredDOB_OfAgeGroup7);
		}

	}

	// Function for set Details of Insured 5 Height
	public static void Insured5_height(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String relation = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_5_Height_Xpath), 60,
				"Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_5_Height_Xpath, relation);
		System.out.println("Data Selected for Insured 5 Height is:" + relation);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 5 Height: " + relation);
	}

	// Function for set Details of Insured 5 Inch
	public static void Insured5_inch(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String inch = TestCaseData[Rownum][Colnum].toString().trim();
		Fluentwait(By.name(superMediclaim_Insured_5_Inch_Xpath), 60,
				"Page is loading Slow so unable to click on Title.");
		selecttext(superMediclaim_Insured_5_Inch_Xpath, inch);
		System.out.println("Data Selected for Insured 5 Inch is:" + inch);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 5 Inch: " + inch);
	}

	// Function for set Details of Insured 5 Weight
	public static void Insured5_weight(String SheetName, int Rownum, int Colnum) throws Exception {
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String weight = TestCaseData[Rownum][Colnum].toString().trim();
		enterText(By.xpath(superMediclaim_Insured_5_Weight_Xpath), weight);
		System.out.println("Data Entered for Insured 5 Weight is :" + weight);
		logger.log(LogStatus.PASS, "Data Entered for Isnured 5 Weight: " + weight);

	}

	// ******** Function for set Details of Insured 6 Relation
		public static void Insured6_Relation(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String relation = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_6_Relation_Xpath), 60,
					"Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_6_Relation_Xpath, relation);
			System.out.println("Data Selected for Insured 6 Relation:" + relation);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 6 Relation: " + relation);
		}

		// Function for set Details of Insured 6 First Name
		public static void Insured6_FirstName(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String firstName = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_6_FName_Xpath), firstName);
			System.out.println("Data Eneterd for Insured 6 First Name is :" + firstName);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 6 First Name: " + firstName);

		}

		// Function for set Details of Insured 6 Last Name
		public static void Insured6_LastName(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String lastName = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_6_LName_Xpath), lastName);
			System.out.println("Data Entered for Insured 6 Last Name is :" + lastName);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 6 Last Name: " + lastName);

		}

		/*// Function for set Details of Insured 6 DOB
		public static void Insured6_DOB(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String DOB = TestCaseData[Rownum][Colnum].toString().trim();
			clickElement(By.xpath(superMediclaim_Insured_6_DOB_Xpath));
			enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(DOB));
			System.out.println("Data Entered for Insured 5 Date of Birth is:" + DOB);
		}*/
		
		// Function for set Details of Insured 6 DOB Based on Specific Age Group
		public static void Insured6_DOB(String SheetName, int Rownum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			// String DOB = TestCaseData[Rownum][Colnum].toString().trim();
			
			String[][] TestCaseData1 = BaseClass.excel_Files("SuperMediclaimCancer_TestData");
			String AgeGroup = TestCaseData1[Rownum][11].toString().trim();

			String InsuredDOB_OfAgeGroup1 = TestCaseData[Rownum][1].toString().trim();
			String InsuredDOB_OfAgeGroup2 = TestCaseData[Rownum][2].toString().trim();
			String InsuredDOB_OfAgeGroup3 = TestCaseData[Rownum][3].toString().trim();
			String InsuredDOB_OfAgeGroup4 = TestCaseData[Rownum][4].toString().trim();
			String InsuredDOB_OfAgeGroup5 = TestCaseData[Rownum][5].toString().trim();
			String InsuredDOB_OfAgeGroup6 = TestCaseData[Rownum][6].toString().trim();
			String InsuredDOB_OfAgeGroup7 = TestCaseData[Rownum][7].toString().trim();
			
			clickElement(By.xpath(superMediclaim_Insured_6_DOB_Xpath));
			// enterText(By.xpath(superMediclaim_Insured_1_DOB_Xpath),
			// String.valueOf(DOB));
			if (AgeGroup.equalsIgnoreCase("5 - 17 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup1));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup1);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup1);
			} else if (AgeGroup.equalsIgnoreCase("18 - 25 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup2));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup2);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup2);
			}

			else if (AgeGroup.equalsIgnoreCase("26 - 30 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup3));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup3);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup3);
			} else if (AgeGroup.equalsIgnoreCase("31 - 35 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup4));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup4);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup4);
			} else if (AgeGroup.equalsIgnoreCase("36 - 40 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup5));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup5);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup5);
			} else if (AgeGroup.equalsIgnoreCase("41 - 45 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup6));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup6);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup6);
			} else if (AgeGroup.equalsIgnoreCase("46 - 50 Years")) {
				enterText(By.xpath(superMediclaim_Insured_6_DOB_Xpath), String.valueOf(InsuredDOB_OfAgeGroup7));
				System.out.println("Data Entered for Insured 6 Date of Birth is:" + InsuredDOB_OfAgeGroup7);
				logger.log(LogStatus.PASS, "Data Entered for Isnured 6 DOB: " + InsuredDOB_OfAgeGroup7);
			}

		}


		// Function for set Details of Insured 6 Height
		public static void Insured6_height(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String relation = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_6_Height_Xpath), 60,
					"Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_6_Height_Xpath, relation);
			System.out.println("Data Selected for Insured 6 Height is:" + relation);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 6 Height: " + relation);
		}

		// Function for set Details of Insured 6 Inch
		public static void Insured6_inch(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String inch = TestCaseData[Rownum][Colnum].toString().trim();
			Fluentwait(By.name(superMediclaim_Insured_6_Inch_Xpath), 60,
					"Page is loading Slow so unable to click on Title.");
			selecttext(superMediclaim_Insured_6_Inch_Xpath, inch);
			System.out.println("Data Selected for Insured 5 Inch is:" + inch);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 6 inch: " + inch);
		}

		// Function for set Details of Insured 6 Weight
		public static void Insured6_weight(String SheetName, int Rownum, int Colnum) throws Exception {
			String[][] TestCaseData = BaseClass.excel_Files(SheetName);
			String weight = TestCaseData[Rownum][Colnum].toString().trim();
			enterText(By.xpath(superMediclaim_Insured_6_Weight_Xpath), weight);
			System.out.println("Data Entered for Insured 6 Weight is :" + weight);
			logger.log(LogStatus.PASS, "Data Entered for Isnured 6 Weight: " + weight);

		}
		
		//Function for click on Next button in Insured Details Page
		public static void clickOnNextButton(){
			clickElement(By.xpath(nextButtonSuperMediclaim_Xpath));
			System.out.println("Clicked on Next Button");
			logger.log(LogStatus.PASS, "Clicked on Next Button");
		}


}
