package com.org.faveo.model;

import org.openqa.selenium.By;

import com.org.faveo.Base.BaseClass;
import com.org.faveo.healthinsurance.AccountnSettingsInterface;
import com.relevantcodes.extentreports.LogStatus;

public class ProposerDetailsPageFn extends BaseClass implements AccountnSettingsInterface
{

	public static void ProposerTitle(String SheetName, int Rownum, int Colnum) throws Exception
	{
		scrollup();
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Title = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Title is:" + Title);
		Fluentwait(By.name("ValidTitle"), 60, "Page is loading Slow so unable to click on Title.");
		selecttext("ValidTitle", Title);
		logger.log(LogStatus.PASS, "Data Entered for Propser Title: "+ Title);
	}
	
	public static void ProposerDOB(String SheetName,int Rownum,int Colnum) throws Exception{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String DOB = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Entered Proposer Date of Birth is:" + DOB);
		clickElement(By.id(Dob_Proposer_id));
		enterText(By.id(Dob_Proposer_id), String.valueOf(DOB));
		logger.log(LogStatus.PASS, "Data Entered for Propser DOB: "+ DOB);
	}

	public static void ProposerAddressLine1(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String address1 = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Entered AdressLine1 is :" + address1);
		enterText(By.xpath(Textbox_AddressLine1_xpath), address1);
		logger.log(LogStatus.PASS, "Data Entered for Propser Address Line 1: "+ address1);
		
	}
	
	public static void ProposerAddressLine2(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String address2 = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Entered AdressLine1 is :" + address2);
		enterText(By.xpath(Textbox_AddressLine2_xpath), address2);
		logger.log(LogStatus.PASS, "Data Entered for Propser Address Line 2: "+ address2);
	}
	
	public static void Pincode(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		int Pincode = Integer.parseInt(TestCaseData[Rownum][Colnum].toString().trim());
		System.out.println("Entered Pincode is : "+Pincode);
		enterText(By.xpath(Textbox_Pincode_xpath), String.valueOf(Pincode));
		logger.log(LogStatus.PASS, "Data Entered for Pin Code: "+ Pincode);
	}

	public static void ProposerHeightFeet(String SheetName, int Rownum, int Colnum) throws Exception
	{
		
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String HeightProposer_Feet = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Proposer Height in Feet is :" + HeightProposer_Feet);
		selecttext("heightFeet", HeightProposer_Feet);
		logger.log(LogStatus.PASS, "Data Entered for Height Feet: "+ HeightProposer_Feet);
	}

	public static void ProposerHeightInch(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String HeightProposer_Inch = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Proposer Height in Inch is :" + HeightProposer_Inch);
		selecttext("heightInches", HeightProposer_Inch);
		logger.log(LogStatus.PASS, "Data Entered for Height Inch: "+ HeightProposer_Inch);
	}
	
	public static void ProposerWeight(String SheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String Weight = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Entered Proposer Weight is :" + Weight);
		enterText(By.xpath(Textbox_Weight_xpath), String.valueOf(Weight));
		logger.log(LogStatus.PASS, "Data Entered for Weight: "+ Weight);
	}
	
	public static void ProposerNominee(String SheetName,int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String NomineeName = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Entered Nominee name   is:" + NomineeName);
		enterText(By.xpath(Textbox_NomineeName_xpath), NomineeName);
		logger.log(LogStatus.PASS, "Data Entered for Propser Nominee: "+ NomineeName);
	}
	
	
	
	public static void ProposerNomineeRelation(String SheetName, int Rownum, int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String NomineeRelation = TestCaseData[Rownum][Colnum].toString().trim();
		System.out.println("Selected Nominee relation is:" + NomineeRelation);
		clickElement(By.xpath("//Select[@name='nomineeRelation']"));
		clickElement(By.xpath("//Select[@name='nomineeRelation']//option[contains(text(),"+"'"+NomineeRelation+"'"+")]"));
		/*selecttext("nomineeRelation", NomineeRelation);*/
		logger.log(LogStatus.PASS, "Data Entered for Propser Nominee Relation: "+ NomineeRelation);
	}
	
	public static void PanCardNumber(String SheetName,int Rownum,int Colnum) throws Exception
	{
		String[][] TestCaseData = BaseClass.excel_Files(SheetName);
		String PanCard = TestCaseData[Rownum][Colnum].toString().trim();
	
		Boolean PanCardNumberPresence = driver.findElements(By.xpath(Textbox_Pancard_xpath)).size() > 0;
		if (PanCardNumberPresence == true) {
			enterText(By.xpath(Textbox_Pancard_xpath), PanCard);
			System.out.println("Entered Pancard Number is :" + PanCard);
		} else {
			System.out.println("PAN Card Field is not Present");
			logger.log(LogStatus.PASS, "Data Entered for Pan Details: "+ PanCard);
		}
	}	
	
	
}
